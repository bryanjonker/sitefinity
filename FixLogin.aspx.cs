﻿namespace SitefinityWebApp
{
    using System;

    using Telerik.Sitefinity.Security;
    using Telerik.Sitefinity.Security.Model;

    public partial class FixLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userManager = UserManager.GetManager();
            var profileManager = UserProfileManager.GetManager();
            userManager.Provider.SuppressSecurityChecks = true;
            profileManager.Provider.SuppressSecurityChecks = true;
            System.Web.Security.MembershipCreateStatus status;
            var user = userManager.CreateUser("jonker", "password@password", "email@test.com", "Question1", "Answer1", true, null, out status);
            var userProfile = profileManager.CreateProfile(user, Guid.NewGuid(), typeof(SitefinityProfile)) as SitefinityProfile;
            userProfile.FirstName = "First";
            userProfile.LastName = "Last";
            user.IsBackendUser = true;
            userManager.SaveChanges();
            profileManager.SaveChanges();
            profileManager.Provider.SuppressSecurityChecks = false;
            userManager.Provider.SuppressSecurityChecks = false;
        }
    }
}