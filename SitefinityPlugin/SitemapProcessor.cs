﻿namespace SitefinityWebApp.SitefinityPlugin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Telerik.Sitefinity.SitemapGenerator.Data;

    using FacetedSearch = UIUC.Custom.DAL.DataAccess.FacetedSearch;
    using Faculty = UIUC.Custom.DAL.DataAccess.Faculty;

    public static class SitemapProcessor
    {
        public static IEnumerable<SitemapEntry> AddItemsToSitemapEntries(IEnumerable<SitemapEntry> originalItems)
        {
            var newItems = originalItems.ToList();
            newItems.AddRange(new Faculty.ExportProcess().GetAllFacultyAppointmentsFacultyProfile().Select(profile => new SitemapEntry
            {
                LastModified = DateTime.Now,
                Location = $"http://education.illinois.edu/faculty/{profile.Username}",
                Priority = 1
            }));
            newItems.AddRange(new FacetedSearch.ExportProcess().GetAllCourses().Select(profile => new SitemapEntry
            {
                LastModified = DateTime.Now,
                Location = $"http://education.illinois.edu/course/{profile.Rubric}/{profile.CourseNumber}",
                Priority = 1
            }));
            return newItems;
        }
    }
}