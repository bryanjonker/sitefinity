﻿namespace SitefinityWebApp.SitefinityPlugin
{
    using System.Web;
    using System.Web.UI;

    public static class TitleManager
    {
        public static void ChangeTitle(string title)
        {
            var currentHandler = HttpContext.Current.CurrentHandler as Page;
            if (currentHandler?.Header != null)
            {
                var header = currentHandler.Header.FindControl("TitlePlaceHolder") ?? currentHandler.Header;
                if (header.FindControl("title") != null)
                {
                    ((LiteralControl)header.FindControl("title")).Text = title;
                }
                else
                {
                    header.Controls.Add(new LiteralControl("title")
                    {
                        ID = "title",
                        Text = title
                    });
                }
            }
        }
    }
}