﻿namespace SitefinityWebApp.SitefinityPlugin
{
    using System;
    using System.Linq;

    using Telerik.Sitefinity;
    using Telerik.Sitefinity.Modules.Pages;
    using Telerik.Sitefinity.Security;
    using Telerik.Sitefinity.Versioning;
    using Telerik.Sitefinity.Versioning.Web.Services;

    public static class PageInformation
    {
        public static string GetLastModifiedUser(string url)
        {
            var pageManager = PageManager.GetManager();
            var tempPage = pageManager.GetPageDataList().Where(t => t.NavigationNode.Title == "Great Minds Think Illinois").SingleOrDefault();
            var allPages = pageManager.GetPageDataList().Skip(600);
            var page = pageManager.GetPageDataList().Where(t => t.NavigationNode.UrlName == url).SingleOrDefault();
            if (page == null)
            {
                return "Page not found";
            }
            var modifiedBy = SecurityManager.GetFormattedUserName(page.LastModifiedBy);

            var vManager = VersionManager.GetManager();
            var history = vManager.GetItemVersionHistory(page.Id);
            var first = history.First();
            var change = new WcfChange(first);
            var mod = change.CreatedByUserName;
            var modifiedBy2 = SecurityManager.GetPrincipalName(first.Owner);

            return mod;
        }
    }
}