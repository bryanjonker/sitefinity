﻿namespace SitefinityWebApp.GoogleSearchAppliance
{
    public class FacultyItem
    {
        public string Department { get; set; }

        public string Image { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }
    }
}