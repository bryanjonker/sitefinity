﻿namespace SitefinityWebApp.GoogleSearchAppliance.FacetedSearch
{
    using System;
    using System.Linq;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class CourseItem
    {
        public CourseItem() {}

        public CourseItem(string rubric, string courseNumber, string title, string creditHours, string faculty, string formats, string termsOffered, string fullDescription, string description)
        {
            this.Id = rubric + courseNumber;
            this.Name = $"{rubric} {courseNumber}: {title}";
            this.Rubric = rubric;
            this.CourseNumber = courseNumber;
            this.FullDescription = fullDescription;
            this.ShortDescription = description;
            this.CreditHours = creditHours;
            this.Formats = formats;
            this.TermsOffered = termsOffered;
            this.Faculty = faculty;
        }

        public CourseItem(Course course)
        {
            this.Id = course.Rubric + course.CourseNumber;
            this.Name = $"{course.Rubric} {course.CourseNumber}: {course.Title}";
            this.Rubric = course.Rubric;
            this.CourseNumber = course.CourseNumber;
            this.FullDescription = course.Description;
            var characterIndexNumber = 225;
            var characterItem = course.Description.Length <= characterIndexNumber ? -1 : course.Description.IndexOf(" ", characterIndexNumber, StringComparison.Ordinal);
            this.ShortDescription = characterItem == -1 ? course.Description : course.Description.Substring(0, characterItem) + "...";
            this.CreditHours = course.CreditHours;
            this.Formats = course.Formats;
            this.TermsOffered = course.TermsOffered;
            this.Faculty = string.Join(", ", course.Sections.OrderBy(p => p.FullName).Select(p => p.GetHtmlName()).Distinct());
            if (string.IsNullOrWhiteSpace(this.Faculty))
            {
                this.Faculty = "N/A";
            }
        }

        public string CreditHours { get; set; }

        public string CourseNumber { get; set; }

        public string Faculty { get; set; }

        public string Formats { get; set; }

        public string FullDescription { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Rubric { get; set; }

        public string ShortDescription { get; set; }

        public string TermsOffered { get; set; }
    }
}