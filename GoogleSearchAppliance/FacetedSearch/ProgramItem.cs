﻿namespace SitefinityWebApp.GoogleSearchAppliance.FacetedSearch
{
    using System;
    using System.Linq;
    using System.Net;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class ProgramItem
    {
        public ProgramItem() {}

        public ProgramItem(string title, string onlineUrl, string url, string objectives, string onlineObjectives, string offCampusObjectives, string department, string fullDescription, string description)
        {
            this.Title = title;
            this.FullDescription =WebUtility.HtmlDecode(fullDescription);
            this.ShortDescription =WebUtility.HtmlDecode(description);
            this.OnlineUrl = !onlineUrl.Equals("none", StringComparison.OrdinalIgnoreCase) ? onlineUrl: string.Empty;
            this.Url =!url.Equals("none", StringComparison.OrdinalIgnoreCase) ? url : string.Empty;
            this.Objectives =!objectives.Equals("none", StringComparison.OrdinalIgnoreCase) ? objectives : string.Empty;
            this.OnlineObjectives =!onlineObjectives.Equals("none", StringComparison.OrdinalIgnoreCase) ? onlineObjectives : string.Empty; 
            this.OffcampusObjectives =!offCampusObjectives.Equals("none", StringComparison.OrdinalIgnoreCase) ? offCampusObjectives : string.Empty;
            this.Department=department;
        }

        public ProgramItem(Program program)
        {
            this.FullDescription = program.Description;
            this.ShortDescription = program.Description.IndexOf(". ") > 0 ? program.Description.Substring(0, program.Description.IndexOf(". ")) : program.Description;
            this.OnlineUrl = program.OnlineUrl;
            this.Url = program.Url;
            this.OffcampusObjectives = string.Join(", ", program.Credentials.Where(o => o.IsOffCampus).OrderBy(o => (int)o.CredentialType).Select(x => x.CredentialTypeString));
            this.OnlineObjectives = string.Join(", ", program.Credentials.Where(o => o.IsOnline).OrderBy(o => (int)o.CredentialType).Select(x => x.CredentialTypeString));
            this.Objectives = string.Join(", ", program.Credentials.Where(o => o.IsOncampus).OrderBy(o => (int)o.CredentialType).Select(x => x.CredentialTypeString));
            this.Department = program.DepartmentName;
            this.Title = program.Name;
        }

        public string FullDescription { get; set; }

        public string Objectives { get; set; }

        public string ShortDescription { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        internal string Department { get; set; }

        internal string OffcampusDepartmentName => this.Department + " Off-Campus";

        internal string OffcampusObjectives { get; set; }

        internal string OncampusDepartmentName => this.Department == "The College of Education" ? this.Department : this.Department + " On-Campus";

        internal string OnlineDepartmentName => this.Department + " Online";

        internal string OnlineObjectives { get; set; }

        internal string OnlineUrl { get; set; }

        public ProgramItem GetOffcampusVersion()
        {
            return new ProgramItem
                       {
                           Department = this.Department,
                           FullDescription = this.FullDescription,
                           Objectives = this.OffcampusObjectives,
                           ShortDescription = this.ShortDescription,
                           Title = this.Title,
                           Url = this.OnlineUrl
                       };
        }

        public ProgramItem GetOnlineVersion()
        {
            return new ProgramItem
                       {
                           Department = this.Department,
                           FullDescription = this.FullDescription,
                           Objectives = this.OnlineObjectives,
                           ShortDescription = this.ShortDescription,
                           Title = this.Title,
                           Url = this.OnlineUrl
                       };
        }
    }
}