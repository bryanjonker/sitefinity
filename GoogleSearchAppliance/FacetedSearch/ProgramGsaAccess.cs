﻿namespace SitefinityWebApp.GoogleSearchAppliance.FacetedSearch
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;

    using Newtonsoft.Json;

    using SitefinityWebApp.Mvc.Models.FacultySearch;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch;

    public class ProgramGsaAccess
    {
        private readonly string googleUrl;

        public ProgramGsaAccess(string googleUrl)
        {
            this.googleUrl=googleUrl.Replace("http://", "https://").Trim('/');
        }

        public IEnumerable<string> GetSuggestions(string searchTerm)
        {
            var returnValue = new List<string>();
            var url = $"{this.googleUrl}/suggest?q={searchTerm}&max=10&format=rich&client=education_program_frontend&site=education_program_collection";
            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream!=null)
            {
                using (var reader = new StreamReader(stream))
                {
                    var returnString = reader.ReadToEnd();
                    var information = JsonConvert.DeserializeObject<Information>(returnString);
                    returnValue.AddRange(information.results.Select(x => x.name));
                }
            }
            returnValue.AddRange(new ExportProcess().GetCourseNames(searchTerm));
            return returnValue.Select(x => x.ToLowerInvariant()).Distinct();
        }

        public ProgramSearchResult SearchData(string searchTerm, string[] formats, string departments, string[] credentials)
        {
            var url = $"{this.googleUrl}/search?q={searchTerm}&output=xml&client=education_program_frontend&site=education_program_collection&entqr=3&rc=1&getfields=title.url.onlineUrl.objectives.onlineObjectives.offcampusObjectives.departmentName&requiredfields=title.url.onlineUrl.objectives.onlineObjectives.offcampusObjectives.departmentName";
            if (!string.IsNullOrEmpty(departments))
            {
                url=url+"&partialfields=department:"+HttpUtility.UrlEncode(departments.Replace("[-]", "|department:"));
            }

            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream==null)
            {
                return new ProgramSearchResult();
            }
            ProgramSearchResult programSearchResult;
            using (var reader = new StreamReader(stream, Encoding.Default))
            {
                programSearchResult=new ProgramSearchResult(reader.ReadToEnd());
            }

            if (credentials.Any()||formats.Any())
            {
                var changedItems = programSearchResult.Programs.ToList();
                if (credentials.Any())
                {
                    foreach (var changedItem in changedItems)
                    {
                        changedItem.Objectives=string.Join(", ", changedItem.Objectives.Split(',').Select(x => x.Trim()).Where(x => credentials.Contains(x, StringComparer.OrdinalIgnoreCase)));
                        changedItem.OnlineObjectives=string.Join(", ", changedItem.OnlineObjectives.Split(',').Select(x => x.Trim()).Where(x => credentials.Contains(x, StringComparer.OrdinalIgnoreCase)));
                        changedItem.OffcampusObjectives=string.Join(", ", changedItem.OffcampusObjectives.Split(',').Select(x => x.Trim()).Where(x => credentials.Contains(x, StringComparer.OrdinalIgnoreCase)));
                    }
                }
                if (formats.Any()&&!formats.Contains("on campus"))
                {
                    changedItems.ForEach(x => x.Objectives=string.Empty);
                }
                if (formats.Any()&&!formats.Contains("online"))
                {
                    changedItems.ForEach(x => x.OnlineObjectives=string.Empty);
                }
                if (formats.Any()&&!formats.Contains("off campus"))
                {
                    changedItems.ForEach(x => x.OffcampusObjectives=string.Empty);
                }
                programSearchResult.Programs=changedItems.Where(x => !string.IsNullOrWhiteSpace(x.Objectives)||!string.IsNullOrWhiteSpace(x.OnlineObjectives)||!string.IsNullOrWhiteSpace(x.OffcampusObjectives));
            }

            return programSearchResult;
        }
    }
}