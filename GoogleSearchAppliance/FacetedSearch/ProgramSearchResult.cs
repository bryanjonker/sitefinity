﻿namespace SitefinityWebApp.GoogleSearchAppliance.FacetedSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public class ProgramSearchResult
    {
        public ProgramSearchResult() {}

        public ProgramSearchResult(string xmlDocument)
        {
            this.Programs = new List<ProgramItem>();
            var document = XDocument.Parse(xmlDocument);
            var spellingNode = document.Descendants("Spelling").FirstOrDefault();
            this.SpellingSuggestion = spellingNode?.Element("Suggestion") == null ? string.Empty : spellingNode.Element("Suggestion").Value;
            if (document.Descendants("M").Any())
            {
                this.TotalResults = Convert.ToInt32(document.Descendants("M").First().Value);
                this.Programs = document.Descendants("R").Select(item => new ProgramItem(
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "title").Attribute("V").Value,
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "onlineUrl").Attribute("V").Value,
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "url").Attribute("V").Value,
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "objectives").Attribute("V").Value,
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "onlineObjectives").Attribute("V").Value,
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "offcampusObjectives").Attribute("V").Value,
                                                                             item.Descendants("MT").Single(i => i.Attribute("N").Value == "departmentName").Attribute("V").Value,
                                                                             item.Descendants("S").Single().Value,
                                                                             item.Descendants("S").Single().Value)
                    ).ToList();
            }
            this.FormatFacet = new string[0];
            this.DepartmentFacet = new string[0];
            this.DegreeFacet = new string[0];
        }

        public string[] DegreeFacet { get; set; }

        public string[] DepartmentFacet { get; set; }

        public string[] FormatFacet { get; set; }

        public IEnumerable<ProgramItem> Programs { get; set; }

        public IEnumerable<Tuple<string, List<ProgramItem>>> SortedProgramItems{
            get
            {
                var returnValue = new Dictionary<string, List<ProgramItem>>();
                foreach (var item in this.Programs.OrderBy(p => p.Department).ThenBy(p => p.Title))
                {
                    if (!string.IsNullOrWhiteSpace(item.OnlineObjectives))
                    {
                        if (!returnValue.ContainsKey(item.OnlineDepartmentName))
                        {
                            returnValue.Add(item.OnlineDepartmentName, new List<ProgramItem>());
                        }
                        returnValue[item.OnlineDepartmentName].Add(item.GetOnlineVersion());
                    }
                    if (!string.IsNullOrWhiteSpace(item.Objectives))
                    {
                        if (!returnValue.ContainsKey(item.OncampusDepartmentName))
                        {
                            returnValue.Add(item.OncampusDepartmentName, new List<ProgramItem>());
                        }
                        returnValue[item.OncampusDepartmentName].Add(item);
                    }
                    if (!string.IsNullOrWhiteSpace(item.OffcampusObjectives))
                    {
                        if (!returnValue.ContainsKey(item.OffcampusDepartmentName))
                        {
                            returnValue.Add(item.OffcampusDepartmentName, new List<ProgramItem>());
                        }
                        returnValue[item.OffcampusDepartmentName].Add(item.GetOffcampusVersion());
                    }
                }
                return returnValue.Select(v => new Tuple<string, List<ProgramItem>>(v.Key, v.Value));
            }
        }

        public string SpellingSuggestion { get; set; }

        public int TotalResults { get; set; }
    }
}