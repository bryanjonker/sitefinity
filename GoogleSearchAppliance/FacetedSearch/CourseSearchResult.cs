﻿namespace SitefinityWebApp.GoogleSearchAppliance.FacetedSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public class CourseSearchResult
    {
        public CourseSearchResult() { }

        public CourseSearchResult(string xmlDocument)
        {
            this.Courses=new List<CourseItem>();
            var document = XDocument.Parse(xmlDocument);
            var spellingNode = document.Descendants("Spelling").FirstOrDefault();
            this.SpellingSuggestion=spellingNode?.Element("Suggestion") == null ? string.Empty : spellingNode.Element("Suggestion").Value;
            if (document.Descendants("M").Any())
            {
                this.TotalResults=Convert.ToInt32(document.Descendants("M").First().Value);
                this.Courses=document.Descendants("R").Select(item => new CourseItem(
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="rubric").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="courseNumber").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="title").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="creditHours").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="faculty").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="formats").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="termsOffered").Attribute("V").Value,
                    item.Descendants("MT").Single(i => i.Attribute("N").Value=="description").Attribute("V").Value,
                    item.Descendants("S").Single().Value)
                ).ToList();
            }
            this.FormatFacet=new string[0];
            this.DepartmentFacet=new string[0];
        }

        public int TotalResults { get; set; }

        public string SpellingSuggestion { get; set; }

        public IEnumerable<CourseItem> Courses { get; set; }

        public string[] FormatFacet { get; set; }

        public string[] DepartmentFacet { get; set; }

        public string[] SubjectFacet { get; set; }
    }
}