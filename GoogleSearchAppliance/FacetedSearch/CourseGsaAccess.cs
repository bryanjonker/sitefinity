﻿namespace SitefinityWebApp.GoogleSearchAppliance.FacetedSearch
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;

    using Newtonsoft.Json;

    using SitefinityWebApp.Mvc.Models.FacultySearch;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch;

    public class CourseGsaAccess
    {
        private readonly string googleUrl;

        public CourseGsaAccess(string googleUrl)
        {
            this.googleUrl = googleUrl.Replace("http://", "https://").Trim('/');
        }

        public IEnumerable<string> GetSuggestions(string searchTerm)
        {
            var returnValue = new List<string>();
            var url = $"{this.googleUrl}/suggest?q={searchTerm}&max=10&format=rich&client=education_course_frontend&site=education_course_collection";
            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream!=null)
            {
                using (var reader = new StreamReader(stream))
                {
                    var returnString = reader.ReadToEnd();
                    var information = JsonConvert.DeserializeObject<Information>(returnString);
                    returnValue.AddRange(information.results.Select(x => x.name));
                }
            }
            returnValue.AddRange(new ExportProcess().GetCourseNames(searchTerm));
            return returnValue.Select(x => x.ToLowerInvariant()).Distinct();
        }


        public CourseSearchResult SearchData(int skip, int size, string searchTerm, string formats, string departments, string rubrics)
        {
            var url = $"{this.googleUrl}/search?q={searchTerm}&start={skip}&num={size}&output=xml&client=education_course_frontend&site=education_course_collection&entqr=3&getfields=rubric.courseNumber.title.creditHours.faculty.formats.termsOffered.description&rc=1&requiredfields=rubric.courseNumber.title.creditHours.faculty.formats.termsOffered.description";
            if (!string.IsNullOrEmpty(formats) && !string.IsNullOrEmpty(departments))
            {
                url = url + "&partialfields=formats:" + HttpUtility.UrlEncode(formats.Replace("[-]", "|formats:")) + "|department:" + HttpUtility.UrlEncode(departments.Replace("[-]", "|department:"));
            }
            else if (!string.IsNullOrEmpty(formats))
            {
                url = url + "&partialfields=formats:" + HttpUtility.UrlEncode(formats.Replace("[-]", "|formats:"));
            }
            else if (!string.IsNullOrEmpty(departments))
            {
                url = url + "&partialfields=department:" + HttpUtility.UrlEncode(departments.Replace("[-]", "|department:"));
            }
            else if (!string.IsNullOrEmpty(rubrics))
            {
                url = url + "&partialfields=rubric:" + HttpUtility.UrlEncode(rubrics.Replace("[-]", "|rubric:"));
            }

            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null)
            {
                return new CourseSearchResult();
            }
            using (var reader = new StreamReader(stream, Encoding.Default))
            {
                return new CourseSearchResult(reader.ReadToEnd());
            }
        }
    }
}