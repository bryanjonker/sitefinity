﻿namespace SitefinityWebApp.GoogleSearchAppliance
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;

    using Newtonsoft.Json;

    using SitefinityWebApp.Mvc.Models.FacultySearch;

    using UIUC.Custom.DAL.DataAccess.Faculty;

    public class GsaAccess
    {
        private readonly string googleUrl;

        public GsaAccess(string googleUrl)
        {
            this.googleUrl = googleUrl.Replace("http://", "https://").Trim('/');
        }
        public IEnumerable<string> GetSuggestions(string searchTerm)
        {
            var returnValue = new List<string>();
            var url = $"{this.googleUrl}/suggest?q={searchTerm}&max=10&format=rich&client=education_faculty_frontend&site=education_faculty_collection";
            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream != null)
            {
                using (var reader = new StreamReader(stream))
                {
                    var returnString = reader.ReadToEnd();
                    var information = JsonConvert.DeserializeObject<Information>(returnString);
                    returnValue.AddRange(information.results.Select(x => x.name));
                }
            }
            returnValue.AddRange(new ExportProcess().SearchFacultyAppointmentsByName(searchTerm).Select(x => x.FullName));
            return returnValue.Select(x => x.ToLowerInvariant()).Distinct();
        }

        public SearchResults SearchData(int skip, int size, string searchTerm, string departments, string centers)
        {
            var url = $"{this.googleUrl}/search?q={searchTerm}&start={skip}&num={size}&output=xml&client=education_faculty_frontend&site=education_faculty_collection&entqr=3&getfields=department.title.image.fullname&rc=1&requiredfields=department.title.image.fullname";
            if (!string.IsNullOrEmpty(departments) && !string.IsNullOrEmpty(centers))
            {
                url = url + "&requiredfields=department:" + HttpUtility.UrlEncode(departments.Replace("[-]", "|department:")) + "|center:" + HttpUtility.UrlEncode(centers.Replace("[-]", "|center:"));
            }
            else if (!string.IsNullOrEmpty(departments))
            {
                url = url + "&requiredfields=department:" + HttpUtility.UrlEncode(departments.Replace("[-]", "|department:"));
            }
            else if (!string.IsNullOrEmpty(centers))
            {
                url = url + "&requiredfields=center:" + HttpUtility.UrlEncode(centers.Replace("[-]", "|center:"));
            }
            if (string.IsNullOrWhiteSpace(searchTerm))
            {
                url = url + "&sort=meta:sort";
            }

            var request = WebRequest.Create(url);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null)
            {
                return new SearchResults();
            }
            using (var reader = new StreamReader(stream, Encoding.Default))
            {
                return new SearchResults(reader.ReadToEnd());
            }
        }
    }
}