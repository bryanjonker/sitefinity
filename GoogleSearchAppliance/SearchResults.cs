﻿namespace SitefinityWebApp.GoogleSearchAppliance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    public class SearchResults
    {
        public List<FacultyItem> Faculty { get; set; }

        public string SpellingSuggestion { get; set; }

        public int TotalResults { get; set; }

        public SearchResults()
        {
            this.Faculty = new List<FacultyItem>();
        }

        public SearchResults(string xmlDocument)
        {
            this.Faculty = new List<FacultyItem>();
            var document = XDocument.Parse(xmlDocument);
            var spellingNode = document.Descendants("Spelling").FirstOrDefault();
            this.SpellingSuggestion = spellingNode == null || spellingNode.Element("Suggestion") == null ? string.Empty : spellingNode.Element("Suggestion").Value;
            if (document.Descendants("M").Any())
            {
                this.TotalResults = Convert.ToInt32(document.Descendants("M").First().Value);
                this.Faculty = document.Descendants("R").Select(item => new FacultyItem
                        {
                            Department = item.Descendants("MT").Single(i => i.Attribute("N").Value == "department").Attribute("V").Value,
                            Image = item.Descendants("MT").Single(i => i.Attribute("N").Value == "image").Attribute("V").Value,
                            Title = item.Descendants("MT").Single(i => i.Attribute("N").Value == "title").Attribute("V").Value,
                            Name = item.Descendants("MT").Single(i => i.Attribute("N").Value == "fullname").Attribute("V").Value,
                            Url = item.Descendants("U").First().Value
                        }).ToList();
            }
        }

        public SearchResults(IEnumerable<Profile> profiles, int totalResults)
        {
            this.TotalResults = totalResults;
            this.SpellingSuggestion = string.Empty;
            this.Faculty = profiles.Select(item => new FacultyItem
                {
                    Department = item.Appointment.Department,
                    Image = item.Image,
                    Name = item.FullName,
                    Title = item.Appointment.Title,
                    Url = "/faculty/" + item.Username
                }).ToList();
        }
    }
}