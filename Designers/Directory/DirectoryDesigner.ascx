<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="height: 600px; width: 900px; overflow: auto; ">
<ol>        
    <li>
        <div style="height: 550px; width: 880px; overflow-y: scroll;">
            <sitefinity:HtmlField ID="content" ClientIDMode="Static" runat="server" DisplayMode="Write" ></sitefinity:HtmlField>
        </div>
    </li>
</ol>
</div>
