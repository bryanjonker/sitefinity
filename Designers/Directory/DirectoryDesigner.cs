using System.Web.UI;

using Case.Framework.Sitefinity.Widgets.EventLink;

[assembly: WebResource(EventLinkWidgetDesigner.scriptReference, "application/x-javascript")]
namespace SitefinityWebApp.Designers.Directory
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    using Telerik.Sitefinity.Modules.Pages;
    using Telerik.Sitefinity.Web.UI;
    using Telerik.Sitefinity.Web.UI.ControlDesign;
    using Telerik.Sitefinity.Web.UI.Fields;

    public class DirectoryDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected virtual HtmlField Content
        {
            get
            {
                return this.Container.GetControl<HtmlField>("content", true);
            }
        }


        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();
            descriptor.AddComponentProperty("content", this.Content.ClientID);
            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences())
                              {
                                  new ScriptReference(
                                      scriptReference,
                                      typeof(DirectoryDesigner)
                                      .Assembly.FullName)
                              };
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/Designers/Directory/DirectoryDesigner.ascx";
        public const string scriptReference = "SitefinityWebApp.Designers.Directory.DirectoryDesigner.js";
        #endregion

        protected override void InitializeControls(GenericContainer container)
        {
        }
    }
}
 
