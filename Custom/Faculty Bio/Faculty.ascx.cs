﻿namespace SitefinityWebApp.Custom
{
    using System;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using LinqToTwitter;

    using Telerik.Sitefinity.Web;

    using UIUC.Custom.DAL.DataAccess.Faculty;
    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    using Media = UIUC.Custom.DAL.DataAccess.Faculty.Model.Media;

    public partial class Faculty : UserControl
    {
        public string ErrorMessage { get; set; }

        public bool SyncLive { get; set; }

        private Profile Profile { get; set; }

        protected void GetPageName()
        {
             var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
             if (parms != null)
             {
                 var lastParam = parms.Last();
                 this.Profile = new ExportProcess().GetFaculty(lastParam);
                 if (this.Profile != null)
                 {
                     this.Page.Title = this.Profile.FullName;
                     this.UserName = this.Profile.Username;
                 }
             }
        }

        protected string UserName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            RouteHelper.SetUrlParametersResolved();
            this.GetPageName();
            if (this.Profile != null)
            {
                this.SetProfilePhoto();
                this.SetInactiveLabel();
                this.SetPrimaryJobInformation();
                this.SetQuote();
                this.SetBios();
                this.SetSocialHyperlinks();
                this.SetTwitterPainel();
                this.SetRepeaters();
            }
            else
            {
                if (!this.Page.IsDesignMode())
                {
                    this.Response.Clear();
                    this.Response.StatusCode = 301;
                    this.Response.Redirect("~/faculty-finder");
                }
            }
        }

        private void SetBios()
        {
            if (!string.IsNullOrEmpty(this.Profile.Biography))
            {
                this.phBio.Visible = true;
                this.ltlBio.Text = CleanData(this.Profile.Biography);
                this.phBioTitle.Visible = true;
            }
            else
            {
                this.phBioTitle.Visible = false;
                this.phBio.Visible = false;
            }

            if (!string.IsNullOrEmpty(this.Profile.Research))
            {
                this.phResearch.Visible = true;
                this.litResearchBiography.Text = CleanData(this.Profile.Research);
            }
            else
            {
                this.phResearch.Visible = false;
            }

            if (!string.IsNullOrEmpty(this.Profile.Teaching))
            {
                this.phTeaching.Visible = true;
                this.litBioTeaching.Text = CleanData(this.Profile.Teaching);
            }
            else
            {
                this.phTeaching.Visible = false;
            }
        }

        private void SetSocialHyperlinks()
        {
            this.hyEmail.Text = this.Profile.Email;
            this.hyEmail.NavigateUrl = string.Concat("mailto:", this.Profile.Email);
            if (!string.IsNullOrEmpty(this.Profile.LinkedIn))
            {

                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, string.Concat("http://www.", this.Profile.LinkedIn.Replace("http://", "").Replace("www.", "")));
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("LinkedIn");
                writer.RenderEndTag(); // A

                this.hyLinkedin.Text = sWriter.ToString();
            }
            else
            {
                this.hyLinkedin.Visible = false;
            }
            if (!string.IsNullOrEmpty(this.Profile.Twitter))
            {
                this.hyTwitter.NavigateUrl = string.Concat("https://twitter.com/", this.Profile.Twitter);
                this.hyTwitter.Target = "_blank";
                this.hyTwitterName.Text = string.Concat("@", this.Profile.Twitter.Replace("@", ""));
                this.hyTwitterName.NavigateUrl = string.Concat("https://twitter.com/", this.Profile.Twitter);
                this.hyFollow.NavigateUrl = string.Concat("https://twitter.com/intent/follow?original_referer=&region=follow_link&screen_name=", this.Profile.Twitter, "&tw_p=followbutton");

            }
            else
            {
                this.hyTwitter.Visible = false;
            }

            this.hyDownloadCv.Visible = false;
        }

        private void SetRepeaters()
        {
            var activities = this.Profile.Activities;

            if (activities != null && activities.Any())
            {
                var honors = activities.Where(x => x.Area.ToLower() == "honor" || x.Area.ToLower() == "position")
                           .OrderBy(x => x.SortOrder.HasValue ? x.SortOrder : 999)
                           .ThenByDescending(x => string.IsNullOrWhiteSpace(x.YearEnded) ? x.YearStarted : x.YearEnded)
                           .ThenByDescending(x => x.YearStarted);

                if (honors.Any())
                {
                    this.rptAwardsHonorsAssociations.ItemDataBound += this.RptAwardsHonorsAssociationsItemDataBound;
                    this.rptAwardsHonorsAssociations.DataSource = honors;
                    this.rptAwardsHonorsAssociations.DataBind();
                }

                var services = activities.Where(x => x.Area.ToLower() == "service")
                           .OrderBy(x => x.SortOrder.HasValue ? x.SortOrder : 999)
                           .ThenByDescending(x => string.IsNullOrWhiteSpace(x.YearEnded) ? x.YearStarted : x.YearEnded)
                           .ThenByDescending(x => x.YearStarted);

                if (services.Any())
                {
                    this.rptServices.ItemDataBound += this.RptServicesItemDataBound;
                    this.rptServices.DataSource = services;
                    this.rptServices.DataBind();
                }

            }

            var grants = this.Profile.Grants;
            if (grants != null && grants.Any())
            {
                this.rptGrant.ItemDataBound += this.RptGrantItemDataBound;
                this.rptGrant.DataSource = grants.OrderByDescending(x => string.IsNullOrWhiteSpace(x.YearEnded) ? "9999" : x.YearEnded).ThenByDescending(x => x.YearStarted);
                this.rptGrant.DataBind();
            }

            var experience = this.Profile.Backgrounds;
            if (experience != null && experience.Any())
            {
                this.rptExperience.ItemDataBound += this.RptExperienceItemDataBound;
                this.rptExperience.DataSource = experience.OrderByDescending(x => string.IsNullOrWhiteSpace(x.YearEnded) ? "9999" : x.YearEnded).ThenByDescending(x => x.YearStarted);
                this.rptExperience.DataBind();
            }

            var lPublications = this.Profile.Publications;
            if (lPublications != null && lPublications.Any())
            {
                this.rptPublications.ItemDataBound += this.RptPublicationsItemDataBound;
                this.rptPublications.DataSource = lPublications.OrderBy(x => x.SortOrder.HasValue ? x.SortOrder : 999).ThenByDescending(x => x.PublishDate).ThenBy(x => x.Name);
                this.rptPublications.DataBind();
            }

            var lLinks = this.Profile.Links;
            if (lLinks != null && lLinks.Any())
            {
                this.rptLinks.ItemDataBound += this.RptLinksItemDataBound;
                this.rptLinks.DataSource = lLinks;
                this.rptLinks.DataBind();
            }

            var lEducational = this.Profile.History;
            if (lEducational != null && lEducational.Any())
            {
                this.rptEducational.ItemDataBound += this.RptEducationalItemDataBound;
                this.rptEducational.DataSource = lEducational.OrderByDescending(x => x.DegreeYear);
                this.rptEducational.DataBind();
            }

            var lCourses = this.Profile.Courses;
            if (lCourses != null && lCourses.Any())
            {
                this.rptCourses.ItemDataBound += this.RptCoursesItemDataBound;
                this.rptCourses.DataSource = lCourses;
                this.rptCourses.DataBind();
            }

            if (this.Profile.Media != null && this.Profile.Media.Any())
            {
                this.rptInTheMedia.ItemDataBound += this.RptMediaItemDataBound;
                this.rptInTheMedia.DataSource = this.Profile.Media.OrderByDescending(x => x.PublicationDate);
                this.rptInTheMedia.DataBind();
            }

            if (this.Profile.NewsItems != null && this.Profile.NewsItems.Any())
            {
                this.rptInOurNews.ItemDataBound += this.RptNewsItemDataBound;
                this.rptInOurNews.DataSource = this.Profile.NewsItems.OrderByDescending(ni => ni.PublicationDate);
                this.rptInOurNews.DataBind();
            }
        }

        private void RptCoursesItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal ltCourse = (Literal)e.Item.FindControl("ltCourse");
                if (ltCourse != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var item = ((Course)e.Item.DataItem);
                    if (item != null)
                    {
                        if (item.IsLinked)
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, $"/course/{item.Rubric}/{item.CourseNumber}");
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write(item.Name);
                            writer.RenderEndTag(); // A
                        }
                        else
                        {
                            writer.Write(item.Name);
                        }
                        writer.Write(string.Concat("&nbsp;",item.Description));
                    }
                    writer.RenderEndTag(); // P
                    ltCourse.Text = sWriter.ToString();
                }
            }
        }

        private void RptServicesItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal ltServices = (Literal)e.Item.FindControl("ltServices");
                if (ltServices != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var item = ((Activity)e.Item.DataItem);
                    if (item != null)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                        if (!string.IsNullOrEmpty(item.Name))
                            writer.Write(item.Name);
                        writer.RenderEndTag(); // STRONG

                        if (!string.IsNullOrEmpty(item.Organization))
                            writer.Write(string.Concat(item.Organization, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(item.YearStarted))
                        {
                            writer.Write(string.Concat(item.YearStarted, "&nbsp;-&nbsp;", string.IsNullOrEmpty(item.YearEnded) ? "present" : item.YearEnded));
                        }
                    }

                    writer.RenderEndTag(); // P
                    ltServices.Text = sWriter.ToString();
                }
            }
        }

        private void RptGrantItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal ltGrant = (Literal)e.Item.FindControl("ltGrant");
                if (ltGrant != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var item = ((Grant)e.Item.DataItem);
                    if (item != null)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                        if (!string.IsNullOrEmpty(item.Role))
                            writer.Write(item.Role);
                        writer.RenderEndTag(); // STRONG

                        if (!string.IsNullOrEmpty(item.Title))
                            writer.Write(string.Concat(item.Title, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(item.Organization))
                            writer.Write(string.Concat(item.Organization, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(item.YearStarted))
                        {
                            writer.Write(string.Concat(item.YearStarted, "&nbsp;-&nbsp;", string.IsNullOrEmpty(item.YearEnded) ? "present" : item.YearEnded));
                        }

                    }

                    writer.RenderEndTag(); // P
                    ltGrant.Text = sWriter.ToString();
                }
            }
        }

        private void RptPublicationsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal ltPubs = (Literal)e.Item.FindControl("ltPubs");
                if (ltPubs != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var item = ((Publication)e.Item.DataItem);
                    if (item != null)
                    {
                        if (!string.IsNullOrEmpty(item.Url))
                        {
                            writer.Write(item.Name);
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, item.Url);
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write("&nbsp;link&nbsp;&gt;");
                            writer.RenderEndTag(); // A
                        }
                        else
                        {
                            writer.Write(item.Name);
                        }
                    }

                    writer.RenderEndTag(); // P
                    ltPubs.Text = sWriter.ToString();
                }
            }
        }

        private void RptLinksItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem !=null)
            {
                Literal ltLinks = (Literal)e.Item.FindControl("ltLinks");
                if (ltLinks != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var item = ((Link)e.Item.DataItem);
                    if (item != null)
                    {
                        if (!string.IsNullOrEmpty(item.Url))
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, item.Url);
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write(item.Name);
                            writer.RenderEndTag(); // A
                        }
                    }

                    writer.RenderEndTag(); // P
                    ltLinks.Text = sWriter.ToString();
                }
            }
        }

        private void RptAwardsHonorsAssociationsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal ltAwards = (Literal)e.Item.FindControl("ltAwards");
                if (ltAwards != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var item = ((Activity)e.Item.DataItem);
                    if (item != null)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                        writer.Write(item.Name);
                        writer.RenderEndTag(); // STRONG
                        if (!string.IsNullOrEmpty(item.Organization))
                        {
                            writer.Write(string.Concat(item.Organization.Replace(", ", ""), ",&nbsp;"));
                        }
                        if (!string.IsNullOrEmpty(item.YearStarted))
                        {
                            writer.Write(string.Concat(item.YearStarted, "&nbsp;-&nbsp;"));
                            writer.Write(string.IsNullOrEmpty(item.YearEnded) || item.YearEnded == "9999" ? "present" : item.YearEnded);
                        }
                    }

                    writer.RenderEndTag(); // P
                    ltAwards.Text = sWriter.ToString();
                }
            }
        }

        private void RptEducationalItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal ltEd = (Literal)e.Item.FindControl("ltEd");
                if (ltEd != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var edItem = ((History)e.Item.DataItem);
                    if (edItem != null)
                    {
                        if (!string.IsNullOrEmpty(edItem.Degree))
                            writer.Write(string.Concat(edItem.Degree, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(edItem.Area))
                            writer.Write(string.Concat(edItem.Area, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(edItem.Institution))
                            writer.Write(edItem.Institution);
                        if (!string.IsNullOrEmpty(edItem.DegreeYear))
                        {
                            writer.Write(string.Concat(",&nbsp;", edItem.DegreeYear));
                        }
                    }
                    writer.RenderEndTag(); // P
                    ltEd.Text = sWriter.ToString();
                }
            }
        }

        private void RptMediaItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                var ltMedia = (Literal)e.Item.FindControl("ltMedia");
                if (ltMedia != null)
                {
                    var media = ((Media)e.Item.DataItem);
                    ltMedia.Text = string.IsNullOrEmpty(media.Url) ?
                        string.Format("<p>{0} on {1}</p>", media.Title, media.PublicationDate.ToShortDateString()) :
                        string.Format("<p><a href='{0}'>{1}</a> on {2}</p>", media.Url, media.Title, media.PublicationDate.ToShortDateString());
                }
            }
        }

        private void RptNewsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                var ltNews = (Literal)e.Item.FindControl("ltNews");
                if (ltNews != null)
                {
                    var news = ((NewsItem)e.Item.DataItem);
                    ltNews.Text = string.Format("<p><a href=\"{0}\">{1}</a> ({2})</p>", news.Url, news.Title, news.PublicationDate.ToShortDateString());
                }
            }
        }

        private void RptExperienceItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Literal lpExp = (Literal)e.Item.FindControl("ltExp");
                if (lpExp != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.RenderBeginTag(HtmlTextWriterTag.P);

                    var experienceItem = ((Background)e.Item.DataItem);
                    if (experienceItem != null)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                        writer.Write(experienceItem.Title);
                        writer.RenderEndTag(); // STRONG
                        if (!string.IsNullOrEmpty(experienceItem.Department))
                            writer.Write(string.Concat(experienceItem.Department, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(experienceItem.Institution))
                            writer.Write(string.Concat(experienceItem.Institution, ",&nbsp;"));
                        if (!string.IsNullOrEmpty(experienceItem.YearStarted))
                        {
                            writer.Write(string.Concat(experienceItem.YearStarted, "&nbsp;-&nbsp;"));
                            writer.Write(string.IsNullOrEmpty(experienceItem.YearEnded) || experienceItem.YearEnded == "9999" ? "present" : experienceItem.YearEnded);
                        }
                    }

                    writer.RenderEndTag(); // P


                    lpExp.Text = sWriter.ToString();


                }

            }
        }

        private void SetQuote()
        {
            if (!string.IsNullOrEmpty(this.Profile.Quote))
            {
                this.phQuote.Visible = true;
                this.ltlQuoteName.Text = this.Profile.FullName;
                this.litQuote.Text = this.Profile.Quote;
            }
            else
            {
                this.phQuote.Visible = false;
            }
        }

        private void SetInactiveLabel()
        {
            this.litInactive.Visible = this.Profile.IsActive; //!profile.GetValue<bool>("isActive");

            if (!this.Profile.IsActive)
            {
                this.litInactive.Text = "Inactive";
            }
        }

        private void SetTwitterPainel()
        {
            var pnTwitterBox = (HtmlGenericControl)this.FindControl("pnlTwitterBox");
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore()
            };
            auth.CredentialStore.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            auth.CredentialStore.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
            auth.CredentialStore.OAuthToken = ConfigurationManager.AppSettings["accessToken"];
            auth.CredentialStore.OAuthTokenSecret = ConfigurationManager.AppSettings["accessTokenSecret"];

            try
            {
                using (var twitterCtx = new TwitterContext(auth))
                {
                    var tweets = twitterCtx.Status.Where(x => x.Type == StatusType.User && x.ScreenName == this.Profile.Twitter).OrderByDescending(x => x.CreatedAt).Take(2);

                    if (tweets.ToList().Any())
                    {
                        this.rptTweets.DataSource = tweets;
                        this.rptTweets.DataBind();
                        pnTwitterBox.Visible = true;
                    }
                    else
                    {
                        pnTwitterBox.Visible = false;
                    }
                }
            }
            catch
            {
                pnTwitterBox.Visible = false;
            }
        }

        private void SetPrimaryJobInformation()
        {
            var jobs = this.Profile.Appointments;

            if (jobs != null && jobs.Any(x => x.IsPrimary))
            {
                var primaryJob = jobs.First(x => x.IsPrimary);

                this.litDepartmentName.Text = primaryJob.Unit;

                this.hyPhone.Text = primaryJob.Phone;
                this.hyPhone.NavigateUrl = string.Concat("tel:", primaryJob.Phone);
                this.ltlAddress.Text = primaryJob.Address1;
                this.ltlCity.Text = string.IsNullOrWhiteSpace(primaryJob.City) ? string.Empty : string.Format("{0}, {1} {2}", primaryJob.City, primaryJob.State, primaryJob.Zip);
                this.ltlBuilding.Text = primaryJob.Building;
                this.ltlRoom.Text = primaryJob.Room;
                this.ltlTitle.Text = primaryJob.Title;
                if (!string.IsNullOrEmpty(primaryJob.Hours))
                {
                    //<p class="faculty-info">Hours</p>

                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "faculty-info");
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write("Hours");
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(primaryJob.Hours);
                    writer.RenderEndTag();
                    this.litHours.Text = sWriter.ToString();
                }

            }
        }

        private void SetProfilePhoto()
        {
            this.imgProfile.ImageUrl = this.Profile.Image;
            this.imgProfile.AlternateText = this.Profile.FullName;
        }

        public string ParseTweet(string rawTweet)
        {
            Regex link = new Regex(@"http(s)?://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&amp;\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?");
            Regex screenName = new Regex(@"@\w+");
            Regex hashTag = new Regex(@"#\w+");

            string formattedTweet = link.Replace(rawTweet, delegate(Match m)
            {
                string val = m.Value;
                return "<a href='" + val + "' target='_blank'>" + val + "</a>";
            });

            formattedTweet = screenName.Replace(formattedTweet, delegate(Match m)
            {
                string val = m.Value.Trim('@');
                return string.Format("<a href='http://twitter.com/{0}' target='_blank'>@{1}</a>", val, val);
            });

            formattedTweet = hashTag.Replace(formattedTweet, delegate(Match m)
            {
                string val = m.Value;
                return string.Format("<a href='http://twitter.com/search?q=%23{0}' target='_blank'>{1}</a>", m.Value.Replace('#', ' ').Trim(), val);
            });

            return formattedTweet;
        }
        public static string FormatMediaDate(string pDate)
        {
            var date = DateTime.Parse(pDate);
            return string.Concat(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(date.Month), " ", date.Day, ", ", date.Year);
        }

        public static string GetTweetTime(DateTime pDate)
        {
            const int Second = 1;
            const int Minute = 60 * Second;
            const int Hour = 60 * Minute;
            const int Day = 24 * Hour;
            const int Month = 30 * Day;

            var ts = new TimeSpan(DateTime.UtcNow.Ticks - pDate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 0)
            {
                return "not yet";
            }
            if (delta < 1 * Minute)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }
            if (delta < 2 * Minute)
            {
                return "a minute ago";
            }
            if (delta < 45 * Minute)
            {
                return ts.Minutes + " minutes ago";
            }
            if (delta < 90 * Minute)
            {
                return "an hour ago";
            }
            if (delta < 24 * Hour)
            {
                return ts.Hours + " hours ago";
            }
            if (delta < 48 * Hour)
            {
                return "yesterday";
            }
            if (delta < 30 * Day)
            {
                return ts.Days + " days ago";
            }
            if (delta < 12 * Month)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }

        private static string CleanData(string s)
        {
            return s.Replace("&lt;i&gt;", "<i>").Replace("&lt;/i&gt;", "</i>")
                .Replace("&lt;b&gt;", "<b>").Replace("&lt;/b&gt;", "</b>")
                .Replace("&lt;u&gt;", "<u>").Replace("&lt;/u&gt;", "</u>");
        }
    }
}