﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Model;
using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;


namespace SitefinityWebApp.Custom.Faculty_Bio
{
    public partial class FacultyBio : System.Web.UI.UserControl
    {
        private Guid _albumnId;


        public Guid FacultyAlbum
        {
            get
            {
                var lib = CaseManagers.Images.GetManager().GetAlbums().FirstOrDefault(x => x.Title == "Default Library");
                if (lib != null)
                    return _albumnId = lib.Id;
                else return _albumnId;
            }
            set
            {
                _albumnId = value;
            }
        }

        public int ImageSize { get; set; }

        protected string getPageName()
        {
            SiteMapNode currentNode = SiteMap.CurrentNode;
            PageSiteNode node = (PageSiteNode)currentNode;
            if (currentNode != null)
            {
                var title = currentNode.Title.Split(' ');
                var faculty = FacultyFinderManager.FacultyProfiles.GetAll().FirstOrDefault(x => x.FName == title[0] && x.LName == title[1]);
                if (faculty != null)
                    return faculty.Username;
            }
            return null;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var userName = getPageName();
            if (userName != null)
            {
                var profile = FacultyFinderManager.FacultyProfiles.GetFaculty().FirstOrDefault(x => x.Username == userName);
                if (profile != null)
                {
                    var lWriter = new StringWriter();
                    var l_writer = new HtmlTextWriter(lWriter);
             
                    // -- build left side of page
                    l_writer.AddAttribute(HtmlTextWriterAttribute.Class, "faculty_bio row");
                    l_writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    // -- Back to results link
                    BuildBackToResultsLink(l_writer);

                    //-- Faculty profile photo
                    BuildProfilePhoto(profile, l_writer);

                    //-- Contact Info
                    BuildContactInfo(profile,l_writer);

                    l_writer.RenderEndTag(); // - /DIV

                    ltLeft.Text = lWriter.ToString();
 
                    // -- build right side of page
                    var tab1Writer = new StringWriter();
                    var tab1 = new HtmlTextWriter(tab1Writer);
               
                    // -- build bio / bio quote
                    BuildBioQuote(tab1,profile);

                    // -- build social box
                    if (!String.IsNullOrEmpty(profile.Twitter))
                        BuildSocialBox(tab1, profile);

                    ltTab1.Text = tab1Writer.ToString(); 
                }
            }
        }

        private void BuildSocialBox(HtmlTextWriter tab1, FacultyModel profile)
        {
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "bg social-box");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
      
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "row");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
            
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "column three-quarter first");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
            tab1.RenderBeginTag(HtmlTextWriterTag.H3);
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "ss-twitter");
            tab1.AddAttribute(HtmlTextWriterAttribute.Href,"http://www.twitter.com/" + profile.Twitter);
            tab1.RenderBeginTag(HtmlTextWriterTag.A);
            tab1.RenderEndTag(); // HRef
            tab1.Write(string.Concat("@", profile.Twitter));
            tab1.RenderEndTag(); //H3
            tab1.RenderEndTag(); //DIV FIRST

            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "column one-quarter last");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
            tab1.AddAttribute(HtmlTextWriterAttribute.Href, string.Concat("https://twitter.com/intent/follow?original_referer=&region=follow_link&screen_name=", profile.Twitter, "&tw_p=followbutton"));
            tab1.RenderBeginTag(HtmlTextWriterTag.A);
            tab1.Write("Follow &rsaquo;");
            tab1.RenderEndTag(); // HRef
            tab1.RenderEndTag(); //DIV LAST

            tab1.RenderEndTag(); //DIV ROW
  
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "row");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);

            // -- Get Tweets
            GetTweets(profile, tab1);

            tab1.RenderEndTag(); //DIV
            tab1.RenderEndTag(); //DIV
  
        }

        private void BuildBioQuote(HtmlTextWriter tab1,FacultyModel profile)
        {
            tab1.RenderBeginTag(HtmlTextWriterTag.H2);
            tab1.Write("Biography");
            tab1.RenderEndTag(); // H2

            tab1.RenderBeginTag(HtmlTextWriterTag.P);
            tab1.RenderBeginTag(HtmlTextWriterTag.Blockquote);
            tab1.Write(profile.BioQuote);
            tab1.RenderBeginTag("footer");
            tab1.Write(profile.Title);
            tab1.RenderEndTag(); // FOOTER
            tab1.RenderEndTag(); // BLOCKQUOTE
            tab1.RenderEndTag(); // P

            tab1.RenderBeginTag(HtmlTextWriterTag.P);
            tab1.Write(profile.Bio);
            tab1.RenderEndTag();
        }


        private void BuildBackToResultsLink(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "/faculty-staff/faculty-finder");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("&lsaquo;&nbsp;&nbsp;Back to Results");
            writer.RenderEndTag();
        }

        private void BuildContactInfo(FacultyModel profile,
            HtmlTextWriter writer)
        {
            
            if (profile.Jobs.Count() >0)
            {
                var primaryJob = profile.Jobs.FirstOrDefault(x => x.PrimaryJob);

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "faculty_bio");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderBeginTag(HtmlTextWriterTag.H6);
                writer.Write("Contact");
                writer.RenderEndTag(); // H6

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "no-bullets");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                if (primaryJob != null)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "tel:" + primaryJob.Phone);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(primaryJob.Phone);
                    writer.RenderEndTag(); // HRef
                    writer.RenderEndTag(); // LI
                }
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "mailto:" + profile.Email);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(profile.Email);
                writer.RenderEndTag(); // HRef
                writer.RenderEndTag(); // LI

                if (!String.IsNullOrEmpty(profile.LinkedIN))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, profile.LinkedIN);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(profile.LinkedIN);
                    writer.RenderEndTag(); // HRef
                    writer.RenderEndTag(); // LI
                }

                if (!String.IsNullOrEmpty(profile.Twitter))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://www.twitter.com/" + profile.Twitter);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(string.Concat("@", profile.Twitter));
                    writer.RenderEndTag(); // HRef
                    writer.RenderEndTag(); // LI
                }
                if (primaryJob != null)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H6);
                    writer.Write("Office");
                    writer.RenderEndTag(); // H6
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(string.Concat(primaryJob.Building, "<br/>"));
                    writer.Write(string.Concat(primaryJob.Address.Street, "<br/>"));
                    writer.Write(string.Concat(primaryJob.Address.City, ", ",primaryJob.Address.StateCode, " ", primaryJob.Address.Zip));
                    writer.RenderEndTag(); // P

                    writer.RenderBeginTag(HtmlTextWriterTag.H6);
                    writer.Write("Hours");
                    writer.RenderEndTag(); // H6
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(string.Concat(primaryJob.Hours, "<br/>"));
                    writer.RenderEndTag(); // P
                }

                writer.RenderEndTag(); // - DIV
            }
        }


        private void BuildProfilePhoto(FacultyModel profile, HtmlTextWriter writer)
        {
           
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "faculty_bio row");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Alt, profile.Title);
            writer.AddAttribute(HtmlTextWriterAttribute.Src, profile.Image);
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag(); // IMG

            if (!string.IsNullOrEmpty(profile.Job) && !string.IsNullOrEmpty(profile.Unit))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                writer.Write(profile.Job + ", ");
                writer.RenderEndTag(); // STRONG

                writer.Write(profile.Unit);
                writer.RenderEndTag(); // H1
            }
            if (!String.IsNullOrEmpty(profile.CV_File_Extention))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "no-bullets");

                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                var cv = string.Concat("/docs/default-source/cv/", profile.Username, profile.CV_File_Extention, "?sfvrsn=2");
                writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, cv);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Download CV");
                writer.RenderEndTag(); // HRef
                writer.RenderEndTag(); // LI
                writer.RenderEndTag(); // UL   

            }
            writer.RenderEndTag(); // DIV
        }

        private void GetTweets(FacultyModel profile,HtmlTextWriter writer)
        {
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = ConfigurationManager.AppSettings["consumerKey"],
                    ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"],
                    AccessToken = ConfigurationManager.AppSettings["accessToken"],
                    AccessTokenSecret = ConfigurationManager.AppSettings["accessTokenSecret"]
                }
            };

            try
            {
                var twitterCtx = new TwitterContext(auth);
                var tweets = twitterCtx.Status.Where(x => x.Type == StatusType.User && x.ScreenName == profile.Twitter).OrderByDescending(x => x.CreatedAt).Take(2);

                if (tweets != null && tweets.ToList().Count > 0)
                {
                    var arr = tweets.ToArray();

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-half first");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute("datetime","");
                    writer.RenderBeginTag("time");
                    writer.Write(GetTweetTime(arr[0].CreatedAt));
                    writer.RenderEndTag(); //time
                  
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(ParseTweet(arr[0].Text));
                    writer.RenderEndTag(); //P
                    writer.RenderEndTag(); //DIV

                    if (arr.Count() > 1)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-half last");
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.AddAttribute("datetime", "");
                        writer.RenderBeginTag("time");
                        writer.Write(GetTweetTime(arr[1].CreatedAt));
                        writer.RenderEndTag();
                        writer.RenderBeginTag(HtmlTextWriterTag.P);
                        writer.Write(ParseTweet(arr[1].Text));
                        writer.RenderEndTag(); //P
                        writer.RenderEndTag(); //DIV 
                    }
                }
               
            }
            catch (Exception)
            {
               
            }
        }

        public string ParseTweet(string rawTweet)
        {
            Regex link = new Regex(@"http(s)?://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&amp;\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?");
            Regex screenName = new Regex(@"@\w+");
            Regex hashTag = new Regex(@"#\w+");

            string formattedTweet = link.Replace(rawTweet, delegate(Match m)
            {
                string val = m.Value;
                return "<a href='" + val + "' target='_blank'>" + val + "</a>";
            });

            formattedTweet = screenName.Replace(formattedTweet, delegate(Match m)
            {
                string val = m.Value.Trim('@');
                return string.Format("<a href='http://twitter.com/{0}' target='_blank'>@{1}</a>", val, val);
            });

            formattedTweet = hashTag.Replace(formattedTweet, delegate(Match m)
            {
                string val = m.Value;
                return string.Format("<a href='http://twitter.com/search?q=%23{0}' target='_blank'>{1}</a>", m.Value.Replace('#', ' ').Trim(), val);
            });

            return formattedTweet;
        }

        public static string GetTweetTime(DateTime pDate)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var ts = new TimeSpan(DateTime.UtcNow.Ticks - pDate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 0)
            {
                return "not yet";
            }
            if (delta < 1 * MINUTE)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }
            if (delta < 2 * MINUTE)
            {
                return "a minute ago";
            }
            if (delta < 45 * MINUTE)
            {
                return ts.Minutes + " minutes ago";
            }
            if (delta < 90 * MINUTE)
            {
                return "an hour ago";
            }
            if (delta < 24 * HOUR)
            {
                return ts.Hours + " hours ago";
            }
            if (delta < 48 * HOUR)
            {
                return "yesterday";
            }
            if (delta < 30 * DAY)
            {
                return ts.Days + " days ago";
            }
            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }
    }
}