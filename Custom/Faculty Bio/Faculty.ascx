﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Faculty.ascx.cs" Inherits="SitefinityWebApp.Custom.Faculty" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>

<sitefinity:FormManager ID="formManager" runat="server" />

<asp:Literal runat="server" ID="litErrorMessage" Visible="false">
    
</asp:Literal>

<asp:Panel runat="server" ID="pnContent">
    <div class="row">
        <div class="column three-quarter right last">
            <section class="content">
                <!-- TABS -->
                <div class="tabs row">

                    <ul class="tab-links">
                        <li class="active"><a href="#tab1">Profile</a></li>
                        <li><a href="#tab2">Research &amp; Service</a></li>
                        <li><a href="#tab3">Teaching</a></li>
                    </ul>

                    <!-- PROFILE -->
                    <div class="tab-content">
                        <div id="tab1" class="tab active">
                            <div class="tabs-container">
                            
                             <asp:PlaceHolder runat="server" id="phBioTitle">
                               <div class="tabs-title">Biography</div>
                            </asp:PlaceHolder>
                             <asp:PlaceHolder runat="server" ID="phQuote">
                               <p>
                                 <blockquote>
                                    <asp:Literal ID="litQuote" runat="server"></asp:Literal>
                                    <footer>
                                        <asp:Literal ID="ltlQuoteName" runat="server"></asp:Literal>
                                    </footer>

                                </blockquote>
                               </p>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="phBio">
                                <p>
                                    <asp:Literal ID="ltlBio" runat="server"></asp:Literal>
                                </p>
                            </asp:PlaceHolder>

                           </div>

                            <!-- SOCIAL BOX -->

                              <div class="bg social-box" id="pnlTwitterBox" runat="server">
                                <div class="row">
                                    <div class="column three-quarter first">
                                      <div class="social-box-title">
                                            <asp:HyperLink runat="server" ID="hyTwitterName" />
                                        </div>
                                    </div>

                                    <div class="column one-quarter last">
                                        <asp:HyperLink ID="hyFollow" runat="server" Text="Follow ›" />
                                    </div>
                                </div>

                                <asp:Repeater runat="server" ID="rptTweets">
                                    <HeaderTemplate>
                                        <div class="row">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="column one-half">
                                            <time datetime=""><%# GetTweetTime(DateTime.Parse(Eval("CreatedAt").ToString())) %></time>
                                            <p><%# ParseTweet(Eval("Text").ToString()) %> </p>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                               </div>

                            <asp:Repeater runat="server" ID="rptExperience">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 1"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Key Professional Appointments</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <asp:Literal ID="ltExp" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Repeater runat="server" ID="rptEducational">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 2"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Education</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltEd" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Repeater runat="server" ID="rptAwardsHonorsAssociations">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 3"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Awards, Honors, Associations</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltAwards" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Repeater runat="server" ID="rptInTheMedia">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand Media"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">In The Media</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltMedia" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>


                            <asp:Repeater runat="server" ID="rptInOurNews">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 4"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">In Our News</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltNews" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>



                            <asp:Repeater runat="server" ID="rptLinks">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 5"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Links</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                      <asp:Literal ID="ltLinks" runat="server"></asp:Literal>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>

                        <!-- RESEARCH & SERVICE -->
                        <div id="tab2" class="tab">
                            <asp:PlaceHolder runat="server" ID="phResearch">
                            <div class="tabs-title">Research &amp; Service</div>
                            <p>
                                <asp:Literal ID="litResearchBiography" runat="server"></asp:Literal>
                            </p>
                            </asp:PlaceHolder>

                            <asp:Repeater runat="server" ID="rptPublications">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 1"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Publications</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltPubs" runat="server"></asp:Literal>    
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                             <asp:Repeater runat="server" ID="rptGrant">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 2"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Grants</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltGrant" runat="server"></asp:Literal>    
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                         </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                             <asp:Repeater runat="server" ID="rptServices">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 3"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Service</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltServices" runat="server"></asp:Literal>    
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                         </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>

                        <!-- TEACHING -->
                        <div id="tab3" class="tab">
                            <asp:PlaceHolder runat="server" ID="phTeaching">
                            <div class="tabs-title">Teaching</div>
                            <p>
                                <asp:Literal ID="litBioTeaching" runat="server" />
                            </p>
                            </asp:PlaceHolder>

                            <asp:Repeater runat="server" ID="rptCourses">
                                <HeaderTemplate>
                                    <div class="expand">
                                        <span role="button" class="expand-this" aria-label="Expand 1"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
                                        <div class="tabs-title">Courses</div>
                                        <section>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="ltCourse" runat="server" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    </section>
            				</div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                    </div>
                </div>

            </section>
        </div>
        <!-- FACULTY BIO -->
        <div class="column one-quarter left first">
            <div class="faculty_bio row">
                
                <a href="#" onclick="window.history.go(-1); return false;" class="button orange">&lsaquo;&nbsp;&nbsp;Back to Results</a>
            </div>

            <div class="faculty_bio row">
                <asp:Image ID="imgProfile" runat="server" />
                <br />
                <asp:Literal runat="server" ID="litInactive"></asp:Literal>
                <div class="responsive_toFloat">
                    <p class="faculty-position"><strong>
                        <asp:Literal ID="ltlTitle" runat="server" /></strong>,
                        <asp:Literal ID="litDepartmentName" runat="server" /></p>

                    <ul class="no-bullets">
                       <li><asp:HyperLink ID="hyDownloadCv" runat="server" Text="Download CV" /></li>
                    </ul>

                    <span class="mobile_toMove">
                        <p class="faculty-info">Contact</p>
                        <ul class="no-bullets">
                            <li>
                                <asp:HyperLink ID="hyPhone" runat="server"></asp:HyperLink></li>
                            <li>
                                <asp:HyperLink ID="hyEmail" runat="server"></asp:HyperLink>
                            </li>
                            <li>
                              
                                <asp:Literal ID="hyLinkedin" runat="server"></asp:Literal></li>
                            <li>
                                <asp:HyperLink ID="hyTwitter" runat="server">Twitter</asp:HyperLink></li>
                        </ul>

                        <p class="faculty-info">Office</p>
                        <p>
                            <asp:Literal ID="ltlRoom" runat="server"></asp:Literal>
                            <asp:Literal ID="ltlBuilding" runat="server"></asp:Literal><br>
                            <asp:Literal ID="ltlAddress" runat="server"></asp:Literal><br>
                            <asp:Literal ID="ltlCity" runat="server"></asp:Literal> 
                        </p>

                      
                            <asp:Literal ID="litHours" runat="server" />
                        <p class="faculty-edit-icon"><a href="https://www.digitalmeasures.com/login/illinois/faculty/" target="_blank"><img src="/Theme/img/icons/faculty_edit_pencil.png" alt="Edit"/></a>
                            <a href="#" class="faculty-refresh"><img src="/Theme/img/icons/faculty_refresh_icon.png" alt="Refresh"/></a><p class="faculty-refresh-message">Refreshing data...please wait</p>
                        </p>
                    </span>
                </div>
            </div>
        </div>
        <div class="faculty-refresh-popup" style="display: none; width: 100%; height: 100%; background-color: #333; z-index: 999; top: 0; left: 0; position: fixed;"></div>
</asp:Panel>

