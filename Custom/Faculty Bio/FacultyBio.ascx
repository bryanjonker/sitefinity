﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacultyBio.ascx.cs" Inherits="SitefinityWebApp.Custom.Faculty_Bio.FacultyBio" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>

     <div class="row">
    	<div class="column three-quarter right last">
            <section class="content">

            	<!-- TABS -->
            	<div class="tabs row">

            		<ul class="tab-links">
       					<li class="active"><a href="#tab1">Profile</a></li>
        				<li><a href="#tab2">Research &amp; Service</a></li>
        				<li><a href="#tab3">Teaching</a></li>
    				</ul>
 					
 					<!-- PROFILE -->
				    <div class="tab-content">
				        <div id="tab1" class="tab active">
                           <asp:Literal runat="server" ID="ltTab1"></asp:Literal>

<%--				            <h2>Biography</h2>
				            <p>
				            	<blockquote>What I value most about my students is dolor consectetur morbi sit adipiscing elit ut enim minim veniam.
				            		<footer>Cris Mayo</footer>
				            	</blockquote>
				            </p>
				            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam nibh. Nunc varius facilisis eros. Sed erat. In in velit quis arcu ornare laoreet. Curabitur adipiscing luctus massa. Integer ut purus ac augue commodo commodo. Nunc nec mi eu justo tempor consectetuer. Etiam vitae nisl.</p>
				            <p>Donec nonummy, enim in lacinia pulvinar, velit tellus scelerisque augue, ac posuere libero urna eget neque.</p>
				            <p>Nunc at velit quis lectus nonummy eleifend. Curabitur eros. Aenean ligula dolor, gravida auctor, auctor et, suscipit in, erat. Sed malesuada, enim ut congue pharetra, massa elit convallis pede, ornare scelerisque libero neque ut neque. In at libero. Curabitur molestie. Sed vel neque. Proin et dolor ac ipsum elementum malesuada. Praesent id orci.</p>

				            <!-- SOCIAL BOX -->
				            <div class="bg social-box">
				            	<div class="row">
				            		<div class="column three-quarter first">
					            		<h3><a class="ss-twitter" href="#" name="twitter"></a>@profCMayo</h3>
					            	</div>
				            		
				            		<div class="column one-quarter last">
	                					<a class="button orange" href="#">Follow &rsaquo;</a>
	                				</div>
                				</div>

                				<div class="row">
                					<div class="column one-half first">
                						<time datetime="">2 minutes ago</time>
                						<p>Nulla euismod, eros ac gravida, justo risus <a href="#">@semper dui</a>, id consectetur nunc magna at erat. <a href="#">http://t.co/GRBkus</a></p>
                					</div>

                					<div class="column one-half last">
                						<time datetime="">10 minutes ago</time>
                						<p>Vivatmus mollis <a href="#">@sagittis</a> adipiscing. Phasellus purus liberonunc magna at erat. <a href="#">http://t.co/ABCdef</a></p>
                					</div>
                				</div>
            				</div>
    --%>
            				<!-- EXPAND -->
            				<div class="expand">
            					<span role="button" class="expand-this"><img src="img/icons/icon-expand.svg" onerror="this.src=img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
            					<h2>Key Professional Appointments</h2>
<%--            					<section>
            						<p><strong>Curabitur ut nunc</strong>
            							Nullam porta urna quis mauris. Aliquam erat volutpat. Donec scelerisque quam vitae est. Aenean vitae diam at erat pellentesque condimentum. Duis pulvinar nisl sed orci. Vivamus turpis nisi, volutpat in, placerat et, pharetra nec, eros. Suspendisse tellus metus, sodales non, venenatis a, ultrices auctor, erat. In ut leo nec elit mattis pellentesque. Sed eros elit, cursus accumsan, sollicitudin a, iaculis quis, diam. Pellentesque fermentum, pede a nonummy varius, ligula velit laoreet erat, et lacinia nibh nulla sit amet nunc. Suspendisse at turpis quis augue pellentesque pretium. Nunc condimentum elit semper felis.
            						</p>
            					</section>--%>
            				</div>
            				<div class="expand">
            					<span role="button" class="expand-this"><img src="img/icons/icon-expand.svg" onerror="this.src=img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
            					<h2>Expertise</h2>
<%--            					<section>
            						<p><strong>Lorem ipsum dolor</strong>
            							Nulla mauris odio, vehicula in, condimentum sit amet, tempus id, metus.
            						</p>
            						<p>Donec at nisi sit amet felis blandit posuere. Aliquam erat volutpat. Cras lobortis orci in quam porttitor cursus.
            						</p>
            						<p>
            							<ul>
            								<li>Aenean dignissim. Curabitur facilisis sem at nisi laoreet placerat.</li>
            								<li>Duis sed ipsum ac nibh mattis feugiat.</li>
            								<li>Proin sed purus. Vivamus lectus ipsum, rhoncus sed, scelerisque sit amet, ultrices in, dolor.</li>
            							</ul>
            						</p>
            						<p>Aliquam vel magna non nunc ornare bibendum.</p>
            						<p><strong>Keywords</strong>
            							<a href="#" class="text_orange">Lorem ipsum</a>, 
            							<a href="#" class="text_orange">Dolor sit amet</a>, 
            							<a href="#" class="text_orange">Consectetur</a>, 
            							<a href="#" class="text_orange">Adipiscing elit</a>, 
            							<a href="#" class="text_orange">Vivamus molestij</a>, 
            							<a href="#" class="text_orange">Urna et laoreet luctus</a>, 
            							<a href="#" class="text_orange">Ut sollicitudin</a>, 
            							<a href="#" class="text_orange">Sollicitudin suscipit</a>, 
            							<a href="#" class="text_orange">Cum socus natoque</a>, 
            							<a href="#" class="text_orange">Penatibus et magnis</a>, 
            							<a href="#" class="text_orange">Dis parturient montes</a>, 
            						</p>
            					</section>--%>
            				</div>
            				<div class="expand">
            					<span role="button" class="expand-this"><img src="img/icons/icon-expand.svg" onerror="this.src=img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
            					<h2>Awards, Honors, Associations</h2>
<%--            					<section>
            						<p><strong>Curabitur ut nunc</strong>
            							Nullam porta urna quis mauris. Aliquam erat volutpat. Donec scelerisque quam vitae est. Aenean vitae diam at erat pellentesque condimentum. Duis pulvinar nisl sed orci. Vivamus turpis nisi, volutpat in, placerat et, pharetra nec, eros. Suspendisse tellus metus, sodales non, venenatis a, ultrices auctor, erat. In ut leo nec elit mattis pellentesque. Sed eros elit, cursus accumsan, sollicitudin a, iaculis quis, diam. Pellentesque fermentum, pede a nonummy varius, ligula velit laoreet erat, et lacinia nibh nulla sit amet nunc. Suspendisse at turpis quis augue pellentesque pretium. Nunc condimentum elit semper felis.
            						</p>
            					</section>--%>
            				</div>
                            <div class="expand">
            					<span role="button" class="expand-this"><img src="img/icons/icon-expand.svg" onerror="this.src=img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
            					<h2>In Our News</h2>
            					<section>
            						<p><strong>Maecenas congue</strong>
            							Fusce auctor, metus eu ultricies vulputate, sapien nibh faucibus ligula, eget sollicitudin augue risus et dolor. Aenean pellentesque, tortor in cursus mattis, ante diam malesuada ligula, ac vestibulum neque turpis ut enim. Cras ornare. Proin ac nisi. Praesent laoreet ante tempor urna. In imperdiet. Nam ut metus et orci fermentum nonummy. Cras vel nunc. Donec feugiat neque eget purus. Quisque rhoncus. Phasellus tempus massa aliquet urna. Integer fringilla quam eget dolor. Curabitur mattis. Aliquam ac lacus. In congue, odio ut tristique adipiscing, diam leo fermentum ipsum, nec sollicitudin dui quam et tortor. Proin id neque ac pede egestas lacinia. Curabitur non odio.
            						</p>
            					</section>
            				</div>
				        </div>
				 		
				 		<!-- RESEARCH & SERVICE -->
				        <div id="tab2" class="tab">
                            <asp:Literal runat="server" ID="ltTab2"></asp:Literal>

<%--				        <h2>Research &amp; Service</h2>
				            <p>Donec pulvinar neque sed semper lacinia. Curabitur lacinia ullamcorper nibh; quis imperdiet velit eleifend ac. Donec blandit mauris eget aliquet lacinia! Donec pulvinar massa interdum risus ornare mollis. In hac habitasse platea dictumst. Ut euismod tempus hendrerit. Morbi ut adipiscing nisi. Etiam rutrum sodales gravida! Aliquam tellus orci, iaculis vel.</p>--%>
				        </div>
				 		
				 		<!-- TEACHING -->
				        <div id="tab3" class="tab">
                             <asp:Literal runat="server" ID="ltTab3"></asp:Literal>

<%--				         <h2>Teaching</h2>
				            <p>Donec pulvinar neque sed semper lacinia. Curabitur lacinia ullamcorper nibh; quis imperdiet velit eleifend ac. Donec blandit mauris eget aliquet lacinia! Donec pulvinar massa interdum ri.</p>--%>
				        </div>
				    </div>
            	</div>
            </section>
        </div>

        <!-- FACULTY BIO -->
        <div class="column one-quarter left first">
            <asp:Literal runat="server" ID="ltLeft"></asp:Literal>
        </div>
</div>