﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Sitefinity.RelatedData;

namespace SitefinityWebApp.Custom.Code
{
    public class NewsWidget : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var repeater = (RadListView)this.FindControl("NewsList");
            repeater.ItemDataBound += new EventHandler<RadListViewItemEventArgs>(repeater_ItemDataBound);
        }

        private void repeater_ItemDataBound(object sender, RadListViewItemEventArgs e)
        {
            if (e.Item != null)
            {
                var ltImage = (Literal)e.Item.FindControl("ltImage");
                if (ltImage != null)
                {
                    var sWriter = new StringWriter();
                    var writer = new HtmlTextWriter(sWriter);

                    var dataItem = ((RadListViewDataItem)e.Item).DataItem;

                    var relatedItem = dataItem.GetRelatedItems("Image");

                    if (relatedItem.Count() > 0)
                    {
                        var image = ((Telerik.Sitefinity.Libraries.Model.Image)((relatedItem)).FirstOrDefault());
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, image.Url + "&size=350");
                        writer.AddAttribute(HtmlTextWriterAttribute.Alt, image.Title);
                        writer.AddAttribute(HtmlTextWriterAttribute.Title, image.Title);
                        writer.RenderBeginTag(HtmlTextWriterTag.Img);
                        writer.RenderEndTag(); // img
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/content/news.jpg");
                        writer.RenderBeginTag(HtmlTextWriterTag.Img);
                        writer.RenderEndTag(); // img

                    }
                    ltImage.Text = sWriter.ToString();
                }

            }


           // throw new NotImplementedException();
        }
    }
}