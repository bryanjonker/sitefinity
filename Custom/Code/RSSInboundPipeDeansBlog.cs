﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using Telerik.Sitefinity.Publishing;
using Telerik.Sitefinity.Publishing.Pipes;

namespace SitefinityWebApp.Custom.Code
{
    /// <summary>
    /// Inbound pipe for FRCC Feedburner RSS feed. Adds relevant properties to the wrapperobject, including
    /// two custom properties, canonicalurl and author
    /// </summary>
    public class RSSInboundPipeDeansBlog : RSSInboundPipe
    {
        private const string SOURCE_FEED_NAME = "Notes from the Dean";

        public override WrapperObject ConvertToWraperObject( SyndicationItem feedItem )
        {
            base.ConvertToWraperObject( feedItem );

            WrapperObject obj = new WrapperObject( null );
            obj.MappingSettings = this.PipeSettings.Mappings;
            obj.Language = this.PipeSettings.LanguageIds.FirstOrDefault();

            if (feedItem.SourceFeed.Title.Text == SOURCE_FEED_NAME)
            {
                // is dean's blog feed, populate wrapper
                obj.AddProperty( PublishingConstants.FieldTitle, feedItem.Title.Text );
                obj.AddProperty( PublishingConstants.FieldPublicationDate, feedItem.PublishDate.UtcDateTime );
                obj.AddProperty( PublishingConstants.FieldContent, feedItem.Summary.Text );

                if (feedItem.Links.Count > 0)
                {
                    obj.AddProperty( PublishingConstants.FieldLink, feedItem.Links.ElementAt( 0 ).Uri );
                }

                obj.AddProperty( PublishingConstants.FieldItemHash, GenerateItemHash( feedItem, this.RssPipeSettings.UrlName ) );
            }
            return obj;
        }
    }

}