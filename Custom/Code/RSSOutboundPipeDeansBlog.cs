﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Blogs.Model;
using Telerik.Sitefinity.Publishing;
using Telerik.Sitefinity.Publishing.Pipes;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using System.Text.RegularExpressions;

namespace SitefinityWebApp.Custom.Code
{
    /// <summary>
    /// Works with RSSInboundPipeDeansBlog class. Takes properties set in that pipe and
    /// sets value for blog posts
    /// </summary>
    public class RSSOutboundPipeDeansBlog : ContentOutboundPipe
    {
        // must match name of custom posts field in sf
        public const string OriginalPostUrlFieldName = "CanonicalLink";

        protected override void SetPropertiesThroughPropertyDescriptor( Telerik.Sitefinity.Model.IContent item, Telerik.Sitefinity.Publishing.WrapperObject wrapperObj )
        {
            base.SetPropertiesThroughPropertyDescriptor( item, wrapperObj );
            
            try
            {
                if (item is BlogPost)
                {
                    //get all properties
                    var myProperties = TypeDescriptor.GetProperties( wrapperObj );
                    // contains all the customizations
                    WrapperObject origObject = (WrapperObject)wrapperObj.WrappedObject;
                    var blogPost = item as BlogPost;

                    //get the Link property
                    var linkProperty = myProperties["Link"];
                    var origURL = linkProperty.GetValue( wrapperObj );
                    DataExtensions.SetValue( blogPost, OriginalPostUrlFieldName, origURL.ToString() );
                    blogPost.Title = origObject.AdditionalProperties[ "Title" ].ToString();
                    blogPost.Content = origObject.AdditionalProperties["Content"].ToString();
                    blogPost.PublicationDate = DateTime.Parse( origObject.AdditionalProperties["PublicationDate"].ToString() );
                    
                }
            }
            catch (Exception)
            {
                //some error handling
                throw;
            }
        }

    }
}