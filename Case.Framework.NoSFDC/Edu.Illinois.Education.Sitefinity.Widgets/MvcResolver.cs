﻿namespace Edu.Illinois.Education.Sitefinity.Widgets
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Web.Caching;

    using Telerik.Sitefinity.Abstractions.VirtualPath;

    public class MvcResolver : IVirtualFileResolver 
    {
        public string GetResourceName(PathDefinition path, string virtualPath)
        {
            return virtualPath;
        }

        public Assembly GetAssembly(PathDefinition path)
        {
            return Assembly.Load(path.ResolverName);
        }

        public bool Exists(PathDefinition definition, string virtualPath)
        {
            Assembly assembly = this.GetAssembly(definition);
            string resourceName = this.GetResourceName(definition, virtualPath);
            return assembly.GetManifestResourceNames().Any(s => s.ToLower() == resourceName.ToLower());
        }

        public Stream Open(PathDefinition definition, string virtualPaht)
        {
            Assembly assembly = this.GetAssembly(definition);
            string resourceName = this.GetResourceName(definition, virtualPaht);
            var resource = assembly.GetManifestResourceNames().First(s => s.ToLower() == resourceName.ToLower());
            return assembly.GetManifestResourceStream(resource);
        }

        public CacheDependency GetCacheDependency(
            PathDefinition definition,
            string virtualPath,
            IEnumerable virtualPathDependencies,
            DateTime utcStart)
        {
            throw new NotImplementedException();
        }
    }
}
