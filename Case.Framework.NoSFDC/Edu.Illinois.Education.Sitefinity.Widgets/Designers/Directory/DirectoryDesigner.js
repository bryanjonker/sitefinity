Type.registerNamespace("SitefinityWebApp.Designers.Directory");

SitefinityWebApp.Designers.Directory.DirectoryDesigner = function (element) {
    this._content = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.Designers.Directory.DirectoryDesigner.initializeBase(this, [element]);
}

SitefinityWebApp.Designers.Directory.DirectoryDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.Designers.Directory.DirectoryDesigner.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.Designers.Directory.DirectoryDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */
    findElement: function (id) {
        var result = $(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */
        $(this.get_content().set_value(controlData.Content));
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();
        var value = this.get_content().get_value();
        controlData.Settings.Content = value;
    },

    get_content: function () {
        return this._content;
    },
    set_content: function (value) {
        this._content = value;
    }

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

}

SitefinityWebApp.Designers.Directory.DirectoryDesigner.registerClass('SitefinityWebApp.Designers.Directory.DirectoryDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
