﻿namespace Edu.Illinois.Education.Sitefinity.Widgets.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Linq;
    using System.Web.Mvc;

    using Edu.Illinois.Education.Sitefinity.Widgets.Designers.Directory;
    using Edu.Illinois.Education.Sitefinity.Widgets.Mvc.Models.Person;

    using Telerik.Sitefinity.Mvc;
    using Telerik.Sitefinity.Web.UI.ControlDesign;

    [ControllerToolboxItem(Name = "Directory Widget", Title = "Directory Widget", SectionName = "Directory")]
    [ControlDesigner(typeof(DirectoryDesigner))]
    public class PersonController : Controller
    {
        private readonly string imagePath = "/Theme/images/portraits/";

        [Category("General")]
        [Description("Insert area information here")]
        [DisplayName("Content")]
        public string Content { get; set; }

        [Category("General")]
        [Description("Insert the department code here")]
        [DisplayName("Department Code")]
        public string DepartmentCode { get; set; }

        private string Netid { get; set; }

        private IEnumerable<PersonViewModel> TempPersonViewModels { get; set; }

        public ActionResult Index()
        {
            var conn = ConfigurationManager.ConnectionStrings["SitefinityDirect"].ConnectionString;
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var personManager = new PersonManager(this.imagePath, mapPath, conn);
            return this.View(string.IsNullOrWhiteSpace(this.DepartmentCode) ? "Index" : "IndexSingle", new IndexAlphabeticalModel
            {
                People = personManager.GetPeople(this.DepartmentCode),
                UsedLetters = personManager.GetFirstLetters(),
                HtmlContent = this.Content 
            });
        }

        public new ActionResult Profile()
        {
            var conn = ConfigurationManager.ConnectionStrings["SitefinityDirect"].ConnectionString;
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var personManager = new PersonManager(this.imagePath, mapPath, conn);
            var profile = personManager.GetSinglePerson(this.Netid);
            if (profile.IsFaculty)
            {
                this.Response.RedirectPermanent("~/faculty/" + this.Netid);
            }
            var viewName = string.IsNullOrWhiteSpace(profile.Username) ? "ProfileNotFound" : "Profile";
            return this.View(viewName, profile);
        }

        public ActionResult Department()
        {
            if (this.Request.Url == null)
            {
                return null;
            }
            var conn = ConfigurationManager.ConnectionStrings["SitefinityDirect"].ConnectionString;
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var personManager = new PersonManager(this.imagePath, mapPath, conn);
            return this.View("IndexAlphabetical", new IndexAlphabeticalModel
            {
                People = this.TempPersonViewModels,
                CurrentLetter = ' ',
                SearchValue = string.Empty,
                CurrentUrl = this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.LastIndexOf(@"/")),
                UsedLetters = personManager.GetFirstLetters()
            });
        }

        public ActionResult Search(string q)
        {
            if (this.Request.Url == null)
            {
                return null;
            }
            var conn = ConfigurationManager.ConnectionStrings["SitefinityDirect"].ConnectionString;
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var personManager = new PersonManager(this.imagePath, mapPath, conn);
            return this.View("IndexAlphabetical", new IndexAlphabeticalModel
            {
                People = personManager.Search(q),
                CurrentLetter = ' ',
                SearchValue = q,
                CurrentUrl = this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.IndexOf("/search", StringComparison.OrdinalIgnoreCase)),
                UsedLetters = personManager.GetFirstLetters()
            });
        }

        public ActionResult SingleLetter()
        {
            if (this.Request.Url == null)
            {
                return null;
            }
            var conn = ConfigurationManager.ConnectionStrings["SitefinityDirect"].ConnectionString;
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var personManager = new PersonManager(this.imagePath, mapPath, conn);
            return this.View("IndexAlphabetical", new IndexAlphabeticalModel
            {
                People = personManager.GetPeopleBySingleLetter(this.Netid.First()),
                CurrentLetter = this.Netid.First(),
                SearchValue = string.Empty,
                CurrentUrl = this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.Length - 2),
                UsedLetters = personManager.GetFirstLetters()
            });
        }

        protected override void HandleUnknownAction(string actionName)
        {
            // HandleUnknownAction is used to set up the widget to handle a default path for the page -- we store the path information and feed it to the action.
            this.Netid = actionName;
            if (actionName.Length == 1)
            {
                this.ActionInvoker.InvokeAction(this.ControllerContext, "SingleLetter");
            }
            var conn = ConfigurationManager.ConnectionStrings["SitefinityDirect"].ConnectionString;
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var personManager = new PersonManager(this.imagePath, mapPath, conn);
            this.TempPersonViewModels = personManager.GetPeopleByDepartment(this.Netid);
            if (this.TempPersonViewModels.Any())
            {
                this.ActionInvoker.InvokeAction(this.ControllerContext, "Department");
            }
            else
            {
                this.ActionInvoker.InvokeAction(this.ControllerContext, "Profile");
            }
        }
    }
}