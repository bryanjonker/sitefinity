﻿namespace Edu.Illinois.Education.Sitefinity.Widgets.Mvc.Models.Person
{
    using System.Collections.Generic;

    public interface IPersonManager
    {
        IEnumerable<PersonViewModel> GetPeople(string department);

        ProfileViewModel GetSinglePerson(string netid);

        IEnumerable<PersonViewModel> GetPeopleBySingleLetter(char startingLetter);

        FullProfileViewModel GetFullProfile(string username);

        IEnumerable<PersonViewModel> Search(string searchCriteria);
        
        IEnumerable<char> GetFirstLetters();
    }
}