﻿namespace Edu.Illinois.Education.Sitefinity.Widgets.Mvc.Models.Person
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;

    using Dapper;

    public class PersonManager : IPersonManager
    {
        private readonly string imagePath;
        private readonly string mapPath;
        private readonly string connectionString;

        public PersonManager(string imagePath, string mapPath, string connectionString)
        {
            this.imagePath = imagePath;
            this.mapPath = mapPath;
            this.connectionString = connectionString;
        }

        public IEnumerable<PersonViewModel> GetPeopleByDepartment(string department)
        {
            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = "SELECT DISTINCT [unit_type],[unit_name],[unit_id],[ed_units_id],[parent_ed_units_id] FROM [dbo].[unit_unit]; SELECT DISTINCT [username],[photo],[lname],[linkedin],[isActive],[fname],[email],[Title] FROM [dbo].[faculty_profile]; SELECT DISTINCT [username],[room],[public_phone],[office_fax],[job_title],[hours],[hierarchy],[ed_units_id],[building],[Title] FROM [dbo].[faculty_professionalappointment]";
                using (var rows = conn.QueryMultiple(sqlCommand))
                {
                    var units = rows.Read();
                    if (!units.Any(x => x.unit_id.ToString().Equals(department, StringComparison.OrdinalIgnoreCase)))
                    {
                        return new List<PersonViewModel>();
                    }
                    var unitDictionary = units.ToDictionary<dynamic, string, string>(unit => unit.ed_units_id.ToString(), unit => unit.unit_name.ToString());
                    var returnValue = rows.Read().Join(rows.Read(), p => p.username.ToString(), j => j.username.ToString(), (p, j) => new { People = p, Job = j })
                        .Select(l => new PersonViewModel
                        {
                            Email = GetNullableValue(l.People.email),
                            FirstName = GetNullableValue(l.People.fname),
                            LastName = GetNullableValue(l.People.lname),
                            Username = GetNullableValue(l.People.username),
                            Title = GetNullableValue(l.Job.job_title),
                            Hierarchy = GetNullableValue(l.Job.hierarchy),
                            Unit = PersonViewModel.GetUnitName(GetNullableValue(l.Job.ed_units_id), unitDictionary),
                            UnitId = GetNullableValue(l.Job.ed_units_id),
                            MainUnit = PersonViewModel.GetUnitMain(GetNullableValue(l.Job.hierarchy), unitDictionary),
                            Phone = GetNullableValue(l.Job.public_phone)
                        }).Distinct(new PersonViewModel());

                    var unitId = units.First(x => x.unit_id.ToString().Equals(department, StringComparison.OrdinalIgnoreCase)).ed_units_id.ToString();
                    return returnValue.Where(list => list.UnitId == unitId).ToList();
                }
            }
        }

        public IEnumerable<char> GetFirstLetters()
        {
            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = "SELECT DISTINCT LEFT([lname],1) as FirstLetter FROM dbo.faculty_professionalappointment job INNER JOIN [dbo].[faculty_profile] prof ON job.username = prof.username";
                var rows = conn.Query(sqlCommand);
                var items = rows.Select<dynamic, string>(x => x.FirstLetter.ToString()).ToList();
                return items.Select(x => char.ToUpperInvariant(Convert.ToChar(x))).OrderBy(x => x);
            }
        }

        public IEnumerable<PersonViewModel> GetPeople(string department)
        {
            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = "SELECT DISTINCT [unit_type],[unit_name],[unit_id],[ed_units_id],[parent_ed_units_id] FROM [dbo].[unit_unit]; SELECT DISTINCT [username],[photo],[lname],[linkedin],[isActive],[fname],[email],[Title] FROM [dbo].[faculty_profile]; SELECT DISTINCT [username],[room],[public_phone],[office_fax],[job_title],[hours],[hierarchy],[ed_units_id],[building],[Title] FROM [dbo].[faculty_professionalappointment]";
                using (var rows = conn.QueryMultiple(sqlCommand))
                {
                    var units = rows.Read();
                    var unitDictionary = units.ToDictionary<dynamic, string, string>(unit => unit.ed_units_id.ToString(), unit => unit.unit_name.ToString());
                    var returnValue = rows.Read().Join(rows.Read(), p => p.username.ToString(), j => j.username.ToString(), (p, j) => new { People = p, Job = j })
                        .Select(l => new PersonViewModel
                        {
                            Email = GetNullableValue(l.People.email),
                            FirstName = GetNullableValue(l.People.fname),
                            LastName = GetNullableValue(l.People.lname),
                            Username = GetNullableValue(l.People.username),
                            Title = GetNullableValue(l.Job.job_title),
                            Hierarchy = GetNullableValue(l.Job.hierarchy),
                            Unit = PersonViewModel.GetUnitName(GetNullableValue(l.Job.ed_units_id), unitDictionary),
                            MainUnit = PersonViewModel.GetUnitMain(GetNullableValue(l.Job.hierarchy), unitDictionary),
                            Phone = GetNullableValue(l.Job.public_phone)
                        }).Distinct(new PersonViewModel());

                    if (string.IsNullOrWhiteSpace(department))
                    {
                        return returnValue.ToList();
                    }
                    if (!units.Any(x => x.unit_id.ToString().Equals(department, StringComparison.OrdinalIgnoreCase)))
                    {
                        return new List<PersonViewModel>();
                    }
                    var unitId = units.First(x => x.unit_id.ToString().Equals(department, StringComparison.OrdinalIgnoreCase)).ed_units_id.ToString();
                    return returnValue.Where(list => list.MainUnitId == unitId).ToList();
                }
            }
        }

        public IEnumerable<PersonViewModel> GetPeopleBySingleLetter(char startingLetter)
        {
            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = "SELECT DISTINCT [username],[room],[public_phone],[office_fax],[job_title],[hours],[hierarchy],[ed_units_id],[building],[Title] FROM [dbo].[faculty_professionalappointment]; SELECT DISTINCT [username],[photo],[lname],[linkedin],[isActive],[fname],[email],[Title] FROM [dbo].[faculty_profile] WHERE lname LIKE @lname";
                using (var rows = conn.QueryMultiple(sqlCommand, new { lname = startingLetter + "%" }))
                {
                    var titlePhones = rows.Read();
                    var returnValue = rows.Read().Select(l =>
                    {
                        var enumerable = titlePhones as dynamic[] ?? titlePhones.ToArray();
                        return new PersonViewModel
                        {
                            Email = GetNullableValue(l.email),
                            FirstName = GetNullableValue(l.fname),
                            LastName = GetNullableValue(l.lname),
                            Username = GetNullableValue(l.username),
                            Title = string.Join(", ", enumerable.Where(tp => GetNullableValue(tp.username) == GetNullableValue(l.username) && GetNullableValue(tp.job_title) != "").Select(tp => GetNullableValue(tp.job_title)).Distinct()),
                            Hierarchy = string.Empty,
                            Unit = string.Empty,
                            MainUnit = string.Empty,
                            Phone = string.Join(", ", enumerable.Where(tp => GetNullableValue(tp.username) == GetNullableValue(l.username) && GetNullableValue(tp.public_phone) != "").Select(tp => GetNullableValue(tp.public_phone)).Distinct()),
                        };
                    }).Distinct(new PersonViewModel());

                    return returnValue.Where(rv => !string.IsNullOrWhiteSpace(rv.Title)).ToList();
                }
            }
        }


        public IEnumerable<PersonViewModel> Search(string searchCriteria)
        {
            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = "SELECT DISTINCT [username],[room],[public_phone],[office_fax],[job_title],[hours],[hierarchy],[ed_units_id],[building],[Title] FROM [dbo].[faculty_professionalappointment]; SELECT DISTINCT [username],[photo],[lname],[linkedin],[isActive],[fname],[email],[Title] FROM [dbo].[faculty_profile] WHERE fname LIKE @search OR lname LIKE @search or username LIKE @search";
                using (var rows = conn.QueryMultiple(sqlCommand, new { search = "%" + searchCriteria + "%" }))
                {
                    var titlePhones = rows.Read();
                    var returnValue = rows.Read().Select(l => new PersonViewModel
                    {
                        Email = GetNullableValue(l.email),
                        FirstName = GetNullableValue(l.fname),
                        LastName = GetNullableValue(l.lname),
                        Username = GetNullableValue(l.username),
                        Title = string.Join(", ", titlePhones.Where(tp => GetNullableValue(tp.username) == GetNullableValue(l.username) && GetNullableValue(tp.job_title) != "").Select(tp => GetNullableValue(tp.job_title)).Distinct()),
                        Hierarchy = string.Empty,
                        Unit = string.Empty,
                        MainUnit = string.Empty,
                        Phone = string.Join(", ", titlePhones.Where(tp => GetNullableValue(tp.username) == GetNullableValue(l.username) && GetNullableValue(tp.public_phone) != "").Select(tp => GetNullableValue(tp.public_phone)).Distinct()),
                    }).Distinct(new PersonViewModel());

                    return returnValue.Where(rv => !string.IsNullOrWhiteSpace(rv.Title)).ToList();
                }
            }
        }
        
        public ProfileViewModel GetSinglePerson(string netid)
        {
            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = "SELECT DISTINCT [unit_type],[unit_name],[unit_id],[ed_units_id],[parent_ed_units_id] FROM [dbo].[unit_unit]; SELECT DISTINCT [username],[photo],[lname],[linkedin],[isActive],[fname],[email],[Title] FROM [dbo].[faculty_profile] WHERE username = @username; SELECT DISTINCT [username],[room],[public_phone],[office_fax],[job_title],[hours],[hierarchy],[ed_units_id],[building],[Title],[PRIMARY_JOB],[JOB_TYPE],street, city, state_code, zip FROM [dbo].[faculty_professionalappointment] LEFT OUTER JOIN [dbo].[sf_addresses] ON [dbo].[faculty_professionalappointment].address = [dbo].[sf_addresses].id WHERE username = @username";
                using (var rows = conn.QueryMultiple(sqlCommand, new { username = netid }))
                {
                    var unitDictionary = new Dictionary<string, string>();
                    foreach (var unit in rows.Read())
                    {
                        unitDictionary.Add(unit.ed_units_id.ToString(), unit.unit_name.ToString());
                    }

                    var faculty = rows.Read();
                    var enumerable = faculty as dynamic[] ?? faculty.ToArray();
                    if (!enumerable.Any())
                    {
                        return new ProfileViewModel();
                    }

                    var returnValue = new ProfileViewModel
                    {
                        Email = GetNullableValue(enumerable.First().email),
                        FirstName = GetNullableValue(enumerable.First().fname),
                        LastName = GetNullableValue(enumerable.First().lname),
                        Username = netid,
                        ImageUrl = GetNullableValue(enumerable.First().photo).Equals("yes", StringComparison.OrdinalIgnoreCase) ? this.GetPhoto(netid) : string.Empty
                    };

                    returnValue.JobInfo = rows.Read().Select(j => new AddressViewModel
                    {
                        Title = GetNullableValue(j.job_title),
                        Hierarchy = GetNullableValue(j.hierarchy),
                        Unit = PersonViewModel.GetUnitName(GetNullableValue(j.ed_units_id), unitDictionary),
                        Phone = GetNullableValue(j.public_phone),
                        Address = GetNullableValue(j.street),
                        City = GetNullableValue(j.city),
                        State = GetNullableValue(j.state_code),
                        Zip = GetNullableValue(j.zip),
                        Room = GetNullableValue(j.room),
                        Building = GetNullableValue(j.building),
                        Hours = GetNullableValue(j.hours),
                        JobType = j.JOB_TYPE.ToString()
                    }).Distinct().ToList();

                    return returnValue;
                }
            }
        }

        public FullProfileViewModel GetFullProfile(string netid)
        {
            var profile = this.GetSinglePerson(netid);
            if (string.IsNullOrWhiteSpace(profile.Username))
            {
                return new FullProfileViewModel();
            }
            var returnValue = new FullProfileViewModel();
            returnValue.Username = profile.Username;
            returnValue.Title = profile.Title;
            returnValue.JobInfo = profile.JobInfo;

            using (var conn = new SqlConnection(this.connectionString))
            {
                var sqlCommand = @"SELECT DISTINCT [year_start], [year_end], [organization_name], [name] FROM [dbo].[faculty_activitiehonor] WHERE username = @username; 
                    SELECT DISTINCT [institution], [degree_year], [degree], [area] FROM [dbo].[faculty_educationalbackground] WHERE username = @username; 
                    SELECT DISTINCT [link_url], [link_name] FROM [dbo].[faculty_link] WHERE username = @username; 
                    SELECT DISTINCT [pubUrl], [bib_entry] FROM [dbo].[faculty_publications] WHERE username = @username; 
                    SELECT DISTINCT [year_start], [year_end], [position_title], [institution] FROM [dbo].[faculty_professionalhistory] WHERE username = @username; 
                    SELECT DISTINCT [bio_research], [bio] FROM [dbo].[faculty_profile] WHERE username = @username";
                using (var rows = conn.QueryMultiple(sqlCommand, new { username = netid }))
                {
                    returnValue.Honors = rows.Read().Select(f => (string)FullProfileViewModel.FormatHonor(f.year_start.ToString(), f.year_end.ToString(), f.name.ToString(), f.organization_name.ToString())).Where(f => !string.IsNullOrWhiteSpace(f));
                    returnValue.EducationalBackground = rows.Read().Select(f => (string)FullProfileViewModel.FormatBackground(f.degree_year.ToString(), f.degree.ToString(), f.area.ToString())).Where(f => !string.IsNullOrWhiteSpace(f));
                    returnValue.Links = rows.Read().Select(f => (Tuple<string, string>)FullProfileViewModel.FormatLink(f.link_url.ToString(), f.link_name.ToString()));
                    returnValue.Publications = rows.Read().Select(f => (Tuple<string, string>)FullProfileViewModel.FormatPublication(f.pubUrl.ToString(), f.bib_entry.ToString()));
                    returnValue.Appointments = rows.Read().Select(f => (string)FullProfileViewModel.FormatAppointment(f.year_start.ToString(), f.year_end.ToString(), f.position_title.ToString(), f.institution.ToString())).Where(f => !string.IsNullOrWhiteSpace(f));
                    var finalRow = rows.Read().First();
                    returnValue.ProfessionalSummary = finalRow.bio.ToString();
                    returnValue.ResearchStatement = finalRow.bio_research.ToString();
                }
            }
            return returnValue;
        }

        private string GetPhoto(string userName)
        {
            if (string.IsNullOrWhiteSpace(this.mapPath))
            {
                return string.Empty;
            }
            var file = new FileInfo(this.mapPath + userName + ".jpg");
            return file.Exists ? this.imagePath + userName + ".jpg" : string.Empty;
        }

        private static string GetNullableValue(dynamic value)
        {
            return value == null ? string.Empty : value.ToString();
        }
    }
}