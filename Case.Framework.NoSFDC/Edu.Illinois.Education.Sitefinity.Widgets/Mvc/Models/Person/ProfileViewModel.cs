﻿namespace Edu.Illinois.Education.Sitefinity.Widgets.Mvc.Models.Person
{
    using System.Collections.Generic;
    using System.Linq;

    public class ProfileViewModel : PersonViewModel
    {
        public string ImageUrl { get; set; }
        public IEnumerable<AddressViewModel> JobInfo { get; set; }
        public bool IsFaculty
        {
            get
            {
                return this.JobInfo != null 
                    && this.JobInfo.Any(x => x.JobType.Contains("Faculty"));
            }
        }
    }
}