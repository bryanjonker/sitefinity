﻿namespace UIUC.Custom.XmlService
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public static class FeedUrlHelper
    {
        public static IEnumerable<Article> GetArticles(string url, string titleNodeValue, int numberOfArticles)
        {
            try
            {
                return XDocument.Load(url).Descendants(titleNodeValue)
                    .Take(numberOfArticles)
                    .Select(x => new Article
                    {
                        Title = x.Descendants("title").FirstOrDefault()?.Value,
                        Description = x.Descendants("description").FirstOrDefault()?.Value,
                        Link = x.Descendants("link").FirstOrDefault()?.Value,
                        ArticleDate = x.Descendants("pubDate").FirstOrDefault()?.Value
                    });
            }
            catch
            {
                return new List<Article>
                {
                    new Article { Title = "Feed currently unavailable" }
                };
            }
        }
    }
}