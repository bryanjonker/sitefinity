﻿namespace UIUC.Custom.XmlService
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    public class XmlUtil
    {
        public static DateTime GetNodeDateTime(XmlNode pXmlNode, string pNodeNameDate)
        {
            var dateValue = GetNodeValue(pXmlNode, pNodeNameDate);
            if (string.IsNullOrWhiteSpace(dateValue))
            {
                return new DateTime();
            }
            DateTime returnValue;
            return DateTime.TryParse(dateValue, out returnValue) ? returnValue : NormalizeDate(dateValue);
        }

        private static DateTime NormalizeDate(string date)
        {
            try
            {
                Dictionary<string, string> timeZones = new Dictionary<string, string> { { "EST", "-05:00" }, { "CST", "-06:00" }, { "CDT", "-05:00" }, { "MST", "-07:00" }, { "PST", "-08:00" }, { "+0000", "+0000" } };

                string inputDate = date;
                string modifiedInputDate = inputDate.Substring(0, inputDate.LastIndexOf(" ", StringComparison.Ordinal));
                string timeZoneIdentifier = inputDate.Substring(inputDate.LastIndexOf(" ", StringComparison.Ordinal) + 1);
                string timeZoneOffset = timeZones[timeZoneIdentifier];
                string dateForParsing = modifiedInputDate + " " + timeZoneOffset;

                DateTime dt = DateTime.ParseExact(dateForParsing, "ddd, dd MMM yyyy HH:mm:ss zzz", System.Globalization.CultureInfo.CurrentCulture);
                return dt;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public static DateTime GetNodeDateTime(XmlNode pXmlNode, string pNodeNameDate, string pNodeNameTime)
        {
            var timeValue = GetNodeValue(pXmlNode, pNodeNameTime);
            var dateValue = GetNodeValue(pXmlNode, pNodeNameDate);

            if (!string.IsNullOrWhiteSpace(timeValue))
            {
                return string.IsNullOrWhiteSpace(dateValue) ? new DateTime() : DateTime.Parse(string.Concat(dateValue, " ", timeValue));
            }
            return string.IsNullOrWhiteSpace(dateValue) ? new DateTime() : DateTime.Parse(dateValue);
        }

        public static string GetNodeValue(XmlNode pXmlNode, string pNodeName)
        {
            var node = pXmlNode.SelectSingleNode(pNodeName);
            return node?.InnerText ?? string.Empty;
        }

        public static bool? GetNodeBoolean(XmlNode pXmlNode, string pNodeName)
        {
            var boolNode = GetNodeValue(pXmlNode, pNodeName);
            if (string.IsNullOrWhiteSpace(boolNode))
            {
                return null;
            }
            return bool.Parse(boolNode);
        }
    }
}