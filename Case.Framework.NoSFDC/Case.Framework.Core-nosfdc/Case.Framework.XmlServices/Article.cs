﻿namespace UIUC.Custom.XmlService
{
    using System.Collections.Generic;

    public class Article
    {
        public string ArticleDate { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }

        public Dictionary<string, string> Translations => new Dictionary<string, string>
                                                              {
                                                                  { "[title]", this.Title },
                                                                  { "[date]", this.ArticleDate },
                                                                  { "[description]", this.Description },
                                                                  { "[link]", this.Link }
                                                              };
    }
}