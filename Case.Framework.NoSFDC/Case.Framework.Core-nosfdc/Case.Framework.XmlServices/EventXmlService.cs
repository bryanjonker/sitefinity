﻿namespace UIUC.Custom.XmlService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using UIUC.Custom.DAL.Events;

    public class EventXmlService
    {
        public List<EventItem> ReadEventXml(DateTime? startDate = null, bool getAllAfterStartDate = false, string xmlUrl = "http://illinois.edu/calendar/eventXML/2772.xml", DateTime? endDate = null)
        {
            var lEvents = new List<EventItem>();

            if (startDate == null)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlUrl);
                GetEventsFromXml(xmlDoc, lEvents);
            }
            else
            {
                string xmlUrlWithParameters;
                if (getAllAfterStartDate)
                {
                    var auxDate = startDate.Value;

                    var endDateAux = endDate ?? DateTime.Now.AddMonths(2);
                    while (auxDate < endDateAux)
                    {
                        xmlUrlWithParameters = string.Join(string.Empty, xmlUrl, "?startDate=", auxDate.Date.ToString());
                        var xmlDoc = new XmlDocument();
                        xmlDoc.Load(xmlUrlWithParameters);
                        GetEventsFromXml(xmlDoc, lEvents);
                        auxDate = auxDate.AddDays(15);
                    }
                }
                else
                {
                    xmlUrlWithParameters = string.Join(string.Empty, xmlUrl, "?startDate=", startDate.Value.Date.ToString());
                    if (endDate != null)
                    {
                        xmlUrlWithParameters = string.Join(string.Empty, xmlUrl, "&endDate=", endDate.Value.ToString());
                    }
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(xmlUrlWithParameters);
                    GetEventsFromXml(xmlDoc, lEvents);
                }
            }
            return lEvents;
        }

        private static void GetEventsFromXml(XmlDocument xmlDoc, List<EventItem> lEvents)
        {
            var xmlNodeList = xmlDoc.ChildNodes[1].SelectNodes("publicEventWS");
            if (xmlNodeList != null)
            {
                foreach (XmlNode item in xmlNodeList)
                {
                    if (lEvents.All(x => x.EventId != Int32.Parse(XmlUtil.GetNodeValue(item, "eventId"))))
                    {
                        lEvents.Add(new EventItem
                        {
                            EventId = Int32.Parse(XmlUtil.GetNodeValue(item, "eventId")),
                            ContactEmail = XmlUtil.GetNodeValue(item, "contactEmail"),
                            ContactPhone = XmlUtil.GetNodeValue(item, "contactPhone"),
                            CreatedBy = XmlUtil.GetNodeValue(item, "createdBy"),
                            CreatedDate = XmlUtil.GetNodeDateTime(item, "createdDate"),
                            StartDate = XmlUtil.GetNodeDateTime(item, "startDate", "startTime"),
                            StartTime = XmlUtil.GetNodeValue(item, "startTime"),
                            EndDate = XmlUtil.GetNodeDateTime(item, "endDate", "endTime"),
                            EndTime = XmlUtil.GetNodeValue(item, "endTime"),
                            TitleLong = XmlUtil.GetNodeValue(item, "titleLong"),
                            TitleLink = XmlUtil.GetNodeValue(item, "titleLink"),
                            TitleShort = XmlUtil.GetNodeValue(item, "titleShort"),
                            EditedBy = XmlUtil.GetNodeValue(item, "editedBy"),
                            EditedDate = XmlUtil.GetNodeDateTime(item, "editedDate"),
                            EventType = XmlUtil.GetNodeValue(item, "eventType"),
                            Recurrence = XmlUtil.GetNodeBoolean(item, "recurrence"),
                            TimeType = XmlUtil.GetNodeValue(item, "timeType"),
                            RegistrationLabel = XmlUtil.GetNodeValue(item, "registrationLabel"),
                            RegistrationUrl = XmlUtil.GetNodeValue(item, "registrationUrl"),
                            DateDisplay = XmlUtil.GetNodeBoolean(item, "dateDisplay"),
                            SearchTerms = XmlUtil.GetNodeValue(item, "searchTerms"),

                            ContactName = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "contactNameHtml")) ? XmlUtil.GetNodeValue(item, "contactNameText") : XmlUtil.GetNodeValue(item, "contactNameHtml"),
                            Sponsor = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "sponsorHtml")) ? XmlUtil.GetNodeValue(item, "sponsorText") : XmlUtil.GetNodeValue(item, "sponsorHtml"),
                            Speaker = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "speakerHtml")) ? XmlUtil.GetNodeValue(item, "speakerText") : XmlUtil.GetNodeValue(item, "speakerHtml"),
                            Description = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "descriptionHtml")) ? XmlUtil.GetNodeValue(item, "descriptionText") : XmlUtil.GetNodeValue(item, "descriptionHtml"),
                            EndTimeLabel = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "endTimeLabelHtml")) ? XmlUtil.GetNodeValue(item, "endTimeLabelText") : XmlUtil.GetNodeValue(item, "endTimeLabelHtml"),
                            CostText = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "costHtml")) ? XmlUtil.GetNodeValue(item, "costText") : XmlUtil.GetNodeValue(item, "costHtml"),
                            LocationText = string.IsNullOrWhiteSpace(XmlUtil.GetNodeValue(item, "locationHtml")) ? XmlUtil.GetNodeValue(item, "locationText") : XmlUtil.GetNodeValue(item, "locationHtml")
                        });
                    }
                }
            }
        }
    }
}