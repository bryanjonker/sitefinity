﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace UIUC.Custom.XmlService
{
    public class BlogService
    {
        public static Dictionary<string,string> GetLastPostData(string pUrl )
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pUrl))
                    pUrl = "https://blogs.education.illinois.edu/dean/feed/";

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(pUrl);
                XmlNode firstPost = null;
                DateTime auxdate = new DateTime(1900, 01, 01);
                foreach (XmlNode item in xmlDoc.SelectSingleNode("rss").SelectSingleNode("channel").SelectNodes("item"))
                {

                    if (auxdate < XmlUtil.GetNodeDateTime(item, "pubDate"))
                    {
                        firstPost = item;
                        auxdate = XmlUtil.GetNodeDateTime(item, "pubDate");
                    }
                }
                
                var result = new Dictionary<string, string>();

                result.Add("Title", XmlUtil.GetNodeValue(firstPost, "title"));
                result.Add("Link", XmlUtil.GetNodeValue(firstPost, "link"));
                result.Add("Description", XmlUtil.GetNodeValue(firstPost, "description"));
                result.Add("PubDate", XmlUtil.GetNodeDateTime(firstPost, "pubDate").ToString());

                return result;
            }
            catch { return new Dictionary<string, string>(); }
        }

    }
}
