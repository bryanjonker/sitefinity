﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using Telerik.Sitefinity.Services.Search;
using Telerik.Sitefinity.Services;
using Case.Framework.Sitefinity.Extensions;
using Telerik.Sitefinity.Services.Search.Web.UI.Public;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Model;
using UIUC.Custom.Sitefinity.ContentManager;

namespace UIUC.Custom.WebApi.Api
{
    public class SearchController : BaseApiController
    {
        public virtual int GetFacetCount(string pCatalogueName, string pSearchQuery, string pSearchFields, string pFacetCategory, string pFacetValue)
        {
            var result = GetSearchResults(pCatalogueName, pSearchQuery, pSearchFields, pFacetCategory, pFacetValue, null, "");

            return result.HitCount;
        }

        public virtual Telerik.Sitefinity.Services.Search.Data.IResultSet GetSearchResults(
                string pCatalogueName,
                string pSearchQuery,
                string pSearchFields,
                string pFacetCategory,
                string pFacetValue,
                string pOrderBy,
                string pTake)
        {
            string[] SearchFields = null;

            //these are the Fields we'll search in
            if (string.IsNullOrWhiteSpace(pSearchFields))
            {
                SearchFields = new string[] { "Title", "Content" };
            }
            else
            {
                SearchFields = new JavaScriptSerializer().Deserialize<string[]>(pSearchFields);
            }

            var orderBy = string.IsNullOrWhiteSpace(pOrderBy) ? null : new JavaScriptSerializer().Deserialize<string[]>(pOrderBy);


            //get the Sitefinity Search service            
            var searchService = Telerik.Sitefinity.Services.ServiceBus.ResolveService<ISearchService>();

            string compiledQuery = string.Empty;

            if (string.IsNullOrWhiteSpace(pFacetCategory) || string.IsNullOrWhiteSpace(pFacetValue) || pFacetValue == "-1")
            {
                if (!string.IsNullOrWhiteSpace(pSearchQuery))
                {
                    //Build the query
                    compiledQuery = searchService.BuildQuery(pSearchQuery, SearchFields, SystemManager.CurrentContext.AppSettings.Multilingual);
                }
                else
                {
                    compiledQuery = SearchExtension.BuildCustomQuery(searchService, pSearchQuery, SearchFields, false, null);
                }
            }
            else
            {
                var FacetList = new List<FacetModel>();
                FacetList.Add(new FacetModel() { FacetCategory = pFacetCategory, FacetValue = new List<string>() { pFacetValue } });

                compiledQuery = SearchExtension.BuildCustomQuery(searchService, pSearchQuery, SearchFields, false, FacetList);
            }

            int take;

            //Get the search results
            if (!string.IsNullOrWhiteSpace(pTake))
            {
                take = int.Parse(pTake);
                var result = searchService.Search(pCatalogueName, compiledQuery, null, 0, take, orderBy, null);
                return result;
            }
            else
            {
                var result = searchService.Search(pCatalogueName, compiledQuery, null, orderBy, null);
                return result;
            }

        }

        public virtual Telerik.Sitefinity.Services.Search.Data.IResultSet GetSearchResults(
            string pCatalogueName,
            string pSearchQuery,
            string pSearchFields,
            string pFacets,
            string pOrderBy,
            string pTake)
        {
            var searchService = Telerik.Sitefinity.Services.ServiceBus.ResolveService<ISearchService>();
            string[] SearchFields = null;

            if (string.IsNullOrWhiteSpace(pSearchFields))
            {
                SearchFields = new string[] { "Title", "Content" };
            }
            else
            {
                SearchFields = new JavaScriptSerializer().Deserialize<string[]>(pSearchFields);
            }

            var Facets = new JavaScriptSerializer().Deserialize<List<FacetModel>>(pFacets);

            string compiledQuery = SearchExtension.BuildCustomQuery(searchService, pSearchQuery, SearchFields, false, Facets);

            var orderBy = string.IsNullOrWhiteSpace(pOrderBy) ? null : new JavaScriptSerializer().Deserialize<string[]>(pOrderBy);

            int take;

            //Get the search results
            if (!string.IsNullOrWhiteSpace(pTake))
            {
                take = int.Parse(pTake);
                var result = searchService.Search(pCatalogueName, compiledQuery, null, 0, take, orderBy, null);
                return result;
            }
            else
            {
                var result = searchService.Search(pCatalogueName, compiledQuery, null, orderBy, null);
                return result;
            }
        }

        public virtual List<object> GetFacetCount(string pCatalogueName, string pSearchQuery, string pSearchFields, string pFacets)
        {

            var Facets = new JavaScriptSerializer().Deserialize<List<FacetModel>>(pFacets);
            var result = new List<object>();

            //Parallel.ForEach(Facets, item =>
            //{
            //    Parallel.ForEach(item.FacetValue, facet =>
            //    {
            //        result.Add(new { value = facet, Count = GetSearchResults(pCatalogueName, pSearchQuery, pSearchFields, item.FacetCategory, facet,null,"").HitCount });
            //    });

            //});

            foreach (var item in Facets)
            {
                foreach (var facet in item.FacetValue)
                {
                    if (facet == "-1" && string.IsNullOrWhiteSpace(pSearchQuery))
                    {
                        if (pCatalogueName == "courses")
                        {
                            var dynamicModuleManager = DynamicModuleManager.GetManager();
                            Type courseType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.UIUCCourse.Course");
                            var courses = dynamicModuleManager.GetDataItems(courseType)
                                .Where(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live
                                    && x.Visible
                                     );

                            if (courses != null)
                            {
                                result.Add(new { value = facet, Count = courses.Count() });
                            }
                            else
                            {
                                result.Add(new { value = facet, Count = 0 });
                            }

                        }
                    }
                    else
                    {
                        if (pCatalogueName == "programs")
                        {
                            if (facet == "-1")
                            {
                                result.Add(new { value = facet, Count = ProgramsContentManager.GetAreaOfStudyCountByFormat("", pSearchQuery) });

                            }
                            else
                                if (item.FacetCategory == "Format")
                                {

                                    result.Add(new { value = facet, Count = ProgramsContentManager.GetAreaOfStudyCountByFormat(facet, pSearchQuery) });
                                }
                                else if (item.FacetCategory == "CredentialType")
                                {

                                    result.Add(new { value = facet, Count = ProgramsContentManager.GetAreaOfStudyCountByDegree("CredentialType", facet, pSearchQuery) });

                                } if (item.FacetCategory == "Department")
                            {
                                result.Add(new { value = facet, Count = ProgramsContentManager.GetAreaOfStudyCountByDepartment(facet, pSearchQuery) });
                            }
                        }
                        else
                        {
                            result.Add(new { value = facet, Count = GetSearchResults(pCatalogueName, pSearchQuery, pSearchFields, item.FacetCategory, facet, null, "").HitCount });
                        }
                    }
                }
            }

            return result;
        }

    }
}
