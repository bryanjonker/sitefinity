﻿using System.Reflection;
using Case.Framework.Sitefinity.Themes.Scripts;

namespace Case.Framework.Sitefinity.Themes.Aspnet
{
    public class DiagnosticBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.AddEmbeddedResources<ScriptBundle>(
                "~/Case.Framework.Sitefinity.Themes.Aspnet.Resources",
                Assembly.GetExecutingAssembly(),
                "Case.Framework.Sitefinity.Themes.Aspnet.Resources",

                "jquery.js",
                "knockout.js",
                "diagnostic-page.js"
            );
        }
    }
}