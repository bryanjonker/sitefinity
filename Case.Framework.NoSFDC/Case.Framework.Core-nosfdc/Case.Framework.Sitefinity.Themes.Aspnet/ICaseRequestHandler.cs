namespace Case.Framework.Sitefinity.Themes.Aspnet
{
    interface ICaseRequestHandler
    {
        void ProcessRequest(string path);
    }
}