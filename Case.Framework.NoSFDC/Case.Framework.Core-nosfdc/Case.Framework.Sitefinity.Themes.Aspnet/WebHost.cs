using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using Case.Framework.Sitefinity.Themes.TinyIoC;

namespace Case.Framework.Sitefinity.Themes.Aspnet
{
    public class WebHost : HostBase
    {
        protected override IEnumerable<Assembly> LoadAssemblies()
        {
            return BuildManager.GetReferencedAssemblies().Cast<Assembly>();
        }

        protected override bool CanCreateRequestLifetimeProvider
        {
            get { return true; }
        }

        protected override TinyIoCContainer.ITinyIoCObjectLifetimeProvider CreateRequestLifetimeProvider()
        {
            return new HttpContextLifetimeProvider(() => Container.Resolve<HttpContextBase>());
        }

        protected override void ConfigureContainer()
        {
            // These are before base.ConfigureContainer() so the application is able to override them - for example providing a different IUrlModifier.
            Container.Register((c, p) => HttpContext());
            Container.Register((c, p) => c.Resolve<HttpContextBase>().Request);
            Container.Register((c, p) => c.Resolve<HttpContextBase>().Response);
            Container.Register(typeof(IUrlModifier), CreateUrlModifier());
            
            Container.Register<ICaseRequestHandler, AssetRequestHandler>("AssetRequestHandler").AsPerRequestSingleton(CreateRequestLifetimeProvider());
            Container.Register<ICaseRequestHandler>(
                (c, n) =>
                new CachedFileRequestHandler(c.Resolve<HttpRequestBase>(), c.Resolve<HttpResponseBase>(), c.Resolve<CaseSettings>().CacheDirectory),
                "CachedFileRequestHandler"
            );
            Container.Register<ICaseRequestHandler, BundleRequestHandler<Scripts.ScriptBundle>>("ScriptBundleRequestHandler").AsPerRequestSingleton(CreateRequestLifetimeProvider());
            Container.Register<ICaseRequestHandler, BundleRequestHandler<Stylesheets.StylesheetBundle>>("StylesheetBundleRequestHandler").AsPerRequestSingleton(CreateRequestLifetimeProvider());
            Container.Register<ICaseRequestHandler, BundleRequestHandler<HtmlTemplates.HtmlTemplateBundle>>("HtmlTemplateBundleRequestHandler").AsPerRequestSingleton(CreateRequestLifetimeProvider());
            Container.Register<IDiagnosticRequestHandler, DiagnosticRequestHandler>().AsPerRequestSingleton(CreateRequestLifetimeProvider());
            Container.Register<RawFileRequestRewriter>().AsPerRequestSingleton(CreateRequestLifetimeProvider());

            base.ConfigureContainer();
        }

        IUrlModifier CreateUrlModifier()
        {
            return new VirtualDirectoryPrepender(AppDomainAppVirtualPath);
        }

        protected virtual string AppDomainAppVirtualPath
        {
            get { return HttpRuntime.AppDomainAppVirtualPath; }
        }

        protected virtual HttpContextBase HttpContext()
        {
            return new HttpContextWrapper(System.Web.HttpContext.Current);
        }

        public PlaceholderRewriter CreatePlaceholderRewriter()
        {
            return Container.Resolve<PlaceholderRewriter>();
        }

        protected override IConfiguration<CaseSettings> CreateHostSpecificSettingsConfiguration()
        {
            return new WebHostSettingsConfiguration(AppDomainAppVirtualPath);
        }

        const string RequestContainerKey = "CaseRequestContainer";

        public void StoreRequestContainerInHttpContextItems()
        {
            var context = HttpContext();
            context.Items[RequestContainerKey] = Container.GetChildContainer();
        }

        public TinyIoCContainer RequestContainer
        {
            get { return HttpContext().Items[RequestContainerKey] as TinyIoCContainer; }
        }

        public void RemoveRequestContainerFromHttpContextItems()
        {
            var context = HttpContext();
            var container = context.Items[RequestContainerKey] as TinyIoCContainer;
            if (container != null)
            {
                container.Dispose();
                context.Items.Remove(RequestContainerKey);
            }
        }
    }
}