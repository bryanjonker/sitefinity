namespace Case.Framework.Sitefinity.Themes.Aspnet
{
    interface IDiagnosticRequestHandler
    {
        void ProcessRequest();
    }
}