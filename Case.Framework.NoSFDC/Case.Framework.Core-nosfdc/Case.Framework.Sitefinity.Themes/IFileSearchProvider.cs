using System;

namespace Case.Framework.Sitefinity.Themes
{
    public interface IFileSearchProvider
    {
        IFileSearch GetFileSearch(Type bundleType);
    }
}