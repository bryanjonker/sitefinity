﻿using System.Collections.Generic;
using Case.Framework.Sitefinity.Themes.IO;

namespace Case.Framework.Sitefinity.Themes
{
    public interface IFileSearch
    {
        IEnumerable<IFile> FindFiles(IDirectory directory);
        bool IsMatch(string filename);
    }
}
