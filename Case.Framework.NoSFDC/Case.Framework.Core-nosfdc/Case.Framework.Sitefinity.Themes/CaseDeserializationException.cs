using System;
using System.Runtime.Serialization;

namespace Case.Framework.Sitefinity.Themes
{
    [Serializable]
    class CaseDeserializationException : Exception
    {
        public CaseDeserializationException(string message) : base(message)
        {   
        }

        public CaseDeserializationException(string message, Exception innerException) : base(message, innerException)
        {   
        }

        protected CaseDeserializationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}