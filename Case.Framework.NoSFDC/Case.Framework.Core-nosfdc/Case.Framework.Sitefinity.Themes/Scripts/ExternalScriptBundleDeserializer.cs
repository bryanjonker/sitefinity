using System.Xml.Linq;
using Case.Framework.Sitefinity.Themes.TinyIoC;
using Case.Framework.Sitefinity.Themes.Utilities;

namespace Case.Framework.Sitefinity.Themes.Scripts
{
    class ExternalScriptBundleDeserializer : ScriptBundleDeserializerBase<ExternalScriptBundle>
    {
        public ExternalScriptBundleDeserializer(TinyIoCContainer container)
            : base(container)
        {
        }

        protected override ExternalScriptBundle CreateBundle(XElement element)
        {
            var url = GetUrlAttribute(element);
            var path = GetPathAttribute();
            var fallbackCondition = element.AttributeValueOrNull("FallbackCondition");

            var externalScriptBundle = new ExternalScriptBundle(url, path, fallbackCondition);
            AssignScriptBundleProperties(externalScriptBundle, element);
            externalScriptBundle.FallbackRenderer = CreateHtmlRenderer<ScriptBundle>("FallbackRenderer");
            return externalScriptBundle;
        }

        string GetUrlAttribute(XElement manifestElement)
        {
            return manifestElement.AttributeValueOrThrow(
                "Url",
                () => new CaseDeserializationException("ExternalScriptBundle manifest element is missing \"Url\" attribute.")
            );
        }
    }
}