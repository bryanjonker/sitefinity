﻿using Case.Framework.Sitefinity.Themes.BundleProcessing;

namespace Case.Framework.Sitefinity.Themes.Scripts
{
    public class AssignScriptRenderer : IBundleProcessor<ScriptBundle>
    {
        readonly IUrlGenerator urlGenerator;
        readonly CaseSettings settings;

        public AssignScriptRenderer(IUrlGenerator urlGenerator, CaseSettings settings)
        {
            this.urlGenerator = urlGenerator;
            this.settings = settings;
        }

        public void Process(ScriptBundle bundle)
        {
            if (settings.IsDebuggingEnabled)
            {
                bundle.Renderer = new DebugScriptBundleHtmlRenderer(urlGenerator);
            }
            else
            {
                bundle.Renderer = new ScriptBundleHtmlRenderer(urlGenerator);
            }
        }
    }
}