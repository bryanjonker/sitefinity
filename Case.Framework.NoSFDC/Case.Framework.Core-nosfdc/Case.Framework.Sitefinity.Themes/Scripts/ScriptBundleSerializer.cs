﻿using System.Xml.Linq;

namespace Case.Framework.Sitefinity.Themes.Scripts
{
    class ScriptBundleSerializer : ScriptBundleSerializerBase<ScriptBundle>
    {
        public ScriptBundleSerializer(XContainer container) 
            : base(container)
        {
        }
    }
}