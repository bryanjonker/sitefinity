﻿using System;
using Case.Framework.Sitefinity.Themes.BundleProcessing;
using Case.Framework.Sitefinity.Themes.TinyIoC;

namespace Case.Framework.Sitefinity.Themes.Scripts
{
    public class ScriptPipeline : BundlePipeline<ScriptBundle>
    {
        public ScriptPipeline(TinyIoCContainer container, CaseSettings settings)
            : base(container)
        {
            AddRange(new IBundleProcessor<ScriptBundle>[]
            {
                container.Resolve<AssignScriptRenderer>(),
                new ParseJavaScriptReferences(),
                new SortAssetsByDependency(),
                new AssignHash()
            });

            if (settings.IsDebuggingEnabled) return;

            Add(new ConcatenateAssets { Separator = Environment.NewLine + ";" + Environment.NewLine });
            var minifier = container.Resolve<IJavaScriptMinifier>();
            Add(new MinifyAssets(minifier));
        }
    }
}