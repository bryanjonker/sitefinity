namespace Case.Framework.Sitefinity.Themes
{
    public interface IBundleCacheRebuilder
    {
        void RebuildCache();
    }
}