﻿using System;
using Case.Framework.Sitefinity.Themes.BundleProcessing;

namespace Case.Framework.Sitefinity.Themes.HtmlTemplates
{
    public class ParseHtmlTemplateReferences : ParseReferences<HtmlTemplateBundle>
    {
        protected override bool ShouldParseAsset(IAsset asset)
        {
            return asset.Path.EndsWith(".htm", StringComparison.OrdinalIgnoreCase)
                || asset.Path.EndsWith(".html", StringComparison.OrdinalIgnoreCase)
                || asset.Path.EndsWith(".jst", StringComparison.OrdinalIgnoreCase)
                || asset.Path.EndsWith(".tmpl", StringComparison.OrdinalIgnoreCase)
                || asset.Path.EndsWith(".mustache", StringComparison.OrdinalIgnoreCase);
        }

        protected override ICommentParser CreateCommentParser()
        {
            return new HtmlTemplateCommentParser();
        }
    }
}
