namespace Case.Framework.Sitefinity.Themes.HtmlTemplates
{
    public interface IHtmlTemplateIdStrategy
    {
        string HtmlTemplateId(HtmlTemplateBundle bundle, IAsset htmlTemplateAsset);
    }
}