﻿using System;
using System.Linq;
using Case.Framework.Sitefinity.Themes.Utilities;

namespace Case.Framework.Sitefinity.Themes.HtmlTemplates
{
    class InlineHtmlTemplateBundleRenderer : IBundleHtmlRenderer<HtmlTemplateBundle>
    {
        public string Render(HtmlTemplateBundle bundle)
        {
            return string.Join(
                Environment.NewLine,
                bundle.Assets.Select(asset => asset.OpenStream().ReadToEnd()).ToArray()
            );
        }
    }
}