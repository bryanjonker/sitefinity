﻿using Case.Framework.Sitefinity.Configuration;
using Case.Framework.Sitefinity.Themes.BundleProcessing;
using Case.Framework.Sitefinity.Themes.Scripts;
using Case.Framework.Sitefinity.Themes.Spriting;
using Case.Framework.Sitefinity.Themes.Stylesheets;
using System;
using Telerik.Sitefinity.Configuration;


namespace Case.Framework.Sitefinity.Themes.Configuration
{
    public class BundleConfig:IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {

            foreach (var sfBundle in Config.Get<CASEFrameworkConfig>().BundlesMapping.Values)
            {
                if (sfBundle.BundleType == BundleType.CSS)
                {
                    if (sfBundle.BrowserType == BrowserType.All)
                    {
                        bundles.Add<StylesheetBundle>(sfBundle.AssetPath);
                    }
                    else
                    {
                        bundles.Add<StylesheetBundle>(sfBundle.AssetPath, 
                            b => b.Condition = sfBundle.BrowserType.ToString());
                    }
                }
                else if (sfBundle.BundleType == BundleType.Script)
                {
                    bundles.AddPerSubDirectory<ScriptBundle>(sfBundle.AssetPath);
                }
            }


            //bundles.AddPerSubDirectory<StylesheetBundle>("assets/styles", new FileSearch
            //{
            //    Pattern = "*.css"
            //});

            //bundles.Add<StylesheetBundle>("assets/styles", b =>
            //{
            //    //  b.SpriteImages();
            //    b.Pipeline.Add<ConcatenateAssets>();
            //});
            //bundles.Add<StylesheetBundle>("assets/iestyles", b => b.Condition = "IE");

            //bundles.AddPerSubDirectory<ScriptBundle>("assets/scripts");
            //bundles.AddUrlWithLocalAssets(
            //    "//ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js",
            //    new LocalAssetSettings
            //    {
            //        FallbackCondition = "!window.jQuery",
            //        Path = "assets/scripts/jquery"
            //    }
            //);
        }
        public class FileAccessAuthorizationConfiguration : IConfiguration<IFileAccessAuthorization>
        {
            public void Configure(IFileAccessAuthorization files)
            {
                files.AllowAccess(path => path.StartsWith("~/assets/styles/images", StringComparison.OrdinalIgnoreCase));
            }
        }
    }
}
