namespace Case.Framework.Sitefinity.Themes
{
    public interface IBundleCollectionInitializer
    {
        void Initialize(BundleCollection bundles);
    }
}