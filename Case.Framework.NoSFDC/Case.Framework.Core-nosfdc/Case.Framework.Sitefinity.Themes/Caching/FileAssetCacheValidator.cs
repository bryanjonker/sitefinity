using System;
using Case.Framework.Sitefinity.Themes.IO;

namespace Case.Framework.Sitefinity.Themes.Caching
{
    class FileAssetCacheValidator : IAssetCacheValidator
    {
        readonly IDirectory sourceDirectory;

        public FileAssetCacheValidator(IDirectory sourceDirectory)
        {
            this.sourceDirectory = sourceDirectory;
        }

        public bool IsValid(string assetPath, DateTime asOfDateTime)
        {
            var file = sourceDirectory.GetFile(assetPath);
            return file.Exists && file.LastWriteTimeUtc <= asOfDateTime;
        }
    }
}