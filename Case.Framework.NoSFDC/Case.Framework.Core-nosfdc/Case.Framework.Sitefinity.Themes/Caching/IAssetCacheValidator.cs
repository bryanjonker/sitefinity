using System;

namespace Case.Framework.Sitefinity.Themes.Caching
{
    public interface IAssetCacheValidator
    {
        bool IsValid(string assetPath, DateTime asOfDateTime);
    }
}