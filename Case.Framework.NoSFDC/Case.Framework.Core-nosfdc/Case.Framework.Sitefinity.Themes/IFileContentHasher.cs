namespace Case.Framework.Sitefinity.Themes
{
    public interface IFileContentHasher
    {
        byte[] Hash(string filename);
    }
}