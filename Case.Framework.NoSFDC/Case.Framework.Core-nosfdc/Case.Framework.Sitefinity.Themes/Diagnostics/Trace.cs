using System.Diagnostics;

namespace Case.Framework.Sitefinity.Themes.Diagnostics
{
    public static class Trace
    {
        public static readonly TraceSource Source = new TraceSource("Cassette");
    }
}