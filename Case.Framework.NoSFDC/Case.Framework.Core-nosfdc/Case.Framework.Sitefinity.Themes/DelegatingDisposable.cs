using System;

namespace Case.Framework.Sitefinity.Themes
{
    class DelegatingDisposable : IDisposable
    {
        readonly Action dispose;

        public DelegatingDisposable(Action dispose)
        {
            this.dispose = dispose;
        }

        public void Dispose()
        {
            dispose();
        }
    }
}