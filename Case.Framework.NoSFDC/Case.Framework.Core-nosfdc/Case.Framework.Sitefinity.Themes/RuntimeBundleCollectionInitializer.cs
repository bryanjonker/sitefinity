namespace Case.Framework.Sitefinity.Themes
{
    class RuntimeBundleCollectionInitializer : IBundleCollectionInitializer
    {
        readonly CaseSettings settings;
        readonly CacheAwareBundleCollectionInitializer cacheAwareBundleCollectionInitializer;
        readonly BundleCollectionInitializer bundleCollectionInitializer;

        public RuntimeBundleCollectionInitializer(
            CaseSettings settings,
            CacheAwareBundleCollectionInitializer cacheAwareBundleCollectionInitializer,
            BundleCollectionInitializer bundleCollectionInitializer)
        {
            this.settings = settings;
            this.cacheAwareBundleCollectionInitializer = cacheAwareBundleCollectionInitializer;
            this.bundleCollectionInitializer = bundleCollectionInitializer;
        }

        public void Initialize(BundleCollection bundles)
        {
            if (settings.IsDebuggingEnabled)
            {
                bundleCollectionInitializer.Initialize(bundles);
            }
            else
            {
                cacheAwareBundleCollectionInitializer.Initialize(bundles);
            }
        }
    }
}