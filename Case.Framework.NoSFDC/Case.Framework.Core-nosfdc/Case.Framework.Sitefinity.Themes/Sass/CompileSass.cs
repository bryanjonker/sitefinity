﻿using System;
using System.Linq;
using Case.Framework.Sitefinity.Themes.BundleProcessing;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    public class CompileSass : IBundleProcessor<StylesheetBundle>
    {
        readonly ISassCompiler sassCompiler;
        readonly CaseSettings settings;

        public CompileSass(ISassCompiler sassCompiler, CaseSettings settings)
        {
            this.sassCompiler = sassCompiler;
            this.settings = settings;
        }

        public void Process(StylesheetBundle bundle)
        {
            var sassAssets = bundle.Assets.Where(IsSassOrScss);
            foreach (var asset in sassAssets)
            {
                asset.AddAssetTransformer(new CompileAsset(sassCompiler, settings.SourceDirectory));
            }
        }

        bool IsSassOrScss(IAsset asset)
        {
            var path = asset.Path;
            return path.EndsWith(".scss", StringComparison.OrdinalIgnoreCase) ||
                   path.EndsWith(".sass", StringComparison.OrdinalIgnoreCase);
        }
    }
}