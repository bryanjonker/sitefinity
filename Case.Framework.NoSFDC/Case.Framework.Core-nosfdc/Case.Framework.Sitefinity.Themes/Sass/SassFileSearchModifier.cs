
namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    public class SassFileSearchModifier : IFileSearchModifier<StylesheetBundle>
    {
        public void Modify(FileSearch fileSearch)
        {
            fileSearch.Pattern += ";*.scss;*.sass";
        }
    }
}