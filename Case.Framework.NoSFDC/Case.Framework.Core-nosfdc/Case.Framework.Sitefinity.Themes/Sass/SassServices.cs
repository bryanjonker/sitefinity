﻿using Case.Framework.Sitefinity.Themes.TinyIoC;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    [ConfigurationOrder(20)]
    public class SassServices : IConfiguration<TinyIoCContainer>
    {
        public void Configure(TinyIoCContainer container)
        {
            container.Register<ISassCompiler, SassCompiler>();
        }
    }
}