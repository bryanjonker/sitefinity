﻿#if !NET35

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Case.Framework.Sitefinity.Themes.IO;
using System.Web;


namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    /// <remarks>
    ///
    /// </remarks>
    public class SassCompiler : ISassCompiler
    {
      
        bool initialized;
        readonly object _lock = new object();
        List<string> importedFilePaths = new List<string>();
        IDirectory rootDirectory;

        public CompileResult Compile(string source, CompileContext context)
        {
            var sourceFile = context.RootDirectory.GetFile(context.SourceFilePath);
            lock (_lock)
            {
                rootDirectory = sourceFile.Directory;
                Initialize();

                try
                {
                    LibSassNet.ISassCompiler Compiler = new LibSassNet.SassCompiler();
                    var file = HttpContext.Current.Server.MapPath(sourceFile.FullPath);
                    
                    // This is just a temporary fix for now
                    // We'll come back to this at some point and implement this the right way
                     string css = Compiler.CompileFile(file, additionalIncludePaths: new[] { file }).CSS;
                     if (!importedFilePaths.Contains(sourceFile.FullPath))
                         importedFilePaths.Add(sourceFile.FullPath);
                    
                    return new CompileResult(css, importedFilePaths);
                }
                catch (Exception e) 
                {
                    // Provide more information for SassSyntaxErrors
                    if (e.Message == "Sass::SyntaxError")
                    {
                        throw CreateSassSyntaxError(sourceFile, e);
                    }
                    else
                    {
                        throw;
                    }
                }
                finally
                {
                }
            }
        }

      
        Exception CreateSassSyntaxError(IFile sourceFile, Exception originalException)
        {
            dynamic rubyError = originalException;
            var message = new StringBuilder();
            message.AppendFormat("{0}\n\n", rubyError.to_s());
            message.AppendFormat("Backtrace:\n{0}\n\n", rubyError.sass_backtrace_str(sourceFile.FullPath) ?? "");
            message.AppendFormat("FileName: {0}\n\n", rubyError.sass_filename() ?? sourceFile.FullPath);
            message.AppendFormat("MixIn: {0}\n\n", rubyError.sass_mixin() ?? "");
            message.AppendFormat("Line Number: {0}\n\n", rubyError.sass_line() ?? "");
            message.AppendFormat("Sass Template:\n{0}\n\n", rubyError.sass_template ?? "");
            return new Exception(message.ToString(), originalException);
        }

        void Initialize()
        {
            if (initialized) return;

          
            initialized = true;
        }

        IDirectory RootDirectory()
        {
            return rootDirectory;
        }
    }
}
#endif