using System;
using Case.Framework.Sitefinity.Themes.BundleProcessing;
using Case.Framework.Sitefinity.Themes.Scripts;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    public class ParseSassReferences : ParseReferences<StylesheetBundle>
    {
        protected override bool ShouldParseAsset(IAsset asset)
        {
            var path = asset.Path;
            return path.EndsWith(".scss", StringComparison.OrdinalIgnoreCase) ||
                   path.EndsWith(".sass", StringComparison.OrdinalIgnoreCase);
        }

        protected override ICommentParser CreateCommentParser()
        {
            // Sass supports the same comment syntax as JavaScript.
            // So we'll just reuse the JavaScript comment parser!
            return new JavaScriptCommentParser();
        }
    }
}