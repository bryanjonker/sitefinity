using Case.Framework.Sitefinity.Themes.Stylesheets;

namespace Case.Framework.Sitefinity.Themes.Spriting
{
    public static class StylesheetBundleExtensions
    {
        public static void SpriteImages(this StylesheetBundle bundle)
        {
            bundle.Pipeline.Insert<SpriteImages>(bundle.Pipeline.Count);
        }
    }
}