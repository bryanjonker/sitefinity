namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic
{
    interface ISpriteGenerator
    {
        SpritePackage GenerateFromCss(string cssContent, string cssPath);
    }
}