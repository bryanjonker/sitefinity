﻿using System.Collections.Generic;
using Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Parser;

namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Generator
{
    interface ICssImageExtractor
    {
        IEnumerable<BackgroundImageClass> ExtractImageUrls(string cssContent);
    }
}