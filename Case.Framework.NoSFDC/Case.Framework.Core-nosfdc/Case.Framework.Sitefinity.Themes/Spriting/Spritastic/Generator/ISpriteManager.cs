using System;
using System.Collections.Generic;
using Case.Framework.Sitefinity.Themes.Spriting.Spritastic.ImageLoad;
using Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Parser;

namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Generator
{
    interface ISpriteManager : IEnumerable<SpritedImage>, IDisposable
    {
        void Add(BackgroundImageClass imageUrl);
        IList<Sprite> Flush();
        IImageLoader ImageLoader { get; set; }
        Predicate<BackgroundImageClass> ImageExclusionFilter { get; set; }
        IList<SpriteException> Errors { get; }
    }
}