using System.Collections.Generic;
using Case.Framework.Sitefinity.Themes.Spriting.Spritastic.ImageLoad;
using Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Parser;

namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Generator
{
    interface ISpriteContainer : IEnumerable<SpritedImage>
    {
        SpritedImage AddImage (BackgroundImageClass image);
        void AddImage(SpritedImage image);
        int Size { get; }
        int Colors { get; }
        int Width { get; }
        int Height { get; }
        IImageLoader ImageLoader { get; set; }
    }
}