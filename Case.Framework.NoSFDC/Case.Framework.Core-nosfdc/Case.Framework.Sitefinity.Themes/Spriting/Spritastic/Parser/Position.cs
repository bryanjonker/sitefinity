﻿namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Parser
{
    struct Position
    {
        public PositionMode PositionMode { get; set; }
        public int Offset { get; set; }
        public Direction Direction { get; set; }
    }
}