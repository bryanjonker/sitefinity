namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Parser
{
    enum RepeatStyle
    {
        Repeat,
        NoRepeat,
        XRepeat,
        YRepeat
    }
}