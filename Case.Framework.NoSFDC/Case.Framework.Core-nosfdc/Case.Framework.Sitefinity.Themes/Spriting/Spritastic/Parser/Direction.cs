namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Parser
{
    enum Direction
    {
        Center,
        Left,
        Right,
        Top,
        Bottom
    }
}