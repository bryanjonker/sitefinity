﻿namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.SpriteStore
{
    interface ISpriteStore
    {
        string SaveSpriteAndReturnUrl(byte[] spriteBytes);
    }
}
