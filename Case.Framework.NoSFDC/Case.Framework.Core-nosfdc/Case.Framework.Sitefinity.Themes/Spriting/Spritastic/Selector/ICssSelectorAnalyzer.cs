namespace Case.Framework.Sitefinity.Themes.Spriting.Spritastic.Selector
{
    interface ICssSelectorAnalyzer
    {
        bool IsInScopeOfTarget(string targetSelector, string comparableSelector);
    }
}