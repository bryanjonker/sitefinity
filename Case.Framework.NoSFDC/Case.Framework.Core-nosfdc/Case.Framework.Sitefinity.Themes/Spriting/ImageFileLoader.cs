using System;
using System.IO;
using System.Text.RegularExpressions;
using Case.Framework.Sitefinity.Themes.IO;
#if NET35
using Case.Framework.Sitefinity.Themes.Utilities;
#endif
using Case.Framework.Sitefinity.Themes.Spriting.Spritastic.ImageLoad;

namespace Case.Framework.Sitefinity.Themes.Spriting
{
    class ImageFileLoader : IImageLoader
    {
        readonly IDirectory directory;

        public ImageFileLoader(IDirectory directory)
        {
            this.directory = directory;
        }

        public string BasePath { get; set; }

        public byte[] GetImageBytes(string url)
        {
            using (var stream = OpenFile(url))
            using (var memory = new MemoryStream((int)stream.Length))
            {
                stream.CopyTo(memory);
                return memory.ToArray();
            }
        }

        Stream OpenFile(string url)
        {
            var path = GetFilePathFromCaseFileUrl(url);
            return directory.GetFile(path).OpenRead();
        }

        string GetFilePathFromCaseFileUrl(string url)
        {
            // Image URLs will already have been rewritten by the ExpandCssUrls processor.
            // e.g. "/Case.axd/file/some/path-hash.png"
            // We need to map back to the original path
            // e.g. "some/path.png"

            var urlRegex = UrlRegex();
            var match = urlRegex.Match(url);
            if (!match.Success) throw new ArgumentException("URL must be a Case URL of a PNG image.", "url");

            var filename = match.Groups["filename"].Value;
            return filename + ".png";
        }

        Regex UrlRegex()
        {
            var prefix = Regex.Escape("case.axd/file/");
            return new Regex(
                prefix + @"(?<filename>.*)-[A-Za-z0-9\-_=]+\.png",
                RegexOptions.IgnoreCase
                );
        }
    }
}