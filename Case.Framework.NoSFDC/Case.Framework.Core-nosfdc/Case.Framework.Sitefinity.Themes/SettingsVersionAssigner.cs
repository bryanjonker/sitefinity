
namespace Case.Framework.Sitefinity.Themes
{
    [ConfigurationOrder(1)]
    class SettingsVersionAssigner : IConfiguration<CaseSettings>
    {
        readonly AssemblyScanner assemblyScanner;

        public SettingsVersionAssigner(AssemblyScanner assemblyScanner)
        {
            this.assemblyScanner = assemblyScanner;
        }

        public void Configure(CaseSettings settings)
        {
            settings.Version = assemblyScanner.HashAssemblies();
        }
    }
}