﻿using System;

namespace Case.Framework.Sitefinity.Themes
{
    public static class Constants
    {
        public const string VALUE_CUSTOM_VIRTUAL_ROOT_PATH = "~/CaseThemesSF";

        public const string VALUE_VIRTUAL_MASTERPAGE_PATH = "~/CaseMasterPagesSF/";

        public const string VALUE_VIRTUAL_MASTERPAGE_NAMESPACE = "Case.Framework.Sitefinity.Themes.Resources.Pages";
    }
}
