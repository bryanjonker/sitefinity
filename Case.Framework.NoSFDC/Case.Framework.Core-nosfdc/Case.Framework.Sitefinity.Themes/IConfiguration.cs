﻿namespace Case.Framework.Sitefinity.Themes
{
    public interface IConfiguration<in T>
    {
        void Configure(T configurable);
    }
}