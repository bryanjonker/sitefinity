﻿using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace Case.Framework.Sitefinity.Themes.Classes
{
    public class MasterPageVirtualFile : VirtualFile
    {
        private string _virtualPath;

        public MasterPageVirtualFile(string virtualPath)
            : base(virtualPath)
        {
            this._virtualPath = virtualPath;
        }

        public override Stream Open()
        {
            if (HttpContext.Current == null)
            {
                return MasterPageVirtualFile.ReadResource(this._virtualPath);
            }
            if (HttpContext.Current.Cache[this._virtualPath] == null)
            {
                HttpContext.Current.Cache.Insert(this._virtualPath, MasterPageVirtualFile.ReadResource(this._virtualPath));
            }
            return (Stream)HttpContext.Current.Cache[this._virtualPath];
        }

        private static Stream ReadResource(string embeddedFileName)
        {
            string str = string.Concat("Case.Framework.Sitefinity.Themes.Resources.Pages.", VirtualPathUtility.GetFileName(embeddedFileName));
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(str);
        }
    }
}
