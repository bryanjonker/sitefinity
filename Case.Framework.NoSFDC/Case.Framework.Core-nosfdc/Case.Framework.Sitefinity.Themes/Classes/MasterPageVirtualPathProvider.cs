﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;


namespace Case.Framework.Sitefinity.Themes.Classes
{
    public class MasterPageVirtualPathProvider : VirtualPathProvider
    {
        public MasterPageVirtualPathProvider()
        {
        }

        public override bool FileExists(string virtualPath)
        {
            if (!MasterPageVirtualPathProvider.IsProviderPath(virtualPath))
            {
                return base.Previous.FileExists(virtualPath);
            }
            return (MasterPageVirtualFile)this.GetFile(virtualPath) != null;
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            return null;
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            if (MasterPageVirtualPathProvider.IsProviderPath(virtualPath))
            {
                return new MasterPageVirtualFile(virtualPath);
            }
            return base.Previous.GetFile(virtualPath);
        }

        private static bool IsProviderPath(string virtualPath)
        {
            return VirtualPathUtility.ToAppRelative(virtualPath).StartsWith("~/CaseMasterPagesSF/", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
