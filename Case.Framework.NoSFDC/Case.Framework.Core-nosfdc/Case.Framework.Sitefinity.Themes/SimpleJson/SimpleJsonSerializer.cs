﻿namespace Case.Framework.Sitefinity.Themes
{
    public class SimpleJsonSerializer : IJsonSerializer
    {
        public string Serialize(object objectToSerialize)
        {
            return SimpleJson.SerializeObject(objectToSerialize);
        }
    }
}