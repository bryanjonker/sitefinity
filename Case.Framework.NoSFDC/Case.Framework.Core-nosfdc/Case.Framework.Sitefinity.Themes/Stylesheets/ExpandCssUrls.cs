﻿using Case.Framework.Sitefinity.Themes.BundleProcessing;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    public class ExpandCssUrls : IBundleProcessor<StylesheetBundle>
    {
        readonly IUrlGenerator urlGenerator;
        readonly CaseSettings settings;

        public ExpandCssUrls(IUrlGenerator urlGenerator, CaseSettings settings)
        {
            this.urlGenerator = urlGenerator;
            this.settings = settings;
        }

        public void Process(StylesheetBundle bundle)
        {
            foreach (var asset in bundle.Assets)
            {
                asset.AddAssetTransformer(new ExpandCssUrlsAssetTransformer(settings.SourceDirectory, urlGenerator));
            }
        }
    }
}