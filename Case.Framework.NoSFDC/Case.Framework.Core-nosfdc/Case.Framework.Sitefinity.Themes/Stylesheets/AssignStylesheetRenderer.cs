﻿using Case.Framework.Sitefinity.Themes.BundleProcessing;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    public class AssignStylesheetRenderer : IBundleProcessor<StylesheetBundle>
    {
        readonly IUrlGenerator urlGenerator;
        readonly CaseSettings settings;

        public AssignStylesheetRenderer(IUrlGenerator urlGenerator, CaseSettings settings)
        {
            this.urlGenerator = urlGenerator;
            this.settings = settings;
        }

        public void Process(StylesheetBundle bundle)
        {
            if (settings.IsDebuggingEnabled)
            {
                bundle.Renderer = new DebugStylesheetHtmlRenderer(urlGenerator);
            }
            else
            {
                bundle.Renderer = new StylesheetHtmlRenderer(urlGenerator);
            }
        }
    }
}