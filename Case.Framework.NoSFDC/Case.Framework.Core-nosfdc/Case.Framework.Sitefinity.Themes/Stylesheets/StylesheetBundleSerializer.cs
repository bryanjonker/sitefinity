using System.Xml.Linq;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    class StylesheetBundleSerializer : StylesheetBundleSerializerBase<StylesheetBundle>
    {
        public StylesheetBundleSerializer(XContainer container) 
            : base(container)
        {
        }
    }
}