﻿using System;
using Case.Framework.Sitefinity.Themes.BundleProcessing;

namespace Case.Framework.Sitefinity.Themes.Stylesheets
{
    public class ConvertImageUrlsToDataUris : AddTransformerToAssets<StylesheetBundle>
    {
        public delegate ConvertImageUrlsToDataUris Factory(Func<string, bool> shouldEmbedUrl);

        readonly Func<string, bool> shouldEmbedUrl;
        readonly CaseSettings settings;

        public ConvertImageUrlsToDataUris(Func<string, bool> shouldEmbedUrl, CaseSettings settings)
        {
            this.shouldEmbedUrl = shouldEmbedUrl;
            this.settings = settings;
        }

        protected override IAssetTransformer CreateAssetTransformer(StylesheetBundle bundle)
        {
            return new CssImageToDataUriTransformer(shouldEmbedUrl, settings.SourceDirectory);
        }
    }
}