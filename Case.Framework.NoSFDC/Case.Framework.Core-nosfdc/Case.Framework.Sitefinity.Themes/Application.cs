﻿using Case.Framework.Sitefinity;
using Case.Framework.Sitefinity.Models.Abstracts;
using Case.Framework.Sitefinity.Themes.Classes;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Web.Hosting;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Web.UI;

namespace Case.Framework.Sitefinity.Themes
{
    public class Application : BaseApplication<Application>
    {
        public Application()
        {
        }

        protected static void OnBootstrapperInitialized(object sender, ExecutedEventArgs e)
        {
            if (e.CommandName == "Bootstrapped")
            {
                string str = "Bootstrap Columns";
                int? nullable = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("100%", "1 Column (span12)", "sfL100", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.1col_col12.ascx", str, nullable, ToolboxType.PageLayouts);
                string str1 = "Bootstrap Columns";
                int? nullable1 = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("33% + 67%", "2 Columns (col-4 + col-8)", "sfL33_67", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.2col_col4-8.ascx", str1, nullable1, ToolboxType.PageLayouts);
                string str2 = "Bootstrap Columns";
                int? nullable2 = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("50% + 50%", "2 Columns (col-6 + col-6)", "sfL50_50", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.2col_col6-6.ascx", str2, nullable2, ToolboxType.PageLayouts);
                string str3 = "Bootstrap Columns";
                int? nullable3 = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("67% + 33%", "2 Columns (col-8 + col-4)", "sfL67_33", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.2col_col8-4.ascx", str3, nullable3, ToolboxType.PageLayouts);
                string str4 = "Bootstrap Columns";
                int? nullable4 = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("33% + 33% + 33%", "3 Columns (col-4 + col-4 + col-4)", "sfL33_34_33", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.3col_col4-4-4.ascx", str4, nullable4, ToolboxType.PageLayouts);
                string str5 = "Bootstrap Columns";
                int? nullable5 = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("25% + 50% + 25%", "3 Columns (col-3 + col-6 + col-3)", "sfL25_50_25", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.3col_col3-6-3.ascx", str5, nullable5, ToolboxType.PageLayouts);
                string str6 = "Bootstrap Columns";
                int? nullable6 = null;
                ConfigHelper.RegisterToolboxWidget<LayoutControl>("4 x 25%", "4 Columns (col-3 + col-3 + col-3 + col-3)", "sfL25_25_25_25", null, "~/CaseThemesSF/Case.Framework.Sitefinity.Themes.Resources.Layouts.4col_col3-3-3-3.ascx", str6, nullable6, ToolboxType.PageLayouts);
            }
        }

        protected static void OnBootstrapperInitializing(object sender, ExecutingEventArgs e)
        {
            if (e.CommandName == "RegisterRoutes")
            {
                ConfigHelper.RegisterVirtualPath("~/CaseThemesSF/*", "Case.Framework.Sitefinity.Themes");
                ConfigHelper.RegisterVirtualPath("~/CaseMasterPagesSF/*", "Case.Framework.Sitefinity.Themes");
            }
        }

        public static void PreInit()
        {
            BaseApplication<Application>.RegisterStartup();
            HostingEnvironment.RegisterVirtualPathProvider(new MasterPageVirtualPathProvider());
            Bootstrapper.Initializing += new EventHandler<ExecutingEventArgs>(Application.OnBootstrapperInitializing);
            Bootstrapper.Initialized += new EventHandler<ExecutedEventArgs>(Application.OnBootstrapperInitialized);
        }
    }
}