﻿using System.Collections.Generic;
using Case.Framework.Sitefinity.Themes.IO;

namespace Case.Framework.Sitefinity.Themes
{
    public interface IBundleFactory<out T>
        where T : Bundle
    {
        T CreateBundle(string path, IEnumerable<IFile> allFiles, BundleDescriptor bundleDescriptor);
    }
}