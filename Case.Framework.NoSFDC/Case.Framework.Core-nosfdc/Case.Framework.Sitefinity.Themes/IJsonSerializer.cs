﻿namespace Case.Framework.Sitefinity.Themes
{
    public interface IJsonSerializer
    {
        string Serialize(object objectToSerialize);
    }
}