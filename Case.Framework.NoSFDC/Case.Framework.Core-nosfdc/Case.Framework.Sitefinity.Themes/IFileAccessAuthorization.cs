using System;

namespace Case.Framework.Sitefinity.Themes
{
    public interface IFileAccessAuthorization
    {
        void AllowAccess(string path);
        void AllowAccess(Func<string, bool> pathPredicate);
        bool CanAccess(string path);
    }
}