using System;
using System.Collections.Generic;
using Case.Framework.Sitefinity.Themes.IO;

namespace Case.Framework.Sitefinity.Themes
{
    class FilePathComparer : IEqualityComparer<IFile>
    {
        public bool Equals(IFile x, IFile y)
        {
            return x.FullPath.Equals(y.FullPath, StringComparison.OrdinalIgnoreCase);
        }

        public int GetHashCode(IFile obj)
        {
            return obj.FullPath.GetHashCode();
        }
    }
}