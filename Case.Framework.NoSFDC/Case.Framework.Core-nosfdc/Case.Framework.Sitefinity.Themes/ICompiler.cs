﻿namespace Case.Framework.Sitefinity.Themes
{
    public interface ICompiler
    {
        CompileResult Compile(string source, CompileContext context);
    }
}