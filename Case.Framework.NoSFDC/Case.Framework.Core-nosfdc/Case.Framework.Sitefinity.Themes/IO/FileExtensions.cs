﻿using System.IO;

namespace Case.Framework.Sitefinity.Themes.IO
{
    public static class FileExtensions
    {
        public static Stream OpenRead(this IFile file)
        {
            return file.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }
    }
}
