using System.Collections.Generic;

namespace Case.Framework.Sitefinity.Themes
{
    public class AssetPathComparer : IEqualityComparer<IAsset>
    {
        public bool Equals(IAsset x, IAsset y)
        {
            return x.Path.Equals(y.Path);
        }

        public int GetHashCode(IAsset obj)
        {
            return obj.Path.GetHashCode();
        }
    }
}