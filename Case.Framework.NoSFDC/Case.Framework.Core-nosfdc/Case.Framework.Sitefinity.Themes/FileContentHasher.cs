using Case.Framework.Sitefinity.Themes.IO;
using Case.Framework.Sitefinity.Themes.Utilities;

namespace Case.Framework.Sitefinity.Themes
{
    public class FileContentHasher : IFileContentHasher
    {
        readonly CaseSettings settings;

        public FileContentHasher(CaseSettings settings)
        {
            this.settings = settings;
        }

        public byte[] Hash(string filename)
        {
            var file = settings.SourceDirectory.GetFile(filename);
            using (var stream = file.OpenRead())
            {
                return stream.ComputeSHA1Hash();
            }
        }
    }
}