﻿namespace Case.Framework.Sitefinity.Themes
{
    public interface IExternalBundle
    {
        string ExternalUrl { get; }
    }
}