using System.Xml.Linq;
using Case.Framework.Sitefinity.Themes.IO;

namespace Case.Framework.Sitefinity.Themes
{
    public interface IBundleDeserializer<out T> where T : Bundle
    {
        T Deserialize(XElement element, IDirectory cacheDirectory);
    }
}