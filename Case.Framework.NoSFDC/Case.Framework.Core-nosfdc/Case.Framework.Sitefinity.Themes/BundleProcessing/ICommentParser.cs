﻿using System.Collections.Generic;

namespace Case.Framework.Sitefinity.Themes.BundleProcessing
{
    public interface ICommentParser
    {
        IEnumerable<Comment> Parse(string code);
    }
}
