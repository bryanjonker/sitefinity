﻿namespace Case.Framework.Sitefinity.Themes.BundleProcessing
{
    public interface IBundleProcessor<in T>
        where T : Bundle
    {
        void Process(T bundle);
    }
}