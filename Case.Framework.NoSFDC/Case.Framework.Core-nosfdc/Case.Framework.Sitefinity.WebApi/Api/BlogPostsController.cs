﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using Telerik.Sitefinity.Blogs.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class BlogPostsController : BaseChildController<BlogPostModel, BlogPost>
    {
        public BlogPostsController()
            : base(CaseManagers.BlogPosts)
        {
        }
    }
}