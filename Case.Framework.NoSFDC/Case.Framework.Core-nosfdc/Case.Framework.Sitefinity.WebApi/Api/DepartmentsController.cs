﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using System.Collections.Generic;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class DepartmentsController : BaseApiController
    {
        public DepartmentsController()
        {
        }

        public virtual IEnumerable<DepartmentModel> Get()
        {
            return CaseManagers.Departments.GetAll(null, 0, 0);
        }

        public virtual DepartmentModel Get(Guid id)
        {
            return CaseManagers.Departments.GetById(id, null);
        }

        public virtual DepartmentModel GetByName(string value)
        {
            return CaseManagers.Departments.GetByName(value, null);
        }

        public virtual IEnumerable<DepartmentModel> GetByParent(string value)
        {
            return CaseManagers.Departments.GetByParent(value, null, null, 0, 0);
        }

        public virtual IEnumerable<DepartmentModel> GetByParentId(Guid id)
        {
            return CaseManagers.Departments.GetByParentId(id, null, null, 0, 0);
        }

        public virtual IEnumerable<DepartmentModel> GetByParentTitle(string value)
        {
            return CaseManagers.Departments.GetByParentTitle(value, null, null, 0, 0);
        }

        public virtual DepartmentModel GetByTitle(string value)
        {
            return CaseManagers.Departments.GetByTitle(value, null);
        }
    }
}