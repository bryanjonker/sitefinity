﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using Telerik.Sitefinity.Lists.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class ListsController : BaseContentController<ListItemModel, ListItem>
    {
        public ListsController()
            : base(CaseManagers.Lists)
        {
        }
    }
}