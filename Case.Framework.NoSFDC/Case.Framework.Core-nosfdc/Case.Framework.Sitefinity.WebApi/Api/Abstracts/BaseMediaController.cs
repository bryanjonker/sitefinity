﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.WebApi.Api.Abstracts
{
    public abstract class BaseMediaController<TContentModel, TContent> : BaseChildController<TContentModel, TContent>
        where TContentModel : MediaModel, new()
        where TContent : IDataItem, IDynamicFieldsContainer
    {
        public IMediaManager<TContentModel, TContent> MediaManager
        {
            get;
            set;
        }

        public BaseMediaController()
        {
        }

        public BaseMediaController(IMediaManager<TContentModel, TContent> manager)
            : base(manager)
        {
            this.MediaManager = manager;
        }

        public virtual IEnumerable<TContentModel> GetByExtension(string value)
        {
            return this.MediaManager.GetByExtension(value, null, null, 0, 0);
        }
    }
}