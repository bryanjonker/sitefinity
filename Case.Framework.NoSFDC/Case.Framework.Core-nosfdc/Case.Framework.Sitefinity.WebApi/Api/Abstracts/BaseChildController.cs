﻿using System.Linq;
using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.WebApi.Api.Abstracts
{
    public abstract class BaseChildController<TContentModel, TContent> : BaseContentController<TContentModel, TContent>
        where TContentModel : ContentModel, new()
        where TContent : IDataItem, IDynamicFieldsContainer
    {
        public IChildManager<TContentModel, TContent> ChildManager
        {
            get;
            set;
        }

        public BaseChildController()
        {
        }

        public BaseChildController(IChildManager<TContentModel, TContent> manager)
            : base(manager)
        {
            this.ChildManager = manager;
        }

        public virtual IEnumerable<TContentModel> GetByParent(string value)
        {
            return this.ChildManager.GetByParent(value, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByParentId(Guid id)
        {
            return this.ChildManager.GetByParentId(id, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByParentTitle(string value)
        {
            return this.ChildManager.GetByParentTitle(value, null, null, 0, 0);
        }
    }
}