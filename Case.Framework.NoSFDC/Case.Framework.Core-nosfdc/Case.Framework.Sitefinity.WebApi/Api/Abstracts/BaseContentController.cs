﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Web.Http;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.WebApi.Api.Abstracts
{
    public abstract class BaseContentController<TContentModel, TContent> : BaseApiController
        where TContentModel : ContentModel, new()
        where TContent : IDataItem, IDynamicFieldsContainer
    {
        public IContentManager<TContentModel, TContent> Manager
        {
            get;
            set;
        }

        public BaseContentController()
        {
        }

        public BaseContentController(IContentManager<TContentModel, TContent> manager)
        {
            this.Manager = manager;
        }

        public virtual IEnumerable<TContentModel> Get()
        {
            return this.Manager.GetAll(null, null, 0, 0);
        }

        public virtual TContentModel Get(Guid id)
        {
            return this.Manager.GetById(id, null);
        }

        public virtual IEnumerable<TContentModel> GetByCategory(string value)
        {
            return this.Manager.GetByCategory(value, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByCategoryId(Guid id)
        {
            return this.Manager.GetByCategoryId(id, null, null, 0, 0);
        }

        public virtual TContentModel GetByName(string value)
        {
            return this.Manager.GetByName(value, null);
        }

        public virtual IEnumerable<TContentModel> GetByTag(string value)
        {
            return this.Manager.GetByTag(value, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByTagId(Guid id)
        {
            return this.Manager.GetByTagId(id, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByTaxonomy(string key, string value)
        {
            return this.Manager.GetByTaxonomy(key, value, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByTaxonomyId(string key, Guid id)
        {
            return this.Manager.GetByTaxonomyId(key, id, null, null, 0, 0);
        }

        public virtual IEnumerable<TContentModel> GetByTaxonomyTitle(string key, string value)
        {
            return this.Manager.GetByTaxonomyTitle(key, value, null, null, 0, 0);
        }

        public virtual TContentModel GetByTitle(string value)
        {
            return this.Manager.GetByTitle(value, null);
        }

        public virtual IEnumerable<TContentModel> GetRecent(int take = 25)
        {
            return this.Manager.GetRecent(null, null, take, 0);
        }

        [HttpGet]
        public virtual IEnumerable<TContentModel> Search(string value)
        {
            return this.Manager.Search(value, null, null, 0, 0);
        }
    }
}