﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using System.Collections.Generic;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class TaxonomiesController : BaseApiController
    {
        public TaxonomiesController()
        {
        }

        public virtual IEnumerable<TaxonModel> Get()
        {
            return CaseManagers.Taxonomies.GetAll(null, null, 0, 0);
        }

        public virtual TaxonModel Get(Guid id)
        {
            return CaseManagers.Taxonomies.GetById(id, null);
        }
    }
}