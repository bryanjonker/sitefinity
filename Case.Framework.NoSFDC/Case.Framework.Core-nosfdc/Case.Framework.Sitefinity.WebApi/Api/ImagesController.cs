﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Telerik.Sitefinity.Libraries.Model;
using System.Linq;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class ImagesController : BaseMediaController<ImageModel, Image>
    {
        public ImagesController()
            : base(CaseManagers.Images)
        {
        }

        [HttpGet]
        public IEnumerable<ImageModel> GetImagesByAlbumId(string Id)
        {
            Guid albumId = new Guid(Id);

            var images = CaseManagers.Images.GetAll().Where(x => x.AlbumId == albumId);

            if (images != null)
                return images;

            return new List<ImageModel>();
        }
    }
}