﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using System.Web;
using System.Web.Http;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Claims;
using Telerik.Sitefinity.Security.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class AccountsController : BaseApiController
    {
        public AccountsController()
        {
        }

        public virtual UserModel GetCurrent()
        {
            return CaseManagers.Accounts.GetCurrent();
        }

        [HttpGet]
        public virtual UserLoggingReason Login(string username, string password, bool persistent = true)
        {
            User user;
            UserManager manager = UserManager.GetManager();
            UserLoggingReason userLoggingReason = SecurityManager.AuthenticateUser(manager.Provider.Name, username, password, persistent, out user);
            if (userLoggingReason == UserLoggingReason.UserAlreadyLoggedIn)
            {
                ClaimsManager.Logout(HttpContext.Current, ClaimsManager.GetCurrentPrincipal());
                userLoggingReason = SecurityManager.AuthenticateUser(manager.Provider.Name, username, password, persistent, out user);
            }
            return userLoggingReason;
        }
    }
}