﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class ContentsController : BaseApiController
    {
        public ContentsController()
        {
        }

        public virtual string Get(Guid id)
        {
            return CaseManagers.Contents.GetById(id, null);
        }

        public virtual string GetByName(string value)
        {
            return CaseManagers.Contents.GetByName(value, null);
        }

        public virtual string GetByTitle(string value)
        {
            return CaseManagers.Contents.GetByTitle(value, null);
        }
    }
}