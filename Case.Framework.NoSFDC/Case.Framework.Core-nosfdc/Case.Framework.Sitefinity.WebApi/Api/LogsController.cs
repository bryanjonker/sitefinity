﻿using Case.Framework.Sitefinity.Utilities;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using System.Web.Http;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class LogsController : BaseApiController
    {
        public LogsController()
        {
        }

        [HttpPost]
        public virtual void Create(string message, string file, string line, string url, string userAgent)
        {
            ErrorHelper.LogMessage(message, file, line, url, userAgent);
        }
    }
}