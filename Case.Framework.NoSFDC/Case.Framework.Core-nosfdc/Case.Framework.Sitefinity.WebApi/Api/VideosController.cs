﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using Telerik.Sitefinity.Libraries.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class VideosController : BaseMediaController<VideoModel, Video>
    {
        public VideosController()
            : base(CaseManagers.Videos)
        {
        }
    }
}