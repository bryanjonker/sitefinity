﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using Telerik.Sitefinity.Blogs.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class BlogsController : BaseContentController<BlogModel, Blog>
    {
        public BlogsController()
            : base(CaseManagers.Blogs)
        {
        }
    }
}