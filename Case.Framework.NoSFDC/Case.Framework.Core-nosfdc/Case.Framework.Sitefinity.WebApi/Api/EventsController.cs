﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.Events.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class EventsController : BaseChildController<EventModel, Event>
    {
        public EventsController()
            : base(CaseManagers.Events)
        {
        }

        public virtual IEnumerable<EventModel> GetPast()
        {
            return CaseManagers.Events.GetPast(null, null, 0, 0);
        }

        public virtual IEnumerable<EventModel> GetRange(DateTime start, DateTime? end = null)
        {
            return CaseManagers.Events.GetRange(start, end, null, null, 0, 0);
        }

        public virtual IEnumerable<EventModel> GetUpcoming()
        {
            return CaseManagers.Events.GetUpcoming(null, null, 0, 0);
        }
    }
}