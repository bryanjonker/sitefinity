﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;
using Telerik.Sitefinity.News.Model;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class NewsController : BaseContentController<NewsItemModel, NewsItem>
    {
        public NewsController()
            : base(CaseManagers.News)
        {
        }
    }
}