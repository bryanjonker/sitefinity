﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using System;

namespace Case.Framework.Sitefinity.WebApi.Api
{
    public class PagesController : BaseApiController
    {
        public PagesController()
        {
        }

        public virtual PageModel Get()
        {
            return CaseManagers.Pages.GetAll();
        }

        public virtual PageModel Get(Guid id)
        {
            return CaseManagers.Pages.GetById(id);
        }

        public virtual PageModel GetByUrl(string value)
        {
            return CaseManagers.Pages.GetByUrl(value, true);
        }
    }
}