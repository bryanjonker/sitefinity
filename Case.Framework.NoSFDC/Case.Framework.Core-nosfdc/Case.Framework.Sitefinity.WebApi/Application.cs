﻿using Case.Framework.Sitefinity.Configuration;
using Case.Framework.Sitefinity.Models.Abstracts;
using Case.Framework.Sitefinity.Utilities;
using Case.Framework.Sitefinity.WebApi.Routes;
using System;
using System.Collections.ObjectModel;
using System.Web.Routing;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;

namespace Case.Framework.Sitefinity.WebApi
{
    public class Application : BaseApplication<Application>
    {
        public Application()
        {
        }

        protected static void OnBootstrapperInitialized(object sender, ExecutedEventArgs e)
        {
            if (e.CommandName == "RegisterRoutes" && (!ConfigHelper.DoesSectionExist<CASEFrameworkConfig>() || Config.Get<CASEFrameworkConfig>().EnableWebApi))
            {
                string str = PageHelper.GetScriptsPath().TrimStart(new char[] { '/' });
                RouteTable.Routes.Add(new Route(string.Concat(str, "/baseservicesurl"), new ServicesUrlModule()));
                WebApiConfig.RegisterAll();
            }
        }

        public static void PreInit()
        {
            BaseApplication<Application>.RegisterStartup();
            Bootstrapper.Initialized += new EventHandler<ExecutedEventArgs>(Application.OnBootstrapperInitialized);
        }
    }
}