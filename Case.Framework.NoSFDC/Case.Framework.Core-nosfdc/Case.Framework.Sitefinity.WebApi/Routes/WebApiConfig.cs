﻿using Case.Framework.Sitefinity.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

using Telerik.Sitefinity.Configuration;

namespace Case.Framework.Sitefinity.WebApi.Routes
{
    using System.Web;
    using System.Web.Routing;

    using HttpMethodConstraint = System.Web.Http.Routing.HttpMethodConstraint;

    public static class WebApiConfig
    {
        public static void RegisterAll()
        {
            WebApiConfig.RegisterRoutes(GlobalConfiguration.Configuration);

            var formatters = GlobalConfiguration.Configuration.Formatters;

            formatters.Remove(formatters.XmlFormatter);

        }

        public static void RegisterRoutes(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("CaseWebApiControllerActionId", "api/{controller}/{action}/{id}", null, new { action = "^[a-zA-Z]+[_ a-zA-Z0-9]*$", id = new GuidConstraint() });
            config.Routes.MapHttpRoute("CaseWebApiControllerActionName", "api/{controller}/{action}/{name}", null, new { action = "^[a-zA-Z]+[_ a-zA-Z0-9]*$", name = "^[a-zA-Z]+[_ a-zA-Z0-9]*$" });
            config.Routes.MapHttpRoute("CaseWebApiControllerId", "api/{controller}/{id}", new { action = "Get" }, new { id = new GuidConstraint() });
            config.Routes.MapHttpRoute("CaseWebApiControllerAction", "api/{controller}/{action}", null, new { action = "^[a-zA-Z]+[_ a-zA-Z0-9]*$" });
            HttpRouteCollection routes = config.Routes;
            var variable = new { action = "Get" };
            HttpMethod[] get = new HttpMethod[] { HttpMethod.Get };
            routes.MapHttpRoute("CaseWebApiControllerGet", "api/{controller}", variable, new { httpMethod = new HttpMethodConstraint(get) });
            HttpRouteCollection httpRouteCollections = config.Routes;
            HttpMethod[] post = new HttpMethod[] { HttpMethod.Post };
            httpRouteCollections.MapHttpRoute("CaseWebApiControllerPost", "api/{controller}", null, new { httpMethod = new HttpMethodConstraint(post) });
            HttpRouteCollection routes1 = config.Routes;
            HttpMethod[] put = new HttpMethod[] { HttpMethod.Put };
            routes1.MapHttpRoute("CaseWebApiControllerPut", "api/{controller}", null, new { httpMethod = new HttpMethodConstraint(put) });
            HttpRouteCollection httpRouteCollections1 = config.Routes;
            var variable1 = new { action = "Delete" };
            HttpMethod[] delete = new HttpMethod[] { HttpMethod.Delete };
            httpRouteCollections1.MapHttpRoute("CaseWebApiControllerDelete", "api/{controller}", variable1, new { httpMethod = new HttpMethodConstraint(delete) });
        }
    }

    public class GuidConstraint : IRouteConstraint
    {
        public GuidConstraint()
        {
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            object obj;
            Guid guid;
            if (!values.TryGetValue(parameterName, out obj))
            {
                return false;
            }
            if (obj is Guid)
            {
                return true;
            }
            if (!Guid.TryParse(Convert.ToString(obj), out guid))
            {
                return false;
            }
            return guid != Guid.Empty;
        }
    }

}