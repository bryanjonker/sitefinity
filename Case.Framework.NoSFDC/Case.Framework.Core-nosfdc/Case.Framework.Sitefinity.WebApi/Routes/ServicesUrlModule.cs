﻿namespace Case.Framework.Sitefinity.WebApi.Routes
{
    using System.Web;
    using System.Web.Routing;

    public class ServicesUrlModule : IHttpHandler, IRouteHandler
    {
        public string Key
        {
            get
            {
                return "baseservicesurl";
            }
        }

        public string Value
        {
            get
            {
                return VirtualPathUtility.ToAbsolute("~/api");
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/javascript";
            context.Response.Write(this.Value);
        }

        public bool IsReusable{
            get
            {
                return true;
            }
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }
    }
}