﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Sitefinity.Configuration
{
    public enum DatasourceType
    {
        Courses,
        Faculty,
        Grants,
        Units,
        Events
    }
}
