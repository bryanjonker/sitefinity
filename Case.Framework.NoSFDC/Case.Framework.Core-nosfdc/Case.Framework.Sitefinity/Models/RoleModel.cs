﻿using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Security;

namespace Case.Framework.Sitefinity.Models
{
    public class RoleModel
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Provider
        {
            get;
            set;
        }

        public RoleModel(RoleInfo sfContent)
        {
            this.Id = sfContent.Id;
            this.Name = sfContent.Name;
            this.Provider = sfContent.Provider;
        }
    }
}