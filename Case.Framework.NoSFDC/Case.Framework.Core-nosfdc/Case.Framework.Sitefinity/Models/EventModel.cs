﻿using Case.Framework.Sitefinity.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Events.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Events;
using Telerik.Sitefinity.Modules.GenericContent;

namespace Case.Framework.Sitefinity.Models
{
    public class EventModel : ContentModel
    {
        public bool Active
        {
            get;
            set;
        }

        public List<TaxonModel> Categories
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public int CommentsCount
        {
            get;
            set;
        }

        public string ContactCell
        {
            get;
            set;
        }

        public string ContactEmail
        {
            get;
            set;
        }

        public string ContactName
        {
            get;
            set;
        }

        public string ContactPhone
        {
            get;
            set;
        }

        public string ContactWeb
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public DateTime? EndDate
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public CalendarModel Parent
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Street
        {
            get;
            set;
        }

        public string Summary
        {
            get;
            set;
        }

        public List<TaxonModel> Tags
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public string LongTitle
        {
            get;
            set;
        }

        public EventModel()
        {
            this.Categories = new List<TaxonModel>();
            this.Tags = new List<TaxonModel>();
        }

        public EventModel(Event sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                this.Content = sfContent.Content;
                this.Summary = sfContent.Summary;
                this.StartDate = sfContent.EventStart;
                this.EndDate = sfContent.EventEnd;
                this.Street = sfContent.Street;
                this.City = sfContent.City;
                this.Country = sfContent.Country;
                this.ContactName = sfContent.ContactName;
                this.ContactEmail = sfContent.ContactEmail;
                this.ContactWeb = sfContent.ContactWeb;
                this.ContactPhone = sfContent.ContactPhone;
                this.ContactCell = sfContent.ContactCell;
                this.Url = sfContent.GetFullUrl(sfContent.DefaultPageId);
                this.Slug = sfContent.UrlName;
                this.Status = sfContent.Status;
                this.Active = true;
                if (sfContent.Parent != null)
                {
                    this.Parent = new CalendarModel(sfContent.Parent);
                }
                this.Categories = sfContent.GetTaxa("Category");
                this.Tags = sfContent.GetTaxa("Tags");
                if (sfContent.DoesFieldExist("Image"))
                {
                    this.Image = sfContent.GetValue<string>("Image");
                }
                if (sfContent.DoesFieldExist("TitleLong"))
                {
                    this.LongTitle = sfContent.GetValue<string>("TitleLong");
                }
            }
        }
    }
}