﻿namespace Case.Framework.Sitefinity.Models
{
    using System.Collections.Generic;
    using System.Linq;

    using Case.Framework.Sitefinity.Extensions;

    using Telerik.Sitefinity.Blogs.Model;
    using Telerik.Sitefinity.GenericContent.Model;
    using Telerik.Sitefinity.Model;
    using Telerik.Sitefinity.Modules.Blogs;

    public class BlogPostModel : ContentModel
    {
        public bool Active
        {
            get;
            set;
        }

        public List<TaxonModel> Categories
        {
            get;
            set;
        }

        public int CommentsCount
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public BlogModel Parent
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Summary
        {
            get;
            set;
        }

        public List<TaxonModel> Tags
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public BlogPostModel()
        {
            this.Categories = new List<TaxonModel>();
            this.Tags = new List<TaxonModel>();
        }

        public BlogPostModel(BlogPost sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                this.Content = sfContent.Content;
                this.Summary = sfContent.Summary;
                this.Status = sfContent.Status;
                this.Slug = sfContent.UrlName;
                this.Active = true;
                this.Parent = new BlogModel(sfContent.Parent);
                this.Url = sfContent.GetFullUrl(sfContent.DefaultPageId);
                if (sfContent.Parent.DefaultPageId.HasValue)
                {
                    this.Url = string.Concat(this.Parent.Url, this.Url);
                }
                this.Categories = sfContent.GetTaxa("Category");
                this.Tags = sfContent.GetTaxa("Tags");
                if (sfContent.DoesFieldExist("Image"))
                {
                    this.Image = sfContent.GetValue<string>("Image");
                }
            }
        }
    }
}