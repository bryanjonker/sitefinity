﻿using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models.Interfaces;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace Case.Framework.Sitefinity.Models
{
    public abstract class DynamicModel : IDataModel
    {
        public bool Active
        {
            get;
            set;
        }

        public DateTime DateCreated
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public abstract string MappedType
        {
            get;
        }

        public Guid OriginalContentId
        {
            get;
            set;
        }

        public DateTime PublicationDate
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public DynamicModel()
        {
        }

        public DynamicModel(DynamicContent sfContent)
        {
            if (sfContent != null)
            {
                this.Id = sfContent.Id;
                this.OriginalContentId = sfContent.OriginalContentId;
                this.Slug = sfContent.UrlName;
                this.DateCreated = sfContent.DateCreated;
                this.PublicationDate = sfContent.PublicationDate;
                this.LastModified = sfContent.LastModified;
                this.Status = sfContent.Status;
                this.Active = sfContent.Status == ContentLifecycleStatus.Live;
                this.Title = sfContent.GetTitle();
            }
        }

        public virtual DynamicContent ToSitefinityModel()
        {
            DynamicContent dataItem;
            DynamicModuleManager manager = DynamicModuleManager.GetManager();
            if (this.Id != Guid.Empty)
            {
                dataItem = manager.GetDataItem(TypeResolutionService.ResolveType(this.MappedType), this.Id);
                if (dataItem != null)
                {
                    DynamicContent dynamicContent = manager.Lifecycle.Edit(dataItem, null) as DynamicContent;
                    dataItem = manager.Lifecycle.CheckOut(dynamicContent, null) as DynamicContent;
                }
            }
            else
            {
                dataItem = manager.CreateDataItem(TypeResolutionService.ResolveType(this.MappedType));
                DateTime utcNow = DateTime.UtcNow;
                DateTime dateTime = utcNow;
                this.DateCreated = utcNow;
                dataItem.DateCreated = dateTime;
                if (!string.IsNullOrWhiteSpace(this.Slug))
                {
                    dataItem.UrlName = this.Slug;
                }
                else if (!string.IsNullOrWhiteSpace(this.Title))
                {
                    string str = ContentHelper.GenerateUrlName(this.Title);
                    string str1 = str;
                    this.Slug = str;
                    dataItem.UrlName = str1;
                }
            }
            dataItem.TrySetValue("Title", this.Title);
            return dataItem;
        }
    }
}