﻿using Case.Framework.Sitefinity.Utilities;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using System;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI;

namespace Case.Framework.Sitefinity.Models.Abstracts
{
    public abstract class BaseApplication<T> : IHttpModule
    where T : IHttpModule
    {
        protected BaseApplication()
        {
        }

        public virtual void Dispose()
        {
        }

        public static new Type GetType()
        {
            return typeof(T);
        }

        public virtual void Init(HttpApplication context)
        {
            try
            {
                context.PreRequestHandlerExecute += new EventHandler((object sender, EventArgs e) =>
                {
                  //  if (!PageHelper.IsMvcPage())
                //    {
                    Page currentHandler = HttpContext.Current.CurrentHandler as Page;

                    if (currentHandler != null)
                    {
                        BaseApplication<T> baseApplication = this;
                        currentHandler.PreInit += new EventHandler(baseApplication.OnPagePreInit);
                        BaseApplication<T> baseApplication1 = this;
                        currentHandler.Init += new EventHandler(baseApplication1.OnPageInit);
                        BaseApplication<T> baseApplication2 = this;
                        currentHandler.PreLoad += new EventHandler(baseApplication2.OnPagePreLoad);
                        BaseApplication<T> baseApplication3 = this;
                        currentHandler.Load += new EventHandler(baseApplication3.OnPageLoad);
                        BaseApplication<T> baseApplication4 = this;
                        currentHandler.PreRender += new EventHandler(baseApplication4.OnPagePreRender);

                        BaseApplication<T> baseApplication5 = this;
                        currentHandler.Error += new EventHandler(baseApplication5.OnPageError);
                    }
                //    }
                });
            }
            catch { }
        }

        protected virtual void OnPageError(object sender, EventArgs e)
        {
        }

        protected virtual void OnPageInit(object sender, EventArgs e)
        {
        }

        protected virtual void OnPageLoad(object sender, EventArgs e)
        {
        }

        protected virtual void OnPagePreInit(object sender, EventArgs e)
        {
        }

        protected virtual void OnPagePreLoad(object sender, EventArgs e)
        {
        }

        protected virtual void OnPagePreRender(object sender, EventArgs e)
        {
            
        }

        public static void RegisterStartup()
        {
            DynamicModuleUtility.RegisterModule(BaseApplication<T>.GetType());
        }
    }
}