﻿using System;
using System.Runtime.CompilerServices;

namespace Case.Framework.Sitefinity.Models
{
    public class ParentModel
    {
        public string Description
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public float Ordinal
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public ParentModel()
        {
        }
    }
}