﻿using Case.Framework.Sitefinity.Extensions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lists.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class ListItemModel : ContentModel
    {
       
        public bool Active
        {
            get;
            set;
        }

        public List<TaxonModel> Categories
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public float Ordinal
        {
            get;
            set;
        }

        public ParentModel Parent
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public List<TaxonModel> Tags
        {
            get;
            set;
        }
        

        public ListItemModel()
        {
            this.Categories = new List<TaxonModel>();
            this.Tags = new List<TaxonModel>();
        }

        public ListItemModel(ListItem sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                this.Content = sfContent.Content;
                this.Slug = sfContent.UrlName;
                this.Ordinal = sfContent.Ordinal;
                this.Status = sfContent.Status;
                this.Active = sfContent.Status == ContentLifecycleStatus.Live;
               
                if (sfContent.Parent != null)
                {
                    ParentModel parentModel = new ParentModel()
                    {
                        Id = sfContent.Parent.Id,
                        Title = sfContent.Parent.Title,
                        Description = sfContent.Parent.Description,
                        Slug = sfContent.Parent.UrlName
                    };
                    this.Parent = parentModel;
                }
                this.Categories = sfContent.GetTaxa("Category");
                this.Tags = sfContent.GetTaxa("Tags");
            }
        }
    }
}