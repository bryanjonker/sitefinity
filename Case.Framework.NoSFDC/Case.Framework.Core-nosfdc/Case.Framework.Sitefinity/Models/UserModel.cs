﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Claims;

namespace Case.Framework.Sitefinity.Models
{
    public class UserModel
    {
        public Guid Id
        {
            get;
            set;
        }

        public bool IsAuthenticated
        {
            get;
            set;
        }

        public bool IsBackendUser
        {
            get;
            set;
        }

        public bool IsUnrestricted
        {
            get;
            set;
        }

        public DateTime LastLoginDate
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public List<RoleModel> Roles
        {
            get;
            set;
        }

        public UserModel(ClaimsIdentityProxy sfContent)
        {
            if (sfContent != null)
            {
                this.Id = sfContent.UserId;
                this.Name = sfContent.Name;
                this.IsAuthenticated = sfContent.IsAuthenticated;
                this.IsBackendUser = sfContent.IsBackendUser;
                this.IsUnrestricted = sfContent.IsUnrestricted;
                this.LastLoginDate = sfContent.LastLoginDate;
                this.Roles = new List<RoleModel>();
                if (sfContent.Roles != null && sfContent.Roles.Count<RoleInfo>() > 0)
                {
                    sfContent.Roles.ToList<RoleInfo>().ForEach((RoleInfo r) => this.Roles.Add(new RoleModel(r)));
                }
            }
        }
    }
}