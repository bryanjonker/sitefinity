﻿using System;
using Telerik.Sitefinity.Libraries.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class VideoModel : MediaModel
    {
        public VideoModel()
        {
        }

        public VideoModel(Video sfMedia)
            : base(sfMedia)
        {
        }
    }
}