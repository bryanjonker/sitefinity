﻿using Case.Framework.Sitefinity.Models.Interfaces;
using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class ContentModel : IDataModel
    {
        public DateTime DateCreated
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public DateTime PublicationDate
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public ContentModel()
        {
        }

        public ContentModel(IContent sfContent)
        {
            if (sfContent != null)
            {
                this.Id = sfContent.Id;
                this.Title = sfContent.Title;
                this.PublicationDate = sfContent.PublicationDate;
                this.LastModified = sfContent.LastModified;
                this.DateCreated = sfContent.DateCreated;
            }
        }
    }
}