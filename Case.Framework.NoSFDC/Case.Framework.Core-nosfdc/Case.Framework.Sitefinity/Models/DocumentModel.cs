﻿using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Libraries.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class DocumentModel : MediaModel
    {
        public string Other
        {
            get;
            set;
        }

        public DocumentModel()
        {
        }

        public DocumentModel(Document sfMedia)
            : base(sfMedia)
        {
            if (sfMedia != null)
            {
                this.Other = sfMedia.Parts;
            }
        }
    }
}