﻿using Case.Framework.Sitefinity.Extensions;
using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class LibraryModel
    {
        public bool Active
        {
            get;
            set;
        }

        public DateTime DateCreated
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public long MaxItemSize
        {
            get;
            set;
        }

        public long MaxSize
        {
            get;
            set;
        }

        public DateTime PublicationDate
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public int ViewsCount
        {
            get;
            set;
        }

        public LibraryModel()
        {
        }

        public LibraryModel(Library sfContent)
        {
            if (sfContent != null)
            {
                this.Id = sfContent.Id;
                this.Title = sfContent.Title;
                this.Description = sfContent.Description;
                this.ViewsCount = sfContent.ViewsCount;
                this.MaxItemSize = sfContent.MaxItemSize;
                this.MaxSize = sfContent.MaxSize;
                this.Status = sfContent.Status;
                this.DateCreated = sfContent.DateCreated;
                this.PublicationDate = sfContent.PublicationDate;
                this.LastModified = sfContent.LastModified;
                this.Url = sfContent.GetFullUrl(sfContent.DefaultPageId);
                this.Slug = sfContent.UrlName;
                this.Active = sfContent.Status == ContentLifecycleStatus.Live;
            }
        }
    }
}