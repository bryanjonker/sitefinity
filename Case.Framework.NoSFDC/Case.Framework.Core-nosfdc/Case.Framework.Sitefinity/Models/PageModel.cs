﻿namespace Case.Framework.Sitefinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Case.Framework.Core.Utilities;

    using Telerik.Sitefinity.Data.Metadata;
    using Telerik.Sitefinity.GenericContent.Model;
    using Telerik.Sitefinity.Metadata.Model;
    using Telerik.Sitefinity.Pages.Model;
    using Telerik.Sitefinity.Utilities.TypeConverters;
    using Telerik.Sitefinity.Web;

    public class PageModel
    {
        public bool Active
        {
            get;
            set;
        }

        public bool Crawlable
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Framework
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public bool IsBackend
        {
            get;
            set;
        }

        public List<PageModel> Items
        {
            get;
            set;
        }

        public float Ordinal
        {
            get;
            set;
        }

        public PageModel Parent
        {
            get;
            set;
        }

        public string SafeName
        {
            get;
            set;
        }

        public bool ShowInNavigation
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Theme
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        /// <summary>
        /// Custom field values
        /// </summary>
        public Dictionary<string, object> CustomFieldValues { get; set; }

        public PageModel()
        {
            this.Items = new List<PageModel>();
        }

        public PageModel(SiteMapNode sfContent, bool includeParents = true, bool includeChildren = true, bool includeCustomFieldValues = false)
        {
            PageSiteNode pageSiteNode = sfContent as PageSiteNode;
            if (pageSiteNode != null)
            {
                this.Id = pageSiteNode.Id;
                this.Title = pageSiteNode.Title;
                this.Description = pageSiteNode.Description;
                this.Url = WebHelper.ResolveUrl(pageSiteNode.Url);
                this.Crawlable = pageSiteNode.Crawlable;
                this.IsBackend = pageSiteNode.IsBackend;
                this.ShowInNavigation = pageSiteNode.ShowInNavigation;
                this.SafeName = WebHelper.GenerateSafeName(pageSiteNode.UrlName, false);
                this.Ordinal = pageSiteNode.Ordinal;
                this.Status = pageSiteNode.Status;
                this.Theme = pageSiteNode.Theme;
                this.Framework = pageSiteNode.Framework.ToString();
                this.Slug = pageSiteNode.UrlName;
                this.Active = pageSiteNode.Status == ContentLifecycleStatus.Live;
                if (includeParents)
                {
                    this.Parent = new PageModel(sfContent.ParentNode, false, false);
                }
                if (includeChildren)
                {
                    this.Items = new List<PageModel>();
                    if (pageSiteNode.HasChildNodes)
                    {
                        foreach (PageSiteNode childNode in pageSiteNode.ChildNodes)
                        {
                            if (!childNode.ShowInNavigation || (childNode.NodeType != NodeType.Standard || childNode.Status != ContentLifecycleStatus.Live) && (childNode.NodeType == NodeType.Standard || childNode.Status != ContentLifecycleStatus.Master))
                            {
                                continue;
                            }
                            this.Items.Add(new PageModel(childNode, false, true, true));
                        }
                    }
                }

                if (includeCustomFieldValues)
                {
                    var fields = GetMetaFieldsForType(typeof(PageNode).FullName);
                    if (fields.Count() > 0)
                    {
                        var customFields = new Dictionary<string, object>();
                        foreach (var field in fields)
                        {
                            customFields.Add(field.FieldName, pageSiteNode.GetCustomFieldValue(field.FieldName));
                        }
                        this.CustomFieldValues = customFields;
                    }
                }
            }
        }

        private static IList<MetaField> GetMetaFieldsForType(string type)
        {
            var existingType = TypeResolutionService.ResolveType(type);
            var existingClassName = existingType.Name;
            var existingNamespace = existingType.Namespace;
            var mgr = MetadataManager.GetManager();

            var types = mgr.GetMetaTypes().FirstOrDefault(dt => dt.ClassName == existingClassName && dt.Namespace == existingNamespace);
            var fields = types != null ? types.Fields : new List<MetaField>();

            return fields;
        }
    }
}