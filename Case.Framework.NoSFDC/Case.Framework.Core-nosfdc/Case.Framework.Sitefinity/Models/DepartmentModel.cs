﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class DepartmentModel
    {
        public int Count
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string FullUrl
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public float Ordinal
        {
            get;
            set;
        }

        public ParentModel Parent
        {
            get;
            set;
        }

        public bool RenderAsLink
        {
            get;
            set;
        }

        public bool ShowInNavigation
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public List<DepartmentModel> Subtaxa
        {
            get;
            set;
        }

        public string TaxonName
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public DepartmentModel()
        {
            this.Subtaxa = new List<DepartmentModel>();
        }

        public DepartmentModel(HierarchicalTaxon sfContent)
        {
            TaxonomyManager manager = TaxonomyManager.GetManager();
            this.Id = sfContent.Id;
            this.Title = sfContent.Title;
            this.Description = sfContent.Description;
            this.FullUrl = sfContent.FullUrl;
            this.Ordinal = sfContent.Ordinal;
            this.RenderAsLink = sfContent.RenderAsLink;
            this.ShowInNavigation = sfContent.ShowInNavigation;
            this.TaxonName = sfContent.Taxonomy.TaxonName;
            this.Slug = sfContent.UrlName;
            this.LastModified = sfContent.LastModified;
            if (sfContent.Parent != null)
            {
                ParentModel parentModel = new ParentModel()
                {
                    Id = sfContent.Parent.Id,
                    Title = sfContent.Parent.Title,
                    Description = sfContent.Parent.Description,
                    Ordinal = sfContent.Parent.Ordinal,
                    Slug = sfContent.Parent.UrlName
                };
                this.Parent = parentModel;
            }
            this.Subtaxa = new List<DepartmentModel>();
            sfContent.Subtaxa.ToList<HierarchicalTaxon>().ForEach((HierarchicalTaxon c) => this.Subtaxa.Add(new DepartmentModel(c)));
            this.Count = (int)manager.GetTaxonItemsCount(sfContent.Id, ContentLifecycleStatus.Live);
        }
    }
}