﻿using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Linq;
using Telerik.Sitefinity.Ecommerce.Catalog.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace Case.Framework.Sitefinity.Models
{
    public class ImageModel : MediaModel
    {
        public Guid AlbumId
        {
            get;
            set;
        }

        public string AlbumTitle
        {
            get;
            set;
        }

        public string AlternativeText
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public string Link
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }

        public ImageModel()
        {
        }

        public ImageModel(Image sfContent)
            : base(sfContent)
        {
            if (sfContent == null) return;
            if (sfContent.FolderId.HasValue)
                AlbumId = sfContent.FolderId.Value;
            else AlbumId = sfContent.Album.Id;
            AlbumTitle = sfContent.Album.Title;
            Width = sfContent.Width;
            Height = sfContent.Height;
            AlternativeText = sfContent.AlternativeText;
            if (sfContent.DoesFieldExist("Link"))
            {
                Link = sfContent.GetValue<string>("Link");
            }
        }

        public ImageModel(ProductImage sfContent)
        {
            if (sfContent != null)
            {
                base.Title = sfContent.Title;
                this.Width = sfContent.Width;
                this.Height = sfContent.Height;
                base.Url = sfContent.Url;
                this.AlternativeText = sfContent.AlternativeText;
                base.Ordinal = (float)sfContent.Ordinal;
            }
        }

        public virtual Image ToSitefinityModel()
        {
            LibrariesManager manager = LibrariesManager.GetManager();
            Image image = null;
            Album album = null;
            if (base.Id != Guid.Empty)
            {
                image = manager.GetImage(base.Id);
                if (image != null)
                {
                    Image image1 = manager.Lifecycle.Edit(image, null) as Image;
                    image = manager.Lifecycle.CheckOut(image1, null) as Image;
                }
            }
            else
            {
                image = manager.CreateImage();
                DateTime utcNow = DateTime.UtcNow;
                DateTime dateTime = utcNow;
                base.DateCreated = utcNow;
                image.DateCreated = dateTime;
                image.LastModified = DateTime.UtcNow;
                if (!string.IsNullOrWhiteSpace(base.Slug))
                {
                    image.UrlName = base.Slug;
                }
                else if (!string.IsNullOrWhiteSpace(base.Title))
                {
                    string str = ContentHelper.GenerateUrlName(base.Title);
                    string str1 = str;
                    base.Slug = str;
                    image.UrlName = str1;
                }
            }
            image.Title = base.Title;
            image.Description = base.Description;
            image.AlternativeText = this.AlternativeText;
            image.Author = base.Author;
            image.Height = this.Height;
            image.Width = this.Width;
            image.Ordinal = base.Ordinal;
            if (image.DoesFieldExist("Link"))
            {
                this.Link = image.GetValue<string>("Link");
            }
            if (base.Parent != null)
            {
                if (base.Parent.Id != Guid.Empty)
                {
                    album = manager.GetAlbum(base.Parent.Id);
                }
                else if (!string.IsNullOrWhiteSpace(base.Parent.Slug))
                {
                    album = manager.GetAlbums().FirstOrDefault<Album>((Album a) => a.UrlName == (Lstring)this.Parent.Slug);
                }
                if (album == null && !string.IsNullOrWhiteSpace(base.Parent.Title))
                {
                    album = (base.Parent.Id != Guid.Empty ? manager.CreateAlbum(base.Parent.Id) : manager.CreateAlbum());
                    album.Title = base.Parent.Title;
                    album.Description = base.Parent.Description;
                    album.DateCreated = DateTime.UtcNow;
                    album.PublicationDate = DateTime.UtcNow;
                    album.LastModified = DateTime.UtcNow;
                    if (!string.IsNullOrWhiteSpace(base.Parent.Slug))
                    {
                        album.UrlName = base.Parent.Slug;
                    }
                    else if (!string.IsNullOrWhiteSpace(base.Parent.Title))
                    {
                        LibraryModel parent = base.Parent;
                        string str2 = ContentHelper.GenerateUrlName(base.Parent.Title);
                        string str3 = str2;
                        parent.Slug = str2;
                        album.UrlName = str3;
                    }
                    manager.SaveChanges();
                }
                image.Parent = album;
            }
            image.SetTaxa("Category", base.Categories);
            image.SetTaxa("Tags", base.Tags);
            return image;
        }
    }
}