﻿using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class TaxonModel
    {
        public string Classification
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public bool IsHierarchy
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public float Ordinal
        {
            get;
            set;
        }

        public ParentModel Parent
        {
            get;
            set;
        }

        public bool ShowInNavigation
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public TaxonModel()
        {
        }

        public TaxonModel(ITaxon sfContent)
        {
            if (sfContent != null)
            {
                this.Id = sfContent.Id;
                this.Title = sfContent.Title;
                this.Description = sfContent.Description;
                this.ShowInNavigation = sfContent.ShowInNavigation;
                this.Slug = sfContent.UrlName;
                this.Ordinal = sfContent.Ordinal;
                this.LastModified = sfContent.LastModified;
                this.Classification = sfContent.Taxonomy.Name;
                this.IsHierarchy = sfContent is HierarchicalTaxon;
                if (this.IsHierarchy)
                {
                    HierarchicalTaxon hierarchicalTaxon = sfContent as HierarchicalTaxon;
                    if (hierarchicalTaxon.Parent != null)
                    {
                        ParentModel parentModel = new ParentModel()
                        {
                            Id = hierarchicalTaxon.Parent.Id,
                            Title = hierarchicalTaxon.Parent.Title,
                            Description = hierarchicalTaxon.Parent.Description,
                            Ordinal = hierarchicalTaxon.Parent.Ordinal,
                            Slug = hierarchicalTaxon.Parent.UrlName
                        };
                        this.Parent = parentModel;
                    }
                }
            }
        }
    }
}