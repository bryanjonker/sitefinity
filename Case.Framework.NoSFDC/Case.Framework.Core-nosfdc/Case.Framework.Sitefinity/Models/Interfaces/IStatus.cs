﻿using System;
using Telerik.Sitefinity.GenericContent.Model;

namespace Case.Framework.Sitefinity.Models.Interfaces
{
    public interface IStatus
    {
        ContentLifecycleStatus Status
        {
            get;
            set;
        }
    }
}