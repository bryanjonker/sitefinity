﻿using Case.Framework.Sitefinity.Models;
using System;

namespace Case.Framework.Sitefinity.Models.Interfaces
{
    public interface IHierarchy
    {
        DynamicModel Parent
        {
            get;
            set;
        }
    }
}