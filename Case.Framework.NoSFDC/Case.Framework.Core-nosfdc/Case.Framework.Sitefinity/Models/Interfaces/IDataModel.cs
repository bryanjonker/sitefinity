﻿using System;

namespace Case.Framework.Sitefinity.Models.Interfaces
{
    public interface IDataModel
    {
        DateTime DateCreated
        {
            get;
            set;
        }

        Guid Id
        {
            get;
            set;
        }

        DateTime LastModified
        {
            get;
            set;
        }

        DateTime PublicationDate
        {
            get;
            set;
        }
    }
}