﻿using Case.Framework.Sitefinity.Extensions;
using System;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Events.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class CalendarModel
    {
        public string Color
        {
            get;
            set;
        }

        public DateTime DateCreated
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public DateTime PublicationDate
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public CalendarModel()
        {
        }

        public CalendarModel(Calendar sfContent)
        {
            if (sfContent != null)
            {
                this.Id = sfContent.Id;
                this.Title = sfContent.Title;
                this.PublicationDate = sfContent.PublicationDate;
                this.LastModified = sfContent.LastModified;
                this.DateCreated = sfContent.DateCreated;
                this.Description = sfContent.Description;
                this.Color = sfContent.Color;
                this.Url = sfContent.GetFullUrl(null);
                this.Slug = sfContent.UrlName;
            }
        }
    }
}