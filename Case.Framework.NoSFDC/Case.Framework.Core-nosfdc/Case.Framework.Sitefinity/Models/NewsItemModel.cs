﻿using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Modules.News;
using Telerik.Sitefinity.News.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class NewsItemModel : ContentModel
    {
        public bool Active
        {
            get;
            set;
        }

        public string Author
        {
            get;
            set;
        }

        public List<TaxonModel> Categories
        {
            get;
            set;
        }

        public int CommentsCount
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public string SourceName
        {
            get;
            set;
        }

        public string SourceSite
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Summary
        {
            get;
            set;
        }

        public List<TaxonModel> Tags
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public NewsItemModel()
        {
            this.Categories = new List<TaxonModel>();
            this.Tags = new List<TaxonModel>();
        }

        public NewsItemModel(NewsItem sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                this.Content = sfContent.Content;
                this.Summary = sfContent.Summary;
                this.Author = sfContent.Author;
                this.SourceName = sfContent.SourceName;
                this.SourceSite = sfContent.SourceSite;
                this.Url = sfContent.GetFullUrl(sfContent.DefaultPageId);
                this.Slug = sfContent.UrlName;
                this.Status = sfContent.Status;
                this.Active = true;
                this.Categories = sfContent.GetTaxa("Category");
                this.Tags = sfContent.GetTaxa("Tags");
                //if (sfContent.DoesFieldExist("Image"))
                //{
                //    this.Image = sfContent.GetValue<string>("Image");
                //}
            }
        }

        public NewsItem ToSitefinityModel()
        {
            NewsManager manager = NewsManager.GetManager();
            NewsItem newsItem = null;
            if (base.Id != Guid.Empty)
            {
                newsItem = manager.GetNewsItem(base.Id);
                if (newsItem != null)
                {
                    NewsItem newsItem1 = manager.Lifecycle.Edit(newsItem, null) as NewsItem;
                    newsItem = manager.Lifecycle.CheckOut(newsItem1, null) as NewsItem;
                }
            }
            if (newsItem == null)
            {
                newsItem = manager.CreateNewsItem();
                DateTime utcNow = DateTime.UtcNow;
                DateTime dateTime = utcNow;
                base.DateCreated = utcNow;
                newsItem.DateCreated = dateTime;
                if (!string.IsNullOrWhiteSpace(this.Slug))
                {
                    newsItem.UrlName = this.Slug;
                }
                else if (!string.IsNullOrWhiteSpace(base.Title))
                {
                    string str = ContentHelper.GenerateUrlName(base.Title);
                    string str1 = str;
                    this.Slug = str;
                    newsItem.UrlName = str1;
                }
            }
            newsItem.Title = base.Title;
            newsItem.Content = this.Content;
            newsItem.Summary = this.Summary;
            newsItem.Author = this.Author;
            newsItem.SourceName = this.SourceName;
            newsItem.SourceSite = this.SourceSite;
            newsItem.SetTaxa("Category", this.Categories);
            newsItem.SetTaxa("Tags", this.Tags);
            return newsItem;
        }
    }
}