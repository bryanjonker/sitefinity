﻿using Case.Framework.Sitefinity.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Blogs.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Blogs;

namespace Case.Framework.Sitefinity.Models
{
    public class BlogModel : ContentModel
    {
        public bool Active
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public int PostsCount
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public BlogModel()
        {
        }

        public BlogModel(Blog sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                this.Description = sfContent.Description;
                this.Status = sfContent.Status;
                this.Url = sfContent.GetFullUrl(sfContent.DefaultPageId);
                this.Slug = sfContent.UrlName;
                this.Active = sfContent.Status == ContentLifecycleStatus.Live;
                this.PostsCount = BlogsManager.GetManager().GetBlogPosts().Count<BlogPost>((BlogPost c) => (c.Parent.Id == sfContent.Id) && (int)c.Status == 2);
                if (sfContent.DoesFieldExist("Image"))
                {
                    this.Image = sfContent.GetValue<string>("Image");
                }
            }
        }
    }
}