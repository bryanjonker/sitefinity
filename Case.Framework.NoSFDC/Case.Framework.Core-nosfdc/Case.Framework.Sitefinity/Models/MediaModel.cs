﻿using Case.Framework.Core.Utilities;
using Case.Framework.Sitefinity.Extensions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;

namespace Case.Framework.Sitefinity.Models
{
    public class MediaModel : ContentModel
    {
        public bool Active
        {
            get;
            set;
        }

        public string Author
        {
            get;
            set;
        }

        public List<TaxonModel> Categories
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Extension
        {
            get;
            set;
        }

        public string File
        {
            get;
            set;
        }

        public bool IsPrimary
        {
            get;
            set;
        }

        public string MimeType
        {
            get;
            set;
        }

        public float Ordinal
        {
            get;
            set;
        }

        public LibraryModel Parent
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public ContentLifecycleStatus Status
        {
            get;
            set;
        }

        public List<TaxonModel> Tags
        {
            get;
            set;
        }

        public long TotalSize
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public int ViewsCount
        {
            get;
            set;
        }

        public MediaModel()
        {
            this.Categories = new List<TaxonModel>();
            this.Tags = new List<TaxonModel>();
        }

        public MediaModel(MediaContent sfContent)
        {
            if (sfContent != null)
            {
                base.Id = sfContent.Id;
                base.Title = sfContent.Title;
                this.Description = sfContent.Description;
                this.Author = sfContent.Author;
                this.Ordinal = sfContent.Ordinal;
                this.Url = WebHelper.ResolveUrl(string.Concat("~", sfContent.Urls[0].Url));
                this.Slug = sfContent.UrlName;
                this.Extension = sfContent.Extension;
                this.MimeType = sfContent.MimeType;
                this.TotalSize = sfContent.TotalSize;
                this.ViewsCount = sfContent.ViewsCount;
                base.DateCreated = sfContent.DateCreated;
                base.PublicationDate = sfContent.PublicationDate;
                this.Parent = new LibraryModel(sfContent.Parent);
                this.Status = sfContent.Status;
                this.Active = sfContent.Status == ContentLifecycleStatus.Live;
                this.Categories = sfContent.GetTaxa("Category");
                this.Tags = sfContent.GetTaxa("Tags");
            }
        }
    }
}