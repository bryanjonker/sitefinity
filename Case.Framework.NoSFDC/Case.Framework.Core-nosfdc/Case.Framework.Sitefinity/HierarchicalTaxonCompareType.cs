﻿using System;

namespace Case.Framework.Sitefinity
{
    public enum HierarchicalTaxonCompareType
    {
        Equals,
        StartsWith,
        EndsWith,
        Contains
    }
}