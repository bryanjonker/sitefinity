﻿using Case.Framework.Sitefinity.Configuration;
using Case.Framework.Sitefinity.Models.Abstracts;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Web.Hosting;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Services;

namespace Case.Framework.Sitefinity
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class Application : BaseApplication<Application>
    {
        public Application()
        {
        }

        protected static void OnBootstrapperApplicationStart(object sender, EventArgs e)
        {
        }

        protected static void OnBootstrapperInitialized(object sender, ExecutedEventArgs e)
        {
            Config.RegisterSection<CASEFrameworkConfig>();

        }

        protected static void OnBootstrapperInitializing(object sender, ExecutingEventArgs e)
        {
            if (e.CommandName == "RegisterRoutes")
            {
                ConfigHelper.RegisterVirtualPath("~/CaseSF/*");
            }
        }

        protected override void OnPagePreRender(object sender, EventArgs e)
        {
            
            PageHelper.RegisterClientSideStartup(null, null, null, null);
            base.OnPagePreRender(sender, e);
        }

        public static void PreInit()
        {
            BaseApplication<Application>.RegisterStartup();
            SystemManager.ApplicationStart += new EventHandler<EventArgs>(Application.OnBootstrapperApplicationStart);
            Bootstrapper.Initializing += new EventHandler<ExecutingEventArgs>(Application.OnBootstrapperInitializing);
            Bootstrapper.Initialized += new EventHandler<ExecutedEventArgs>(Application.OnBootstrapperInitialized);
        }
    }
}