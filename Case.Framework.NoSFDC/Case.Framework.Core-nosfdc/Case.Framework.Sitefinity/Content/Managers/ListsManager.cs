﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using Telerik.Sitefinity.Lists.Model;
using Telerik.Sitefinity.Modules.Lists;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class ListsManager : BaseChildManager<Telerik.Sitefinity.Modules.Lists.ListsManager, ListItem, ListsManager, ListItemModel>
    {
        public ListsManager()
        {
        }

        protected override ListItemModel CreateInstance(ListItem sfContent)
        {
            return new ListItemModel(sfContent);
        }

        protected override IQueryable<ListItem> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetListItems();
        }

        protected override ListItem Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetListItem(id);
        }

        protected override IOrderedQueryable<ListItem> SortResults(IQueryable<ListItem> sfContents)
        {
            return
                from i in sfContents
                orderby i.Ordinal
                select i;
        }
    }
}