﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class DepartmentsManager : BaseSingletonManager<TaxonomyManager, DepartmentsManager>
    {
        public DepartmentsManager()
        {
        }

        public virtual IEnumerable<DepartmentModel> GetAll(string providerName = null, int take = 0, int skip = 0)
        {
            HierarchicalTaxonomy hierarchicalTaxonomy = base.GetManager(providerName).GetTaxonomies<HierarchicalTaxonomy>().First<HierarchicalTaxonomy>((HierarchicalTaxonomy t) => t.Name == "Departments");
            IEnumerable<Taxon> taxa =
                from t in hierarchicalTaxonomy.Taxa
                where t.Parent == null
                select t;
            if (skip > 0)
            {
                taxa = taxa.Skip<Taxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<Taxon>(take);
            }
            return
                from i in taxa
                select new DepartmentModel(i as HierarchicalTaxon);
        }

        public virtual DepartmentModel GetById(Guid id, string providerName = null)
        {
            HierarchicalTaxon taxon = base.GetManager(providerName).GetTaxon<HierarchicalTaxon>(id);
            if (taxon == null)
            {
                return null;
            }
            return new DepartmentModel(taxon);
        }

        public virtual DepartmentModel GetByName(string value, string providerName = null)
        {
            IQueryable<HierarchicalTaxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<HierarchicalTaxon>()
                where p.UrlName.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (!taxa.Any<HierarchicalTaxon>())
            {
                return null;
            }
            return new DepartmentModel(taxa.SingleOrDefault<HierarchicalTaxon>());
        }

        public virtual IEnumerable<DepartmentModel> GetByParent(string value, string providerName = null, Expression<Func<HierarchicalTaxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<HierarchicalTaxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<HierarchicalTaxon>()
                where p.Parent.Name.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (filter != null)
            {
                taxa = taxa.Where<HierarchicalTaxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<HierarchicalTaxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<HierarchicalTaxon>(take);
            }
            return
                from i in taxa
                select new DepartmentModel(i);
        }

        public virtual IEnumerable<DepartmentModel> GetByParentId(Guid id, string providerName = null, Expression<Func<HierarchicalTaxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<HierarchicalTaxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<HierarchicalTaxon>()
                where p.Parent.Id == id
                select p;
            if (filter != null)
            {
                taxa = taxa.Where<HierarchicalTaxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<HierarchicalTaxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<HierarchicalTaxon>(take);
            }
            return
                from i in taxa
                select new DepartmentModel(i);
        }

        public virtual IEnumerable<DepartmentModel> GetByParentTitle(string value, string providerName = null, Expression<Func<HierarchicalTaxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<HierarchicalTaxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<HierarchicalTaxon>()
                where p.Parent.Title.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (filter != null)
            {
                taxa = taxa.Where<HierarchicalTaxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<HierarchicalTaxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<HierarchicalTaxon>(take);
            }
            return
                from i in taxa
                select new DepartmentModel(i);
        }

        public virtual DepartmentModel GetByTitle(string value, string providerName = null)
        {
            IQueryable<HierarchicalTaxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<HierarchicalTaxon>()
                where p.Title.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (!taxa.Any<HierarchicalTaxon>())
            {
                return null;
            }
            return new DepartmentModel(taxa.FirstOrDefault<HierarchicalTaxon>());
        }
    }
}