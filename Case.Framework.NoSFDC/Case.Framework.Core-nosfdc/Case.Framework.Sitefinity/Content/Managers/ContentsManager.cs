﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.GenericContent;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class ContentsManager : BaseSingletonManager<ContentManager, ContentsManager>
    {
        public ContentsManager()
        {
        }

        public virtual string GetById(Guid id, string providerName = null)
        {
            ContentItem contentItem = base.GetManager(providerName).GetItems<ContentItem>().SingleOrDefault<ContentItem>((ContentItem i) => i.Id == id);
            if (contentItem == null)
            {
                return string.Empty;
            }
            return contentItem.Content.Value;
        }

        public virtual string GetByName(string value, string providerName = null)
        {
            ContentItem contentItem = base.GetManager(providerName).GetItems<ContentItem>().SingleOrDefault<ContentItem>((ContentItem i) => i.UrlName.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)i.Status == 2);
            if (contentItem == null)
            {
                return string.Empty;
            }
            return contentItem.Content.Value;
        }

        public virtual string GetByTitle(string value, string providerName = null)
        {
            ContentItem contentItem = base.GetManager(providerName).GetItems<ContentItem>().FirstOrDefault<ContentItem>((ContentItem i) => i.Title.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)i.Status == 2);
            if (contentItem == null)
            {
                return string.Empty;
            }
            return contentItem.Content.Value;
        }
    }
}