﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseContentManager<TManager, TContent, TBaseManager, TContentModel> : BaseFieldManager<TManager, TContent, TBaseManager, TContentModel>, IContentManager<TContentModel, TContent>, IDataManager<TContentModel, TContent>
        where TManager : class, IManager
        where TContent : IContent, IDynamicFieldsContainer, ILifecycleDataItem, ILocatable, ILocalizable
        where TBaseManager : BaseManager<TManager>, new()
        where TContentModel : ContentModel, new()
    {
        public override IEnumerable<TContentModel> GetAll(string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            var tContents =
                from i in Get(providerName)
                where (int)((ILifecycleDataItem)i).Status == 2  && ((IContent)i).Visible
                select i;
            if (filter != null)
            {
                tContents = tContents.Where(filter);
            }
            if (skip > 0)
            {
                tContents = tContents.Skip(skip);
            }
            if (take > 0)
            {
                tContents = tContents.Take(take);
            }
             return tContents.Select(CreateInstance);
            
            //from i in tContents
            //    select ((BaseContentManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }

        public virtual TContentModel GetByName(string value, string providerName = null)
        {
            IQueryable<TContent> tContents =
                from i in this.Get(providerName)
                where ((ILocatable)i).UrlName.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)((ILifecycleDataItem)i).Status == 2 && ((IContent)i).Visible
                select i;
            return !tContents.Any() ? default(TContentModel) : tContents.Select(CreateInstance).FirstOrDefault();
        }

        public virtual TContentModel GetByTitle(string value, string providerName = null)
        {
            IQueryable<TContent> tContents =
                from i in Get(providerName)
                where ((IContent)i).Title.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)((ILifecycleDataItem)i).Status == 2
                select i;
            return !tContents.Any() ? default(TContentModel) : tContents.Select(CreateInstance).FirstOrDefault();
        }

        public virtual IEnumerable<TContentModel> GetRecent(string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<TContent> publicationDate =
                from s in Get(providerName)
                where (int)((ILifecycleDataItem)s).Status == 2 && ((IContent)s).Visible
                select s;
            if (filter != null)
            {
                publicationDate = publicationDate.Where(filter);
            }
            publicationDate =
                from i in publicationDate
                orderby ((IScheduleable)i).PublicationDate descending
                select i;
            if (skip > 0)
            {
                publicationDate = publicationDate.Skip(skip);
            }
            if (take > 0)
            {
                publicationDate = publicationDate.Take(take);
            }
            return publicationDate.Select(CreateInstance);
            //    from i in publicationDate
              //  select ((BaseContentManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }

        public virtual IEnumerable<TContentModel> Search(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<TContent> tContents =
                from i in Get(providerName)
                where (((IContent)i).Title.ToString().ToLower().Contains(value.ToLower()) ||
                ((IContent)i).Description.ToString().ToLower().Contains(value.ToLower())) && (int)((ILifecycleDataItem)i).Status == 2 && ((IContent)i).Visible
                select i;
            if (filter != null)
            {
                tContents = tContents.Where(filter);
            }
            if (skip > 0)
            {
                tContents = tContents.Skip(skip);
            }
            if (take > 0)
            {
                tContents = tContents.Take(take);
            }
            return tContents.Select(CreateInstance);
            //from i in tContents
            //select ((BaseContentManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }
    }
}