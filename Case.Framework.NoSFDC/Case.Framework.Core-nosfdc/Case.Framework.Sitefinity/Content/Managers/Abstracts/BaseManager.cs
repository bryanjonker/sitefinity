﻿using System;
using System.Collections.Generic;
using Telerik.Sitefinity.Data;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseManager<TManager>
    where TManager : class, IManager
    {
        private const string DEFAULT_PROVIDER_NAME = "Default";

        private Type _managerType;

        private IDictionary<string, TManager> _managers;

        protected BaseManager()
        {
        }

        public TManager GetManager(string providerName = null)
        {
            if (this._managerType == null)
            {
                this._managerType = typeof(TManager);
            }
            if (this._managers == null)
            {
                Dictionary<string, TManager> strs = new Dictionary<string, TManager>()
                {
                    { "Default", (TManager)(ManagerBase.GetManager(this._managerType) as TManager) }
                };
                this._managers = strs;
            }
            if (string.IsNullOrWhiteSpace(providerName))
            {
                return this._managers["Default"];
            }
            if (!this._managers.ContainsKey(providerName))
            {
                this._managers.Add(providerName, (TManager)(ManagerBase.GetManager(this._managerType, providerName) as TManager));
            }
            return this._managers[providerName];
        }
    }
}