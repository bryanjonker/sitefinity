﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseMediaManager<TManager, TContent, TBaseManager, TContentModel> : BaseChildManager<TManager, TContent, TBaseManager, TContentModel>, IMediaManager<TContentModel, TContent>, IChildManager<TContentModel, TContent>, IContentManager<TContentModel, TContent>, IDataManager<TContentModel, TContent>
        where TManager : class, IManager
        where TContent : MediaContent
        where TBaseManager : BaseManager<TManager>, new()
        where TContentModel : MediaModel, new()
    {
        protected BaseMediaManager()
        {
        }

        public virtual IEnumerable<TContentModel> GetByExtension(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<TContent> tContents =
                from s in this.Get(providerName)
                where s.Extension.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)s.Status == 2
                select s;
            if (filter != null)
            {
                tContents = tContents.Where<TContent>(filter);
            }
            tContents = this.SortResults(tContents);
            if (skip > 0)
            {
                tContents = tContents.Skip<TContent>(skip);
            }
            if (take > 0)
            {
                tContents = tContents.Take<TContent>(take);
            }
            return
                from i in tContents
                select ((BaseMediaManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }

        protected override IOrderedQueryable<TContent> SortResults(IQueryable<TContent> sfContents)
        {
            return
                from i in sfContents
                orderby i.Ordinal
                select i;
        }
    }
}