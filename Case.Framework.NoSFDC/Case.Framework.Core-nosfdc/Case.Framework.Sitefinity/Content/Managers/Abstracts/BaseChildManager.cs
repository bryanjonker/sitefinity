﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseChildManager<TManager, TContent, TBaseManager, TContentModel> : BaseContentManager<TManager, TContent, TBaseManager, TContentModel>, IChildManager<TContentModel, TContent>, IContentManager<TContentModel, TContent>, IDataManager<TContentModel, TContent>
        where TManager : class, IManager
        where TContent : IContent, IDynamicFieldsContainer, ILifecycleDataItem, ILocatable, ILocalizable, IHasParent
        where TBaseManager : BaseManager<TManager>, new()
        where TContentModel : ContentModel, new()
    {
        public virtual IEnumerable<TContentModel> GetByParent(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<TContent> tContents =
                from s in this.Get(providerName)
                where ((IHasParent)s).Parent.UrlName.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)((ILifecycleDataItem)s).Status == 2 && ((IContent)s).Visible
                select s;
            if (filter != null)
            {
                tContents = tContents.Where(filter);
            }
            tContents = this.SortResults(tContents);
            if (skip > 0)
            {
                tContents = tContents.Skip(skip);
            }
            if (take > 0)
            {
                tContents = tContents.Take(take);
            }
            return tContents.Select(CreateInstance);
                //from i in tContents
                //select ((BaseChildManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }

        public virtual IEnumerable<TContentModel> GetByParentId(Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            var tContents =
                from s in Get(providerName)
                where (((IHasParent)s).Parent.Id == id) && (int)((ILifecycleDataItem)s).Status == 2 && ((IContent)s).Visible
                select s;
            if (filter != null)
            {
                tContents = tContents.Where(filter);
            }
            tContents = this.SortResults(tContents);
            if (skip > 0)
            {
                tContents = tContents.Skip(skip);
            }
            if (take > 0)
            {
                tContents = tContents.Take(take);
            }
            return tContents.Select(CreateInstance);
                //from i in tContents
                //select ((BaseChildManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }

        public virtual IEnumerable<TContentModel> GetByParentTitle(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<TContent> tContents =
                from s in this.Get(providerName)
                where ((IHasParent)s).Parent.Title.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)((ILifecycleDataItem)s).Status == 2 && ((IContent)s).Visible
                select s;
            if (filter != null)
            {
                tContents = tContents.Where(filter);
            }
            tContents = this.SortResults(tContents);
            if (skip > 0)
            {
                tContents = tContents.Skip(skip);
            }
            if (take > 0)
            {
                tContents = tContents.Take(take);
            }   
            return tContents.Select(CreateInstance);
                //from i in tContents
                //select ((BaseChildManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(i);
        }

        protected virtual IOrderedQueryable<TContent> SortResults(IQueryable<TContent> sfContents)
        {
            return
                from i in sfContents
                orderby ((IScheduleable)i).PublicationDate descending
                select i;
        }
    }
}