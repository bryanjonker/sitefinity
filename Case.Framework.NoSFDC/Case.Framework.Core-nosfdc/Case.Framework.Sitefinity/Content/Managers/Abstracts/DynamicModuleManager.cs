﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class DynamicModuleManager<TBaseDynamicModuleManager, TDynamicModel> : BaseFieldManager<DynamicModuleManager, DynamicContent, TBaseDynamicModuleManager, TDynamicModel>, IDynamicManager<TDynamicModel>
        where TBaseDynamicModuleManager : BaseManager<DynamicModuleManager>, new()
        where TDynamicModel : DynamicModel, new()
    {
        protected DynamicModuleManager()
        {
        }

        public virtual TDynamicModel Create(TDynamicModel value, string providerName = null)
        {
            TDynamicModel tDynamicModel;
            using (ElevatedModeRegion elevatedModeRegion = new ElevatedModeRegion(base.GetManager(providerName)))
            {
                try
                {
                    DynamicContent sitefinityModel = value.ToSitefinityModel();
                    DynamicModel parent = null;
                    if ((object)value is IHierarchy)
                    {
                        parent = ((object)value as IHierarchy).Parent;
                    }
                    if (!base.GetManager(providerName).PublishAndSave(sitefinityModel, parent))
                    {
                        tDynamicModel = default(TDynamicModel);
                    }
                    else
                    {
                        value.Id = sitefinityModel.Id;
                        tDynamicModel = value;
                    }
                }
                catch 
                {
                    tDynamicModel = default(TDynamicModel);
                }
            }
            return tDynamicModel;
        }

        public virtual bool DoesTypeExist()
        {
            try
            {
                TypeResolutionService.ResolveType(Activator.CreateInstance<TDynamicModel>().MappedType);
                return true;
            }
            catch 
            {
                return false;
            }
        }

        protected override IQueryable<DynamicContent> Get(string providerName = null)
        {
            return
                from c in base.GetManager(providerName).GetDataItems(this.GetDynamicType())
                where (int)c.Status == 2
                select c;
        }

        protected override DynamicContent Get(Guid id, string providerName = null)
        {
            return this.Get(id, false, providerName);
        }

        protected virtual DynamicContent Get(Guid id, bool checkout, string providerName = null)
        {
            if (id == Guid.Empty)
            {
                return null;
            }
            DynamicContent dataItem = base.GetManager(providerName).GetDataItem(this.GetDynamicType(), id);
            if (!checkout)
            {
                return dataItem;
            }
            if (dataItem == null)
            {
                return null;
            }
            DynamicContent dynamicContent = base.GetManager(providerName).Lifecycle.Edit(dataItem) as DynamicContent;
            return this.GetManager(providerName).Lifecycle.CheckOut(dynamicContent) as DynamicContent;
        }

        public override IEnumerable<TDynamicModel> GetAll(string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<DynamicContent> dynamicContents = this.Get(providerName);
            if (filter != null)
            {
                dynamicContents = dynamicContents.Where(filter);
            }
            if (skip > 0)
            {
                dynamicContents = dynamicContents.Skip<DynamicContent>(skip);
            }
            if (take > 0)
            {
                dynamicContents = dynamicContents.Take<DynamicContent>(take);
            }

            //Don't cast since it's not necessary; The element was not having the Provider populated so GetReferencedItems would return nothing
            return dynamicContents.Select(CreateInstance);
            //return
            //    from i in dynamicContents
            //    select ((DynamicModuleManager<TBaseDynamicModuleManager, TDynamicModel>)this).CreateInstance(i);
        }

        public virtual TDynamicModel GetByName(string value, string providerName = null)
        {
            DynamicContent dynamicContent = this.Get(providerName).FirstOrDefault<DynamicContent>((DynamicContent i) => i.UrlName == (Lstring)value.ToLower());
            if (dynamicContent == null)
            {
                return default(TDynamicModel);
            }
            return this.CreateInstance(dynamicContent);
        }

        public virtual TDynamicModel GetByTitle(string value, string providerName = null)
        {
            IEnumerable<TDynamicModel> byField = this.GetByField<string>("Title", value, providerName, null, 0, 0);
            if (byField.Any())
            {
                return byField.First();
            }
            return default(TDynamicModel);
        }

        public override Type GetDynamicType()
        {
            return TypeResolutionService.ResolveType(Activator.CreateInstance<TDynamicModel>().MappedType);
        }

        public virtual IEnumerable<TDynamicModel> GetRecent(string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<DynamicContent> publicationDate =
                from s in this.Get(providerName)
                where (int)s.Status == 2
                select s;
            if (filter != null)
            {
                publicationDate = publicationDate.Where<DynamicContent>(filter);
            }
            publicationDate =
                from i in publicationDate
                orderby i.PublicationDate descending
                select i;
            if (skip > 0)
            {
                publicationDate = publicationDate.Skip<DynamicContent>(skip);
            }
            if (take > 0)
            {
                publicationDate = publicationDate.Take<DynamicContent>(take);
            }
            return
                from i in publicationDate
                select ((DynamicModuleManager<TBaseDynamicModuleManager, TDynamicModel>)this).CreateInstance(i);
        }

        public virtual bool Update(TDynamicModel value, string providerName = null)
        {
            bool flag;
            using (ElevatedModeRegion elevatedModeRegion = new ElevatedModeRegion(base.GetManager(providerName)))
            {
                try
                {
                    DynamicContent sitefinityModel = value.ToSitefinityModel();
                    flag = (sitefinityModel != null ? base.GetManager(providerName).PublishAndSave(sitefinityModel, null) : false);
                }
                catch
                {
                    flag = false;
                }
            }
            return flag;
        }
    }
}