﻿using Case.Framework.Sitefinity.Models.Interfaces;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.OpenAccess;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseFieldManager<TManager, TContent, TBaseManager, TContentModel> : BaseDataManager<TManager, TContent, TBaseManager, TContentModel>
        where TManager : class, IManager
        where TContent : IDynamicFieldsContainer, ILifecycleDataItem, ILocatable, ILocalizable
        where TBaseManager : BaseManager<TManager>, new()
        where TContentModel : IDataModel, new()
    {
        protected BaseFieldManager()
        {
        }

        public virtual IEnumerable<TContentModel> GetByCategory(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomy("Category", value, providerName, filter, take, skip);
        }

        public virtual IEnumerable<TContentModel> GetByCategoryId(Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomyId("Category", id, providerName, filter, take, skip);
        }

        public virtual IEnumerable<TContentModel> GetByField<TFieldType>(string key, TFieldType value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        where TFieldType : class
        {
            TFieldType empty = value;
            IQueryable<TContent> tContents = Enumerable.Empty<TContent>().AsQueryable<TContent>();
            if (ContentHelper.DoesTypeContainField(this.GetDynamicType(), key))
            {
                if (typeof(TFieldType) != typeof(string))
                {
                    tContents =
                        from i in this.Get(providerName)
                        where ((IDynamicFieldsContainer)i).GetValue<TFieldType>(key) == empty
                        select i;
                }
                else
                {
                    if (empty == null)
                    {
                        empty = string.Empty as TFieldType;
                    }
                    tContents =
                        from i in this.Get(providerName)
                        where ((IDynamicFieldsContainer)i).GetValue<string>(key).ToLower() == empty.ToString().ToLower()
                        select i;
                }
                if (filter != null)
                {
                    tContents = tContents.Where<TContent>(filter);
                }
                if (skip > 0)
                {
                    tContents = tContents.Skip<TContent>(skip);
                }
                if (take > 0)
                {
                    tContents = tContents.Take<TContent>(take);
                }
            }
            return tContents.Select(CreateInstance);
      
            //return
            //    from x in tContents
            //    select ((BaseFieldManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(x);
        }

        public virtual IEnumerable<TContentModel> GetByTag(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomy("Tags", value, providerName, filter, take, skip);
        }

        public virtual IEnumerable<TContentModel> GetByTagId(Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomyId("Tag", id, providerName, filter, take, skip);
        }

        public virtual IEnumerable<TContentModel> GetByTaxonomy(string key, string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            Taxon taxon = TaxonomyManager.GetManager(providerName).GetTaxa<Taxon>().FirstOrDefault<Taxon>((Taxon t) => t.Name == value);
            if (taxon == null)
            {
                return Enumerable.Empty<TContentModel>().AsQueryable<TContentModel>();
            }
            return this.GetByTaxonomyId(key, taxon.Id, providerName, filter, take, skip);
        }

        public virtual IEnumerable<TContentModel> GetByTaxonomyId(string key, Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<TContent> tContents = Enumerable.Empty<TContent>().AsQueryable<TContent>();
            if (ContentHelper.DoesTypeContainField(this.GetDynamicType(), key))
            {
                tContents =
                    from i in this.Get(providerName)
                    where ((IDynamicFieldsContainer)i).GetValue<TrackedList<Guid>>(key).Contains(id) && (int)((ILifecycleDataItem)i).Status == 2
                    select i;
                if (filter != null)
                {
                    tContents = tContents.Where<TContent>(filter);
                }
                if (skip > 0)
                {
                    tContents = tContents.Skip<TContent>(skip);
                }
                if (take > 0)
                {
                    tContents = tContents.Take<TContent>(take);
                }
            }
            return
                from x in tContents
                select ((BaseFieldManager<TManager, TContent, TBaseManager, TContentModel>)this).CreateInstance(x);
        }

        public virtual IEnumerable<TContentModel> GetByTaxonomyTitle(string key, string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0)
        {
            Taxon taxon = TaxonomyManager.GetManager(providerName).GetTaxa<Taxon>().FirstOrDefault<Taxon>((Taxon t) => t.Title == (Lstring)value);
            if (taxon == null)
            {
                return Enumerable.Empty<TContentModel>().AsQueryable<TContentModel>();
            }
            return this.GetByTaxonomyId(key, taxon.Id, providerName, filter, take, skip);
        }

        public virtual Type GetDynamicType()
        {
            return typeof(TContent);
        }
    }
}