﻿using System;
using Telerik.Sitefinity.Data;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseSingletonManager<TManager, TBaseManager> : BaseManager<TManager>
        where TManager : class, IManager
        where TBaseManager : BaseManager<TManager>, new()
    {
        private readonly static TBaseManager _instance;

        public static TBaseManager Instance
        {
            get
            {
                return BaseSingletonManager<TManager, TBaseManager>._instance;
            }
        }

        static BaseSingletonManager()
        {
            BaseSingletonManager<TManager, TBaseManager>._instance = Activator.CreateInstance<TBaseManager>();
        }

        protected BaseSingletonManager()
        {
        }
    }
}