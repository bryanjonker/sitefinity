﻿using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Abstracts
{
    public abstract class BaseDataManager<TManager, TDataItem, TBaseManager, TDataModel> : BaseSingletonManager<TManager, TBaseManager>, IDataManager<TDataModel, TDataItem>
        where TManager : class, IManager
        where TDataItem : IDataItem
        where TBaseManager : BaseManager<TManager>, new()
        where TDataModel : IDataModel, new()
    {
        protected BaseDataManager()
        {
        }

        protected abstract TDataModel CreateInstance(TDataItem sfContent);

        protected abstract IQueryable<TDataItem> Get(string providerName = null);

        protected abstract TDataItem Get(Guid id, string providerName = null);

        public abstract IEnumerable<TDataModel> GetAll(string providerName = null, Expression<Func<TDataItem, bool>> filter = null, int take = 0, int skip = 0);

        public virtual TDataModel GetById(Guid id, string providerName = null)
        {
            return this.CreateInstance(this.Get(id, providerName));
        }
    }
}