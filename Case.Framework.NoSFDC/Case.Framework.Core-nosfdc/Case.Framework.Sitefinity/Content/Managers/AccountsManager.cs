﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Claims;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class AccountsManager : BaseSingletonManager<UserManager, AccountsManager>
    {
        public AccountsManager()
        {
        }

        public UserModel GetCurrent()
        {
            return new UserModel(ClaimsManager.GetCurrentIdentity(null));
        }
    }
}