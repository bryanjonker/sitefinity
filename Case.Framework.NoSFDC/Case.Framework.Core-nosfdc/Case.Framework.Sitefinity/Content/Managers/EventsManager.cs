﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Events.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Events;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class EventsManager : BaseContentManager<Telerik.Sitefinity.Modules.Events.EventsManager, Event, EventsManager, EventModel>, IChildManager<EventModel, Event>, IContentManager<EventModel, Event>, IDataManager<EventModel, Event>
    {
        public EventsManager()
        {
        }

        protected override EventModel CreateInstance(Event sfContent)
        {
            return new EventModel(sfContent);
        }

        protected override IQueryable<Event> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetEvents();
        }

        protected override Event Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetEvent(id);
        }

        public virtual IEnumerable<EventModel> GetByParent(string value, string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> eventStart =
                from s in this.Get(providerName)
                where s.Parent.UrlName.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)s.Status == 2
                select s;
            if (filter != null)
            {
                eventStart = eventStart.Where<Event>(filter);
            }
            eventStart =
                from i in eventStart
                orderby i.EventStart
                select i;
            if (skip > 0)
            {
                eventStart = eventStart.Skip<Event>(skip);
            }
            if (take > 0)
            {
                eventStart = eventStart.Take<Event>(take);
            }
            return
                from i in eventStart
                select this.CreateInstance(i);
        }

        public virtual IEnumerable<EventModel> GetByParentId(Guid id, string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> eventStart =
                from s in this.Get(providerName)
                where (s.Parent.Id == id) && (int)s.Status == 2
                select s;
            if (filter != null)
            {
                eventStart = eventStart.Where<Event>(filter);
            }
            eventStart =
                from i in eventStart
                orderby i.EventStart
                select i;
            if (skip > 0)
            {
                eventStart = eventStart.Skip<Event>(skip);
            }
            if (take > 0)
            {
                eventStart = eventStart.Take<Event>(take);
            }
            return
                from i in eventStart
                select this.CreateInstance(i);
        }

        public virtual IEnumerable<EventModel> GetByParentTitle(string value, string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> eventStart =
                from s in this.Get(providerName)
                where s.Parent.Title.Equals(value, StringComparison.OrdinalIgnoreCase) && (int)s.Status == 2
                select s;
            if (filter != null)
            {
                eventStart = eventStart.Where<Event>(filter);
            }
            eventStart =
                from i in eventStart
                orderby i.EventStart
                select i;
            if (skip > 0)
            {
                eventStart = eventStart.Skip<Event>(skip);
            }
            if (take > 0)
            {
                eventStart = eventStart.Take<Event>(take);
            }
            return
                from i in eventStart
                select this.CreateInstance(i);
        }

        public virtual IEnumerable<EventModel> GetPast(string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> eventStart =
                from i in this.Get(providerName)
                where (i.EventStart < DateTime.Now) && (int)i.Status == 2
                select i;
            if (filter != null)
            {
                eventStart = eventStart.Where<Event>(filter);
            }
            eventStart =
                from i in eventStart
                orderby i.EventStart descending
                select i;
            if (skip > 0)
            {
                eventStart = eventStart.Skip<Event>(skip);
            }
            if (take > 0)
            {
                eventStart = eventStart.Take<Event>(take);
            }
            return
                from e in eventStart
                select this.CreateInstance(e);
        }

        public virtual IEnumerable<EventModel> GetRange(DateTime start, DateTime? end = null, string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> date =
                from i in this.Get(providerName)
                where (i.EventStart.Date >= start.Date) && (int)i.Status == 2
                select i;
            if (end.HasValue)
            {
                date =
                    from i in date
                    where i.EventStart.Date <= end.Value.Date
                    select i;
            }
            if (filter != null)
            {
                date = date.Where<Event>(filter);
            }
            date =
                from i in date
                orderby i.EventStart
                select i;
            if (skip > 0)
            {
                date = date.Skip<Event>(skip);
            }
            if (take > 0)
            {
                date = date.Take<Event>(take);
            }
            return
                from e in date
                select this.CreateInstance(e);
        }

        public virtual IEnumerable<EventModel> GetUpcoming(string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> eventStart =
                from i in this.Get(providerName)
                where (i.EventStart >= DateTime.Now) && (int)i.Status == 2
                select i;
            if (filter != null)
            {
                eventStart = eventStart.Where<Event>(filter);
            }
            eventStart =
                from i in eventStart
                orderby i.EventStart
                select i;
            if (skip > 0)
            {
                eventStart = eventStart.Skip<Event>(skip);
            }
            if (take > 0)
            {
                eventStart = eventStart.Take<Event>(take);
            }
            return
                from e in eventStart
                select this.CreateInstance(e);
        }

        public override IEnumerable<EventModel> Search(string value, string providerName = null, Expression<Func<Event, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Event> eventStart =
                from i in this.Get(providerName)
                where (i.Title.ToString().ToLower().Contains(value.ToLower()) || i.Content.ToString().ToLower().Contains(value.ToLower())) && (int)i.Status == 2
                select i;
            if (filter != null)
            {
                eventStart = eventStart.Where<Event>(filter);
            }
            eventStart =
                from i in eventStart
                orderby i.EventStart
                select i;
            if (skip > 0)
            {
                eventStart = eventStart.Skip<Event>(skip);
            }
            if (take > 0)
            {
                eventStart = eventStart.Take<Event>(take);
            }
            return
                from i in eventStart
                select this.CreateInstance(i);
        }
    }
}