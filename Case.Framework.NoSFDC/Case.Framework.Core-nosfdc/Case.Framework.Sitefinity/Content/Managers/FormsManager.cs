﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Forms.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Forms;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Security.Claims;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class FormsManager : BaseSingletonManager<Telerik.Sitefinity.Modules.Forms.FormsManager, FormsManager>
    {
        public FormsManager()
        {
        }

        public void CreateFormEntries(string formName, IDictionary<string, object> inputs, string providerName = null)
        {
            DateTime dateTime;
            base.GetManager(providerName).Provider.SuppressSecurityChecks = true;
            FormDescription formByName = base.GetManager(providerName).GetFormByName(formName);
            FormEntry currentUserId = base.GetManager(providerName).CreateFormEntry(formByName.EntriesTypeName);
            foreach (KeyValuePair<string, object> input in inputs)
            {
                if (!currentUserId.DoesFieldExist(input.Key))
                {
                    continue;
                }
                currentUserId.SetValue(input.Key, (string)input.Value);
            }
            currentUserId.UserId = ClaimsManager.GetCurrentUserId();
            currentUserId.IpAddress = (inputs.Keys.Contains("IpAddress") ? (string)inputs["IpAddress"] : HttpContext.Current.Request.UserHostAddress);
            if (AppSettings.CurrentSettings.Multilingual)
            {
                currentUserId.Language = CultureInfo.CurrentUICulture.Name;
            }
            formByName.FormEntriesSeed = formByName.FormEntriesSeed + (long)1;
            currentUserId.ReferralCode = formByName.FormEntriesSeed.ToString();
            currentUserId.SubmittedOn = (!inputs.Keys.Contains("SubmittedOn") || !DateTime.TryParse((string)inputs["SubmittedOn"], out dateTime) ? DateTime.UtcNow : dateTime);
            base.GetManager(providerName).SaveChanges();
            base.GetManager(providerName).Provider.SuppressSecurityChecks = false;
        }

        public bool ImportFormEntries(string formName, string docName, string providerName = null)
        {
            LibrariesManager manager = LibrariesManager.GetManager(providerName);
            Document document = (
                from d in manager.GetDocuments()
                where (d.UrlName == (Lstring)docName) && (int)d.Status == 2
                select d).FirstOrDefault<Document>();
            if (document == null)
            {
                return false;
            }
            return this.ImportFormEntries(formName, manager.Download(document), providerName);
        }

        public bool ImportFormEntries(string formName, Stream fileStream, string providerName = null)
        {
            using (CsvReader csvReader = new CsvReader(new StreamReader(fileStream)))
            {
                while (csvReader.Read())
                {
                    this.CreateFormEntries(formName, csvReader.GetRecord<object>() as IDictionary<string, object>, providerName);
                }
            }
            return true;
        }
    }
}