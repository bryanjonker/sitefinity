﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Workflow;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class ImagesManager : BaseMediaManager<LibrariesManager, Image, ImagesManager, ImageModel>
    {
        public ImagesManager()
        {
        }

        public virtual ImageModel Create(ImageModel value, string providerName = null)
        {
            ImageModel imageModel;
            using (ElevatedModeRegion elevatedModeRegion = new ElevatedModeRegion(base.GetManager(providerName)))
            {
                try
                {
                    Image sitefinityModel = value.ToSitefinityModel();
                    sitefinityModel.PublicationDate = DateTime.UtcNow;
                    if (!string.IsNullOrWhiteSpace(value.File))
                    {
                        string str = HttpContext.Current.Server.MapPath(value.File);
                        if (File.Exists(str))
                        {
                            using (FileStream fileStream = new FileStream(str, FileMode.Open, FileAccess.Read))
                            {
                                base.GetManager(providerName).Upload(sitefinityModel, fileStream, Path.GetExtension(str));
                            }
                        }
                    }
                    if (sitefinityModel.OriginalContentId != Guid.Empty)
                    {
                        Image image = base.GetManager(providerName).Lifecycle.CheckIn(sitefinityModel, null, true) as Image;
                        base.GetManager(providerName).SaveChanges();
                        Dictionary<string, string> strs = new Dictionary<string, string>()
                        {
                            { "ContentType", typeof(Image).FullName }
                        };
                        WorkflowManager.MessageWorkflow(image.Id, typeof(Image), null, "Publish", false, strs);
                    }
                    else
                    {
                        base.GetManager(providerName).SaveChanges();
                        Dictionary<string, string> strs1 = new Dictionary<string, string>()
                        {
                            { "ContentType", typeof(Image).FullName }
                        };
                        WorkflowManager.MessageWorkflow(sitefinityModel.Id, typeof(Image), null, "Publish", false, strs1);
                    }
                    base.GetManager(providerName).SaveChanges();
                    value.Id = sitefinityModel.Id;
                    imageModel = value;
                }
                catch
                {
                    imageModel = null;
                }
            }
            return imageModel;
        }

        protected override ImageModel CreateInstance(Image sfContent)
        {
            return new ImageModel(sfContent);
        }

        protected override IQueryable<Image> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetImages();
        }

        protected override Image Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetImage(id);
        }
    }
}