﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.News;
using Telerik.Sitefinity.News.Model;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class NewsManager : BaseContentManager<Telerik.Sitefinity.Modules.News.NewsManager, NewsItem, 
        NewsManager, NewsItemModel>
    {
        public NewsManager()
        {
        }

        protected override NewsItemModel CreateInstance(NewsItem sfContent)
        {
            return new NewsItemModel(sfContent);
        }

        protected override IQueryable<NewsItem> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetNewsItems();
        }

        protected override NewsItem Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetNewsItem(id);
        }

        public override IEnumerable<NewsItemModel> Search(string value, string providerName = null, Expression<Func<NewsItem, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<NewsItem> newsItems =
                from i in this.Get(providerName)
                where (i.Title.ToString().ToLower().Contains(value.ToLower()) || i.Content.ToString().ToLower().Contains(value.ToLower())) && (int)i.Status == 2
                select i;
            if (filter != null)
            {
                newsItems = newsItems.Where<NewsItem>(filter);
            }
            if (skip > 0)
            {
                newsItems = newsItems.Skip<NewsItem>(skip);
            }
            if (take > 0)
            {
                newsItems = newsItems.Take<NewsItem>(take);
            }
            return
                from i in newsItems
                select this.CreateInstance(i);
        }
    }
}