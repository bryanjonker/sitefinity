﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Blogs.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Blogs;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class BlogPostsManager : BaseChildManager<Telerik.Sitefinity.Modules.Blogs.BlogsManager, BlogPost, BlogPostsManager, BlogPostModel>
    {
        public BlogPostsManager()
        {
        }

        protected override BlogPostModel CreateInstance(BlogPost sfContent)
        {
            return new BlogPostModel(sfContent);
        }

        protected override IQueryable<BlogPost> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetBlogPosts();
        }

        protected override BlogPost Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetBlogPost(id);
        }

        public override IEnumerable<BlogPostModel> Search(string value, string providerName = null, Expression<Func<BlogPost, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<BlogPost> blogPosts =
                from i in this.Get(providerName)
                where (i.Title.ToString().ToLower().Contains(value.ToLower()) || i.Content.ToString().ToLower().Contains(value.ToLower())) && (int)i.Status == 2
                select i;
            if (filter != null)
            {
                blogPosts = blogPosts.Where<BlogPost>(filter);
            }
            if (skip > 0)
            {
                blogPosts = blogPosts.Skip<BlogPost>(skip);
            }
            if (take > 0)
            {
                blogPosts = blogPosts.Take<BlogPost>(take);
            }
            return
                from i in blogPosts
                select this.CreateInstance(i);
        }
    }
}