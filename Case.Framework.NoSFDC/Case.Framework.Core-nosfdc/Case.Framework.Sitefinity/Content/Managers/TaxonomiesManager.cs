﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class TaxonomiesManager : BaseSingletonManager<TaxonomyManager, TaxonomiesManager>
    {
        public TaxonomiesManager()
        {
        }

        public virtual IEnumerable<TaxonModel> GetAll(string providerName = null, Expression<Func<Taxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Taxon> taxa = base.GetManager(providerName).GetTaxa<Taxon>();
            if (filter != null)
            {
                taxa = taxa.Where<Taxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<Taxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<Taxon>(take);
            }
            return
                from i in taxa
                select new TaxonModel(i);
        }

        public virtual TaxonModel GetById(Guid id, string providerName = null)
        {
            Taxon taxon = base.GetManager(providerName).GetTaxon<Taxon>(id);
            if (taxon == null)
            {
                return null;
            }
            return new TaxonModel(taxon);
        }

        public virtual TaxonModel GetByName(string value, string providerName = null)
        {
            IQueryable<Taxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<Taxon>()
                where p.UrlName.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (!taxa.Any<Taxon>())
            {
                return null;
            }
            return new TaxonModel(taxa.SingleOrDefault<Taxon>());
        }

        public virtual IEnumerable<TaxonModel> GetByParent(string value, string providerName = null, Expression<Func<Taxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Taxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<Taxon>()
                where p.Parent.Name.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (filter != null)
            {
                taxa = taxa.Where<Taxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<Taxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<Taxon>(take);
            }
            return
                from i in taxa
                select new TaxonModel(i);
        }

        public virtual IEnumerable<TaxonModel> GetByParentId(Guid id, string providerName = null, Expression<Func<Taxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Taxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<Taxon>()
                where p.Parent.Id == id
                select p;
            if (filter != null)
            {
                taxa = taxa.Where<Taxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<Taxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<Taxon>(take);
            }
            return
                from i in taxa
                select new TaxonModel(i);
        }

        public virtual IEnumerable<TaxonModel> GetByParentTitle(string value, string providerName = null, Expression<Func<Taxon, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Taxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<Taxon>()
                where p.Parent.Title.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (filter != null)
            {
                taxa = taxa.Where<Taxon>(filter);
            }
            if (skip > 0)
            {
                taxa = taxa.Skip<Taxon>(skip);
            }
            if (take > 0)
            {
                taxa = taxa.Take<Taxon>(take);
            }
            return
                from i in taxa
                select new TaxonModel(i);
        }

        public virtual TaxonModel GetByTitle(string value, string providerName = null)
        {
            IQueryable<Taxon> taxa =
                from p in base.GetManager(providerName).GetTaxa<Taxon>()
                where p.Title.Equals(value, StringComparison.OrdinalIgnoreCase)
                select p;
            if (!taxa.Any<Taxon>())
            {
                return null;
            }
            return new TaxonModel(taxa.FirstOrDefault<Taxon>());
        }
    }
}