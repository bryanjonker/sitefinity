﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Content.Managers.Interfaces;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.OpenAccess;
using Telerik.Sitefinity.Blogs.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Blogs;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;


namespace Case.Framework.Sitefinity.Content.Managers
{
    public class BlogsManager : BaseDataManager<Telerik.Sitefinity.Modules.Blogs.BlogsManager, Blog, BlogsManager, BlogModel>, IContentManager<BlogModel, Blog>, IDataManager<BlogModel, Blog>
    {
        public BlogsManager()
        {
        }

        protected override BlogModel CreateInstance(Blog sfContent)
        {
            return new BlogModel(sfContent);
        }

        protected override IQueryable<Blog> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetBlogs();
        }

        protected override Blog Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetBlog(id);
        }

        public override IEnumerable<BlogModel> GetAll(string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Blog> blogs =
                from i in this.Get(providerName)
                select i;
            if (filter != null)
            {
                blogs = blogs.Where<Blog>(filter);
            }
            if (skip > 0)
            {
                blogs = blogs.Skip<Blog>(skip);
            }
            if (take > 0)
            {
                blogs = blogs.Take<Blog>(take);
            }
            return
                from i in blogs
                select this.CreateInstance(i);
        }

        public virtual IEnumerable<BlogModel> GetByCategory(string value, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomy("Category", value, providerName, filter, take, skip);
        }

        public virtual IEnumerable<BlogModel> GetByCategoryId(Guid id, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomyId("Category", id, providerName, filter, take, skip);
        }

        public virtual BlogModel GetByName(string value, string providerName = null)
        {
            IQueryable<Blog> blogs =
                from i in this.Get(providerName)
                where i.UrlName.Equals(value, StringComparison.OrdinalIgnoreCase)
                select i;
            if (!blogs.Any<Blog>())
            {
                return null;
            }
            return this.CreateInstance(blogs.FirstOrDefault<Blog>());
        }

        public virtual IEnumerable<BlogModel> GetByTag(string value, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomy("Tags", value, providerName, filter, take, skip);
        }

        public virtual IEnumerable<BlogModel> GetByTagId(Guid id, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            return this.GetByTaxonomyId("Tag", id, providerName, filter, take, skip);
        }

        public virtual IEnumerable<BlogModel> GetByTaxonomy(string key, string value, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            Taxon taxon = TaxonomyManager.GetManager(providerName).GetTaxa<Taxon>().FirstOrDefault<Taxon>((Taxon t) => t.Name == value);
            if (taxon == null)
            {
                return Enumerable.Empty<BlogModel>().AsQueryable<BlogModel>();
            }
            return this.GetByTaxonomyId(key, taxon.Id, providerName, filter, take, skip);
        }

        public virtual IEnumerable<BlogModel> GetByTaxonomyId(string key, Guid id, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Blog> blogs = Enumerable.Empty<Blog>().AsQueryable<Blog>();
            if (ContentHelper.DoesTypeContainField(typeof(Blog), key))
            {
                blogs =
                    from i in this.Get(providerName)
                    where i.GetValue<TrackedList<Guid>>(key).Contains(id)
                    select i;
                if (filter != null)
                {
                    blogs = blogs.Where<Blog>(filter);
                }
                if (skip > 0)
                {
                    blogs = blogs.Skip<Blog>(skip);
                }
                if (take > 0)
                {
                    blogs = blogs.Take<Blog>(take);
                }
            }
            return
                from x in blogs
                select this.CreateInstance(x);
        }

        public virtual IEnumerable<BlogModel> GetByTaxonomyTitle(string key, string value, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            Taxon taxon = TaxonomyManager.GetManager(providerName).GetTaxa<Taxon>().FirstOrDefault<Taxon>((Taxon t) => t.Title == (Lstring)value);
            if (taxon == null)
            {
                return Enumerable.Empty<BlogModel>().AsQueryable<BlogModel>();
            }
            return this.GetByTaxonomyId(key, taxon.Id, providerName, filter, take, skip);
        }

        public virtual BlogModel GetByTitle(string value, string providerName = null)
        {
            IQueryable<Blog> blogs =
                from i in this.Get(providerName)
                where i.Title.Equals(value, StringComparison.OrdinalIgnoreCase)
                select i;
            if (!blogs.Any<Blog>())
            {
                return null;
            }
            return this.CreateInstance(blogs.FirstOrDefault<Blog>());
        }

        public virtual IEnumerable<BlogModel> GetRecent(string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Blog> publicationDate =
                from s in this.Get(providerName)
                select s;
            if (filter != null)
            {
                publicationDate = publicationDate.Where<Blog>(filter);
            }
            publicationDate =
                from i in publicationDate
                orderby i.PublicationDate descending
                select i;
            if (skip > 0)
            {
                publicationDate = publicationDate.Skip<Blog>(skip);
            }
            if (take > 0)
            {
                publicationDate = publicationDate.Take<Blog>(take);
            }
            return
                from i in publicationDate
                select this.CreateInstance(i);
        }

        public virtual IEnumerable<BlogModel> Search(string value, string providerName = null, Expression<Func<Blog, bool>> filter = null, int take = 0, int skip = 0)
        {
            IQueryable<Blog> blogs =
                from i in this.Get(providerName)
                where (i.Title.ToString().ToLower().Contains(value.ToLower()) || i.Description.ToString().ToLower().Contains(value.ToLower()))
                select i;
            if (filter != null)
            {
                blogs = blogs.Where<Blog>(filter);
            }
            if (skip > 0)
            {
                blogs = blogs.Skip<Blog>(skip);
            }
            if (take > 0)
            {
                blogs = blogs.Take<Blog>(take);
            }
            return
                from i in blogs
                select this.CreateInstance(i);
        }
    }
}