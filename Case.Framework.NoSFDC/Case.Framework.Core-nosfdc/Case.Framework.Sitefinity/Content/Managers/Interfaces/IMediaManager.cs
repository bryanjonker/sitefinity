﻿using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Interfaces
{
    public interface IMediaManager<TContentModel, TContent> : IChildManager<TContentModel, TContent>, IContentManager<TContentModel, TContent>, IDataManager<TContentModel, TContent>
        where TContentModel : MediaModel
        where TContent : IDataItem, IDynamicFieldsContainer
    {
        IEnumerable<TContentModel> GetByExtension(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);
    }
}