﻿using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Interfaces
{
    public interface IContentManager<TContentModel, TContent> : IDataManager<TContentModel, TContent>
        where TContentModel : ContentModel
        where TContent : IDataItem, IDynamicFieldsContainer
    {
        IEnumerable<TContentModel> GetByCategory(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByCategoryId(Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        TContentModel GetByName(string value, string providerName = null);

        IEnumerable<TContentModel> GetByTag(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByTagId(Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByTaxonomy(string key, string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByTaxonomyId(string key, Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByTaxonomyTitle(string key, string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        TContentModel GetByTitle(string value, string providerName = null);

        IEnumerable<TContentModel> GetRecent(string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> Search(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);
    }
}