﻿using Case.Framework.Sitefinity.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Interfaces
{
    public interface IDataManager<TContentModel, TContent>
        where TContentModel : IDataModel
        where TContent : IDataItem
    {
        IEnumerable<TContentModel> GetAll(string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        TContentModel GetById(Guid id, string providerName = null);
    }
}