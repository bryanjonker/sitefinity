﻿using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Telerik.Sitefinity.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Interfaces
{
    public interface IChildManager<TContentModel, TContent> : IContentManager<TContentModel, TContent>, IDataManager<TContentModel, TContent>
        where TContentModel : ContentModel
        where TContent : IDataItem, IDynamicFieldsContainer
    {
        IEnumerable<TContentModel> GetByParent(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByParentId(Guid id, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TContentModel> GetByParentTitle(string value, string providerName = null, Expression<Func<TContent, bool>> filter = null, int take = 0, int skip = 0);
    }
}