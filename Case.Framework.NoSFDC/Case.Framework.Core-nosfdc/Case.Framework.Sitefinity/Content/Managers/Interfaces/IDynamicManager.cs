﻿using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Telerik.Sitefinity.DynamicModules.Model;

namespace Case.Framework.Sitefinity.Content.Managers.Interfaces
{
    public interface IDynamicManager<TDynamicModel>
    where TDynamicModel : DynamicModel
    {
        IEnumerable<TDynamicModel> GetAll(string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0);

        TDynamicModel GetById(Guid id, string providerName = null);

        TDynamicModel GetByName(string value, string providerName = null);

        IEnumerable<TDynamicModel> GetByTaxonomy(string key, string value, string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TDynamicModel> GetByTaxonomyId(string key, Guid id, string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0);

        IEnumerable<TDynamicModel> GetByTaxonomyTitle(string key, string value, string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0);

        TDynamicModel GetByTitle(string value, string providerName = null);

        IEnumerable<TDynamicModel> GetRecent(string providerName = null, Expression<Func<DynamicContent, bool>> filter = null, int take = 0, int skip = 0);
    }
}