﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Linq;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class DocumentsManager : BaseMediaManager<LibrariesManager, Document, DocumentsManager, DocumentModel>
    {
        public DocumentsManager()
        {
        }

        protected override DocumentModel CreateInstance(Document sfContent)
        {
            return new DocumentModel(sfContent);
        }

        protected override IQueryable<Document> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetDocuments();
        }

        protected override Document Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetDocument(id);
        }
    }
}