﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Linq;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class VideosManager : BaseMediaManager<LibrariesManager, Video, VideosManager, VideoModel>
    {
        public VideosManager()
        {
        }

        protected override VideoModel CreateInstance(Video sfContent)
        {
            return new VideoModel(sfContent);
        }

        protected override IQueryable<Video> Get(string providerName = null)
        {
            return base.GetManager(providerName).GetVideos();
        }

        protected override Video Get(Guid id, string providerName = null)
        {
            return base.GetManager(providerName).GetVideo(id);
        }
    }
}