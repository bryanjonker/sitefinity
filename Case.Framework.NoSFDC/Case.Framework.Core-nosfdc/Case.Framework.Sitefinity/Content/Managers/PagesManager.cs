﻿using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Models;
using System;
using System.Web;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.Configuration;

namespace Case.Framework.Sitefinity.Content.Managers
{
    public class PagesManager : BaseSingletonManager<PageManager, PagesManager>
    {
        public PagesManager()
        {
        }

        public virtual PageModel GetAll(bool includeChildren,bool includeCustomFieldValues = false)
        {
            return this.GetById(SiteInitializer.CurrentFrontendRootNodeId, false, includeChildren,includeCustomFieldValues);
        }

        public virtual PageModel GetAll()
        {
            return this.GetById(SiteInitializer.CurrentFrontendRootNodeId,true,true);
        }

        public virtual PageModel GetById(Guid id, bool includeParent = false, bool includeChildren = false,bool includeCustomFieldValues = false)
        {
            if (id == Guid.Empty)
            {
                return null;
            }
            return new PageModel(SiteMapBase.GetCurrentProvider().FindSiteMapNodeFromKey(id.ToString()), includeParent, includeChildren, includeCustomFieldValues);
        }

        public virtual PageModel GetByUrl(string value, bool includeChildren = true)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }
            SiteMapProvider currentProvider = SiteMapBase.GetCurrentProvider();
            char[] chrArray = new char[] { '~', '/' };
            return new PageModel(currentProvider.FindSiteMapNode(VirtualPathUtility.ToAbsolute(string.Concat("~/", value.TrimStart(chrArray)))), true, includeChildren);
        }

        public virtual PageNode GetCurrentPage(string providerName = null)
        {
            Guid currentSiteMapNodeId = this.GetCurrentSiteMapNodeId();
            if (currentSiteMapNodeId == Guid.Empty)
            {
                return null;
            }
            return base.GetManager(providerName).GetPageNode(currentSiteMapNodeId);
        }

        public virtual SiteMapNode GetCurrentSiteMapNode()
        {
            return SiteMapBase.GetActualCurrentNode();
        }

        public virtual Guid GetCurrentSiteMapNodeId()
        {
            PageSiteNode actualCurrentNode = SiteMapBase.GetActualCurrentNode();
            Guid empty = Guid.Empty;
            if (actualCurrentNode != null)
            {
                Guid.TryParse(actualCurrentNode.Key, out empty);
            }
            return empty;
        }

        public virtual ThemeElement GetCurrentTheme(string providerName = null)
        {
            PageNode currentPage = this.GetCurrentPage(providerName);
            if (currentPage != null)
            {
                PageTemplate template = currentPage.GetPageData(null).Template;
                if (template != null)
                {
                    string theme = template.Theme;
                    if (!string.IsNullOrWhiteSpace(theme) && theme != "notheme")
                    {
                        return Config.Get<AppearanceConfig>().FrontendThemes[theme];
                    }
                }
            }
            return null;
        }
    }
}