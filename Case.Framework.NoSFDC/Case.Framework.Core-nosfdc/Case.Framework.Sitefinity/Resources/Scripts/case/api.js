﻿/**
 * This is an API wrapper for Sitefinity
 */
define([
	'jquery',
	'case/helpers'
], function ($, CaseHelpers) {

    return {
        //START PAGES API
        getPages: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/pages'));
        },

        getPagesById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/pages/' + id));
        },

        getPagesByUrl: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/pages/getbyurl?value=' + value));
        },

        //START CONTENT API
        getContentsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/contents/' + id));
        },

        getContentsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/contents/getbyname?value=' + value));
        },

        getContentsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/contents/getbytitle?value=' + value));
        },

        //START NEWS API
        getNews: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/news'));
        },

        getNewsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/' + id));
        },

        getNewsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getbyname?value=' + value));
        },

        getNewsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getbytitle?value=' + value));
        },

        getNewsByCategory: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getbycategory?value=' + value));
        },

        getNewsByTag: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getbytag?value=' + value));
        },

        getNewsByCategoryId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getbycategoryid/' + id));
        },

        getNewsByTagId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getbytagid/' + id));
        },

        getNewsBySearch: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/search?value=' + value));
        },

        getNewsByRecent: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/news/getrecent'));
        },

        //START EVENTS API
        getEvents: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/events'));
        },

        getEventsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/' + id));
        },

        getEventsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getbyname?value=' + value));
        },

        getEventsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getbytitle?value=' + value));
        },

        getEventsByPast: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getpast'));
        },

        getEventsByUpcoming: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getupcoming'));
        },

        getEventsByRange: function (start, end) {
            return $.getJSON(CaseHelpers.toServicesUrl(
			    '/events/getbyrange?start=' + start + '&end=' + end));
        },

        getEventsByCategory: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getbycategory?value=' + value));
        },

        getEventsByTag: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getbytag?value=' + value));
        },

        getEventsByCategoryId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getbycategoryid/' + id));
        },

        getEventsByTagId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getbytagid/' + id));
        },

        getEventsBySearch: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/search?value=' + value));
        },

        getEventsByRecent: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/events/getrecent'));
        },

        //START PRODUCTS API
        getProducts: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/products'));
        },

        getProductsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/' + id));
        },

        getProductsBySku: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbysku?value=' + value));
        },

        getProductsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbyname?value=' + value));
        },

        getProductsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbytitle?value=' + value));
        },

        getProductsByCategory: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbycategory?value=' + value));
        },

        getProductsByTag: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbytag?value=' + value));
        },

        getProductsByCategoryId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbycategoryid/' + id));
        },

        getProductsByTagId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getbytagid/' + id));
        },

        getProductsBySearch: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/search?value=' + value));
        },

        getProductsByRecent: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/products/getrecent'));
        },

        //START BLOGS API
        getBlogs: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogs'));
        },

        getBlogsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogs/' + id));
        },

        getBlogsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogs/getbyname?value=' + value));
        },

        getBlogsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogs/getbytitle?value=' + value));
        },

        getBlogsBySearch: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogs/search?value=' + value));
        },

        //START BLOG POSTS API
        getBlogPosts: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts'));
        },

        getBlogPostsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/' + id));
        },

        getBlogPostsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbyname?value=' + value));
        },

        getBlogPostsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbytitle?value=' + value));
        },

        getBlogPostsByCategory: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbycategory?value=' + value));
        },

        getBlogPostsByTag: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbytag?value=' + value));
        },

        getBlogPostsByCategoryId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbycategoryid/' + id));
        },

        getBlogPostsByTagId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbytagid/' + id));
        },

        getBlogPostsBySearch: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/search?value=' + value));
        },

        getBlogPostsByParent: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbyparent?value=' + value));
        },

        getBlogPostsByParentTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbyparenttitle?value=' + value));
        },

        getBlogPostsByParentId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getbyparentid/' + id));
        },

        getBlogPostsByRecent: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/blogposts/getrecent'));
        },

        //START IMAGES API
        getImagesById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/images/' + id));
        },

        getImagesByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/images/getbyname?value=' + value));
        },

        getImagesByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/images/getbytitle?value=' + value));
        },

        getImagesByParent: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/images/getbyparent?value=' + value));
        },

        getImagesByParentId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/images/getbyparentid/' + id));
        },

        getImagesByRecent: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/images/getrecent'));
        },

        //START VIDEOS API
        getVideosById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/videos/' + id));
        },

        getVideosByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/videos/getbyname?value=' + value));
        },

        getVideosByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/videos/getbytitle?value=' + value));
        },

        getVideosByParent: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/videos/getbyparent?value=' + value));
        },

        getVideosByParentId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/videos/getbyparentid/' + id));
        },

        getVideosByRecent: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/videos/getrecent'));
        },

        //START DOCS API
        getDocumentsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/documents/' + id));
        },

        getDocumentsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/documents/getbyname?value=' + value));
        },

        getDocumentsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/documents/getbytitle?value=' + value));
        },

        getDocumentsByParent: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/documents/getbyparent?value=' + value));
        },

        getDocumentsByParentId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/documents/getbyparentid/' + id));
        },

        getDocumentsByRecent: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/documents/getrecent'));
        },

        //START LISTS API
        getListsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/lists/' + id));
        },

        getListsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/lists/getbyname?value=' + value));
        },

        getListsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/lists/getbytitle?value=' + value));
        },

        //START DEPARTMENTS API
        getDepartments: function () {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments'));
        },

        getDepartmentsById: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments/' + id));
        },

        getDepartmentsByName: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments/getbyname?value=' + value));
        },

        getDepartmentsByTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments/getbytitle?value=' + value));
        },

        getDepartmentsByParent: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments/getbyparent?value=' + value));
        },

        getDepartmentsByParentTitle: function (value) {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments/getbyparenttitle?value=' + value));
        },

        getDepartmentsByParentId: function (id) {
            return $.getJSON(CaseHelpers.toServicesUrl('/departments/getbyparentid/' + id));
        }
    };
});
