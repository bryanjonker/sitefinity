﻿using Case.Framework.Sitefinity;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Case.Framework.Sitefinity")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CASE Partners, Inc.")]
[assembly: AssemblyProduct("Case.Framework.Sitefinity")]
[assembly: AssemblyCopyright("Copyright © 2014 - CASE Partners, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3e2cf125-5ca4-4ac1-bf15-0cc37ae245c4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.*")]

[assembly: PreApplicationStartMethod(typeof(Application), "PreInit")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.angular.angular.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.case.alerts.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.case.api.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.case.helpers.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.bootstrap.css.bootstrap.min.css", "text/css")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.bootstrap.js.bootstrap.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.glide.jquery.glide.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.js-url.url.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.lostorage.loStorage.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.moment.moment.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.moment.moment-timezone-with-data.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.require.css.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.underscore.lodash.underscore.min.js", "text/javascript")]
[assembly: WebResource("Case.Framework.Sitefinity.Resources.Scripts.underscore.underscore.string.min.js", "text/javascript")]
