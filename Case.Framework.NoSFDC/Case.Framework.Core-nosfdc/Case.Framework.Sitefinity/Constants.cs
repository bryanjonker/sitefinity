﻿using System;

namespace Case.Framework.Sitefinity
{
    public static class Constants
    {
        public const string VALUE_CUSTOM_VIRTUAL_ROOT_PATH = "~/CaseSF";

        public const string VALUE_DEFAULT_SCRIPTS_PATH = "~/Scripts";

        public const string VALUE_TOP_STYLES_PLACEHOLDER = "TopStylesPlaceHolder";

        public const string VALUE_TOP_SCRIPTS_PLACEHOLDER = "TopScriptsPlaceHolder";

        public const string VALUE_KENDO_DATAVIZ_CSS = "http://cdn.kendostatic.com/2014.1.318/styles/kendo.dataviz.min.css";

        public const string VALUE_TOOLBOX_SECTION_NAME = "Extras";
    }
}