﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Sitefinity
{
    public enum SitefinityContentType
    {
        UserRole,
        UserRoleLink,
        User,
        FormDefinition,
        DocumentLibrary,
        DynamicContentType,
        Event,
        Other
    }
}
