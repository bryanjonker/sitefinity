﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using System;

namespace Case.Framework.Sitefinity.Data
{
    public static class CaseManagers
    {
        public static AccountsManager Accounts
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Security.UserManager, AccountsManager>.Instance;
            }
        }

        public static BlogPostsManager BlogPosts
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Blogs.BlogsManager, BlogPostsManager>.Instance;
            }
        }

        public static BlogsManager Blogs
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Blogs.BlogsManager, BlogsManager>.Instance;
            }
        }

        public static ContentsManager Contents
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.GenericContent.ContentManager, ContentsManager>.Instance;
            }
        }

        public static DepartmentsManager Departments
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Taxonomies.TaxonomyManager, DepartmentsManager>.Instance;
            }
        }

        public static DocumentsManager Documents
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Libraries.LibrariesManager, DocumentsManager>.Instance;
            }
        }

        public static EventsManager Events
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Events.EventsManager, EventsManager>.Instance;
            }
        }

        public static FormsManager Forms
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Forms.FormsManager, FormsManager>.Instance;
            }
        }

        public static ImagesManager Images
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Libraries.LibrariesManager, ImagesManager>.Instance;
            }
        }

        public static ListsManager Lists
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Lists.ListsManager, ListsManager>.Instance;
            }
        }

        public static NewsManager News
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.News.NewsManager, NewsManager>.Instance;
            }
        }

        public static PagesManager Pages
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Pages.PageManager, PagesManager>.Instance;
            }
        }

        public static TaxonomiesManager Taxonomies
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Taxonomies.TaxonomyManager, TaxonomiesManager>.Instance;
            }
        }

        public static VideosManager Videos
        {
            get
            {
                return BaseSingletonManager<Telerik.Sitefinity.Modules.Libraries.LibrariesManager, VideosManager>.Instance;
            }
        }
    }
}