﻿using System;
using Telerik.Sitefinity.Web.UI;

namespace Case.Framework.Sitefinity.Web.Controls.Abstracts
{
    public abstract class BaseSimpleView : SimpleView
    {
        protected BaseSimpleView()
        {
        }
    }
}