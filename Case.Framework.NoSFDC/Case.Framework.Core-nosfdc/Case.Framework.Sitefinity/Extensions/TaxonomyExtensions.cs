﻿namespace Case.Framework.Sitefinity.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    using Case.Framework.Sitefinity.Models;

    using Newtonsoft.Json;

    using Telerik.OpenAccess;
    using Telerik.Sitefinity.DynamicModules.Model;
    using Telerik.Sitefinity.Model;
    using Telerik.Sitefinity.Taxonomies;
    using Telerik.Sitefinity.Taxonomies.Model;

    public static class TaxonomyExtensions
    {
        public static IEnumerable<HierarchicalTaxon> FlattenHierarchy(this HierarchicalTaxon parent)
        {
            if (parent != null)
            {
                foreach (HierarchicalTaxon subtaxa in parent.Subtaxa)
                {
                    yield return subtaxa;
                    foreach (HierarchicalTaxon hierarchicalTaxon in subtaxa.FlattenHierarchy())
                    {
                        yield return hierarchicalTaxon;
                    }
                }
            }
        }

        public static string FlattenTitles(this IEnumerable<TaxonModel> items, char seperator = ',', bool appendSpace = true)
        {
            return string.Join(string.Concat((appendSpace ? " " : string.Empty), seperator),
                from c in items
                select c.Title).TrimStart(new char[0]);
        }

        public static string FlattenToString(this IEnumerable<HierarchicalTaxon> items, char seperator = ',', bool appendSpace = true)
        {
            string empty = string.Empty;
            foreach (HierarchicalTaxon item in items)
            {
                empty = string.Concat(empty, string.Format("{0}{1}{2}", item.Title, seperator, (appendSpace ? " " : "")));
            }
            return empty.Trim().TrimEnd(new char[] { seperator });
        }

        public static string FlattenUrlNames(this IEnumerable<TaxonModel> items, char seperator = ',', bool appendSpace = false)
        {
            return string.Join(string.Concat((appendSpace ? " " : string.Empty), seperator),
                from c in items
                select c.Slug).TrimStart(new char[0]);
        }

        public static List<HierarchicalTaxon> GetCategories(this DynamicContent item)
        {
            TrackedList<Guid> value = item.GetValue<TrackedList<Guid>>("Category");
            HierarchicalTaxonomy taxonomy = TaxonomyManager.GetManager().GetTaxonomy<HierarchicalTaxonomy>(TaxonomyManager.CategoriesTaxonomyId);
            return 
                (from x in taxonomy.Taxa
                where value.Contains(x.Id)
                select (HierarchicalTaxon)x).ToList();
        }

        public static HierarchicalTaxon GetFirstParentTaxon(this HierarchicalTaxon currentTaxon, string textToFind, HierarchicalTaxonCompareType type, bool isCaseSensitive)
        {
            StringComparison stringComparison = (isCaseSensitive ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase);
            while (currentTaxon != null)
            {
                if (TextInTaxonTitle(currentTaxon.Title, textToFind, type, stringComparison))
                {
                    return currentTaxon;
                }
                currentTaxon = currentTaxon.Parent;
            }
            return null;
        }

        public static List<HierarchicalTaxon> GetParentTaxa(this HierarchicalTaxon currentTaxon)
        {
            List<HierarchicalTaxon> hierarchicalTaxons = new List<HierarchicalTaxon>();
            if (currentTaxon.Parent != null)
            {
                HierarchicalTaxon parent = currentTaxon.Parent;
                hierarchicalTaxons.Add(parent);
                while (parent.HasParent())
                {
                    parent = parent.Parent;
                    hierarchicalTaxons.Add(parent);
                }
            }
            return hierarchicalTaxons;
        }

        public static HierarchicalTaxon GetRootTaxon(this HierarchicalTaxon currentTaxon)
        {
            if (currentTaxon.Parent == null)
            {
                return currentTaxon;
            }
            HierarchicalTaxon parent = currentTaxon.Parent;
            while (parent.HasParent())
            {
                parent = parent.Parent;
            }
            return parent;
        }

        public static List<Taxon> GetTags(this DynamicContent item)
        {
            TrackedList<Guid> value = item.GetValue<TrackedList<Guid>>("Tags");
            Taxonomy taxonomy = TaxonomyManager.GetManager().GetTaxonomy<Taxonomy>(TaxonomyManager.TagsTaxonomyId);
            return (
                from x in taxonomy.Taxa
                where value.Contains(x.Id)
                select x).ToList<Taxon>();
        }

        public static T GetValue<T>(this Taxon currentTaxon, string key, bool createIfDoesntExist = false, bool isJson = false)
        {
            if (currentTaxon.Attributes.ContainsKey(key))
            {
                if (isJson)
                {
                    return JsonConvert.DeserializeObject<T>(currentTaxon.Attributes[key]);
                }
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(currentTaxon.Attributes[key]);
            }
            if (createIfDoesntExist)
            {
                T t = default(T);
                var str = (t == null ? "null" : default(T).ToString());
                currentTaxon.Attributes.Add(key, str);
            }
            return default(T);
        }

        public static bool HasAttribute(this Taxon currentTaxon, string key)
        {
            if (currentTaxon == null || currentTaxon.Attributes == null)
            {
                return false;
            }
            return currentTaxon.Attributes.ContainsKey(key);
        }

        public static void SetValue(this Taxon currentTaxon, string key, object value, bool isJson = false)
        {
            string str = (isJson ? JsonConvert.SerializeObject(value) : value.ToString());
            if (str.Length >= 255)
            {
                throw new ArgumentOutOfRangeException("value", "Data length exceeded, Max 255");
            }
            if (!currentTaxon.Attributes.ContainsKey(key))
            {
                currentTaxon.Attributes.Add(key, str);
                return;
            }
            currentTaxon.Attributes[key] = str;
        }

        private static bool HasParent(this HierarchicalTaxon currentTaxon)
        {
            return currentTaxon.Parent != null;
        }

        private static bool TextInTaxonTitle(string taxonTitle, string textToFind, HierarchicalTaxonCompareType type, StringComparison compareType)
        {
            switch (type)
            {
                case HierarchicalTaxonCompareType.Equals:
                    {
                        return taxonTitle.Equals(textToFind, compareType);
                    }
                case HierarchicalTaxonCompareType.StartsWith:
                    {
                        return taxonTitle.StartsWith(textToFind, compareType);
                    }
                case HierarchicalTaxonCompareType.EndsWith:
                    {
                        return taxonTitle.EndsWith(textToFind, compareType);
                    }
                case HierarchicalTaxonCompareType.Contains:
                    {
                        return taxonTitle.Contains(textToFind);
                    }
            }
            return false;
        }
    }
}