﻿using Case.Framework.Core.Utilities;
using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Telerik.OpenAccess;
using Telerik.Sitefinity;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Extensions
{
    public static class ContentExtensions
    {
        public static bool GetBoolean(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return false;
            }
            return Convert.ToBoolean(item.GetValue(fieldName), CultureInfo.CurrentCulture);
        }

        public static DateTime GetDateTime(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return DateTime.MinValue;
            }
            return Convert.ToDateTime(item.GetValue(fieldName), CultureInfo.CurrentCulture);
        }

        public static DateTime? GetDateTimeSafe(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return null;
            }
            DateTime dateTime = Convert.ToDateTime(item.GetValue(fieldName), CultureInfo.CurrentCulture);
            if (dateTime != DateTime.MinValue)
            {
                return new DateTime?(dateTime);
            }
            return null;
        }

        public static string GetDescription(this DynamicContent item)
        {
            return item.GetStringSafe("Description", "");
        }

        public static DocumentModel GetDocument(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return null;
            }
            Document document = item.GetRelatedItems<Document>(fieldName).FirstOrDefault<Document>();
            if (document == null)
            {
                return null;
            }
            return new DocumentModel(document);
        }

        public static IQueryable<DocumentModel> GetDocuments(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return Enumerable.Empty<DocumentModel>().AsQueryable<DocumentModel>();
            }
            return
                from x in item.GetRelatedItems<Document>(fieldName)
                select new DocumentModel(x);
        }

        public static string GetFullUrl(this ILocatable content, Guid? defaultPageId = null)
        {
            string empty = string.Empty;
            if (!defaultPageId.HasValue)
            {
                empty = content.Urls.FirstOrDefault<UrlData>().Url;
            }
            else
            {
                PageModel byId = CaseManagers.Pages.GetById(defaultPageId.GetValueOrDefault());
                if (byId != null)
                {
                    empty = byId.Url;
                }
            }
            return WebHelper.ResolveUrl(empty);
        }

        public static Telerik.Sitefinity.Pages.Model.PageNode GetLinkedPage(this IDynamicFieldsContainer item, string fieldName)
        {
           
            if (!item.DoesFieldExist(fieldName))
            {
                return null;
            }
            IDataItem url = item.GetRelatedItems(fieldName).FirstOrDefault();
            

            if (url == null){
                return null;
            }
            var page = (Telerik.Sitefinity.Pages.Model.PageNode)url;

            return page;
        }



        public static ImageModel GetImage(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return null;
            }
            Image image = item.GetRelatedItems<Image>(fieldName).FirstOrDefault<Image>();
            if (image == null)
            {
                return null;
            }
            return new ImageModel(image);
        }

        public static IQueryable<ImageModel> GetImages(this IDynamicFieldsContainer item, string fieldName)
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return Enumerable.Empty<ImageModel>().AsQueryable<ImageModel>();
            }
            return
                from x in item.GetRelatedItems<Image>(fieldName)
                select new ImageModel(x);
        }

        public static string GetStringSafe(this IDynamicFieldsContainer item, string fieldName, string defaultValue = "")
        {
            if (!item.DoesFieldExist(fieldName))
            {
                return defaultValue;
            }
            string str = Convert.ToString(item.GetValue(fieldName), CultureInfo.CurrentCulture);
            if (string.IsNullOrEmpty(str))
            {
                return defaultValue;
            }
            return str;
        }

        public static List<TaxonModel> GetTaxa(this IDynamicFieldsContainer content, string taxonomyField)
        {
            TaxonomyManager manager = TaxonomyManager.GetManager();
            List<TaxonModel> taxonModels = new List<TaxonModel>();
            if (content.DoesFieldExist(taxonomyField))
            {
                TrackedList<Guid> value = content.GetValue<TrackedList<Guid>>(taxonomyField);
                if (value.Any<Guid>())
                {
                    foreach (Guid guid in value)
                    {
                        taxonModels.Add(new TaxonModel(manager.GetTaxon(guid)));
                    }
                }
            }
            return taxonModels;
        }

        public static string GetTitle(this DynamicContent item)
        {
            return item.GetStringSafe("Title", "");
        }

        public static void SetRelation(this DynamicContent dataItem, string field, MediaModel media)
        {
            Image image;
            Video video;
            Document document;
            if (dataItem.DoesFieldExist(field))
            {
                LibrariesManager manager = LibrariesManager.GetManager();
                if (media is ImageModel)
                {
                    image = (media.Id == Guid.Empty ? manager.GetImages().FirstOrDefault<Image>((Image i) => (i.UrlName == (Lstring)media.Slug) && (int)i.Status == 2) : manager.GetImage(media.Id));
                    if (image != null)
                    {
                        dataItem.CreateRelation(image, field);
                        return;
                    }
                }
                else if (media is VideoModel)
                {
                    video = (media.Id == Guid.Empty ? manager.GetVideos().FirstOrDefault<Video>((Video i) => (i.UrlName == (Lstring)media.Slug) && (int)i.Status == 2) : manager.GetVideo(media.Id));
                    if (video != null)
                    {
                        dataItem.CreateRelation(video, field);
                        return;
                    }
                }
                else if (media is DocumentModel)
                {
                    document = (media.Id == Guid.Empty ? manager.GetDocuments().FirstOrDefault<Document>((Document i) => (i.UrlName == (Lstring)media.Slug) && (int)i.Status == 2) : manager.GetDocument(media.Id));
                    if (document != null)
                    {
                        dataItem.CreateRelation(document, field);
                    }
                }
            }
        }

        public static void SetTaxa(this IOrganizable dataItem, string field, List<TaxonModel> list)
        {
            dataItem.Organizer.Clear(field);
            if (list.Any<TaxonModel>())
            {
                using (TaxonomyManager manager = TaxonomyManager.GetManager())
                {
                    IQueryable<ITaxon> taxa =
                        from t in manager.GetTaxa<ITaxon>()
                        where list.Any<TaxonModel>((TaxonModel x) => (x.Id == t.Id) || ((Lstring)x.Slug == t.UrlName))
                        select t;
                    dataItem.Organizer.AddTaxa(field, (
                        from t in taxa
                        select t.Id).ToArray<Guid>());
                }
            }
        }

        public static void TrySetValue(this IDynamicFieldsContainer dataItem, string field, object value)
        {
            if (dataItem.DoesFieldExist(field))
            {
                try
                {
                    dataItem.SetValue(field, value);
                }
                catch
                {
                    // ignored
                }
            }
        }
    }
}