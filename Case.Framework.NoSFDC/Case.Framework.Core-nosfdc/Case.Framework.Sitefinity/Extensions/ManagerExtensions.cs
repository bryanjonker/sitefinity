﻿namespace Case.Framework.Sitefinity.Extensions
{
    using System;
    using System.Linq;

    using Case.Framework.Sitefinity.Models;

    using Telerik.Sitefinity;
    using Telerik.Sitefinity.DynamicModules;
    using Telerik.Sitefinity.DynamicModules.Model;
    using Telerik.Sitefinity.Lifecycle;
    using Telerik.Sitefinity.Model;
    using Telerik.Sitefinity.Utilities.TypeConverters;

    public static class ManagerExtensions
    {
        public static bool PublishAndSave(this DynamicModuleManager manager, DynamicContent dataItem, DynamicModel parentItem = null)
        {
            bool flag;
            try
            {
                dataItem.PublicationDate = DateTime.UtcNow;
                if (dataItem.OriginalContentId != Guid.Empty)
                {
                    ILifecycleDataItem lifecycleDataItem = manager.Lifecycle.CheckIn(dataItem, null, true);
                    manager.Lifecycle.Publish(lifecycleDataItem, null);
                }
                else
                {
                    if (parentItem != null)
                    {
                        DynamicContent dynamicContent = manager.GetDataItems(TypeResolutionService.ResolveType(parentItem.MappedType)).First<DynamicContent>((DynamicContent i) => (i.UrlName == (Lstring)parentItem.Slug) && (int)i.Status == 0);
                        dataItem.SetParent(dynamicContent.Id, parentItem.MappedType);
                    }
                    manager.Lifecycle.Publish(dataItem, null);
                    dataItem.SetWorkflowStatus(manager.Provider.ApplicationName, "Published", null);
                }
                manager.SaveChanges();
                flag = true;
            }
            catch
            {
                flag = false;
            }
            return flag;
        }
    }
}