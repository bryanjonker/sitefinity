﻿using Case.Framework.Core.Utilities;
using Case.Framework.Sitefinity.Configuration;
using Case.Framework.Sitefinity.Configuration.Elements;
using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Fluent;
using Telerik.Sitefinity.Fluent.Pages;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.News;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Resources;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.Configuration;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class PageHelper
    {
        private static Page cachedPage;

        private static Page CachedPage
        {
            get
            {
                Page page = PageHelper.cachedPage;
                if (page == null)
                {
                    page = new Page();
                    PageHelper.cachedPage = page;
                }
                return page;
            }
        }

        public static string GetCurrentThemePath()
        {
            ThemeElement currentTheme = CaseManagers.Pages.GetCurrentTheme(null);
            if (currentTheme == null)
            {
                return string.Empty;
            }
            string path = currentTheme.Path;
            if (string.IsNullOrWhiteSpace(path))
            {
                return currentTheme.Namespace;
            }
            if (path.StartsWith("~/App_Data", StringComparison.OrdinalIgnoreCase))
            {
                path = string.Concat("~/", path.Substring(11));
            }
            return VirtualPathUtility.ToAbsolute(path);
        }

        public static string GetScriptsPath()
        {
            string str = (ConfigHelper.DoesSectionExist<CASEFrameworkConfig>() ? Config.Get<CASEFrameworkConfig>().Scripts.ScriptsPath : string.Empty);
            if (string.IsNullOrWhiteSpace(str))
            {
                return VirtualPathUtility.ToAbsolute("~/Scripts");
            }
            return VirtualPathUtility.ToAbsolute(str);
        }

        public static string GetWebResourceUrl(string resourceName, Type type = null)
        {
            Page currentHandler = HttpContext.Current.CurrentHandler as Page ?? PageHelper.CachedPage;
            if (type == null)
            {
                if (!resourceName.StartsWith("Telerik.Sitefinity.Resources."))
                {
                    type = typeof(PageHelper);
                }
                else
                {
                    type = typeof(Reference);
                }
            }
            if (currentHandler == null)
            {
                return string.Empty;
            }
            return currentHandler.ClientScript.GetWebResourceUrl(type, resourceName);
        }

        public static bool IsMvcPage()
        {
            try
            {
                PageSiteNode currentSiteMapNode = CaseManagers.Pages.GetCurrentSiteMapNode() as PageSiteNode;
                if (currentSiteMapNode == null)
                {
                    return false;
                }
                return currentSiteMapNode.Framework == PageTemplateFramework.Mvc;
            }
            catch { return false; }
        }

        public static bool IsBackEnd()
        {
            try
            {
                PageSiteNode currentSiteMapNode = CaseManagers.Pages.GetCurrentSiteMapNode() as PageSiteNode;

                return currentSiteMapNode?.IsBackend ?? false;
            }
            catch
            {
                return false;
            }
        }

        public static void RegisterClientSideStartup(bool? includeJquery = null, bool? includeRequireJS = null, PageNode sfPage = null, Page page = null)
        {
            if (page == null)
            {
                page = HttpContext.Current.CurrentHandler as Page;
            }
            if (sfPage == null)
            {
                try
                {
                    sfPage = CaseManagers.Pages.GetCurrentPage(null);
                }
                catch
                {
                    // ignored
                }
            }
            if (page != null && sfPage != null && !sfPage.IsBackend && !page.IsDesignMode())
            {
                var title = page.Title;
                if (page.Title.ToLower() == "article" || page.Title.ToLower() == "news")
                {
                    var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
                    if (parms != null)
                    {
                        var lastParam = parms.Last();
                        var newsManager = Telerik.Sitefinity.Modules.News.NewsManager.GetManager();
                        var article = newsManager.GetNewsItems().FirstOrDefault(x => x.UrlName == lastParam && x.Status == ContentLifecycleStatus.Live);
                        if (article != null) 
                        {
                            title = article.Title;
                        }
                    }
                }
                WebHelper.IncludeTitle(!String.IsNullOrEmpty(Config.Get<CASEFrameworkConfig>().PageTitlePrefix)
                                        ? string.Format("{0} | {1}", title, Config.Get<CASEFrameworkConfig>().PageTitlePrefix)
                                        : title,
                    "TitlePlaceHolder",
                    "title");

                ScriptManager scriptManager = PageManager.ConfigureScriptManager(page, (ScriptRef)((long)0));
                if (includeJquery.GetValueOrDefault(Config.Get<CASEFrameworkConfig>().Scripts.IncludeJquery))
                {
                    PageManager.ConfigureScriptManager(page, ScriptRef.JQuery);
                }
                if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeJqueryUI)
                {
                    PageManager.ConfigureScriptManager(page, ScriptRef.JQueryUI);
                }
                if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeTwitterBootstrap)
                {
                    scriptManager.Scripts.Add(new ScriptReference("Case.Framework.Sitefinity.Resources.Scripts.bootstrap.js.bootstrap.min.js", "Case.Framework.Sitefinity"));
                    WebHelper.IncludeCss("Case.Framework.Sitefinity.Resources.Scripts.bootstrap.css.bootstrap.min.css", typeof(PageHelper), null, null);
                }
                if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeKendoAll)
                {
                   // PageManager.ConfigureScriptManager(page, ScriptRef.KendoAll);
                    scriptManager.Scripts.Add(new ScriptReference("Telerik.Sitefinity.Resources.Scripts.Kendo.kendo.all.min.js", "Telerik.Sitefinity.Resources"));
                    if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeKendoStyles)
                    {
                        WebHelper.IncludeCss("Telerik.Sitefinity.Resources.Scripts.Kendo.styles.kendo_common_min.css", typeof(Reference), "TopStylesPlaceHolder", null);
                        WebHelper.IncludeCss("Telerik.Sitefinity.Resources.Scripts.Kendo.styles.kendo_default_min.css", typeof(Reference), "TopStylesPlaceHolder", null);
                        WebHelper.IncludeCss("http://cdn.kendostatic.com/2014.1.318/styles/kendo.dataviz.min.css", "TopStylesPlaceHolder", null);
                    }
                }
                else if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeKendoWeb)
                {
                   // PageManager.ConfigureScriptManager(page, ScriptRef.KendoWeb);
                    scriptManager.Scripts.Add(new ScriptReference("Telerik.Sitefinity.Resources.Scripts.Kendo.kendo.web.min.js", "Telerik.Sitefinity.Resources"));
                    if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeKendoStyles)
                    {
                        WebHelper.IncludeCss("Telerik.Sitefinity.Resources.Scripts.Kendo.styles.kendo_common_min.css", typeof(Reference), "TopStylesPlaceHolder", null);
                        WebHelper.IncludeCss("Telerik.Sitefinity.Resources.Scripts.Kendo.styles.kendo_default_min.css", typeof(Reference), "TopStylesPlaceHolder", null);
                    }
                }
                if (Config.Get<CASEFrameworkConfig>().Scripts.IncludeAngularJS)
                {
                    scriptManager.Scripts.Add(new ScriptReference("Case.Framework.Sitefinity.Resources.Scripts.angular.angular.min.js", "Case.Framework.Sitefinity"));
                }
                if (includeRequireJS.GetValueOrDefault(Config.Get<CASEFrameworkConfig>().Scripts.IncludeRequireJS))
                {
                    string scriptsPath = PageHelper.GetScriptsPath();
                    if (!page.IsInBrowseAndEditMode())
                    {
                        scriptManager.Scripts.Add(new ScriptReference("Telerik.Sitefinity.Resources.Scripts.RequireJS.require.min.js", "Telerik.Sitefinity.Resources"));
                    }
                    scriptManager.Scripts.Add(new ScriptReference(string.Concat(scriptsPath, "/baseurl")));
                    scriptManager.Scripts.Add(new ScriptReference(string.Concat(scriptsPath, "/basemvcurl")));
                    scriptManager.Scripts.Add(new ScriptReference(string.Concat(scriptsPath, "/basescriptsurl")));
                    scriptManager.Scripts.Add(new ScriptReference(string.Concat(scriptsPath, "/baseservicesurl")));
                    scriptManager.Scripts.Add(new ScriptReference(string.Concat(scriptsPath, "/main")));
                }
                if (!string.IsNullOrWhiteSpace(Config.Get<CASEFrameworkConfig>().Scripts.StartupScript))
                {
                    string empty = string.Empty;
                    if (!Config.Get<CASEFrameworkConfig>().Scripts.StartupScript.StartsWith("~/") && !Config.Get<CASEFrameworkConfig>().Scripts.StartupScript.StartsWith("/") && !Config.Get<CASEFrameworkConfig>().Scripts.StartupScript.ToLower().StartsWith("http"))
                    {
                        empty = string.Concat(PageHelper.GetScriptsPath(), "/");
                    }
                    scriptManager.Scripts.Add(new ScriptReference(string.Concat(empty, Config.Get<CASEFrameworkConfig>().Scripts.StartupScript)));
                }
            }
        }
    }
}