﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data.Metadata;
using Telerik.Sitefinity.Metadata.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.ModuleEditor.Web.Services.Model;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web.UI.ContentUI.Config;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Detail;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Master.Config;
using Telerik.Sitefinity.Web.UI.Fields.Config;
using Telerik.Sitefinity.Web.UI.Fields.Enums;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class UserProfileHelper
    {

        /// <summary>
        /// Register a custom field for a specific User Profile
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userProfileType"></param>
        /// <param name="fieldName"></param>
        /// <example></example>
        public static void RegisterFieldForUserProfile<T>(string userProfileType, string fieldName, bool hideField)
           where T : FieldControlDefinitionElement
        {

            // Check if the field is not already present for this content type
            var itemClrType = TypeResolutionService.ResolveType(userProfileType);

            // Specify the persistent filed CLR type (e.g. String, Guid[], ContentLink).
            // Please ensure your custom field has been properly implemented to work with that CLR type
            var persistentFieldType = typeof(string);
            var itemType = itemClrType.FullName;

            // Check to see if the field exists
            var fieldExists = GetMetaFieldsForType(itemType).SingleOrDefault(f => f.FieldName == fieldName) != null;
            if (fieldExists) return;

            // Add the metafield that will hold the data to the profile
            App.WorkWith()
                .DynamicData()
                .Type(itemClrType)
                .Field()
                   .TryCreateNew(fieldName, persistentFieldType).Do(
                    field =>
                    {
                        field.Hidden = hideField;
                        field.MetaAttributes.Add(new MetaFieldAttribute(Guid.NewGuid())
                        {
                            Name = "UserFriendlyDataType",
                            Value = "ShortText"
                        });
                        field.MetaAttributes.Add(new MetaFieldAttribute(Guid.NewGuid())
                        {
                            Name = "hidden",
                            Value = Convert.ToString(hideField)
                        });
                        field.MetaAttributes.Add(new MetaFieldAttribute(Guid.NewGuid())
                        {
                            Name = "IsCommonProperty",
                            Value = "true",
                        });
                        field.DBType = "VARCHAR";
                        field.DBLength = "255";
                    }
                    )
                .SaveChanges(true);

            // Get correct module configuration depending on item type
            var manager = ConfigManager.GetManager();

            // Suppress the security
            var suppressSecurityChecks = manager.Provider.SuppressSecurityChecks;
            manager.Provider.SuppressSecurityChecks = true;

            // Get Backend views(e.g. Edit, Create) configuration
            var section = Config.Get<ContentViewConfig>();
            var profileTypeName = userProfileType.Replace("Telerik.Sitefinity.Security.Model.", "");
            var definitionName = "ProfileType_" + profileTypeName;
            var backendSection = section.ContentViewControls[definitionName];
            var views = backendSection.ViewsConfig.Values.Where(v => v.ViewType == typeof(DetailFormView));

            foreach (DetailFormViewElement view in views)
            {

                // If there are no custom fields added before, the new field will be placed int he CustomFieldsSection
                var sectionToInsert = CustomFieldsContext.GetSection(view, CustomFieldsContext.customFieldsSectionName, itemType);
                var fieldConfigElementType = TypeResolutionService.ResolveType(typeof(T).FullName);

                // Create a new instance of our field configuration in the current view configuration
                var newElement = Activator.CreateInstance(fieldConfigElementType, new object[] { sectionToInsert.Fields }) as T;

                // Populate custom field values
                if (newElement == null) continue;
                newElement.DataFieldName = fieldName;
                newElement.FieldName = fieldName;
                newElement.Title = fieldName;
                newElement.DisplayMode = FieldDisplayMode.Write;
                newElement.Hidden = hideField;

                sectionToInsert.Fields.Add(newElement);

            }

            // Save and restart the application
            manager.SaveSection(section);
            manager.Provider.SuppressSecurityChecks = suppressSecurityChecks;
            SystemManager.RestartApplication(true);
        }
        /// <summary>
        /// Returns an IList of Meta Fields for the specified type
        /// </summary>
        public static IList<MetaField> GetMetaFieldsForType(string type)
        {

            var existingType = TypeResolutionService.ResolveType(type);
            var existingClassName = existingType.Name;
            var existingNamespace = existingType.Namespace;
            var metaType = MetadataManager.GetManager().GetMetaTypes().SingleOrDefault(dt => dt.ClassName == existingClassName && dt.Namespace == existingNamespace);

            if (metaType == null) return null;

            var fields = metaType
                .Fields;

            return fields;
        }

        public static void SaveField(User user, string fieldName, string value)
        {
            UserProfileManager profileManager = UserProfileManager.GetManager();
            SitefinityProfile userProfile = profileManager.GetUserProfile<SitefinityProfile>(user);
            if (userProfile != null)
            {
                userProfile.SetValue(fieldName, value);
                profileManager.SaveChanges();
            }

        }


    }
}
