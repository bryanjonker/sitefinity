﻿using System;
using System.Security.Principal;
using System.Web;
using Telerik.Sitefinity.Abstractions;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class ErrorHelper
    {
        public static void LogException(Exception ex)
        {
            Log.Write(ex, ConfigurationPolicy.ErrorLog);
        }

        public static void LogMessage(string message, ConfigurationPolicy configurationPolicy = 0)
        {
            Log.Write(message, configurationPolicy);
        }

        public static void LogMessage(string message, string file, string line, string url, string userAgent)
        {
            object[] objArray = new object[] { message, file, line, url, userAgent, HttpContext.Current.User.Identity.Name };
            ErrorHelper.LogMessage(string.Format("JavaScript Error: \r\n                    message: {0},\r\n                    file: {1},\r\n                    line: {2},\r\n                    url: {3},\r\n                    userAgent: {4},\r\n                    userName: {5}", objArray), ConfigurationPolicy.Default);
        }
    }
}