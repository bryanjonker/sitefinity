﻿using System;
using Telerik.Sitefinity.Services;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class SystemHelper
    {
        public static void RestartApplication()
        {
            if (SystemManager.Initializing)
            {
                return;
            }
            SystemManager.RestartApplication(true, true);
        }
    }
}