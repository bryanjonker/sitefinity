﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Metadata.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class TaxonomyHelper
    {
        public static IEnumerable<T> GetTaxonItems<T>(ITaxon taxon, int skip = 0, int take = 999)
        {
            ContentDataProviderBase provider = ManagerBase.GetMappedManager(typeof(T)).Provider as ContentDataProviderBase;
            TaxonomyPropertyDescriptor propertyDescriptor = TaxonomyManager.GetPropertyDescriptor(typeof(T), taxon);
            int? nullable = new int?(0);
            IEnumerable itemsByTaxon = provider.GetItemsByTaxon(taxon.Id, propertyDescriptor.MetaField.IsSingleTaxon, propertyDescriptor.Name, typeof(T), string.Empty, string.Empty, skip, take, ref nullable);
            return itemsByTaxon as IEnumerable<T>;
        }
    }
}