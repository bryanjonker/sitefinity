﻿using System;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class ResourceHelper
    {
        public static string ToVirtualPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return string.Empty;
            }
            return string.Concat("~/CaseSF/", path);
        }
    }
}