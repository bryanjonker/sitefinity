﻿using Case.Framework.Core.Utilities;
using Case.Framework.Sitefinity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Abstractions.VirtualPath.Configuration;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Localization.Configuration;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Configuration;
using Telerik.Sitefinity.Mvc.Proxy;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Web.Configuration;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class ConfigHelper
    {
        public static void DisableToolbox(string name)
        {
            using (ConfigManager manager = Config.GetManager())
            {
                ToolboxesConfig section = manager.GetSection<ToolboxesConfig>();
                manager.Provider.SuppressSecurityChecks = true;
                ToolboxItem toolboxItem = section.Toolboxes["PageControls"].Sections.FirstOrDefault<ToolboxSection>((ToolboxSection tb) => tb.Name == "ContentToolboxSection").Tools.FirstOrDefault<ToolboxItem>((ToolboxItem t) => t.Name == name);
                if (toolboxItem != null)
                {
                    toolboxItem.Enabled = false;
                    manager.SaveSection(section);
                }
                manager.Provider.SuppressSecurityChecks = false;
            }
        }

        public static bool DoesSectionExist<T>()
        where T : ConfigSection
        {
            return Config.GetManager().GetAllConfigSections().Any<ConfigSection>((ConfigSection c) => c.TagName.Equals(typeof(T).Name, StringComparison.OrdinalIgnoreCase));
        }

        public static bool IsModuleActive(string name)
        {
            AppModuleSettings item = Config.Get<SystemConfig>().ApplicationModules[name];
            return item.StartupType != StartupType.Disabled;
        }

        public static bool RegisterTemplate(string name, string title, string masterPage, string theme, PageTemplateFramework framework = 0)
        {
            bool flag = false;
            PageManager manager = PageManager.GetManager();
            using (ElevatedModeRegion elevatedModeRegion = new ElevatedModeRegion(manager))
            {
                PageTemplate pageTemplate = (
                    from t in manager.GetTemplates()
                    where t.Name == name
                    select t).SingleOrDefault<PageTemplate>();
                try
                {
                    CultureInfo cultureInfo = new CultureInfo(Config.Get<ResourcesConfig>().DefaultBackendCulture.UICulture);
                    if (pageTemplate == null)
                    {
                        PageTemplate customTemplatesCategoryId = manager.CreateTemplate();
                        customTemplatesCategoryId.Name = name;
                        customTemplatesCategoryId.MasterPage = masterPage;
                        customTemplatesCategoryId.Category = SiteInitializer.CustomTemplatesCategoryId;
                        customTemplatesCategoryId.Framework = framework;
                        customTemplatesCategoryId.Title[cultureInfo] = title;
                        TemplateDraft templateDraft = manager.EditTemplate(customTemplatesCategoryId.Id, cultureInfo);
                        TemplateDraft templateDraft1 = manager.TemplatesLifecycle.CheckOut(templateDraft, cultureInfo);
                        templateDraft = manager.TemplatesLifecycle.CheckIn(templateDraft1, cultureInfo, true);
                        templateDraft.Themes.SetString(cultureInfo, theme);
                        templateDraft.ApprovalWorkflowState.Value = "Published";
                        manager.TemplatesLifecycle.Publish(templateDraft, cultureInfo);
                        manager.SaveChanges();
                        flag = true;
                    }
                    else if (!pageTemplate.AvailableCultures.Contains<CultureInfo>(cultureInfo))
                    {
                        pageTemplate.Title[cultureInfo] = title;
                        TemplateDraft templateDraft2 = manager.EditTemplate(pageTemplate.Id, cultureInfo);
                        TemplateDraft templateDraft3 = manager.TemplatesLifecycle.CheckOut(templateDraft2, cultureInfo);
                        templateDraft2 = manager.TemplatesLifecycle.CheckIn(templateDraft3, cultureInfo, true);
                        templateDraft2.Themes.SetString(cultureInfo, theme);
                        templateDraft2.ApprovalWorkflowState.Value = "Published";
                        manager.TemplatesLifecycle.Publish(templateDraft2, cultureInfo);
                        manager.SaveChanges();
                        flag = true;
                    }
                }
                catch
                {
                    // ignored
                }
            }
            return flag;
        }

        public static void RegisterTheme(string name, string path)
        {
            using (ConfigManager manager = Config.GetManager())
            {
                AppearanceConfig section = manager.GetSection<AppearanceConfig>();
                if (!section.FrontendThemes.ContainsKey(name))
                {
                    ThemeElement themeElement = new ThemeElement(section.FrontendThemes)
                    {
                        Name = name,
                        Path = path
                    };
                    section.FrontendThemes.Add(themeElement);
                    manager.Provider.SuppressSecurityChecks = true;
                    manager.SaveSection(section);
                    manager.Provider.SuppressSecurityChecks = false;
                }
            }
        }

        public static void RegisterToolboxWidget<T>(string title, string description = null, string cssClass = null,
            string resourceClassId = null, string layoutTemplate = "", string sectionName = "Extras", int? sectionOrdinal = null, ToolboxType toolboxType = 0,int ordinal =99)
        {
            using (ConfigManager manager = Config.GetManager())
            {
                ToolboxesConfig section = manager.GetSection<ToolboxesConfig>();
                manager.Provider.SuppressSecurityChecks = true;
                Toolbox item = section.Toolboxes[toolboxType.ToString()];
                ToolboxSection toolboxSection = item.Sections.FirstOrDefault<ToolboxSection>((ToolboxSection x) => x.Name == sectionName);
                if (toolboxSection == null)
                {
                    ToolboxSection toolboxSection1 = new ToolboxSection(item.Sections)
                    {
                        Name = WebHelper.GenerateSafeName(sectionName, true),
                        Title = sectionName,
                        Description = sectionName,
                        Ordinal = (float)sectionOrdinal.GetValueOrDefault(ordinal)
                    };
                    toolboxSection = toolboxSection1;
                    item.Sections.Add(toolboxSection);
                }

                Type type = typeof(T);
                if (!toolboxSection.Tools.Any<ToolboxItem>((ToolboxItem t) => t.Name == type.Name))
                {
                    string str = WebHelper.GenerateSafeName(title, true);
                    if (char.IsNumber(str, 0))
                    {
                        str = string.Concat(toolboxSection.Name, str);
                    }
                    ToolboxItem toolboxItem = new ToolboxItem(toolboxSection.Tools)
                    {
                        Name = str,
                        Title = title,
                        Description = description ?? title,
                        CssClass = cssClass,
                        ResourceClassId = resourceClassId
                    };
                    ToolboxItem fullName = toolboxItem;
                    if (!typeof(Controller).IsAssignableFrom(type))
                    {
                        fullName.ControlType = type.FullName;
                    }
                    else
                    {
                        fullName.ControlType = type.FullName;
                        fullName.ControllerType = type.FullName;
                        fullName.Parameters.Add("ControllerName", type.FullName);
                    }
                    if (!string.IsNullOrWhiteSpace(layoutTemplate))
                    {
                        fullName.LayoutTemplate = layoutTemplate;
                    }
                    toolboxSection.Tools.Add(fullName);
                    manager.SaveSection(section);
                    manager.Provider.SuppressSecurityChecks = false;
                }
            }
        }

        public static void RegisterVirtualPath(string path, string assembly = "Case.Framework.Sitefinity")
        {
            VirtualPathSettingsConfig virtualPathSettingsConfig = Config.Get<VirtualPathSettingsConfig>();
            if (!virtualPathSettingsConfig.VirtualPaths.ContainsKey(path))
            {
                ConfigElementDictionary<string, VirtualPathElement> virtualPaths = virtualPathSettingsConfig.VirtualPaths;
                VirtualPathElement virtualPathElement = new VirtualPathElement(virtualPathSettingsConfig.VirtualPaths)
                {
                    VirtualPath = path,
                    ResolverName = "EmbeddedResourceResolver",
                    ResourceLocation = assembly
                };
                virtualPaths.Add(virtualPathElement);
                using (ConfigManager manager = Config.GetManager())
                {
                    manager.Provider.SuppressSecurityChecks = true;
                    manager.SaveSection(virtualPathSettingsConfig);
                    manager.Provider.SuppressSecurityChecks = false;
                }
            }
        }
    }
}