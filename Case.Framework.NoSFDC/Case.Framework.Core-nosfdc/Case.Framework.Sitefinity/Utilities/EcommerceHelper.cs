﻿using Case.Framework.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Data.Metadata;
using Telerik.Sitefinity.Ecommerce.Catalog.Model;
using Telerik.Sitefinity.Ecommerce.Orders.Model;
using Telerik.Sitefinity.Fluent;
using Telerik.Sitefinity.Fluent.DynamicData;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Ecommerce.Catalog;
using Telerik.Sitefinity.Modules.Ecommerce.Configuration;
using Telerik.Sitefinity.Modules.Ecommerce.Events;
using Telerik.Sitefinity.Modules.Ecommerce.Orders;
using Telerik.Sitefinity.Modules.Ecommerce.Orders.Configuration;
using Telerik.Sitefinity.Modules.GenericContent.Configuration;
using Telerik.Sitefinity.Web.UI;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class EcommerceHelper
    {
        public static void CheckoutHandler(IEcommerceCheckoutPageChangingEvent evt)
        {
            if (evt.ShoppingCartId == Guid.Empty)
            {
                return;
            }
            if (evt.CurrentStepIndex != 3)
            {
                return;
            }
            EcommerceHelper.SaveCustomOrderFields(evt);
        }

        public static void CreateCustomOrderFields()
        {
            MetadataManager manager = MetadataManager.GetManager();
            if (manager.GetMetaType(typeof(Order)) == null)
            {
                manager.CreateMetaType(typeof(Order));
                manager.SaveChanges();
            }
            if (manager.GetMetaType(typeof(CartOrder)) == null)
            {
                manager.CreateMetaType(typeof(CartOrder));
                manager.SaveChanges();
            }
            foreach (string customOrderField in EcommerceHelper.GetCustomOrderFields())
            {
                App.WorkWith().DynamicData().Type(typeof(Order)).Field().TryCreateNew(customOrderField, typeof(string)).SaveChanges(true);
                App.WorkWith().DynamicData().Type(typeof(CartOrder)).Field().TryCreateNew(customOrderField, typeof(string)).SaveChanges(true);
            }
        }

        public static IEnumerable<string> GetCustomOrderFields()
        {
            string str;
            OrderConfig section = ConfigManager.GetManager().GetSection<OrderConfig>();
            str = (section.DefaultProvider == "OpenAccessDataProvider" ? "OpenAccessProvider" : section.DefaultProvider);
            string item = section.Providers[str].Parameters["customFields"];
            List<string> strs = new List<string>();
            if (!string.IsNullOrWhiteSpace(item))
            {
                string[] strArrays = item.Split(new char[] { ',' });
                for (int i = 0; i < (int)strArrays.Length; i++)
                {
                    strs.Add(strArrays[i].Trim());
                }
            }
            return strs;
        }

        public static void OrderNotificationHandler(Order order)
        {
            CatalogManager manager = CatalogManager.GetManager();
            foreach (OrderDetail detail in order.Details)
            {
                Product product = manager.GetProduct(detail.ProductId);
                if (!product.DoesFieldExist("ConfirmationEmail"))
                {
                    continue;
                }
                string merchantEmail = ConfigManager.GetManager().GetSection<EcommerceConfig>().MerchantEmail;
                string[] customerEmail = new string[] { merchantEmail, order.Customer.CustomerEmail };
                string empty = string.Empty;
                string value = string.Empty;
                if (!product.DoesFieldExist("ConfirmationEmail"))
                {
                    continue;
                }
                value = product.GetValue<string>("ConfirmationEmail");
                if (string.IsNullOrWhiteSpace(value))
                {
                    continue;
                }
                if (value.Contains("{{OrderID}}"))
                {
                    Guid id = order.Id;
                    value = value.Replace("{{OrderID}}", id.ToString());
                }
                if (value.Contains("{{OrderNumber}}"))
                {
                    int orderNumber = order.OrderNumber;
                    value = value.Replace("{{OrderNumber}}", orderNumber.ToString());
                }
                if (value.Contains("{{FirstName}}"))
                {
                    value = value.Replace("{{FirstName}}", order.Customer.CustomerFirstName);
                }
                if (value.Contains("{{LastName}}"))
                {
                    value = value.Replace("{{LastName}}", order.Customer.CustomerLastName);
                }
                if (value.Contains("{{Email}}"))
                {
                    value = value.Replace("{{Email}}", order.Customer.CustomerEmail);
                }
                if (value.Contains("{{Product.Title}}"))
                {
                    value = value.Replace("{{Product.Title}}", product.Title);
                }
                if (value.Contains("{{Product.StartDate}}") && product.DoesFieldExist("StartDate"))
                {
                    DateTime dateTime = product.GetValue<DateTime>("StartDate");
                    value = value.Replace("{{Product.StartDate}}", dateTime.ToLongDateString());
                }
                if (value.Contains("{{Product.EndDate}}") && product.DoesFieldExist("EndDate"))
                {
                    DateTime value1 = product.GetValue<DateTime>("EndDate");
                    value = value.Replace("{{Product.EndDate}}", value1.ToLongDateString());
                }
                foreach (string customOrderField in EcommerceHelper.GetCustomOrderFields())
                {
                    string str = string.Concat("{{Order.", customOrderField, "}}");
                    if (!value.Contains(str) || !order.DoesFieldExist(customOrderField))
                    {
                        continue;
                    }
                    value = value.Replace(str, order.GetValue<string>(customOrderField));
                }
                if (product.DoesFieldExist("ConfirmationSubject"))
                {
                    empty = product.GetValue<string>("ConfirmationSubject");
                    if (!string.IsNullOrWhiteSpace(empty))
                    {
                        if (empty.Contains("{{OrderID}}"))
                        {
                            Guid guid = order.Id;
                            empty = empty.Replace("{{OrderID}}", guid.ToString());
                        }
                        if (empty.Contains("{{OrderNumber}}"))
                        {
                            int num = order.OrderNumber;
                            empty = empty.Replace("{{OrderNumber}}", num.ToString());
                        }
                        if (empty.Contains("{{FirstName}}"))
                        {
                            empty = empty.Replace("{{FirstName}}", order.Customer.CustomerFirstName);
                        }
                        if (empty.Contains("{{LastName}}"))
                        {
                            empty = empty.Replace("{{LastName}}", order.Customer.CustomerLastName);
                        }
                        if (empty.Contains("{{Email}}"))
                        {
                            empty = empty.Replace("{{Email}}", order.Customer.CustomerEmail);
                        }
                        if (empty.Contains("{{Product.Title}}"))
                        {
                            empty = empty.Replace("{{Product.Title}}", product.Title);
                        }
                    }
                }
                if (string.IsNullOrWhiteSpace(empty))
                {
                    empty = "Thank you for your order!";
                }
                MailHelper.SendEmail(merchantEmail, customerEmail, empty, value);
            }
        }

        public static void OrderPlacedHandler(Guid orderId)
        {
            Order order = OrdersManager.GetManager().GetOrder(orderId);
            EcommerceHelper.OrderNotificationHandler(order);
            if (order.Total == new decimal(0))
            {
                order.OrderStatus = OrderStatus.Paid;
            }
        }

        public static void SaveCustomOrderField(Guid cartId, GenericContainer container, string name)
        {
            TextBox control = container.GetControl<TextBox>(name, false);
            if (control != null)
            {
                EcommerceHelper.SaveCustomOrderField(cartId, name, control.Text);
            }
        }

        public static void SaveCustomOrderField(Guid cartId, string name, string value)
        {
            OrdersManager manager = OrdersManager.GetManager();
            CartOrder cartOrder = manager.GetCartOrder(cartId);
            if (cartOrder != null)
            {
                MetafieldPropertyDescriptor item = TypeDescriptor.GetProperties(cartOrder)[name] as MetafieldPropertyDescriptor;
                if (item != null)
                {
                    item.SetValue(cartOrder, value);
                    manager.SaveChanges();
                }
            }
        }

        public static void SaveCustomOrderFields(IEcommerceCheckoutPageChangingEvent evt)
        {
            foreach (string customOrderField in EcommerceHelper.GetCustomOrderFields())
            {
                EcommerceHelper.SaveCustomOrderField(evt.ShoppingCartId, evt.Container, customOrderField);
            }
        }
    }
}