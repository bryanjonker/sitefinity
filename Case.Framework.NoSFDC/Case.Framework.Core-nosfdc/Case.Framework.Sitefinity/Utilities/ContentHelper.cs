﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Data.Metadata;
using Telerik.Sitefinity.Fluent;
using Telerik.Sitefinity.Fluent.DynamicData;
using Telerik.Sitefinity.Metadata.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.ModuleEditor.Web.Services.Model;
using Telerik.Sitefinity.Modules.Blogs.Configuration;
using Telerik.Sitefinity.Modules.Events;
using Telerik.Sitefinity.Modules.Events.Configuration;
using Telerik.Sitefinity.Modules.GenericContent.Configuration;
using Telerik.Sitefinity.Modules.Libraries.Configuration;
using Telerik.Sitefinity.Modules.Libraries.Documents;
using Telerik.Sitefinity.Modules.Libraries.Videos;
using Telerik.Sitefinity.Modules.News.Configuration;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web.UI.ContentUI.Config;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Detail;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Master.Config;
using Telerik.Sitefinity.Web.UI.Fields.Config;
using Telerik.Sitefinity.Web.UI.Fields.Enums;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class ContentHelper
    {
        public const string URL_NAME_CHARS_TO_REPLACE = "[^\\w\\-\\!\\$\\'\\(\\)\\=\\@\\d_]+";

        public const string URL_NAME_REPLACE_STRING = "-";

        public static bool DoesTypeContainField(Type dynamicType, string fieldName)
        {
            return App.WorkWith().DynamicData().Type(dynamicType).Get().Fields.Any<MetaField>((MetaField f) => f.FieldName == fieldName);
        }

        public static bool DoesTypeContainsField(string dynamicType, string fieldName)
        {
            return ContentHelper.DoesTypeContainField(TypeResolutionService.ResolveType(dynamicType), fieldName);
        }

        public static string GenerateUrlName(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }
            return Regex.Replace(value.ToLower(), "[^\\w\\-\\!\\$\\'\\(\\)\\=\\@\\d_]+", "-");
        }

        public static IManager GetContentManager(object item)
        {
            return ManagerBase.GetMappedManager(item.GetType());
        }

        public static IList<MetaField> GetMetaFieldsForType(string type)
        {
            Type type1 = TypeResolutionService.ResolveType(type);
            string name = type1.Name;
            string @namespace = type1.Namespace;
            MetaType metaType = (
                from dt in MetadataManager.GetManager().GetMetaTypes()
                where (dt.ClassName == name) && (dt.Namespace == @namespace)
                select dt).FirstOrDefault<MetaType>();
            return metaType.Fields;
        }

        public static string GetModuleBackendDefinitionName(string itemType)
        {
            string backendDefinitionName;
            string str = itemType;
            string str1 = str;
            if (str != null)
            {
                switch (str1)
                {
                    case "Telerik.Sitefinity.Blogs.Model.BlogPost":
                        {
                            backendDefinitionName = "PostsBackend";
                            break;
                        }
                    case "Telerik.Sitefinity.Events.Model.Event":
                        {
                            backendDefinitionName = EventsDefinitions.BackendDefinitionName;
                            break;
                        }
                    case "Telerik.Sitefinity.News.Model.NewsItem":
                        {
                            backendDefinitionName = "NewsBackend";
                            break;
                        }
                    case "Telerik.Sitefinity.Libraries.Model.Document":
                        {
                            backendDefinitionName = DocumentsDefinitions.BackendDefinitionName;
                            break;
                        }
                    case "Telerik.Sitefinity.Libraries.Model.Image":
                        {
                            backendDefinitionName = "ImagesBackend";
                            break;
                        }
                    case "Telerik.Sitefinity.Libraries.Model.Video":
                        {
                            backendDefinitionName = VideosDefinitions.BackendVideosDefinitionName;
                            break;
                        }
                    default:
                        {
                            throw new NotImplementedException(string.Format("Unsupported type {0}", itemType));
                        }
                }
                return backendDefinitionName;
            }
            throw new NotImplementedException(string.Format("Unsupported type {0}", itemType));
        }

        public static ContentModuleConfigBase GetModuleConfigSection(string itemType)
        {
            ContentModuleConfigBase contentModuleConfigBase;
            string str = itemType;
            string str1 = str;
            if (str != null)
            {
                switch (str1)
                {
                    case "Telerik.Sitefinity.Blogs.Model.BlogPost":
                        {
                            contentModuleConfigBase = Config.Get<BlogsConfig>();
                            break;
                        }
                    case "Telerik.Sitefinity.Events.Model.Event":
                        {
                            contentModuleConfigBase = Config.Get<EventsConfig>();
                            break;
                        }
                    case "Telerik.Sitefinity.News.Model.NewsItem":
                        {
                            contentModuleConfigBase = Config.Get<NewsConfig>();
                            break;
                        }
                    case "Telerik.Sitefinity.Libraries.Model.Document":
                    case "Telerik.Sitefinity.Libraries.Model.Image":
                    case "Telerik.Sitefinity.Libraries.Model.Video":
                        {
                            contentModuleConfigBase = Config.Get<LibrariesConfig>();
                            break;
                        }
                    default:
                        {
                            throw new NotImplementedException(string.Format("Unsupported type {0}", itemType));
                        }
                }
                return contentModuleConfigBase;
            }
            throw new NotImplementedException(string.Format("Unsupported type {0}", itemType));
        }

        public static void RegisterFieldSelector<T, E>(string fieldName)
            where T : IContent
            where E : FieldControlDefinitionElement
        {
            string fullName = typeof(T).FullName;
            if ((
                from f in ContentHelper.GetMetaFieldsForType(fullName)
                where f.FieldName == fieldName
                select f).SingleOrDefault<MetaField>() == null)
            {
                App.WorkWith().DynamicData().Type(typeof(T)).Field().TryCreateNew(fieldName, typeof(Guid[])).SaveChanges(true);
                ConfigManager manager = ConfigManager.GetManager();
                bool suppressSecurityChecks = manager.Provider.SuppressSecurityChecks;
                manager.Provider.SuppressSecurityChecks = true;
                ContentModuleConfigBase moduleConfigSection = ContentHelper.GetModuleConfigSection(fullName);
                string moduleBackendDefinitionName = ContentHelper.GetModuleBackendDefinitionName(fullName);
                ContentViewControlElement item = moduleConfigSection.ContentViewControls[moduleBackendDefinitionName];
                IEnumerable<ContentViewDefinitionElement> values = 
                    from v in item.ViewsConfig.Values
                    where v.ViewType == typeof(DetailFormView)
                    select v;
                foreach (DetailFormViewElement value in values)
                {
                    ContentViewSectionElement section = CustomFieldsContext.GetSection(value, CustomFieldsContext.customFieldsSectionName, fullName);
                    Type type = TypeResolutionService.ResolveType(typeof(E).FullName);
                    object[] fields = new object[] { section.Fields };
                    E nullable = (E)(Activator.CreateInstance(type, fields) as E);
                    if (nullable == null)
                    {
                        continue;
                    }
                    nullable.DataFieldName = fieldName;
                    nullable.FieldName = fieldName;
                    nullable.Title = fieldName;
                    nullable.DisplayMode = new FieldDisplayMode?(FieldDisplayMode.Write);
                    section.Fields.Add(nullable);
                }
                manager.SaveSection(moduleConfigSection);
                manager.Provider.SuppressSecurityChecks = suppressSecurityChecks;
                SystemHelper.RestartApplication();
            }
        }
    }
}