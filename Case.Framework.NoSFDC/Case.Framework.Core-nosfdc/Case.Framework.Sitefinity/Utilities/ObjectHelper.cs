﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Sitefinity.Utilities
{
    public static class ObjectHelper<T>
  where T : class
    {
        public static void SetPropertyValues(T obj, PropertyInfo pi, object value)
        {

            pi.SetValue(obj, value, null);
        }


        public static void SetPropertyValues(object obj, PropertyInfo pi, string value)
        {

            // translate properties of the class
            if (pi.PropertyType.Equals(typeof(DateTime)))
            {
                if (!string.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDateTime(value), null);

            }
            else if (pi.PropertyType.Equals(typeof(DateTime?)))
            {
                if (!string.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDateTime(value), null);

            }
            else if (pi.PropertyType.Equals(typeof(int)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToInt32(value), null);

            }
            else if (pi.PropertyType.Equals(typeof(bool)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToBoolean(value), null);
            }
            else if (pi.PropertyType.Equals(typeof(decimal)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDecimal(value), null);
            }
            else if (pi.PropertyType.Equals(typeof(double)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDouble(value), null);
            }
            else if (pi.PropertyType.Equals(typeof(string)))
            {
                pi.SetValue(obj, value, null);
            }
        }


        public static void SetPropertyValues(T obj, PropertyInfo pi, string value)
        {

            // translate properties of the class
            if (pi.PropertyType.Equals(typeof(DateTime)))
            {
                if (!string.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDateTime(value), null);

            }
            else if (pi.PropertyType.Equals(typeof(DateTime?)))
            {
                if (!string.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDateTime(value), null);

            }
            else if (pi.PropertyType.Equals(typeof(int)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToInt32(value), null);

            }
            else if (pi.PropertyType.Equals(typeof(bool)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToBoolean(value), null);
            }
            else if (pi.PropertyType.Equals(typeof(decimal)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDecimal(value), null);
            }
            else if (pi.PropertyType.Equals(typeof(double)))
            {
                if (!String.IsNullOrEmpty(value))
                    pi.SetValue(obj, Convert.ToDouble(value), null);
            }
            else if (pi.PropertyType.Equals(typeof(string)))
            {
                pi.SetValue(obj, value, null);
            }
        }
    }
}
