﻿using System;

namespace Case.Framework.Sitefinity
{
    public enum ToolboxType
    {
        PageControls,
        PageLayouts,
        FormControls
    }
}