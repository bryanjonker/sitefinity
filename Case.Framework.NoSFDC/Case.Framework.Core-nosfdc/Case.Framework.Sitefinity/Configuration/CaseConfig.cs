﻿using Case.Framework.Sitefinity.Configuration.Elements;
using System;
using System.Configuration;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;


namespace Case.Framework.Sitefinity.Configuration
{
    public class CASEFrameworkConfig : ConfigSection
    {
        [ConfigurationProperty("PageTitlePrefix", DefaultValue="")]
        [ObjectInfo(Title = "Page Title Prefix", Description = "Text that will be displayed before each page title in the browser i.e. Company - Home")]
        public string PageTitlePrefix
        {
            get
            {
                return (string)this["PageTitlePrefix"];
            }
            set
            {
                this["PageTitlePrefix"] = value;
            }
        }

        
        [ConfigurationProperty("EnableJsonP", DefaultValue = false)]
        [ObjectInfo(Title = "Enable JSONP", Description = "Specify if Web API services should be accessible over JSONP (Web API must be enabled, requires application restart).")]
        public bool EnableJsonP
        {
            get
            {
                return (bool)this["EnableJsonP"];
            }
            set
            {
                this["EnableJsonP"] = value;
            }
        }

        [ConfigurationProperty("EnableWebApi", DefaultValue = true)]
        [ObjectInfo(Title = "Enable Web API", Description = "Specify if Web API should be enabled for web services (requires application restart).")]
        public bool EnableWebApi
        {
            get
            {
                return (bool)this["EnableWebApi"];
            }
            set
            {
                this["EnableWebApi"] = value;
            }
        }

        [ConfigurationProperty("EnableAuthenticationProvider", DefaultValue = false)]
        [ObjectInfo(Title = "Enable Salesforce Authentication Providers", Description = "Specify if Salesforce authentication providers should be enabled (requires application restart).")]
        public bool EnableAuthenticationProvider
        {
            get
            {
                return (bool)this["EnableAuthenticationProvider"];
            }
            set
            {
                this["EnableAuthenticationProvider"] = value;
            }
        }

  
        [ConfigurationProperty("Scripts")]
        [ObjectInfo(Title = "Scripts", Description = "Settings for the scripts.")]
        public ScriptsElement Scripts
        {
            get
            {
                return (ScriptsElement)this["Scripts"];
            }
        }
        [ConfigurationProperty("CustomFields")]
        [ObjectInfo(Title = "Ecommerce Custom Fields", Description = "Comma seperated value of custom ecommerce cart order fields and order fields.")]
        public EcommerceCustomFieldsElement ECommerceFields
        {
            get
            {
                return (EcommerceCustomFieldsElement)this["CustomFields"];
            }
        }

      
        [ConfigurationCollection(typeof(BundleMapping), AddItemName = "type")]
        [ConfigurationProperty("bundlesMapping", IsDefaultCollection = true)]
        [ObjectInfo(Title = "Bundles", Description = "Web optimization bundle configuration.")]
        public virtual ConfigElementDictionary<string, BundleMapping> BundlesMapping
        {
            get
            {
                return (ConfigElementDictionary<string, BundleMapping>)this["bundlesMapping"];
            }
            set
            {
                this["bundlesMapping"] = value;
            }
        }

        [ConfigurationCollection(typeof(SyncConfigElement), AddItemName = "syncJobItem")]
        [ConfigurationProperty("syncConfig", IsDefaultCollection = true)]
        [ObjectInfo(Title = "Data Sync Schedule", Description = "Data Syncronization Schedule.")]
        public virtual ConfigElementDictionary<string, SyncConfigElement> SyncJobs
        {
            get
            {
                return (ConfigElementDictionary<string, SyncConfigElement>)this["syncConfig"];
            }
            set
            {
                this["syncConfig"] = value;
            }
        }


        public CASEFrameworkConfig()
        {
        }

        protected override void OnPropertiesInitialized()
        {
            base.OnPropertiesInitialized();
        }

      
    }
}
