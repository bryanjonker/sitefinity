﻿using System;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Web.Configuration;


namespace Case.Framework.Sitefinity.Configuration.Elements
{
    public class BundleMapping : ConfigElement
    {
        
        [ConfigurationProperty("AssetPath", IsRequired = true, IsKey = true)]
        [ObjectInfo(Title = "Asset Path", Description = "Path to assets i.e. assets/styles")]
        public virtual string AssetPath
        {
            get
            {
                return (string)this["AssetPath"];
            }
            set
            {
                this["AssetPath"] = value;
            }
        }
        [ConfigurationProperty("BrowserType", DefaultValue = BrowserType.All)]
        [ObjectInfo(Title = "BrowserType", Description = "What type of browser is this bundle for (IE,Firefox,Chrome,Safari,Opera,All)")]
        public virtual BrowserType BrowserType
        {
            get
            {
                return (BrowserType)this["BrowserType"];
            }
            set
            {
                this["BrowserType"] = value;
            }
        }


        [ConfigurationProperty("BundleType", IsRequired = true,DefaultValue=BundleType.CSS)]
        [ObjectInfo(Title="Bundle Type", Description="What type of bundle is this (CSS or Script)")]
        public virtual BundleType BundleType
        {
            get
            {
                return (BundleType)this["BundleType"];
            }
            set
            {
                this["BundleType"] = value;
            }
        }
     
        public BundleMapping(ConfigElement parent)
            : base(parent)
        {
        }
    }
}
