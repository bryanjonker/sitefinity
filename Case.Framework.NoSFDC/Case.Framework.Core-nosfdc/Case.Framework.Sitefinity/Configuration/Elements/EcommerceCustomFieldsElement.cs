﻿using System;
using System.ComponentModel;
using System.Configuration;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;

namespace Case.Framework.Sitefinity.Configuration.Elements
{
    public class EcommerceCustomFieldsElement : ConfigElement
    {
        [ConfigurationProperty("CustomFields", DefaultValue = null)]
        [ObjectInfo(Title = "Custom Fields", Description = "Comma seperated value of custom ecommerce cart order fields and order fields.")]
        public string CustomFields
        {
            get
            {
                return (string)this["CustomFields"];
            }
            set
            {
                this["CustomFields"] = value;
            }
        }

        public EcommerceCustomFieldsElement(ConfigElement parent)
            : base(parent)
        {
        }
    }
}
