﻿using System;
using System.Configuration;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;

namespace Case.Framework.Sitefinity.Configuration.Elements
{
    public class ScriptsElement : ConfigElement
    {
        [ConfigurationProperty("ScriptsPath", DefaultValue = "~/Scripts")]
        [ObjectInfo(Title = "Scripts Path", Description = "Root path used for scripts folder.")]
        public string ScriptsPath
        {
            get
            {
                return (string)this["ScriptsPath"];
            }
            set
            {
                this["ScriptsPath"] = value;
            }
        }

        [ConfigurationProperty("StartupScript", DefaultValue = "")]
        [ObjectInfo(Title = "Startup Script", Description = "Specify script file to load on page startup")]
        public string StartupScript
        {
            get
            {
                return (string)this["StartupScript"];
            }
            set
            {
                this["StartupScript"] = value;
            }
        }

        [ConfigurationProperty("IncludeAngularJS", DefaultValue = false)]
        [ObjectInfo(Title = "Include AngularJS", Description = "Includes AngularJS on every page")]
        public bool IncludeAngularJS
        {
            get
            {
                return (bool)this["IncludeAngularJS"];
            }
            set
            {
                this["IncludeAngularJS"] = value;
            }
        }

        [ConfigurationProperty("IncludeJquery", DefaultValue = true)]
        [ObjectInfo(Title = "Include jQuery", Description = "Includes jQuery on every page")]
        public bool IncludeJquery
        {
            get
            {
                return (bool)this["IncludeJquery"];
            }
            set
            {
                this["IncludeJquery"] = value;
            }
        }
        [ConfigurationProperty("IncludeJqueryUI", DefaultValue = true)]
        [ObjectInfo(Title = "Include jQuery UI", Description = "Includes jQuery UI on every page")]
        public bool IncludeJqueryUI
        {
            get
            {
                return (bool)this["IncludeJqueryUI"];
            }
            set
            {
                this["IncludeJqueryUI"] = value;
            }
        }



        [ConfigurationProperty("IncludeKendoAll", DefaultValue = false)]
        [ObjectInfo(Title = "Include Kendo UI (All)", Description = "Includes Kendo UI Web, DataViz, and Mobile on every page (will override including Kendo UI Web)")]
        public bool IncludeKendoAll
        {
            get
            {
                return (bool)this["IncludeKendoAll"];
            }
            set
            {
                this["IncludeKendoAll"] = value;
            }
        }

        [ConfigurationProperty("IncludeKendoStyles", DefaultValue = true)]
        [ObjectInfo(Title = "Include Kendo UI Styles", Description = "Includes Kendo UI styles on every page (only default theme available)")]
        public bool IncludeKendoStyles
        {
            get
            {
                return (bool)this["IncludeKendoStyles"];
            }
            set
            {
                this["IncludeKendoStyles"] = value;
            }
        }

        [ConfigurationProperty("IncludeKendoWeb", DefaultValue = true)]
        [ObjectInfo(Title = "Include Kendo UI (Web)", Description = "Includes Kendo UI Web on every page")]
        public bool IncludeKendoWeb
        {
            get
            {
                return (bool)this["IncludeKendoWeb"];
            }
            set
            {
                this["IncludeKendoWeb"] = value;
            }
        }

        [ConfigurationProperty("IncludeRequireJS", DefaultValue = true)]
        [ObjectInfo(Title = "Include RequireJS", Description = "Includes RequireJS on every page")]
        public bool IncludeRequireJS
        {
            get
            {
                return (bool)this["IncludeRequireJS"];
            }
            set
            {
                this["IncludeRequireJS"] = value;
            }
        }

        [ConfigurationProperty("IncludeTwitterBootstrap", DefaultValue = false)]
        [ObjectInfo(Title = "Include Twitter Bootstrap", Description = "Includes Twitter Bootstrap on every page")]
        public bool IncludeTwitterBootstrap
        {
            get
            {
                return (bool)this["IncludeTwitterBootstrap"];
            }
            set
            {
                this["IncludeTwitterBootstrap"] = value;
            }
        }

    

        public ScriptsElement(ConfigElement parent)
            : base(parent)
        {
        }
    }
}
