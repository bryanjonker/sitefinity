﻿using System;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Web.Configuration;


namespace Case.Framework.Sitefinity.Configuration.Elements
{
    public class SyncConfigElement : ConfigElement
    {
        
        [ConfigurationProperty("jobName", IsRequired = true, IsKey = true)]
        [ObjectInfo(Title = "Job Name", Description = "Name of scheduled job")]
        public virtual string JobName
        {
            get
            {
                return (string)this["jobName"];
            }
            set
            {
                this["jobName"] = value;
            }
        }
        [ConfigurationProperty("SyncDuration", IsRequired = false, DefaultValue = 10)]
        [ObjectInfo(Title = "Sync duration", Description = "How often to sync data (In Minutes).")]
        public virtual int SyncDuration
        {
            get
            {
                return (int)this["SyncDuration"];
            }
            set
            {
                this["SyncDuration"] = value;
            }
        }

        [ConfigurationProperty("DataSourceType", IsRequired = false, DefaultValue = DatasourceType.Faculty)]
        [ObjectInfo(Title = "DataSourceType", Description = "Datasource Type (Faculty,Courses,Grants,Units,Events)")]
        public virtual DatasourceType DataSourceType
        {
            get
            {
                return (DatasourceType)this["DataSourceType"];
            }
            set
            {
                this["DataSourceType"] = value;
            }
        }

        [ConfigurationProperty("ImportRecords", IsRequired = false, DefaultValue = 0)]
        [ObjectInfo(Title = "How many records?", Description = "How many records to import 0 = all")]
        public virtual int ImportRecords
        {
            get
            {
                return (int)this["ImportRecords"];
            }
            set
            {
                this["ImportRecords"] = value;
            }
        }

        [ConfigurationProperty("ImportAllEvents", IsRequired = false, DefaultValue = null)]
        [ObjectInfo(Title = "Import all events date.", Description = "This day the sync process will import all events(since 2012)")]
        public virtual DateTime? ImportAllEventsDate
        {
            get
            {
                return (DateTime?)this["ImportAllEvents"];
            }
            set
            {
                this["ImportAllEvents"] = value;
            }
        }

        public SyncConfigElement(ConfigElement parent)
            : base(parent)
        {
        }
    }
}
