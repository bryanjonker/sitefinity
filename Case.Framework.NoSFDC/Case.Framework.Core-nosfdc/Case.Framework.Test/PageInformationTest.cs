﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UIUC.Custom.Test
{
    using SitefinityWebApp.SitefinityPlugin;

    [TestClass]
    public class PageInformationTest
    {
        [TestMethod]
        public void CheckPage()
        {
            var url = "/research/breakthrough-topics";
            var lastModified = PageInformation.GetLastModifiedUser(url);
            Console.WriteLine($"Last Modified for {url} is {lastModified}");
        }
    }
}
