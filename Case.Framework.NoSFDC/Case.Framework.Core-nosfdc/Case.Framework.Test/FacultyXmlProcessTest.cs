﻿namespace UIUC.Custom.Test
{
    using System;
    using System.IO;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SitefinityWebApp.GoogleSearchAppliance;
    using UIUC.Custom.DAL.DataAccess.Faculty;
    using UIUC.Custom.DAL.DataAccess.GoogleSearch;

    [TestClass]
    public class FacultyXmlProcessTest
    {
        [TestMethod]
        public void GetSingleFaculty()
        {
            var xmlProcess = new XmlProcess("http://test.education.illinois.edu", "education_faculty_collection");
            var xmlString = xmlProcess.GenerateXmlFile("burbules");
            Console.WriteLine(xmlString);
            Assert.IsTrue(xmlString.Contains("http://test.education.illinois.edu/faculty/burbules"));
            Assert.IsTrue(xmlString.Contains("political theory"));
            Assert.IsTrue(!xmlString.Contains("http://test.education.illinois.edu/faculty/billcope"));
        }

        [TestMethod]
        public void GetAllFaculty()
        {
            var xmlProcess = new XmlProcess("http://test.education.illinois.edu", "education_faculty_collection");
            var xmlString = xmlProcess.GenerateXmlFile(" ");
            Console.WriteLine(xmlString);
            Assert.IsTrue(xmlString.Contains("http://test.education.illinois.edu/faculty/burbules"));
            Assert.IsTrue(xmlString.Contains("political theory"));
            Assert.IsTrue(xmlString.Contains("http://test.education.illinois.edu/faculty/billcope"));
        }
        
        [TestMethod]
        public void SendFaculty()
        {
            var xmlProcess = new XmlProcess("http://test.education.illinois.edu", "education_faculty_collection");
            var xmlSubmitter = new XmlSubmitter("http://search-dev.admin.uillinois.edu:19900/xmlfeed", "education_faculty_collection");

            xmlSubmitter.PostForm(xmlProcess.GenerateXmlFile(" "), true);
        }

        [TestMethod]
        public void GetFaculty()
        {
            string xml = "<GSP VER=\"3.2\"><TM>0.009795</TM><Q>bill cope</Q><PARAM name=\"q\" value=\"bill cope\" original_value=\"bill+cope\"/><PARAM name=\"sitesearch\" value=\"test.education.illinois.edu\" original_value=\"test.education.illinois.edu\"/><PARAM name=\"requiredfields\" value=\"department:Education Policy, Organization and Leadership\" original_value=\"department:Education+Policy,+Organization+and+Leadership\"/><PARAM name=\"entqr\" value=\"3\" original_value=\"3\"/><PARAM name=\"getfields\" value=\"department.title.image.fullname\" original_value=\"department.title.image.fullname\"/><PARAM name=\"getfields\" value=\"department.title.fullname.image\" original_value=\"department.title.fullname.image\"/><PARAM name=\"rc\" value=\"1\" original_value=\"1\"/><PARAM name=\"ie\" value=\"UTF-8\" original_value=\"UTF-8\"/><PARAM name=\"ulang\" value=\"en\" original_value=\"en\"/><PARAM name=\"ip\" value=\"130.126.166.23\" original_value=\"130.126.166.23\"/><PARAM name=\"access\" value=\"p\" original_value=\"p\"/><PARAM name=\"sort\" value=\"date:D:L:d1\" original_value=\"date:D:L:d1\"/><RES SN=\"1\" EN=\"1\"><M>1</M><XT/><WXT/><R N=\"1\"><U>http://test.education.illinois.edu/faculty/billcope</U><UE>http://test.education.illinois.edu/faculty/billcope</UE><T><b>Bill Cope&#39;s</b> R&amp;D explores the pedagogical affordances of <b>...</b></T><RK>5</RK><CRAWLDATE>3 Jun 2015</CRAWLDATE><ENT_SOURCE>T4-KFFF8QYMJC66K</ENT_SOURCE><FS NAME=\"date\" VALUE=\"\"/><MT N=\"department\" V=\"Education Policy, Organization and Leadership\"/><MT N=\"title\" V=\"Professor\"/><MT N=\"image\" V=\"/Theme/images/portraits/billcope.jpg\"/><MT N=\"fullname\" V=\"Bill Cope\"/><S>&lt;b&gt;Bill Cope&amp;#39;s&lt;/b&gt; R&amp;amp;D explores the pedagogical affordances of technology&lt;br&gt; mediated learning environments. From 2010-2013 &lt;b&gt;...&lt;/b&gt;  </S><LANG>en</LANG><HAS><L/><C SZ=\"2k\" CID=\"0GmmNaDMltIJ\" ENC=\"ISO-8859-1\"/></HAS></R></RES></GSP>";

            var results = new SearchResults(xml);
            Assert.AreEqual(string.Empty, results.SpellingSuggestion);
            Assert.AreEqual(1, results.TotalResults);
            Assert.AreEqual("Bill Cope", results.Faculty.First().Name);
            Assert.AreEqual("Education Policy, Organization and Leadership", results.Faculty.First().Department);
            Assert.AreEqual("Professor", results.Faculty.First().Title);
            Assert.AreEqual("/Theme/images/portraits/billcope.jpg", results.Faculty.First().Image);
            Assert.AreEqual("http://test.education.illinois.edu/faculty/billcope", results.Faculty.First().Url);
        }

        [TestMethod]
        public void GetFacultyWithSpelling()
        {
            string xml = "<GSP VER=\"3.2\"><TM>0.009795</TM><Q>bill cope</Q><PARAM name=\"q\" value=\"bill cope\" original_value=\"bill+cope\"/><PARAM name=\"sitesearch\" value=\"test.education.illinois.edu\" original_value=\"test.education.illinois.edu\"/><PARAM name=\"requiredfields\" value=\"department:Education Policy, Organization and Leadership\" original_value=\"department:Education+Policy,+Organization+and+Leadership\"/><PARAM name=\"entqr\" value=\"3\" original_value=\"3\"/><PARAM name=\"getfields\" value=\"department.title.image.fullname\" original_value=\"department.title.image.fullname\"/><PARAM name=\"getfields\" value=\"department.title.fullname.image\" original_value=\"department.title.fullname.image\"/><PARAM name=\"rc\" value=\"1\" original_value=\"1\"/><PARAM name=\"ie\" value=\"UTF-8\" original_value=\"UTF-8\"/><PARAM name=\"ulang\" value=\"en\" original_value=\"en\"/><PARAM name=\"ip\" value=\"130.126.166.23\" original_value=\"130.126.166.23\"/><PARAM name=\"access\" value=\"p\" original_value=\"p\"/><PARAM name=\"sort\" value=\"date:D:L:d1\" original_value=\"date:D:L:d1\"/><Spelling><Suggestion q=\"bill coppe technologies\">bill &lt;b&gt;&lt;i&gt;coppe&lt;/i&gt;&lt;/b&gt; technologies</Suggestion></Spelling><RES SN=\"1\" EN=\"1\"><M>1</M><XT/><WXT/><R N=\"1\"><U>http://test.education.illinois.edu/faculty/billcope</U><UE>http://test.education.illinois.edu/faculty/billcope</UE><T><b>Bill Cope&#39;s</b> R&amp;D explores the pedagogical affordances of <b>...</b></T><RK>5</RK><CRAWLDATE>3 Jun 2015</CRAWLDATE><ENT_SOURCE>T4-KFFF8QYMJC66K</ENT_SOURCE><FS NAME=\"date\" VALUE=\"\"/><MT N=\"department\" V=\"Education Policy, Organization and Leadership\"/><MT N=\"title\" V=\"Professor\"/><MT N=\"image\" V=\"/Theme/images/portraits/billcope.jpg\"/><MT N=\"fullname\" V=\"Bill Cope\"/><S>&lt;b&gt;Bill Cope&amp;#39;s&lt;/b&gt; R&amp;amp;D explores the pedagogical affordances of technology&lt;br&gt; mediated learning environments. From 2010-2013 &lt;b&gt;...&lt;/b&gt;  </S><LANG>en</LANG><HAS><L/><C SZ=\"2k\" CID=\"0GmmNaDMltIJ\" ENC=\"ISO-8859-1\"/></HAS></R></RES></GSP>";

            var results = new SearchResults(xml);
            Assert.AreEqual("bill <b><i>coppe</i></b> technologies", results.SpellingSuggestion);
            Assert.AreEqual(1, results.TotalResults);
            Assert.AreEqual("Bill Cope", results.Faculty.First().Name);
        }

        [TestMethod]
        public void GetSearchResultsFromGoogleAndCheckEncoding()
        {
            var searchResults = new GsaAccess("https://search-dev.admin.uillinois.edu").SearchData(0, 16, "equity issues", string.Empty, string.Empty);
            foreach (var result in searchResults.Faculty)
            {
                Console.WriteLine(result.Name);
            }
        }
    }
}
