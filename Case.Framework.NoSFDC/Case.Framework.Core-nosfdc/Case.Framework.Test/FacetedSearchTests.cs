﻿namespace UIUC.Custom.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using SitefinityWebApp.GoogleSearchAppliance.FacetedSearch;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch;
    using UIUC.Custom.DAL.DataAccess.GoogleSearch;

    [TestClass]
    public class FacetedSearchTests
    {
        [TestMethod]
        public void FacetedSearchPrograms()
        {
            var item = new FacetedSearchContext();
            var courses = item.Courses.ToList();
            var programs = item.Programs.ToList();
        }

        [TestMethod]
        public void ImportFacetedSearch()
        {
            var import = new ImportProcess();
            import.ImportCourseData();
        }

        [TestMethod]
        public void SendFacetedSearch()
        {
            var xmlProcess = new XmlProcess("http://test.education.illinois.edu", "education_course_collection");
            var xmlSubmitter = new XmlSubmitter("http://search-dev.admin.uillinois.edu:19900/xmlfeed", "education_course_collection");
            xmlSubmitter.PostForm(xmlProcess.GenerateXmlFile(), true);
        }

        [TestMethod]
        public void SearchOnFacetedSearch()
        {
            var export = new ExportProcess();
            var courses = export.GetCourses(new List<string>(), new List<string>(), new List<string>(), new List<string>(), 0, 100);
            Console.WriteLine(courses.Count());
        }

        [TestMethod]
        public void SearchOnFacetedSearchSpring()
        {
            var export = new ExportProcess();
            var courses = export.GetCourses(new List<string>(), new List<string> { "Spring" }, new List<string>(), new List<string>(), 0, 100);
            Console.WriteLine(courses.Count());
            Assert.IsTrue(courses.Any());
        }

        [TestMethod]
        public void SearchOnFacetedSearchSpringFall()
        {
            var export = new ExportProcess();
            var courses = export.GetCourses(new List<string>(), new List<string> { "Spring", "Fall" }, new List<string>(), new List<string>(), 0, 100);
            Console.WriteLine(courses.Count());
            foreach (var course in courses)
            {
                Console.WriteLine(course.TermsOffered);
            }
            Assert.IsTrue(courses.Any());
        }

        [TestMethod]
        public void SearchOnFacetedSearchDepartment()
        {
            var export = new ExportProcess();
            var courses = export.GetCourses(new List<string>(), new List<string> { "Spring", "Fall" }, new List<string> { "EPOL", "CI" }, new List<string>(), 0, 100);
            Console.WriteLine(courses.Count());
            Assert.IsTrue(courses.Any());
        }

        [TestMethod]
        public void SearchCount()
        {
            var export = new ExportProcess();
            var courses = export.GetCourseCount(new List<string>(), new List<string> { "Spring", "Fall" }, new List<string> { "EPOL", "CI" }, new List<string>());
            Console.WriteLine(courses);
            Assert.IsTrue(courses > 0);
        }

        [TestMethod]
        public void FacetedSearchCountByDepartment()
        {
            var export = new ExportProcess();
            var courses = export.GetFacetCountByDepartment(new List<string>(), new List<string>(), new List<string>(), new List<string>());
            Assert.IsTrue(courses.Any());
        }

        [TestMethod]
        public void FacetedSearchCountByFormatSingle()
        {
            var export = new ExportProcess();
            var courses = export.GetFacetCountByFormat(new List<string>(), new List<string>(), new List<string> { "CI" }, new List<string>());
            Assert.IsTrue(courses.Any());

        }

        [TestMethod]
        public void FacetedSearchCountByFormat()
        {
            var export = new ExportProcess();
            var courses = export.GetFacetCountByFormat(new List<string>(), new List<string>(), new List<string>(), new List<string>());
            Assert.IsTrue(courses.Any());
        }

        [TestMethod]
        public void SearchOnFacetedSearchSpecific()
        {
            var export = new ExportProcess();
            var courses = export.GetSpecificCourse("EDPR", "420");
            Console.WriteLine(courses.Sections.Count());
        }


        [TestMethod]
        public void SearchOnFacetedSearchSpecificNoneFound()
        {
            var export = new ExportProcess();
            var courses = export.GetSpecificCourse("XXX", "420");
            Assert.IsNull(courses);
        }

        [TestMethod]
        public void GetFacultyFromSearchAppliance()
        {
            var searchResults = new CourseGsaAccess("https://search-dev.admin.uillinois.edu").SearchData(0, 16, "equity issues", string.Empty, string.Empty, string.Empty);
            foreach (var result in searchResults.Courses)
            {
                Console.WriteLine(result.Name);
            }
        }
    }
}