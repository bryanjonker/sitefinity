﻿namespace UIUC.Custom.Test
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using SitefinityWebApp.Mvc.Models.Person;

    [TestClass]
    public class FacultyRetrieve
    {
        [TestMethod]
        public void TestFacultyRetrieve()
        {
            var manager = new PersonManager();
            var items = manager.GetPeopleBySingleLetter('A');
            foreach (var item in items)
            {
                Console.WriteLine(item.LastName + " " + item.Username + " " + item.FullName + " " + item.Title + " " + item.FormattedPhoneLink + " " + item.FormattedPhone + " " + item.Email);
            }
        }
    }
}
