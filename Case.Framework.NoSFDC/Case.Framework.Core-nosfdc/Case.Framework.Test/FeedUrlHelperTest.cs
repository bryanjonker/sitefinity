﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UIUC.Custom.Test
{
    using UIUC.Custom.XmlService;

    [TestClass]
    public class FeedUrlHelperTest
    {
        [TestMethod]
        public void FeedUrlHelperGetInformationTest()
        {
            var items = FeedUrlHelper.GetArticles("http://status.illinois.edu/SystemStatus/jsp/rss_feed.jsp?id=200", "item", 3);
            foreach (var item in items)
            {
                Console.WriteLine(item.Title);
            }
        }
    }
}
