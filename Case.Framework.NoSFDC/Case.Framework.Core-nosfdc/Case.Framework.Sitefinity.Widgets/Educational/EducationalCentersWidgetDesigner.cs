﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.Educational
{

    public class EducationalCentersWidgetDesigner : ControlDesignerBase
    {

        protected override string LayoutTemplateName
        {
            get
            {
                return TemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(EducationalCentersWidgetDesigner);
            }
        }
        #region Control references
        /// <summary>
        /// The LinkButton for select AlbumId
        /// </summary>
        protected internal virtual LinkButton SelectButtonAlbumId
        {
            get
            {
                return Container.GetControl<LinkButton>("selectButtonAlbumId", false);
            }
        }

        protected internal virtual LinkButton SelectButtonPageId
        {
            get
            {
                return Container.GetControl<LinkButton>("selectButtonPageId",false);
                
            }
        }

        /// <summary>
        /// The Flat Selector for AlbumId
        /// </summary>
        protected internal virtual FolderSelector AlbumIdItemSelector
        {
            get
            {
                return Container.GetControl<FolderSelector>("AlbumIdItemSelector", false);
            }
        }

        protected internal virtual GenericPageSelector PageIdItemSelector
        {
            get
            {
                return Container.GetControl<GenericPageSelector>("PageIdItemSelector", false);
                
            }
        }

        /// <summary>
        /// The LinkButton for "Done"
        /// </summary>
        protected virtual LinkButton DoneButtonAlbumId
        {
            get
            {
                return Container.GetControl<LinkButton>("lnkDoneAlbumId", true);
            }
        }

        protected virtual LinkButton DoneButtonPageId
        {
            get
            {
                return Container.GetControl<LinkButton>("lnkDonePageId", true);
                
            }
        }

        /// <summary>
        /// The LinkButton for "Cancel"
        /// </summary>
        protected virtual LinkButton CancelButtonAlbumId
        {
            get
            {
                return Container.GetControl<LinkButton>("lnkCancelAlbumId", true);
            }
        }

        protected virtual LinkButton CancelButtonPageId
        {
            get
            {
                return Container.GetControl<LinkButton>("lnkCancelPageId", true);
                
            }
        }

        /// <summary>
        /// The button area control
        /// </summary>
        protected virtual Control ButtonAreaAlbumId
        {
            get
            {
                return Container.GetControl<Control>("buttonAreaPanelAlbumId", false);
            }
        }

        protected virtual Control ButtonAreaPageId
        {
            get
            {
                return Container.GetControl<Control>("buttonAreaPanelPageId", false);
                
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(GenericContainer container)
        {
            // Place your initialization logic here

          
        }
        #endregion
        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("selectButtonAlbumId", SelectButtonAlbumId.ClientID);
            descriptor.AddComponentProperty("AlbumIdItemSelector", AlbumIdItemSelector.ClientID);
            descriptor.AddElementProperty("lnkDoneAlbumId", DoneButtonAlbumId.ClientID);
            descriptor.AddElementProperty("lnkCancelAlbumId", CancelButtonAlbumId.ClientID);

            descriptor.AddElementProperty("selectButtonPageId", SelectButtonPageId.ClientID);
            descriptor.AddComponentProperty("PageIdItemSelector",PageIdItemSelector.ClientID);
            descriptor.AddElementProperty("lnkDonePageId",DoneButtonPageId.ClientID);
            descriptor.AddElementProperty("lnkCancelPageId",CancelButtonPageId.ClientID);
            


            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences())
            {
                new ScriptReference(SReference, typeof (EducationalCentersWidgetDesigner).Assembly.FullName)
            };
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        private const string TemplateName = "Case.Framework.Sitefinity.Widgets.Educational.Resources.EducationalCentersWidgetDesigner.ascx";
        private const string SReference = "Case.Framework.Sitefinity.Widgets.Educational.Resources.EducationalCentersWidgetDesigner.js";
    }

}
