﻿Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Educational");

Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner = function (element) {

    this._selectButtonAlbumId = null;
    this._selectButtonAlbumIdClickDelegate = null;
    this._lnkDoneAlbumId = null;
    this._lnkCancelAlbumId = null;
    this._selectAlbumIdDialog = null;
    this._AlbumIdItemSelector = null;
    this._AlbumIdSelectedKeys = null;
    this._AlbumIdSelectedItems = null;
    this._AlbumIdBinderBound = false;
    this._doneSelectingAlbumIdDelegate = null;
    this._cancelAlbumIdDelegate = null;
    this._AlbumIdItemSelectorCloseDelegate = null;

    this._selectButtonPageId = null;
    this._selectButtonPageIdClickDelegate = null;
    this._lnkDonePageId = null;
    this._lnkCancelPageId = null;
    this._selectPageIdDialog = null;
    this._PageIdItemSelector = null;
    this._PageIdSelectedKeys = null;
    this._PageIdSelectedItems = null;
    this._PageIdBinderBound = false;
    this._doneSelectingPageIdDelegate = null;
    this._cancelPageIdDelegate = null;
    this._PageIdItemSelectorCloseDelegate = null;
    

    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner.callBaseMethod(this, 'initialize');
        /* Initialize AlbumId */
        if (this._selectButtonAlbumId) {
            this._selectButtonAlbumIdClickDelegate = Function.createDelegate(this, this._selectButtonAlbumIdClicked);
            $addHandler(this._selectButtonAlbumId, "click", this._selectButtonAlbumIdClickDelegate);
        }
        if (this._selectButtonPageId) {
            this._selectButtonPageIdClickDelegate = Function.createDelegate(this, this._selectButtonPageIdClicked);
            $addHandler(this._selectButtonPageId, "click", this._selectButtonPageIdClickDelegate);
        }

        if (this._lnkDoneAlbumId) {
            this._AlbumIdDoneSelectingDelegate = Function.createDelegate(this, this._AlbumIdDoneSelecting);
            $addHandler(this._lnkDoneAlbumId, "click", this._AlbumIdDoneSelectingDelegate);
        }

        if (this._lnkDonePageId) {
            this._PageIdDoneSelectingDelegate = Function.createDelegate(this, this._PageIdDoneSelecting);
            $addHandler(this._lnkDonePageId, "click", this._PageIdDoneSelectingDelegate);
        }

        if (this._lnkCancelAlbumId) {
            this._AlbumIdCancelDelegate = Function.createDelegate(this, this._AlbumIdItemSelectorCloseHandler);
            $addHandler(this._lnkCancelAlbumId, "click", this._AlbumIdCancelDelegate);
        }

        if (this._lnkCancelPageId) {
            this._PageIdCancelDelegate = Function.createDelegate(this, this._PageIdItemSelectorCloseHandler);
            $addHandler(this._lnkCancelPageId, "click", this._PageIdCancelDelegate);
        }


        this._selectAlbumIdDialog = jQuery("#AlbumIdSelector").dialog({
            autoOpen: false,
            modal: false,
            width: 540,
            height: "auto",
            closeOnEscape: true,
            resizable: false,
            draggable: false,
            zIndex: 5000,
        });

        this._selectPageIdDialog = jQuery("#PageIdSelector").dialog({
            autoOpen: false,
            modal: false,
            width: 540,
            height: "auto",
            closeOnEscape: true,
            resizable: false,
            draggable: false,
            zIndex: 5000,
        });
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        if (controlData.AlbumId) {
            this.get_selectedAlbumId().innerHTML = controlData.AlbumTitle;
            this.get_selectButtonAlbumId().innerHTML = '<span class=\"sfLinkBtnIn\">Change</span>';
        }
        

        if (controlData.PageId) {
            this.get_selectedPageId().innerHTML = controlData.PageTitle;
            this.get_selectButtonPageId().innerHTML = '<span class=\"sfLinkBtnIn\">Change</span>';
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        var selectedAlbumId = this.get_AlbumIdSelectedItems()[0];
        if (selectedAlbumId != null) {
            controlData.AlbumId = selectedAlbumId.Id;
            controlData.AlbumTitle = selectedAlbumId.Title;
        }

        var selectedItem = this.get_PageIdSelectedItems();
        if (selectedItem) {
            controlData.PageId = selectedItem.Id;
            controlData.PageTitle = selectedItem.Title.Value;
        }
    },

    /* --------------------------------- event handlers ---------------------------------- */
    _selectButtonAlbumIdClicked: function (sender, args) {
        var itemSelector = this.get_AlbumIdItemSelector();
        if (itemSelector) {
            // dynamic items don't support fallback, the binder search should work as in monolingual 
            //itemSelector._selectorSearchBox.get_binderSearch()._multilingual = false;
            itemSelector.dataBind();
        }

        this._selectAlbumIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._selectAlbumIdDialog.dialog().parent().css("min-width", "525px");
        dialogBase.resizeToContent();
        return false;
    },

    _selectButtonPageIdClicked: function (sender, args) {
        var itemSelector = this.get_PageIdItemSelector();
        if (itemSelector) {
             itemSelector.dataBind();
        }

        this._selectPageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();

        this._selectPageIdDialog.dialog().parent().css("min-width", "525px");
        dialogBase.resizeToContent();

        return false;
    },

    _AlbumIdItemSelectorCloseHandler: function (sender, args) {
        this._selectAlbumIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _PageIdItemSelectorCloseHandler: function (sender, args) {
        this._selectPageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _AlbumIdDoneSelecting: function (sender, args) {
        var selectedItem = this.get_AlbumIdSelectedItems()[0];
        if (selectedItem != null) {
            this.get_selectedAlbumId().innerHTML = selectedItem.Title;
            this.get_selectButtonAlbumId().innerHTML = '<span class=\"sfLinkBtnIn\">Change</span>';
        }
        this._selectAlbumIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _PageIdDoneSelecting: function (sender, args) {
        var selectedItem = this.get_PageIdSelectedItems();

        if (selectedItem != null) {
            this.get_selectedPageId().innerHTML = selectedItem.Title.Value;
            this.get_selectButtonPageId().innerHTML = '<span class=\"sfLinkBtnIn\">Change</span>';
        }

        this._selectPageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },
    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */
    /* AlbumId properties */
    get_selectButtonAlbumId: function () {
        return this._selectButtonAlbumId;
    },

    get_selectButtonPageId: function() {
        return this._selectButtonPageId;
    },

    set_selectButtonAlbumId: function (value) {
        this._selectButtonAlbumId = value;
    },

    set_selectButtonPageId: function(value) {
        this._selectButtonPageId = value;
    },

    get_AlbumIdItemSelector: function () {
        return this._AlbumIdItemSelector;
    },

    get_PageIdItemSelector: function () {
        return this._PageIdItemSelector;
    },

    set_AlbumIdItemSelector: function (value) {
        this._AlbumIdItemSelector = value;
    },

    set_PageIdItemSelector: function (value) {
        this._PageIdItemSelector = value;
    },

    get_AlbumIdBinder: function () {
        return this._AlbumIdItemSelector.get_binder();
    },

    get_PageIdBinder: function () {
        return this._PageIdItemSelector.get_binder();
    },

    get_AlbumIdSelectedKeys: function () {
        return this._AlbumIdItemSelector.get_selectedKeys();
    },
    get_PageIdSelectedKeys: function () {
        return this._PageIdItemSelector.get_selectedKeys();
    },

    set_AlbumIdSelectedKeys: function (keys) {
        this._selectedKeys = keys;
    },

    set_PageIdSelectedKeys: function (keys) {
        this._selectedKeys = keys;
    },

    get_AlbumIdSelectedItems: function () {
        return this._AlbumIdItemSelector.getSelectedItems();
    },
    get_PageIdSelectedItems: function () {
        return this._PageIdItemSelector.get_selectedItem();
    },
        

    set_AlbumIdSelectedItems: function (items) {
        this._AlbumIdSelectedItems = items;
        if (this._AlbumIdBinderBound) {
            this._AlbumIdItemSelector.bindSelector();
        }
    },

    set_PageIdSelectedItems: function (items) {
        this._PageIdSelectedItems = items;
        if (this._PageIdBinderBound) {
            this._PageIdItemSelector.bindSelector();
        }
    },


    get_lnkDoneAlbumId: function () {
        return this._lnkDoneAlbumId;
    },
    get_lnkDonePageId: function () {
        return this._lnkDonePageId;
    },

    set_lnkDoneAlbumId: function (value) {
        this._lnkDoneAlbumId = value;
    },
    set_lnkDonePageId: function (value) {
        this._lnkDonePageId = value;
    },

    get_lnkCancelAlbumId: function () {
        return this._lnkCancelAlbumId;
    },
    get_lnkCancelPageId: function () {
        return this._lnkCancelPageId;
    },

    set_lnkCancelAlbumId: function (value) {
        this._lnkCancelAlbumId = value;
    },
    set_lnkCancelPageId: function (value) {
        this._lnkCancelPageId = value;
    },

    get_selectedAlbumId: function () {
        if (this._selectedAlbumIdLabel == null) {
            this._selectedAlbumIdLabel = jQuery(this.get_element()).find('#selectedAlbumIdLabel').get(0);
        }
        return this._selectedAlbumIdLabel;
    },
    get_selectedPageId: function () {
    if (this._selectedPageIdLabel == null) {
        this._selectedPageIdLabel = jQuery(this.get_element()).find('#selectedPageIdLabel').get(0);
    }
    return this._selectedPageIdLabel;
}

}

Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Educational.EducationalCentersWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
