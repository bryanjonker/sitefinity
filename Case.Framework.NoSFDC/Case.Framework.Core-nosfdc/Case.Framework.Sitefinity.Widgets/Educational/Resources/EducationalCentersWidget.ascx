﻿<%@ Control Language="C#" Inherits="Case.Images.Gallery.GalleryWidget.cs" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>
<sitefinity:FormManager id="formManager" runat="server"/>
<%--<script src="/Theme/js/flexslider/jquery.flexslider-min.js"></script>
<link href="/Theme/css/flexslider.css" rel="stylesheet" />--%>


<%--<style>
 .carousel a {
text-decoration: none;
text-align: left;
}
</style>--%>
<div class="row">
   <div class="column carousel-wrapper">
      <div class="carousel">  
         
<asp:Repeater runat="server" ID="rptSlider">
    <ItemTemplate>
        <div class="column one-third">
            <asp:HyperLink id="Link" runat="server">
                <asp:Literal ID="imgCtrl" runat="server"  ClientIDMode="Static"></asp:Literal>                
           </asp:HyperLink>
             <asp:HyperLink id="HyperLink1" runat="server">
                <asp:Literal ID="ltCtrl" runat="server"></asp:Literal>
	        </asp:HyperLink>
        </div>
    </ItemTemplate>
</asp:Repeater>
       <a class="carousel_l" >&lsaquo;</a><a class="carousel_r">&rsaquo;</a>
    </div>
        <asp:Literal ID="ltCtrlfulllist" runat="server"></asp:Literal>
   </div>
</div>

