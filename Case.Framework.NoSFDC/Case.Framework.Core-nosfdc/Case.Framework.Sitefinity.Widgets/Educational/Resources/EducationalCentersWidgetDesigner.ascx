﻿<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
       <div id="PageIdSelector">
           <sitefinity:GenericPageSelector
                id="PageIdItemSelector"
                runat="server"
                WebServiceUrl="~/Sitefinity/Services/Pages/PagesService.svc/"
                ShowOnlySelectedAsGridOnLoad="true"
                   MarkItemsWithoutTranslation="true"
                AllowMultipleSelection="false" />
        <asp:Panel runat="server" ID="buttonPanelPageId" class="sfButtonArea sfSelectorBtns">
              <asp:LinkButton ID="lnkDonePageId" runat="server" OnClientClick="return false;" CssClass="sfLinkBtn sfSave">
                <strong class="sfLinkBtnIn">
                    <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:Labels, Done %>" />
                </strong>
            </asp:LinkButton>
            <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:Labels, or%>" />
            <asp:LinkButton ID="lnkCancelPageId" runat="server" CssClass="sfCancel" OnClientClick="return false;">
                <asp:Literal ID="Literal8" runat="server" Text="<%$Resources:Labels, Cancel %>" />
            </asp:LinkButton>
        </asp:Panel>
       </div>
        <div><p>Select the page for full list of educational centers.</p><br /></div>
    <label class="sfTxtLbl" for="selectedPageIdLabel" style="margin-bottom:5px;">Page</label>
    <span class="sfSelectedItem" id="selectedPageIdLabel">
        <asp:Literal ID="Literal9" runat="server" Text="" />
    </span>
    <asp:LinkButton ID="selectButtonPageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
            <asp:Literal ID="Literal10" runat="server" Text="Select..." />
        </span>
    </asp:LinkButton>
    </li>
    <li class="sfFormCtrl">
    <div id="AlbumIdSelector">  
        <sitefinity:FolderSelector ID="AlbumIdItemSelector" runat="server"
            AllowMultipleSelection="false"
            BindOnLoad="false"
            AllowSearch="true"
            ShowButtonsArea="false"
            WebServiceUrl="~/Sitefinity/Services/Lists/ListService.svc/" />

        <asp:Panel runat="server" ID="buttonAreaPanelAlbumId" class="sfButtonArea sfSelectorBtns">
            <asp:LinkButton ID="lnkDoneAlbumId" runat="server" OnClientClick="return false;" CssClass="sfLinkBtn sfSave">
                <strong class="sfLinkBtnIn">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Labels, Done %>" />
                </strong>
            </asp:LinkButton>
            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Labels, or%>" />
            <asp:LinkButton ID="lnkCancelAlbumId" runat="server" CssClass="sfCancel" OnClientClick="return false;">
                <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:Labels, Cancel %>" />
            </asp:LinkButton>
        </asp:Panel>
    </div>
    <div><p>Select the list from which you wish the educational centers to display.</p><br /></div>
    <label class="sfTxtLbl" for="selectedAlbumIdLabel" style="margin-bottom:5px;">List Content</label>
    <span class="sfSelectedItem" id="selectedAlbumIdLabel">
        <asp:Literal ID="Literal4" runat="server" Text="" />
    </span>
    <asp:LinkButton ID="selectButtonAlbumId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
            <asp:Literal ID="Literal5" runat="server" Text="Select..." />
        </span>
    </asp:LinkButton>
    </li>
    
</ol>
</div>
