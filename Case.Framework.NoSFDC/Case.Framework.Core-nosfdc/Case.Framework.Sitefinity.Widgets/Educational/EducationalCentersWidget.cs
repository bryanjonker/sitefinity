﻿using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Case.Framework.Sitefinity.Extensions;

namespace Case.Framework.Sitefinity.Widgets.Educational
{
    [ControlDesigner(typeof(EducationalCentersWidgetDesigner))]
    public class EducationalCentersWidget : SimpleView
    {
        protected override string LayoutTemplateName
        {
            get
            {
                return TemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(EducationalCentersWidget);
            }
        }

        protected virtual Literal LtCtrlfulllist
        {
            get
            {
                return Container.GetControl<Literal>("ltCtrlfulllist",true);
                
            }
        }

        protected virtual Repeater RptSlider
        {
            get
            {
                return Container.GetControl<Repeater>("rptSlider", true);
            }

        }
        
        public Guid AlbumId { get; set; }

        public Guid PageId { get; set; }

        public string FullListUrl { get; set; }

        public string AlbumTitle { get; set; }

        public string PageTitle { get; set; }

        private int counter = 1;

        protected override void InitializeControls(GenericContainer container)
        {
            if (AlbumId.Equals(Guid.Empty)) return;
            var listsManager = CaseManagers.Lists.GetManager();

            var list = listsManager.GetLists().FirstOrDefault(x => x.Id == AlbumId);
            var listItems = new List<Telerik.Sitefinity.Lists.Model.ListItem>();

            if (list != null)
                listItems = Page.IsDesignMode()
                    ? list.ListItems.Where(x => x.Status == ContentLifecycleStatus.Live).Take(1).ToList()
                    : list.ListItems.Where(x => x.Status == ContentLifecycleStatus.Live).ToList();

            RptSlider.ItemDataBound += rptSlider_ItemDataBound;
            RptSlider.DataSource = listItems;
            RptSlider.DataBind();


            if (PageId == Guid.Empty) return;
            var page = CaseManagers.Pages.GetById(PageId);
            if (page != null)
                LtCtrlfulllist.Text = "<div class=\"carousel-full\"><a class=\"full-list button orange\" href=\"" + page.Url +
                                      "\">Full List&nbsp;&rsaquo;</a></div>";
        }

        protected void rptSlider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            var imgControl = (Literal)e.Item.FindControl("imgCtrl");
            var link = (HyperLink)e.Item.FindControl("Link");
            var HyperLink1 = (HyperLink)e.Item.FindControl("HyperLink1");
            var ltCtrl = (Literal)e.Item.FindControl("ltCtrl");

            if (imgControl == null || link == null || ltCtrl == null || HyperLink1 == null) return;
            if (e.Item.DataItem == null) return;
            var img = ((Telerik.Sitefinity.Lists.Model.ListItem)e.Item.DataItem).GetImage("Thumbnail");

            if (img != null)
            {
                var  pg = (PageNode)e.Item.DataItem.GetRelatedItems("Link").FirstOrDefault();

                if (pg != null)
                {
                    link.NavigateUrl = pg.GetDefaultUrl();
                    HyperLink1.NavigateUrl = pg.GetDefaultUrl();
                }

                imgControl.Text = "<img alt=\"" + img.AlternativeText + "\" src=\"" + img.Url + "\"/>";
            }

            
            //<p id="carousel1_text" runat="server">Center for Culturally Responsive Evaluation and Assessment</p>
            ltCtrl.Text = "<p>" + ((Telerik.Sitefinity.Lists.Model.ListItem)(e.Item.DataItem)).Title + "</p>";
            counter++;
        }

        private const string TemplateName = "Case.Framework.Sitefinity.Widgets.Educational.Resources.EducationalCentersWidget.ascx";
    }

}
