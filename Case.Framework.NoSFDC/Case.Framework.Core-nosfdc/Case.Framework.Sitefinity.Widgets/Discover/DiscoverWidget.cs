﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;

namespace Case.Framework.Sitefinity.Widgets.Discover
{
    public class DiscoverWidget : SimpleView
    {
        protected override string LayoutTemplateName
        {
            get
            {
                return DiscoverWidget.layoutTemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(DiscoverWidget);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {

        }

        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.Discover.Resources.DiscoverWidget.ascx";


    }

}
