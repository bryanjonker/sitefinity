﻿Type.registerNamespace("Case.Framework.Sitefinity.Widgets.CTA");

Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner = function (element) {
    Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner.initializeBase(this, [element]);

    this._beforeSaveChanges = null;
    this._container = null;
    this._data = new Array();
    this._rdViewMode = null;
    this._itemSelectDelegate = null;

    /* Initialize PageId fields */
    this._pageSelectorPageId = null;
    this._selectorTagPageId = null;
    this._PageIdDialog = null;

    this._showPageSelectorPageIdDelegate = null;
    this._pageSelectedPageIdDelegate = null;
}

Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner.prototype = {
    initialize: function () {
        Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner.callBaseMethod(this, 'initialize');

        this._beforeSaveChanges = Function.createDelegate(this, this.beforeSaveChanges);
        this._propertyEditor.add_beforeSaveChanges(this._beforeSaveChanges);

        _container = this;
        jQuery("#aAddTab").click(function () {
            _container.addTab();
        });
        /* Initialize PageId */
        this._showPageSelectorPageIdDelegate = Function.createDelegate(this, this._showPageSelectorPageIdHandler);
        $addHandler(this.get_pageSelectButtonPageId(), "click", this._showPageSelectorPageIdDelegate);

        this._pageSelectedPageIdDelegate = Function.createDelegate(this, this._pageSelectedPageIdHandler);
        this.get_pageSelectorPageId().add_doneClientSelection(this._pageSelectedPageIdDelegate);

        if (this._selectorTagPageId) {
            this._PageIdDialog = jQuery(this._selectorTagPageId).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                height: 500,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }
    },
    dispose: function () {
        Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner.callBaseMethod(this, 'dispose');

        if (this._selectorView) {
            this._selectorView.add_onItemSelectCommand(this._itemSelectDelegate);
        }
        /* Dispose PageId */
        if (this._showPageSelectorPageIdDelegate) {
            $removeHandler(this.get_pageSelectButtonPageId(), "click", this._showPageSelectorPageIdDelegate);
            delete this._showPageSelectorPageIdDelegate;
        }

        if (this._pageSelectedPageIdDelegate) {
            this.get_pageSelectorPageId().remove_doneClientSelection(this._pageSelectedPageIdDelegate);
            delete this._pageSelectedPageIdDelegate;
        }
    },

    get_rdViewMode: function () {
        return this._rdViewMode
    },
    set_rdViewMode: function (value) {
        this._rdViewMode = value;
    },
    /* PageId properties */
    get_pageSelectButtonPageId: function () {
        if (this._pageSelectButtonPageId == null) {
            this._pageSelectButtonPageId = jQuery(this.get_element()).find("#pageSelectButtonPageId").get(0);
        }
        return this._pageSelectButtonPageId;
    },
    get_selectedPageIdLabel: function () {
        if (this._selectedPageIdLabel == null) {
            this._selectedPageIdLabel = jQuery(this.get_element()).find("#selectedPageIdLabel").get(0);
        }
        return this._selectedPageIdLabel;
    },
    get_pageSelectorPageId: function () {
        return this._pageSelectorPageId;
    },
    set_pageSelectorPageId: function (val) {
        this._pageSelectorPageId = val;
    },
    get_selectorTagPageId: function () {
        return this._selectorTagPageId;
    },
    set_selectorTagPageId: function (value) {
        this._selectorTagPageId = value;
    },


    beforeSaveChanges: function (sender, cancelEventArgs) {
        //var ser = Sys.Serialization.JavaScriptSerializer.serialize(_container._data);
        //var deser = Sys.Serialization.JavaScriptSerializer.deserialize(ser);
    },
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control();
        if (controlData.Data != null && controlData.Data != undefined && controlData.Data.trim().length > 0) {
            var list = Sys.Serialization.JavaScriptSerializer.deserialize(controlData.Data);
            _container.loadTabs(list);
            var rdViewMode = this.get_rdViewMode();
            if (controlData.ViewMode != null) {
                rdViewMode.set_value(controlData.ViewMode);
                rdViewMode.set_text(controlData.ViewMode);
            }
            $("#selPageType").val(controlData.PageType);
        }
    },
    applyChanges: function () {
        var view;
        var _rdViewModeSelected = this.get_rdViewMode().get_selectedItem();
        if (_rdViewModeSelected) view = _rdViewModeSelected.get_text();
        if (_container._data.length > 3 && view == 'Horizontal') {
            alert('You can only add 3 links for horizontal view. Selecting OK on this message, will close the widget dialog and changes will not be saved.');
            return;
        }
        if (_container.getSelectedTab() != null) {
            if (_container.getSelectedTab().Index > 0) {
                _container.pushData();
            }
        }
        var controlData = this._propertyEditor.get_control();
        controlData.Data = Sys.Serialization.JavaScriptSerializer.serialize(_container._data);
        controlData.PageType = $("#selPageType").val();
        if (view == null) {
            controlData.ViewMode = "Vertical";
        } else {
            controlData.ViewMode = view;
        }
    },
    loadTabs: function (list) {
        var parentContainer = $('#ulAdded');
        parentContainer.html('');
        _container._data = [];

        for (var x = 0; x < list.length; x++) {
            var tabIndex = list[x].Index;
            var replaced = list[x].Name.split('_').join(' ');
            var tabName = replaced;

            var li = $('<li id="' + tabIndex + '"></li>');
            var label = $('<label style="color: #888;">' + tabName + '</label>');
            var removeButton = $('<a href="javascript:void(0);">Remove</a>');

            label.click(_container.selectTab);
            removeButton.click(_container.removeTab);

            li.append(label);
            li.append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
            li.append(removeButton);


            parentContainer.append(li);
            _container._data.push(list[x]);

            _container.selectDefaultTab();
            dialogBase.resizeToContent();
        }
    },
    addTab: function () {
        var view;
        var _rdViewModeSelected = this.get_rdViewMode().get_selectedItem();
        if (_rdViewModeSelected) view = _rdViewModeSelected.get_text();

        if (_container._data.length == 3 && view == 'Horizontal') {
            alert('You can only add 3 links for horizontal view');
            return;
        }

        if ($('#txtTitle').val().length > 0 || $('#txtUrl').val().length > 0) {
            _container.pushData();
            $('label', $('#ulAdded')).css('fontWeight', 'normal');
            $('label', $('#ulAdded')).css('text-decoration', 'none');
        } else {
            $('label', $('#ulAdded')).css('fontWeight', 'normal');
            $('label', $('#ulAdded')).css('text-decoration', 'none');
        }

        var parentContainer = $('#ulAdded');
        var tabIndex = ($('li', parentContainer).length);
        var tabName = 'Link ' + (tabIndex + 1);

        var li = $('<li id="' + tabIndex + '"></li>');
        var label = $('<label style="color: #888;font-weight:bold;text-decoration:underline;">' + tabName + '</label>');
        var removeButton = $('<a href="javascript:void(0);">Remove</a>');

        label.click(_container.selectTab);
        removeButton.click(_container.removeTab);

        li.append(label);
        li.append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        li.append(removeButton);


        parentContainer.append(li);
        _container._data.push({ Name: tabName, Index: tabIndex, Title: '', URL: '' });
        _container.setEditorValue();
        dialogBase.resizeToContent();
    },
    getSelectedTab: function () {
        var tabIndex = 0;

        $('#ulAdded li label').each(function (index) {
            if ($(this).css('fontWeight') == 'bold') {
                tabIndex = parseInt($(this).parent().attr('id'));
            }
        });

        for (var x = 0; x < _container._data.length; x++) {
            if (_container._data[x].Index == tabIndex) {
                return _container._data[x];
            }
        }

        return null;
    },
    getSelectedTabIndex: function () {
        var tabIndex = -1;

        $('#ulAdded li label').each(function (index) {
            if ($(this).css('fontWeight') == 'bold') {
                tabIndex = index;
            }
        });

        return tabIndex;
    },
  
    setEditorValue: function () {

        if (_container.getSelectedTabIndex() != -1) {
            var data = _container._data[_container.getSelectedTabIndex()];

            $('#divStep2').css('display', 'block');
            if (data != null && data != undefined) {
                var replaced = data.Title.split('_').join(' ');

                $('#txtTitle').val(replaced);
                $('#txtUrl').val(data.URL);
                $('#txtUrlGuid').val(data.URLGuid);
            }

            dialogBase.resizeToContent();
        }
        else {
            $('#txtTitle').val('');
            $('#txtUrl').val('');
            $('#txtUrlGuid').val('00000000-0000-0000-0000-000000000000');
            $('#divStep2').css('display', 'none');
            dialogBase.resizeToContent();
        }
    },
    pushData: function () {
        if (_container.getSelectedTabIndex() > -1) {
            var data = _container.getSelectedTab();
            for (var x = 0; x < _container._data.length; x++) {
                if (_container._data[x].Index == data.Index) {
                    var replaced = $('#txtTitle').val().split(' ').join('_');
                    _container._data[x].Name = replaced; // data.Name;
                    _container._data[x].Title = $('#txtTitle').val();
                    _container._data[x].URL = $('#txtUrl').val();
                    _container._data[x].URLGuid = $('#txtUrlGuid').val();
                  }
            }
        }
    },
    selectDefaultTab: function () {
        if (_container.getSelectedTabIndex() == -1) {
            $('label', $('#ulAdded')).css('fontWeight', 'normal');
            $('label:first', $('#ulAdded')).css('fontWeight', 'bold');
            $('label:first', $('#ulAdded')).css('text-decoration', 'underline');
            _container.setEditorValue();
        }
    },

    /* PageId private methods */
    _showPageSelectorPageIdHandler: function (selectedItem) {
        this._PageIdDialog.dialog("open");
        this._PageIdDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
        var pagesSelector = this.get_pageSelectorPageId().get_pageSelector();
        $('.sfNavDesignerCtrl').hide();
        if ($('#txtUrlGuid').val() != "00000000-0000-0000-0000-000000000000" && $('#txtUrlGuid').val() != '') {
            pagesSelector.set_selectedItems([{ Id: $('#txtUrlGuid').val() }]);
            $('.rtsLI.rtsFirst a').parent().click();
        } else {
            pagesSelector.set_selectedItems(null);
            $('.rtsLI.rtsLast a').parent().click();
        }
    },

    _pageSelectedPageIdHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorPageId().get_pageSelector();
        $('.sfNavDesignerCtrl').show();
        this._PageIdDialog.dialog("close");
        dialogBase.resizeToContent();

        if (items != null) {
            var selectedPage = pagesSelector.get_selectedItem();
            if (this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value() != '' && this.get_pageSelectorPageId().get_tabstrip().get_selectedIndex() == 1) {
                this.get_pageSelectButtonPageId().innerHTML = '<span>Change...</span>';
                $('#txtUrl').val(this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value());
                $('#txtUrlGuid').val('00000000-0000-0000-0000-000000000000');
            }
            else if (selectedPage && this.get_pageSelectorPageId().get_tabstrip().get_selectedIndex() == 0) {
                this.get_pageSelectButtonPageId().innerHTML = '<span>Change...</span>';
                $('#txtUrlGuid').val(selectedPage.Id);
                $('#txtUrl').val(selectedPage.Title.Value);
            }
            else {
                this.get_pageSelectButtonPageId().innerHTML = '<span>Select...</span>';
                $('#txtUrl').val('');
                $('#txtUrlGuid').val('00000000-0000-0000-0000-000000000000');
            }
        }
    },


    //Events
    removeTab: function () {
        var itemToRemove = $(this).parent();
        var indexToRemove = parseInt(itemToRemove.attr('id'));
        //delete _container._data[itemToRemove.attr('id')];
        for (var x = 0; x < _container._data.length; x++) {
            if (_container._data[x].Index == indexToRemove) {
                _container._data.remove(x);
            }
        }

        itemToRemove.remove();
        _container.selectDefaultTab();
    },
    selectTab: function () {
        if ($(this).css('fontWeight') != 'bold') {
            if (_container.getSelectedTabIndex() > -1) {
                _container.pushData();
            }
            $('label', $('#ulAdded')).css('fontWeight', 'normal');
            $('label', $('#ulAdded')).css('text-decoration', 'none');
            $(this).css('fontWeight', 'bold');
            $(this).css('text-decoration', 'underline');

            _container.setEditorValue();
        }
    }
}

Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.CTA.CTAWidgetDesigner',
Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();

Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};