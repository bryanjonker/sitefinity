﻿<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="designers" Namespace="Telerik.Sitefinity.Web.UI.ControlDesign" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Modules.Libraries.Web.UI.Designers" TagPrefix="sitefinity" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Telerik.Sitefinity.Resources.Scripts.Kendo.styles.kendo_common_min.css"
        Static="True" />
    <sitefinity:ResourceFile Name="Telerik.Sitefinity.Resources.Scripts.Kendo.kendo.web.min.js" />
      <sitefinity:ResourceFile Name="Case.Framework.Sitefinity.Widgets.CTA.Resources.KendoWidgetStyle.css"
        Static="true" AssemblyInfo="Case.Framework.Sitefinity.Widgets.CTA.CTAWidget, Case.Framework.Sitefinity.Widgets" />
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>

<div id="selectorTagPageId" runat="server" style="display: none;">
    <sitefinity:PagesSelector runat="server" ID="pageSelectorPageId" 
        AllowExternalPagesSelection="true" AllowMultipleSelection="false" />
</div>

<div class="sfContentViews">
<div class="sfColWrapper sfEqualCols sfModeSelector sfClearfix sfNavDesignerCtrl sfNavDim">
<div id="RotatorDesignChoice" class="sfLeftCol">
    <h2 class="sfStep1">Add Buttons & Pick Display Style</h2>
    <br />
    <div id="ViewMode">
            <ul id="panelBar">
                <li>
                    <div id="lnkOptions">
                        <div id="lnkFieldEvents">
                            <div class="sfExample">
                                <span style="font-weight: bold">Choose audience type for the Call To Action: </span>
                            </div>
                            <div>
                                <select name="selPageType" id="selPageType">
                                    <option value="">Unknown</option>
                                    <option value="graduate">Graduate Students</option>
                                    <option value="undergraduate">Undergraduate Students</option>
                                    <option value="tech">Need Tech Help</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lnkOptions">
                        <div id="lnkFieldEvents">
                            <div class="sfExample">
                                <span style="font-weight: bold">Select the view mode (Horizontal, Vertical): </span>
                            </div>
                            <div>
                                <telerik:RadComboBox ID="rdViewMode" runat="server" Width="100%" Skin="Metro"
                                    EmptyMessage="Choose a view..." CssClass="comboWrapper">
                                </telerik:RadComboBox>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="sfStep2Options">
        </div>
    </div>
    <br />
    <div class="sfModeSwitcher">
		<a id="aAddTab" class="sfLinkBtn"><strong class="sfLinkBtnIn">Add Button</strong></a>
    </div>
    <br />
   <div class="sfExample">
        <span style="font-weight: bold">Buttons: </span>
   </div>  
    <ul id="ulAdded" class="sfRadioList RotatorDesignList"> </ul>
    <br />
</div>
<div class="sfRightCol">
    <h2 class="sfStep2">Enter URL & Text for Buttons.</h2>
    <div id="divStep2" class="sfStep2Options" style="display: none;">    
        <div id="groupSettingPageSelect">
            <div class="sfExpandableSection">
                <ul class="sfRadioList RotatorDesignList">
                   <li>
                        <label style="color: #333;">Title</label>
                        <br />
                        <input id="txtTitle" type="text" class="sfTxt" />
                    </li>
                     <li>
                        <label style="color: #333;">URL</label>
                        <br />
                        <span class="sfLinkBtn sfChange">
                            <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonPageId">
                                <asp:Literal runat="server" Text="Select Page..." />
                            </a>
                        </span>
                        <input id="txtUrl" type="text" class="sfTxt" disabled="disabled" />
                        <input id="txtUrlGuid" type="text" class="sfTxt" style="width: 600px; visibility: hidden;" />
                    </li>
                </ul>
                <br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        </div>    
    </div>
</div>
</div>
</div>
