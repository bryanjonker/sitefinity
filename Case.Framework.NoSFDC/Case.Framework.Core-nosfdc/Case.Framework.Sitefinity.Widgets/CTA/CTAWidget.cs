﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.CTA
{
    [RequireScriptManager]
    [ControlDesigner(typeof(CTAWidgetDesigner))]
    public class CTAWidget : SimpleView
    {
        private string data = string.Empty;
        private string viewMode = string.Empty;
        private string pageType = string.Empty;

        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string PageType
        {
            get
            {
                return pageType;
            }
            set
            {
                pageType = value;
            }
        }

        public string ViewMode
        {
            get
            {
                return viewMode;
            }
            set
            {
                viewMode = value;
            }
        }
        protected virtual Literal ltJavaScript
        {
            get
            {
                return this.Container.GetControl<Literal>("ltJavaScript", true);
            }
        }
        protected virtual Literal ltCTA
        {
            get
            {
                return this.Container.GetControl<Literal>("ltCTA", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                List<Section> sections = new JavaScriptSerializer().Deserialize<List<Section>>(Data);
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);
                for (int x = 0; x < sections.Count; x++)
                {
                    if (viewMode == "Horizontal")
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "button white");
                        writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + sections[x].Name);
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                        writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + sections[x].Name);
                        writer.AddAttribute(HtmlTextWriterAttribute.Style, "width: 100%; display: block");
                    }

                    writer.AddAttribute(HtmlTextWriterAttribute.Href, GetUrl(sections[x]));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(sections[x].Name.Replace("_", " ") + "&nbsp;&rsaquo;");
                    writer.RenderEndTag(); //A
                }
                ltCTA.Text = sWriter.ToString();
                var scriptTag = "<script type='text/javascript'>var pageType = '{0}';</script>";
                if (this.pageType.Equals("graduate", StringComparison.OrdinalIgnoreCase))
                {
                    ltJavaScript.Text = string.Format(scriptTag, "graduate");
                }
                else if (this.pageType.Equals("undergraduate", StringComparison.OrdinalIgnoreCase))
                {
                    ltJavaScript.Text = string.Format(scriptTag, "undergraduate");
                }
                else if (this.pageType.Equals("tech", StringComparison.OrdinalIgnoreCase))
                {
                    ltJavaScript.Text = string.Format(scriptTag, "tech");
                }
                else
                {
                    ltJavaScript.Text = string.Format(scriptTag, string.Empty);
                }
            }
            catch { }
        }


        protected override string LayoutTemplateName
        {
            get 
            {
                return this.viewMode == "Horizontal" ?
                    "Case.Framework.Sitefinity.Widgets.CTA.Resources.Views.CTAWidget.ascx" :
                    "Case.Framework.Sitefinity.Widgets.CTA.Resources.Views.CTAWidgetVert.ascx";
            }
        }
        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }

        private static string GetUrl(Section section)
        {
            if (section.URLGuid != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(section.URLGuid);
                if (page != null)
                {
                    return page.Url;
                }
            }
            return section.URL;
        }

        public class Section
        {
            public string Title;
            public string Name;
            public string Index;
            public string URL;
            public Guid URLGuid;
        }
    }
}
