﻿using System.Runtime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.WebControls
{
    public class CustomSiteMapNodeItem : WebControl, IDataItemContainer
    {
        private readonly int _itemIndex;

        private SiteMapNodeItemType _itemType;

        /// <summary>
        ///     Retrieves the index that the <see cref="T:System.Web.UI.WebControls.SiteMapPath" /> control uses to track and
        ///     manage the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> in its internal collections.
        /// </summary>
        /// <returns>
        ///     An integer that represents the location of the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> in
        ///     the <see cref="T:System.Web.UI.WebControls.SiteMapPath" /> control's internal collections.
        /// </returns>
        public virtual int ItemIndex
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")] get { return _itemIndex; }
        }

        /// <summary>Retrieves the functional type of the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" />.</summary>
        /// <returns>
        ///     A member of the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItemType" /> enumeration that indicates the
        ///     functional role of the node item in the navigation path hierarchy.
        /// </returns>
        public virtual SiteMapNodeItemType ItemType
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")] get { return _itemType; }
        }

        /// <summary>
        ///     Gets or sets the <see cref="T:System.Web.SiteMapNode" /> object that the
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> represents.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Web.SiteMapNode" /> object that the
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapPath" /> control uses to display a site navigation user interface.
        /// </returns>
        public virtual SiteMapNode SiteMapNode { get; set; }

        /// <summary>Gets an object that is used in simplified data-binding operations.</summary>
        /// <returns>An object that represents the value to use when data-binding operations are performed.</returns>
        object IDataItemContainer.DataItem
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")] get { return SiteMapNode; }
        }


        /// <summary>Gets the index of the data item that is bound to the control.</summary>
        /// <returns>
        ///     An integer that represents the location of the data item in the
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapPath" /> control's internal collections.
        /// </returns>
        int IDataItemContainer.DataItemIndex
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")] get { return ItemIndex; }
        }

        /// <summary>Gets the position of the data item as displayed in the control.</summary>
        /// <returns>
        ///     An integer that represents the location of the data item in the
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapPath" /> control's internal collections.
        /// </returns>
        int IDataItemContainer.DisplayIndex
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")] get { return ItemIndex; }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> class, using the
        ///     specified index and <see cref="T:System.Web.UI.WebControls.SiteMapNodeItemType" />.
        /// </summary>
        /// <param name="itemIndex">
        ///     The index in the <see cref="P:System.Web.UI.Control.Controls" /> collection that the
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapPath" /> control uses to track the
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> object.
        /// </param>
        /// <param name="itemType">
        ///     The functional type of <see cref="T:System.Web.SiteMapNode" /> that this
        ///     <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> represents.
        /// </param>
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public CustomSiteMapNodeItem(int itemIndex, SiteMapNodeItemType itemType)
        {
            _itemIndex = itemIndex;
            _itemType = itemType;
        }

        /// <summary>
        ///     Sets the current <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" />
        ///     <see cref="P:System.Web.UI.WebControls.SiteMapNodeItem.ItemType" /> property.
        /// </summary>
        /// <param name="itemType">One of the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItemType" /> values. </param>
        protected internal virtual void SetItemType(SiteMapNodeItemType itemType)
        {
            _itemType = itemType;
        }


        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Li; }
        }


        protected override void Render(HtmlTextWriter writer)
        {
            // RenderContents(writer);
            RenderBeginTag(writer);
            RenderContents(writer);
            RenderEndTag(writer);


            /* instead of: 

             */
        }
    }
}
