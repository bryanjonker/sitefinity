﻿using System;
using System.Runtime;

namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.WebControls
{
    public class CustomSiteMapNodeItemEventArgs : EventArgs
    {
        private readonly CustomSiteMapNodeItem _item;

        /// <summary>Gets the node item that is the source of the event.</summary>
        /// <returns>The <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> that is the source of the event.</returns>
        public CustomSiteMapNodeItem Item
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")] get { return _item; }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Web.UI.WebControls.SiteMapNodeItemEventArgs" /> class,
        ///     setting the specified <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> object as the source of the event.
        /// </summary>
        /// <param name="item">A <see cref="T:System.Web.UI.WebControls.SiteMapNodeItem" /> that is the source of the event. </param>
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public CustomSiteMapNodeItemEventArgs(CustomSiteMapNodeItem item)
        {
            _item = item;
        }
    }
}
