﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.WebControls
{
    using UIUC.Custom.DAL.DataAccess.Faculty;

    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class BreadcrumbTrail : CompositeDataBoundControl
    {
        private Style _rootNodeStyle;

        private Style _nodeStyle;

        private Style _currentNodeStyle;

        private Style _pathSeparatorStyle;

        private Style _mergedCurrentNodeStyle;

        private Style _mergedRootNodeStyle;

        [Category("Styles")]
        [DefaultValue(null)]
        [Description("The style applied to current node.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public Style CurrentNodeStyle
        {
            get
            {
                if (_currentNodeStyle != null) return _currentNodeStyle;
                _currentNodeStyle = new Style();
                if (IsTrackingViewState)
                {
                    ((IStateManager) _currentNodeStyle).TrackViewState();
                }
                return _currentNodeStyle;
            }
        }

        [Browsable(false)]
        [DefaultValue(null)]
        [Description("The template used for the current node.")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof (SiteMapNodeItem))]
        public virtual ITemplate CurrentNodeTemplate { get; set; }

        private Style MergedCurrentNodeStyle
        {
            get
            {
                if (_mergedCurrentNodeStyle != null) return _mergedCurrentNodeStyle;
                _mergedCurrentNodeStyle = new Style();
                _mergedCurrentNodeStyle.CopyFrom(NodeStyle);
                _mergedCurrentNodeStyle.CopyFrom(CurrentNodeStyle);
                return _mergedCurrentNodeStyle;
            }
        }


        private Style MergedRootNodeStyle
        {
            get
            {
                if (_mergedRootNodeStyle == null)
                {
                    _mergedRootNodeStyle = new Style();
                    _mergedRootNodeStyle.CopyFrom(NodeStyle);
                    _mergedRootNodeStyle.CopyFrom(RootNodeStyle);
                }
                return _mergedRootNodeStyle;
            }
        }

        [Category("Styles")]
        [DefaultValue(null)]
        [Description("The style applied to navigation nodes.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public Style NodeStyle
        {
            get
            {
                if (_nodeStyle != null) return _nodeStyle;
                _nodeStyle = new Style();
                if (IsTrackingViewState)
                {
                    ((IStateManager) _nodeStyle).TrackViewState();
                }
                return _nodeStyle;
            }
        }

        [Browsable(false)]
        [DefaultValue(null)]
        [Description("The template used for the navigation nodes.")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof (SiteMapNodeItem))]
        public virtual ITemplate NodeTemplate { get; set; }

        [Category("Behavior")]
        [DefaultValue(-1)]
        [Description("The number of parent nodes to display.")]
        [Themeable(false)]
        public virtual int ParentLevelsDisplayed
        {
            get
            {
                var item = ViewState["ParentLevelsDisplayed"];
                if (item == null)
                {
                    return -1;
                }
                return (int) item;
            }
            set
            {
                if (value < -1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                ViewState["ParentLevelsDisplayed"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(0)]
        [Description("The direction of path to render.")]
        public virtual PathDirection PathDirection
        {
            get
            {
                var item = ViewState["PathDirection"];
                if (item == null)
                {
                    return PathDirection.RootToCurrent;
                }
                return (PathDirection) item;
            }
            set
            {
                if (value < PathDirection.RootToCurrent || value > PathDirection.CurrentToRoot)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                ViewState["PathDirection"] = value;
            }
        }
        [Category("Appearance")]
        [DefaultValue(false)]
        [Description("Show alternate view.")]
        public virtual bool ShowAlternateView
        {
            get
            {
                var item = ViewState["ShowAlternateView"];
                if (item == null)
                {
                    return true;
                }
                return (bool)item;
            }
            set { ViewState["ShowAlternateView"] = value; }

        }

        [Category("Appearance")]
        [DefaultValue(" > ")]
        [Description("The separator string between each node.")]
        [Localizable(true)]
        public virtual string PathSeparator
        {
            get
            {
                var item = (string) ViewState["PathSeparator"];
                if (item == null)
                {
                    return " > ";
                }
                return item;
            }
            set { ViewState["PathSeparator"] = value; }
        }

        [Category("Styles")]
        [DefaultValue(null)]
        [Description("The style applied to the path separators.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public Style PathSeparatorStyle
        {
            get
            {
                if (_pathSeparatorStyle != null) return _pathSeparatorStyle;
                _pathSeparatorStyle = new Style();
                if (IsTrackingViewState)
                {
                    ((IStateManager) _pathSeparatorStyle).TrackViewState();
                }
                return _pathSeparatorStyle;
            }
        }

        [Browsable(false)]
        [DefaultValue(null)]
        [Description("The template used for the path separators.")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof (SiteMapNodeItem))]
        public virtual ITemplate PathSeparatorTemplate { get; set; }

        [Category("Appearance")]
        [DefaultValue(false)]
        [Description("Indicates whether the current node will be rendered as a link.")]
        public virtual bool RenderCurrentNodeAsLink
        {
            get
            {
                var item = ViewState["RenderCurrentNodeAsLink"];
                if (item == null)
                {
                    return false;
                }
                return (bool) item;
            }
            set { ViewState["RenderCurrentNodeAsLink"] = value; }
        }

        [Category("Styles")]
        [DefaultValue(null)]
        [Description("The style applied to root node.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public Style RootNodeStyle
        {
            get
            {
                if (_rootNodeStyle != null) return _rootNodeStyle;
                _rootNodeStyle = new Style();
                if (IsTrackingViewState)
                {
                    ((IStateManager) _rootNodeStyle).TrackViewState();
                }
                return _rootNodeStyle;
            }
        }

        [Browsable(false)]
        [DefaultValue(null)]
        [Description("The template used for the root node.")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof (SiteMapNodeItem))]
        public virtual ITemplate RootNodeTemplate { get; set; }

        [Category("Behavior")]
        [DefaultValue(true)]
        [Description("Indicates whether tooltip will be shown.")]
        [Themeable(false)]
        public virtual bool ShowToolTips
        {
            get
            {
                var item = ViewState["ShowToolTips"];
                if (item == null)
                {
                    return true;
                }
                return (bool) item;
            }
            set { ViewState["ShowToolTips"] = value; }
        }

        [Category("Accessibility")]
        [DefaultValue("Skip Navigation Links")]
        [Description(
            "The text that appears in the ALT attribute of the invisible image link that allows screen readers to skip repetitive content."
            )]
        [Localizable(true)]
        public virtual string SkipLinkText
        {
            get
            {
                var item = ViewState["SkipLinkText"] as string;
                return item ?? "Skip Navigation Links";
            }
            set { ViewState["SkipLinkText"] = value; }
        }

        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            IList<SiteMapNode> list = (
                from SiteMapNode x in dataSource
                where !ReferenceEquals(x, null)
                select x).ToList<SiteMapNode>();
            int count = 0;
            if (ParentLevelsDisplayed > -1)
            {
                count = list.Count - ParentLevelsDisplayed - 1;
                if (count < 0)
                {
                    count = 0;
                }
            }
            var num = 0;
            for (var i = count; i < list.Count; i++)
            {
                var item = list[i];
                if (i == list.Count - 1)
                {
                    if (ShowAlternateView)
                    {

                        if (item.Title.ToLower() == "article" || item.Title.ToLower() == "news")
                        {
                            var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
                            if (parms != null)
                            {
                                var lastParam = parms.Last();
                                var article = CaseManagers.News.GetAll().FirstOrDefault(x => x.Slug == lastParam);
                                if (article != null)
                                {
                                    var newItem = new SiteMapNode(item.Provider, item.Key, item.Url, article.Title, item.Description);
                                    int num1 = num;
                                    num = num1 + 1;
                                    this.CreateItem(num1, SiteMapNodeItemType.Current, newItem);

                                }
                                else
                                {
                                    int num1 = num;
                                    num = num1 + 1;
                                    this.CreateItem(num1, SiteMapNodeItemType.Current, item);
                                }
                            }
                            else
                            {
                                int num1 = num;
                                num = num1 + 1;
                                this.CreateItem(num1, SiteMapNodeItemType.Current, item);

                            }
                        }
                        else if (item.Title.ToLower() == "event")
                        {
                            var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
                            if (parms != null)
                            {
                                var lastParam = parms.Last();
                                var article = CaseManagers.Events.GetAll().FirstOrDefault(x => x.Slug == lastParam);
                                if (article != null)
                                {
                                    var newItem = new SiteMapNode(item.Provider, item.Key, item.Url, article.Title, item.Description);
                                    int num1 = num;
                                    num = num1 + 1;
                                    this.CreateItem(num1, SiteMapNodeItemType.Current, newItem);

                                }
                                else
                                {
                                    int num1 = num;
                                    num = num1 + 1;
                                    this.CreateItem(num1, SiteMapNodeItemType.Current, item);
                                }
                            }
                            else
                            {
                                int num1 = num;
                                num = num1 + 1;
                                this.CreateItem(num1, SiteMapNodeItemType.Current, item);

                            }
                        }
                        else if (item.Title.ToLower() == "faculty profiles")
                        {
                            var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
                            if (parms != null)
                            {
                                var lastParam = parms.Last();
                                var exportProcess = new ExportProcess();
                                var faculty = exportProcess.GetFacultySingle(lastParam);
                                if (faculty != null)
                                {
                                    var newItem = new SiteMapNode(item.Provider, item.Key, item.Url, faculty.FullName, item.Description);
                                    int num1 = num;
                                    num = num1 + 1;
                                    this.CreateItem(num1, SiteMapNodeItemType.Current, newItem);

                                }
                                else
                                {
                                    int num1 = num;
                                    num = num1 + 1;
                                    this.CreateItem(num1, SiteMapNodeItemType.Current, item);
                                }
                            }
                            else
                            {
                                int num1 = num;
                                num = num1 + 1;
                                this.CreateItem(num1, SiteMapNodeItemType.Current, item);

                            }
                        }
                        else
                        {
                            int num1 = num;
                            num = num1 + 1;
                            this.CreateItem(num1, SiteMapNodeItemType.Current, item);

                        }
                    }
                }
                else if (i != 0)
                {
                    //int num5 = num;
                    //num = num5 + 1;
                    //this.CreateItem(num5, SiteMapNodeItemType.PathSeparator, null);
                    //num++;
                    var num2 = num;
                    num = num2 + 1;
                    CreateItem(num2, SiteMapNodeItemType.Parent, item);
                }
                else
                {
                    var num4 = num;
                    num = num4 + 1;
                    CreateItem(num4, SiteMapNodeItemType.Root, item);
                }
            }
            return num;
        }

        private void CreateItem(int itemIndex, SiteMapNodeItemType itemType, SiteMapNode node)
        {
            var siteMapNodeItem = new CustomSiteMapNodeItem(itemIndex, itemType);
            var siteMapNodeItemEventArg = new CustomSiteMapNodeItemEventArgs(siteMapNodeItem);
            siteMapNodeItem.SiteMapNode = node;
            InitializeItem(siteMapNodeItem);
            OnItemCreated(siteMapNodeItemEventArg);
            if (PathDirection != PathDirection.CurrentToRoot)
            {
                Controls.AddAt(-1, siteMapNodeItem);
            }
            else
            {
                Controls.AddAt(0, siteMapNodeItem);
            }
            siteMapNodeItem.DataBind();
            OnItemDataBound(siteMapNodeItemEventArg);
            siteMapNodeItem.SiteMapNode = null;
            siteMapNodeItem.EnableViewState = false;
        }

        protected virtual void InitializeItem(CustomSiteMapNodeItem item)
        {
            ITemplate rootNodeTemplate = null;
            Style mergedRootNodeStyle = null;
            SiteMapNodeItemType itemType = item.ItemType;
            SiteMapNode siteMapNode = item.SiteMapNode;
            switch (itemType)
            {
                case SiteMapNodeItemType.Root:
                {
                    rootNodeTemplate = RootNodeTemplate ?? NodeTemplate;
                    mergedRootNodeStyle = MergedRootNodeStyle;
                    break;
                }
                case SiteMapNodeItemType.Parent:
                {
                    rootNodeTemplate = NodeTemplate;
                    mergedRootNodeStyle = _nodeStyle;
                    break;
                }
                case SiteMapNodeItemType.Current:
                {
                    rootNodeTemplate = CurrentNodeTemplate ?? NodeTemplate;
                    mergedRootNodeStyle = MergedCurrentNodeStyle;
                    break;
                }
                case SiteMapNodeItemType.PathSeparator:
                {
                    rootNodeTemplate = PathSeparatorTemplate;
                    mergedRootNodeStyle = _pathSeparatorStyle;
                    break;
                }
            }
            if (rootNodeTemplate != null)
            {
                rootNodeTemplate.InstantiateIn(item);
                item.ApplyStyle(mergedRootNodeStyle);
                return;
            }
            if (itemType == SiteMapNodeItemType.PathSeparator)
            {
                var literal = new Literal
                {
                    Mode = LiteralMode.Encode,
                    Text = PathSeparator
                };
                item.Controls.Add(literal);
                item.ApplyStyle(mergedRootNodeStyle);
                return;
            }
            if (itemType == SiteMapNodeItemType.Current && !RenderCurrentNodeAsLink)
            {
                var literal1 = new Literal
                {
                    Mode = LiteralMode.Encode,
                    Text = siteMapNode.Title
                };
                item.Controls.Add(literal1);
                item.ApplyStyle(mergedRootNodeStyle);
                return;
            }
            var hyperLink = new HyperLink();

            if (mergedRootNodeStyle != null)
            {
                hyperLink.Font.Underline = mergedRootNodeStyle.Font.Underline;
            }
            hyperLink.EnableTheming = false;
            hyperLink.Enabled = Enabled;
            if (!siteMapNode.Url.StartsWith("\\\\", StringComparison.Ordinal))
            {
                hyperLink.NavigateUrl = (Context != null
                    ? Context.Response.ApplyAppPathModifier(
                        ResolveClientUrl(HttpUtility.UrlPathEncode(siteMapNode.Url)))
                    : siteMapNode.Url);
            }
            else
            {
                hyperLink.NavigateUrl = ResolveClientUrl(HttpUtility.UrlPathEncode(siteMapNode.Url));
            }
            hyperLink.Text = HttpUtility.HtmlEncode(siteMapNode.Title);
            if (ShowToolTips)
            {
                hyperLink.ToolTip = siteMapNode.Description;
            }

            item.Controls.Add(hyperLink);
            hyperLink.ApplyStyle(mergedRootNodeStyle);
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
            }
            else
            {
                var objArray = (object[]) savedState;
                base.LoadViewState(objArray[0]);
                if (objArray[1] != null)
                {
                    ((IStateManager) CurrentNodeStyle).LoadViewState(objArray[1]);
                }
                if (objArray[2] != null)
                {
                    ((IStateManager) NodeStyle).LoadViewState(objArray[2]);
                }
                if (objArray[3] != null)
                {
                    ((IStateManager) RootNodeStyle).LoadViewState(objArray[3]);
                }
                if (objArray[4] != null)
                {
                    ((IStateManager) PathSeparatorStyle).LoadViewState(objArray[4]);
                }
            }
        }

        protected virtual void OnItemCreated(CustomSiteMapNodeItemEventArgs e)
        {
            if (ItemCreated != null)
            {
                ItemCreated(this, e);
            }
        }

        protected virtual void OnItemDataBound(CustomSiteMapNodeItemEventArgs e)
        {
            if (ItemDataBound != null)
            {
                ItemDataBound(this, e);
            }
        }

        protected override object SaveViewState()
        {
            object[] objArray = {base.SaveViewState(), null, null, null, null};
            objArray[1] = (_currentNodeStyle != null ? ((IStateManager) _currentNodeStyle).SaveViewState() : null);
            objArray[2] = (_nodeStyle != null ? ((IStateManager) _nodeStyle).SaveViewState() : null);
            objArray[3] = (_rootNodeStyle != null ? ((IStateManager) _rootNodeStyle).SaveViewState() : null);
            objArray[4] = (_pathSeparatorStyle != null ? ((IStateManager) _pathSeparatorStyle).SaveViewState() : null);
            var objArray1 = objArray;
            return objArray1.Any(t => t != null) ? objArray1 : null;
        }

        protected override void TrackViewState()
        {
            base.TrackViewState();
            if (_currentNodeStyle != null)
            {
                ((IStateManager) _currentNodeStyle).TrackViewState();
            }
            if (_nodeStyle != null)
            {
                ((IStateManager) _nodeStyle).TrackViewState();
            }
            if (_rootNodeStyle != null)
            {
                ((IStateManager) _rootNodeStyle).TrackViewState();
            }
            if (_pathSeparatorStyle != null)
            {
                ((IStateManager) _pathSeparatorStyle).TrackViewState();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            RenderContents(writer);
        }

        [Category("Action")]
        [Description("Fires when an item is created.")]
        public event CustomSiteMapNodeItemEventHandler ItemCreated;

        [Category("Action")]
        [Description("Fires when an item is databound.")]
        public event CustomSiteMapNodeItemEventHandler ItemDataBound;
    }
}
