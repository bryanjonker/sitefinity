﻿Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation");

Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner = function (element) {
    Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner.initializeBase(this, [element]);

    this._renderCurrentNodeAsLinkCheckBox = null;
    this._useHomePageAsRootNodeCheckBox = null;
    this._showToolTipsCheckBox = null;
    this._parentLevelsDisplayedTextBox = null;
    this._pathSeparatorTextBox = null;
    this._pathDirectionComboBox = null;
    this._nodeCssClassTextBox = null;
    this._currentNodeCssClassTextBox = null;
    this._rootNodeCssClassTextBox = null;
    this._pathSeparatorCssClassTextBox = null;
    this._customTemplatePathTextBox = null;
    this._showAlternateViewCheckBox = null;

    this._radioSelectionChangedDelegate = null;
}

Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */

    initialize: function () {
        Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner.callBaseMethod(this, 'initialize');

        this._toogleDesignSettingsDelegate = Function.createDelegate(this, function () {
            $("#groupDesignSettings").toggleClass("sfExpandedSection");
            dialogBase.resizeToContent();
        });

        this._radioSelectionChangedDelegate = Function.createDelegate(this, this.radioSelectionChanged);

        jQuery("#expanderDesignSettings").click(this._toogleDesignSettingsDelegate);
    },

    dispose: function () {
        Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner.callBaseMethod(this, 'dispose');

        $clearHandlers(this.get_dontLimitParentLevelsDisplayedRadio());
        $clearHandlers(this.get_limitParentLevelsDisplayedRadio());

        if (this._radioSelectionChangedDelegate != null) {
            delete this._radioSelectionChangedDelegate;

            this._radioSelectionChangedDelegate = null;
        }
    },

    /* --------------------------------- public methods --------------------------------- */
    // implementation of IControlDesigner: Forces the control to refresh from the control Data
    refreshUI: function () {

        var controlData = this.get_controlData();

        this.get_renderCurrentNodeAsLinkCheckBox().checked = controlData.RenderCurrentNodeAsLink;

        this.get_includeGroupPagesCheckBox().checked = controlData.IncludeGroupPages;

        this.get_useHomePageAsRootNodeCheckBox().checked = controlData.UseHomePageAsRootNode;

        this.get_showToolTipsCheckBox().checked = controlData.ShowToolTips;

        this.get_showAlternateViewCheckBox().checked = controlData.ShowAlternateView;

        this.get_parentLevelsDisplayedTextBox().set_value(controlData.ParentLevelsDisplayed);

        this.get_pathSeparatorTextBox().set_value(controlData.PathSeparator);

        var pathDirectionItem = this.get_pathDirectionComboBox().findItemByValue(controlData.PathDirection);

        if (pathDirectionItem)
            pathDirectionItem.select();

        this.get_nodeCssClassTextBox().set_value(controlData.NodeCssClass);

        this.get_currentNodeCssClassTextBox().set_value(controlData.CurrentNodeCssClass);

        this.get_rootNodeCssClassTextBox().set_value(controlData.RootNodeCssClass);

        this.get_pathSeparatorCssClassTextBox().set_value(controlData.PathSeparatorCssClass);

        this.get_customTemplatePathTextBox().set_value(controlData.LayoutTemplatePath);

        this.get_dontLimitParentLevelsDisplayedRadio().onclick = this._radioSelectionChangedDelegate;
        this.get_limitParentLevelsDisplayedRadio().onclick = this._radioSelectionChangedDelegate;

        if (controlData.ParentLevelsDisplayed >= 0) {
            this.get_limitParentLevelsDisplayedRadio().checked = true;
        }
        else {
            this.get_dontLimitParentLevelsDisplayedRadio().checked = true;
        }

        this.radioSelectionChanged();
    },

    // implementation of IControlDesigner: forces the designer view to apply the changes on UI to the control Data
    applyChanges: function () {

        var controlData = this.get_controlData();

        controlData.RenderCurrentNodeAsLink = this.get_renderCurrentNodeAsLinkCheckBox().checked;

        controlData.IncludeGroupPages = this.get_includeGroupPagesCheckBox().checked;

        controlData.UseHomePageAsRootNode = this.get_useHomePageAsRootNodeCheckBox().checked;

        controlData.ShowToolTips = this.get_showToolTipsCheckBox().checked;

        controlData.ShowAlternateView = this.get_showAlternateViewCheckBox().checked;

        controlData.ParentLevelsDisplayed = this.get_parentLevelsToDisplay();

        controlData.PathSeparator = this.get_pathSeparatorTextBox().get_value();

        controlData.PathDirection = this.get_pathDirectionComboBox().get_value();

        controlData.NodeCssClass = this.get_nodeCssClassTextBox().get_value();

        controlData.CurrentNodeCssClass = this.get_currentNodeCssClassTextBox().get_value();

        controlData.RootNodeCssClass = this.get_rootNodeCssClassTextBox().get_value();

        controlData.LayoutTemplatePath = this.get_customTemplatePathTextBox().get_value();

        controlData.PathSeparatorCssClass = this.get_pathSeparatorCssClassTextBox().get_value();
    },


    /* --------------------------------- event handlers --------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties --------------------------------- */


    get_renderCurrentNodeAsLinkCheckBox: function () {
        return this._renderCurrentNodeAsLinkCheckBox;
    },
    set_renderCurrentNodeAsLinkCheckBox: function (value) {
        if (this._renderCurrentNodeAsLinkCheckBox != value) {
            this._renderCurrentNodeAsLinkCheckBox = value;
            this.raisePropertyChanged("renderCurrentNodeAsLinkCheckBox");
        }
    },

    get_includeGroupPagesCheckBox: function () {
        return this._includeGroupPagesCheckBox;
    },
    set_includeGroupPagesCheckBox: function (value) {
        if (this._includeGroupPagesCheckBox != value) {
            this._includeGroupPagesCheckBox = value;
            this.raisePropertyChanged("includeGroupPagesCheckBox");
        }
    },

    get_useHomePageAsRootNodeCheckBox: function () {
        return this._useHomePageAsRootNodeCheckBox;
    },
    set_useHomePageAsRootNodeCheckBox: function (value) {
        if (this._useHomePageAsRootNodeCheckBox != value) {
            this._useHomePageAsRootNodeCheckBox = value;
            this.raisePropertyChanged("useHomePageAsRootNodeCheckBox");
        }
    },

    get_showToolTipsCheckBox: function () {
        return this._showToolTipsCheckBox;
    },
    set_showToolTipsCheckBox: function (value) {
        if (this._showToolTipsCheckBox != value) {
            this._showToolTipsCheckBox = value;
            this.raisePropertyChanged("showToolTipsCheckBox");
        }
    },

    get_showAlternateViewCheckBox: function () {
        return this._showAlternateViewCheckBox;
    },
    set_showAlternateViewCheckBox: function (value) {
        if (this._showAlternateViewCheckBox != value) {
            this._showAlternateViewCheckBox = value;
            this.raisePropertyChanged("showAlternateViewCheckBox");
        }
    },

    get_parentLevelsDisplayedTextBox: function () {
        return this._parentLevelsDisplayedTextBox;
    },
    set_parentLevelsDisplayedTextBox: function (value) {
        if (this._parentLevelsDisplayedTextBox != value) {
            this._parentLevelsDisplayedTextBox = value;
            this.raisePropertyChanged("parentLevelsDisplayedTextBox");
        }
    },

    get_pathSeparatorTextBox: function () {
        return this._pathSeparatorTextBox;
    },
    set_pathSeparatorTextBox: function (value) {
        if (this._pathSeparatorTextBox != value) {
            this._pathSeparatorTextBox = value;
            this.raisePropertyChanged("pathSeparatorTextBox");
        }
    },

    get_pathDirectionComboBox: function () {
        return this._pathDirectionComboBox;
    },
    set_pathDirectionComboBox: function (value) {
        if (this._pathDirectionComboBox != value) {
            this._pathDirectionComboBox = value;
            this.raisePropertyChanged("pathDirectionComboBox");
        }
    },

    get_nodeCssClassTextBox: function () {
        return this._nodeCssClassTextBox;
    },
    set_nodeCssClassTextBox: function (value) {
        if (this._nodeCssClassTextBox != value) {
            this._nodeCssClassTextBox = value;
            this.raisePropertyChanged("nodeCssClassTextBox");
        }
    },

    get_currentNodeCssClassTextBox: function () {
        return this._currentNodeCssClassTextBox;
    },
    set_currentNodeCssClassTextBox: function (value) {
        if (this._currentNodeCssClassTextBox != value) {
            this._currentNodeCssClassTextBox = value;
            this.raisePropertyChanged("currentNodeCssClassTextBox");
        }
    },

    get_rootNodeCssClassTextBox: function () {
        return this._rootNodeCssClassTextBox;
    },
    set_rootNodeCssClassTextBox: function (value) {
        if (this._rootNodeCssClassTextBox != value) {
            this._rootNodeCssClassTextBox = value;
            this.raisePropertyChanged("rootNodeCssClassTextBox");
        }
    },

    get_pathSeparatorCssClassTextBox: function () {
        return this._pathSeparatorCssClassTextBox;
    },
    set_pathSeparatorCssClassTextBox: function (value) {
        if (this._pathSeparatorCssClassTextBox != value) {
            this._pathSeparatorCssClassTextBox = value;
            this.raisePropertyChanged("pathSeparatorCssClassTextBox");
        }
    },

    get_customTemplatePathTextBox: function () {
        return this._customTemplatePathTextBox;
    },
    set_customTemplatePathTextBox: function (value) {
        if (this._customTemplatePathTextBox != value) {
            this._customTemplatePathTextBox = value;
            this.raisePropertyChanged("customTemplatePathTextBox");
        }
    },

    get_limitParentLevelsDisplayedRadio: function () {
        return this._limitParentLevelsDisplayedRadio;
    },
    set_limitParentLevelsDisplayedRadio: function (value) {
        if (this._limitParentLevelsDisplayedRadio != value) {
            this._limitParentLevelsDisplayedRadio = value;
            this.raisePropertyChanged("limitParentLevelsDisplayedRadio");
        }
    },

    get_dontLimitParentLevelsDisplayedRadio: function () {
        return this._dontLimitParentLevelsDisplayedRadio;
    },
    set_dontLimitParentLevelsDisplayedRadio: function (value) {
        if (this._dontLimitParentLevelsDisplayedRadio != value) {
            this._dontLimitParentLevelsDisplayedRadio = value;
            this.raisePropertyChanged("dontLimitParentLevelsDisplayedRadio");
        }
    },

    radioSelectionChanged: function () {

        if (this.get_dontLimitParentLevelsDisplayedRadio().checked) {
            this.get_parentLevelsDisplayedTextBox().disable();
        }
        else {
            this.get_parentLevelsDisplayedTextBox().enable();
        }
    },

    get_parentLevelsToDisplay: function () {

        if (this.get_limitParentLevelsDisplayedRadio().checked) {
            return this.get_parentLevelsDisplayedTextBox().get_value();
        }
        else {
            return -1;
        }
    }
}

Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.BreadcrumbsDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();