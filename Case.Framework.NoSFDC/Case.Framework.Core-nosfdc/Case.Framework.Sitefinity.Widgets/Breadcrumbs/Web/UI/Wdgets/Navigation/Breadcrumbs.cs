﻿namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.WebControls;
    using Case.Framework.Sitefinity.Widgets.SubSiteFooter;

    using Telerik.Sitefinity.GenericContent.Model;
    using Telerik.Sitefinity.Model;
    using Telerik.Sitefinity.Modules.Events;
    using Telerik.Sitefinity.Modules.News;
    using Telerik.Sitefinity.Modules.Pages;
    using Telerik.Sitefinity.Pages.Model;
    using Telerik.Sitefinity.Services;
    using Telerik.Sitefinity.Web;
    using Telerik.Sitefinity.Web.UI;
    using Telerik.Sitefinity.Web.UI.ControlDesign;

    [ControlDesigner(typeof(BreadcrumbsDesigner))]
    public class Breadcrumbs : SimpleView
    {
        private int parentLevelsDisplayed = -1;

        public Breadcrumbs()
        {
            this.RenderCurrentNodeAsLink = false;
            this.UseHomePageAsRootNode = true;
            this.IncludeGroupPages = false;
            this.ShowToolTips = true;
            this.PathSeparator = " > ";
            this.PathDirection = PathDirection.RootToCurrent;
            this.NodeCssClass = string.Empty;
            this.CurrentNodeCssClass = string.Empty;
            this.RootNodeCssClass = string.Empty;
            this.PathSeparatorCssClass = string.Empty;
        }

        public string CurrentNodeCssClass { get; set; }

        public bool IncludeGroupPages { get; set; }

        public string NodeCssClass { get; set; }

        public int ParentLevelsDisplayed
        {
            get { return this.parentLevelsDisplayed; }
            set
            {
                if (value < -1)
                {
                    // ReSharper disable NotResolvedInText
                    throw new ArgumentOutOfRangeException("ParentLevelsDisplayed");
                    // ReSharper restore NotResolvedInText
                }
                this.parentLevelsDisplayed = value;
            }
        }

        public PathDirection PathDirection { get; set; }

        public string PathSeparator { get; set; }

        public string PathSeparatorCssClass { get; set; }

        public bool RenderCurrentNodeAsLink { get; set; }

        public string RootNodeCssClass { get; set; }

        public bool ShowAlternateView { get; set; }

        public bool ShowSocial { get; set; }

        public bool ShowToolTips { get; set; }

        public bool UseHomePageAsRootNode { get; set; }

        protected override string LayoutTemplateName
        {
            get
            {
                if (this.ShowAlternateView)
                    return
          "Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.Resources.Views.BreadcrumbsGrey.ascx";
                else
                    return
                        "Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.Resources.Views.Breadcrumbs.ascx";
            }
        }

        protected virtual Literal ltpageTitle
        {
            get
            {
                return this.Container.GetControl<Literal>("ltpageTitle", true);
            }
        }

        protected virtual Literal ltSocial
        {
            get
            {
                return this.Container.GetControl<Literal>("ltSocial", true);
            }
        }

        protected virtual Panel pnlShare
        {
            get
            {
                return this.Container.GetControl<Panel>("pnlShare", true);
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Div; }
        }

        private BreadcrumbTrail BreadcrumbTrailControl
        {
            get { return this.Container.GetControl<BreadcrumbTrail>("BreadcrumbTrail", true); }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            this.InitializeBreadcrumbTrailControl();
        }

        private static string GetNavTitle(SiteMapNode node)
        {
            if (node == null)
            {
                return string.Empty;
            }
            var pageNode = node as PageSiteNode;
            var pageAltTitleObject = pageNode.GetCustomFieldValue("ShortTitle");
            if (pageAltTitleObject != null)
            {
                var siteMapNodeTitle = pageAltTitleObject as string;
                return !string.IsNullOrEmpty(siteMapNodeTitle) ? pageAltTitleObject.ToString() : node.Title;
            }
            return node.Title;
        }

        private void InitializeBreadcrumbTrailControl()
        {
            this.BreadcrumbTrailControl.RenderCurrentNodeAsLink = this.RenderCurrentNodeAsLink;
            this.BreadcrumbTrailControl.PathSeparator = this.PathSeparator;
            this.BreadcrumbTrailControl.ParentLevelsDisplayed = this.ParentLevelsDisplayed;
            this.BreadcrumbTrailControl.ShowToolTips = this.ShowToolTips;
            this.BreadcrumbTrailControl.PathDirection = this.PathDirection;
            this.BreadcrumbTrailControl.CurrentNodeStyle.CssClass = this.CurrentNodeCssClass;
            this.BreadcrumbTrailControl.RootNodeStyle.CssClass = this.RootNodeCssClass;
            this.BreadcrumbTrailControl.NodeStyle.CssClass = this.NodeCssClass;
            this.BreadcrumbTrailControl.PathSeparatorStyle.CssClass = this.PathSeparatorCssClass;

            var siteMapNodeEqualityComparer = new SiteMapNodeEqualityComparer();
            var siteMapNodes = new List<SiteMapNode>();
            SiteMapNode currentNode = SiteMap.CurrentNode;
            PageManager manager = PageManager.GetManager();

            PageSiteNode node = (PageSiteNode)currentNode;
            if (currentNode != null)
            {
                try
                {
                    var sTitle = new StringWriter();
                    var titleWriter = new HtmlTextWriter(sTitle);

                    var titleTag = this.Page.Title;
                    if (this.Page.Title.ToLower() == "article" || this.Page.Title.ToLower() == "news")
                    {
                        var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
                        if (parms != null)
                        {
                            var lastParam = parms.Last();
                            var article = NewsManager.GetManager().GetNewsItems().FirstOrDefault(x => x.UrlName == lastParam && x.Status == ContentLifecycleStatus.Live);
                            if (article != null)
                            {
                                titleTag = article.Title;
                            }
                        }
                    }
                    else if (this.Page.Title.ToLower() == "event")
                    {
                        var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
                        if (parms != null)
                        {
                            var lastParam = parms.Last();
                            var eventItems = EventsManager.GetManager().GetEvents().Where(x => x.UrlName == lastParam && x.Status == ContentLifecycleStatus.Live);
                            var eventItem = !eventItems.Any() ? null :
                                eventItems.Count() == 1 ? eventItems.Single() :
                                eventItems.FirstOrDefault(e => HttpContext.Current.Request.Url.AbsolutePath.EndsWith(e.ItemDefaultUrl));
                            if (eventItem != null)
                            {
                                titleTag = eventItem.DoesFieldExist("TitleLong") && !string.IsNullOrWhiteSpace(eventItem.GetValue<string>("TitleLong")) ?
                                    eventItem.GetValue<string>("TitleLong") :
                                    (string)eventItem.Title;
                            }
                        }
                    }
                    if (titleTag.Length > 40)
                    {
                        titleWriter.AddAttribute("class", "small-h1");
                    }
                    titleWriter.RenderBeginTag(HtmlTextWriterTag.H1);
                    titleWriter.Write(titleTag);
                    titleWriter.RenderEndTag();
                    this.ltpageTitle.Text = sTitle.ToString();

                    if (node != null && (bool)node.GetCustomFieldValue("ShowSocialShare"))
                    {
                        this.pnlShare.Visible = true;
                        var sWriter = new StringWriter();
                        var writer = new HtmlTextWriter(sWriter);

                        //facebook
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "Facebook");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=700,height=400,top=500,left=500');return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://www.facebook.com/sharer.php?u=" + HttpContext.Current.Request.Url.AbsoluteUri);
                        writer.AddAttribute(HtmlTextWriterAttribute.Title, "Share on Facebook");
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#xF610;");
                        writer.RenderEndTag(); //A

                        //twitter
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "Twitter");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=700,height=400,top=500,left=500');return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://twitter.com/share?url=" + HttpContext.Current.Request.Url.AbsoluteUri);
                        writer.AddAttribute(HtmlTextWriterAttribute.Title, "Share on Twitter");
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#xF611;");
                        writer.RenderEndTag(); //A

                        //linked-inx
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "LinkedIn");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=700,height=400,top=500,left=500');return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://www.linkedin.com/shareArticle?mini=true&url=" + HttpContext.Current.Request.Url.AbsoluteUri);
                        writer.AddAttribute(HtmlTextWriterAttribute.Title, "Share on LinkedIn");
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#xF612;");
                        writer.RenderEndTag(); //A

                        //linked-in
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-standard");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "Print");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.print();return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                        writer.AddAttribute(HtmlTextWriterAttribute.Title, "Printer-friendly view");
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#x2399;");
                        writer.RenderEndTag(); //A

                        this.ltSocial.Text = sWriter.ToString();
                    }
                    else
                        this.pnlShare.Visible = false;
                }
                catch
                {
                    this.pnlShare.Visible = true;
                }
            }

            bool suppressSecurityChecks = manager.Provider.SuppressSecurityChecks;
            manager.Provider.SuppressSecurityChecks = true;
            var provider = (SiteMapBase)SiteMap.Provider;

            var rootNode = SiteMap.RootNode;

            if (this.ShowAlternateView)
            {
                var subsite = SubsiteLogic.GetSubsite(SiteMap.CurrentNode);
                if (subsite != null)
                {
                    rootNode = provider.FindSiteMapNode(subsite.UnitShortName);
                }
            }

            while (currentNode != null && !siteMapNodeEqualityComparer.Equals(currentNode, rootNode))
            {
                var pageSiteNode = currentNode as PageSiteNode;
                if (pageSiteNode != null && this.IsNodeAccessible(pageSiteNode))
                {
                    siteMapNodes.Add(pageSiteNode);
                }
                if (pageSiteNode == null)
                    continue;
                var parent = manager.GetPageNode(pageSiteNode.Id).Parent;
                currentNode = parent == null ? null : provider.GetSiteMapNodeForPage(parent);
            }

            if (this.UseHomePageAsRootNode)
            {
                Guid homePageId = SystemManager.CurrentContext.CurrentSite.HomePageId;
                if (homePageId != Guid.Empty)
                {
                    SiteMapNode siteMapNode = SiteMap.Provider.FindSiteMapNodeFromKey(homePageId.ToString());
                    if (siteMapNode != null && this.IsNodeAccessible(siteMapNode as PageSiteNode) &&
                        !siteMapNodes.Contains(siteMapNode, siteMapNodeEqualityComparer))
                    {
                        siteMapNodes.Add(siteMapNode);
                    }
                }
            }
            if (this.ShowAlternateView)
            {
                siteMapNodes.Add(rootNode);
            }

            siteMapNodes.Reverse();

            // -- Remove current node from collection
            if (siteMapNodes.Count() > 1)
                siteMapNodes.RemoveAt(siteMapNodes.Count() - 1);

            if (this.ShowAlternateView)
            {
                var firstNode = siteMapNodes.FirstOrDefault() as PageSiteNode;
                var pageAltTitleObject = GetNavTitle(firstNode);
                SiteMapNode newNode = new SiteMapNode(firstNode.Provider, firstNode.Key, firstNode.Url,
                 pageAltTitleObject
                    );
                siteMapNodes.RemoveAt(0);
                siteMapNodes.Insert(0, newNode);
            }

            this.BreadcrumbTrailControl.DataSource = siteMapNodes;
            this.BreadcrumbTrailControl.DataBind();
            manager.Provider.SuppressSecurityChecks = suppressSecurityChecks;
        }

        private bool IsNodeAccessible(SiteMapNode siteMapNode)
        {
            if (siteMapNode == null)
            {
                throw new ArgumentNullException("siteMapNode");
            }
            var pageSiteNode = siteMapNode as PageSiteNode;
            if (pageSiteNode == null)
            {
                throw new NotSupportedException("The only supported node type is 'Telerik.Sitefinity.Web.PageSiteNode'.");
            }
            if (!pageSiteNode.IsAccessibleToUser(this.Context))
            {
                return false;
            }
            if (pageSiteNode.NodeType == NodeType.Group)
            {
                if (!this.IncludeGroupPages || !pageSiteNode.ShowInNavigation)
                {
                    return false;
                }
            }
            else if (!pageSiteNode.Visible)
            {
                return false;
            }
            if (pageSiteNode.AvailableLanguages != null && pageSiteNode.AvailableLanguages.Length > 0)
            {
                if (pageSiteNode.NodeType == NodeType.Group)
                {
                    if (!pageSiteNode.AvailableLanguages.Contains(CultureInfo.InvariantCulture) &&
                        !pageSiteNode.AvailableLanguages.Contains(CultureInfo.CurrentUICulture))
                    {
                        return false;
                    }
                }
                else if (!pageSiteNode.AvailableLanguages.Contains(CultureInfo.CurrentUICulture))
                {
                    return false;
                }
            }
            if (string.IsNullOrWhiteSpace(pageSiteNode.Title))
            {
                return false;
            }
            return true;
        }
    }
}