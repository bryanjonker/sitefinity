﻿using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Localization.Data;

namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation
{
    [ObjectInfo("BreadcrumbsResources", ResourceClassId = "BreadcrumbsResources")]
    public class BreadcrumbsResources : Resource
    {
        [ResourceEntry("BreadcrumbsResourcesDescription", Value = "Contains localizable resources for the Breadcrumbs widget.", Description = "The description to display in the 'Interface Labels & Messages' section of the Sitefinity backend.", LastModified = "2011/06/19")]
        public string BreadcrumbsResourcesDescription
        {
            get
            {
                return base["BreadcrumbsResourcesDescription"];
            }
        }

        [ResourceEntry("BreadcrumbsResourcesTitle", Value = "Breadcrumbs Widget", Description = "The title to display in the 'Interface Labels & Messages' section of the Sitefinity backend.", LastModified = "2011/06/19")]
        public string BreadcrumbsResourcesTitle
        {
            get
            {
                return base["BreadcrumbsResourcesTitle"];
            }
        }

        [ResourceEntry("CurrentNodeCssClassPropertyLabel", Value = "Current node CSS class", Description = "Label of the 'CurrentNodeCssClass' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string CurrentNodeCssClassPropertyLabel
        {
            get
            {
                return base["CurrentNodeCssClassPropertyLabel"];
            }
        }

        [ResourceEntry("CurrentToRootPathDirectionTitle", Value = "Current to root", Description = "Title of the 'CurrentToRoot' path direction.", LastModified = "2011/06/19")]
        public string CurrentToRootPathDirectionTitle
        {
            get
            {
                return base["CurrentToRootPathDirectionTitle"];
            }
        }

        [ResourceEntry("DontLimitParentLevelsDisplayedLabel", Value = "Don't limit the number of parent nodes to display.", Description = "Label for the 'DontLimitParentLevelsDisplayed' radio selection shown in the widget designer.", LastModified = "2011/06/19")]
        public string DontLimitParentLevelsDisplayedLabel
        {
            get
            {
                return base["DontLimitParentLevelsDisplayedLabel"];
            }
        }

        [ResourceEntry("EmptyWidgetText", Value = "Edit Breadcrumbs Settings", Description = "Text to show when Breadcrumbs widget hasn't been configured.", LastModified = "2011/06/19")]
        public string EmptyWidgetText
        {
            get
            {
                return base["EmptyWidgetText"];
            }
        }

        [ResourceEntry("IncludeGroupPagesPropertyLabel", Value = "Include group pages", Description = "Label of the 'IncludeGroupPages' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string IncludeGroupPagesPropertyLabel
        {
            get
            {
                return base["IncludeGroupPagesPropertyLabel"];
            }
        }

        [ResourceEntry("LimitParentLevelsDisplayedLabel", Value = "Display only a limited number of parent nodes:", Description = "Label for the 'LimitParentLevelsDisplayed' radio selection shown in the widget designer.", LastModified = "2011/06/19")]
        public string LimitParentLevelsDisplayedLabel
        {
            get
            {
                return base["LimitParentLevelsDisplayedLabel"];
            }
        }

        [ResourceEntry("NodeCssClassPropertyLabel", Value = "Node CSS class", Description = "Label of the 'NodeCssClass' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string NodeCssClassPropertyLabel
        {
            get
            {
                return base["NodeCssClassPropertyLabel"];
            }
        }

        [ResourceEntry("PathDirectionPropertyLabel", Value = "Path direction", Description = "Label of the 'PathDirection' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string PathDirectionPropertyLabel
        {
            get
            {
                return base["PathDirectionPropertyLabel"];
            }
        }

        [ResourceEntry("PathSeparatorCssClassPropertyLabel", Value = "Path separator CSS class", Description = "Label of the 'PathSeparatorCssClass' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string PathSeparatorCssClassPropertyLabel
        {
            get
            {
                return base["PathSeparatorCssClassPropertyLabel"];
            }
        }

        [ResourceEntry("PathSeparatorPropertyLabel", Value = "Path separator", Description = "Label of the 'PathSeparator' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string PathSeparatorPropertyLabel
        {
            get
            {
                return base["PathSeparatorPropertyLabel"];
            }
        }

        [ResourceEntry("PropertyEditorTitle", Value = "Breadcrumbs Settings", Description = "Title for the Breadcrumbs designer.", LastModified = "2011/06/19")]
        public string PropertyEditorTitle
        {
            get
            {
                return base["PropertyEditorTitle"];
            }
        }

        [ResourceEntry("RenderCurrentNodeAsLinkPropertyLabel", Value = "Render current node as a hyperlink", Description = "Label of the 'RenderCurrentNodeAsLink' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string RenderCurrentNodeAsLinkPropertyLabel
        {
            get
            {
                return base["RenderCurrentNodeAsLinkPropertyLabel"];
            }
        }

        [ResourceEntry("RootNodeCssClassPropertyLabel", Value = "Root node CSS class", Description = "Label of the 'RootNodeCssClass' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string RootNodeCssClassPropertyLabel
        {
            get
            {
                return base["RootNodeCssClassPropertyLabel"];
            }
        }

        [ResourceEntry("RootToCurrentPathDirectionTitle", Value = "Root to current", Description = "Title of the 'RootToCurrent' path direction.", LastModified = "2011/06/19")]
        public string RootToCurrentPathDirectionTitle
        {
            get
            {
                return base["RootToCurrentPathDirectionTitle"];
            }
        }

        [ResourceEntry("ShowToolTipsPropertyLabel", Value = "Show node tooltips", Description = "Label of the 'ShowToolTips' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string ShowToolTipsPropertyLabel
        {
            get
            {
                return base["ShowToolTipsPropertyLabel"];
            }
        }

        [ResourceEntry("UseHomePageAsRootNodePropertyLabel", Value = "Home page as root node", Description = "Label of the 'UseHomePageAsRootNode' property shown in the widget designer.", LastModified = "2011/06/19")]
        public string UseHomePageAsRootNodePropertyLabel
        {
            get
            {
                return base["UseHomePageAsRootNodePropertyLabel"];
            }
        }

        public BreadcrumbsResources()
        {
        }

        public BreadcrumbsResources(ResourceDataProvider dataProvider)
            : base(dataProvider)
        {
        }
    }
}
