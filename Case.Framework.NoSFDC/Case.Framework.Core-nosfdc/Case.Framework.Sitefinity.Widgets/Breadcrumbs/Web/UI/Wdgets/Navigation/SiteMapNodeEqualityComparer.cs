﻿using System;
using System.Collections.Generic;
using System.Web;


namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation
{
    internal class SiteMapNodeEqualityComparer : IEqualityComparer<SiteMapNode>
    {
        public bool Equals(SiteMapNode x, SiteMapNode y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null)
            {
                return false;
            }
            if (y == null)
            {
                return false;
            }
            return x.Key == y.Key && string.Equals(x.Url, y.Url, StringComparison.OrdinalIgnoreCase);
        }

        public int GetHashCode(SiteMapNode obj)
        {
            return obj.GetHashCode();
        }
    }
}
