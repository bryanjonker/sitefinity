﻿<%@ Control Language="C#" %>

<div class="sfContentViews sfSingleContentView">
    <br />

    <ul class="sfRadioList">
        <li>
            <asp:RadioButton runat="server"
                ID="DontLimitParentLevelsDisplayedRadio"           
                Text='<%$ Resources:BreadcrumbsResources,DontLimitParentLevelsDisplayedLabel %>'
                GroupName="ParentLevelsToDisplay" />
        </li>
        <li style="margin-top: 8px;">
            <asp:RadioButton runat="server"
                ID="LimitParentLevelsDisplayedRadio"              
                Text='<%$ Resources:BreadcrumbsResources,LimitParentLevelsDisplayedLabel %>'
                GroupName="ParentLevelsToDisplay" />
            
            <telerik:RadNumericTextBox runat="server" 
                ID="ParentLevelsDisplayedTextBox" 
                DataType="System.Int32"
                MinValue="1" 
                CssClass="sfTxt"
                Width="30px"
                Skin="Sitefinity">
                <NumberFormat DecimalDigits="0" GroupSeparator="" />
            </telerik:RadNumericTextBox>
        </li>
    </ul>

    <br />

    <ul>
        <li>
            <asp:Label runat="server"
                AssociatedControlID="PathSeparatorTextBox"
                Text='<%$ Resources:BreadcrumbsResources,PathSeparatorPropertyLabel %>' 
                CssClass="sfTxtLbl" />
        </li>
        <li>
            <telerik:RadTextBox runat="server" 
                ID="PathSeparatorTextBox" 
                CssClass="sfTxt"
                Width="200px" 
                Skin="Sitefinity" />
        </li>

        <li>
            <asp:Label runat="server"
                AssociatedControlID="PathDirectionComboBox"
                Text='<%$ Resources:BreadcrumbsResources,PathDirectionPropertyLabel %>' 
                CssClass="sfTxtLbl" />
        </li>

        <li>
            <telerik:RadComboBox runat="server" 
                ID="PathDirectionComboBox" 
                AllowCustomText="false" 
                DataValueField="Value"
                DataTextField="Name"
                Skin="Sitefinity"
                Width="200px" >
            </telerik:RadComboBox>
        </li>
    </ul>

    <br />   

    <ul class="sfRadioList">
        <li>
            <asp:CheckBox runat="server"
                ID="UseHomePageAsRootNodeCheckBox"
                Text='<%$ Resources:BreadcrumbsResources, UseHomePageAsRootNodePropertyLabel %>' />
        </li>
        <li>
            <asp:CheckBox runat="server"
                ID="IncludeGroupPagesCheckBox"
                Text='<%$ Resources:BreadcrumbsResources, IncludeGroupPagesPropertyLabel %>' />
        </li>
        <li>
            <asp:CheckBox runat="server"
                ID="RenderCurrentNodeAsLinkCheckBox"
                Text='<%$ Resources:BreadcrumbsResources, RenderCurrentNodeAsLinkPropertyLabel %>' />
        </li>
        <li>        
            <asp:CheckBox runat="server"
                ID="ShowToolTipsCheckBox"
                Text='<%$ Resources:BreadcrumbsResources, ShowToolTipsPropertyLabel %>' />
        </li>
           <li>        
            <asp:CheckBox runat="server"
                ID="ShowAlternateViewCheckBox"
                Text='Show alternate view' />
        </li>        
    </ul>
    
    <div id="groupDesignSettings" class="sfExpandableSection">
        <h3><a href="javascript:void(0);" class="sfMoreDetails" id="expanderDesignSettings"><asp:Literal runat="server" Text="<%$Resources:Labels,DesignSettings %>" /></a></h3>
        <ul class="sfTargetList">
            <li>
                <asp:Label runat="server"
                    AssociatedControlID="NodeCssClassTextBox"
                    Text='<%$ Resources:BreadcrumbsResources,NodeCssClassPropertyLabel %>' 
                    CssClass="sfTxtLbl"
                    Font-Bold="false" />
                

                <telerik:RadTextBox runat="server" 
                    ID="NodeCssClassTextBox" 
                    CssClass="sfTxt"
                    Width="200px" 
                    Skin="Sitefinity" />
            </li>
            <li>
                <asp:Label runat="server"
                    AssociatedControlID="CurrentNodeCssClassTextBox"
                    Text='<%$ Resources:BreadcrumbsResources,CurrentNodeCssClassPropertyLabel %>' 
                    CssClass="sfTxtLbl"
                    Font-Bold="false" />

                <telerik:RadTextBox runat="server" 
                    ID="CurrentNodeCssClassTextBox" 
                    CssClass="sfTxt"
                    Width="200px" 
                    Skin="Sitefinity" />
            </li>
            <li>
                <asp:Label runat="server"
                    AssociatedControlID="RootNodeCssClassTextBox"
                    Text='<%$ Resources:BreadcrumbsResources,RootNodeCssClassPropertyLabel %>' 
                    CssClass="sfTxtLbl"
                    Font-Bold="false" />

                <telerik:RadTextBox runat="server" 
                    ID="RootNodeCssClassTextBox" 
                    CssClass="sfTxt"
                    Width="200px" 
                    Skin="Sitefinity" />
            </li>
            <li>
                <asp:Label runat="server"
                    AssociatedControlID="PathSeparatorCssClassTextBox"
                    Text='<%$ Resources:BreadcrumbsResources,PathSeparatorCssClassPropertyLabel %>' 
                    CssClass="sfTxtLbl"
                    Font-Bold="false" />

                <telerik:RadTextBox runat="server" 
                    ID="PathSeparatorCssClassTextBox" 
                    CssClass="sfTxt"
                    Width="200px" 
                    Skin="Sitefinity" />
            </li>
            <li>
                <asp:Label runat="server"
                    AssociatedControlID="CustomTemplatePathTextBox"
                    Text='<%$ Resources:Labels,CustomTemplatePath %>' 
                    CssClass="sfTxtLbl"
                    Font-Bold="false" />

                <telerik:RadTextBox runat="server" 
                    ID="CustomTemplatePathTextBox" 
                    CssClass="sfTxt"
                    Width="200px" 
                    Skin="Sitefinity" />                
                
                <p class="sfExample"><asp:Literal runat="server" Text="<%$Resources:Labels,CustomTemplatePathExample %>" /></p>
            </li>
        </ul>
    </div>  
</div>