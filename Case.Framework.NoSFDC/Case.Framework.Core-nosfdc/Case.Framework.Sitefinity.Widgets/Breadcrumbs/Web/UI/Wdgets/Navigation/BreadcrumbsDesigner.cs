﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Web.UI;

namespace Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation
{
    public class BreadcrumbsDesigner : ControlDesignerBase
    {
        protected virtual RadTextBox CurrentNodeCssClassTextBox
        {
            get { return Container.GetControl<RadTextBox>("CurrentNodeCssClassTextBox", true); }
        }

        protected virtual RadTextBox CustomTemplatePathTextBox
        {
            get { return Container.GetControl<RadTextBox>("CustomTemplatePathTextBox", true); }
        }

        protected virtual RadioButton DontLimitParentLevelsDisplayedRadio
        {
            get { return Container.GetControl<RadioButton>("DontLimitParentLevelsDisplayedRadio", true); }
        }

        protected virtual CheckBox IncludeGroupPagesCheckBox
        {
            get { return Container.GetControl<CheckBox>("IncludeGroupPagesCheckBox", true); }
        }

        protected virtual CheckBox ShowAlternateViewCheckBox
        {
            get { return Container.GetControl<CheckBox>("ShowAlternateViewCheckBox", true); }
        }

        protected override string LayoutTemplateName
        {
            get
            {
                return
                    "Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.Resources.Views.BreadcrumbsDesigner.ascx";
            }
        }


        protected virtual RadioButton LimitParentLevelsDisplayedRadio
        {
            get { return Container.GetControl<RadioButton>("LimitParentLevelsDisplayedRadio", true); }
        }

        protected virtual RadTextBox NodeCssClassTextBox
        {
            get { return Container.GetControl<RadTextBox>("NodeCssClassTextBox", true); }
        }

        protected virtual RadNumericTextBox ParentLevelsDisplayedTextBox
        {
            get { return Container.GetControl<RadNumericTextBox>("ParentLevelsDisplayedTextBox", true); }
        }

        protected virtual RadComboBox PathDirectionComboBox
        {
            get { return Container.GetControl<RadComboBox>("PathDirectionComboBox", true); }
        }

        protected virtual RadTextBox PathSeparatorCssClassTextBox
        {
            get { return Container.GetControl<RadTextBox>("PathSeparatorCssClassTextBox", true); }
        }

        protected virtual RadTextBox PathSeparatorTextBox
        {
            get { return Container.GetControl<RadTextBox>("PathSeparatorTextBox", true); }
        }

        protected virtual CheckBox RenderCurrentNodeAsLinkCheckBox
        {
            get { return Container.GetControl<CheckBox>("RenderCurrentNodeAsLinkCheckBox", true); }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get { return typeof (BreadcrumbsResources); }
        }

        protected virtual RadTextBox RootNodeCssClassTextBox
        {
            get { return Container.GetControl<RadTextBox>("RootNodeCssClassTextBox", true); }
        }

        protected virtual CheckBox ShowToolTipsCheckBox
        {
            get { return Container.GetControl<CheckBox>("ShowToolTipsCheckBox", true); }
        }

        protected virtual CheckBox UseHomePageAsRootNodeCheckBox
        {
            get { return Container.GetControl<CheckBox>("UseHomePageAsRootNodeCheckBox", true); }
        }

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            IList<ScriptDescriptor> scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var scriptControlDescriptor = (ScriptControlDescriptor) scriptDescriptors.Last();
            scriptControlDescriptor.AddElementProperty("renderCurrentNodeAsLinkCheckBox",
                RenderCurrentNodeAsLinkCheckBox.ClientID);
            scriptControlDescriptor.AddElementProperty("includeGroupPagesCheckBox", IncludeGroupPagesCheckBox.ClientID);
            scriptControlDescriptor.AddElementProperty("useHomePageAsRootNodeCheckBox",
                UseHomePageAsRootNodeCheckBox.ClientID);
            scriptControlDescriptor.AddElementProperty("showToolTipsCheckBox", ShowToolTipsCheckBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("parentLevelsDisplayedTextBox",
                ParentLevelsDisplayedTextBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("pathSeparatorTextBox", PathSeparatorTextBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("pathDirectionComboBox", PathDirectionComboBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("nodeCssClassTextBox", NodeCssClassTextBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("currentNodeCssClassTextBox",
                CurrentNodeCssClassTextBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("rootNodeCssClassTextBox", RootNodeCssClassTextBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("pathSeparatorCssClassTextBox",
                PathSeparatorCssClassTextBox.ClientID);
            scriptControlDescriptor.AddComponentProperty("customTemplatePathTextBox", CustomTemplatePathTextBox.ClientID);
            scriptControlDescriptor.AddElementProperty("dontLimitParentLevelsDisplayedRadio",
                DontLimitParentLevelsDisplayedRadio.ClientID);
            scriptControlDescriptor.AddElementProperty("limitParentLevelsDisplayedRadio",
                LimitParentLevelsDisplayedRadio.ClientID);
            scriptControlDescriptor.AddElementProperty("showAlternateViewCheckBox", ShowAlternateViewCheckBox.ClientID);

            return scriptDescriptors;
        }

        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            IList<ScriptReference> scriptReferences = new List<ScriptReference>(base.GetScriptReferences())
            {
                new ScriptReference(
                    "Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation.Resources.Scripts.BreadcrumbsDesigner.js",
                    typeof (BreadcrumbsDesigner).Assembly.FullName)
            };
            return scriptReferences.ToArray();
        }

        protected override void InitializeControls(GenericContainer container)
        {
            DesignerMode = ControlDesignerModes.Simple;
            InitializePathDirectionComboBox();
        }

        private void InitializePathDirectionComboBox()
        {
            var list = (
                from PathDirection v in Enum.GetValues(typeof (PathDirection))
                select
                    new
                    {
                        Name = Res.Get<BreadcrumbsResources>(string.Concat(v.ToString(), "PathDirectionTitle")),
                        Value = v
                    }).ToList();
            PathDirectionComboBox.DataSource = list;
            PathDirectionComboBox.DataBind();
        }
    }
}
