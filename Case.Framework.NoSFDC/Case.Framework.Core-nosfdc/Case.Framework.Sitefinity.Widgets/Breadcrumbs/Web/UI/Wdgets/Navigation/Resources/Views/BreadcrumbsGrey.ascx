﻿<%@ Register Assembly="Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.WebControls.BreadcrumbTrail" Namespace="Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.WebControls" TagPrefix="case" %>
<%@ Control Language="C#"%>

    <!-- BREADCRUMBS -->
    <div class="bg gray">
        <div class="row">
            <nav role="navigation" aria-label="Breadcrumbs">
        	<ul class="breadcrumbs">
               <span class="breadcrumbs_icon"></span>
        	   <case:BreadcrumbTrail ID="BreadcrumbTrail" runat="server" />
        	</ul>
            </nav>
        </div>
    </div>

    <!-- PAGE HEADER -->
    <header class="page-header bg">
	    <div class="row">
        	<div class="column three-quarter first">
	             <asp:Literal runat="server" ID="ltpageTitle"></asp:Literal>
	        </div>
            <asp:Panel runat="server" ID="pnlShare" Visible="true">
        	<!-- SHARE BOX -->
        	<div class="column one-quarter last">
        		<div id="share"><span>Share</span> 
    				 <asp:Literal runat="server" ID="ltSocial"></asp:Literal>
    			</div>
        	</div>
            </asp:Panel>
    	</div>
    </header>
