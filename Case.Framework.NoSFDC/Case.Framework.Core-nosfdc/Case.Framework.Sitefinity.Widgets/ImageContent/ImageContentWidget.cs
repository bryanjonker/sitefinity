﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.ImageContent
{
    [RequireScriptManager]
    [ControlDesigner(typeof(ImageContentWidgetDesigner))]
    public class ImageContentWidget : SimpleView
    {
        private string data = string.Empty;
        private string viewMode = string.Empty;
        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string HeaderText
        {
            get;
            set;
        }

        protected virtual Literal ltHeader
        {
            get
            {
                return this.Container.GetControl<Literal>("ltHeader", true);
            }
        }

        protected virtual Literal ltImageContent
        {
            get
            {
                return this.Container.GetControl<Literal>("ltImageContent", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                List<Section> sections = new JavaScriptSerializer().Deserialize<List<Section>>(Data);
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                foreach (var section in sections.OrderBy(x => x.OrderBy))
                {
                    var url = GetUrl(section);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "row");
                    writer.RenderBeginTag("article");

                    //if (!string.IsNullOrWhiteSpace(url))
                    //{
                    //    writer.AddAttribute(HtmlTextWriterAttribute.Class, "text-dark_gray");
                    //    writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + section.Name);
                    //    writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
                    //    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    //}
                    //else
                    //{
                    //    writer.AddAttribute(HtmlTextWriterAttribute.Class, "text-dark_gray");
                    //    writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + section.Name);
                    //    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                    //    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    //}

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-third first");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, section.Title);

                    System.Uri sUrl = new System.Uri(section.Image);
                   
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, sUrl.AbsolutePath);
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag(); // img
                    writer.RenderEndTag(); // div
                    
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "column two-third last");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "image-title");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    if (!string.IsNullOrWhiteSpace(url))
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + section.Name);
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write(section.Title + "&nbsp;&rsaquo;");
                        writer.RenderEndTag(); //A
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + section.Name);
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write(section.Title + "&nbsp;&rsaquo;");
                        writer.RenderEndTag(); //A
                    }
                    writer.RenderEndTag(); // DIV
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(section.Content);
                    writer.RenderEndTag(); // P
                    writer.RenderEndTag(); // div
                 //   writer.RenderEndTag(); // A
                    writer.RenderEndTag(); //article
                }
                ltImageContent.Text = sWriter.ToString();
                ltHeader.Text = HeaderText;
            }
            catch { }
        }


        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.ImageContent.Resources.Views.ImageContentWidget.ascx";
            }
        }

        private static string GetUrl(Section section)
        {
            if (section.URLGuid != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(section.URLGuid);
                if (page != null)
                {
                    return page.Url;
                }
            }
            return section.URL;
        }

        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }

        public class Section
        {
            public string Title;
            public string Name;
            public string Index;
            public string URL;
            public string Image;
            public string Content;
            public Guid URLGuid;
            public string OrderBy;
        }
    }
}
