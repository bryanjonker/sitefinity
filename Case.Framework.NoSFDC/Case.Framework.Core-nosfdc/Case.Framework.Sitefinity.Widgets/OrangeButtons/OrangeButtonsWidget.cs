﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.OrangeButtons
{
    [RequireScriptManager]
    [ControlDesigner(typeof(OrangeButtonsWidgetDesigner))]
    public class OrangeButtonsWidget : SimpleView
    {
        private string data = string.Empty;
        private string viewMode = string.Empty;
        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string HeaderText
        {
            get;
            set;
        }

        protected virtual Literal ltHeader
        {
            get
            {
                return this.Container.GetControl<Literal>("ltHeader", true);
            }
        }

        protected virtual Literal ltOrangeButtons
        {
            get
            {
                return this.Container.GetControl<Literal>("ltOrangeButtons", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                List<Section> sections = new JavaScriptSerializer().Deserialize<List<Section>>(Data);
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);
                for (int x = 0; x < sections.Count; x++)
                {

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + sections[x].Name);

                    writer.AddAttribute(HtmlTextWriterAttribute.Href, GetUrl(sections[x]));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, sections[x].Name);
                    System.Uri sUrl = new System.Uri(sections[x].Image);
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, sUrl.AbsolutePath);
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag();
                    writer.Write(sections[x].Name.Replace("_", " ") + "&nbsp;&rsaquo;");
                    writer.RenderEndTag(); //A
                }
                ltOrangeButtons.Text = sWriter.ToString();
                ltHeader.Text = HeaderText;
            }
            catch { }
        }

                protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.OrangeButtons.Resources.Views.OrangeButtonsWidget.ascx";
            }
        }

        private static string GetUrl(Section section)
        {
            if (section.URLGuid != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(section.URLGuid);
                if (page != null)
                {
                    return page.Url;
                }
            }
            return section.URL;
        }

        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }

        public class Section
        {
            public string Title;
            public string Name;
            public string Index;
            public string URL;
            public string Image;
            public Guid URLGuid;
        }
    }
}
