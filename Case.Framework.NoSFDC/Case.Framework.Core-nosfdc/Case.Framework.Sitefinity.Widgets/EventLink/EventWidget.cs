﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Modules.Events;
using Telerik.Sitefinity.Events.Model;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using System.Text;
using System.Globalization;
using System.Collections.Generic;

namespace Case.Framework.Sitefinity.Widgets.Events
{

    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Events.Template.EventWidgetDesigner))]
    public class EventWidget : SimpleView
    {
        #region Properties
        public bool HomeWidget { get; set; }
        public bool MicrositeHomeWidget { get; set; }
        public string Title { get; set; }
        public string UrlEventDetails { get; set; }
        public string UrlEventsList { get; set; }
        public bool ShowLocation { get; set; }
        public bool ShowDescription { get; set; }

        private int count;
        private int totalEvents;

        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return EventWidget.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }
        #endregion

        #region Control References

        protected virtual Panel pnEventsHome
        {
            get
            {
                return this.Container.GetControl<Panel>("pnEventsHome", true);
            }
        }

        protected virtual Panel pnEventsNonHome
        {
            get
            {
                return this.Container.GetControl<Panel>("pnEventsNonHome", true);
            }
        }

        protected virtual Literal litTitle
        {
            get
            {
                return this.Container.GetControl<Literal>("litTitle", true);
            }
        }

        protected virtual Literal litTitleHome
        {
            get
            {
                return this.Container.GetControl<Literal>("litTitleHome", true);
            }
        }

        protected virtual Repeater rptEventsNonHome
        {
            get
            {
                return this.Container.GetControl<Repeater>("rptEventsNonHome", true);
            }
        }

        protected virtual Repeater rptHome
        {
            get
            {
                return this.Container.GetControl<Repeater>("rptHome", true);
            }
        }

        protected virtual HyperLink hlMoreEvents
        {
            get
            {
                return this.Container.GetControl<HyperLink>("hlMoreEvents", true);
            }
        }

        protected virtual HyperLink hlViewAll
        {
            get
            {
                return this.Container.GetControl<HyperLink>("hlViewAll", true);
            }
        }


        #endregion

        #region Methods
        /// <summary>
        /// Initializes the controls.
        /// </summary>
        /// <param name="container"></param>
        /// <remarks>
        /// Initialize your controls in this method. Do not override CreateChildControls method.
        /// </remarks>
        protected override void InitializeControls(GenericContainer container)
        {           

            count = 1;
            pnEventsHome.Visible = HomeWidget || MicrositeHomeWidget;
            pnEventsNonHome.Visible = !HomeWidget;
            hlMoreEvents.NavigateUrl = UrlEventsList;
            hlViewAll.NavigateUrl = UrlEventsList;

            var manager = new Telerik.Sitefinity.Modules.Events.EventsManager();
         

            if (HomeWidget || MicrositeHomeWidget)
            {
                InitializeHomeWidgets(manager);
            }
            else
            {
                InitializeNonHome(manager);
            }
        }

     
        #endregion

        #region Events

        protected void rptEventsHome_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var litTitle = (Literal)e.Item.FindControl("litTitle");
            var litEventDate = (Literal)e.Item.FindControl("litEventDate");
            var litEventTime = (Literal)e.Item.FindControl("litEventTime");
            //var litEventEndDate = (Literal)e.Item.FindControl("litEventEndDate");
            var litLocation = (Literal)e.Item.FindControl("litLocation");
            var lblContent = (Label)e.Item.FindControl("lblContent");
            var litLink = (Literal)e.Item.FindControl("litLink");
            var litOpenDiv = (Literal)e.Item.FindControl("litOpenDiv");
            var litCloseDiv = (Literal)e.Item.FindControl("litCloseDiv");
            var hyEventDetail = (HyperLink)e.Item.FindControl("hyEventDetail");
            
            if (e.Item.DataItem != null)
            {
                if (count % 2 != 0)
                {
                    litOpenDiv.Text = "<div>";
                    if (count == totalEvents)
                    {
                        litCloseDiv.Text = "</div>";
                    }
                }
                else
                {
                    litCloseDiv.Text = "</div>";
                }

                count++;

                var EventItem = (Event)e.Item.DataItem;
                hyEventDetail.NavigateUrl = string.Concat(UrlEventDetails, EventItem.UrlName);
                //litLink.Text = string.Concat("<a href='", UrlEventDetails, EventItem.UrlName, "'>");
                litTitle.Text = EventItem.Title;

                litEventDate.Text = EventItem.EventStart.ToString("m",
                  CultureInfo.CreateSpecificCulture("en-us"));

                if (EventItem.EventEnd.HasValue && EventItem.EventEnd.Value.Date > EventItem.EventStart.Date && EventItem.EventEnd.Value.Date>DateTime.Now.Date)
                {
                    litEventDate.Text = DateTime.Now.Date.ToString("m",
                  CultureInfo.CreateSpecificCulture("en-us"));
                }
                else
                {
                    litEventDate.Text = EventItem.EventStart.ToString("m",
                  CultureInfo.CreateSpecificCulture("en-us"));

                }

                var stBuilder = new StringBuilder();

                if (EventItem.AllDayEvent)
                {
                    litEventTime.Text = "ALL DAY";
                }
                else
                {
                    if (EventItem.GetValue("TimeType").ToString().ToUpper() != "NONE")
                    {
                        litEventTime.Visible = true;
                        stBuilder.Append(", <time datetime=''>");
                        stBuilder.Append(EventItem.GetValue("StartTime").ToString());

                        if (EventItem.GetValue("TimeType").ToString().ToUpper() == "START_AND_END_TIME")
                        {
                            stBuilder.Append(" - ");
                            stBuilder.Append(EventItem.GetValue("EndTime").ToString());
                        }

                        stBuilder.Append("</time>");

                        litEventTime.Text = stBuilder.ToString();
                    }
                    else
                    {
                        litEventTime.Visible = false;
                    }
                }

                litLocation.Visible = ShowLocation;

                if (ShowLocation && !string.IsNullOrWhiteSpace(EventItem.Location))
                {
                    litLocation.Text = string.Concat(EventItem.Location, " <br />");
                }
                else
                {
                    litLocation.Visible = false;
                }

                lblContent.Visible = ShowDescription;

                if (ShowDescription && !string.IsNullOrWhiteSpace(EventItem.Content))
                {
                    lblContent.Text = EventItem.Content.ToString().Length > 140 ? string.Concat(EventItem.Content.ToString().Substring(0, 140), "...") : EventItem.Content.ToString();
                }
                else
                {
                    lblContent.Visible = false;
                }

            }
        }

        protected void rptEventsNonHome_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var litTitle = (Literal)e.Item.FindControl("litTitle");
            var litEventDate = (Literal)e.Item.FindControl("litEventDate");
            var litEventTime = (Literal)e.Item.FindControl("litEventTime");
          //  var litEventEndDate = (Literal)e.Item.FindControl("litEventEndDate");
            var litLocation = (Literal)e.Item.FindControl("litLocation");
            var lblContent = (Label)e.Item.FindControl("lblContent");
            var litLink = (Literal)e.Item.FindControl("litLink");

            if (e.Item.DataItem != null)
            {               
                var EventItem = (Event)e.Item.DataItem;

                litLink.Text = string.Concat("<a href='", UrlEventDetails, EventItem.UrlName, "'>");

                litTitle.Text = EventItem.Title;
               
                if (EventItem.EventEnd.HasValue && EventItem.EventEnd.Value.Date > EventItem.EventStart.Date && EventItem.EventEnd.Value.Date>DateTime.Now.Date)
                {                    
                    litEventDate.Text = string.Concat(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(DateTime.Now.Month), " <span>", DateTime.Now.Day, "</span>");
                }
                else
                {
                    litEventDate.Text = string.Concat(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(EventItem.EventStart.Month), " <span>", EventItem.EventStart.Day, "</span>");
                }

                var stBuilder = new StringBuilder();

                if (EventItem.AllDayEvent)
                {
                    litEventTime.Text = "ALL DAY";
                }
                else
                {
                    if (EventItem.GetValue("TimeType").ToString().ToUpper() != "NONE")
                    {
                        litEventTime.Visible = true;
                        stBuilder.Append("<time datetime=''>");
                        stBuilder.Append(EventItem.GetValue("StartTime").ToString());

                        if (EventItem.GetValue("TimeType").ToString().ToUpper() == "START_AND_END_TIME")
                        {
                            stBuilder.Append(" - ");
                            stBuilder.Append(EventItem.GetValue("EndTime").ToString());
                        }

                        stBuilder.Append("</time>");

                        litEventTime.Text = stBuilder.ToString();
                    }
                    else
                    {
                        litEventTime.Visible = false;
                    }
                }
                
                litLocation.Visible = ShowLocation;

                if (ShowLocation && !string.IsNullOrWhiteSpace(EventItem.Location))
                {
                    litLocation.Text = string.Concat(EventItem.Location, " <br />");
                }
                else
                {
                    litLocation.Visible = false;
                }

                lblContent.Visible = ShowDescription;
                
                if (ShowDescription && !string.IsNullOrWhiteSpace(EventItem.Content))
                {
                    lblContent.Text = EventItem.Content.ToString().Length > 140 ? string.Concat(EventItem.Content.ToString().Substring(0, 140), "...") : EventItem.Content.ToString();
                }
                else
                {
                    lblContent.Visible = false;
                }
            }
        }
        #endregion

        #region Private members & constants
        private void InitializeHomeWidgets(EventsManager manager)
        {
            litTitleHome.Text = string.IsNullOrWhiteSpace(Title) ? "Events" : Title;
            IEnumerable<Event> events = null;
            if (HomeWidget)
            {

                events = manager.GetEvents()
                    .Where(x => (x.EventStart.Date >= DateTime.Now.Date || x.EventEnd >= DateTime.Now) && x.GetValue<bool>("HomeWidget") 
                        && x.Visible && x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live)
                    .OrderBy(x => x.EventStart);
            }
            else if (MicrositeHomeWidget)
            {

                events = manager.GetEvents()
                    .Where(x => (x.EventStart.Date >= DateTime.Now.Date || x.EventEnd >= DateTime.Now) && x.GetValue<bool>("MicrositeHomeWidget") 
                        && x.Visible && x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live)
                    .OrderBy(x => x.EventStart);
            }

            rptHome.ItemDataBound += new RepeaterItemEventHandler(rptEventsHome_ItemDataBound);
            totalEvents = events.Count();

            rptHome.DataSource = events;
            rptHome.DataBind();
        }

        private void InitializeNonHome(EventsManager manager)
        {
            litTitle.Text = string.IsNullOrWhiteSpace(Title) ? "Upcoming Events" : Title;

            var events = manager.GetEvents().Where(x => x.EventStart.Date >= DateTime.Now.Date || x.EventEnd >= DateTime.Now 
                && x.Visible && x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live)
                .OrderBy(x => Guid.NewGuid()) //get random
                .Take(3)
                .OrderBy(x => x.EventStart); ;

            rptEventsNonHome.ItemDataBound += new RepeaterItemEventHandler(rptEventsNonHome_ItemDataBound);

            rptEventsNonHome.DataSource = events;
            rptEventsNonHome.DataBind();
        }

        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Events.Template.EventWidget.ascx";
        #endregion
    }
}
