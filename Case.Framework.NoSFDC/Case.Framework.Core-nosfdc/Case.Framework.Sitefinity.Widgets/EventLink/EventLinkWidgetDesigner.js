Type.registerNamespace("Case.Framework.Sitefinity.Widgets.EventLink");

Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner = function (element) {
    /* Initialize Title fields */
    this._title = null;
    
    /* Initialize PageId fields */
    this._pageSelectorPageId = null;
    this._selectorTagPageId = null;
    this._PageIdDialog = null;
 
    this._showPageSelectorPageIdDelegate = null;
    this._pageSelectedPageIdDelegate = null;
    
    /* Initialize Description fields */
    this._description = null;
    
    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner.callBaseMethod(this, 'initialize');

        /* Initialize PageId */
        this._showPageSelectorPageIdDelegate = Function.createDelegate(this, this._showPageSelectorPageIdHandler);
        $addHandler(this.get_pageSelectButtonPageId(), "click", this._showPageSelectorPageIdDelegate);

        this._pageSelectedPageIdDelegate = Function.createDelegate(this, this._pageSelectedPageIdHandler);
        this.get_pageSelectorPageId().add_doneClientSelection(this._pageSelectedPageIdDelegate);

        if (this._selectorTagPageId) {
            this._PageIdDialog = jQuery(this._selectorTagPageId).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner.callBaseMethod(this, 'dispose');

        /* Dispose PageId */
        if (this._showPageSelectorPageIdDelegate) {
            $removeHandler(this.get_pageSelectButtonPageId(), "click", this._showPageSelectorPageIdDelegate);
            delete this._showPageSelectorPageIdDelegate;
        }

        if (this._pageSelectedPageIdDelegate) {
            this.get_pageSelectorPageId().remove_doneClientSelection(this._pageSelectedPageIdDelegate);
            delete this._pageSelectedPageIdDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI Title */
        jQuery(this.get_title()).val(controlData.Title);

        /* RefreshUI PageId */
        if (controlData.PageId && controlData.PageId !== "00000000-0000-0000-0000-000000000000") {
            var pagesSelectorPageId = this.get_pageSelectorPageId().get_pageSelector();
            var selectedPageLabelPageId = this.get_selectedPageIdLabel();
            var selectedPageButtonPageId = this.get_pageSelectButtonPageId();
            pagesSelectorPageId.add_selectionApplied(function (o, args) {
                var selectedPage = pagesSelectorPageId.get_selectedItem();
                if (selectedPage) {
                    selectedPageLabelPageId.innerHTML = selectedPage.Title.Value;
                    jQuery(selectedPageLabelPageId).show();
                    selectedPageButtonPageId.innerHTML = '<span>Change</span>';
                }
            });
            pagesSelectorPageId.set_selectedItems([{ Id: controlData.PageId }]);
        }

        if (controlData.ExternalUrl != null) {
            jQuery(this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox()._textBoxElement).val(controlData.ExternalUrl);
            jQuery(this.get_pageSelectorPageId().get_extPagesSelector().get_titleTextBox()).val(controlData.Description);
            this.get_selectedPageIdLabel().innerHTML = controlData.ExternalUrl;
            jQuery(this.get_selectedPageIdLabel()).show();
            this.get_pageSelectButtonPageId().innerHTML = '<span>Change</span>';
            $('.rtsLI.rtsFirst a').parent().click();
        } else {
            $('.rtsLI.rtsLast a').parent().click();
        }

        /* RefreshUI Description */
        jQuery(this.get_description()).val(controlData.Description);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges Title */
        controlData.Title = jQuery(this.get_title()).val();

        /* ApplyChanges PageId */

        /* ApplyChanges Description */
        controlData.Description = jQuery(this.get_description()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* PageId private methods */
    _showPageSelectorPageIdHandler: function (selectedItem) {
        this._PageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._PageIdDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();

        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorPageId().get_pageSelector();
        if (controlData.PageId != "00000000-0000-0000-0000-000000000000") {
            pagesSelector.set_selectedItems([{ Id: controlData.PageId }]);
            $('.rtsLI.rtsFirst a').parent().click();
        } else {
            $('.rtsLI.rtsLast a').parent().click();
        }

    },

    _pageSelectedPageIdHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorPageId().get_pageSelector();


        if (items == null) {
            this._PageIdDialog.dialog("close");
            jQuery("#designerLayoutRoot").show();
            dialogBase.resizeToContent();
            return;
        }
        var selectedPage = pagesSelector.get_selectedItem();

        if (jQuery("#" + this.get_pageSelectorPageId().get_tabstrip()._tabData[1]._implPageViewID + ":visible").length > 0)
            selectedPage = null;

        this._PageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();


        if (selectedPage) {
            this.get_selectedPageIdLabel().innerHTML = selectedPage.Title.Value;
            jQuery(this.get_selectedPageIdLabel()).show();
            this.get_pageSelectButtonPageId().innerHTML = '<span>Change</span>';
            controlData.PageId = selectedPage.Id;
            controlData.ExternalUrl = null;
            $(this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox()).val('')
        }
        else {
            if (this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value() != '') {
                this.get_selectedPageIdLabel().innerHTML = this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value();
                jQuery(this.get_description()).val(jQuery(this.get_pageSelectorPageId().get_extPagesSelector().get_titleTextBox()).val());
                jQuery(this.get_selectedPageIdLabel()).show();
                this.get_pageSelectButtonPageId().innerHTML = '<span>Change</span>';
                controlData.PageId = "00000000-0000-0000-0000-000000000000";
                controlData.ExternalUrl = this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value();

            } else {
                jQuery(this.get_selectedPageIdLabel()).hide();
                this.get_pageSelectButtonPageId().innerHTML = '<span>Select...</span>';
                controlData.PageId = "00000000-0000-0000-0000-000000000000";

            }
        }
    },

    /* --------------------------------- properties -------------------------------------- */

    /* Title properties */
    get_title: function () { return this._title; }, 
    set_title: function (value) { this._title = value; },

    /* PageId properties */
    get_pageSelectButtonPageId: function () {
        if (this._pageSelectButtonPageId == null) {
            this._pageSelectButtonPageId = this.findElement("pageSelectButtonPageId");
        }
        return this._pageSelectButtonPageId;
    },
    get_selectedPageIdLabel: function () {
        if (this._selectedPageIdLabel == null) {
            this._selectedPageIdLabel = this.findElement("selectedPageIdLabel");
        }
        return this._selectedPageIdLabel;
    },
    get_pageSelectorPageId: function () {
        return this._pageSelectorPageId;
    },
    set_pageSelectorPageId: function (val) {
        this._pageSelectorPageId = val;
    },
    get_selectorTagPageId: function () {
        return this._selectorTagPageId;
    },
    set_selectorTagPageId: function (value) {
        this._selectorTagPageId = value;
    },

    /* Description properties */
    get_description: function () { return this._description; }, 
    set_description: function (value) { this._description = value; }
}

Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.EventLink.EventLinkWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
