﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.OrangeLinkButton
{


    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.OrangeLinkButton.OrangeLinkWidgetDesigner))]
    public class OrangeLinkWidget : SimpleView
    {
        public string Title { get; set; }
        public Guid PageId { get; set; }
        public string Description { get; set; }
        public string ExternalUrl { get; set; }

       
        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(OrangeLinkWidget);
            }
        }

        protected virtual Literal ltHeader
        {
            get
            {
                return this.Container.GetControl<Literal>("ltHeader", true);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);

            if (PageId != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(PageId);

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(string.Concat(Description,"&nbsp;&rsaquo;"));
                writer.RenderEndTag();

            }
            else if (!String.IsNullOrEmpty(ExternalUrl))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, ExternalUrl);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(string.Concat(Description, "&nbsp;&rsaquo;"));
                writer.RenderEndTag();
            }
            ltHeader.Text = sWriter.ToString();
        }


        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.EventLink.Resources.Views.EventLinkWidget.ascx"; }
        }
  
    }
}
