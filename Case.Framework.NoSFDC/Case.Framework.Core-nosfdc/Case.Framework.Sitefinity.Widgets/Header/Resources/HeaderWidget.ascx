﻿<%@ Control Language="C#" Inherits="Case.Framework.Sitefinity.Widgets.Header.HeaderWidget.cs" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>

  <div class="row">
    	    <div class="column three-quarter first">
        	    <h1><%=Page.Title %></h1>
        	</div>
      <asp:Panel runat="server" ID="pnlShare" Visible="true">
    		<!-- SHARE BOX -->
    		<div class="column one-quarter bottom last">
    			<div id="share"><span>Share</span> 
					 <asp:Literal runat="server" ID="ltSocial"></asp:Literal>
				</div>
    		</div>
          </asp:Panel>
		</div>