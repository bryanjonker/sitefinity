using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.DeansBlogPostHome
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPost"/> widget
    /// </summary>
    public class DeansBlogLastPostDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return DeansBlogLastPostDesigner.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the Title property
        /// </summary>
        protected virtual Control Title
        {
            get
            {
                return this.Container.GetControl<Control>("Title", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting ImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting ImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the ImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorImageId", false);
            }
        }

        ///// <summary>
        ///// Gets the control that is bound to the BlogUrl property
        ///// </summary>
        //protected virtual Control BlogUrl
        //{
        //    get
        //    {
        //        return this.Container.GetControl<Control>("BlogUrl", true);
        //    }
        //}

        /// <summary>
        /// Gets the control that is bound to the BlogFeedUrl property
        /// </summary>
        protected virtual Control BlogFeedUrl
        {
            get
            {
                return this.Container.GetControl<Control>("BlogFeedUrl", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("title", this.Title.ClientID);
            descriptor.AddElementProperty("selectButtonImageId", this.SelectButtonImageId.ClientID);
            descriptor.AddElementProperty("deselectButtonImageId", this.DeselectButtonImageId.ClientID);
            descriptor.AddComponentProperty("selectorImageId", this.SelectorImageId.ClientID);
            //descriptor.AddElementProperty("blogUrl", this.BlogUrl.ClientID);
            descriptor.AddElementProperty("blogFeedUrl", this.BlogFeedUrl.ClientID);
            descriptor.AddProperty("imageServiceUrl", this.imageServiceUrl);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(DeansBlogLastPostDesigner.scriptReference, typeof(DeansBlogLastPostDesigner).Assembly.FullName));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.js";
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
        #endregion
    }
}
 
