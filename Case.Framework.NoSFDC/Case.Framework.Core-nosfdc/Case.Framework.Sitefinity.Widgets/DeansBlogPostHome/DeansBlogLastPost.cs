﻿using System;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web.UI;
using UIUC.Custom.XmlService;

namespace Case.Framework.Sitefinity.Widgets.DeansBlogPostHome
{
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner))]
    public class DeansBlogLastPost : SimpleView
    {
        #region Properties

        public string BlogFeedUrl { get; set; }
        public Guid ImageId { get; set; }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return DeansBlogLastPost.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        public string Title { get; set; }

        #endregion Properties

        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPost.ascx";

        protected virtual HyperLink hyPostUrl
        {
            get
            {
                return this.Container.GetControl<HyperLink>("hyPostUrl", true);
            }
        }

        protected virtual Image image
        {
            get
            {
                return this.Container.GetControl<Image>("image", true);
            }
        }

        protected virtual Literal litDate
        {
            get
            {
                return this.Container.GetControl<Literal>("litDate", true);
            }
        }

        protected virtual Literal litPostTitle
        {
            get
            {
                return this.Container.GetControl<Literal>("litPostTitle", true);
            }
        }

        protected virtual Literal litTitle
        {
            get
            {
                return this.Container.GetControl<Literal>("litTitle", true);
            }
        }

        //protected virtual HyperLink hyAllPosts
        //{
        //    get
        //    {
        //        return this.Container.GetControl<HyperLink>("hyAllPosts", true);
        //    }
        //}

        #region Methods

        /// <summary>
        /// Initializes the controls.
        /// </summary>
        /// <param name="container"></param>
        /// <remarks>
        /// Initialize your controls in this method. Do not override CreateChildControls method.
        /// </remarks>
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                var lastPost = BlogService.GetLastPostData(BlogFeedUrl);
                litPostTitle.Text = lastPost["Title"];
                litDate.Text = DateTime.Parse(lastPost["PubDate"]).ToString("MMM d");

                if (ImageId != null && ImageId != Guid.Empty)
                {
                    LibrariesManager manager = LibrariesManager.GetManager();
                    var imageFromLib = manager.GetImage(ImageId);
                    image.ImageUrl = imageFromLib.Url;
                    image.AlternateText = imageFromLib.AlternativeText;
                }

                if (!string.IsNullOrWhiteSpace(Title))
                {
                    litTitle.Text = Title;
                }
                else
                {
                    litTitle.Text = "Dean's Blog";
                }

                hyPostUrl.NavigateUrl = lastPost["Link"];
            }
            catch { }
            //hyAllPosts.NavigateUrl = BlogUrl;
            //hyAllPosts.Text = "All Posts ›";
        }

        #endregion Methods
    }
}