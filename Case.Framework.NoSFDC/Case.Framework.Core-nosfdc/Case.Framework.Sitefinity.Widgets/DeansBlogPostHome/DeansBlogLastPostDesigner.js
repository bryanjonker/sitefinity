Type.registerNamespace("Case.Framework.Sitefinity.Widgets.DeansBlogPostHome");

Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner = function (element) {
    /* Initialize Title fields */
    this._title = null;
    
    /* Initialize ImageId fields */
    this._selectButtonImageId = null;
    this._selectButtonImageIdClickDelegate = null;
    this._deselectButtonImageId = null;
    this._deselectButtonImageIdClickDelegate = null;
    this._selectorImageIdCloseDelegate = null;
    this._selectorImageIdUploaderViewFileChangedDelegate = null;
    
    this._ImageIdDialog = null;
    this._selectorImageId = null;
    this._ImageIdId = null;
    
    /* Initialize BlogUrl fields */
    //this._blogUrl = null;
    
    /* Initialize BlogFeedUrl fields */
    this._blogFeedUrl = null;
    
    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.callBaseMethod(this, 'initialize');

        /* Initialize ImageId */
        this._selectButtonImageIdClickDelegate = Function.createDelegate(this, this._selectButtonImageIdClicked);
        if (this._selectButtonImageId) {
            $addHandler(this._selectButtonImageId, "click", this._selectButtonImageIdClickDelegate);
        }

        this._deselectButtonImageIdClickDelegate = Function.createDelegate(this, this._deselectButtonImageIdClicked);
        if (this._deselectButtonImageId) {
            $addHandler(this._deselectButtonImageId, "click", this._deselectButtonImageIdClickDelegate);
        }

        if (this._selectorImageId) {
            this._ImageIdDialog = jQuery(this._selectorImageId.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorImageIdCloseDelegate
            });
        } 

        jQuery("#previewImageId").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorImageIdInsertDelegate = Function.createDelegate(this, this._selectorImageIdInsertHandler);
        this._selectorImageId.set_customInsertDelegate(this._selectorImageIdInsertDelegate);
        $addHandler(this._selectorImageId._cancelLink, "click", this._selectorImageIdCloseHandler);
        this._selectorImageIdCloseDelegate = Function.createDelegate(this, this._selectorImageIdCloseHandler);
        this._selectorImageIdUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorImageIdUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.callBaseMethod(this, 'dispose');

        /* Dispose ImageId */
        if (this._selectButtonImageId) {
            $removeHandler(this._selectButtonImageId, "click", this._selectButtonImageIdClickDelegate);
        }
        if (this._selectButtonImageIdClickDelegate) {
            delete this._selectButtonImageIdClickDelegate;
        }
        
        if (this._deselectButtonImageId) {
            $removeHandler(this._deselectButtonImageId, "click", this._deselectButtonImageIdClickDelegate);
        }
        if (this._deselectButtonImageIdClickDelegate) {
            delete this._deselectButtonImageIdClickDelegate;
        }

        $removeHandler(this._selectorImageId._cancelLink, "click", this._selectorImageIdCloseHandler);

        if (this._selectorImageIdCloseDelegate) {
            delete this._selectorImageIdCloseDelegate;
        }

        if (this._selectorImageIdUploaderViewFileChangedDelegate) {
            this._selectorImageId._uploaderView.remove_onFileChanged(this._selectorImageIdUploaderViewFileChangedDelegate);
            delete this._selectorImageIdUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI Title */
        jQuery(this.get_title()).val(controlData.Title);

        /* RefreshUI ImageId */
        this.get_selectedImageId().innerHTML = controlData.ImageId;
        if (controlData.ImageId && controlData.ImageId != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonImageId()).show()
            var url = this.imageServiceUrl + controlData.ImageId + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewImageId").show();
                    jQuery("#previewImageId").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonImageId()).hide()
        }

        /* RefreshUI BlogUrl */
        //jQuery(this.get_blogUrl()).val(controlData.BlogUrl);

        /* RefreshUI BlogFeedUrl */
        jQuery(this.get_blogFeedUrl()).val(controlData.BlogFeedUrl);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges Title */
        controlData.Title = jQuery(this.get_title()).val();

        /* ApplyChanges ImageId */
        controlData.ImageId = this.get_selectedImageId().innerHTML;

        /* ApplyChanges BlogUrl */
        //controlData.BlogUrl = jQuery(this.get_blogUrl()).val();

        /* ApplyChanges BlogFeedUrl */
        controlData.BlogFeedUrl = jQuery(this.get_blogFeedUrl()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* ImageId event handlers */
    _selectButtonImageIdClicked: function (sender, args) {
        this._selectorImageId._uploaderView.add_onFileChanged(this._selectorImageIdUploaderViewFileChangedDelegate);
        this._ImageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._ImageIdDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorImageId.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorImageId.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonImageIdClicked: function (sender, args) {
        jQuery("#previewImageId").hide();
                    jQuery("#previewImageId").attr("src", "");
        this.get_selectedImageId().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonImageId()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* ImageId private methods */
    _selectorImageIdInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._ImageIdId = selectedItem.Id;
            this.get_selectedImageId().innerHTML = this._ImageIdId;
            jQuery(this.get_deselectButtonImageId()).show()
            this.get_selectButtonImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewImageId").show();
                    jQuery("#previewImageId").attr("src", selectedItem.ThumbnailUrl);
        }
        this._ImageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorImageIdCloseHandler: function () {
        if(this._ImageIdDialog){
            this._ImageIdDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorImageIdUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* Title properties */
    get_title: function () { return this._title; }, 
    set_title: function (value) { this._title = value; },

    /* ImageId properties */
    get_selectorImageId: function () {
        return this._selectorImageId;
    },
    set_selectorImageId: function (value) {
        this._selectorImageId = value;
    },
    get_selectButtonImageId: function () {
        return this._selectButtonImageId;
    },
    set_selectButtonImageId: function (value) {
        this._selectButtonImageId = value;
    },
    get_deselectButtonImageId: function () {
        return this._deselectButtonImageId;
    },
    set_deselectButtonImageId: function (value) {
        this._deselectButtonImageId = value;
    },
    get_selectedImageId: function () {
        if (this._selectedImageId == null) {
            this._selectedImageId = this.findElement("selectedImageId");
        }
        return this._selectedImageId;
    },

    /* BlogUrl properties */
    //get_blogUrl: function () { return this._blogUrl; }, 
    //set_blogUrl: function (value) { this._blogUrl = value; },

    /* BlogFeedUrl properties */
    get_blogFeedUrl: function () { return this._blogFeedUrl; }, 
    set_blogFeedUrl: function (value) { this._blogFeedUrl = value; }
}

Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner.registerClass('Case.Framework.Sitefinity.Widgets.DeansBlogPostHome.DeansBlogLastPostDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
