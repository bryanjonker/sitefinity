﻿<%@ Control Language="C#" %>

   <div class="updates-title">
    <asp:Literal Text="" ID="litTitle" runat="server" />
    </div>

<article>
    
        <asp:Image  ID="image" runat="server" />        
        <asp:HyperLink ID="hyPostUrl" runat="server" Class="updates-link" >
          <asp:Literal ID="litPostTitle" runat="server" />
       </asp:HyperLink>
       <time datetime=""><asp:Literal Text="" ID="litDate" runat="server" /></time>
    
</article>

