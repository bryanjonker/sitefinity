<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>
<style>
div.RadWindow{
    width:800px!important;
}
</style>


<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
    <asp:CheckBox runat="server" ID="IsSectionHeader" Text="Section Menu" CssClass="sfCheckBox"/>        
    <div class="sfExample">Setting this will make the orange background of the logo and menu area of the site have a transparency - Only available on the main site templates</div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:CheckBox runat="server" ID="IsMicrositeMenu" Text="Microsite Menu" CssClass="sfCheckBox microSite"/>        
    <div class="sfExample">Use microsite header / mega menu view</div>
    </li>
    
    <li class="sfFormCtrl">
    <div id="htmlField" style="display:none;width:800px;height:600px;">
        <asp:Label runat="server" AssociatedControlID="MicrositeLogoContent" CssClass="sfTxtLbl">Logo Content</asp:Label>
        <sfFields:HtmlField ID="MicrositeLogoContent" ClientIDMode="Static"
        runat="server" Height="200" Width="700"
        DisplayMode="Write"></sfFields:HtmlField>
        <div class="sfExample"></div>
    </div>
    </li>
    
</ol>
</div>
