Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Navigation");

Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner = function (element) {
    /* Initialize IsSectionHeader fields */
    this._isSectionHeader = null;
    
    /* Initialize IsMicrositeMenu fields */
    this._isMicrositeMenu = null;
    
    /* Initialize MicrositeLogoContent fields- */
    this._micrositeLogoContent = null;
    
    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var propertyEditor = this._propertyEditor;

        jQuery(this.get_isMicrositeMenu()).change(function () {
            if ($(this).is(":checked")) {
                $('#htmlField').show();
                setTimeout(function () {
                    $('.ToggleAdvancedToolbars').parent().css('right', '103px')
                    jQuery('#designerLayoutRoot').css('max-height', '600px')
                    jQuery('#designerLayoutRoot').css('overflow-y', 'hidden')
                    jQuery('#designerLayoutRoot').css('overflow-x', 'hidden')
                    propertyEditor._dialog.SetSize(800, 800);
                }, 200);
              
            } else {
                $('#htmlField').hide();
                dialogBase.resizeToContent();
            }
        });
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI IsSectionHeader */
        jQuery(this.get_isSectionHeader()).attr("checked", controlData.IsSectionHeader);

        /* RefreshUI IsMicrositeMenu */
        jQuery(this.get_isMicrositeMenu()).attr("checked", controlData.IsMicrositeMenu);
     /* RefreshUI MicrositeLogoContent */
        this.get_micrositeLogoContent().set_value((controlData.MicrositeLogoContent));

        if (controlData.IsMicrositeMenu) {
            $('#htmlField').show();
            setTimeout(function () {
                $('.ToggleAdvancedToolbars').parent().css('right', '103px')
                propertyEditor._dialog.SetSize(800, 800);
                jQuery('#designerLayoutRoot').css('max-height', '600px')
                jQuery('#designerLayoutRoot').css('overflow-y', 'hidden')
                jQuery('#designerLayoutRoot').css('overflow-x', 'hidden')
            }, 200);
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges IsSectionHeader */
        controlData.IsSectionHeader = jQuery(this.get_isSectionHeader()).is(":checked");

        /* ApplyChanges IsMicrositeMenu */
        controlData.IsMicrositeMenu = jQuery(this.get_isMicrositeMenu()).is(":checked");

        if (controlData.IsMicrositeMenu) {
            /* ApplyChanges MicrositeLogoContent */
            controlData.MicrositeLogoContent = this.get_micrositeLogoContent().get_value();
        } else {
            controlData.MicrositeLogoContent = '';
        }
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* IsSectionHeader properties */
    get_isSectionHeader: function () { return this._isSectionHeader; }, 
    set_isSectionHeader: function (value) { this._isSectionHeader = value; },

    /* IsMicrositeMenu properties */
    get_isMicrositeMenu: function () { return this._isMicrositeMenu; }, 
    set_isMicrositeMenu: function (value) { this._isMicrositeMenu = value; },

    /* MicrositeLogoContent properties */
    get_micrositeLogoContent: function () { return this._micrositeLogoContent; }, 
    set_micrositeLogoContent: function (value) { this._micrositeLogoContent = value; }

}

Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
