﻿namespace Case.Framework.Sitefinity.Widgets.Navigation
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Case.Framework.Core.Utilities;
    using Case.Framework.Sitefinity.Data;
    using Case.Framework.Sitefinity.Models;

    using Telerik.Sitefinity.Web;
    using Telerik.Sitefinity.Web.UI;

    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner))]
    public class NavigationWidget : SimpleView
    {
        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.Navigation.Resources.Views.NavigationWidget.ascx";

        /// <summary>
        /// This is used to determin if the orange background of the top of the site is has transparency or not
        /// </summary>
        public bool IsSectionHeader { get; set; }

        public string MicrositeLogoContent { get; set; }

        public bool IsMicrositeMenu { get; set; }

        protected override string LayoutTemplateName{
            get
            {
                if (this.IsMicrositeMenu)
                {
                    return "Case.Framework.Sitefinity.Widgets.Navigation.Resources.Views.MicroNavigationWidget.ascx";
                }
                else
                {
                    return layoutTemplateName;
                }
            }
        }

        protected override Type ResourcesAssemblyInfo{
            get
            {
                return typeof(NavigationWidget);
            }
        }

        protected virtual Repeater rpNav{
            get
            {
                return this.Container.GetControl<Repeater>("rpNav", false);
            }
        }

        protected virtual Repeater rpInfo{
            get
            {
                return this.Container.GetControl<Repeater>("rpInfo", false);
            }
        }

        protected virtual Literal ltSection{
            get
            {
                return this.Container.GetControl<Literal>("ltSection", false);
            }
        }

        protected virtual Literal ltNavigation{
            get
            {
                return this.Container.GetControl<Literal>("ltNavigation", false);
            }
        }

        protected int tabIndex { get; set; }

        protected int col { get; set; }

        protected override void InitializeControls(GenericContainer container)
        {
            if (!this.IsMicrositeMenu)
            {
                if (this.IsSectionHeader)
                {
                    this.ltSection.Text = "<div class=\"js_warning\">For the best experience on this site, please <a href=\"http://enable-javascript.com\" target=\"_blank\">enable JavaScript</a> in your browser.</div><header id=\"masthead\" class=\"transparent\" role=\"banner\">";
                }
                else
                {
                    this.ltSection.Text = "<div class=\"js_warning\">For the best experience on this site, please <a href=\"http://enable-javascript.com\" target=\"_blank\">enable JavaScript</a> in your browser.</div><header id=\"masthead\" role=\"banner\">";
                }
                if (this.rpNav != null)
                {
                    this.rpNav.ItemDataBound += new RepeaterItemEventHandler(this.rpNav_ItemDataBound);
                }
                if (this.rpInfo != null)
                {
                    this.rpInfo.ItemDataBound += new RepeaterItemEventHandler(this.rpInfo_ItemDataBound);
                }
            }
            else
            {
                this.ltSection.Text = this.MicrositeLogoContent;
                var pages = CaseManagers.Pages.GetAll(true, true);

                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);
                var urlsAdded = new Dictionary<string, string>();

                var firstColumnPages = new string[] { "blog", "forum", "events", "news", "news and events" };
                var socialLinks = new string[] { "social media links" };
                this.col = 1;
                // -- render first column
                this.RenderFirstCol(urlsAdded, pages.Items.Where(x => firstColumnPages.Contains(x.Title.ToLower())).ToList(), writer);
                // -- render others
                this.RenderItem(urlsAdded, pages.Items.Where(x => !firstColumnPages.Contains(x.Title.ToLower()) && !socialLinks.Contains(x.Title.ToLower())).ToList(), writer);
                // -- render last
                this.RenderItem(urlsAdded, pages.Items.Where(x => socialLinks.Contains(x.Title.ToLower())).ToList(), writer);
                this.ltNavigation.Text = sWriter.ToString();
            }
        }

        private void RenderFirstCol(Dictionary<string, string> urlsAdded, IList<PageModel> pages, HtmlTextWriter writer, bool renderUl = true)
        {
            if (this.col == 1 && renderUl)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-quarter first");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            }
            foreach (var page in pages)
            {
                // -- First column locked into firstColumnPages name values any other names will be ignored
                var firstColumnPages = new string[] { "blog", "forum", "events", "news", "news and events" };
                var pos = Array.IndexOf(firstColumnPages, page.Title.ToLower());
                if (pos == -1)
                {
                    continue;
                }

                if (page.Title.ToLower() == "news and events")
                {
                    this.RenderFirstCol(urlsAdded, page.Items, writer, false);
                    urlsAdded.Add(string.Concat(this.col, "-", page.Title), page.Url); // -- add the url to urls we added collection
                }
                //  if (urlsAdded.Any(x => x.Contains(page.Url))) return;
                if (this.ContainsKeyValue(urlsAdded, string.Concat(this.col, "-", page.Title), page.Url))
                {
                    continue;
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "menu-section");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Tabindex, this.tabIndex.ToString());
                writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(page.Title);
                writer.RenderEndTag(); // href
                writer.RenderEndTag(); // div

                if (page.Items.Count() > 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "expand");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    foreach (var cPage in page.Items)
                    {
                        // -- First column locked into firstColumnPages name values any other names will be ignored
                        var cPos = Array.IndexOf(firstColumnPages, cPage.Title.ToLower());
                        if (cPos == -1)
                        {
                            continue;
                        }

                        if (this.ContainsKeyValue(urlsAdded, string.Concat(this.col, "-", cPage.Title), cPage.Url))
                        {
                            continue;
                        }
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, cPage.Url);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write(cPage.Title);
                        writer.RenderEndTag(); // href
                        writer.RenderEndTag(); // li
                        this.tabIndex++;
                        urlsAdded.Add(string.Concat(this.col, "-", cPage.Title), cPage.Url); // -- add the url to urls we added collection
                    }

                    writer.RenderEndTag(); // span
                }
                else
                {
                    this.tabIndex++;
                }
                urlsAdded.Add(string.Concat(this.col, "-", page.Title), page.Url); // -- add the url to urls we added collection
            }

            if (renderUl)
            {
                this.col++;
                writer.RenderEndTag();
            }
        }

        private void RenderItem(Dictionary<string, string> urlsAdded, IList<PageModel> pages, HtmlTextWriter writer)
        {
            foreach (var page in pages)
            {
                if (this.col < 3)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-quarter");
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "social-media-links column one-quarter last");
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                }

                //  if (urlsAdded.Any(x => x.Contains(page.Url))) return;
                if (this.ContainsKeyValue(urlsAdded, page.Title, page.Url))
                {
                    return;
                }

                if (this.col >= 3)
                {
                    if (page.Items.Count() > 0)
                    {
                        foreach (var sPage in page.Items.OrderBy(x => x.Title))
                        {
                            if (sPage.Title.ToLower() == "facebook")
                            {
                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-facebook");
                            }
                            else if (sPage.Title.ToLower() == "twitter")
                            {
                                writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-twitter");
                            }
                            writer.AddAttribute(HtmlTextWriterAttribute.Tabindex, this.tabIndex.ToString());
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, sPage.Url);
                            writer.AddAttribute(HtmlTextWriterAttribute.Name, sPage.Title);
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "hide-FAE");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(sPage.Title);
                            writer.RenderEndTag(); //span
                            writer.RenderEndTag(); // href
                            writer.Write(writer.NewLine);
                            this.tabIndex++;
                            urlsAdded.Add(string.Concat(this.col, "-", sPage.Title), sPage.Url); // -- add the url to urls we added collection
                        }
                    }
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "menu-section");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(page.Title);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "expand-menu");
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/icons/icon-expand.svg");
                    writer.AddAttribute("onerror", "this.src=/theme/img/icons/icon-expand.png;");
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Expand");
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "12px");
                    writer.AddAttribute(HtmlTextWriterAttribute.Height, "12px");
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag(); //img
                    writer.RenderEndTag(); // href
                    writer.RenderEndTag(); // href
                    writer.RenderEndTag(); // div

                    if (page.Items.Count() > 0)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "expand");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        foreach (var cPage in page.Items)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Li);
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, cPage.Url);
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write(cPage.Title);
                            writer.RenderEndTag(); // href
                            writer.RenderEndTag(); // li
                            this.tabIndex++;
                            urlsAdded.Add(string.Concat(this.col, "-", cPage.Title), cPage.Url); // -- add the url to urls we added collection
                        }

                        writer.RenderEndTag(); // span
                    }
                    else
                    {
                        this.tabIndex++;
                    }
                    urlsAdded.Add(string.Concat(this.col, "-", page.Title), page.Url); // -- add the url to urls we added collection
                }
                writer.RenderEndTag();
            }
            this.col++;
        }

        private bool ContainsKeyValue(Dictionary<string, string> dictionary,
                                      string expectedKey, string expectedValue)
        {
            string actualValue;
            return dictionary.TryGetValue(expectedKey, out actualValue) &&
                   actualValue == expectedValue;
        }

        private void rpInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.DataItem != null)
                {
                    ISitefinitySiteMapNode cNode = e.Item.DataItem as ISitefinitySiteMapNode;
                    PageSiteNode node = (PageSiteNode)cNode;

                    var showItem = node?.GetCustomFieldValue("IsShownOnInfoFor") as Boolean?;

                    if (!showItem.HasValue || !showItem.Value)
                    {
                        e.Item.Visible = false;
                    }
                    else
                    {
                        var Node = ((SiteMapNode)e.Item.DataItem);
                        var ltMainLink = (Literal)e.Item.FindControl("ltMainLink");

                        if (ltMainLink != null)
                        {
                            var sWriter = new StringWriter();
                            var writer = new HtmlTextWriter(sWriter);

                            if (Node.ChildNodes != null && Node.ChildNodes.Count > 0)
                            {
                                foreach (var subNode in Node.ChildNodes)
                                {
                                    var subPageNode = (PageSiteNode)subNode;
                                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                                    writer.AddAttribute(HtmlTextWriterAttribute.Href, WebHelper.ResolveUrl(subPageNode.Url));
                                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                                    writer.Write(subPageNode.Title);
                                    writer.RenderEndTag();
                                    writer.RenderEndTag();
                                    this.tabIndex++;
                                }
                            }
                            else
                            {
                                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                                writer.AddAttribute(HtmlTextWriterAttribute.Href, WebHelper.ResolveUrl(node.Url));
                                writer.RenderBeginTag(HtmlTextWriterTag.A);
                                writer.Write(node.Title);
                                writer.RenderEndTag();
                                writer.RenderEndTag();
                                this.tabIndex++;
                            }
                            ltMainLink.Text = sWriter.ToString();
                        }
                    }
                }
            }
            catch {}
        }

        private void rpNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                ISitefinitySiteMapNode cNode = e.Item.DataItem as ISitefinitySiteMapNode;
                PageSiteNode node = (PageSiteNode)cNode;

                var showItem = node?.GetCustomFieldValue("IsShownOnInfoFor") as Boolean?;
                if (!showItem.HasValue || !showItem.Value)
                {
                    Literal ltMainLink = (Literal)e.Item.FindControl("ltMainLink");
                    if (ltMainLink != null)
                    {
                        var sWriter = new StringWriter();
                        var writer = new HtmlTextWriter(sWriter);
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, WebHelper.ResolveUrl(node.Url));
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write(node.Title);
                        writer.RenderEndTag();
                        ltMainLink.Text = sWriter.ToString();
                    }

                    if (this.col == 1)
                    {
                        HtmlControl html = (HtmlControl)e.Item.FindControl("ulList");
                        if (html != null)
                        {
                            html.Attributes["class"] = "column one-quarter first";
                        }
                    }
                    else if (this.col == 4)
                    {
                        HtmlControl html = (HtmlControl)e.Item.FindControl("ulList");
                        if (html != null)
                        {
                            html.Attributes["class"] = "column one-quarter last";
                        }
                    }
                    this.col++;
                    this.tabIndex++;
                }

                if (!node.ShowInNavigation || !node.Visible)
                {
                    e.Item.Visible = false;
                }
                else
                {
                    var Node = ((SiteMapNode)e.Item.DataItem);

                    if (Node.HasChildNodes)
                    {
                        System.Web.UI.WebControls.Repeater rpSub = (System.Web.UI.WebControls.Repeater)e.Item.FindControl("rpSubNav");
                        if (rpSub != null)
                        {
                            rpSub.ItemDataBound += new RepeaterItemEventHandler(this.rpSub_ItemDataBound);
                            rpSub.DataSource = Node.ChildNodes;
                            rpSub.DataBind();
                        }
                    }
                }
            }
        }

        private void rpSub_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                ISitefinitySiteMapNode cNode = e.Item.DataItem as ISitefinitySiteMapNode;
                PageSiteNode node = (PageSiteNode)cNode;

                if (!CanShowSubLink(node))
                {
                    e.Item.Visible = false;
                }
                else
                {
                    Literal ltMainLink = (Literal)e.Item.FindControl("ltMainLink");
                    if (ltMainLink != null)
                    {
                        var sWriter = new StringWriter();
                        var writer = new HtmlTextWriter(sWriter);
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, WebHelper.ResolveUrl(node.Url));
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write(node.Title);
                        writer.RenderEndTag();
                        ltMainLink.Text = sWriter.ToString();
                        this.tabIndex++;
                    }
                }
            }
        }

        private static bool CanShowSubLink(PageSiteNode node)
        {
            if (node.ShowInNavigation)
            {
                if (node.Visible)
                {
                    return true;
                }
                if (node.NodeType == Telerik.Sitefinity.Pages.Model.NodeType.InnerRedirect || node.NodeType == Telerik.Sitefinity.Pages.Model.NodeType.OuterRedirect
                    || (node.NodeType == Telerik.Sitefinity.Pages.Model.NodeType.Group && node.HasChildNodes)) // Hide sub link if it is a group page without any children to redirect to
                {
                    return true;
                }

                return false;
            }
            return false;
        }
    }
}