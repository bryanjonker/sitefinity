using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
//
[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Navigation
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidget"/> widget
    /// </summary>
    public class NavigationWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return NavigationWidgetDesigner.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the IsSectionHeader property
        /// </summary>
        protected virtual Control IsSectionHeader
        {
            get
            {
                return this.Container.GetControl<Control>("IsSectionHeader", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the IsMicrositeMenu property
        /// </summary>
        protected virtual Control IsMicrositeMenu
        {
            get
            {
                return this.Container.GetControl<Control>("IsMicrositeMenu", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the MicrositeLogoContent property
        /// </summary>
        protected virtual Telerik.Sitefinity.Web.UI.Fields.HtmlField MicrositeLogoContent
        {
            get
            {
                return this.Container.GetControl<Telerik.Sitefinity.Web.UI.Fields.HtmlField>("MicrositeLogoContent", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("isSectionHeader", this.IsSectionHeader.ClientID);
            descriptor.AddElementProperty("isMicrositeMenu", this.IsMicrositeMenu.ClientID);
            descriptor.AddComponentProperty("micrositeLogoContent", this.MicrositeLogoContent.ClientID);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(NavigationWidgetDesigner.scriptReference, typeof(NavigationWidgetDesigner).Assembly.FullName));
            return scripts;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Navigation.NavigationWidgetDesigner.js";
        #endregion
    }
}
 
