﻿<%@ Control Language="C#" %>
<%@ Import Namespace="Telerik.Sitefinity.Web.UI.NavigationControls.Extensions.LightNavigationControlTemplate" %>
<%@ Import Namespace="Telerik.Sitefinity.Web.UI.NavigationControls" %>

<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="navigation" Namespace="Telerik.Sitefinity.Web.UI.NavigationControls" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>

<!-- JAVASCRIPT WARNING -->
<div class="js_warning">For the best experience on this site, please <a href="http://enable-javascript.com" target="_blank">enable JavaScript</a> in your browser.</div>
<!-- MASTHEAD • MICROSITE • TOP -->
<div id="masthead_microsite_top" class="bg orange" role="complementary" aria-label="Microsite Bar">
	<div class="row">
	   	<div class="column">
	    	<a href="http://education.illinois.edu/">an initiative of <img alt="Education at Illinois" src="/theme/img/logo-education-at-illinois.png" height="12">
            </a>
            <a href="http://illinois.edu"><img alt="Logo" class="i-mark mini" src="/theme/img/logo-transparent.png" alt="Logo" height="24"></a>
	    </div>
	</div>
</div>
<!-- MASTHEAD • MICROSITE-->
<header id="masthead" class="microsite" role="banner">
	<div class="row">
        <asp:Literal runat="server" ID="ltSection"></asp:Literal>
		<section class="masthead-ui">
		    <a class="reveal-menu button dark_gray inline open" href="#">
	    		<span class="menu-img"></span>
	    		<span class="menu-txt">Menu</span>
	    	</a>
			<section class="search" role="search" aria-label="Site Search">
				<label for="site-search">Site Search</label>
                <a class="search-btn" href="#">Search</a>
				<input id="site-search" class="search-inp" type="search" />
			</section>
		</section>
	</div>

<!-- MAIN MENU • MICROSITE -->
<nav id="microsite-navigation" role="navigation" aria-label="Microsite Navigation">
    <div id="masthead_microsite_top_mobile" class="bg orange toMove" role="complementary" aria-label="Microsite Bar">
        <div class="row">
            <div class="column">
                <a href="http://education.illinois.edu/">an initiative of <img alt="Education at Illinois" src="/theme/img/logo-education-at-illinois.png" height="12">
                </a>
                <a href="http://illinois.edu"><img alt="Logo" class="i-mark mini" src="/theme/img/logo-transparent.png" height="24"></a>
            </div>
        </div>
    </div>
    <div class="sitemap row">
        <asp:Literal runat="server" ID="ltNavigation"></asp:Literal>
    </div>


</nav></header>
<!-- END MAIN MENU • MICROSITE-->


