﻿<%@ Control Language="C#" %>
<asp:SiteMapDataSource ID="dataSource" runat="server" ShowStartingNode="false" StartFromCurrentNode="false"/>


    <!-- INCLUDE: MASTHEAD -->
    <asp:Literal runat="server" ID="ltSection"></asp:Literal>
    <!-- MASTHEAD -->
<div class="row">

	<div class="logo">
		<a href="http://illinois.edu/" tabindex="-1">
			<span id="logo-standard">
				<img alt="Logo" class="default" src="/theme/img/logo.png"/>
				<img alt="White Logo" class="alternate" src="/theme/img/logo-white.png" />
			</span>
		</a>
		<a href="http://education.illinois.edu/" tabindex="-1">
			<img class="coe" alt="Education at Illinois" src="/theme/img/logo-education-at-illinois.png" />
            <img class="coe coe-mobile" alt="Education at Illinois" src="/theme/img/logo-education-at-illinois-mobile.png" />
		</a>
		<a href="http://illinois.edu/" tabindex="-1">
			<span id="logo-microsite">
				<img alt="Logo" class="default" src="/theme/img/logo.png">
				<img alt="White Logo" class="alternate" src="/theme/img/logo-white.png">
			</span>
		</a>
		<a href="http://illinois.edu/" tabindex="-1">College of Illinois</a>
	</div>


	<section class="masthead-ui">
	    <a class="reveal-menu button blue inline open" href="#" tabindex="-1">
            <span class="menu-img"></span>
	    	<span class="menu-txt">Menu</span>
	    </a>
	    <section class="search" role="search" aria-label="Site Search">
            <label for="site-search">Site Search</label>
            <a class="search-btn" href="#">Search</a>
			<input id="site-search" class="search-inp" type="search" placeholder="Search..." />
	    </section>
	</section>
</div>

<!-- MAIN MENU -->
<nav id="primary-navigation" role="navigation" aria-label="Primary Navigation">
     <span class="hide-FAE"><h1>College of Education</h1></span>
	 <asp:Repeater ID="rpNav" runat="server" DataSourceID="dataSource">
            <HeaderTemplate>
                <div class="sitemap row">
                    <!-- Sitemap -->
            </HeaderTemplate>
            <ItemTemplate>
                <ul class="column one-quarter" runat="server" id="ulList">
                    <div class="menu-section">
                        <asp:Literal runat="server" ID="ltMainLink"></asp:Literal>
                        <a class="expand-menu"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="12px" height="12px"></a>
                    </div>
                    <span class="expand">
                    <asp:Repeater runat="server" ID="rpSubNav">
                        <ItemTemplate>
                            <li>
                             <asp:Literal runat="server" ID="ltMainLink"></asp:Literal>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    </span>
                </ul>
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>

	<div class="row">
		<div class="audience-box column one-half">
          <div class="audience-bg">
			<div class="column one-half">
				<a class="button orange apply_menu" href="#">Apply</a>
				<a class="button orange visit_menu" href="#">Visit</a>
				<a class="button orange chat_menu" href="#">Chat</a>
				<a class="button white" href="/home/make-a-gift">Make a Gift</a>
			</div>
			<ul class="column one-half">
				<div class="menu-section">Information for<a class="expand-menu"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="12px" height="12px"></a></div>
				<span class="expand">
                 <asp:Repeater runat="server" ID="rpInfo" DataSourceID="dataSource">
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="ltMainLink"></asp:Literal>
                    </ItemTemplate>
                 </asp:Repeater>
                </span>
			</ul>
            </div>
		</div>
		<ul class="column one-quarter">
			<li><a href="/news-events" class="news-events">News &amp; Events</a></li>
			<li><a href="/home/find-people">Find People</a></li>
			<li><a href="/home/available-positions">Available Positions</a></li>
			<li><a href="/home/facilities-and-maps">Facilities &amp; Maps</a></li>
			<li><a href="/home/my-education-portal">My Education Portal</a></li>
			<li><a href="http://illinois.edu/"><b>illinois.edu</b></a></li>
		</ul>
        <div class="social-media-links column one-quarter">
			<a class="online-program button orange" href="/home/online-and-off-campus-programs">Get Your Degree Online</a><br>
			<a class="ss-facebook ss-social" href="https://www.facebook.com/educationatillinois?ref=ts" name="Facebook"><span class="hide-FAE">Facebook</span></a>
			<a class="ss-twitter ss-social" href="https://twitter.com/edILLINOIS" name="Twitter"><span class="hide-FAE">Twitter</span></a>
			<a class="ss-linkedin ss-social" href="http://www.linkedin.com/profile/view?id=126941221&trk=nav_responsive_tab_profile" name="LinkedIn"><span class="hide-FAE">LinkedIn</span></a>
			<a class="ss-youtube ss-social" href="https://www.youtube.com/user/educationatillinois" name="Youtube"><span class="hide-FAE">Youtube</span></a>
			<a class="ss-flickr ss-social lastitem" href="https://www.flickr.com/photos/educationatillinois/sets/" name="Flickr"><span class="hide-FAE">Flickr</span></a>
		</div>
	</div><!-- /.row -->
</nav></header>
<!-- END MAIN MENU -->

