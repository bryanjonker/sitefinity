﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.GenericContent.Model;

namespace Case.Framework.Sitefinity.Widgets.Navigation
{
    [RequireScriptManager]
    [ControlDesigner(typeof(CleanNavDesigner))]
    public class CleanNav : SimpleView
    {
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                var html = new StringWriter();
                var htmlWriter = new HtmlTextWriter(html);

                var pageManager = PageManager.GetManager();
                var currentNode = SiteMapBase.GetCurrentProvider().CurrentNode as PageSiteNode;
                var pageId = Guid.Empty;  // Default to top level memu

                switch (SelectionMode)
                {
                    case "SelectedPageChildren":
                        if (!SelectedPageId.ToString().IsNullOrEmpty())
                        {
                            pageId = SelectedPageId;
                        }
                        break;
                    case "CurrentPageSiblings":
                        if (currentNode != null)
                        {
                            if (currentNode.ParentNode != null)
                            {
                                var parentNode = currentNode.ParentNode as PageSiteNode;
                                pageId = parentNode.Id;
                            }
                        }
                        break;
                    case "CurrentPageChildren":
                        pageId = currentNode.Id;
                        break;
                }

                SiteMapProvider provider = SiteMapBase.GetCurrentProvider();
                if (RenderAsDropdown)
                {
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "sidebar column sidebar-collapse");
                    htmlWriter.AddAttribute("role", "navigation");
                    htmlWriter.AddAttribute("aria-label", "Section Sidebar");
                    htmlWriter.RenderBeginTag("nav");
                }
                else
                {
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "sidebar");
                    htmlWriter.AddAttribute("role", "navigation");
                    htmlWriter.AddAttribute("aria-label", "Section Sidebar");
                    htmlWriter.RenderBeginTag("nav");
                }
                if (provider != null)
                {
                    SiteMapNode siteMapNode = pageId == Guid.Empty ? SiteMapBase.GetCurrentProvider().RootNode : provider.FindSiteMapNodeFromKey(pageId.ToString());
                    //Current page at top of list code
                    if (ShowCurrent && (SelectionMode == "CurrentPageChildren" || SelectionMode == "SelectedPageChildren" || SelectionMode == "CurrentPageSiblings"))
                    {
                        RenderItem(writer, siteMapNode, 1, ShowCurrent);
                        if (siteMapNode != null)
                            foreach (SiteMapNode m in siteMapNode.ChildNodes)
                            {
                                RenderItem(writer, m, 1);
                            }
                    }
                    else
                    {
                        if (siteMapNode != null)
                            foreach (SiteMapNode m in siteMapNode.ChildNodes)
                            {
                                RenderItem(writer, m, 1);
                            }
                    }
                }

            
                if (!RenderAsDropdown)
                {
                    htmlWriter.Write(MenuTitle.Text);
                    htmlWriter.Write(WrapTemplate.Replace("[[nav]]", sWriter.ToString()));
                    htmlWriter.RenderEndTag();
                    MenuContent.Text = html.ToString();
                    MenuTitle.Text = "";
                }
                else
                {
                    if (ShowCurrent)
                    {
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                    htmlWriter.Write(MenuTitle.Text);
                    htmlWriter.Write(sWriter.ToString());
                    htmlWriter.RenderEndTag();
                    MenuContent.Text = html.ToString();
                    MenuTitle.Text = "";
                }

                
                //RUN WRITERS
            }
            catch { }
        }

        protected void RenderItem(HtmlTextWriter writer, SiteMapNode node, int count, bool showTitle = false)
        {
            if (showTitle)
            {
                if (!RenderAsDropdown)
                {
                    var strWriter = new StringWriter();
                    var htmlWriter = new HtmlTextWriter(strWriter);
                    
                    var h2 = new Label();
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "sidebar-title");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    h2.Text = "In This Section";
                    h2.RenderControl(htmlWriter);
                    htmlWriter.RenderEndTag();
                    MenuTitle.Text = strWriter.ToString();

                    var lbl = new Label();
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "sidebar-section");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    lbl.Text = GetNavTitle(node);
                    lbl.RenderControl(writer);
                    writer.RenderEndTag();
                }
                else
                {
                  //  var hyp = new HyperLink {CssClass = "reveal-sidebar"};
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "reveal-sidebar");
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "sidebar-title");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class,"menu-img");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    writer.RenderEndTag();
                    writer.Write("In This Section");
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "drop-down");
                    writer.RenderBeginTag("section");
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                    var lbl = new Label();
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "sidebar-section");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    lbl.Text = GetNavTitle(node);
                    lbl.RenderControl(writer);
                    writer.RenderEndTag();
               //     hyp.RenderControl(writer);
                }
            }
            else
            {
                var currentNode = SiteMapBase.GetCurrentProvider().CurrentNode as PageSiteNode;
                var pageNode = (PageSiteNode)node;

                if (!pageNode.ShowInNavigation && !ShowAll) return;

                /* Create and Render Link */
                var hyp = new HyperLink();
                if ((pageNode.NodeType == NodeType.Group) && pageNode.HasChildNodes && (OverrideGroup == true))
                {
                    hyp.NavigateUrl = ResolveUrl(node.ChildNodes[0].Url);
                }
                else
                {
                    hyp.NavigateUrl = ResolveUrl(node.Url);
                }
                hyp.Text = GetNavTitle(node);
                    

                var classes = "";
                if (!this.IsDesignMode())
                {

                    if (currentNode != null)
                    {
                        if (currentNode.PageId == pageNode.PageId)
                        {
                            classes += " active";
                        }
                    }

                    //if (node.ChildNodes.Count > 0)
                    //{
                    //    classes += " parent";
                    //}

                    //if (node.PreviousSibling == null)
                    //{
                    //    classes += " first";
                    //}

                    //if (node.NextSibling == null)
                    //{
                    //    classes += " last";
                    //}

                    if (UniqueClasses)
                    {
                        classes += " " + pageNode.UrlName.ToLower();
                    }

                    if (classes.Length > 0)
                    {
                        //   writer.AddAttribute(HtmlTextWriterAttribute.Class, classes.Trim());
                    }
                }



                writer.RenderBeginTag(HtmlTextWriterTag.Li);
                hyp.CssClass = classes.Trim();
                hyp.Text = ItemTemplate.Replace("[[link]]", hyp.Text);
                hyp.ToolTip = GetNavTitle(node);
                hyp.RenderControl(writer);

                if (!Page.IsDesignMode())
                {
                    if (node.ChildNodes.Count > 0 && (count < LayerLimit || LayerLimit < 1) &&
                        currentNode.PageId == pageNode.PageId)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "inner");

                        writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                        foreach (SiteMapNode m in node.ChildNodes)
                        {
                            RenderItem(writer, m, count++);
                        }

                        writer.RenderEndTag();
                    }
                }
                writer.RenderEndTag();
            }
        }

        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.Navigation.Resources.Views.CleanNav.ascx"; }
        }

        public Literal MenuContent
        {
            get { return Container.GetControl<Literal>("Menu", true); }
        }

        public Literal MenuTitle
        {
            get
            {
                return Container.GetControl<Literal>("MenuTitle", true);
                
            }
        }
        public override void RenderBeginTag(HtmlTextWriter writer)
        {

        }
        public override void RenderEndTag(HtmlTextWriter writer)
        {

        }

        #region Variables
        public Guid SelectedPageId
        {
            get;
            set;
        }

        public Boolean UniqueClasses
        {
            get;
            set;
        }

        public Boolean OverrideGroup
        {
            get;
            set;
        }

        public Boolean ShowAll
        {
            get;
            set;
        }

        public string SelectionMode
        {
            get;
            set;
        }
        public bool ShowCurrent
        {
            get { return true; }
        }

        public bool RenderAsDropdown
        {
            get; 
            set;
        }
// ReSharper disable once InconsistentNaming
        protected string _wrapTemplate = "<ul>[[nav]]</ul>";
        public string WrapTemplate
        {
            get { return _wrapTemplate; }
            set { _wrapTemplate = value; }
        }

// ReSharper disable once InconsistentNaming
        protected string _itemTemplate = "[[link]]";
        public string ItemTemplate
        {
            get { return _itemTemplate; }
            set { _itemTemplate = value; }
        }

// ReSharper disable once InconsistentNaming
        protected int _layerLimit = 0;
        public int LayerLimit
        {
            get { return _layerLimit; }
            set { _layerLimit = value; }
        }
        #endregion

        private static string GetNavTitle(SiteMapNode node)
        {
            if (node == null)
            {
                return string.Empty;
            }
            var pageNode = node as PageSiteNode;
            var pageAltTitleObject = pageNode.GetCustomFieldValue("ShortTitle");
            if (pageAltTitleObject != null)
            {
                var siteMapNodeTitle = pageAltTitleObject as string;
                if (!string.IsNullOrEmpty(siteMapNodeTitle))
                    return pageAltTitleObject.ToString();
                else return node.Title;
            }
            else return node.Title;
        }
    }
}
