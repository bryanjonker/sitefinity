using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;
using System.Web.UI.HtmlControls;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Research
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Research.ResearchWidget"/> widget
    /// </summary>
    public class ResearchWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return layoutTemplatePath;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the Headertext property
        /// </summary>
        protected virtual Control Headertext
        {
            get
            {
                return this.Container.GetControl<Control>("Headertext", true);
            }
        }

        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual PagesSelector PageSelectorBreakThroughPageId
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorBreakThroughPageId", true);
            }
        }

        /// <summary>
        /// Gets the selector tag.
        /// </summary>
        /// <value>The selector tag.</value>
        public HtmlGenericControl SelectorTagBreakThroughPageId
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagBreakThroughPageId", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting ResearchImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonResearchImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonResearchImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting ResearchImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonResearchImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonResearchImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the ResearchImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorResearchImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorResearchImageId", false);
            }
        }


        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual PagesSelector PageSelectorResearchLinks
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorResearchLinks", true);
            }
        }

        /// <summary>
        /// Gets the selector tag.
        /// </summary>
        /// <value>The selector tag.</value>
        public HtmlGenericControl SelectorTagResearchLinks
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagResearchLinks", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here

            if (this.PropertyEditor != null)
            {
                var uiCulture = this.PropertyEditor.PropertyValuesCulture;
                this.PageSelectorBreakThroughPageId.UICulture = uiCulture;
            }
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("headertext", this.Headertext.ClientID);
            descriptor.AddComponentProperty("pageSelectorBreakThroughPageId", this.PageSelectorBreakThroughPageId.ClientID);
            descriptor.AddElementProperty("selectorTagBreakThroughPageId", this.SelectorTagBreakThroughPageId.ClientID);
            descriptor.AddElementProperty("selectButtonResearchImageId", this.SelectButtonResearchImageId.ClientID);
            descriptor.AddElementProperty("deselectButtonResearchImageId", this.DeselectButtonResearchImageId.ClientID);
            descriptor.AddComponentProperty("selectorResearchImageId", this.SelectorResearchImageId.ClientID);
            descriptor.AddComponentProperty("pageSelectorResearchLinks", this.PageSelectorResearchLinks.ClientID);
            descriptor.AddElementProperty("selectorTagResearchLinks", this.SelectorTagResearchLinks.ClientID);

            descriptor.AddProperty("imageServiceUrl", this.imageServiceUrl);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(ResearchWidgetDesigner.scriptReference, typeof(ResearchWidgetDesigner).Assembly.FullName));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "Case.Framework.Sitefinity.Widgets.Research.Resources.Views.ResearchWidgetDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Research.Resources.Javascript.ResearchWidgetDesigner.js";
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
        #endregion
    }
}
 
