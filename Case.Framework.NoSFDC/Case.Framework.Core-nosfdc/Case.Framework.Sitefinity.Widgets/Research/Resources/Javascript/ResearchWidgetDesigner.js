Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Research");

Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner = function (element) {
    /* Initialize Headertext fields */
    this._headertext = null;
    
    /* Initialize BreakThroughPageId fields */
    this._pageSelectorBreakThroughPageId = null;
    this._selectorTagBreakThroughPageId = null;
    this._BreakThroughPageIdDialog = null;
 
    this._showPageSelectorBreakThroughPageIdDelegate = null;
    this._pageSelectedBreakThroughPageIdDelegate = null;
    

    /* Initialize ResearchLinks fields */
    this._pageSelectorResearchLinks = null;
    this._selectorTagResearchLinks = null;
    this._ResearchLinksDialog = null;

    this._showPageSelectorResearchLinksDelegate = null;
    this._pageSelectedResearchLinksDelegate = null;


    /* Initialize ResearchImageId fields */
    this._selectButtonResearchImageId = null;
    this._selectButtonResearchImageIdClickDelegate = null;
    this._deselectButtonResearchImageId = null;
    this._deselectButtonResearchImageIdClickDelegate = null;
    this._selectorResearchImageIdCloseDelegate = null;
    this._selectorResearchImageIdUploaderViewFileChangedDelegate = null;
    
    this._ResearchImageIdDialog = null;
    this._selectorResearchImageId = null;
    this._ResearchImageIdId = null;
    
    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner.callBaseMethod(this, 'initialize');

        /* Initialize BreakThroughPageId */
        this._showPageSelectorBreakThroughPageIdDelegate = Function.createDelegate(this, this._showPageSelectorBreakThroughPageIdHandler);
        $addHandler(this.get_pageSelectButtonBreakThroughPageId(), "click", this._showPageSelectorBreakThroughPageIdDelegate);

        this._pageSelectedBreakThroughPageIdDelegate = Function.createDelegate(this, this._pageSelectedBreakThroughPageIdHandler);
        this.get_pageSelectorBreakThroughPageId().add_doneClientSelection(this._pageSelectedBreakThroughPageIdDelegate);

        if (this._selectorTagBreakThroughPageId) {
            this._BreakThroughPageIdDialog = jQuery(this._selectorTagBreakThroughPageId).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }

        /* Initialize ResearchLinks */
        this._showPageSelectorResearchLinksDelegate = Function.createDelegate(this, this._showPageSelectorResearchLinksHandler);
        $addHandler(this.get_pageSelectButtonResearchLinks(), "click", this._showPageSelectorResearchLinksDelegate);

        this._pageSelectedResearchLinksDelegate = Function.createDelegate(this, this._pageSelectedResearchLinksHandler);
        this.get_pageSelectorResearchLinks().add_doneClientSelection(this._pageSelectedResearchLinksDelegate);

        if (this._selectorTagResearchLinks) {
            this._ResearchLinksDialog = jQuery(this._selectorTagResearchLinks).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }

        /* Initialize ResearchImageId */
        this._selectButtonResearchImageIdClickDelegate = Function.createDelegate(this, this._selectButtonResearchImageIdClicked);
        if (this._selectButtonResearchImageId) {
            $addHandler(this._selectButtonResearchImageId, "click", this._selectButtonResearchImageIdClickDelegate);
        }

        this._deselectButtonResearchImageIdClickDelegate = Function.createDelegate(this, this._deselectButtonResearchImageIdClicked);
        if (this._deselectButtonResearchImageId) {
            $addHandler(this._deselectButtonResearchImageId, "click", this._deselectButtonResearchImageIdClickDelegate);
        }

        if (this._selectorResearchImageId) {
            this._ResearchImageIdDialog = jQuery(this._selectorResearchImageId.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorResearchImageIdCloseDelegate
            });
        } 

        jQuery("#previewResearchImageId").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorResearchImageIdInsertDelegate = Function.createDelegate(this, this._selectorResearchImageIdInsertHandler);
        this._selectorResearchImageId.set_customInsertDelegate(this._selectorResearchImageIdInsertDelegate);
        $addHandler(this._selectorResearchImageId._cancelLink, "click", this._selectorResearchImageIdCloseHandler);
        this._selectorResearchImageIdCloseDelegate = Function.createDelegate(this, this._selectorResearchImageIdCloseHandler);
        this._selectorResearchImageIdUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorResearchImageIdUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner.callBaseMethod(this, 'dispose');

        /* Dispose BreakThroughPageId */
        if (this._showPageSelectorBreakThroughPageIdDelegate) {
            $removeHandler(this.get_pageSelectButtonBreakThroughPageId(), "click", this._showPageSelectorBreakThroughPageIdDelegate);
            delete this._showPageSelectorBreakThroughPageIdDelegate;
        }

        if (this._pageSelectedBreakThroughPageIdDelegate) {
            this.get_pageSelectorBreakThroughPageId().remove_doneClientSelection(this._pageSelectedBreakThroughPageIdDelegate);
            delete this._pageSelectedBreakThroughPageIdDelegate;
        }

        /* Dispose ResearchLinks */
        if (this._showPageSelectorResearchLinksDelegate) {
            $removeHandler(this.get_pageSelectButtonResearchLinks(), "click", this._showPageSelectorResearchLinksDelegate);
            delete this._showPageSelectorResearchLinksDelegate;
        }

        if (this._pageSelectedResearchLinksDelegate) {
            this.get_pageSelectorResearchLinks().remove_doneClientSelection(this._pageSelectedResearchLinksDelegate);
            delete this._pageSelectedResearchLinksDelegate;
        }

        /* Dispose ResearchImageId */
        if (this._selectButtonResearchImageId) {
            $removeHandler(this._selectButtonResearchImageId, "click", this._selectButtonResearchImageIdClickDelegate);
        }
        if (this._selectButtonResearchImageIdClickDelegate) {
            delete this._selectButtonResearchImageIdClickDelegate;
        }
        
        if (this._deselectButtonResearchImageId) {
            $removeHandler(this._deselectButtonResearchImageId, "click", this._deselectButtonResearchImageIdClickDelegate);
        }
        if (this._deselectButtonResearchImageIdClickDelegate) {
            delete this._deselectButtonResearchImageIdClickDelegate;
        }

        $removeHandler(this._selectorResearchImageId._cancelLink, "click", this._selectorResearchImageIdCloseHandler);

        if (this._selectorResearchImageIdCloseDelegate) {
            delete this._selectorResearchImageIdCloseDelegate;
        }

        if (this._selectorResearchImageIdUploaderViewFileChangedDelegate) {
            this._selectorResearchImageId._uploaderView.remove_onFileChanged(this._selectorResearchImageIdUploaderViewFileChangedDelegate);
            delete this._selectorResearchImageIdUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI Headertext */
        jQuery(this.get_headertext()).val(controlData.Headertext);

        /* RefreshUI BreakThroughPageId */
        if (controlData.BreakThroughPageId && controlData.BreakThroughPageId !== "00000000-0000-0000-0000-000000000000") {
            var pagesSelectorBreakThroughPageId = this.get_pageSelectorBreakThroughPageId().get_pageSelector();
            var selectedPageLabelBreakThroughPageId = this.get_selectedBreakThroughPageIdLabel();
            var selectedPageButtonBreakThroughPageId = this.get_pageSelectButtonBreakThroughPageId();
            pagesSelectorBreakThroughPageId.add_selectionApplied(function (o, args) {
                var selectedPage = pagesSelectorBreakThroughPageId.get_selectedItem();
                if (selectedPage) {
                    selectedPageLabelBreakThroughPageId.innerHTML = selectedPage.Title.Value;
                    jQuery(selectedPageLabelBreakThroughPageId).show();
                    selectedPageButtonBreakThroughPageId.innerHTML = '<span>Change</span>';
                }
            });
            pagesSelectorBreakThroughPageId.set_selectedItems([{ Id: controlData.BreakThroughPageId}]);
        }        

        /* RefreshUI ResearchLinks */
        if (controlData.ResearchLinks && controlData.ResearchLinks !== "00000000-0000-0000-0000-000000000000") {
            var wrapper = this.get_pageSelectorResearchLinks().get_pageSelector();
            var selector = this.get_pageSelectorResearchLinks().get_pageSelector();;
            var selectedPageLabelResearchLinks = this.get_selectedResearchLinksLabel();
            var selectedPageButtonResearchLinks = this.get_pageSelectButtonResearchLinks();

            var selecting = false;
            wrapper.add_selectionApplied(function () {
                if (selecting) {
                    return;
                }
                selecting = true;
                var pageIds = controlData.ResearchLinks;
                if (pageIds) {

                    selectedPageButtonResearchLinks.innerHTML = '<span>Change</span>';

                    var pages = pageIds.split(",");
                    var oldMethod = selector._updateTreeSelection;
                    selector._updateTreeSelection = function () {
                        if (this._treeIsBound == false) {
                            this._treeMustBeUpdated = true;
                            return;
                        }
                        this._treeMustBeUpdated = false;
                        var tree = this.get_itemsTree();
                        var binder = tree.getBinder();
                        if (this._selectedItemId) {
                            binder.setSelectedValues([this._selectedItemId], true, true);
                        } else {
                            var selectedItems = tree.get_selectedItems();
                            if (selectedItems && selectedItems.length > 0) {
                                binder.clearSelection();
                                binder.setSelectedItems(selectedItems, true, true);
                            } else {
                                if (this._selectedItemIds) {
                                    binder.setSelectedValues(this._selectedItemIds, true, true);
                                }
                            }
                        }
                        this._raiseSelectionApplied(this, {});
                    };
                    selector.set_selectedItemIds(pages);
                    selector._updateTreeSelection = oldMethod;
                    selectedPageLabelResearchLinks.innerHTML = '';

                    for (var x = 0; x < selector.get_selectedItems().length; x++) {
                        var page = selector.get_selectedItems()[x];
                        if (x != (selector.get_selectedItems().length - 1))
                            selectedPageLabelResearchLinks.innerHTML += page.Title.Value + ', ';
                        else
                            selectedPageLabelResearchLinks.innerHTML += page.Title.Value;
                    }
                    selectedPageLabelResearchLinks.innerHTML += "<br/>";
                    selectedPageLabelResearchLinks.innerHTML += "<br/>";
                    jQuery(selectedPageLabelResearchLinks).show();
                    dialogBase.resizeToContent();

                }
            });
        }

        /* RefreshUI ResearchImageId */
        this.get_selectedResearchImageId().innerHTML = controlData.ResearchImageId;
        if (controlData.ResearchImageId && controlData.ResearchImageId != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonResearchImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonResearchImageId()).show()
            var url = this.imageServiceUrl + controlData.ResearchImageId + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewResearchImageId").show();
                    jQuery("#previewResearchImageId").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonResearchImageId()).hide()
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges Headertext */
        controlData.Headertext = jQuery(this.get_headertext()).val();

        /* ApplyChanges BreakThroughPageId */

        /* ApplyChanges ResearchImageId */
        controlData.ResearchImageId = this.get_selectedResearchImageId().innerHTML;
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* ResearchImageId event handlers */
    _selectButtonResearchImageIdClicked: function (sender, args) {
        this._selectorResearchImageId._uploaderView.add_onFileChanged(this._selectorResearchImageIdUploaderViewFileChangedDelegate);
        this._ResearchImageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._ResearchImageIdDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorResearchImageId.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorResearchImageId.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonResearchImageIdClicked: function (sender, args) {
        jQuery("#previewResearchImageId").hide();
                    jQuery("#previewResearchImageId").attr("src", "");
        this.get_selectedResearchImageId().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonResearchImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonResearchImageId()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* BreakThroughPageId private methods */
    _showPageSelectorBreakThroughPageIdHandler: function (selectedItem) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorBreakThroughPageId().get_pageSelector();
        if (controlData.BreakThroughPageId) {
            pagesSelector.set_selectedItems([{ Id: controlData.BreakThroughPageId }]);
        }
        this._BreakThroughPageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._BreakThroughPageIdDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedBreakThroughPageIdHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorBreakThroughPageId().get_pageSelector();
        this._BreakThroughPageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
        if (items == null) {
            return;
        }
        var selectedPage = pagesSelector.get_selectedItem();
        if (selectedPage) {
            this.get_selectedBreakThroughPageIdLabel().innerHTML = selectedPage.Title.Value;
            jQuery(this.get_selectedBreakThroughPageIdLabel()).show();
            this.get_pageSelectButtonBreakThroughPageId().innerHTML = '<span>Change</span>';
            controlData.BreakThroughPageId = selectedPage.Id;
        }
        else {
            jQuery(this.get_selectedBreakThroughPageIdLabel()).hide();
            this.get_pageSelectButtonBreakThroughPageId().innerHTML = '<span>Select...</span>';
            controlData.BreakThroughPageId = "00000000-0000-0000-0000-000000000000";
        }
    },

    /* ResearchLinks private methods */
    _showPageSelectorResearchLinksHandler: function (selectedItem) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorResearchLinks().get_pageSelector();
        if (controlData.ResearchLinks) {
            pagesSelector.set_selectedItems([{ Id: controlData.ResearchLinks }]);
        }
        this._ResearchLinksDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._ResearchLinksDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedResearchLinksHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorResearchLinks().get_pageSelector();

        if (items == null) {
            this._ResearchLinksDialog.dialog("close");
            jQuery("#designerLayoutRoot").show();
            dialogBase.resizeToContent();
            return;
        }

        if (items.length > 4) {
            alert('You can only have 4 pages selected.')
            return;
        }

        this._ResearchLinksDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
        var selectedPages = pagesSelector.get_selectedItems();
        var pages = [];
        if (selectedPages) {
            this.get_selectedResearchLinksLabel().innerHTML = '';
            for (var i = 0, l = selectedPages.length; i < l; i++) {
                var page = selectedPages[i];
                pages.push(page.Id);
                if (i != (selectedPages.length - 1))
                    this.get_selectedResearchLinksLabel().innerHTML += page.Title.Value + ', ';
                else
                    this.get_selectedResearchLinksLabel().innerHTML += page.Title.Value;

            }
            this.get_selectedResearchLinksLabel().innerHTML += '<br/>';
            this.get_selectedResearchLinksLabel().innerHTML += '<br/>';
            jQuery(this.get_selectedResearchLinksLabel()).show();
            controlData.ResearchLinks = pages.join();
        }
        else {
            jQuery(this.get_selectedResearchLinksLabel()).hide();
            this.get_pageSelectButtonResearchLinks().innerHTML = '<span>Select...</span>';
            controlData.ResearchLinks = "00000000-0000-0000-0000-000000000000";
        }
    },

    /* ResearchImageId private methods */
    _selectorResearchImageIdInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._ResearchImageIdId = selectedItem.Id;
            this.get_selectedResearchImageId().innerHTML = this._ResearchImageIdId;
            jQuery(this.get_deselectButtonResearchImageId()).show()
            this.get_selectButtonResearchImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewResearchImageId").show();
                    jQuery("#previewResearchImageId").attr("src", selectedItem.ThumbnailUrl);
        }
        this._ResearchImageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorResearchImageIdCloseHandler: function () {
        if(this._ResearchImageIdDialog){
            this._ResearchImageIdDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorResearchImageIdUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* Headertext properties */
    get_headertext: function () { return this._headertext; }, 
    set_headertext: function (value) { this._headertext = value; },

    /* BreakThroughPageId properties */
    get_pageSelectButtonBreakThroughPageId: function () {
        if (this._pageSelectButtonBreakThroughPageId == null) {
            this._pageSelectButtonBreakThroughPageId = this.findElement("pageSelectButtonBreakThroughPageId");
        }
        return this._pageSelectButtonBreakThroughPageId;
    },
    get_selectedBreakThroughPageIdLabel: function () {
        if (this._selectedBreakThroughPageIdLabel == null) {
            this._selectedBreakThroughPageIdLabel = this.findElement("selectedBreakThroughPageIdLabel");
        }
        return this._selectedBreakThroughPageIdLabel;
    },
    get_pageSelectorBreakThroughPageId: function () {
        return this._pageSelectorBreakThroughPageId;
    },
    set_pageSelectorBreakThroughPageId: function (val) {
        this._pageSelectorBreakThroughPageId = val;
    },
    get_selectorTagBreakThroughPageId: function () {
        return this._selectorTagBreakThroughPageId;
    },
    set_selectorTagBreakThroughPageId: function (value) {
        this._selectorTagBreakThroughPageId = value;
    },

    /* ResearchLinks properties */
    get_pageSelectButtonResearchLinks: function () {
        if (this._pageSelectButtonResearchLinks == null) {
            this._pageSelectButtonResearchLinks = this.findElement("pageSelectButtonResearchLinks");
        }
        return this._pageSelectButtonResearchLinks;
    },
    get_selectedResearchLinksLabel: function () {
        if (this._selectedResearchLinksLabel == null) {
            this._selectedResearchLinksLabel = this.findElement("selectedResearchLinksLabel");
        }
        return this._selectedResearchLinksLabel;
    },
    get_pageSelectorResearchLinks: function () {
        return this._pageSelectorResearchLinks;
    },
    set_pageSelectorResearchLinks: function (val) {
        this._pageSelectorResearchLinks = val;
    },
    get_selectorTagResearchLinks: function () {
        return this._selectorTagResearchLinks;
    },
    set_selectorTagResearchLinks: function (value) {
        this._selectorTagResearchLinks = value;
    },

    /* ResearchImageId properties */
    get_selectorResearchImageId: function () {
        return this._selectorResearchImageId;
    },
    set_selectorResearchImageId: function (value) {
        this._selectorResearchImageId = value;
    },
    get_selectButtonResearchImageId: function () {
        return this._selectButtonResearchImageId;
    },
    set_selectButtonResearchImageId: function (value) {
        this._selectButtonResearchImageId = value;
    },
    get_deselectButtonResearchImageId: function () {
        return this._deselectButtonResearchImageId;
    },
    set_deselectButtonResearchImageId: function (value) {
        this._deselectButtonResearchImageId = value;
    },
    get_selectedResearchImageId: function () {
        if (this._selectedResearchImageId == null) {
            this._selectedResearchImageId = this.findElement("selectedResearchImageId");
        }
        return this._selectedResearchImageId;
    }
}

Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
