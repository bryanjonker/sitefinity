<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="Headertext" CssClass="sfTxtLbl">Header text</asp:Label>
    <asp:TextBox ID="Headertext" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <label class="sfTxtLbl" for="selectedBreakThroughPageIdLabel">Breakthrough Page</label>
    <span style="display: none;" class="sfSelectedItem" id="selectedBreakThroughPageIdLabel">
        <asp:Literal runat="server" Text="" />
    </span>
    <span class="sfLinkBtn sfChange">
        <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonBreakThroughPageId">
            <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </a>
    </span>
    <div id="selectorTagBreakThroughPageId" runat="server" style="display: none;">
        <sf:PagesSelector runat="server" ID="pageSelectorBreakThroughPageId" 
            AllowExternalPagesSelection="false" AllowMultipleSelection="false" />
    </div>
    <div class="sfExample"></div>
    </li>
       <li class="sfFormCtrl">
    <label class="sfTxtLbl" for="selectedResearchLinksLabel">Research & Outreach Page(s)</label>
    <span style="display: none;" class="sfSelectedItem" id="selectedResearchLinksLabel">
        <asp:Literal ID="Literal1" runat="server" Text="" />
    </span>
    <span class="sfLinkBtn sfChange">
        <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonResearchLinks">
            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </a>
    </span>
    <div id="selectorTagResearchLinks" runat="server" style="display: none;">
        <sf:PagesSelector runat="server" ID="pageSelectorResearchLinks" 
            AllowExternalPagesSelection="false" AllowMultipleSelection="true" />
    </div>
    <div class="sfExample"></div>
    </li>
    <li class="sfFormCtrl">
    <asp:Label runat="server" CssClass="sfTxtLbl">Header Background Image</asp:Label>
    <img id="previewResearchImageId" src="" alt="" style="display:none;" />
    <span style="display: none;" class="sfSelectedItem" id="selectedResearchImageId"></span>
    <div>
      <asp:LinkButton ID="selectButtonResearchImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
      </asp:LinkButton>
      <asp:LinkButton ID="deselectButtonResearchImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
      </asp:LinkButton>
    </div>
    <sf:EditorContentManagerDialog runat="server" ID="selectorResearchImageId" DialogMode="Image" HostedInRadWindow="false" BodyCssClass="" />
    <div class="sfExample"></div>
    </li>
    
</ol>
</div>
