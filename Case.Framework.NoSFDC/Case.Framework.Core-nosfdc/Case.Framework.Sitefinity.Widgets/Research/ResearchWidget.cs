﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Builder;
using Telerik.Sitefinity.DynamicModules.Builder.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.Research
{
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Research.ResearchWidgetDesigner))]
    public class ResearchWidget : SimpleView
    {

       public string Headertext { get; set; }
        public Guid ResearchImageId { get; set; }

        private Guid[] selectedResearchPages;

        public Guid[] SelectedResourcePages
        {
            get
            {
                if (selectedResearchPages == null) selectedResearchPages = new Guid[] { };
                return selectedResearchPages;
            }
            set { selectedResearchPages = value; }
        }

        public string ResearchLinks
        {
            get { return string.Join(",", SelectedResourcePages); }
            set
            {
                var list = new List<Guid>();
                if (value != null)
                {
                    var guids = value.Split(',');
                    foreach (var guid in guids)
                    {
                        Guid newGuid;
                        if (Guid.TryParse(guid, out newGuid))
                            list.Add(newGuid);
                    }
                }
                SelectedResourcePages = list.ToArray();
            }
        }
     
        public Guid BreakThroughPageId { get; set; }

        protected virtual Literal LitQuote
        {
            get
            {
                return Container.GetControl<Literal>("ltResearch", true);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                // render research
                RenderResearch(writer);

               
                LitQuote.Text = sWriter.ToString();
            }
            catch { }
        }

        protected void RenderResearch(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "banner-title");
            writer.RenderBeginTag("section");
            if (!String.IsNullOrEmpty(Headertext))
            {
                string txt = Headertext;
                string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                if (lst.Count() > 0)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H1);
                    writer.Write(lst[0]);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "subheading");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    writer.Write(lst[1]);
                    writer.RenderEndTag(); // span
                    writer.RenderEndTag(); //H2

                }
            }
            writer.RenderEndTag();


            writer.AddAttribute(HtmlTextWriterAttribute.Class, "research_top featured");
            if (ResearchImageId != Guid.Empty)
            {
                var image = CaseManagers.Images.GetById(ResearchImageId);
                if (image != null)
                {
                    // url(\"" + imgPath + "\")
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "background: url(" + image.Url + ")  no-repeat center center;background-size: cover;");
                }
            }
            writer.RenderBeginTag("section");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "row");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    
            if (SelectedResourcePages.Count() > 0)
            {

                for (int x = 0; x < SelectedResourcePages.Count(); x++)
                {
                    var page = CaseManagers.Pages.GetById(selectedResearchPages[x]);
                    if (page != null)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-quarter");
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.RenderBeginTag(HtmlTextWriterTag.P);
                        writer.AddAttribute(HtmlTextWriterAttribute.Id,(x + 1).ToString());
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write(page.Title);
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                        writer.RenderEndTag();

                    }
                }

                //Desktop
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "nodes");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "svg");
                writer.AddAttribute("onerror", "this.src=/theme/img/content/research_widget/research_desktop.png;");
                writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Research SVG Widget Desktop");
                writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/content/research_widget/research_desktop.svg");
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag(); // img

                //Mobile
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "mnodes");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "svg");
                writer.AddAttribute("onerror", "this.src=/theme/img/content/research_widget/research_mobile.png;");
                writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Research SVG Widget Mobile");
                writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/content/research_widget/research_mobile.svg");
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag(); // img


                writer.AddAttribute(HtmlTextWriterAttribute.Class, "more_topics");
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                if (BreakThroughPageId != Guid.Empty)
                {
                    var page = CaseManagers.Pages.GetById(BreakThroughPageId);
                    if (page != null)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("More Topics&nbsp;&rsaquo;");
                        writer.RenderEndTag();
                    }
                }
                writer.RenderEndTag();
            }
            writer.RenderEndTag(); // div
            writer.RenderEndTag(); //end research
        }

        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.Research.Resources.Views.ResearchWidget.ascx"; }
        }
        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }
    }
}
