﻿using LinqToTwitter;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Web.UI;

namespace Case.Framework.Sitefinity.Widgets.TFeed
{
   
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.TFeed.Designer.TFeedWidgetDesigner))]
    public class TFeedWidget : SimpleView
    {
        #region Properties
        /// <summary>
        /// Gets or sets the message that will be displayed in the label.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return TFeedWidget.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }
        #endregion

        #region Control References
        protected virtual Literal ltTwitter
        {
            get
            {
                return this.Container.GetControl<Literal>("ltTwitter", true);
            }
        }

    
        #endregion

        #region Methods
        /// <summary>
        /// Initializes the controls.
        /// </summary>
        /// <param name="container"></param>
        /// <remarks>
        /// Initialize your controls in this method. Do not override CreateChildControls method.
        /// </remarks>
        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);
            if (!String.IsNullOrEmpty(Message))
                BuildSocialBox(writer);

            ltTwitter.Text = sWriter.ToString();
        }
        #endregion


        private void BuildSocialBox(HtmlTextWriter tab1)
        {
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "bg social-box");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);

            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "row");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);

            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "column three-quarter first");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "social-box-title");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social ss-twitter");
            tab1.AddAttribute(HtmlTextWriterAttribute.Name, "twitter");
            tab1.RenderBeginTag(HtmlTextWriterTag.Span);
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "hide-FAE");
            tab1.RenderBeginTag(HtmlTextWriterTag.Span);
            tab1.Write("Connect with Twitter");
            tab1.RenderEndTag(); // SPAN
            tab1.RenderEndTag(); // Span
            tab1.Write(string.Concat("@", Message));
            tab1.RenderEndTag(); //DIV
            tab1.RenderEndTag(); //DIV FIRST

            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "column one-quarter last");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);
            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
            tab1.AddAttribute(HtmlTextWriterAttribute.Href, string.Concat("https://twitter.com/intent/follow?original_referer=&region=follow_link&screen_name=", Message, "&tw_p=followbutton"));
            tab1.RenderBeginTag(HtmlTextWriterTag.A);
            tab1.Write("Follow&nbsp;&rsaquo;");
            tab1.RenderEndTag(); // HRef
            tab1.RenderEndTag(); //DIV LAST

            tab1.RenderEndTag(); //DIV ROW

            tab1.AddAttribute(HtmlTextWriterAttribute.Class, "row");
            tab1.RenderBeginTag(HtmlTextWriterTag.Div);

            // -- Get Tweets
            GetTweets(tab1);

            tab1.RenderEndTag(); //DIV
            tab1.RenderEndTag(); //DIV

        }


        private void GetTweets(HtmlTextWriter writer)
        {
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore()
            };

            auth.CredentialStore.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            auth.CredentialStore.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
            auth.CredentialStore.OAuthToken = ConfigurationManager.AppSettings["accessToken"];
            auth.CredentialStore.OAuthTokenSecret = ConfigurationManager.AppSettings["accessTokenSecret"];

            try
            {
                var twitterCtx = new TwitterContext(auth);
                var tweets = twitterCtx.Status.Where(x => x.Type == StatusType.User && x.ScreenName == Message).OrderByDescending(x => x.CreatedAt).Take(2);

                if (tweets.ToList().Any())
                {
                    var arr = tweets.ToArray();

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-half first");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute("datetime", "");
                    writer.RenderBeginTag("time");
                    writer.Write(GetTweetTime(arr[0].CreatedAt));
                    writer.RenderEndTag(); //time

                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(ParseTweet(arr[0].Text));
                    writer.RenderEndTag(); //P
                    writer.RenderEndTag(); //DIV

                    if (arr.Count() > 1)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-half last");
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.AddAttribute("datetime", "");
                        writer.RenderBeginTag("time");
                        writer.Write(GetTweetTime(arr[1].CreatedAt));
                        writer.RenderEndTag();
                        writer.RenderBeginTag(HtmlTextWriterTag.P);
                        writer.Write(ParseTweet(arr[1].Text));
                        writer.RenderEndTag(); //P
                        writer.RenderEndTag(); //DIV 
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        public static string GetTweetTime(DateTime pDate)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var ts = new TimeSpan(DateTime.UtcNow.Ticks - pDate.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 0)
            {
                return "not yet";
            }
            if (delta < 1 * MINUTE)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }
            if (delta < 2 * MINUTE)
            {
                return "a minute ago";
            }
            if (delta < 45 * MINUTE)
            {
                return ts.Minutes + " minutes ago";
            }
            if (delta < 90 * MINUTE)
            {
                return "an hour ago";
            }
            if (delta < 24 * HOUR)
            {
                return ts.Hours + " hours ago";
            }
            if (delta < 48 * HOUR)
            {
                return "yesterday";
            }
            if (delta < 30 * DAY)
            {
                return ts.Days + " days ago";
            }
            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }

        public string ParseTweet(string rawTweet)
        {
            Regex link = new Regex(@"http(s)?://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&amp;\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?");
            Regex screenName = new Regex(@"@\w+");
            Regex hashTag = new Regex(@"#\w+");

            string formattedTweet = link.Replace(rawTweet, delegate(Match m)
            {
                string val = m.Value;
                return "<a href='" + val + "' target='_blank'>" + val + "</a>";
            });

            formattedTweet = screenName.Replace(formattedTweet, delegate(Match m)
            {
                string val = m.Value.Trim('@');
                return string.Format("<a href='http://twitter.com/{0}' target='_blank'>@{1}</a>", val, val);
            });

            formattedTweet = hashTag.Replace(formattedTweet, delegate(Match m)
            {
                string val = m.Value;
                return string.Format("<a href='http://twitter.com/search?q=%23{0}' target='_blank'>{1}</a>", m.Value.Replace('#', ' ').Trim(), val);
            });

            return formattedTweet;
        }


        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.TFeed.TFeedWidget.ascx";
        #endregion
    }
}
