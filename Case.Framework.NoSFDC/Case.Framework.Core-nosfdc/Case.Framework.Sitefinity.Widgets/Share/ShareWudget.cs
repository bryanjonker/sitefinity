﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;
using Telerik.Sitefinity.Model;
using System.IO;


namespace Case.Framework.Sitefinity.Widgets.Share
{

    public class ShareWidget : SimpleView
    {

        public bool showSocial { get; set; }

        protected override string LayoutTemplateName
        {
            get
            {
                return ShareWidget.layoutTemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(ShareWidget);
            }
        }
        protected virtual Panel pnlShare
        {
            get
            {
                return this.Container.GetControl<Panel>("pnlShare", true);
            }

        }

        protected virtual Literal ltSocial
        {
            get
            {
                return this.Container.GetControl<Literal>("ltSocial", true);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            SiteMapNode currentNode = SiteMap.CurrentNode;
            PageSiteNode node = (PageSiteNode)currentNode;
            if (currentNode != null)
            {
                try
                {
                    if ((bool)node.GetCustomFieldValue("ShowSocialShare"))
                    {
                        pnlShare.Visible = true;
                        var sWriter = new StringWriter();
                        var writer = new HtmlTextWriter(sWriter);

                       //facebook
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "Facebook");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=700,height=400,top=500,left=500');return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://www.facebook.com/sharer.php?u=" + HttpContext.Current.Request.Url.AbsoluteUri);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#xF610;");
                        writer.RenderEndTag(); //A

                        //twitter
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "Twitter");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=700,height=400,top=500,left=500');return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://twitter.com/share?url=" + HttpContext.Current.Request.Url.AbsoluteUri);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#xF611;");
                        writer.RenderEndTag(); //A

                        //linked-in
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ss-icon ss-social");
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, "Twitter");
                        writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=700,height=400,top=500,left=500');return false;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://www.linkedin.com/shareArticle?mini=true&url=" + HttpContext.Current.Request.Url.AbsoluteUri);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("&#xF612;");
                        writer.RenderEndTag(); //A



                        ltSocial.Text = sWriter.ToString();

                    }
                    else pnlShare.Visible = false;
                }
                catch
                {
                    pnlShare.Visible = true;
                }

            }
            
        }

  
        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.Share.Resources.ShareWidget.ascx";


    }

}
