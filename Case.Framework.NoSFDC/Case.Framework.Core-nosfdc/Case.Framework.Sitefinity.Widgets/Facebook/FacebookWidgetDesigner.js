Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Facebook");

Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner = function (element) {
    /* Initialize FacebookPageURL fields */
    this._facebookPageURL = null;
    
    /* Initialize width fields */
    this._width = null;
    
    /* Initialize height fields */
    this._height = null;
    
    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI FacebookPageURL */
        jQuery(this.get_facebookPageURL()).val(controlData.FacebookPageURL);

        /* RefreshUI width */
        jQuery(this.get_width()).val(controlData.width);

        /* RefreshUI height */
        jQuery(this.get_height()).val(controlData.height);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges FacebookPageURL */
        controlData.FacebookPageURL = jQuery(this.get_facebookPageURL()).val();

        /* ApplyChanges width */
        controlData.width = jQuery(this.get_width()).val();

        /* ApplyChanges height */
        controlData.height = jQuery(this.get_height()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* FacebookPageURL properties */
    get_facebookPageURL: function () { return this._facebookPageURL; }, 
    set_facebookPageURL: function (value) { this._facebookPageURL = value; },

    /* width properties */
    get_width: function () { return this._width; }, 
    set_width: function (value) { this._width = value; },

    /* height properties */
    get_height: function () { return this._height; }, 
    set_height: function (value) { this._height = value; }
}

Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
