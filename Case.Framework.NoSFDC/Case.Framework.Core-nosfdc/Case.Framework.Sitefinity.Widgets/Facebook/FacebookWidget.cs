﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;

namespace Case.Framework.Sitefinity.Widgets.Facebook
{
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Facebook.FacebookWidgetDesigner))]
    public class FacebookWidget : SimpleView
    {
        protected override string LayoutTemplateName
        {
            get
            {
                return FacebookWidget.layoutTemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(FacebookWidget);
            }
        }

        protected virtual Literal ltFacebook
        {
            get
            {
                 return this.Container.GetControl<Literal>("ltFacebook", true);
            }
        }

        public virtual string FacebookPageURL
        {
            get;
            set;
        }

        public virtual string width
        {
            get;
            set;
        }

        public virtual string height
        {
            get;
            set;
        }
        
        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);

            SiteMapNode currentNode = SiteMap.CurrentNode;
            PageSiteNode node = (PageSiteNode)currentNode;
            if (currentNode != null)
            {
                //<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" 
                //data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "fb-like-box");
                writer.AddAttribute("data-href", FacebookPageURL);
                writer.AddAttribute("data-colorscheme", "light");
                writer.AddAttribute("data-show-faces", "true");
                writer.AddAttribute("data-header", "true");
                writer.AddAttribute("data-stream", "false");
                writer.AddAttribute("data-show-border", "false");
                if (!string.IsNullOrEmpty(width))
                    writer.AddAttribute("data-width", width);
                if (!string.IsNullOrEmpty(height))
                    writer.AddAttribute("data-height", height);

                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderEndTag();

            }

            ltFacebook.Text = sWriter.ToString();
        }

        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.Facebook.Resources.FacebookWidget.ascx";


    }

}
