Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Archive");

Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner = function (element) {
    Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.initializeBase(this, [element]);

    this._CategoriesSelector = null;
    this._resizeControlDesignerDelegate = null;
}


Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.prototype = {
    initialize: function () {
        Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.callBaseMethod(this, 'initialize');
        dialogBase.setWndHeight("500px");
        dialogBase.setWndWidth("500px");
    },
    dispose: function () {
        Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.callBaseMethod(this, 'dispose');
    },
    refreshUI: function () {
        var data = this._propertyEditor.get_control();

        // load categories
        var c = this.get_CategoriesSelector();
        var cats = data.CategoryValue;
        if (cats != null)
            c.set_value(data.CategoryValue.split(","));

        dialogBase.setWndHeight("500px");
        dialogBase.setWndWidth("500px");

        this.resizeEvents();
    },
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();
        dialogBase.setWndHeight("500px");
        dialogBase.setWndWidth("500px");
        debugger;

        // save selected categories
        var c = this.get_CategoriesSelector();
        var cats = c.get_value();
        if (cats != null)
            controlData.CategoryValue = c.get_value().join();
        else controlData.CategoryValue = [];
    },

    // add resize events
    resizeEvents: function () {
       
        // resize control designer on category selector close
        var c = this.get_CategoriesSelector();
        var cs = c.get_taxaSelector();
        cs.add_selectionDone(this._resizeControlDesigner);

        this._resizeControlDesignerDelegate = Function.createDelegate(this, this._resizeControlDesigner);
        $addHandler(c._replaceImageButtonElement, "click", this.Delegate);


        // resize control designer on category selector open]
        var csb = c.get_changeSelectedTaxaButton();
        $addHandler(csb, "click", this._resizeControlDesignerDelegate);
    },


    
    // Categories Selector
    get_CategoriesSelector: function () {
        return this._CategoriesSelector;
    },
    set_CategoriesSelector: function (value) {
        this._CategoriesSelector = value;
    },

    // function to initialize resizer methods and handlers
    _resizeControlDesigner: function () {
        setTimeout("dialogBase.resizeToContent()", 200);
    }
}

Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();