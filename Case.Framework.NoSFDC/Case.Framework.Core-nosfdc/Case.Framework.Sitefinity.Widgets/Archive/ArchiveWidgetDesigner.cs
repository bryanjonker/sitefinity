using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using Telerik.Sitefinity.Web.UI.Fields;
using Telerik.Sitefinity.Taxonomies;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Archive
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidget"/> widget
    /// </summary>
    public class ArchiveWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return ArchiveWidgetDesigner.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
		/// Gets the categories selector.
		/// </summary>
		protected HierarchicalTaxonField CategoriesSelector
		{
			get { return Container.GetControl<HierarchicalTaxonField>("CategoriesSelector", true); }
		}

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // initialize the taxonomy selectors
            CategoriesSelector.TaxonomyId = TaxonomyManager.CategoriesTaxonomyId;
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
          		var descriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
			var descriptor = (ScriptControlDescriptor)descriptors.Last();
			descriptor.AddComponentProperty("CategoriesSelector", this.CategoriesSelector.ClientID);
			return descriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(ArchiveWidgetDesigner.scriptReference, typeof(ArchiveWidgetDesigner).Assembly.FullName));
            return scripts;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner.js";
        #endregion
    }
}
 
