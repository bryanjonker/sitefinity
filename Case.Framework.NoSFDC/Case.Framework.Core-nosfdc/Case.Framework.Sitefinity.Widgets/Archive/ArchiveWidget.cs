﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;

namespace Case.Framework.Sitefinity.Widgets.Archive
{
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidgetDesigner))]
    public class ArchiveWidget : SimpleView
    {
        protected override string LayoutTemplateName
        {
            get
            {
                return ArchiveWidget.layoutTemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(ArchiveWidget);
            }
        }

        /// <summary>
        /// Gets or sets the selected categories.
        /// </summary>
        /// <value>
        /// The selected categories.
        /// </value>
        public Guid[] SelectedCategories
        {
            get
            {
                if (selectedCategories == null) selectedCategories = new Guid[] { };
                return selectedCategories;
            }
            set { selectedCategories = value; }
        }

        private Guid[] selectedCategories;

        /// <summary>
        /// Intermediary property for passing categories to and from the designer
        /// </summary>
        /// <value>
        /// The category value as a comma-delimited string.
        /// </value>
        public string CategoryValue
        {
            get { return string.Join(",", SelectedCategories); }
            set
            {
                var list = new List<Guid>();
                if (value != null)
                {
                    var guids = value.Split(',');
                    foreach (var guid in guids)
                    {
                        Guid newGuid;
                        if (Guid.TryParse(guid, out newGuid))
                            list.Add(newGuid);
                    }
                }
                SelectedCategories = list.ToArray();
            }
        }


        protected virtual Literal ltArchive
        {
            get
            {
                return Container.GetControl<Literal>("ltArchive", true);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);
                SiteMapNode currentNode = SiteMap.CurrentNode;
            PageManager manager = PageManager.GetManager();

            PageSiteNode node = (PageSiteNode)currentNode;
            if (currentNode != null)
            {
                IEnumerable<NewsItemModel> articles;
                if (SelectedCategories.Length > 0)
                    articles = CaseManagers.News.GetByCategoryId(SelectedCategories[0]);
                else articles = CaseManagers.News.GetAll().ToList();

                if (articles != null)
                {
                    var years = articles.GroupBy(x => x.PublicationDate.Date).Select(group => new
                      {
                          Year = group.Key.Year
                      }).OrderByDescending(x => x.Year).Distinct();

                    int i = 0;
                    foreach (var item in years)
                    {
                        ++i;
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "expand");
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "expand-this");
                        writer.AddAttribute("role", "button");
                        writer.AddAttribute("aria-label", "Expand " + i);
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/icons/icon-expand.svg");
                        writer.AddAttribute("onerror", "this.src=/theme/img/icons/icon-expand.png;");
                        writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Expand");
                        writer.AddAttribute(HtmlTextWriterAttribute.Width, "14px");
                        writer.AddAttribute(HtmlTextWriterAttribute.Height, "14px");
                        writer.RenderBeginTag(HtmlTextWriterTag.Img);
                        writer.RenderEndTag(); // IMG
                        writer.RenderEndTag(); // SPAN
                        writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                        writer.Write(item.Year);
                        writer.RenderEndTag(); // STRONG

                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "inputs");
                        writer.RenderBeginTag("section");
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "no-bullets");
                        writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                        var months = articles.Where(x => x.PublicationDate.Year == item.Year).GroupBy(x => x.PublicationDate.Month).Select(group => new
                        {
                            Month = group.Key,
                            Count = group.Count()
                        }).OrderBy(x => x.Month);

                        foreach (var month in months)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Li);
                            if (month.Month < 10)
                                writer.AddAttribute(HtmlTextWriterAttribute.Href, string.Concat(ToAbsoluteUrl(currentNode.Url), "?year=", item.Year, "&month=", month.Month.ToString("0#")));
                            else writer.AddAttribute(HtmlTextWriterAttribute.Href, string.Concat(ToAbsoluteUrl(currentNode.Url), "?year=", item.Year, "&month=", month.Month));
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month.Month));
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(" (" + month.Count + ")");
                            writer.RenderEndTag();
                            writer.RenderEndTag();
                            writer.RenderEndTag(); // LI
                        }

                        writer.RenderEndTag(); // UL
                        writer.RenderEndTag(); // SECTION
                        writer.RenderEndTag(); //DIV
                    }

                    ltArchive.Text = sWriter.ToString();
                }
            }
        }
    
        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.Archive.Resources.ArchiveWidget.ascx";

        private static string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

    }

}
