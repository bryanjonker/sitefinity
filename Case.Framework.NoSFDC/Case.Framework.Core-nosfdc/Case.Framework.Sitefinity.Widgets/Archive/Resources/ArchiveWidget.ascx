﻿<%@ Control Language="C#" Inherits="Case.Framework.Sitefinity.Widgets.Archive.ArchiveWidget.cs" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>
     <!-- ARCHIVES -->
            <div class="archives-title">Archives</div>
            <span class="archives">
                <asp:Literal runat="server" ID="ltArchive"></asp:Literal>
            </span>

