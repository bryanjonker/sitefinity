﻿<%@ Control Language="C#" Inherits="Case.Framework.Sitefinity.Widgets.Header.HeaderWidget.cs" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>

           <div id="find_expert" class="find row bg-gray" role="search" aria-label="Find an Expert">
            	<div class="find-title">Find an Expert</div>
            	<div class="row">
                    <label for="name_kw">By name or keyword</label>
            	</div>
            	<div class="row">
            		<div class="column two-third first">
            			<input id="name_kw" name="name_kw" type="text" class="input_text" placeholder="Enter" value="">
            		</div>
            		<div class="column one-third last">
            			<label for="find">Find an Expert</label>
                        <input id="find" class="button orange" type="submit" value="Find&nbsp;&rsaquo;" />
            		</div>
            	</div>
            </div>
<script src="/Theme/js/FindAnExpert.js"></script>
<div class='ajaxoverlay' style="display: none">
    <div class='ajaxloader'></div>
</div>
<asp:Literal ID="ltDept" runat="server"></asp:Literal>