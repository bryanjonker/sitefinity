﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;

namespace Case.Framework.Sitefinity.Widgets.FindExpert
{
    public class FindExpertWidget : SimpleView
    {
        protected override string LayoutTemplateName
        {
            get
            {
                return FindExpertWidget.layoutTemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(FindExpertWidget);
            }
        }

        protected virtual Literal ltDept
        {
            get
            {
                return this.Container.GetControl<Literal>("ltDept", true);
            }
        }
        public string Department { get; set; }

        protected override void InitializeControls(GenericContainer container)
        {
            if (!string.IsNullOrEmpty(Department))
            {
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                writer.AddAttribute(HtmlTextWriterAttribute.Value, Department);
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "Dep");
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
                writer.RenderBeginTag(HtmlTextWriterTag.Input);
                writer.RenderEndTag();
                ltDept.Text = sWriter.ToString();
            }
        }

        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.FindExpert.Resources.FindExpertWidget.ascx";


    }

}
