﻿<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="designers" Namespace="Telerik.Sitefinity.Web.UI.ControlDesign" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Modules.Libraries.Web.UI.Designers" TagPrefix="sitefinity" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>

<div id="selectorTagPageId" runat="server" style="display: none;">
    <sitefinity:PagesSelector runat="server" ID="pageSelectorPageId" 
        AllowExternalPagesSelection="true" AllowMultipleSelection="false" />
</div>

<div class="sfContentViews">
<div class="sfColWrapper sfEqualCols sfModeSelector sfClearfix sfNavDesignerCtrl sfNavDim">
<div id="RotatorDesignChoice" class="sfLeftCol">
    <h2 class="sfStep1">Add Links / Header Text</h2>
    <br />
    <div class="sfFormCtrl">
            <asp:Label ID="Label2" runat="server" CssClass="sfTxtLbl">Header Text</asp:Label>
            <input id="txtDescription" type="text" class="sfTxt" style="width:150px;" />
            <div class="sfExample">Title to be shown above links</div>
   </div>      
    <div class="sfModeSwitcher">
		<a id="aAddTab" class="sfLinkBtn"><strong class="sfLinkBtnIn">Add Link</strong></a>
    </div>
    <br />
   <div class="sfExample">
        <span style="font-weight: bold">Links: </span>
   </div>  
    <ul id="ulAdded" class="sfRadioList RotatorDesignList"> </ul>
    <br />
</div>
<div class="sfRightCol">
    <h2 class="sfStep2">Enter URL, Text & Image for Links.</h2>
    <div id="divStep2" class="sfStep2Options" style="display: none;">    
        <div id="groupSettingPageSelect">
            <div class="sfExpandableSection">
                <ul class="sfRadioList RotatorDesignList">
                   <li>
                        <label style="color: #333;">Title</label>
                        <br />
                        <input id="txtTitle" type="text" class="sfTxt" />
                    </li>
                     <li>
                        <label style="color: #333;">URL</label>
                        <br />
                        <span class="sfLinkBtn sfChange">
                            <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonPageId">
                                <asp:Literal runat="server" Text="Select Page..." />
                            </a>
                        </span>
                        <input id="txtUrl" type="text" class="sfTxt" style="width: 600px;" disabled="disabled" />
                        <input id="txtUrlGuid" type="text" class="sfTxt" style="width: 600px; visibility: hidden;" />
                    </li>
                    <li>
                        <label style="color: #333;">Column Number</label>
                        <br />
                        <select id="txtColumn" class="sfTxt">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </li>
                    <li>
                        <label style="color: #333;">Row Number (to reorder items)</label>
                        <br />
                        <select id="txtRow" class="sfTxt">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                        </select>
                    </li>
                </ul>
                <br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        </div>    
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    function openSelector(forState) {
        $('#selectorTag').css('display', 'block');
        dialogBase.resizeToContent();

        _forState = forState;
    }
</script>

