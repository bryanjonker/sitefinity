﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.FacultyResources
{
    [RequireScriptManager]
    [ControlDesigner(typeof(FacultyResourcesWidgetDesigner))]
    public class FacultyResourcesWidget : SimpleView
    {
        private string data = string.Empty;
        private string viewMode = string.Empty;
        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string HeaderText
        {
            get;
            set;
        }

        protected virtual Literal ltLinks
        {
            get
            {
                return this.Container.GetControl<Literal>("ltLinks", true);
            }
        }

        protected virtual Literal ltHeader
        {
            get
            {
                return this.Container.GetControl<Literal>("ltHeader", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);
                string[] css = { "column one-third first", "column one-third", "column one-third last" };


                for (int col = 1; col < 4; col++)
                {
                   var strCol = col.ToString();
                   var sections = new JavaScriptSerializer().Deserialize<List<Section>>(Data).Where(x => x.Column == strCol).ToList().OrderBy(x => x.Row).ThenBy(x => x.Name).ToList();

                    writer.AddAttribute(HtmlTextWriterAttribute.Class, css[col-1]);
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    for (int x = 0; x < sections.Count; x++)
                    {
                        if (sections[x].Column == "1")
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button white");
                            writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + sections[x].Name);

                            writer.AddAttribute(HtmlTextWriterAttribute.Href, GetUrl(sections[x]));
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write(sections[x].Name.Replace("_", " ") + "&nbsp;&rsaquo;");
                            writer.RenderEndTag(); //A
                        }
                        else
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.P);
                            writer.AddAttribute(HtmlTextWriterAttribute.Id, "lnk_" + sections[x].Name);
                            writer.AddAttribute(HtmlTextWriterAttribute.Href, GetUrl(sections[x]));
                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                            writer.Write(sections[x].Name.Replace("_", " ") + "&nbsp;&rsaquo;");
                            writer.RenderEndTag(); //A
                            writer.RenderEndTag(); //P
                        }
                    }
                    writer.RenderEndTag();
                }

                ltHeader.Text = HeaderText;
                ltLinks.Text = sWriter.ToString();
            }
            catch { }
        }


        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.FacultyResources.Resources.Views.FacultyResourcesWidget.ascx";
            }
        }

        private static string GetUrl(Section section)
        {
            if (section.URLGuid != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(section.URLGuid);
                if (page != null)
                {
                    return page.Url;
                }
            }
            return section.URL;
        }

        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }

        public class Section
        {
            public string Title;
            public string Name;
            public string Index;
            public string URL;
            public string Column;
            public Guid URLGuid;
            public string Row;
        }
    }
}
