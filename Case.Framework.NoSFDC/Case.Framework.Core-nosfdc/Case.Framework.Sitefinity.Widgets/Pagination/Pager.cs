﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;

namespace Case.Framework.Sitefinity.Widgets.Pagination
{
    public class CPager : Telerik.Sitefinity.Web.UI.Pager
    {
        #region Properties

        /// <summary>
        /// Gets the name of the layout template.
        /// </summary>
        /// <value>The name of the layout template.</value>
        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.Pagination.Pager.ascx";
            }
        }

        /// <summary>
        /// Gets or sets the layout template path.
        /// </summary>
        /// <value>The layout template path.</value>
        public override string LayoutTemplatePath
        {
            get
            {
                return "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Pagination.Resources.Pager.ascx";
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected virtual Literal ltPaginationInfo
        {
            get
            {
                return this.Container.GetControl<Literal>("ltPaginationInfo", true);
            }
        }
        #endregion

        #region Overrides

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var currentPage = this.Container.GetControl<Literal>("currentPage", true);
            if (currentPage != null)
            {
                currentPage.Text = this.CurrentPage.ToString();
            }

           
            var pagesCount = this.Container.GetControl<Literal>("pagesCount", true);
            if (pagesCount != null)
            {
                var pageCount = (int)Math.Ceiling((double)this.VirtualItemCount / this.PageSize);

                pagesCount.Text = pageCount.ToString();
            }

            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "pagesize");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.PageSize.ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();


            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "itemCount");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.VirtualItemCount.ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "page");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.CurrentPage.ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            ltPaginationInfo.Text = sWriter.ToString();
        }

        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>();
            scripts.Add(new ScriptReference
            {
                Assembly = typeof(Telerik.Sitefinity.Web.UI.Pager).Assembly.GetName().ToString(),
                Name = "Telerik.Sitefinity.Web.Scripts.Pager.js"
            });

            scripts.Add(new ScriptReference
            {
                Assembly = this.GetType().Assembly.GetName().ToString(),
                Name = "Case.Framework.Sitefinity.Widget.Pagination.Resources.Pager.js"
            });

            return scripts;
        }

        #endregion
    }

}
