﻿Type.registerNamespace("Telerik.Sitefinity.Web.UI");
Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Pagination");
 
CustomPager.Pager = function (element) {
    CustomPager.Pager.initializeBase(this, [element]);
}
 
CustomPager.Pager.prototype = {
    initialize: function () {
        CustomPager.Pager.callBaseMethod(this, "initialize");
    },
 
    dispose: function () {
    }
};
 
CustomPager.Pager.registerClass("Case.Framework.Sitefinity.Widgets.Pagination.CPager", Telerik.Sitefinity.Web.UI.Pager);