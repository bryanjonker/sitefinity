﻿<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sf" %>
     <div class="row">
       <ul class="pagination"></ul>
    </div>
<div style="display:none">
           <asp:Literal runat="server" id="currentPage" Visible="false"></asp:Literal>
           <asp:Literal runat="server" id="pagesCount" Visible="false"></asp:Literal>
           <div runat="server" id="numeric" visible="false"></div>
        <sf:SitefinityHyperLink ID="cmdPrev" runat="server" Text="<" visible="false" />
        <sf:SitefinityHyperLink ID="cmdNext" runat="server" Text=">" visible="false" />
</div>
<asp:Literal runat="server" ID="ltPaginationInfo"></asp:Literal>