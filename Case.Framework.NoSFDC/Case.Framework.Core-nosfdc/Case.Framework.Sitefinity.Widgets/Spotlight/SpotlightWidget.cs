﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.Spotlight
{
    [RequireScriptManager, Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Spotlight.SpotlightWidgetDesigner))]
    public class SpotlightWidget : SimpleView
    {
        public string Title { get; set; }
        public Guid ImageId { get; set; }
        public Guid PageId { get; set; }
        public string Description { get; set; }
        public string Snippet { get; set; }
        public string ExternalUrl { get; set; }

        protected virtual Literal ltSpotlight
        {
            get
            {
                return this.Container.GetControl<Literal>("ltSpotlight", true);
            }
        }
     
        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "updates-title");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.Write(Title);
            writer.RenderEndTag();
            writer.RenderBeginTag("article");

            if (PageId != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(PageId);

                writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                if (ImageId != Guid.Empty)
                {
                    var image = CaseManagers.Images.GetById(ImageId);
                    if (image != null)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Alt, image.AlternativeText);
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, image.Url);
                        writer.RenderBeginTag(HtmlTextWriterTag.Img);
                        writer.RenderEndTag(); // img
                    }
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "updates-link");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(Description);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.Write(Snippet);
                writer.RenderEndTag();
                writer.RenderEndTag(); // A
            }
            else
            {
                if (!string.IsNullOrEmpty(ExternalUrl))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, ExternalUrl);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                }
                if (ImageId != Guid.Empty)
                {
                    var image = CaseManagers.Images.GetById(ImageId);
                    if (image != null)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Alt, image.AlternativeText);
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, image.Url);
                        writer.RenderBeginTag(HtmlTextWriterTag.Img);
                        writer.RenderEndTag(); // img
                    }
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "updates-link");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(Description);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.Write(Snippet);
                writer.RenderEndTag();
                if (!string.IsNullOrEmpty(ExternalUrl))
                {
                    writer.RenderEndTag(); // A
                }
            }
      
            writer.RenderEndTag(); // article

            ltSpotlight.Text = sWriter.ToString();
        }


        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.Spotlight.Resources.Views.SpotlightWidget.ascx"; }
        }
  
    }
}
