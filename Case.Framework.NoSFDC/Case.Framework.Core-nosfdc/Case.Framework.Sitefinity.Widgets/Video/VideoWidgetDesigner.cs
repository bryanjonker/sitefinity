using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Web.UI;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Video
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Video.VideoWidget"/> widget
    /// </summary>
    public class VideoWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return VideoWidgetDesigner.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion


        
        #region Control references
        /// <summary>
        /// Gets the control that is bound to the YouTubeVideoUrl property
        /// </summary>
        protected virtual Control YouTubeVideoUrl
        {
            get
            {
                return this.Container.GetControl<Control>("YouTubeVideoUrl", true);
            }
        }

        protected virtual Control KalturaSnippet
        {
            get
            {
                return this.Container.GetControl<Control>("KalturaSnippet", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting VideoGuid.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonVideoGuid
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonVideoGuid", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting VideoGuid.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonVideoGuid
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonVideoGuid", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the VideoGuid property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorVideoGuid
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorVideoGuid", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the VideoTitle property
        /// </summary>
        protected virtual Control VideoTitle
        {
            get
            {
                return this.Container.GetControl<Control>("VideoTitle", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the VideoDescription property
        /// </summary>
        protected virtual Telerik.Sitefinity.Web.UI.Fields.HtmlField VideoDescription
        {
            get
            {
                return this.Container.GetControl<Telerik.Sitefinity.Web.UI.Fields.HtmlField>("VideoDescription", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the LightboxDescription property
        /// </summary>
        protected virtual Telerik.Sitefinity.Web.UI.Fields.HtmlField LightboxDescription
        {
            get
            {
                return this.Container.GetControl<Telerik.Sitefinity.Web.UI.Fields.HtmlField>("LightboxDescription", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the BottonLinkTitle property
        /// </summary>
        protected virtual Control BottonLinkTitle
        {
            get
            {
                return this.Container.GetControl<Control>("BottonLinkTitle", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the BottonLinkUrl property
        /// </summary>
        protected virtual Control BottonLinkUrl
        {
            get
            {
                return this.Container.GetControl<Control>("BottonLinkUrl", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the AboveVideoLinkText property
        /// </summary>
        protected virtual Control AboveVideoLinkText
        {
            get
            {
                return this.Container.GetControl<Control>("AboveVideoLinkText", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the AboveVideoLinkUrl property
        /// </summary>
        protected virtual Control AboveVideoLinkUrl
        {
            get
            {
                return this.Container.GetControl<Control>("AboveVideoLinkUrl", true);
            }
        }

        protected virtual RadComboBox rdViewMode
        {
            get
            {
                return this.Container.GetControl<RadComboBox>("rdViewMode", true);
            }
        }

        protected virtual RadComboBox rdVideoPosition
        {
            get
            {
                return this.Container.GetControl<RadComboBox>("rdVideoPosition", true);
            }
        }


        /// <summary>
        /// The LinkButton for selecting ImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting ImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the ImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorImageId", false);
            }
        }
        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            this.Width = 700;
            this.Height = 800;

            //this.
            List<RadComboBoxItem> items = new List<RadComboBoxItem>();

            items.Add(new RadComboBoxItem { Text = "General Layout", Value = "0" });
            items.Add(new RadComboBoxItem { Text = "Lightbox Layout (Research & Outreach)", Value = "1" });


            rdViewMode.DataTextField = "Text";
            rdViewMode.DataValueField = "Value";
            rdViewMode.DataSource = items;
            rdViewMode.DataBind();

            items.Clear();
            items.Add(new RadComboBoxItem { Text = "Left", Value = "0",Checked = true });
            items.Add(new RadComboBoxItem { Text = "Right", Value = "1" });

            rdVideoPosition.DataTextField = "Text";
            rdVideoPosition.DataValueField = "Value";
            rdVideoPosition.DataSource = items;
            rdVideoPosition.DataBind();


        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("youTubeVideoUrl", this.YouTubeVideoUrl.ClientID);
            descriptor.AddElementProperty("kalturaSnippet", this.KalturaSnippet.ClientID);
            descriptor.AddElementProperty("selectButtonVideoGuid", this.SelectButtonVideoGuid.ClientID);
            descriptor.AddElementProperty("deselectButtonVideoGuid", this.DeselectButtonVideoGuid.ClientID);
            descriptor.AddComponentProperty("selectorVideoGuid", this.SelectorVideoGuid.ClientID);
            descriptor.AddElementProperty("videoTitle", this.VideoTitle.ClientID);
            descriptor.AddComponentProperty("videoDescription", this.VideoDescription.ClientID);
            descriptor.AddComponentProperty("lightboxDescription", this.LightboxDescription.ClientID);
            descriptor.AddElementProperty("bottonLinkTitle", this.BottonLinkTitle.ClientID);
            descriptor.AddElementProperty("bottonLinkUrl", this.BottonLinkUrl.ClientID);
            descriptor.AddElementProperty("aboveVideoLinkText", this.AboveVideoLinkText.ClientID);
            descriptor.AddElementProperty("aboveVideoLinkUrl", this.AboveVideoLinkUrl.ClientID);
            descriptor.AddProperty("videoServiceUrl", this.videoServiceUrl);
            descriptor.AddComponentProperty("rdViewMode", this.rdViewMode.ClientID);
            descriptor.AddComponentProperty("rdVideoPosition", this.rdVideoPosition.ClientID);
            descriptor.AddElementProperty("selectButtonImageId", this.SelectButtonImageId.ClientID);
            descriptor.AddElementProperty("deselectButtonImageId", this.DeselectButtonImageId.ClientID);
            descriptor.AddComponentProperty("selectorImageId", this.SelectorImageId.ClientID);
            descriptor.AddProperty("imageServiceUrl", this.imageServiceUrl);
            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(VideoWidgetDesigner.scriptReference, typeof(VideoWidgetDesigner).Assembly.FullName));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Video.Resources.Views.VideoWidgetDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Video.Resources.Javascript.VideoWidgetDesigner.js";
        private string videoServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/VideoService.svc/");
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
        #endregion
    }
}
 
