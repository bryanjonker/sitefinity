Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Video");

Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner = function (element) {
    /* Initialize YouTubeVideoUrl fields */
    this._youTubeVideoUrl = null;
    this._kalturaSnippet = null;
    
    /* Initialize VideoGuid fields */
    this._selectButtonVideoGuid = null;
    this._selectButtonVideoGuidClickDelegate = null;
    this._deselectButtonVideoGuid = null;
    this._deselectButtonVideoGuidClickDelegate = null;
    this._selectorVideoGuidCloseDelegate = null;
    this._selectorVideoGuidUploaderViewFileChangedDelegate = null;
    
    this._VideoGuidDialog = null;
    this._selectorVideoGuid = null;
    this._VideoGuidId = null;
    
    this._rdViewMode = null;

    this._rdVideoPosition = null;

    /* Initialize ImageId fields */
    this._selectButtonImageId = null;
    this._selectButtonImageIdClickDelegate = null;
    this._deselectButtonImageId = null;
    this._deselectButtonImageIdClickDelegate = null;
    this._selectorImageIdCloseDelegate = null;
    this._selectorImageIdUploaderViewFileChangedDelegate = null;

    this._ImageIdDialog = null;
    this._selectorImageId = null;
    this._ImageIdId = null;

    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Initialize VideoTitle fields */
    this._videoTitle = null;
    
    /* Initialize VideoDescription fields */
    this._videoDescription = null;
    
    /* Initialize LightboxDescription fields */
    this._lightboxDescription = null;
    
    /* Initialize BottonLinkTitle fields */
    this._bottonLinkTitle = null;
    
    /* Initialize BottonLinkUrl fields */
    this._bottonLinkUrl = null;
    
    /* Initialize AboveVideoLinkText fields */
    this._aboveVideoLinkText = null;
    
    /* Initialize AboveVideoLinkUrl fields */
    this._aboveVideoLinkUrl = null;
    
    /* Initialize the service url for the vudei thumbnails */
    this.videoServiceUrl = null;

    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner.callBaseMethod(this, 'initialize');

        /* Initialize VideoGuid */
        this._selectButtonVideoGuidClickDelegate = Function.createDelegate(this, this._selectButtonVideoGuidClicked);
        if (this._selectButtonVideoGuid) {
            $addHandler(this._selectButtonVideoGuid, "click", this._selectButtonVideoGuidClickDelegate);
        }

        this._deselectButtonVideoGuidClickDelegate = Function.createDelegate(this, this._deselectButtonVideoGuidClicked);
        if (this._deselectButtonVideoGuid) {
            $addHandler(this._deselectButtonVideoGuid, "click", this._deselectButtonVideoGuidClickDelegate);
        }

        if (this._selectorVideoGuid) {
            this._VideoGuidDialog = jQuery(this._selectorVideoGuid.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorVideoGuidCloseDelegate
            });
        }

       

        jQuery("#rdViewMode").change(function () {            
            jQuery("#default").show();
            debugger;
            if (jQuery(this).val() == "General Layout") {
                jQuery("#general").show();
                jQuery("#lightbox").hide();                
            } else if (jQuery(this).val() == "Lightbox Layout (Research & Outreach)") {
                jQuery("#general").hide();
                jQuery("#lightbox").show();
                
            }
        });

        jQuery("#previewVideoGuid").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorVideoGuidInsertDelegate = Function.createDelegate(this, this._selectorVideoGuidInsertHandler);
        this._selectorVideoGuid.set_customInsertDelegate(this._selectorVideoGuidInsertDelegate);
        $addHandler(this._selectorVideoGuid._cancelLink, "click", this._selectorVideoGuidCloseHandler);
        this._selectorVideoGuidCloseDelegate = Function.createDelegate(this, this._selectorVideoGuidCloseHandler);
        this._selectorVideoGuidUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorVideoGuidUploaderViewFileChangedHandler);

        /* Initialize ImageId */
        this._selectButtonImageIdClickDelegate = Function.createDelegate(this, this._selectButtonImageIdClicked);
        if (this._selectButtonImageId) {
            $addHandler(this._selectButtonImageId, "click", this._selectButtonImageIdClickDelegate);
        }

        this._deselectButtonImageIdClickDelegate = Function.createDelegate(this, this._deselectButtonImageIdClicked);
        if (this._deselectButtonImageId) {
            $addHandler(this._deselectButtonImageId, "click", this._deselectButtonImageIdClickDelegate);
        }

        if (this._selectorImageId) {
            this._ImageIdDialog = jQuery(this._selectorImageId.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorImageIdCloseDelegate
            });
        }

        jQuery("#previewImageId").load(function () {
            dialogBase.resizeToContent();
        });
        debugger;
        if (this.get_selectButtonVideoGuid().innerHTML != null && this.get_selectButtonVideoGuid().innerHTML.indexOf("Change") > -1) {
            jQuery(this.get_youTubeVideoUrl()).attr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).attr("disabled", "disabled");
        }
        else {
            jQuery(this.get_youTubeVideoUrl()).removeAttr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).removeAttr("disabled", "disabled");
        }

        if (jQuery(this.get_youTubeVideoUrl()).val() != null && jQuery(this.get_youTubeVideoUrl()).val() != '') {
            jQuery(this.get_selectorVideoGuid()).attr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).attr("disabled", "disabled");
        } else {
            jQuery(this.get_selectorVideoGuid()).removeAttr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).removeAttr("disabled", "disabled");
        }

        this._selectorImageIdInsertDelegate = Function.createDelegate(this, this._selectorImageIdInsertHandler);
        this._selectorImageId.set_customInsertDelegate(this._selectorImageIdInsertDelegate);
        $addHandler(this._selectorImageId._cancelLink, "click", this._selectorImageIdCloseHandler);
        this._selectorImageIdCloseDelegate = Function.createDelegate(this, this._selectorImageIdCloseHandler);
        this._selectorImageIdUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorImageIdUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner.callBaseMethod(this, 'dispose');

        /* Dispose VideoGuid */
        if (this._selectButtonVideoGuid) {
            $removeHandler(this._selectButtonVideoGuid, "click", this._selectButtonVideoGuidClickDelegate);
        }
        if (this._selectButtonVideoGuidClickDelegate) {
            delete this._selectButtonVideoGuidClickDelegate;
        }
        
        if (this._deselectButtonVideoGuid) {
            $removeHandler(this._deselectButtonVideoGuid, "click", this._deselectButtonVideoGuidClickDelegate);
        }
        if (this._deselectButtonVideoGuidClickDelegate) {
            delete this._deselectButtonVideoGuidClickDelegate;
        }

        $removeHandler(this._selectorVideoGuid._cancelLink, "click", this._selectorVideoGuidCloseHandler);

        if (this._selectorVideoGuidCloseDelegate) {
            delete this._selectorVideoGuidCloseDelegate;
        }

        if (this._selectorVideoGuidUploaderViewFileChangedDelegate) {
            this._selectorVideoGuid._uploaderView.remove_onFileChanged(this._selectorVideoGuidUploaderViewFileChangedDelegate);
            delete this._selectorVideoGuidUploaderViewFileChangedDelegate;
        }

        /* Dispose ImageId */
        if (this._selectButtonImageId) {
            $removeHandler(this._selectButtonImageId, "click", this._selectButtonImageIdClickDelegate);
        }
        if (this._selectButtonImageIdClickDelegate) {
            delete this._selectButtonImageIdClickDelegate;
        }

        if (this._deselectButtonImageId) {
            $removeHandler(this._deselectButtonImageId, "click", this._deselectButtonImageIdClickDelegate);
        }
        if (this._deselectButtonImageIdClickDelegate) {
            delete this._deselectButtonImageIdClickDelegate;
        }

        $removeHandler(this._selectorImageId._cancelLink, "click", this._selectorImageIdCloseHandler);

        if (this._selectorImageIdCloseDelegate) {
            delete this._selectorImageIdCloseDelegate;
        }

        if (this._selectorImageIdUploaderViewFileChangedDelegate) {
            this._selectorImageId._uploaderView.remove_onFileChanged(this._selectorImageIdUploaderViewFileChangedDelegate);
            delete this._selectorImageIdUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI YouTubeVideoUrl */
        jQuery(this.get_youTubeVideoUrl()).val(controlData.YouTubeVideoUrl);

        if (jQuery(this.get_youTubeVideoUrl()).val() != null && jQuery(this.get_youTubeVideoUrl()).val() != '' && this.get_selectButtonVideoGuid().innerHTML.indexOf("Change")>-1) {
            jQuery(this.get_selectorVideoGuid()).attr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).attr("disabled", "disabled");
        } else {
            jQuery(this.get_selectorVideoGuid()).removeAttr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).removeAttr("disabled", "disabled");
        }
        debugger;
        /* RefreshUI KalturaSnippet */
        jQuery(this.get_kalturaSnippet()).val(controlData.KalturaSnippet);

        /* RefreshUI VideoGuid */
        this.get_selectedVideoGuid().innerHTML = controlData.VideoGuid;
        if (controlData.VideoGuid && controlData.VideoGuid != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonVideoGuid().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonVideoGuid()).show()
            var url = this.videoServiceUrl + controlData.VideoGuid + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewVideoGuid").show();
                    jQuery("#previewVideoGuid").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
            jQuery(this.get_youTubeVideoUrl()).val("");
            jQuery(this.get_youTubeVideoUrl()).attr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).attr("disabled", "disabled");
        }
        else {
            jQuery(this.get_deselectButtonVideoGuid()).hide()
            jQuery(this.get_youTubeVideoUrl()).removeAttr("disabled", "disabled");
            jQuery(this.get_kalturaSnippet()).removeAttr("disabled", "disabled");
        }

        /* RefreshUI ImageId */
        this.get_selectedImageId().innerHTML = controlData.ImageId;
        if (controlData.ImageId && controlData.ImageId != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonImageId()).show()
            var url = this.imageServiceUrl + controlData.ImageId + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewImageId").show();
                    jQuery("#previewImageId").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                    
                }
            });
            
        }
        else {
            jQuery(this.get_deselectButtonImageId()).hide()
          
        }

        if (jQuery("#rdViewMode").val() == "General Layout") {
            jQuery("#general").show();
            jQuery("#lightbox").hide();
        } else if (jQuery("#rdViewMode").val() == "Lightbox Layout (Research & Outreach)") {
            jQuery("#general").hide();
            jQuery("#lightbox").show();

        }

        var rdViewMode = this.get_rdViewMode();
        if (controlData.ViewMode != null) {
            rdViewMode.set_value(controlData.ViewMode);
            rdViewMode.set_text(controlData.ViewMode);
        }

        var rdVideoPosition = this.get_rdVideoPosition();
        if (controlData.VideoPosition != null) {
            rdVideoPosition.set_value(controlData.VideoPosition);
            rdVideoPosition.set_text(controlData.VideoPosition);
        }

        /* RefreshUI VideoDescription */
        //jQuery(this.get_videoDescription()).val(controlData.VideoDescription);
        this.get_videoDescription().set_value(controlData.VideoDescription);
        
        /* RefreshUI LightboxDescription */
        //jQuery(this.get_lightboxDescription()).val(controlData.LightboxDescription);
        this.get_lightboxDescription().set_value(controlData.LightboxDescription);

        /* RefreshUI VideoTitle */
        jQuery(this.get_videoTitle()).val(controlData.VideoTitle);

        /* RefreshUI BottonLinkTitle */
        jQuery(this.get_bottonLinkTitle()).val(controlData.BottonLinkTitle);

        /* RefreshUI BottonLinkUrl */
        jQuery(this.get_bottonLinkUrl()).val(controlData.BottonLinkUrl);

        /* RefreshUI AboveVideoLinkText */
        jQuery(this.get_aboveVideoLinkText()).val(controlData.AboveVideoLinkText);

        /* RefreshUI AboveVideoLinkUrl */
        jQuery(this.get_aboveVideoLinkUrl()).val(controlData.AboveVideoLinkUrl);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        var _rdViewModeSelected = this.get_rdViewMode().get_selectedItem();
        if (_rdViewModeSelected) controlData.ViewMode = _rdViewModeSelected.get_text();

        var _rdVideoPositionSelected = this.get_rdVideoPosition().get_selectedItem();
        if (_rdVideoPositionSelected) controlData.VideoPosition = _rdVideoPositionSelected.get_text();       
        
        /* ApplyChanges VideoDescription */
        //controlData.VideoDescription = jQuery(this.get_videoDescription()).val();
        controlData.VideoDescription = this.get_videoDescription().get_value();

        /* ApplyChanges LightboxDescription */
        controlData.LightboxDescription = this.get_lightboxDescription().get_value();// jQuery(this.get_lightboxDescription()).val();

        /* ApplyChanges YouTubeVideoUrl */
        controlData.YouTubeVideoUrl = jQuery(this.get_youTubeVideoUrl()).val();

        controlData.KalturaSnippet = jQuery(this.get_kalturaSnippet()).val();

        /* ApplyChanges VideoGuid */
        controlData.VideoGuid = this.get_selectedVideoGuid().innerHTML;

        /* ApplyChanges ImageId */
        controlData.ImageId = this.get_selectedImageId().innerHTML;

        /* ApplyChanges VideoTitle */
        controlData.VideoTitle = jQuery(this.get_videoTitle()).val();
      
        

        /* ApplyChanges BottonLinkTitle */
        controlData.BottonLinkTitle = jQuery(this.get_bottonLinkTitle()).val();

        /* ApplyChanges BottonLinkUrl */
        controlData.BottonLinkUrl = jQuery(this.get_bottonLinkUrl()).val();

        /* ApplyChanges AboveVideoLinkText */
        controlData.AboveVideoLinkText = jQuery(this.get_aboveVideoLinkText()).val();

        /* ApplyChanges AboveVideoLinkUrl */
        controlData.AboveVideoLinkUrl = jQuery(this.get_aboveVideoLinkUrl()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* VideoGuid event handlers */
    _selectButtonVideoGuidClicked: function (sender, args) {
        this._selectorVideoGuid._uploaderView.add_onFileChanged(this._selectorVideoGuidUploaderViewFileChangedDelegate);
        this._VideoGuidDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._VideoGuidDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorVideoGuid.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorVideoGuid.get_uploaderView().get_settingsPanel()).hide();
        jQuery(this.get_youTubeVideoUrl()).val("");
        jQuery(this.get_youTubeVideoUrl()).attr("disabled", "disabled");
        jQuery(this.get_kalturaSnippet()).attr("disabled", "disabled");
        return false;
    },

    _deselectButtonVideoGuidClicked: function (sender, args) {
        jQuery("#previewVideoGuid").hide();
                    jQuery("#previewVideoGuid").attr("src", "");
        this.get_selectedVideoGuid().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonVideoGuid().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonVideoGuid()).hide()
		dialogBase.resizeToContent();

        jQuery(this.get_youTubeVideoUrl()).removeAttr("disabled");
        jQuery(this.get_kalturaSnippet()).removeAttr("disabled", "disabled");
        return false;
    },

    /* ImageId event handlers */
    _selectButtonImageIdClicked: function (sender, args) {
        this._selectorImageId._uploaderView.add_onFileChanged(this._selectorImageIdUploaderViewFileChangedDelegate);
        this._ImageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._ImageIdDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorImageId.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorImageId.get_uploaderView().get_settingsPanel()).hide();

        jQuery(this.get_youTubeVideoUrl()).attr("disabled", "disabled");
        jQuery(this.get_kalturaSnippet()).attr("disabled", "disabled");
        return false;
    },

    _deselectButtonImageIdClicked: function (sender, args) {
        jQuery("#previewImageId").hide();
        jQuery("#previewImageId").attr("src", "");
        this.get_selectedImageId().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonImageId()).hide()
        dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* VideoGuid private methods */
    _selectorVideoGuidInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._VideoGuidId = selectedItem.Id;
            this.get_selectedVideoGuid().innerHTML = this._VideoGuidId;
            jQuery(this.get_deselectButtonVideoGuid()).show()
            this.get_selectButtonVideoGuid().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewVideoGuid").show();
                    jQuery("#previewVideoGuid").attr("src", selectedItem.SnapshotUrl);
        }
        this._VideoGuidDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorVideoGuidCloseHandler: function () {
        if(this._VideoGuidDialog){
            this._VideoGuidDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        jQuery("#propertyEditor_ctl00_ctl00_ctl00_ctl00_ctl00_YouTubeVideoUrl").removeAttr("disabled");
        dialogBase.resizeToContent();

    },

    _selectorVideoGuidUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* ImageId private methods */
    _selectorImageIdInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._ImageIdId = selectedItem.Id;
            this.get_selectedImageId().innerHTML = this._ImageIdId;
            jQuery(this.get_deselectButtonImageId()).show()
            this.get_selectButtonImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewImageId").show();
            jQuery("#previewImageId").attr("src", selectedItem.ThumbnailUrl);
        }
        this._ImageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorImageIdCloseHandler: function () {
        if(this._ImageIdDialog){
            this._ImageIdDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorImageIdUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* YouTubeVideoUrl properties */
    get_youTubeVideoUrl: function () { return this._youTubeVideoUrl; }, 
    set_youTubeVideoUrl: function (value) { this._youTubeVideoUrl = value; },

    /* KalturaSnippet properties */
    get_kalturaSnippet: function () { return this._kalturaSnippet; },
    set_kalturaSnippet: function (value) { this._kalturaSnippet = value; },

    /* VideoGuid properties */
    get_selectorVideoGuid: function () {
        return this._selectorVideoGuid;
    },
    set_selectorVideoGuid: function (value) {
        this._selectorVideoGuid = value;
    },
    get_selectButtonVideoGuid: function () {
        return this._selectButtonVideoGuid;
    },
    set_selectButtonVideoGuid: function (value) {
        this._selectButtonVideoGuid = value;
    },
    get_deselectButtonVideoGuid: function () {
        return this._deselectButtonVideoGuid;
    },
    set_deselectButtonVideoGuid: function (value) {
        this._deselectButtonVideoGuid = value;
    },
    get_selectedVideoGuid: function () {
        if (this._selectedVideoGuid == null) {
            this._selectedVideoGuid = this.findElement("selectedVideoGuid");
        }
        return this._selectedVideoGuid;
    },

    get_rdViewMode: function () {
        return this._rdViewMode
    },
    set_rdViewMode: function (value) {
        this._rdViewMode = value;
    },

    get_rdVideoPosition: function () {
        return this._rdVideoPosition
    },
    set_rdVideoPosition: function (value) {
        this._rdVideoPosition = value;
    },

    /* ImageId properties */
    get_selectorImageId: function () {
        return this._selectorImageId;
    },
    set_selectorImageId: function (value) {
        this._selectorImageId = value;
    },
    get_selectButtonImageId: function () {
        return this._selectButtonImageId;
    },
    set_selectButtonImageId: function (value) {
        this._selectButtonImageId = value;
    },
    get_deselectButtonImageId: function () {
        return this._deselectButtonImageId;
    },
    set_deselectButtonImageId: function (value) {
        this._deselectButtonImageId = value;
    },
    get_selectedImageId: function () {
        if (this._selectedImageId == null) {
            this._selectedImageId = this.findElement("selectedImageId");
        }
        return this._selectedImageId;
    },

    /* VideoTitle properties */
    get_videoTitle: function () { return this._videoTitle; }, 
    set_videoTitle: function (value) { this._videoTitle = value; },

    /* VideoDescription properties */
    get_videoDescription: function () { return this._videoDescription; }, 
    set_videoDescription: function (value) { this._videoDescription = value; },

    /* LightboxDescription properties */
    get_lightboxDescription: function () { return this._lightboxDescription; }, 
    set_lightboxDescription: function (value) { this._lightboxDescription = value; },

    /* BottonLinkTitle properties */
    get_bottonLinkTitle: function () { return this._bottonLinkTitle; }, 
    set_bottonLinkTitle: function (value) { this._bottonLinkTitle = value; },

    /* BottonLinkUrl properties */
    get_bottonLinkUrl: function () { return this._bottonLinkUrl; }, 
    set_bottonLinkUrl: function (value) { this._bottonLinkUrl = value; },

    /* AboveVideoLinkText properties */
    get_aboveVideoLinkText: function () { return this._aboveVideoLinkText; }, 
    set_aboveVideoLinkText: function (value) { this._aboveVideoLinkText = value; },

    /* AboveVideoLinkUrl properties */
    get_aboveVideoLinkUrl: function () { return this._aboveVideoLinkUrl; }, 
    set_aboveVideoLinkUrl: function (value) { this._aboveVideoLinkUrl = value; }
}

Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
