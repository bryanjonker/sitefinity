﻿<%@ Control Language="C#" %>
<link href="/Theme/css/video-js.css" rel="stylesheet" />
<asp:Literal runat="server" ID="litMessage" />
<%--//general--%>
<asp:Panel runat="server" ID="pnVideoGeneral" ClientIDMode="Static" class="row full">

    <div class="video small row">

        <asp:Panel runat="server" ID="pnContentGeneral">
            <asp:Literal Text="" ID="txtVideotag" runat="server" />

        </asp:Panel>

        <asp:Panel runat="server" ID="pnVideoInfoGeneral" class="column one-third last">
            <div ID="lblVideoTitle" runat="server" class="video-name"></div>
            <p id="aria_info">
                <asp:Literal ID="litVideoDescription" runat="server" />
            </p>
        </asp:Panel>
    </div>
</asp:Panel>
<%--//research--%>
<asp:Panel runat="server" ID="pnVideoLightFull" class=" row full bg-gray" Visible="false">

    <div class="video row">

        <asp:Panel runat="server" ID="pnContentLight">
            <div class="video-title">
                <asp:Literal ID="litLightTitle" runat="server" /></div>

            <asp:Literal ID="litHtmlContent" runat="server" />
            
            <asp:HyperLink class="button orange" runat="server" ID="hlFullList" />

        </asp:Panel>

        <asp:Panel runat="server" ID="pnVideoLight">
            <p>
                <span class="hide-FAE">Video Title</span>
                <asp:HyperLink ID="hlAboveVideoLink" runat="server">
                    <div id="aria_info">
                        <asp:Literal ID="litAboveVideoLinkText" runat="server" />

                    </div>
                </asp:HyperLink>
            </p>

            <asp:Literal ID="litVideoLight" runat="server" />
        </asp:Panel>
    </div>
</asp:Panel>






