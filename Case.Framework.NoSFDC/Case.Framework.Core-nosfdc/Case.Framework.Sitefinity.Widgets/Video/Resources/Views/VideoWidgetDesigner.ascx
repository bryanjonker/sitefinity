<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 800px; overflow: auto; ">

     <div id="ViewMode">
            <ul id="panelBar">
                <li>
                    <div id="lnkOptions">
                        <div id="lnkFieldEvents">
                            <div class="sfExample">
                                
                                <asp:Label runat="server" CssClass="sfTxtLbl">Select the view mode:</asp:Label>
                            </div>
                            <div>
                                <telerik:RadComboBox ID="rdViewMode" runat="server" Width="30%" Skin="Metro"
                                    EmptyMessage="Choose a view..." CssClass="comboWrapper" ClientIDMode="Static">
                                </telerik:RadComboBox>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="sfStep2Options">
        </div>
    </div>

<ol id="default" style="display:none" >        
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="YouTubeVideoUrl"  CssClass="sfTxtLbl">YouTube Video Url</asp:Label>
    <asp:TextBox ID="YouTubeVideoUrl" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    <li>Or</li>
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="KalturaSnippet"  CssClass="sfTxtLbl">Kaltura Embed Code</asp:Label>
    <asp:TextBox ID="KalturaSnippet" runat="server" CssClass="sfTxt" />
    <div class="sfExample">Get the Kaltura Embed Code from the MediaSpace site -- it will start with &lt;iframe</div>
    </li>
    <li>Or</li>
    <li class="sfFormCtrl" >
    <asp:Label runat="server" CssClass="sfTxtLbl">Video</asp:Label>
    <img id="previewVideoGuid" src="" alt="" style="display:none;" />
    <span style="display: none;" class="sfSelectedItem" id="selectedVideoGuid"></span>
    <div>
      <asp:LinkButton ID="selectButtonVideoGuid" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
      </asp:LinkButton>
      <asp:LinkButton ID="deselectButtonVideoGuid" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
      </asp:LinkButton>
    </div>
    <sf:EditorContentManagerDialog runat="server" ID="selectorVideoGuid" DialogMode="Media" HostedInRadWindow="false" BodyCssClass="" />
    <div class="sfExample">Select a video</div>
    </li>
    
    <li class="sfFormCtrl">
         <asp:Label runat="server" AssociatedControlID="VideoTitle" CssClass="sfTxtLbl">Select the video position:</asp:Label>
     <telerik:RadComboBox ID="rdVideoPosition" runat="server" Width="30%" Skin="Metro"
                                     CssClass="comboWrapper" ClientIDMode="Static">
                                </telerik:RadComboBox>
    <div class="sfExample"></div>
    </li>

    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="VideoTitle" CssClass="sfTxtLbl">Video Title</asp:Label>
    <asp:TextBox ID="VideoTitle" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>

    </ol>

    <ol id="general" style="display:none">
        <li class="sfFormCtrl">
        <asp:Label runat="server" AssociatedControlID="VideoDescription" CssClass="sfTxtLbl">Video Description</asp:Label>
       <%-- <asp:TextBox ID="VideoDescription" runat="server" CssClass="sfTxt" />    --%>        
            <sfFields:HtmlField ID="VideoDescription" ClientIDMode="Static"
    runat="server" Height="400" Width="400"
    DisplayMode="Write" ></sfFields:HtmlField>
        <div class="sfExample"></div>
        </li>
    </ol>

    <ol id="lightbox" style="display:none">

        <li class="sfFormCtrl">
    <asp:Label runat="server" CssClass="sfTxtLbl">Background Image*</asp:Label>
    <img id="previewImageId" src="" alt="" style="display:none;" />
    <span style="display: none;" class="sfSelectedItem" id="selectedImageId"></span>
    <div>
      <asp:LinkButton ID="selectButtonImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
      </asp:LinkButton>
      <asp:LinkButton ID="deselectButtonImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
      </asp:LinkButton>
    </div>
    <sf:EditorContentManagerDialog runat="server" ID="selectorImageId" DialogMode="Image" HostedInRadWindow="false" BodyCssClass="" />
    <div class="sfExample"></div>
    </li>
        
    <li class="sfFormCtrl" style="height: 500px">
    <asp:Label runat="server" AssociatedControlID="LightboxDescription" CssClass="sfTxtLbl">Lightbox Video Description</asp:Label>
    <%--<asp:TextBox ID="LightboxDescription" runat="server" CssClass="sfTxt" />--%>
        <sfFields:HtmlField ID="LightboxDescription" ClientIDMode="Static"
    runat="server" Height="400" Width="400"
    DisplayMode="Write" ></sfFields:HtmlField>
    <div class="sfExample"></div>
    </li>
    <li> <br /><br /></li>
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="BottonLinkTitle" CssClass="sfTxtLbl">Button Link Text</asp:Label>
    <asp:TextBox ID="BottonLinkTitle" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="BottonLinkUrl" CssClass="sfTxtLbl">Button Link Url</asp:Label>
    <asp:TextBox ID="BottonLinkUrl" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="AboveVideoLinkText" CssClass="sfTxtLbl">Above Video Link Text</asp:Label>
    <asp:TextBox ID="AboveVideoLinkText" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="AboveVideoLinkUrl" CssClass="sfTxtLbl">Above Video Link Url</asp:Label>
    <asp:TextBox ID="AboveVideoLinkUrl" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
</ol>
</div>
