﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Case.Framework.Sitefinity.Data;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Case.Framework.Sitefinity.Extensions;


namespace Case.Framework.Sitefinity.Widgets.Video
{


    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Video.VideoWidgetDesigner))]
    public class VideoWidget : SimpleView
    {
        #region Properties

        public string YouTubeVideoUrl { get; set; }
        public string KalturaSnippet { get; set; }
        public Guid VideoGuid { get; set; }

        public string ViewMode { get; set; }
        public string VideoPosition { get; set; }

        public string VideoTitle { get; set; }
        public string VideoDescription { get; set; }

        public string LightboxDescription { get; set; }
        public string BottonLinkTitle { get; set; }
        public string BottonLinkUrl { get; set; }

        public string AboveVideoLinkText { get; set; }
        public string AboveVideoLinkUrl { get; set; }

        public Guid ImageId { get; set; }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return VideoWidget.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }
        #endregion

        #region Control References
        protected virtual Literal txtVideotag
        {
            get
            {
                return this.Container.GetControl<Literal>("txtVideotag", true);
            }
        }
        //protected virtual Literal litYoutubeVideoTag
        //{
        //    get
        //    {
        //        return this.Container.GetControl<Literal>("litYoutubeVideoTag", true);
        //    }
        //}
        protected virtual Panel pnVideoGeneral
        {
            get
            {
                return this.Container.GetControl<Panel>("pnVideoGeneral", true);
            }
        }
        //protected virtual HiddenField hdVideoCTA
        //{
        //    get
        //    {
        //        return this.Container.GetControl<HiddenField>("hdVideoCTA", true);
        //    }
        //}
        protected virtual HtmlGenericControl lblVideoTitle
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("lblVideoTitle", true);
            }
        }
        protected virtual Literal litVideoDescription
        {
            get
            {
                return this.Container.GetControl<Literal>("litVideoDescription", true);
            }
        }
        protected virtual Literal litMessage
        {
            get
            {
                return this.Container.GetControl<Literal>("litMessage", true);
            }
        }

        protected virtual Panel pnVideoInfoGeneral
        {
            get
            {
                return this.Container.GetControl<Panel>("pnVideoInfoGeneral", true);
            }
        }

        protected virtual Panel pnVideoLight
        {
            get
            {
                return this.Container.GetControl<Panel>("pnVideoLight", true);
            }
        }

        protected virtual Panel pnVideoLightFull
        {
            get
            {
                return this.Container.GetControl<Panel>("pnVideoLightFull", true);
            }
        }

        protected virtual Panel pnContentLight
        {
            get
            {
                return this.Container.GetControl<Panel>("pnContentLight", true);
            }
        }


        protected virtual Panel pnContentGeneral
        {
            get
            {
                return this.Container.GetControl<Panel>("pnContentGeneral", true);
            }
        }

        //protected virtual PlaceHolder phAuxGeneral
        //{
        //    get
        //    {
        //        return this.Container.GetControl<PlaceHolder>("phAuxGeneral", true);
        //    }
        //}

        //protected virtual PlaceHolder phAuxLight
        //{
        //    get
        //    {
        //        return this.Container.GetControl<PlaceHolder>("phAuxLight", true);
        //    }
        //}

        protected virtual HyperLink hlFullList
        {
            get
            {
                return this.Container.GetControl<HyperLink>("hlFullList", true);
            }
        }

        protected virtual Literal litLightTitle
        {
            get
            {
                return this.Container.GetControl<Literal>("litLightTitle", true);
            }
        }

        protected virtual HyperLink hlAboveVideoLink
        {
            get
            {
                return this.Container.GetControl<HyperLink>("hlAboveVideoLink", true);
            }
        }

        protected virtual Literal litAboveVideoLinkText
        {
            get
            {
                return this.Container.GetControl<Literal>("litAboveVideoLinkText", true);
            }
        }

        protected virtual Literal litVideoLight
        {
            get
            {
                return this.Container.GetControl<Literal>("litVideoLight", true);
            }
        }

        protected virtual Literal litHtmlContent
        {
            get
            {
                return this.Container.GetControl<Literal>("litHtmlContent", true);
            }
        }

        
        #endregion

        #region Methods

        /// <summary>
        /// Initializes the controls.
        /// </summary>
        /// <param name="container"></param>
        /// <remarks>
        /// Initialize your controls in this method. Do not override CreateChildControls method.
        /// </remarks>
        protected override void InitializeControls(GenericContainer container)
        {
        }


        protected override void OnPreRender(EventArgs e)
        {
                base.OnPreRender(e);
                Telerik.Sitefinity.Libraries.Model.Video video = null;
                if (VideoGuid == Guid.Empty && string.IsNullOrWhiteSpace(YouTubeVideoUrl) && string.IsNullOrWhiteSpace(this.KalturaSnippet))
                {
                    if (Page.IsDesignMode())
                    {
                        litMessage.Text = "You need to configure this widget (using the Edit button).";
                }
                }
                else
                {

                    if (ViewMode == "General Layout")
                    {
                        pnVideoGeneral.Visible = true;
                        pnVideoLightFull.Visible = false;

                        if (VideoGuid == Guid.Empty && string.IsNullOrWhiteSpace(this.KalturaSnippet))
                        {
                            if (YouTubeVideoUrl.Contains("youtube"))
                            {
                                txtVideotag.Text = CreateYoutubeTag();
                                ConfigureTitleAndDescriptionGeneralView(string.Empty, string.Empty);
                            }
                            else
                            {
                                txtVideotag.Text = CreateVimeoTag();
                                ConfigureTitleAndDescriptionGeneralView(string.Empty, string.Empty);
                            }
                        }
                        else if (VideoGuid == Guid.Empty)
                        {
                            txtVideotag.Text = this.KalturaSnippet;
                            ConfigureTitleAndDescriptionGeneralView(string.Empty, string.Empty);
                        }
                        else
                        {
                            video = LibrariesManager.GetManager().GetVideo(VideoGuid);
                           // txtVideotag.Text = CreateVideoJsTag(video);
                            txtVideotag.Visible = false;
                            ConfigureTitleAndDescriptionGeneralView(video.Title, video.Description);
                        }


                        pnContentGeneral.CssClass = "column two-third first full-size";

                        if (VideoPosition == "Right")
                        {
                            pnContentGeneral.CssClass = "column two-third first full-size right-video";
                        }
                    }
                    else
                    {
                        pnVideoGeneral.Visible = false;
                        pnVideoLightFull.Visible = true;

                        if (VideoGuid == Guid.Empty && string.IsNullOrWhiteSpace(this.KalturaSnippet))
                        {
                            if (YouTubeVideoUrl.Contains("youtube"))
                            {
                                litVideoLight.Text = CreateYoutubeTag();
                                ConfigureTitleAndDescriptionGeneralView(string.Empty, string.Empty);
                            }
                            else
                            {
                                litVideoLight.Text = CreateVimeoTag();
                                ConfigureTitleAndDescriptionGeneralView(string.Empty, string.Empty);
                            }
                        }
                        else if (VideoGuid == Guid.Empty)
                        {
                            txtVideotag.Text = this.KalturaSnippet;
                            ConfigureTitleAndDescriptionGeneralView(string.Empty, string.Empty);
                        }
                        else
                        {
                            video = LibrariesManager.GetManager().GetVideo(VideoGuid);
                           // litVideoLight.Text = CreateVideoJsTag(video);
                            litVideoLight.Visible = false;
                            ConfigureTitleAndDescriptionLightView(video.Title, video.Description);
                        }

                        if ((!string.IsNullOrWhiteSpace(BottonLinkUrl)) && (!string.IsNullOrWhiteSpace(BottonLinkTitle)))
                        {
                            hlFullList.NavigateUrl = BottonLinkUrl;
                            hlFullList.Text = BottonLinkTitle;
                        }
                        else
                        {
                            hlFullList.Visible = false;
                        }

                        if ((!string.IsNullOrWhiteSpace(AboveVideoLinkUrl)) && (!string.IsNullOrWhiteSpace(AboveVideoLinkText)))
                        {
                            hlAboveVideoLink.NavigateUrl = AboveVideoLinkUrl;
                            litAboveVideoLinkText.Text = AboveVideoLinkText;
                        }
                        else
                        {
                            hlAboveVideoLink.Visible = false;
                        }



                        if (ImageId != Guid.Empty)
                        {
                            var image = new LibrariesManager().GetImage(ImageId);                            
                            pnVideoLight.Style.Add("background", string.Join(string.Empty, "url('", image.Url, "') no-repeat center;"));
                         //   pnVideoLight.Style.Add("background-size", "880px 685px !important;");
                        }
                        else
                        {
                            if (Page.IsDesignMode())
                            {                            
                                litMessage.Text = "Background Image is a required field for Lightbox Layout view";
                            }
                        }


                        pnVideoLight.CssClass = "column two-third bg-img last";
                        pnContentLight.CssClass = "column one-third first";

                        if (VideoPosition == "Left")
                        {
                            pnVideoLight.CssClass = "column two-third bg-img last left-video";
                            pnContentLight.CssClass = "column one-third first left-video";
                        }

                    }
                }
        }


        #endregion

        #region Private members & constants
        private void ConfigureTitleAndDescriptionLightView(string pTitle, string pDesc)
        {
            if (string.IsNullOrWhiteSpace(VideoTitle))
            {
                litLightTitle.Text = pTitle;
            }
            else
            {
                litLightTitle.Text = VideoTitle;
            }

            if (string.IsNullOrWhiteSpace(LightboxDescription))
            {
                litHtmlContent.Text = pDesc;
            }
            else
            {
                litHtmlContent.Text = LightboxDescription;
            }
        }

        private void ConfigureTitleAndDescriptionGeneralView(string pTitle, string pDesc)
        {
            if (string.IsNullOrWhiteSpace(VideoTitle))
            {
                lblVideoTitle.InnerText = pTitle;
            }
            else
            {
                lblVideoTitle.InnerText = VideoTitle;
            }

            if (string.IsNullOrWhiteSpace(VideoDescription))
            {
                var desc = pDesc;

                if (desc.Length > 500)
                {
                    litVideoDescription.Text = string.Concat(desc.Substring(0, 500), "...");
                }
                else
                {
                    litVideoDescription.Text = desc;
                }
            }
            else
            {
                litVideoDescription.Text = VideoDescription;
            }
        }


        private string CreateYoutubeTag()
        {
            var stb = new StringBuilder();

            if (ViewMode != "General Layout")
            {
                string imageUrl = "";
                if (ImageId != Guid.Empty)
                {
                    var image = new LibrariesManager().GetImage(ImageId);
                    imageUrl = image.Url;
                }

                stb.Append("<div class=\"video-container\"><span class=\"iframe-container vjs-default-skin vjs-big-play-centered\" style=\"background-image: url('" + imageUrl + "');\">");
            }
            stb.Append("<embed id='youtubeframe' width=\"");

            AddVideoSizes(stb);
                        
            stb.Append("\" src=\"http://www.youtube.com/embed/");

            stb.Append(GetYoutubeVideoCode(YouTubeVideoUrl));

            stb.Append("?rel=0");

            stb.Append("&\" frameborder=\"0\" allowfullscreen></embed>");

            if (ViewMode != "General Layout")
            {
                stb.Append("</span>");
            }

            return stb.ToString();
        }

        private string CreateVimeoTag()
        {
            var stb = new StringBuilder();

            if (ViewMode != "General Layout")
            {
                stb.Append("<span class=\"youtube_container\">");
            }
            stb.Append("<iframe frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen width=\"");

            AddVideoSizes(stb);

            stb.Append("\" src='https://player.vimeo.com/video/");

            if (YouTubeVideoUrl.LastIndexOf("/") > 0)
            {
                stb.Append(YouTubeVideoUrl.Substring(YouTubeVideoUrl.LastIndexOf("/") + 1));
            }
            else
            {
                stb.Append(YouTubeVideoUrl);
            }

            stb.Append("'></iframe>");
            
            if (ViewMode != "General Layout")
            {
                stb.Append("<div class=\"vjs-big-play-button\" role=\"button\" aria-live=\"polite\" tabindex=\"0\" aria-label=\"play video poster\"></div></div>");
            }

            return stb.ToString();
        }

        private void AddVideoSizes(StringBuilder stb)
        {
            if (ViewMode == "General Layout")
            {
                stb.Append(592);
                stb.Append("\" height=\"");
                stb.Append(333);                
            }
            else
            {
                stb.Append(720);
                stb.Append("\" height=\"");
                stb.Append(388);
            }
        }

        private string GetYoutubeVideoCode(string pVideoUrl)
        {
            if (pVideoUrl.LastIndexOf("v=") > 0)
            {
                return pVideoUrl.Substring(YouTubeVideoUrl.LastIndexOf("v=") + 2);
            }
            else
            {
                return pVideoUrl;
            }
        }

        private void CreateVideoJsTag(Telerik.Sitefinity.Libraries.Model.Video video, HtmlVideo pVideoJs)
        {
            var stbVideoTag = new StringBuilder();
            //stbVideoTag.Append("<video class=\"video-js vjs-default-skin vjs-big-play-centered\" controls preload=\"auto\" ");
            //stbVideoTag.Append("width=\"");

            

            //stbVideoTag.Append("\" poster=\"");
            //stbVideoTag.Append(video.ThumbnailUrl);
            //stbVideoTag.Append("\" data-setup=\"{\"example_option\":true}\">");
            stbVideoTag.Append("<source src=\"");
            stbVideoTag.Append(video.Url);
            stbVideoTag.Append("\" type='video/mp4' />");
            stbVideoTag.Append("<source src=\"");
            stbVideoTag.Append(video.Url);
            stbVideoTag.Append("\" type='video/webm' />");
            stbVideoTag.Append("<source src=\"");
            stbVideoTag.Append(video.Url);
            stbVideoTag.Append("\" type='video/ogg' />");

            //stbVideoTag.Append("</video>");

            pVideoJs.InnerHtml = stbVideoTag.ToString();
            
            if (ViewMode == "General Layout")
            {
                pVideoJs.Attributes.Add("width", "592");
                pVideoJs.Attributes.Add("height", "333");                                
            }
            else
            {
                pVideoJs.Attributes.Add("width", "720");
                pVideoJs.Attributes.Add("height", "388");
            }

            //return stbVideoTag.ToString();
                            
        }

        public string CreateInformationDiv(string pTitle, string pDescription)
        {
            var stb = new StringBuilder();
            stb.Append(@"<div class=""column one-third last"">");

            stb.Append("<h4>");
            stb.AppendFormat("{0}</h4>", pTitle);
            stb.Append(@" <p id=""aria_info"">");
            stb.Append(pDescription);
            stb.Append(" </p>");

            stb.Append("</div>");

            return stb.ToString();
        }


        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Video.Resources.Views.VideoWidget.ascx";
        #endregion
    }
}
