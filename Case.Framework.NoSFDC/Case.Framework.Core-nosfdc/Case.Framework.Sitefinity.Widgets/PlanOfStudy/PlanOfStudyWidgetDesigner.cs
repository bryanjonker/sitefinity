﻿namespace Case.Framework.Sitefinity.Widgets.PlanOfStudy
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;

    using Telerik.Sitefinity.Web.UI;
    using Telerik.Sitefinity.Web.UI.ControlDesign;
    using Telerik.Sitefinity.Web.UI.Fields;

    public class PlanOfStudyWidgetDesigner : ControlDesignerBase
    {
        protected override string LayoutTemplateName => "Case.Framework.Sitefinity.Widgets.PlanOfStudy.Resources.Views.PlanOfStudyWidgetDesigner.ascx";

        protected virtual HtmlField TxtContent => this.Container.GetControl<HtmlField>("txtContent", false);

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var desc = (ScriptControlDescriptor)scriptDescriptors.Last();
            desc.AddComponentProperty("txtContent", this.TxtContent.ClientID);
            return scriptDescriptors.ToArray();
        }

        public override IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var res = new List<System.Web.UI.ScriptReference>(base.GetScriptReferences());
            var assemblyName = this.GetType().Assembly.GetName().ToString();
            res.Add(new ScriptReference("Case.Framework.Sitefinity.Widgets.PlanOfStudy.Resources.Javascript.PlanOfStudyWidgetDesigner.js", assemblyName));
            return res.ToArray();
        }

        protected override void InitializeControls(GenericContainer container)
        {
        }
    }
}