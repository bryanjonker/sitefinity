﻿﻿<%@ Control Language="C#" %>
<div class="accordion singleItem">
    <p><strong><asp:Literal runat="server" ID="ltTitle"></asp:Literal></strong></p>
    <div class="singleItem">
    <table>
        <thead> <tr> <th>Courses</th> <th>Hours</th> </tr> </thead> 
        <tbody>
            <asp:Literal runat="server" ID="ltMain"></asp:Literal>
            <tr><td><b>Total</b></td><td><asp:Literal runat="server" ID="ltTotal"></asp:Literal></td></tr>
        </tbody>
    </table>
    </div>
</div>   