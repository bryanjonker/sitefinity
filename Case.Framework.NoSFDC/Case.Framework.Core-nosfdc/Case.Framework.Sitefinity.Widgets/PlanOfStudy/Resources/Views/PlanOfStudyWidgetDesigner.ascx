﻿<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" TagPrefix="sitefinity" %>
<div class="sfContentViews">
<div class="sfColWrapper sfEqualCols sfModeSelector sfClearfix sfNavDesignerCtrl sfNavDim">
<div class="sfLeftCol">
    <h2 class="sfStep1">Plans Of Study</h2>
    <div class="sfModeSwitcher">
		<a id="aAddTab" class="sfLinkBtn"><strong class="sfLinkBtnIn">Add Section</strong></a>
    </div>
    <div>
        <label style="color: #333;">Title</label>: <input id="txtTitle" type="text" class="sfTxt" value="Typical Plan of Study" />
    </div>
    <ul id="ulAdded" class="sfRadioList RotatorDesignList"> </ul>
    <br />
</div>
<div class="sfRightCol">
    <div class="sfStep2Options">    
        <div id="groupSettingPageSelect">
            <div class="sfExpandableSection">
                <ul class="sfRadioList RotatorDesignList">
                   <li>
                        <label style="color: #333;" id="chosenItem">Title</label>
                        <div style="max-height: 200px; width: 600px; overflow-y: scroll;">
                             <sitefinity:HtmlField ID="txtContent" ClientIDMode="Static" runat="server" DisplayMode="Write" ></sitefinity:HtmlField>
                        </div>                    
                   </li>
                   <li style="width: 575px;">
                        <label style="color: #333;">Hours</label>: <input id="txtHours" type="text" class="sfTxt" style="width: 50px;" />
                        <label style="color: #333;">Order</label>: <input id="txtOrder" type="text" class="sfTxt" style="width: 350px;" />
                   </li>
                </ul>
                <input id="txtId" type="text" class="sfTxt" style="display:none;"/>
            </div>
        </div>    
    </div>
</div>
</div>
</div>

