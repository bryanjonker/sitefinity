﻿Type.registerNamespace("Case.Framework.Sitefinity.Widgets.PlanOfStudy");

var _data = new Array();

Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner = function (element) {
    Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner.initializeBase(this, [element]);
    this._container = null;
    this._txtContent = null;
}

Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner.prototype = {
    initialize: function () {
        $(".sfStep2Options").hide();
        Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner.callBaseMethod(this, 'initialize');
        _container = this;
        $("#aAddTab").click(function () { _container.addTab(); });
        $("#aSaveInformation").click(function () { _container.saveInformation() });
    },

    dispose: function () {
        Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner.callBaseMethod(this, 'dispose');
    },

    refreshUI: function () {
        var controlData = this._propertyEditor.get_control();
        if ($("#txtTitle").val() === "") {
            $("#txtTitle").val(controlData.Title);
        }
        if (_data == null || _data.length === 0) {
            if (controlData.Data != null) {
                _data = controlData.Data.split("|---|");
            }
        }

        var parentContainer = $('#ulAdded');
        parentContainer.html('');

        for (var x = 0; x < _data.length; x++) {
            var li = $('<li id="' + x + '"></li>');
            var label = "";
            if (_data[x].length < 8) {
                label = $('<label style="color: #888;">Line ' + (x + 1) + '</label>');
            } else {
                var div = document.createElement("div");
                div.innerHTML = _data[x];
                var text = div.textContent || div.innerText || "";
                label = $('<label style="color: #888;">' + text.trim().substring(0, 8) + '</label>');
            }
            label.click(_container.selectTab);
            li.append(label);
            parentContainer.append(li);
        }
        dialogBase.resizeToContent();
    },

    applyChanges: function () {
        saveInformation();
        var controlData = this._propertyEditor.get_control();
        controlData.Title = $("#txtTitle").val();
        var finalString = "";
        if (_data == null || _data.length === 0) {
            controlData.Data = "";
        } else {
            for (var x = 0; x < _data.length; x++) {
                if (_data[x] !== "") {
                    if (finalString === "") {
                        finalString = _data[x];
                    } else {
                        finalString = finalString + "|---|" + _data[x];
                    }
                }
            }
            controlData.Data = finalString;
        }
    },

    get_txtContent: function () {
        return this._txtContent;
    },

    set_txtContent: function (value) {
        this._txtContent = value;
    },

    get_controlData: function () {
        return this.get_propertyEditor().get_control();
    },

    get_propertyEditor: function () {
        return this._propertyEditor;
    },

    //Events
    addTab: function () {
        saveInformation();
        $(".sfStep2Options").show();
        _data.push("");
        $('label', $('#ulAdded')).css('fontWeight', 'normal');
        $('label', $('#ulAdded')).css('text-decoration', 'none');
        $find("txtContent").set_value("");
        $("#txtHours").val("");
        $("#txtOrder").val("");
        $("#chosenItem").text("Enter Information for Line " + _data.length);
        $("#txtId").val(_data.length - 1);
        this.refreshUI();
    },

    selectTab: function () {
        saveInformation();
        $(".sfStep2Options").show();
        dialogBase.resizeToContent();
        var item = $(this).parent();
        var index = parseInt(item.attr('id'));
        var data = _data[index].split("||");
        $find("txtContent").set_value(data[0]);
        $("#txtHours").val(data[1]);
        $("#txtOrder").val(data[2]);
        $("#txtId").val(index);
        $("#chosenItem").text("Enter Information for Line " + (index + 1));
    }
}

function saveInformation() {
    if ($("#txtId").val() !== "") {
        var index = parseInt($("#txtId").val());
        if (index != null && index !== "") {
            var information = "";
            if ($("#txtOrder").val() !== "") {
                information = $find("txtContent").get_value() + "||" + $("#txtHours").val() + "||" + $("#txtOrder").val();
            }
            _data[index] = information;
            $find("txtContent").set_value("");
            $("#txtHours").val("");
            $("#txtOrder").val("");
            $("#chosenItem").text("");
            $("#txtId").val("");
            $('label', $('#ulAdded')).css('fontWeight', 'normal');
            $('label', $('#ulAdded')).css('text-decoration', 'none');
        }
    }
}


Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();