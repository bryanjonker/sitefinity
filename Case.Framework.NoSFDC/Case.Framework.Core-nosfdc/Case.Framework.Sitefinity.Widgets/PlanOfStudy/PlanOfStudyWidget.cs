﻿namespace Case.Framework.Sitefinity.Widgets.PlanOfStudy
{
    using System;
    using System.Linq;
    using System.Web.UI.WebControls;

    using Telerik.Sitefinity.Web.UI;

    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.PlanOfStudy.PlanOfStudyWidgetDesigner))]
    public class PlanOfStudyWidget : SimpleView
    {
        public string Data { get; set; } = string.Empty;

        public string Title { get; set; } = string.Empty;

        protected override string LayoutTemplateName => "Case.Framework.Sitefinity.Widgets.PlanOfStudy.Resources.Views.PlanOfStudyWidget.ascx";

        protected virtual Literal MainLiteral => this.Container.GetControl<Literal>("ltMain", true);

        protected virtual Literal TitleLiteral => this.Container.GetControl<Literal>("ltTitle", true);

        protected virtual Literal TotalLiteral => this.Container.GetControl<Literal>("ltTotal", true);

        protected override void InitializeControls(GenericContainer container)
        {
            var total = 0;
            var items = this.Data.Split(new[] { "|---|" }, StringSplitOptions.None);
            foreach (var arrayItems in items.Select(x => x.Split(new[] { "||" }, StringSplitOptions.None)).Where(z => z.Length == 3).OrderBy(y => y[2]))
            {
                this.MainLiteral.Text = this.MainLiteral.Text + $"<tr><td>{arrayItems[0]}</td><td>{arrayItems[1]}</td></tr>";
                int totalSingle;
                if (int.TryParse(arrayItems[1], out totalSingle))
                {
                    total = total + totalSingle;
                }
            }
            this.TotalLiteral.Text = total.ToString();
            this.TitleLiteral.Text = this.Title;
        }
    }
}