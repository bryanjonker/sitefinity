﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Model;
using Telerik.Sitefinity.DynamicModules.Model;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Managers
{
    public class PublicationsManager : DynamicModuleManager<PublicationsManager, PublicationsModel>
    {
        /// <summary>
        /// Creates the CASE instance from the Sitefinity object.
        /// </summary>
        /// <param name="sfContent">Content of the sf.</param>
        /// <returns></returns>
        protected override PublicationsModel CreateInstance(DynamicContent sfContent)
        {
            return new PublicationsModel(sfContent);
        }
    }
}
