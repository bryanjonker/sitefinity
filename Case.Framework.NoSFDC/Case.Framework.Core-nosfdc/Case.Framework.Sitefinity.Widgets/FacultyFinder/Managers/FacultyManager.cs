﻿
namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Case.Framework.Sitefinity.Content.Managers.Abstracts;
    using Case.Framework.Sitefinity.Widgets.FacultyFinder.Model;

    using Telerik.Sitefinity.DynamicModules.Model;

    using UIUC.Custom.DAL.DataAccess.Faculty;

    public class FacultyManager : DynamicModuleManager<FacultyManager, FacultyModel>
    {
        /// <summary>
        /// Creates the CASE instance from the Sitefinity object.
        /// </summary>
        /// <param name="sfContent">Content of the sf.</param>
        /// <returns></returns>
        protected override FacultyModel CreateInstance(DynamicContent sfContent)
        {
            return new FacultyModel(sfContent);
        }

        public IEnumerable<FacultyModel> GetFaculty()
        {
            var exportProcess = new ExportProcess();
            var jobs = exportProcess.GetAllFacultyAppointmentsFacultyProfile();
            return jobs.Where(j => j.Appointments.Any()).Select(x => new FacultyModel
                {
                    IsActive = true,
                    Active = true,
                    Activities = new List<ActivityHonorModel>(),
                    Background = new List<EducationalBackgroundModel>(),
                    BioQuote = x.Quote,
                    Bio = x.Biography,
                    BioResearch = x.Research,
                    BioTeaching = x.Teaching,
                    Courses = new List<CoursesModel>(),
                    UnitCategory = x.Appointment.Unit,
                    Email = x.Email,
                    Education = string.Empty,
                    FName = x.FirstName,
                    Grants = new List<GrantsModel>(),
                    ProfessionalHistory = new List<ProfessionalhistoryModel>(),
                    Id = new Guid(),
                    Image = x.Image,
                    LinkedIN = x.LinkedIn,
                    JobType = x.Appointment.JobType,
                    Job = x.Appointment.Title,
                    Jobs = x.Appointments.Select(ap => new JobInfoModel
                    {
                        Active = true,
                        Building = ap.Building,
                        JobType = ap.JobType,
                        EdUnitsId = ap.UnitId,
                        Hierarchy = ap.Hierarchy,
                        JobTitle = ap.Title
                    }).ToList(),
                    Links = new List<LinksModel>(),
                    LName = x.LastName,
                    Media = new List<MediaModel>(),
                    UnitShortName = x.Appointment.Unit,
                    UnitNames = x.Appointments.Select(ap => ap.Department).ToList(), 
                    Publications = new List<PublicationsModel>(),
                    Photo = x.Image,
                    Slug = x.Username,
                    SubUnits = string.Empty,
                    Title = x.FullName,
                    Twitter = x.Twitter,
                    Username = x.Username,
                    Unit = x.Appointment.Unit,
                    UIN = x.Uin
                });
        }
    }
}
