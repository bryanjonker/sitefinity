﻿<%@ Control Language="C#" Inherits="Case.Framework.Sitefinity.Widgets.Header.HeaderWidget.cs" %>

<div class="column three-quarter right last">
        	<section class="content">

        		<!-- search results -->
        		<div class="column search-results">
                    
        			<!-- PAGINATION -->
        			<div class="row">
        				<p class="pagination display" style="display:none"></p>
        					<div class="main-form pagination" role="form" aria-label="Top Pagination">
							<label for="pagination-top">Show<span class="hide-FAE"> Pagination Top</span></label>
		                	<select id="pagination-top">
		                		<option value="6">6 per page</option>
		                		<option value="18">18 per page</option>
		                		<option value="36" selected="selected">36 per page</option>
                                <option value="500">All faculty</option>
		                	</select>
						</div>
        				<ul id="pagination" class="pagination pager">
        					
        				</ul>
        			</div>
     

                    <asp:Repeater runat="server" ID="lvFaculty">
                        <HeaderTemplate>
                            <ul class="results">
                        </HeaderTemplate>
                        <ItemTemplate>
                               <li><img src="<%# Eval("Image") %>" alt="<%# Eval("FullName") %>" /><a href="/faculty/<%# Eval("Username") %>"><div class="faculty-name"><%# Eval("FullName") %></div></a><div class="faculty-description"><%# Eval("Appointment.Title") %></div><p><%# Eval("Appointment.Department") %></p>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
        			<div class="hr row"></div>

                    <!-- PAGINATION -->
        			<div class="row">
        				<div class="main-form pagination" role="form" aria-label="Bottom Pagination">
							 <label for="pagination-bottom">Show<span class="hide-FAE"> Pagination Bottom</span></label>
		                	<select id="pagination-bottom">
		                		<option value="6">6 per page</option>
		                		<option value="18">18 per page</option>
		                		<option value="36" selected="selected">36 per page</option>
                                <option value="500">All faculty</option>
		                	</select>
						</div>
        				<ul class="pagination pager">
        					
        				</ul>
        			</div>
        		</div>
			</section>
		</div>

		<div class="column one-quarter left first">

        	<!-- search tools -->
				<div class="find search-tools row">
					<div class="row">
						<div class="main-form" role="search" aria-label="Find Faculty">
							<label for="name_kw">Input Name or Keyword</label>
                            <input id="name_kw" name="name_kw" type="text" class="input_text" placeholder="Name or keyword" value="" />

							<label for="find">Find Faculty</label>
                            <input id="find" class="button orange" type="submit" href="#" value="FIND&nbsp;&rsaquo;" />
						</div>
					</div>
					<div class="expand">
		            <span class="expand-this" role="button" aria-label="Expand 1"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
		            	<div class="expand-title">Departments</div>
                        <section id="departments">
                            <asp:Repeater runat="server" ID="lvDepartments">
                                <HeaderTemplate>
                                    <ul class="inputs">
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <li>
                                     <asp:Literal ID="ltCheckbox" runat="server"></asp:Literal>
 <%--                                   <input id="<%# Eval("Name") %>" name="<%# Eval("Name") %>" value="<%# Eval("Name") %>" type="checkbox" />--%>
                                    <label for="<%# Eval("Name") %>"><%# Eval("Name") %></label>
                                </li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>
                            </asp:Repeater>
                        </section>
		            </div>
		           <%-- <div class="expand">
		            	<span role="button" class="expand-this"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
		            	<h4>Areas</h4>
		            	<section>
                            <asp:Repeater runat="server" ID="lvAreas">
                                <HeaderTemplate>
                                    <ul class="inputs">
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <li>
                                    <input id="<%# Eval("Name") %>" name="<%# Eval("Name") %>" value="<%# Eval("Name") %>" type="checkbox" />
                                    <label for="<%# Eval("Name") %>"><%# Eval("Name") %></label>
                                </li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>
                            </asp:Repeater>
		            	</section>
		            </div>--%>
					<div class="expand">
		            	<span class="expand-this" role="button" aria-label="Expand 2"><img src="/theme/img/icons/icon-expand.svg" onerror="this.src=/theme/img/icons/icon-expand.png;" alt="Expand" width="14px" height="14px"></span>
		            	<div class="expand-title">Centers and Initiatives</div>
		            	<section id="Centers">
                            <asp:Repeater runat="server" ID="lvCats">
                                <HeaderTemplate>
                                    <ul class="inputs">
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <li>
                                    <input id="<%# Eval("Name") %>" name="<%# Eval("Name") %>" value="<%# Eval("Name") %>" type="checkbox" />
                                    <label for="<%# Eval("Name") %>"><%# Eval("Name") %></label>
                                </li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>
                            </asp:Repeater>		            
		            	</section>
		            </div>
				</div>
            </div>

<script src="/Theme/js/jquery.tmpl.min.js" type="text/javascript"></script>
<script src="/Theme/js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="/Theme/js/FacultyFinder.js" type="text/javascript"></script>



<script type="text/x-kendo-template" id="template">
<li><img alt="Faculty Image" src="#= Image #" alt="#= Title #" /><a href=""><h2>#= Title #</h2></a><h3>#=Job.JobTitle #</h3><p>#= Unit.UnitName #</p>
</script>
<div class='ajaxoverlay' style="display: none">
    <div class='ajaxloader'></div>
</div>
<asp:Literal ID="ltDept" runat="server"></asp:Literal>