﻿using Case.Framework.Core.Classes;
using Case.Framework.Core.Extensions;
using Case.Framework.Sitefinity.WebApi.Api.Abstracts;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Api
{
    public class FacultyController : BaseDynamicController<FacultyModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FacultyController" /> class.
        /// </summary>
        public FacultyController()
            : base(FacultyFinderManager.FacultyProfiles)
        {

        }

        public override System.Collections.Generic.IEnumerable<FacultyModel> Get()
        {
            return FacultyFinderManager.FacultyProfiles.GetFaculty();
        }

        [HttpGet]
        public MetaData GetPaged(int page, int size)
        {

            var list = FacultyFinderManager.FacultyProfiles.GetFaculty()
                .Select(x => new FacultyItem
                {
                    Job = x.Job,
                    Match = x.Match,
                    Title = x.Title,
                    Unit = x.Unit,
                    UnitUrl = x.UnitUrl,
                    Url = x.Url,
                    Image = x.Image
                }).ToPagedList(page, size);
            return new MetaData
            {
                Faculty = list,
                PagerInfo = new PagingInfo
                {
                    PageCount = list.GetMetaData().PageCount,
                    FirstItemOnPage = list.GetMetaData().FirstItemOnPage,
                    HasNextPage = list.GetMetaData().HasNextPage,
                    HasPreviousPage = list.GetMetaData().HasPreviousPage,
                    IsFirstPage = list.GetMetaData().IsFirstPage,
                    IsLastPage = list.GetMetaData().IsLastPage,
                    LastItemOnPage = list.GetMetaData().LastItemOnPage,
                    PageNumber = list.GetMetaData().PageNumber,
                    PageSize = list.GetMetaData().PageSize,
                    TotalItemCount = list.GetMetaData().TotalItemCount
                }
            };
        }

        [HttpGet]
        public MetaData Search(string value, int? page, int? size)
        {
            
            return GetSearchResult(value, page, size, string.Empty);
        }

        [HttpGet]
        public MetaData SearchFacultyByDepartment(string term, string departments, string centers, int? page, int? size)
        {
            return SearchFaculty(term, departments, centers, page, size);
        }

        [HttpGet]
        public MetaData SearchFaculty(string term, string departments, string centers, int? page, int? size)
        {
           if (!page.HasValue)
                page = 1;

            if (!size.HasValue)
                size = 6;

            List<string> termsSearch = new List<string>();
            List<string> departmentsSearch = new List<string>();
            List<string> centersSearch = new List<string>();
            List<FacultyModel> filteredFaculty = new List<FacultyModel>();

            bool searchDepartment = false;
            bool searchCenters = false;

            if(!term.IsNullOrEmpty())
                termsSearch = term.Split("[-]", 0).ToList();

            if (!departments.IsNullOrEmpty())
            {
                departmentsSearch = departments.Split("[-]", 0).ToList();

                if (departmentsSearch.Count() > 0)
                    searchDepartment = true;
            }

            if (!centers.IsNullOrEmpty())
            {
                centersSearch = centers.Split("[-]", 0).ToList();

                if (centersSearch.Count() > 0)
                    searchCenters = true;
            }


            var faculty = FacultyFinderManager.FacultyProfiles.GetFaculty().Where(x => x.Jobs.Any(y => y.JobType.ToLower().Contains("faculty")));

            if (searchDepartment && searchCenters)
            {
                foreach (var fac in faculty)
                {

                    if (fac.UnitNames.Any(x => departmentsSearch.Any(y => x.ToLower() == y)) && centersSearch.Any(x => fac.UnitCategory.ToLower().Contains(x)))
                    {
                        filteredFaculty.Add(fac);
                    }

                }
            }
            else if (searchDepartment)
            {
                foreach (var fac in faculty)
                {

                    if (fac.UnitNames.Any(x => departmentsSearch.Any(y => x.ToLower() == y)))
                    {
                        filteredFaculty.Add(fac);
                    }

                }
            }
            else if (searchCenters)
            {
                foreach (var fac in faculty)
                {
                    if (centersSearch.Any(x => fac.UnitCategory.ToLower().Contains(x)))
                    {
                        filteredFaculty.Add(fac);
                    }

                }
            }

            List<FacultyModel> tmpFaculty = new List<FacultyModel>();
            var termsSearchCount = termsSearch.Count();

            if (termsSearchCount > 0)
            {
                if (filteredFaculty.Count() <= 0)
                {
                    filteredFaculty = faculty.ToList();
                }

                IEnumerable<FacultyModel> items = null;
                for (int i = 0; i < termsSearchCount; i++)
                {

                    items = filteredFaculty.Where(x =>
                    {
                        var keys = string.Join(", ", x.UnitNames).ToLower();

                        if (keys.Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getSetMatch(termsSearch[i], string.Join(", ", x.UnitNames)));
                            return true;
                        }
                        else if (x.Title.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = x.Title;
                            return true;
                        }
                        else if (x.Unit.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getMatch(termsSearch[i], x.Unit));
                            return true;
                        }
                        else if (x.Job.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getMatch(termsSearch[i], x.Job));
                            return true;
                        }
                        else if (x.SubUnits.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getMatch(termsSearch[i], x.SubUnits));
                            return true;
                        }
                        else if (x.UnitCategory.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getMatch(termsSearch[i], x.UnitCategory));
                            return true;
                        }
                        else if (x.Bio.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getMatch(termsSearch[i], x.Bio));
                            return true;
                        }
                        else if (x.BioResearch.ToLower().Contains(termsSearch[i]))
                        {
                            x.Match = string.Concat(x.Title, ": ", getSetMatch(termsSearch[i], x.BioResearch));
                            return true;
                        }
                        return false;
                    }).ToList();
                }

                tmpFaculty.AddRange(items);

            }
            else
            {
                tmpFaculty.AddRange(filteredFaculty);
            }


            tmpFaculty = tmpFaculty.Distinct().OrderBy(x => x.LName).ToList();
            var list = tmpFaculty.Select(x => new FacultyItem
            {
                Job = x.Job,
                Match = x.Match,
                Title = x.Title,
                Unit = x.Unit,
                UnitUrl = x.UnitUrl,
                Url = x.Url,
                Image = x.Image
            }).ToPagedList(page.Value, size.Value);

            return new MetaData
            {
                Faculty = list,
                PagerInfo = new PagingInfo
                {
                    PageCount = list.GetMetaData().PageCount,
                    FirstItemOnPage = list.GetMetaData().FirstItemOnPage,
                    HasNextPage = list.GetMetaData().HasNextPage,
                    HasPreviousPage = list.GetMetaData().HasPreviousPage,
                    IsFirstPage = list.GetMetaData().IsFirstPage,
                    IsLastPage = list.GetMetaData().IsLastPage,
                    LastItemOnPage = list.GetMetaData().LastItemOnPage,
                    PageNumber = list.GetMetaData().PageNumber,
                    PageSize = list.GetMetaData().PageSize,
                    TotalItemCount = list.GetMetaData().TotalItemCount
                }
            };
        }

        [HttpGet]
        public MetaData SearchByDepartment(string value, int? page, int? size,string department)
        {
            return GetSearchResult(value, page, size,department);
        }
      
        private MetaData GetSearchResult(string value, int? page, int? size, string department)
        {
            if (!page.HasValue)
                page = 1;

            if (!size.HasValue)
                size = 6;

            var search = value.Split("[-]", 0);
            List<FacultyModel> tmpFaculty = new List<FacultyModel>();
            var faculty = FacultyFinderManager.FacultyProfiles.GetFaculty().Where(x => x.Jobs.Any(y => y.JobType.ToLower().Contains("faculty")));

            for (int i = 0; i < search.Length; i++)
            {

                var items = faculty.Where(x =>
                {
                    var keys = string.Join(", ", x.UnitNames).ToLower();

                    if (keys.Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getSetMatch(search[i], string.Join(", ", x.UnitNames)));
                        return true;
                    }
                    else if (x.Title.ToLower().Contains(search[i]))
                    {
                        x.Match = x.Title;
                        return true;
                    }
                    else if (x.Unit.ToLower().Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getMatch(search[i], x.Unit));
                        return true;
                    }
                    else if (x.Job.ToLower().Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getMatch(search[i], x.Job));
                        return true;
                    }
                    else if (x.SubUnits.ToLower().Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getMatch(search[i], x.SubUnits));
                        return true;
                    }
                    else if (x.UnitCategory.ToLower().Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getMatch(search[i], x.UnitCategory));
                        return true;
                    }
                    else if (x.Bio.ToLower().Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getMatch(search[i], x.Bio));
                        return true;
                    }
                    else if (x.BioResearch.ToLower().Contains(search[i]))
                    {
                        x.Match = string.Concat(x.Title, ": ", getSetMatch(search[i], x.BioResearch));
                        return true;
                    }
                    return false;
                });
                if (!string.IsNullOrEmpty(department) && search.Length < 2)
                    if (department.Length > 0)
                        tmpFaculty.AddRange(items.Where(x => x.UnitShortName == department));
                    else
                        tmpFaculty.AddRange(items);
                else tmpFaculty.AddRange(items);

            }
            tmpFaculty = tmpFaculty.OrderBy(x => x.LName).ToList();
            var list = tmpFaculty.Select(x => new FacultyItem
            {
                Job = x.Job,
                Match = x.Match,
                Title = x.Title,
                Unit = x.Unit,
                UnitUrl = x.UnitUrl,
                Url = x.Url,
                Image = x.Image
            }).ToPagedList(page.Value, size.Value);

            return new MetaData
            {
                Faculty = list,
                PagerInfo = new PagingInfo
                {
                    PageCount = list.GetMetaData().PageCount,
                    FirstItemOnPage = list.GetMetaData().FirstItemOnPage,
                    HasNextPage = list.GetMetaData().HasNextPage,
                    HasPreviousPage = list.GetMetaData().HasPreviousPage,
                    IsFirstPage = list.GetMetaData().IsFirstPage,
                    IsLastPage = list.GetMetaData().IsLastPage,
                    LastItemOnPage = list.GetMetaData().LastItemOnPage,
                    PageNumber = list.GetMetaData().PageNumber,
                    PageSize = list.GetMetaData().PageSize,
                    TotalItemCount = list.GetMetaData().TotalItemCount
                }
            };
        }

        private string getMatch(string search, string text)
        {
            string pattern = string.Format(@"\S*{0}+\S*", search);
            Match m = Regex.Match(text, pattern, RegexOptions.IgnoreCase);
            return m.Value;
        }

        private string getSetMatch(string search, string text)
        {
            string pattern = string.Format(@"[^.!?;]*{0}[^.!?;]*", search); //@"([A-Z][^\.!?]*[\.!?])";//string.Format(@"[^.!?;]*{0}[^.!?;]*", search);
            Match m = Regex.Match(text, pattern, RegexOptions.IgnoreCase);
            return m.Value;
        }
    }

    public class MetaData {
        public IPagedList<FacultyItem> Faculty { get; set; }
        public PagingInfo PagerInfo { get; set; }

    }

    public class FacultyItem
    {
        public string Title { get; set; }
        public string UnitUrl { get; set; }
        public string Job { get; set; }
        public string Url { get; set; }
        public string Unit { get; set; }
        public string Match { get; set; }
        public string Image { get; set; }
    }

    public class PagingInfo {

        public int PageCount { get; set; }
        public int TotalItemCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }
        public int FirstItemOnPage { get; set; }
        public int LastItemOnPage { get; set; }
    }

}
