﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class UnitModel : DynamicModel
    {
        public string UnitId { get; set; }
        public string ParentEdUnitId { get; set; }
        public string EdUnitsId { get; set; }
        public string UnitType { get; set; }
        public string UnitName { get; set; }
        public bool isActive { get; set; }
        public int SubUnitOrder { get; set; }
        public string UnitCat { get; set; }
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }
      

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Unit.Unit";
            }
        }
        public UnitModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

        public UnitModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                var subUnitsOrder = sfContent.GetStringSafe("sub_unit_order");
                if (string.IsNullOrEmpty(subUnitsOrder))
                    subUnitsOrder = "0";

                //SET CUSTOM PROPERTIES
                UnitId = sfContent.GetStringSafe("unit_id");
                ParentEdUnitId = sfContent.GetStringSafe("parent_ed_units_id");
                UnitType = sfContent.GetStringSafe("unit_type");
                UnitName = sfContent.GetStringSafe("unit_name");
                isActive = Convert.ToBoolean(sfContent.GetStringSafe("isActive"));
                SubUnitOrder = Convert.ToInt32(subUnitsOrder);
                EdUnitsId = sfContent.GetStringSafe("ed_units_id");
                UnitCat = sfContent.GetStringSafe("category");
  
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES
          
                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }


    }
}
