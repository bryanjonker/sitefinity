﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;


namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class ActivityHonorModel:DynamicModel
    {
        public string Name { get; set; }
        public string OrganizationName { get; set; }
        public string YearStart { get; set; }
        public string YearEnd { get; set; }
        public string Area { get; set; }
        public decimal YearStartSort { get; set; }
        public decimal YearEndSort { get; set; }
        public decimal CustomSort { get; set; }
        public string Username { get; set; }
   
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.ActivitieHonor";
            }
        }

          public ActivityHonorModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

          public ActivityHonorModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                Name = sfContent.GetStringSafe("name");
                OrganizationName = sfContent.GetStringSafe("organization_name");
                YearStart = sfContent.GetStringSafe("year_start");
                YearEnd = sfContent.GetStringSafe("year_end");
                Area = sfContent.GetStringSafe("area");
                if (!string.IsNullOrEmpty(sfContent.GetStringSafe("year_start_sort")))
                    YearStartSort = Convert.ToDecimal(sfContent.GetStringSafe("year_start_sort"));
                else YearStartSort = 0;
                if (!string.IsNullOrEmpty(sfContent.GetStringSafe("year_end_sort")))
                    YearEndSort = Convert.ToDecimal(sfContent.GetStringSafe("year_end_sort"));
                else YearEndSort = 0;
                if (!string.IsNullOrEmpty(sfContent.GetStringSafe("custom_sort")))
                    CustomSort = Convert.ToDecimal(sfContent.GetStringSafe("custom_sort"));
                else CustomSort = 0;
                Username = sfContent.GetStringSafe("username");
              
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES

                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }

    }
}
