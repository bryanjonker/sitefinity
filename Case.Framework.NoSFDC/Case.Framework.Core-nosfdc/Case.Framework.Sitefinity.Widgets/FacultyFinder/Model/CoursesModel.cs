﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;


namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class CoursesModel:DynamicModel
    {
        public string Description { get; set; }
        public string Department { get; set; }
        public string Subject { get; set; }
        public string Number { get; set; }
        public string Format { get; set; }
        public string FormatSearch { get; set; }
        public string SubjectName { get; set; }
        public string InstructorNames { get; set; }
        public bool IsActive { get; set; }
        public string TermsOffered { get; set; }
        public decimal CreditHoursMin { get; set; }
        public decimal CreditHoursMax { get; set; }
        public List<Guid> RelatedItems { get; set; }
    
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.UIUCCourse.Course";
            }
        }

          public CoursesModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

          public CoursesModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                Description = sfContent.GetStringSafe("Description");
                Department = sfContent.GetStringSafe("Department");
                Subject = sfContent.GetStringSafe("Subject");
                Number = sfContent.GetStringSafe("Number");
                Format = sfContent.GetStringSafe("Format");
                FormatSearch = sfContent.GetStringSafe("FormatSearch");
                SubjectName = sfContent.GetStringSafe("Subject_name");
                InstructorNames = sfContent.GetStringSafe("InstructorsNames");
                IsActive = Convert.ToBoolean(sfContent.GetStringSafe("isActive"));
                TermsOffered = sfContent.GetStringSafe("Terms_offered");
                if (!string.IsNullOrEmpty(sfContent.GetStringSafe("Credit_hours_max")))
                    CreditHoursMax = Convert.ToDecimal(sfContent.GetStringSafe("Credit_hours_max"));
                else CreditHoursMax = 0;
                if (!string.IsNullOrEmpty(sfContent.GetStringSafe("Credit_hours_min")))
                    CreditHoursMin = Convert.ToDecimal(sfContent.GetStringSafe("Credit_hours_min"));
                else CreditHoursMin = 0;
                RelatedItems = sfContent.GetRelatedItems("Instructor").Select(x => x.Id).ToList();
              
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES

                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }

    }
}
