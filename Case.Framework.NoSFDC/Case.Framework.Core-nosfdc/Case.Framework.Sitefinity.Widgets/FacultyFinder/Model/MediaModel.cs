﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;


namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class MediaModel : DynamicModel
    {
        public string Link { get; set; }
        public string Description { get; set; }
        public string MediaTitle { get; set; }
        public string Username { get; set; }
        public DateTime Date {get;set;}

        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.Media";
            }
        }

        public MediaModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

        public MediaModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                Link = sfContent.GetStringSafe("link");
                Description = sfContent.GetStringSafe("description");
                MediaTitle = sfContent.GetStringSafe("media_title");
                Date = Convert.ToDateTime(sfContent.GetStringSafe("date"));
                Username = sfContent.GetStringSafe("username");

            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES

                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }

    }
}
