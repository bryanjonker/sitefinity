﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class ProfessionalhistoryModel : DynamicModel
    {


        public string Institution { get; set; }
        public string YearStart { get; set; }
        public string YearEnd { get; set; }
        public string Username { get; set; }
        public string PositionTitle { get; set; }
        public string Department { get; set; }
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.Professionalhistory";
            }
        }

        public ProfessionalhistoryModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

        public ProfessionalhistoryModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                Institution = sfContent.GetStringSafe("institution");
                YearStart = sfContent.GetStringSafe("year_start");
                YearEnd = sfContent.GetStringSafe("year_end");
                PositionTitle = sfContent.GetStringSafe("position_title");
                Department = sfContent.GetStringSafe("department");
                Username = sfContent.GetStringSafe("username");
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES

                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }
    }
}

