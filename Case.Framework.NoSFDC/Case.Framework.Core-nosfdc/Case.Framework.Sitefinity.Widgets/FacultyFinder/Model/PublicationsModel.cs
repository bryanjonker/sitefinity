﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class PublicationsModel : DynamicModel
    {


        public string BibEntry { get; set; }
        public string Username { get; set; }
        public string PubUrl {get;set;}
        public string PubStatus {get;set;}
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.Publications";
            }
        }

        public PublicationsModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

        public PublicationsModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                BibEntry = sfContent.GetStringSafe("bib_entry");
                Username = sfContent.GetStringSafe("username");
                PubUrl = sfContent.GetStringSafe("pubUrl");
                PubStatus = sfContent.GetStringSafe("pub_STATUS");
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES

                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }
    }
}

