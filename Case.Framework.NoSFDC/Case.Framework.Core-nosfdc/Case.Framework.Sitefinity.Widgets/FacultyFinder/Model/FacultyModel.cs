﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class FacultyModel : DynamicModel
    {
        private List<JobInfoModel> _jobs { get; set; }
        private List<CoursesModel> _courses { get; set; }
        private List<GrantsModel> _grants { get; set; }
        private List<ActivityHonorModel> _activities { get; set; }
        private List<ProfessionalhistoryModel> _professionalHistory { get; set; }
        private List<PublicationsModel> _publications { get; set; }
        private List<MediaModel> _media { get; set; }
        private List<LinksModel> _links { get; set; }
        private List<EducationalBackgroundModel> _background { get; set; }
        
        public string Bio { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string UIN { get; set; }
        public string Twitter { get; set; }
        public string LinkedIN { get; set; }
        public string CV_File_Extention { get; set; }
        public string BioResearch { get; set; }
        public string BioTeaching { get; set; }
        public string BioQuote { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public bool IsActive { get; set; }
        public string Username { get; set; }
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }
        public string Image { get; set; }
        public string Job { get; set; }
        public string JobType { get; set; }
        public string Unit { get; set; }
        public string SubUnits { get; set; }
        public string UnitShortName { get; set; }
        public string UnitCategory { get; set; }
        public string Education { get; set; }
        public string Match { get; set; }
        public List<string> UnitNames { get; set; }
        public string UnitUrl
        {
            get
            {
                if (JobType.ToLower().Contains("faculty"))
                    return string.Concat("/faculty/", Slug);
                else return string.Concat("/people/", Slug);
            }
        }

        public string Url
        {
            get
            {
                if (JobType.ToLower().Contains("faculty"))
                    return string.Concat("/faculty/", Slug);
                else return string.Concat("/people/", Slug);
            }
        }
      
        public List<JobInfoModel> Jobs
        {
            get
            {
                if (this._jobs == null)
                    return new List<JobInfoModel>();
                else return _jobs;
            }
            set { _jobs = value; }
        }

        public List<CoursesModel> Courses
        {
            get
            {
                if (this._courses == null)
                    return new List<CoursesModel>();
                else return _courses;
            }
            set {_courses = value;}
        }

        public List<GrantsModel> Grants
        {
            get
            {
                if (this._grants == null)
                    return new List<GrantsModel>();
                else return _grants;
            }
            set { _grants = value; }
        }

        public List<ActivityHonorModel> Activities
        {
            get
            {
                if (this._activities == null)
                    return new List<ActivityHonorModel>();
                else return _activities;
            }
            set { _activities = value; }
        }

        public List<ProfessionalhistoryModel> ProfessionalHistory
        {
            get
            {
                if (this._professionalHistory == null)
                    return new List<ProfessionalhistoryModel>();
                else return _professionalHistory;
            }
            set
            {
                _professionalHistory = value;
            }
        }

        public List<PublicationsModel> Publications
        {
            get
            {
                if (this._publications == null)
                    return new List<PublicationsModel>();
                else return _publications;
            }
            set
            {
                _publications = value;
            }
        }

        public List<MediaModel> Media
        {
            get
            {
                if(this._media == null)
                    return new List<MediaModel>();
                else return _media;
            }

            set{
                _media = value;
            }
        }

        public List<LinksModel> Links
        {
            get
            {
                if (this._links == null)
                    return new List<LinksModel>();
                else return _links;
            }
            set
            {
                _links = value;
            }
        }

        public List<EducationalBackgroundModel> Background
        {
            get
            {
                if (this._background == null)
                    return new List<EducationalBackgroundModel>();
                else return _background;
            }
            set
            {
                _background = value;
            }

        }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.Profile";
            }
        }
        public FacultyModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
            UnitNames = new List<string>();
        }

        public FacultyModel(DynamicContent sfContent)
            : base(sfContent)
        {
            UnitNames = new List<string>();

            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES

                Bio = sfContent.GetStringSafe("bio");
                FName = sfContent.GetStringSafe("fname");
                LName = sfContent.GetStringSafe("lname");
                UIN = sfContent.GetStringSafe("uin");
                Twitter = sfContent.GetStringSafe("twitter");
                LinkedIN = sfContent.GetStringSafe("linkedin");
                CV_File_Extention = sfContent.GetStringSafe("cv_file_extention");
                BioResearch = sfContent.GetStringSafe("bio_research");
                BioTeaching = sfContent.GetStringSafe("bio_teaching");
                BioQuote = sfContent.GetStringSafe("bio_quote");
                Email = sfContent.GetStringSafe("email");
                Photo = sfContent.GetStringSafe("photo");
                IsActive = Convert.ToBoolean(sfContent.GetStringSafe("isActive"));
                Username = sfContent.GetStringSafe("username");
                Categories = sfContent.GetTaxa("Category");
                Tags = sfContent.GetTaxa("Tags");
                Image = SetPhoto();
            }
        }

        public string SetPhoto()
        {
            var imagePath = "/Theme/images/portraits/";
            var mapPath = @"D:\images\portraits\";
            // var mapPath = @"C:\portraits\";
            if (this.Photo.Equals("yes", StringComparison.OrdinalIgnoreCase))
            {
                var file = new FileInfo(mapPath + this.Username + ".jpg");
                if (file.Exists)
                {
                    return imagePath + this.Username + ".jpg";
                }
                else
                {
                    return imagePath + "_unknown.jpg";
                }
            }
            return imagePath + "_unknown.jpg";
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES
          
                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }
    }
}
