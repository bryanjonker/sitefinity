﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GeoLocations.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class JobInfoModel:DynamicModel
    {

        private Address Address { get; set; }

        public string JobId { get; set; }
        public string EdUnitsId { get; set; }
        public string Hierarchy { get; set; }
        public string JobType { get; set; }
        public string JobTitle { get; set; }
        public string Hours { get; set; }
        public string Room { get; set; }
        public string Building { get; set; }
        public string OfficeFax { get; set; }
        public string Phone { get; set; }
        public bool PrimaryJob { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Username { get; set; }
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }
        public string Image { get; set; }


        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.Professionalappointment";
            }
        }
        public JobInfoModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

        public JobInfoModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                JobId = sfContent.GetStringSafe("job_id");
                EdUnitsId = sfContent.GetStringSafe("ed_units_id");
                Hierarchy = sfContent.GetStringSafe("hierarchy");
                JobType = sfContent.GetStringSafe("JOB_TYPE");
                JobTitle = sfContent.GetStringSafe("job_title");
                Hours = sfContent.GetStringSafe("hours");
                Username = sfContent.GetStringSafe("username");
                Room = sfContent.GetStringSafe("room");
                Building = sfContent.GetStringSafe("building");
                OfficeFax = sfContent.GetStringSafe("office_fax");
                Phone = sfContent.GetStringSafe("public_phone");
                PrimaryJob = Convert.ToBoolean(sfContent.GetStringSafe("PRIMARY_JOB"));
                try
                {
                    Address = sfContent.GetValue<Address>("address");
                    Street = Address.Street;
                    City = Address.City;
                    State = Address.StateCode;
                    Zip = Address.Zip;
                }
                catch { }

                
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES
          
                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }
    }
}
