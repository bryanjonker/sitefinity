﻿﻿using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using System;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using System.Linq;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Model
{
    public class GrantsModel : DynamicModel
    {


        public string GrantId { get; set; }
        public string Role { get; set; }
        public string ProposalTitle { get; set; }
        public string OranizationName { get; set; }
        public string PropStatus { get; set; }
        public string AwdBegDate { get; set; }
        public string AwdEndDate { get; set; }
        public string YearStart { get; set; }
        public string YearEnd { get; set; }
        public string Username { get; set; }
        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }

        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.Faculty.Grant";
            }
        }

        public GrantsModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

        public GrantsModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES
                GrantId = sfContent.GetStringSafe("grant_id");
                Role = sfContent.GetStringSafe("role");
                ProposalTitle = sfContent.GetStringSafe("proposal_title");
                OranizationName = sfContent.GetStringSafe("organization_name");
                PropStatus = sfContent.GetStringSafe("prop_status");
                AwdBegDate = sfContent.GetStringSafe("awd_beg_date");
                AwdEndDate = sfContent.GetStringSafe("awd_end_date");
                YearStart = sfContent.GetStringSafe("year_start");
                YearEnd = sfContent.GetStringSafe("year_end");
                Username = sfContent.GetStringSafe("username");
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES

                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }
    }
}

