﻿using Case.Framework.Sitefinity.Widgets.FacultyFinder.Managers;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder.Data
{
    public class FacultyFinderManager
    {
        public static FacultyManager FacultyProfiles { get { return new FacultyManager(); } }
        public static JobInfoManager JobInfo { get { return JobInfoManager.Instance; } }
        public static UnitsManager Units { get { return UnitsManager.Instance; } }
        public static EducationalBackgroundManager EducationalBackgrounds { get { return EducationalBackgroundManager.Instance; } }
        public static CourseManager Courses { get { return CourseManager.Instance; } }
        public static LinksManager Links { get { return LinksManager.Instance; } }
        public static ActivityHonorManager Activities { get { return ActivityHonorManager.Instance; } }
        public static GrantsManager Grants { get { return GrantsManager.Instance; } }
        public static ProfessionalHistoryManager ProfessionalHistory { get { return ProfessionalHistoryManager.Instance; } }
        public static PublicationsManager PublicationManager { get { return PublicationsManager.Instance; } }
        public static MediaManager Media { get { return MediaManager.Instance; } }
    }
}
    