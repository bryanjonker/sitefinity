﻿using Case.Framework.Sitefinity.Widgets.FacultyFinder.Data;
using Case.Framework.Sitefinity.Widgets.FacultyFinder.Model;
using Case.Framework.Core.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;
using Telerik.Web.UI;

namespace Case.Framework.Sitefinity.Widgets.FacultyFinder
{
    using UIUC.Custom.DAL.DataAccess.Faculty;

    public class FacultyFinderWidget : SimpleView
    {
        protected override string LayoutTemplateName
        {
            get
            {
                return FacultyFinderWidget.layoutTemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(FacultyFinderWidget);
            }
        }

        protected virtual Repeater lvFaculty
        {
            get
            {
                return this.Container.GetControl<Repeater>("lvFaculty", true);
            }
        }
        protected virtual Repeater lvDepartments
        {
            get
            {
                return this.Container.GetControl<Repeater>("lvDepartments", true);
            }
        }
        protected virtual Repeater lvAreas
        {
            get
            {
                return this.Container.GetControl<Repeater>("lvAreas", true);
            }
        }
        protected virtual Repeater lvCats
        {
            get
            {
                return this.Container.GetControl<Repeater>("lvCats", true);
            }
        }

        protected virtual Literal ltDept
        {
            get
            {
                return this.Container.GetControl<Literal>("ltDept", true);
            }
        }

        protected virtual StringWriter sWriter { get; set; }
        protected virtual HtmlTextWriter writer { get; set; }

        protected virtual int rowItemCount
        {
            get;
            set;
        }

        public string Department { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            Telerik.Sitefinity.Web.RouteHelper.SetUrlParametersResolved(true);

            this.lvDepartments.ItemDataBound += this.lvDepartments_ItemDataBound;

            var q = HttpContext.Current.Request.QueryString["q"];

            var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
            if (parms != null)
            {
                Department = parms.Last();
            }

            var exportProcess = new ExportProcess();
            var count = exportProcess.CountFacultyAppointments(new List<string>(), new List<string>());
            var faculty = exportProcess.SearchFacultyAppointments(new List<string>(), new List<string>(), 500, 0);

            var cats = GetUnits().Where(x => x.UnitCat != null && x.UnitCat != string.Empty);

            if (faculty != null)
            {
                lvFaculty.DataSource = faculty;
                lvFaculty.DataBind();

                var tmpUnits = GetUnits().Where(x=>x.UnitType == "Department").OrderBy(x => x.UnitName).Select(x => new Unit { Name = x.UnitName, ShortName = x.UnitId });
                List<Unit> units = new List<Unit>();
                foreach (var unit in tmpUnits)
                {
                    if (!units.Any(x => x.Name == unit.Name))
                        if (!String.IsNullOrEmpty(unit.Name))
                            units.Add(unit);
                }

                lvDepartments.DataSource = units.Distinct();
                lvDepartments.DataBind();
            }

            if (cats != null)
            {
                var tmpCats = cats.OrderBy(x => x.UnitCat).Select(x => new Area { Name = x.UnitName });
                List<Area> cts = new List<Area>();
                foreach (var area in tmpCats.Where(x=>x.Name != string.Empty))
                {
                    if (!cts.Any(x => x.Name == area.Name))
                        cts.Add(area);
                }

                lvCats.DataSource = tmpCats.Distinct();
                lvCats.DataBind();

            }
        }

        private void lvDepartments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);

            if (e.Item.DataItem != null)
            {
                var item = ((Unit)e.Item.DataItem);

                var ltCheckbox = (Literal)e.Item.FindControl("ltCheckbox");

                if (ltCheckbox != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, item.Name);
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, item.Name);
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, item.Name);
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");

                    if (!string.IsNullOrEmpty(Department))
                    {
                        if (item.ShortName.ToLower() == Department.ToLower())
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Checked, "true");

                        }

                    }

                    writer.RenderBeginTag(HtmlTextWriterTag.Input);
                    writer.RenderEndTag();
                    ltCheckbox.Text = sWriter.ToString();

                }

            }
        }

     
        protected override void InitializeControls(GenericContainer container)
        {
            var parms = (string[])HttpContext.Current.Request.RequestContext.RouteData.Values["Params"];
            if (parms != null)
            {
                Department = parms.Last();
            }

            if (!string.IsNullOrEmpty(Department))
            {
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                writer.AddAttribute(HtmlTextWriterAttribute.Value, Department);
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "Dep");
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
                writer.RenderBeginTag(HtmlTextWriterTag.Input);
                writer.RenderEndTag();
                ltDept.Text = sWriter.ToString();
            }
        }

        private static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.FacultyFinder.Resources.Views.FacultyFinderWidget.ascx";

        public List<JobInfoModel> GetJobs()
        {
            var jobs = FacultyFinderManager.JobInfo.GetAll().ToList();
            return jobs;
        }

        public List<UnitModel> GetUnits()
        {
            var units = FacultyFinderManager.Units.GetAll().ToList();
            return units;
        }

        public List<EducationalBackgroundModel> GetEducations()
        {
            var educations = FacultyFinderManager.EducationalBackgrounds.GetAll().ToList();
            return educations;
        }

     }

    public class Unit
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    public class Area
    {
        public string Name { get; set; }
    }
}
