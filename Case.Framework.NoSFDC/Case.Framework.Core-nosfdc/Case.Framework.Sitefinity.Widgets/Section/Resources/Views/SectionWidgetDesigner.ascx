﻿<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto;">
    <ol>
        <li class="sfFormCtrl">
            <asp:Label ID="Label1" runat="server" CssClass="sfTxtLbl">Image</asp:Label>
            <img id="previewImageId" src="" alt="" style="display: none;" />
            <span style="display: none;" class="sfSelectedItem" id="selectedImageId"></span>
            <div>
                <asp:LinkButton ID="selectButtonImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
                </asp:LinkButton>
                <asp:LinkButton ID="deselectButtonImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
                </asp:LinkButton>
            </div>
            <sf:EditorContentManagerDialog runat="server" ID="selectorImageId" DialogMode="Image" HostedInRadWindow="false" BodyCssClass="" />
            <div class="sfExample">Select or upload an image</div>
        </li>

     
        <li class="sfFormCtrl">
            <asp:Label ID="Label2" runat="server" AssociatedControlID="Description" CssClass="sfTxtLbl">Description</asp:Label>
            <asp:TextBox ID="Description" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
            <div class="sfExample">Title to be shown for section, to put a sub heading press ENTER after first line of text and then type another line of text.</div>
        </li>

    </ol>
</div>