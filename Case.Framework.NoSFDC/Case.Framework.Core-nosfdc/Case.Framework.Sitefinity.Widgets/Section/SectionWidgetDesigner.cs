﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;
using Telerik.Sitefinity.Web.UI.Fields.Enums;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Web.UI;
using System.Web.UI.HtmlControls;

namespace Case.Framework.Sitefinity.Widgets.Section
{
    class SectionWidgetDesigner : ControlDesignerBase
    {

        /// <summary>
        /// The LinkButton for selecting ImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting ImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the ImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorImageId", false);
            }
        }

        /// <summary>
       

        /// <summary>
        /// Gets the control that is bound to the Description property
        /// </summary>
        protected virtual Control Description
        {
            get
            {
                return this.Container.GetControl<Control>("Description", true);
            }
        }

        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {

        }

        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.Section.Resources.Views.SectionWidgetDesigner.ascx";
            }
        }

        //protected override void OnInit(EventArgs e)
        //{
        //    base.OnInit(e);
        //    HtmlLink link = new HtmlLink();
        //    link.Attributes.Add("href", VirtualPathUtility.ToAppRelative("~/theme/SimpleImageSelector.css"));
        //    link.Attributes.Add("type", "text/css");
        //    link.Attributes.Add("rel", "stylesheet");
        //    this.Page.Header.Controls.Add(link);
        //}

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var desc = (ScriptControlDescriptor)scriptDescriptors.Last();
            desc.AddElementProperty("selectButtonImageId", this.SelectButtonImageId.ClientID);
            desc.AddElementProperty("deselectButtonImageId", this.DeselectButtonImageId.ClientID);
            desc.AddComponentProperty("selectorImageId", this.SelectorImageId.ClientID);
            desc.AddElementProperty("description", this.Description.ClientID);
            desc.AddProperty("imageServiceUrl", this.imageServiceUrl);

            return new[] { desc };
        }
        public override IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var res = new List<System.Web.UI.ScriptReference>(base.GetScriptReferences());
            var assemblyName = this.GetType().Assembly.GetName().ToString();
            res.Add(new ScriptReference("Case.Framework.Sitefinity.Widgets.Section.Resources.Javascript.SectionWidgetDesigner.js", assemblyName));
            return res.ToArray();
        }
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/"); 
    }
}
