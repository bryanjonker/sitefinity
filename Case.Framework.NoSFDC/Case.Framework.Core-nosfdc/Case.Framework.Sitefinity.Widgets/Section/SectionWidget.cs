﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.Section
{
    [RequireScriptManager]
    [ControlDesigner(typeof(SectionWidgetDesigner))]
    public class SectionWidget : SimpleView
    {
        public Guid ImageId { get; set; }
        public string Description { get; set; }

     

        protected virtual Literal ltSection
        {
            get
            {
                return this.Container.GetControl<Literal>("ltSection", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
                //<section class="text_overlay">
                //</section>


            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);
            if (ImageId != Guid.Empty)
            {
                var image = CaseManagers.Images.GetById(ImageId);
                string imgPath = string.Concat(image.Url,image.Extension);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "text_overlay");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "background: url(" + image.Url + ") no-repeat center 0;background-size: cover;");
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "text_overlay");
            }
            writer.RenderBeginTag("section");
            if (!string.IsNullOrEmpty(Description))
            {
                string[] lst = Description.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                if (lst.Count() > 1)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H1);
                    writer.Write(lst[0]);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "subheading");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    writer.Write(lst[1]);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                else if (lst.Count() == 1)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H1);
                    writer.Write(lst[0]);
                    writer.RenderEndTag();
                }
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Lorem Ipsum<em>Lorem Ipsum<em");
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            ltSection.Text = sWriter.ToString();
        
        }


        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.Section.Resources.Views.SectionWidget.ascx"; }
        }
  
    }
}
