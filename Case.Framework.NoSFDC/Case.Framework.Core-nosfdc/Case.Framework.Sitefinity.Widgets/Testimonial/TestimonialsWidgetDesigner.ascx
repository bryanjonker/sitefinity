<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.ControlDesign"
    TagPrefix="designers" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI"
    TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Stefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields"
    TagPrefix="sf" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile JavaScriptLibrary="JQuery" />
    <sitefinity:ResourceFile Name="Telerik.Sitefinity.Resources.Scripts.Kendo.styles.kendo_common_min.css"
        Static="True" />
    <sitefinity:ResourceFile Name="Telerik.Sitefinity.Resources.Scripts.Kendo.kendo.all.min.js" />
      <sitefinity:ResourceFile Name="Case.Framework.Sitefinity.Widgets.Testimonial.Resources.KendoWidgetStyle.css"
        Static="true" AssemblyInfo="Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidget, Case.Framework.Sitefinity.Widgets" />
</sitefinity:ResourceLinks>
<ul class="hidden" id="storage">
    <li class="TemplateDropDownIndex"></li>
    <li class="GridIndex"></li>
</ul>
<div class="sfContentViews">
    <div class="">
        <div id="RotatorOptions">
            <br />
            <ul id="panelBar">
                <li>Testimonials Rotator Configuration
                    <br />
                    <div id="allEcommerceOptions">
                        <div id="productFieldEvents">
                            <div class="sfExample">
                                <span style="font-weight: bold">Select the content type to use for testimonial data: </span>
                            </div>
                            <div>
                                <telerik:RadComboBox ID="RadComboBoxTypeName" runat="server" Width="100%" Skin="Metro"
                                    EmptyMessage="Choose a content type..." CssClass="comboWrapper">
                                </telerik:RadComboBox>
                            </div>
                        </div>
          
                    </div>
                </li>
            </ul>
            <div class="sfStep2Options">
            </div>
        </div>
    </div>
</div>
<!-- Template for the left column of the Grid -->
<script id="gridLeftTemplate" type="text/x-kendo-tmpl">
                <div class="gridLeftColumn">
                    <span>#: Title #</span>
                </div>
</script>
<!-- Template for the dropDown -->
<script id="dropDownTemplate" type="text/x-kendo-template">
        <span>#: data.Name #</span><div class="hidden"><p>#: data.BodyText#</p></div>
</script>
<!-- Template for the products dropDown -->
<script id="dropDownProductTemplate" type="text/x-kendo-template">
        <div class="listItemProduct"><span><img src="#: data.Item.Thumbnail.Url #" alt="#: data.Item.Thumbnail.Title #" height="22px"/>&nbsp&nbsp&nbsp#:data.Item.Title.Value #</span></div>           </script>
