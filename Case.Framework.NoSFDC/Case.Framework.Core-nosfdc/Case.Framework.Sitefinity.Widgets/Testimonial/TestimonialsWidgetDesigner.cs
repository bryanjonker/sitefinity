using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.ComponentModel;
using Telerik.Sitefinity.DynamicModules.Builder.Model;
using Telerik.Sitefinity.DynamicModules.Builder;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Testimonial
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidget"/> widget
    /// </summary>
    public class TestimonialsWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return layoutTemplateName;
            }
        }

       

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        public RadComboBox comboProductName
        {
            get { return Container.GetControl<RadComboBox>("RadComboBoxTypeName", true); }
        }
        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
            this.comboProductName.DataSource = GetModuleTypes();
            this.comboProductName.DataBind();

            var item = new DataMemberInfo
            {
                Name = "Title",
                HeaderText = "Title",
                ColumnTemplate = "<span>{{Title}}</span>",
                IsSearchField = true
            };
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();
            descriptor.AddComponentProperty("comboTypeName", this.comboProductName.ClientID);


            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(TestimonialsWidgetDesigner.scriptReference, typeof(TestimonialsWidgetDesigner).Assembly.FullName));
            return scripts;
        }

        public List<string> GetModuleTypes()
        {
            List<string> types = new List<string>();
            ModuleBuilderManager dynMgr = ModuleBuilderManager.GetManager();
            var modules = dynMgr.GetItems(typeof(DynamicModule), "", "", 0, 0).Cast<DynamicModule>();
            
            foreach (var module in modules)
            {

                types.Add(module.Name);
                
            }
            return types;

        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplateName = "Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.js";
        #endregion
    }
}
 
