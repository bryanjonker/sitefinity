﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Case.Framework.Sitefinity.Extensions;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.DynamicModules.Builder;
using Telerik.Sitefinity.DynamicModules.Builder.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Case.Framework.Core.Utilities;

namespace Case.Framework.Sitefinity.Widgets.Testimonial
{

    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(TestimonialsWidgetDesigner))]
    public class TestimonialsWidget : SimpleView
    {

        private string _fieldTypeName = string.Empty;

        protected override string LayoutTemplateName
        {
            get
            {
                return TemplateName;
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(TestimonialsWidget);
            }
        }

        public string FieldTypeName
        {
            get
            {
                return _fieldTypeName;
            }
            set
            {
                _fieldTypeName = value;
            }
        }
        protected virtual Literal LitQuote
        {
            get
            {
                return Container.GetControl<Literal>("LitQuote", true);
            }

        }


        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);

            // initialize managers
            var dynMgr = ModuleBuilderManager.GetManager();
            var mgr = DynamicModuleManager.GetManager();
            var rnd = new Random();

            // get module chosen
            var module = dynMgr.GetItems(typeof(DynamicModule), "", "", 0, 0).Cast<DynamicModule>().FirstOrDefault(x=>x.Title == _fieldTypeName);

            if (module == null) return;
            // get list of all dynamic item types
            var dynamicTypes = dynMgr.GetItems(typeof(DynamicModuleType), "", "", 0, 0);
            foreach (var typeName in from DynamicModuleType dynamicType in dynamicTypes where module.Id == dynamicType.ParentModuleId where module.Status != DynamicModuleStatus.NotInstalled && module.Status != DynamicModuleStatus.Inactive select TypeResolutionService.ResolveType(dynamicType.GetFullTypeName()))
            {
                var content = mgr.GetDataItems(typeName).Where(i => i.Status == ContentLifecycleStatus.Live).ToList();
                if (!content.Any()) continue;
                var r = rnd.Next(content.Count());
                var item = content[r];
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "img-quote");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "pull");
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.WriteLine(HtmlRemovalHelper.StripTagsRegexCompiled(item.GetStringSafe("Testimonial")));
                writer.RenderEndTag();
                writer.RenderBeginTag("footer");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.WriteLine(item.GetStringSafe("ByLine"));
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.WriteLine(item.GetStringSafe("SubLine"));
                writer.RenderEndTag();

                writer.RenderEndTag();
                writer.RenderEndTag();
                break;
            }
            LitQuote.Text = sWriter.ToString();
        }

     

        private const string TemplateName = "Case.Framework.Sitefinity.Widgets.Testimonial.Resources.TestimonialsWidget.ascx";
    }

}
