Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Testimonial");

var fieldsArray = new Array();
var autoText = "";
var emptyGuid = '00000000-0000-0000-0000-000000000000';
var DropdownIndex = 0;
var host;
var productType;
var productId;

Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner = function (element) {

    Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.initializeBase(this, [element]);
    this._comboTypeName = null;
    this._resizeControlDesignerDelegate = null;
    this._beforeSaveChangesDelegate = null;
}

Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */

    initialize: function () {
        Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.callBaseMethod(this, 'initialize');
        this._pageLoadDelegate = Function.createDelegate(this, this._pageLoadHandler);
        Sys.Application.add_load(this._pageLoadDelegate);

        this._beforeSaveChangesDelegate = Function.createDelegate(this, this._beforeSaveChanges);
        this.get_propertyEditor().add_beforeSaveChanges(this._beforeSaveChangesDelegate);
    },

    dispose: function () {
        Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.callBaseMethod(this, 'dispose');

        if (this._beforeSaveChangesDelegate) {
            this.get_propertyEditor().remove_beforeSaveChanges(this._beforeSaveChangesDelegate);
            delete this._beforeSaveChangesDelegate;
        }
    },

    /* --------------------------------- public methods --------------------------------- */
    // implementation of IControlDesigner: Forces the control to refersh from the control Data
    refreshUI: function () {
        var data = this.get_controlData();
        host = data.Host;
        autoText = data.FormFieldForEvents;

        this.resizeEvents();

        var comboBoxTypeName = this.get_comboTypeName();
        if (data.FieldTypeName != null) {
            comboBoxTypeName.set_value(data.FieldTypeName);
            comboBoxTypeName.set_text(data.FieldTypeName);
        }

        setTimeout("dialogBase.resizeToContent()", 100);
    },

    // implementation of IControlDesigner: forces the designer view to apply the changes on UI to the control Data
    applyChanges: function () {
        // save selected page
        var controlData = this._propertyEditor.get_control();
        var comboTypeNameSelected = this.get_comboTypeName().get_selectedItem();
        if (comboTypeNameSelected) controlData.FieldTypeName = comboTypeNameSelected.get_text();

    },

    /* ------------------------------------- events ------------------------------------------- */
    // add resize events
    resizeEvents: function () {
        this._resizeControlDesignerDelegate = Function.createDelegate(this, this._resizeControlDesigner);

    },

    _pageLoadHandler: function () {

        $("#attendeesSelection, #eventName, #productSection").removeClass("hidden");
        setTimeout("dialogBase.resizeToContent()", 50);

        var onClose = function () {
            undo.show();
        };


        setTimeout("dialogBase.resizeToContent()", 150);
    },

    // validator logic here:
    _beforeSaveChanges: function (sender, args) {
        var errorMessage = "";
        var toCancel = false;
        args.set_cancel(toCancel);
        if (errorMessage != "") alert(errorMessage);
    },

    /* --------------------------------- event handlers --------------------------------- */

    _commandHandler: function (sender, args) {
        this._radBox = sender;
        var jake = args.get_item().get_value();
        jQuery("#LabelRadListBox").text(jake);
    },

    _resizeControlDesigner: function () {
        setTimeout("dialogBase.resizeToContent()", 100);
    },

    _dropDownOnChange: function () {
        $("#storage li.TemplateDropDownIndex").html($("#titles").data("kendoDropDownList").current().index());
    },


    /* --------------------------------- private methods --------------------------------- */


    _getItem: function (target) {
        var panelBar = $("#panelBar").data("kendoPanelBar");
        var rootItem = panelBar.element.children("li").eq(target);
        return rootItem;
    },

    _panelBarOnSelect: function () {
        setTimeout("dialogBase.resizeToContent()", 200);
    },



    /* --------------------------------- properties --------------------------------- */

    // gets the reference to the propertyEditor control
    get_propertyEditor: function () {
        return this._propertyEditor;
    },
    // sets the reference fo the propertyEditor control
    set_propertyEditor: function (value) {
        this._propertyEditor = value;
    },

    get_comboTypeName: function () {
        return this._comboTypeName
    },
    set_comboTypeName: function (value) {
        this._comboTypeName = value;
    }
}


Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Testimonial.TestimonialsWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();