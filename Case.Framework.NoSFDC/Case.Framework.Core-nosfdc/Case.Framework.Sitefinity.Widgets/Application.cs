﻿namespace Case.Framework.Sitefinity.Widgets
{
    using System;

    using Case.Framework.Sitefinity.Models.Abstracts;
    using Case.Framework.Sitefinity.Utilities;
    using Case.Framework.Sitefinity.Widgets.Breadcrumbs.Web.UI.Wdgets.Navigation;

    using Telerik.Sitefinity.Abstractions;
    using Telerik.Sitefinity.Data;
    using Telerik.Sitefinity.Localization;
    using Telerik.Sitefinity.Services;
    using Telerik.Sitefinity.Web.UI;

    public class Application : BaseApplication<Application>
    {
        protected static void OnBootstrapperApplicationStart(object sender, EventArgs e)
        {
        }

        protected static void OnBootstrapperInitialized(object sender, ExecutedEventArgs e)
        {
            if (e.CommandName != "Bootstrapped") return;

            ConfigHelper.RegisterToolboxWidget<LayoutControl>("33% + 33% + 33%", "3 Columns Spotlight", "sfL33_34_33", null, "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.Layouts.3col_spotlight.ascx", "Microsite Layouts", null, ToolboxType.PageLayouts);

            // Use these Widgets

            //ConfigHelper.RegisterToolboxWidget<Telerik.Sitefinity.Modules.Events.Web.UI.EventsView>("Events", "This widget is used to add a event list block or single event item to the page.", null, "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<FindExpertWidget>("Find an Expert", null, "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<Telerik.Sitefinity.Modules.GenericContent.Web.UI.ContentBlock>("General Content Block", "This widget is used to add a content block to the page.", null, "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<SliderWidget>("Image Carousel", "This widget is used to add a image slider to the page", "sfImageViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<Telerik.Sitefinity.Web.UI.PublicControls.ImageControl>("Image without Caption", "This widget is used to add a image to the page", "sfImageViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<InfoWidget>("Intro Paragraph with Orange Button", null, "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<FacultyResourcesWidget>("Gray Box of Links", null, "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<SectionWidget>("Main Image with Text Overlay", "This widget is used to add a section image header to a page", "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<Telerik.Sitefinity.Modules.News.Web.UI.NewsView>("News", "This widget is used to add a news list block or single news item to the page.", null, "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<CTAWidget>("Orange Buttons without icons", "This widget is used to build the orange apply,visit,chat links.", "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<ImageContentWidget>("Listing Content Blocks with Images", null, "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<OrangeButtonsWidget>("Orange Buttons with icons & Header", "This widget is used to build the orange audience buttons w/ icons.", "sfNewsViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<TestimonialsWidget>("Student Testimonial", "This widget is used to add random testimonials w/ image to page", null, "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<DeansBlogLastPost>("Blog Feed Block", null, null, "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<ResearchWidget>("Research Page Header", null, null, "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<BreakthroughTopicsWidget>("Full-Width Story Blocks", null, "sfImageLibraryViewIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<VideoWidget>("Video Feature Block", null, "sfLanguageSelectorIcn", "", "", "Use These Widgets", 1);
            //ConfigHelper.RegisterToolboxWidget<SpotlightWidget>("Small Spotlight", null, "sfLanguageSelectorIcn", "", "", "Use These Widgets", 1);  
   
            //Advanced Widgets
            //ConfigHelper.RegisterToolboxWidget<EducationalCentersWidget>("Centers & Initiatives Carousel", "This widget is used to add the Educational Centers & Initiatives carousel", "sfImageLibraryViewIcn", "", "", "Use These Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<FacetedSearchWidget>("Faceted Search", "This widget is used to add faceted searching to a page.", "sfSearchBoxIcn", "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<ChevronWidget>("Home Page Chevron Display", "This widget is used to build the home page chevron display.", "sfNewsViewIcn", "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<CleanNav>("In This Section Navigation", "This widget is used to add sidebar page navigation", "sfNavigationIcn", "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<NavigationWidget>("Mega Menu", "This widget is used to add a the top of page mega menu", "sfNavigationIcn", "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<Breadcrumbs.Web.UI.Wdgets.Navigation.Breadcrumbs>("Navigation Breadcrumbs", "This widget is used to add navigational breadcrumbs to a page", "sfBreadcrumbIcn", "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<ShareWidget>("Social Share", "This widget is used to add page sharing to the page", "sfPageSharingIcn", "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<HeaderWidget>("Unit Landing Page Header", "This widget is used to add grey background page header", null, "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<FacultyFinderWidget>("Faculty Finder", "This widget is used to add faculty search to the site", null, "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<SubSiteFooterWidget>("Subsite Footer", "This widget is used to add a footer to the subsite template", null, "", "", "Advanced Widgets", 2);
            //ConfigHelper.RegisterToolboxWidget<DiscoverWidget>("Research Gray Search Block", "This widget is used to build the orange audience buttons w/ icons.", "sfNewsViewIcn", "", "", "Advanced Widgets", 2); 
            Res.RegisterResource<BreadcrumbsResources>();
        }

        protected static void OnBootstrapperInitializing(object sender, ExecutingEventArgs e)
        {
   
        }

        protected override void OnPagePreRender(object sender, EventArgs e)
        {
        
        }

        public static void PreInit()
        {
            RegisterStartup();
            SystemManager.ApplicationStart += OnBootstrapperApplicationStart;
            Bootstrapper.Initializing += OnBootstrapperInitializing;
            Bootstrapper.Initialized += OnBootstrapperInitialized;
        }
    }
}
