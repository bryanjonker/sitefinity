﻿namespace Case.Framework.Sitefinity.Widgets.SystemWidget
{
    using global::System.Web.UI;

    public class PageNotFoundWidget : UserControl 
    {
        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.IsDesignMode())
            {
                base.Render(writer);
                this.Response.Status = "404 Not Found";
                this.Response.StatusCode = 404;
            }
        } 
    }
}
