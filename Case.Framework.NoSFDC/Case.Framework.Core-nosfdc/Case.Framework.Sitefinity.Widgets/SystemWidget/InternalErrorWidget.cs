﻿namespace Case.Framework.Sitefinity.Widgets.SystemWidget
{
    using global::System.Web.UI;

    public class InternalErrorWidget : UserControl 
    {
        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.IsDesignMode())
            {
                base.Render(writer);
                this.Response.Status = "500 Internal Error";
                this.Response.StatusCode = 500;
            }
        } 
    }
}
