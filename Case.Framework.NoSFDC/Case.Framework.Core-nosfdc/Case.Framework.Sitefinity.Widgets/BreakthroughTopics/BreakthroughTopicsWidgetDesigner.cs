using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.BreakthroughTopics
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidget"/> widget
    /// </summary>
    public class BreakthroughTopicsWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return BreakthroughTopicsWidgetDesigner.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the control that is bound to the Topic property
        /// </summary>
        protected virtual Control Topic
        {
            get
            {
                return this.Container.GetControl<Control>("Topic", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the Qtditems property
        /// </summary>
        protected virtual Control Qtditems
        {
            get
            {
                return this.Container.GetControl<Control>("Qtditems", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("topic", this.Topic.ClientID);
            descriptor.AddElementProperty("qtditems", this.Qtditems.ClientID);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(BreakthroughTopicsWidgetDesigner.scriptReference, typeof(BreakthroughTopicsWidgetDesigner).Assembly.FullName));
            return scripts;
        }
        #endregion

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.ascx";
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.js";
        #endregion
    }
}
 
