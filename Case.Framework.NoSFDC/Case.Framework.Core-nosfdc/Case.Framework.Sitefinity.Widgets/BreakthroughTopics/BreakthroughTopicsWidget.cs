﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using System.Collections.Generic;

namespace Case.Framework.Sitefinity.Widgets.BreakthroughTopics
{

    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner))]
    public class BreakthroughTopicsWidget : SimpleView
    {
        #region Properties
        /// <summary>
        /// Gets or sets the message that will be displayed in the label.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the layout template's relative or virtual path.
        /// </summary>
        public override string LayoutTemplatePath
        {
            get
            {
                if (string.IsNullOrEmpty(base.LayoutTemplatePath))
                    return BreakthroughTopicsWidget.layoutTemplatePath;
                return base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }
        #endregion

        public string Topic { get; set; }
        public int Qtditems { get; set; }


        #region Control References

        protected virtual Repeater rptTopics
        {
            get
            {
                return this.Container.GetControl<Repeater>("rptTopics", true);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Initializes the controls.
        /// </summary>
        /// <param name="container"></param>
        /// <remarks>
        /// Initialize your controls in this method. Do not override CreateChildControls method.
        /// </remarks>
        protected override void InitializeControls(GenericContainer container)
        {
            var dynamicModuleManager = DynamicModuleManager.GetManager();
            var breakthroughTopicType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.BreakthroughTopics.BreakthroughTopic");

            if (Qtditems < 1)
            {
                Qtditems = 3;
            }

            var topicObject = SearchTopic();

            var btTopics = dynamicModuleManager.GetDataItems(breakthroughTopicType)
                .Where(x => x.GetValue<bool>("Active") && x.Visible && x.Status==Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live);

            if (topicObject != null)
            {
                btTopics = btTopics.Where(x => x.GetValue<IList<Guid>>("topics").Contains(topicObject.Id));
            }

            rptTopics.ItemDataBound += new RepeaterItemEventHandler(rptTopics_ItemDataBound);

            rptTopics.DataSource = btTopics
                .Where(x => x.Visible && x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live).OrderByDescending(x=>x.PublicationDate)
                .Take(Qtditems);
            rptTopics.DataBind();
        }

        private FlatTaxon SearchTopic()
        {
            var topicObject = TaxonomyManager.GetManager()
                   .GetTaxa<FlatTaxon>().Where(t => t.Taxonomy.Name == "topics").ToList();

            if (topicObject == null || topicObject.Count < 1)
            {
                return null;
            }
            else
                if (!string.IsNullOrWhiteSpace(Topic))
                {
                    return topicObject
                        .Where(t => t.Title.ToString().ToLower() == Topic.ToString().ToLower())
                        .FirstOrDefault();
                }
                else
                {    //get random topic
                    return topicObject
                       .OrderBy(x => Guid.NewGuid())
                       .FirstOrDefault();
                }
        }
        #endregion


        protected void rptTopics_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var litTitle = (Literal)e.Item.FindControl("litTitle");
            //var img = (Image)e.Item.FindControl("img");
            var btImage = (Image)e.Item.FindControl("impactImage"); 
            var link = (HyperLink)e.Item.FindControl("link");
            var breakthroughTopicType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.BreakthroughTopics.BreakthroughTopic");
            if (e.Item.DataItem != null)
            {
                var title = ((DynamicContent)(e.Item.DataItem)).GetValue("Title").ToString();
                litTitle.Text = title;
                link.NavigateUrl = ((DynamicContent)(e.Item.DataItem)).GetValue("Link").ToString();

                if (((DynamicContent)(e.Item.DataItem)).GetRelatedItems<Telerik.Sitefinity.Libraries.Model.Image>("Thumbnail").FirstOrDefault() != null)
                {
                    var url = ((DynamicContent)(e.Item.DataItem)).GetRelatedItems<Telerik.Sitefinity.Libraries.Model.Image>("Thumbnail").FirstOrDefault().Url;
                    btImage.ImageUrl = url;
                    btImage.AlternateText = title;
                    btImage.Width = 350;
                    btImage.Height = 227;
                }
            }
        }

        #region Private members & constants
        public static readonly string layoutTemplatePath = "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidget.ascx";
        #endregion
    }
}
