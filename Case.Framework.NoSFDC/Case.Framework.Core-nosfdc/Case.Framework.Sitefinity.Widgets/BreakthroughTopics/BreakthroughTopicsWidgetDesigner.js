Type.registerNamespace("Case.Framework.Sitefinity.Widgets.BreakthroughTopics");

Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner = function (element) {
    /* Initialize Topic fields */
    this._topic = null;
    
    /* Initialize Qtditems fields */
    this._qtditems = null;
    
    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.callBaseMethod(this, 'initialize');
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI Topic */
        jQuery(this.get_topic()).val(controlData.Topic);

        /* RefreshUI Qtditems */
        jQuery(this.get_qtditems()).val(controlData.Qtditems);
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges Topic */
        controlData.Topic = jQuery(this.get_topic()).val();

        /* ApplyChanges Qtditems */
        controlData.Qtditems = jQuery(this.get_qtditems()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* --------------------------------- properties -------------------------------------- */

    /* Topic properties */
    get_topic: function () { return this._topic; }, 
    set_topic: function (value) { this._topic = value; },

    /* Qtditems properties */
    get_qtditems: function () { return this._qtditems; }, 
    set_qtditems: function (value) { this._qtditems = value; }
}

Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.BreakthroughTopics.BreakthroughTopicsWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
