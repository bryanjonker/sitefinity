﻿<%@ Control Language="C#" %>
<asp:Repeater runat="server" ID="rptTopics">
    
    <ItemTemplate>
        <div class="column one-third">        
            <asp:Image runat="server" id="impactImage"></asp:Image>    
            <asp:HyperLink id="link" runat="server">                                
                 <p>
                    <asp:Literal ID="litTitle" runat="server" />
                </p>
            </asp:HyperLink>
        </div>
    </ItemTemplate>
    
</asp:Repeater>

