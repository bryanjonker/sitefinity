﻿Type.registerNamespace("Case.Framework.Sitefinity.Widgets.CourseWidget");

var _data = new Array();

Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner = function (element) {
    this._container = null;

    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        $(".sfStep2Options").hide();
        Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.callBaseMethod(this, 'initialize');
        _container = this;
        $("#aAddTab").click(function () { _container.addTab(); });
        $("#aSaveInformation").click(function () { _container.saveInformation() });
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.callBaseMethod(this, 'dispose');
    },

    /* --------------------------------- public methods ---------------------------------- */
    findElement: function (id) {
        var result = $(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control();
        if ($("#txtTitle").val() === "") {
            $("#txtTitle").val(controlData.Settings.Title);
        }
        if (_data == null || _data.length === 0) {
            if (controlData.Settings.Courses != null) {
                _data = controlData.Settings.Courses.split(";");
            }
        }

        var parentContainer = $('#ulAdded');
        parentContainer.html('');

        for (var x = 0; x < _data.length; x++) {
            var li = $('<li id="' + x + '"></li>');
            var text = "New Line";
            if (_data[x] !== "") {
                text = _data[x].split(".")[0];
            }
            var label = $('<label style="color: #888;">' + text + '</label>');
            label.click(_container.selectTab);
            li.append(label);
            parentContainer.append(li);
        }
        dialogBase.resizeToContent();
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        saveInformation();
        var controlData = this._propertyEditor.get_control();
        controlData.Settings.Title = $("#txtTitle").val();
        var finalString = "";
        if (_data == null || _data.length === 0) {
            controlData.Data = "";
        } else {
            for (var x = 0; x < _data.length; x++) {
                if (_data[x] !== "") {
                    if (finalString === "") {
                        finalString = _data[x];
                    } else {
                        finalString = finalString + "; " + _data[x];
                    }
                }
            }
            controlData.Settings.Courses = finalString;
        }
    },

    get_content: function () {
        return this._content;
    },
    set_content: function (value) {
        this._content = value;
    },

    /* --------------------------------- event handlers ---------------------------------- */
    addTab: function () {
        saveInformation();
        $(".sfStep2Options").show();
        _data.push("");
        $('label', $('#ulAdded')).css('fontWeight', 'normal');
        $('label', $('#ulAdded')).css('text-decoration', 'none');
        $("#txtCourse").val("");
        $("#txtCourseNumber").val("");
        $("#txtSection").val("");
        $("#txtCourseYear").val("");
        $("#txtTerm").val("");
        $("#txtId").val(_data.length - 1);
        this.refreshUI();
    },

    selectTab: function () {
        saveInformation();
        $(".sfStep2Options").show();
        dialogBase.resizeToContent();
        var item = $(this).parent();
        var index = parseInt(item.attr('id'));
        var data = _data[index].split(".");
        var dataSub = data[0].trim().split(" ");
        $("#txtId").val(index);
        $("#txtCourse").val(dataSub[0]);
        $("#txtCourseNumber").val(dataSub[1]);
        if (dataSub.length > 2) {
            $("#txtSection").val(dataSub[2]);
        } else {
            $("#txtSection").val("");
        }
        if (data.length > 1 && data[1] !== "") {
            $("#txtCourseYear").val(data[1].substr(0, data[1].indexOf(" ")));
            $("#txtTerm").val(data[1].substr(data[1].indexOf(" ") + 1));
        } else {
            $("#txtCourseYear").val("");
            $("#txtTerm").val("");
        }
    }

    /* --------------------------------- private methods --------------------------------- */

}

function saveInformation() {
    if ($("#txtId").val() !== "") {
        var index = parseInt($("#txtId").val());
        if (index != null && index !== "") {
            var information = "";
            if ($("#txtCourse").val() !== "" && $("#txtCourseNumber").val() !== "") {
                information = $("#txtCourse").val() + " " + $("#txtCourseNumber").val() + " " + $("#txtSection").val() + "." + $("#txtCourseYear").val() + " " + $("#txtTerm").val();
            }
            _data[index] = information;
            $("#txtCourse").val("");
            $("#txtCourseNumber").val("");
            $("#txtSection").val("");
            $("#txtCourseYear").val("");
            $("#txtTerm").val("");
            $("#txtId").val("");
        }
    }
}

Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
