﻿namespace Case.Framework.Sitefinity.Widgets.CourseWidget
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;

    using Telerik.Sitefinity.Modules.Pages;
    using Telerik.Sitefinity.Web.UI;
    using Telerik.Sitefinity.Web.UI.ControlDesign;

    public class CourseWidgetDesigner : ControlDesignerBase
    {
        public const string ScriptReference = "Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.js";

        protected override void InitializeControls(GenericContainer container)
        {
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences())
                              {
                                  new ScriptReference(ScriptReference, typeof(CourseWidgetDesigner).Assembly.FullName)
                              };
            return scripts;
        }

        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            return scriptDescriptors;
        }

        protected override string LayoutTemplateName => "Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.ascx";

        public override string LayoutTemplatePath
        {
            get
            {
                return string.IsNullOrEmpty(base.LayoutTemplatePath) ? "~/CaseFrameworkSitefinityWidgets/Case.Framework.Sitefinity.Widgets.CourseWidget.CourseWidgetDesigner.ascx" : base.LayoutTemplatePath;
            }
            set
            {
                base.LayoutTemplatePath = value;
            }
        }

        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
    }
}