<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>


<div class="sfContentViews">
<div class="sfColWrapper sfEqualCols sfModeSelector sfClearfix sfNavDesignerCtrl sfNavDim">
<div id="RotatorDesignChoice" class="sfLeftCol">
    <h2 class="sfStep1">Add Courses / Title</h2>
    <br />
  <div class="sfFormCtrl">
            Title<br />
            <input id="txtTitle" type="text" class="sfTxt" style="width:150px;" />
   </div>   
    <div class="sfModeSwitcher">
		<a id="aAddTab" class="sfLinkBtn"><strong class="sfLinkBtnIn">Add Section</strong></a>
    </div>
    <br />
   <div class="sfExample">
        <span style="font-weight: bold">Buttons: </span>
   </div>  
    <ul id="ulAdded" class="sfRadioList RotatorDesignList"> </ul>
    <br />
</div>
<div class="sfRightCol">
    <h2 class="sfStep2">Enter Course Information.</h2>
    <div id="divStep2" class="sfStep2Options" style="display: none;">    
        <div id="groupSettingPageSelect">
            <div class="sfExpandableSection">
                <ul class="sfRadioList RotatorDesignList">
                    <li>
                        <label style="color: #333;">Course Rubric</label>
                        <br />
                        <input id="txtCourse" type="text" class="sfTxt" />
                    </li>
                    <li>
                        <label style="color: #333;">Course Number</label>
                        <br />
                        <input id="txtCourseNumber" type="text" class="sfTxt" />
                    </li>
                    <li>
                        <label style="color: #333;">Section</label>
                        <br />
                        <input id="txtSection" type="text" class="sfTxt" />
                    </li>
                    <li>
                        <label style="color: #333;">Course Year</label>
                        <br />
                        <input id="txtCourseYear" type="text" class="sfTxt" />
                    </li>
                    <li>
                        <label style="color: #333;">Term</label>
                        <br />
                        <select id="txtTerm" class="sfTxt">
                            <option value="">-- Choose one --</option>
                            <option value="Fall">Fall</option>
                            <option value="Fall 1">Fall 1</option>
                            <option value="Fall 2">Fall 2</option>
                            <option value="Spring">Spring</option>
                            <option value="Spring 1">Spring 1</option>
                            <option value="Spring 2">Spring 2</option>
                            <option value="Summer">Summer</option>
                            <option value="Summer 1">Summer 1</option>
                            <option value="Summer 2">Summer 2</option>
                            
                        </select>
                    </li>
                </ul>
                <input id="txtId" type="text" class="sfTxt" style="display:none;"/>
                <br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        </div>    
    </div>
</div>
</div>
</div>

