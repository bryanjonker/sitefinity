﻿﻿using Case.Framework.Sitefinity.Extensions;
using Case.Framework.Sitefinity.Models;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;

namespace Case.Framework.Sitefinity.Widgets.SubSiteFooter.Model
{
    public class SubSiteModel:DynamicModel
    {
        public string UnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitShortName { get; set; }
        public string UnitAddress1 { get; set; }
        public string UnitAddress2 { get; set; }
        public string UnitAddress3 { get; set; }
        public string UnitFax { get; set; }
        public string UnitPhone { get; set; }
        public string UnitEmail { get; set; }
        public string UnitType { get; set; }
        public string UnitLatLong { get; set; }
        public PageNode UnitUrl { get; set; }

        public List<TaxonModel> Categories { get; set; }
        public List<TaxonModel> Tags { get; set; }


        /// <summary>
        /// Gets the name of the dynamic type.
        /// </summary>
        /// <value>
        /// The name of the dynamic type.
        /// </value>
        public override string MappedType
        {
            get
            {
                return "Telerik.Sitefinity.DynamicTypes.Model.SubSites.SubSite";
            }
        }
        public SubSiteModel()
            : base()
        {
            Categories = new List<TaxonModel>();
            Tags = new List<TaxonModel>();
        }

           public SubSiteModel(DynamicContent sfContent)
            : base(sfContent)
        {
            if (sfContent != null)
            {
                //SET CUSTOM PROPERTIES

                Title = sfContent.GetStringSafe("Title");
                UnitId = sfContent.GetStringSafe("UnitID");
                UnitName = sfContent.GetStringSafe("UnitName");
                UnitShortName = sfContent.GetStringSafe("UnitShortName");
                UnitAddress1 = sfContent.GetStringSafe("UnitAddress1");
                UnitAddress2 = sfContent.GetStringSafe("UnitAddress2");
                UnitAddress3 = sfContent.GetStringSafe("UnitAddress3");
                UnitFax = sfContent.GetStringSafe("UnitFax");
                UnitPhone = sfContent.GetStringSafe("UnitPhone");
                UnitEmail = sfContent.GetStringSafe("UnitEmail");
                UnitType = sfContent.GetStringSafe("UnitType");
                UnitLatLong = sfContent.GetStringSafe("UnitLatLong");
                UnitUrl = sfContent.GetLinkedPage("UnitURL");
                Categories = sfContent.GetTaxa("Category");
                Tags = sfContent.GetTaxa("Tags");
            }
        }

        public override DynamicContent ToSitefinityModel()
        {
            //GET CONTRUCTED CONTENT FROM BASE
            var sfContent = base.ToSitefinityModel();

            //POPULATE MORE FIELDS IF APPLICABLE
            if (sfContent != null)
            {
                //MERGE CUSTOM PROPERTIES
                sfContent.TrySetValue("Title", Title);
                sfContent.TrySetValue("UnitID", UnitId);
                sfContent.TrySetValue("UnitName", UnitName);
                sfContent.TrySetValue("UnitShortName", UnitShortName);
                sfContent.TrySetValue("UnitAddress1", UnitAddress1);
                sfContent.TrySetValue("UnitAddress2", UnitAddress2);
                sfContent.TrySetValue("UnitAddress3", UnitAddress3);
                sfContent.TrySetValue("UnitFax", UnitFax);
                sfContent.TrySetValue("UnitPhone", UnitPhone);
                sfContent.TrySetValue("UnitEmail", UnitEmail);
                sfContent.TrySetValue("UnitType", UnitType);
                sfContent.TrySetValue("UnitLatLong", UnitLatLong);
                sfContent.SetTaxa("Category", Categories);
                sfContent.SetTaxa("Tags", Tags);
            }

            //RETURN SITEFINITY MODEL
            return sfContent;
        }
    }
}
