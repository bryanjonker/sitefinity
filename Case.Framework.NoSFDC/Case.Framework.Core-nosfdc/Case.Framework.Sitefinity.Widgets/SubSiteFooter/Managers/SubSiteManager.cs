﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Content.Managers.Abstracts;
using Case.Framework.Sitefinity.Widgets.SubSiteFooter.Model;
using Telerik.Sitefinity.DynamicModules.Model;


namespace Case.Framework.Sitefinity.Widgets.SubSiteFooter.Managers
{
    public class SubSiteManager : DynamicModuleManager<SubSiteManager, SubSiteModel>
    {
        /// <summary>
        /// Creates the CASE instance from the Sitefinity object.
        /// </summary>
        /// <param name="sfContent">Content of the sf.</param>
        /// <returns></returns>
        protected override SubSiteModel CreateInstance(DynamicContent sfContent)
        {
            return new SubSiteModel(sfContent);
        }
    }
}
