﻿namespace Case.Framework.Sitefinity.Widgets.SubSiteFooter
{
    using System.Linq;
    using System.Web;

    using Case.Framework.Sitefinity.Widgets.SubSiteFooter.Model;

    using Telerik.Sitefinity.Data.Linq.Dynamic;
    using Telerik.Sitefinity.DynamicModules;
    using Telerik.Sitefinity.Utilities.TypeConverters;

    internal static class SubsiteLogic
    {
        public static SubSiteModel GetSubsite(SiteMapNode currentNode)
        {
            if (currentNode == null)
            {
                return null;
            }
            var moduleManager = DynamicModuleManager.GetManager(string.Empty);
            var subsiteType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.SubSites.Subsite");
            foreach (var title in currentNode.Url.Split('/').Reverse().Where(t => !string.IsNullOrWhiteSpace(t) && t != "~"))
            {
                var subsite = moduleManager.GetDataItems(subsiteType).Where(string.Format("(UnitID = \"{0}\" OR UnitShortName = \"{0}\")", title)).Where(s => (int)s.Status == 2);
                if (subsite.Any())
                {
                    return new SubSiteModel(subsite.ToList()[0]);
                }
            }
            return null;
        }
    }
}
