﻿namespace Case.Framework.Sitefinity.Widgets.SubSiteFooter
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Case.Framework.Sitefinity.Widgets.SubSiteFooter.Model;
    using Telerik.Sitefinity.Data.Linq.Dynamic;
    using Telerik.Sitefinity.DynamicModules;
    using Telerik.Sitefinity.Utilities.TypeConverters;
    using Telerik.Sitefinity.Web.UI;

    public class SubSiteFooterWidget : SimpleView
    {
        protected virtual Literal Footer
        {
            get
            {
                return this.Container.GetControl<Literal>("ltFooter", true);
            }
        }

        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.SubSiteFooter.Resources.Views.SubSiteFooterWidget.ascx";
            }
        }

        protected override Type ResourcesAssemblyInfo
        {
            get
            {
                return typeof(SubSiteFooterWidget);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            var node = SubsiteLogic.GetSubsite(SiteMap.CurrentNode);
            if (node == null)
            {
                return;
            }
            this.RenderHtml(node);
        }

        protected void RenderHtml(SubSiteModel subsite)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "bold");
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(subsite.Title);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(subsite.UnitAddress1 + ",&nbsp;" + subsite.UnitAddress2 + ",&nbsp;" + subsite.UnitAddress3);
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "orange_text");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "tel:" + new string(subsite.UnitPhone.Where(char.IsDigit).ToArray()));
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(subsite.UnitPhone);
            writer.RenderEndTag();
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "divider");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write("/");
            writer.RenderEndTag();
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "orange_text");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, "mailto:" + subsite.UnitEmail);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(subsite.UnitEmail);
            writer.RenderEndTag();
            writer.RenderEndTag();

            this.Footer.Text = sWriter.ToString();
        }
    }
}