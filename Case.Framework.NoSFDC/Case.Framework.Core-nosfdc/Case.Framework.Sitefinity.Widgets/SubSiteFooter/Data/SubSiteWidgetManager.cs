﻿using Case.Framework.Sitefinity.Widgets.SubSiteFooter.Managers;

namespace Case.Framework.Sitefinity.Widgets.SubSiteFooter.Data
{
    public class SubSiteWidgetManager
    {
        public static SubSiteManager SubSites { get { return SubSiteManager.Instance; } }
    }
}
