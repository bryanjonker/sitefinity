﻿<%@ Control Language="C#" Inherits="Case.Framework.Sitefinity.Widgets.Header.HeaderWidget.cs" %>
<%@ Register TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>

<!-- FOOTER -->
    <div class="row">
    	<div class="column one-half first">
    		<img alt="College of Education at Illinois" src="/theme/img/logo-college-of-illinois-text.png" />
    		<p class="footer-content left column three-quarter">
                1310 S. 6th St., Champaign, Illinois 61820<br>
    			<a href="tel:2173330960" class="orange_text">(217) 333-0960</a>
				<span class="divider">/</span>
				<a href="mailto:info@education.illinois.edu" class="orange_text">info@education.illinois.edu</a>
    		</p>
    	</div>
    	<div class="column one-half last">
            <asp:Literal ID="ltFooter" runat="server"></asp:Literal>
    		<p>&copy; University of Illinois Board of Trustees</p>
    	</div>
    </div>
