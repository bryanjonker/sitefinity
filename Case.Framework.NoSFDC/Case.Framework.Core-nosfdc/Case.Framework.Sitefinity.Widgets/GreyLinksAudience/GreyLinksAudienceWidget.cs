﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.GreyLinksAudience
{
    [RequireScriptManager]
    [ControlDesigner(typeof(GreyLinksAudienceWidgetDesigner))]
    public class GreyLinksAudienceWidget : SimpleView
    {
        private string data = string.Empty;
        private string viewMode = string.Empty;
        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string HeaderText
        {
            get;
            set;
        }

        protected virtual Literal ltHeader
        {
            get
            {
                return this.Container.GetControl<Literal>("ltHeader", true);
            }
        }

        protected virtual Literal ltLinks
        {
            get
            {
                return this.Container.GetControl<Literal>("ltLinks", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                List<Section> sections = new JavaScriptSerializer().Deserialize<List<Section>>(Data);
                var sWriter = new StringWriter();
                int i = 0;
                sWriter.Write("<div class='row'>");
                foreach (var section in sections.OrderBy(x => x.Order).ThenBy(x => x.Title))
                {
                    if (i == 0)
                    {
                        sWriter.Write("<div class='column one-third first'>");
                    }
                    else if (i % 3 == 0)
                    {
                        sWriter.Write("</div><div class='row'><div class='column one-third first'>");
                    }
                    else if (i % 3 == 2)
                    {
                        sWriter.Write("<div class='column one-third last'>");
                    }
                    else
                    {
                        sWriter.Write("<div class='column one-third'>");
                    }
                    sWriter.Write(string.Format("<a class='button white' id='{0}' href='{1}'>{2}&nbsp;›</a></div>",
                        section.Name, GetUrl(section), section.Name.Replace("_", " ")));
                    i++;
                }
                sWriter.Write("</div></div>");
                ltLinks.Text = sWriter.ToString();
                ltHeader.Text = HeaderText;
            }
            catch { }
        }

                protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.GreyLinksAudience.Resources.Views.GreyLinksAudienceWidget.ascx";
            }
        }

        private static string GetUrl(Section section)
        {
            if (section.URLGuid != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(section.URLGuid);
                if (page != null)
                {
                    return page.Url;
                }
            }
            return section.URL;
        }

        public class Section
        {
            public string Title;
            public string Name;
            public string Index;
            public string URL;
            public Guid URLGuid;
            public string Order;
        }
    }
}
