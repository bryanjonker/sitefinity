﻿﻿<%@ Control Language="C#" %>
<div class="stack bg gray">
    <section class="content full">
        <div id="find_resources" class="find row gray">
            <div class="find-title" style="text-transform: none"><asp:Literal runat="server" ID="ltHeader"></asp:Literal></div>
            <asp:Literal runat="server" ID="ltLinks"></asp:Literal>
        </div>
    </section>
</div>


