﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;

namespace Case.Framework.Sitefinity.Widgets.Info
{
    [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner))]
    public class InfoWidget : SimpleView
    {
        public Guid PageId { get; set; }
        public string PageUrl { get; set; }
        public string Header { get; set; }
        public string Text { get; set; }


        protected virtual Literal ltInfo
        {
            get
            {
                return this.Container.GetControl<Literal>("ltInfo", true);
            }
        }
        protected override void InitializeControls(GenericContainer container)
        {
            var sWriter = new StringWriter();
            var writer = new HtmlTextWriter(sWriter);



            if (PageId != Guid.Empty || !string.IsNullOrWhiteSpace(PageUrl))
            {
                var url = string.IsNullOrWhiteSpace(PageUrl) ? CaseManagers.Pages.GetById(PageId).Url : PageUrl;

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "row no-padding");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "intro_text column three-quarter first");
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.Write(Header);
                writer.RenderEndTag(); // P
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "column one-quarter last request");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "orange button");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("Request<br/>Information&nbsp;&rsaquo;");
                writer.RenderEndTag(); // A
                writer.RenderEndTag(); // DIV
                writer.RenderEndTag(); // DIV
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.Write(Text);
                writer.RenderEndTag(); // P
            }

            ltInfo.Text = sWriter.ToString();

            // <div class="row no-padding">
            //    <p class="intro_text column three-quarter first">Intro text lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            //    <a class="button orange column one-quarter last" href="#">Request Information &rsaquo;</a>
            //</div>
            //<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad fuga quas soluta assumenda porro at eius quos, ducimus necessitatibus similique mollitia labore est fugit maiores odio asperiores ea dolorem itaque!</p>

        }


        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.Info.Resources.Views.InfoWidget.ascx"; }
        }

    }
}
