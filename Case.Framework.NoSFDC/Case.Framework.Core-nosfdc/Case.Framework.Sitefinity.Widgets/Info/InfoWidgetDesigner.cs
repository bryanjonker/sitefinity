using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;
using System.Web.UI.HtmlControls;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Info
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Section.InfoWidget"/> widget
    /// </summary>
    public class InfoWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.Info.Resources.Views.InfoWidgetDesigner.ascx";
            }
        }

      
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual PagesSelector PageSelectorPageId
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorPageId", true);
            }
        }

        /// <summary>
        /// Gets the selector tag.
        /// </summary>
        /// <value>The selector tag.</value>
        public HtmlGenericControl SelectorTagPageId
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagPageId", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the Header property
        /// </summary>
        protected virtual Control Header
        {
            get
            {
                return this.Container.GetControl<Control>("Header", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the Text property
        /// </summary>
        protected virtual Control Text
        {
            get
            {
                return this.Container.GetControl<Control>("Text", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here

            if (this.PropertyEditor != null)
            {
                var uiCulture = this.PropertyEditor.PropertyValuesCulture;
                this.PageSelectorPageId.UICulture = uiCulture;
            }
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddComponentProperty("pageSelectorPageId", this.PageSelectorPageId.ClientID);
            descriptor.AddElementProperty("selectorTagPageId", this.SelectorTagPageId.ClientID);
            descriptor.AddElementProperty("header", this.Header.ClientID);
            descriptor.AddElementProperty("text", this.Text.ClientID);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(InfoWidgetDesigner.scriptReference, typeof(InfoWidgetDesigner).Assembly.FullName));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        #region Private members & constants
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Info.Resources.Javascript.InfoWidgetDesigner.js";
        #endregion
    }
}
 
