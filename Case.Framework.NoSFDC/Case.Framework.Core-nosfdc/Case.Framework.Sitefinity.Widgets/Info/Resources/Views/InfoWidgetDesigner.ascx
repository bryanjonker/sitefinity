<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>
<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
    <label class="sfTxtLbl" for="selectedPageIdLabel">Select Page for Link</label>
    <span style="display: none;" class="sfSelectedItem" id="selectedPageIdLabel">
        <asp:Literal runat="server" Text="" />
        
    </span>
        <span class="sfSelectedItem">
            <input id="txtUrl" type="text" style="display: none;" class="sfTxt" disabled="disabled" />
        </span>
        

    <span class="sfLinkBtn sfChange">
        <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonPageId">
            <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </a>
    </span>
    <div id="selectorTagPageId" runat="server" style="display: none;">
        <sf:PagesSelector runat="server" ID="pageSelectorPageId" 
            AllowExternalPagesSelection="true" AllowMultipleSelection="false" />
    </div>
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="Header" CssClass="sfTxtLbl">Header Text</asp:Label>
    <asp:TextBox ID="Header" runat="server" CssClass="sfTxt" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="Text" CssClass="sfTxtLbl">Paragraph Text</asp:Label>
    <asp:TextBox ID="Text" runat="server" CssClass="sfTxt" TextMode="MultiLine"  Rows="10"/>
    <div class="sfExample"></div>
    </li>
    
</ol>
</div>
