Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Info");

Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner = function (element) {
    /* Initialize PageId fields */
    this._pageSelectorPageId = null;
    this._selectorTagPageId = null;
    this._PageIdDialog = null;
 
    this._showPageSelectorPageIdDelegate = null;
    this._pageSelectedPageIdDelegate = null;
    
    /* Initialize Header fields */
    this._header = null;
    
    /* Initialize Text fields */
    this._text = null;
    
    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner.callBaseMethod(this, 'initialize');

        /* Initialize PageId */
        this._showPageSelectorPageIdDelegate = Function.createDelegate(this, this._showPageSelectorPageIdHandler);
        $addHandler(this.get_pageSelectButtonPageId(), "click", this._showPageSelectorPageIdDelegate);

        this._pageSelectedPageIdDelegate = Function.createDelegate(this, this._pageSelectedPageIdHandler);
        this.get_pageSelectorPageId().add_doneClientSelection(this._pageSelectedPageIdDelegate);

        if (this._selectorTagPageId) {
            this._PageIdDialog = jQuery(this._selectorTagPageId).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner.callBaseMethod(this, 'dispose');

        /* Dispose PageId */
        if (this._showPageSelectorPageIdDelegate) {
            $removeHandler(this.get_pageSelectButtonPageId(), "click", this._showPageSelectorPageIdDelegate);
            delete this._showPageSelectorPageIdDelegate;
        }

        if (this._pageSelectedPageIdDelegate) {
            this.get_pageSelectorPageId().remove_doneClientSelection(this._pageSelectedPageIdDelegate);
            delete this._pageSelectedPageIdDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI PageId */
        if (controlData.PageId && controlData.PageId !== "00000000-0000-0000-0000-000000000000") {
            var pagesSelectorPageId = this.get_pageSelectorPageId().get_pageSelector();
            var selectedPageLabelPageId = this.get_selectedPageIdLabel();
            var selectedPageButtonPageId = this.get_pageSelectButtonPageId();
            pagesSelectorPageId.add_selectionApplied(function (o, args) {
                var selectedPage = pagesSelectorPageId.get_selectedItem();
                if (selectedPage) {
                    selectedPageLabelPageId.innerHTML = selectedPage.Title.Value;
                    jQuery(selectedPageLabelPageId).show();
                    selectedPageButtonPageId.innerHTML = '<span>Change</span>';
                }
            });
            pagesSelectorPageId.set_selectedItems([{ Id: controlData.PageId}]);
        }        

        /* RefreshUI Header */
        jQuery(this.get_header()).val(controlData.Header);

        /* RefreshUI Text */
        jQuery(this.get_text()).val(controlData.Text);

        $('#txtUrl').val(controlData.PageUrl);
        jQuery(this.get_selectedPageIdLabel()).show();

        if (controlData.PageUrl) {
                $('#txtUrl').val(controlData.PageUrl);
                $('#txtUrl').show();
        } else {
            $('#txtUrl').hide();
        }
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges PageId */

        /* ApplyChanges Header */
        controlData.Header = jQuery(this.get_header()).val();

        /* ApplyChanges Text */
        controlData.Text = jQuery(this.get_text()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* PageId private methods */
    _showPageSelectorPageIdHandler: function (selectedItem) {
        debugger;
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorPageId().get_pageSelector();
        if (controlData.PageId) {
            pagesSelector.set_selectedItems([{ Id: controlData.PageId }]);
            $('.rtsLI.rtsLast a').parent().click();
        } else {

            pagesSelector.set_selectedItems(null);
            $('.rtsLI.rtsLast a').parent().click();
            //if (controlData.PageUrl) {
            //    $('#txtUrl').val(controlData.PageUrl);
            //    jQuery(this.get_selectedPageIdLabel()).show();
            //} else {
               
            //}
        }
        this._PageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._PageIdDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedPageIdHandler: function (items) {
        debugger;
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorPageId().get_pageSelector();
        this._PageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
        if (items == null) {
            return;
        }
        var selectedPage = pagesSelector.get_selectedItem();
        if (selectedPage || this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value() != '') {

            if (this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value() != '') {
                controlData.PageUrl = this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value();
                $('#txtUrl').val(this.get_pageSelectorPageId().get_extPagesSelector().get_urlTextBox().get_value());
                $('#txtUrl').show();
                jQuery(this.get_selectedPageIdLabel()).hide();
                this.get_pageSelectButtonPageId().innerHTML = '<span>Change</span>';
                controlData.PageId = "00000000-0000-0000-0000-000000000000";
            } else {
                this.get_selectedPageIdLabel().innerHTML = selectedPage.Title.Value;
                //$('#txtUrl').val(selectedPage.Title.Value);
                $('#txtUrl').hide();
                jQuery(this.get_selectedPageIdLabel()).show();
                this.get_pageSelectButtonPageId().innerHTML = '<span>Change</span>';
                controlData.PageId = selectedPage.Id;
                controlData.PageUrl = "";
            }
        }
        else {
            jQuery(this.get_selectedPageIdLabel()).hide();
            this.get_pageSelectButtonPageId().innerHTML = '<span>Select...</span>';
            controlData.PageId = "00000000-0000-0000-0000-000000000000";
            controlData.PageUrl = "";
            $('#txtUrl').val("");
        }
    },

    /* --------------------------------- properties -------------------------------------- */

    /* PageId properties */
    get_pageSelectButtonPageId: function () {
        if (this._pageSelectButtonPageId == null) {
            this._pageSelectButtonPageId = this.findElement("pageSelectButtonPageId");
        }
        return this._pageSelectButtonPageId;
    },
    get_selectedPageIdLabel: function () {
        if (this._selectedPageIdLabel == null) {
            this._selectedPageIdLabel = this.findElement("selectedPageIdLabel");
        }
        return this._selectedPageIdLabel;
    },
    get_pageSelectorPageId: function () {
        return this._pageSelectorPageId;
    },
    set_pageSelectorPageId: function (val) {
        this._pageSelectorPageId = val;
    },
    get_selectorTagPageId: function () {
        return this._selectorTagPageId;
    },
    set_selectorTagPageId: function (value) {
        this._selectorTagPageId = value;
    },

    /* Header properties */
    get_header: function () { return this._header; }, 
    set_header: function (value) { this._header = value; },

    /* Text properties */
    get_text: function () { return this._text; }, 
    set_text: function (value) { this._text = value; }
}

Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Info.InfoWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
