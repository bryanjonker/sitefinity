﻿using Case.Framework.Sitefinity.Content;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Builder;
using Telerik.Sitefinity.DynamicModules.Builder.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Modules.Pages.Web.UI;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
//--
namespace Case.Framework.Sitefinity.Widgets.Chevron
{
   [Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesigner(typeof(Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner))]
    public class ChevronWidget : SimpleView
    {

       public string FieldTypeName { get; set; }
      
        public Guid MainImageId { get; set; }

        public Guid ResearchImageId { get; set; }

        public string MainOverlayText { get; set; }

        public string TransitionText { get; set; }

        public string ChevronOneText { get; set; }

        public Guid TestimonialsId { get; set; }

        public Guid AcademicProgramIdPageId {get;set;}

        public string ChevronTwoText { get; set; }

        private Guid[] selectedResearchPages;

        public Guid[] SelectedResourcePages
        {
            get
            {
                if (selectedResearchPages == null) selectedResearchPages = new Guid[] { };
                return selectedResearchPages;
            }
            set { selectedResearchPages = value; }
        }

        public string ResearchLinks
        {
            get { return string.Join(",", SelectedResourcePages); }
            set
            {
                var list = new List<Guid>();
                if (value != null)
                {
                    var guids = value.Split(',');
                    foreach (var guid in guids)
                    {
                        Guid newGuid;
                        if (Guid.TryParse(guid, out newGuid))
                            list.Add(newGuid);
                    }
                }
                SelectedResourcePages = list.ToArray();
            }
        }
     
        public Guid BreakThroughPageId { get; set; }

        public string ChevronThreeText { get; set; }

        public Guid StatisticImageId { get; set; }

        public string StatisticText { get; set; }

        public Guid WaysToServePageId { get; set; }

        public string OuttroText { get; set; }

        protected virtual Literal LitQuote
        {
            get
            {
                return Container.GetControl<Literal>("ltChevron", true);
            }
        }

        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                // main introduction
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured introduction background-1");
                writer.AddAttribute(HtmlTextWriterAttribute.Id,"slide-1");
             
                if (MainImageId != Guid.Empty)
                {
                    var image = CaseManagers.Images.GetById(MainImageId);
                    if (image != null)
                    {
                        // url(\"" + imgPath + "\")
                        writer.AddAttribute(HtmlTextWriterAttribute.Style, "background: url(" + image.Url + ") no-repeat center 0 fixed;background-size: cover;");
                    }

                }
                writer.RenderBeginTag("section");

                if (!String.IsNullOrEmpty(MainOverlayText))
                {
                    string txt = MainOverlayText;
                    string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lst.Count() > 0)
                    {
                        //<span class="default"><h2>GREAT MINDS <span>Think EDUCATION AT ILLINOIS</span></h2></span>
                        if (lst.Count() > 1)
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "default");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(lst[1]);
                            writer.RenderEndTag(); // span
                            writer.RenderEndTag(); // DIV
                        }
                        else
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "default");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            writer.RenderEndTag(); //DIV
                        }
                        writer.RenderEndTag(); //SPAN
                    }
                }

                //<div class="overlay">
	    	    //<h2><span class='transition'>Connect with transformative<br>research, teaching, and learning</span></h2>
	            //<div>

                if (!String.IsNullOrEmpty(TransitionText))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "overlay");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    
                    string txt = TransitionText;
                    string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lst.Count() > 0)
                    {
                        if (lst.Count() > 1)
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "transition");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(lst[0]);
                            writer.Write("<br/>");
                            writer.Write(lst[1]);
                            writer.RenderEndTag(); // Span
                        }
                        else
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "transition");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(lst[0]);
                            writer.RenderEndTag(); // Span
                        }
                        writer.RenderEndTag(); // DIV
                    }
                    writer.RenderEndTag(); // DIV
                }
                writer.RenderEndTag(); // section 


                // chevron seperator
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron");
                writer.RenderBeginTag("section");
                if (!String.IsNullOrEmpty(ChevronOneText))
                {
                    string txt = ChevronOneText;
                    string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lst.Count() > 0)
                    {
                        //<section class="chevron" id="chevron-1">
	                    //<h2>Great Minds<span>Look At Students And</span><span>See the Future</span></h2>
	                    //</section>
                        if (lst.Count() > 1)
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            for (int x = 1; x < lst.Count(); x++)
                            {
                                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                                writer.Write(lst[x]);
                                writer.RenderEndTag(); // span
                            }
                            writer.RenderEndTag(); // DIV
                        }
                        else
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            writer.RenderEndTag(); // DIV
                        }
                    }
                }

                writer.RenderEndTag(); //end chevron 1

                // render testimonial
                RenderTestimonial(writer);

                // chevron seperator
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron");
                writer.RenderBeginTag("section");

                if (!String.IsNullOrEmpty(ChevronTwoText))
                {
                    string txt = ChevronTwoText;
                    string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lst.Count() > 0)
                    {
                        //<section class="chevron" id="chevron-1">
                        //<h2>Great Minds<span>Look At Students And</span><span>See the Future</span></h2>
                        //</section>
                        if (lst.Count() > 1)
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            for (int x = 1; x < lst.Count(); x++)
                            {
                                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                                writer.Write(lst[x]);
                                writer.RenderEndTag(); // span
                            }
                            writer.RenderEndTag(); //DIV
                        }
                        else
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            writer.RenderEndTag(); //DIV
                        }
                    }
                }

                writer.RenderEndTag(); //end chevron 2

                // render research
                RenderResearch(writer);

                // chevron seperator
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron");
                writer.RenderBeginTag("section");
                if (!String.IsNullOrEmpty(ChevronThreeText))
                {
                    string txt = ChevronThreeText;
                    string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lst.Count() > 0)
                    {
                        //<section class="chevron" id="chevron-1">
                        //<h2>Great Minds<span>Look At Students And</span><span>See the Future</span></h2>
                        //</section>
                        if (lst.Count() > 1)
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            for (int x = 1; x < lst.Count(); x++)
                            {
                                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                                writer.Write(lst[x]);
                                writer.RenderEndTag(); // span
                            }
                            writer.RenderEndTag(); // DIV
                        }
                        else
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);
                            writer.RenderEndTag(); // DIV
                        }
                    }
                }

                writer.RenderEndTag(); //chevron 3

                // Render statistic
                RenderStatistic(writer);

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "chevron outroduction");
                writer.RenderBeginTag("section");
                writer.RenderEndTag(); //chevron 4

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured outroduction");
                writer.RenderBeginTag("section");
                if (!String.IsNullOrEmpty(OuttroText))
                {
                     //<h1>Great Minds <span class="serif-700 blue">Think Illinois</span></h1>
                   //<h2>Join great minds driven by a passion to make education accessible to all learners, from all backgrounds, and abilities</h2>


                    string txt = OuttroText;
                    string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    if (lst.Count() > 0)
                    {
                        if (lst.Count() == 4)
                        {
                            //<div class="outro-title">Great Minds<div><span class="serif-700 blue">Think </span>
                            //<span class="sans-500 blue">Illinois</span></div></div>
                            //<div class="outro-text">Join great minds driven by a passion to make education accessible to all learners, from all backgrounds, and abilities</div>


                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "outro-title");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[0]);

                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "serif-700 blue");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(lst[1]);
                            writer.RenderEndTag(); // span

                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "serif-500 blue");
                            writer.RenderBeginTag(HtmlTextWriterTag.Span);
                            writer.Write(lst[2]);
                            writer.RenderEndTag(); // span
                            writer.RenderEndTag(); // div
                            writer.RenderEndTag(); // div
                            
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "outro-text");
                            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                            writer.Write(lst[3]);
                            writer.RenderEndTag(); //DIV
                        }
                    }
                }

                writer.RenderEndTag(); //outtro section

                LitQuote.Text = sWriter.ToString();
            }
            catch { }
        }

        protected void RenderStatistic(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured background background-4");
            if (StatisticImageId != Guid.Empty)
            {
                var image = CaseManagers.Images.GetById(StatisticImageId);
                if (image != null)
                {
                    // url(\"" + imgPath + "\")
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "background: url(" + image.Url + ") no-repeat center 0 fixed;background-size: cover;");
                }

            }
            writer.RenderBeginTag("section");

        //       <div class="content">
        //    <div class="cell">
        //        <h1><span>87%</span> of students engage in a service opportunity during their studies</h1>
        //        <a class="button orange" href="#">Ways to Serve &rsaquo;</a>
        //    </div>
        //</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "content");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "cell");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            if (!String.IsNullOrEmpty(StatisticText))
            {
                string txt = StatisticText;
                string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                if (lst.Count() > 0)
                {
                    if (lst.Count() > 1)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "cell-4");
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.Write(lst[0]);
                        writer.RenderEndTag(); // span
                        writer.Write(lst[1]);
                        writer.RenderEndTag(); // DIV
                    }
                }

                if (WaysToServePageId != Guid.Empty)
                {
                    var page = CaseManagers.Pages.GetById(WaysToServePageId);
                    if (page != null)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                        writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                        writer.Write("Ways to Serve&nbsp;&rsaquo;");
                        writer.RenderEndTag();

                    }
                }
            }
            writer.RenderEndTag(); //end div
            writer.RenderEndTag(); //end div
            writer.RenderEndTag(); //end statistic
        }

        protected void RenderResearch(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured background background-3");
            if (ResearchImageId != Guid.Empty)
            {
                var image = CaseManagers.Images.GetById(ResearchImageId);
                if (image != null)
                {
                    // url(\"" + imgPath + "\")
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "background: url(" + image.Url + ") no-repeat center 0 fixed;background-size: cover;");
                }
            }
            writer.RenderBeginTag("section");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "content");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            //Desktop
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "nodes");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "svg");
            writer.AddAttribute("onerror", "this.src=/theme/img/content/home_widget/home_desktop.png;");
            writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Home SVG Widget Desktop");
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/content/home_widget/home_desktop.svg");
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag(); // img

            //Mobile
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "mnodes");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "svg");
            writer.AddAttribute("onerror", "this.src=/theme/img/content/home_widget/home_mobile.png;");
            writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Home SVG Widget Mobile");
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "/theme/img/content/home_widget/home_mobile.svg");
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag(); // img

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "textnodes");
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            var links = BuildLinks();
            for (int x = 0; x < SelectedResourcePages.Count(); x++)
            {
                var page = CaseManagers.Pages.GetById(selectedResearchPages[x]);
                if (page != null)
                {
                    writer.Write(String.Format(links[x], page.Url, page.Title));
                }
            }
            writer.RenderEndTag(); // UL
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "cell");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            if (BreakThroughPageId != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(BreakThroughPageId);
                if (page != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, page.Url);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write("Breakthrough Research&nbsp;&rsaquo;");
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();

            writer.RenderEndTag(); // div
            writer.RenderEndTag(); //end research
        }

        protected void RenderTestimonial(HtmlTextWriter writer)
        {
            // initialize managers
            var dynMgr = ModuleBuilderManager.GetManager();
            var mgr = DynamicModuleManager.GetManager();
            var rnd = new Random();

            // get module chosen
            var module = dynMgr.GetItems(typeof(DynamicModule), "", "", 0, 0).Cast<DynamicModule>().FirstOrDefault(x => x.Title == this.FieldTypeName);

            if (module == null) return;
            // get list of all dynamic item types
            var dynamicTypes = dynMgr.GetItems(typeof(DynamicModuleType), "", "", 0, 0);
            foreach (var typeName in from DynamicModuleType dynamicType in dynamicTypes where module.Id == dynamicType.ParentModuleId where module.Status != DynamicModuleStatus.NotInstalled && module.Status != DynamicModuleStatus.Inactive select TypeResolutionService.ResolveType(dynamicType.GetFullTypeName()))
            {
                var content = mgr.GetDataItems(typeName).Where(i => i.Status == ContentLifecycleStatus.Live).ToList();
                if (!content.Any()) continue;
                var r = rnd.Next(content.Count());
                var item = content[r];


                writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured background background-2");
                if (item.GetImage("Image") != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "background: url(" + item.GetImage("Image").Url + ") no-repeat center 0 fixed;background-size: cover;");
                }
                writer.RenderBeginTag("section");

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "content");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "cell");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderBeginTag(HtmlTextWriterTag.Blockquote);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "pull");
                writer.RenderBeginTag(HtmlTextWriterTag.P);
                writer.WriteLine(item.GetStringSafe("Testimonial"));
                writer.RenderEndTag();
                writer.RenderBeginTag("footer");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.WriteLine(item.GetStringSafe("ByLine"));
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.WriteLine(item.GetStringSafe("SubLine"));
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag(); // blockquote
                if (item.GetLinkedPage("Link") != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "button orange");
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, item.GetLinkedPage("Link").GetFullUrl().Replace("~",""));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write("Academic Programs&nbsp;&rsaquo;");
                    writer.RenderEndTag();
                }
               
                writer.RenderEndTag(); // div
                writer.RenderEndTag(); // div
                writer.RenderEndTag(); // section
                break;
            }
        }

        protected List<string> BuildLinks()
        {
            var lstContent = new List<string>();
            string link8 = "<li class=\"eighth\"><a href=\"{0}\">{1}</a>";
            string link7 = "<li class=\"seventh\"><a href=\"{0}\">{1}</a>";
            string link6 = "<li class=\"sixth\"><a href=\"{0}\">{1}</a>";
            string link5 = "<li class=\"fifth\"><a href=\"{0}\">{1}</a>";
            string link4 = "<li class=\"fourth\"><a href=\"{0}\">{1}</a>";
            string link3 = "<li class=\"third\"><a href=\"{0}\">{1}</a>";
            string link2 = "<li class=\"second\"><a href=\"{0}\">{1}</a>";
            string link1 = "<li class=\"first\"><a href=\"{0}\">{1}</a>";

            lstContent.Add(link1);
            lstContent.Add(link2);
            lstContent.Add(link3);
            lstContent.Add(link4);
            lstContent.Add(link5);
            lstContent.Add(link6);
            lstContent.Add(link7);
            lstContent.Add(link8);

            return lstContent;

        }
        protected override string LayoutTemplateName
        {
            get { return "Case.Framework.Sitefinity.Widgets.Chevron.Resources.Views.ChevronWidget.ascx"; }
        }
        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }
    }
}
