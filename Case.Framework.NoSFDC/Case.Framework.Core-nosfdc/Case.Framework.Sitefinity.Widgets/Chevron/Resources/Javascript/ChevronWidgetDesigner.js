Type.registerNamespace("Case.Framework.Sitefinity.Widgets.Chevron");

Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner = function (element) {
    /* Initialize MainImageId fields */
    this._selectButtonMainImageId = null;
    this._selectButtonMainImageIdClickDelegate = null;
    this._deselectButtonMainImageId = null;
    this._deselectButtonMainImageIdClickDelegate = null;
    this._selectorMainImageIdCloseDelegate = null;
    this._selectorMainImageIdUploaderViewFileChangedDelegate = null;
    
    this._MainImageIdDialog = null;
    this._selectorMainImageId = null;
    this._MainImageIdId = null;
    
    /* Initialize MainOverlayText fields */
    this._mainOverlayText = null;
    
    /* Initialize TransitionText fields */
    this._transitionText = null;
    
    /* Initialize ChevronOneText fields */
    this._chevronOneText = null;
    
    /* Initialize TestimonialsId fields */
    this._comboTypeName = null;
   
    /* Initialize ChevronTwoText fields */
    this._chevronTwoText = null;
    
    /* Initialize ResearchImageId fields */
    this._selectButtonResearchImageId = null;
    this._selectButtonResearchImageIdClickDelegate = null;
    this._deselectButtonResearchImageId = null;
    this._deselectButtonResearchImageIdClickDelegate = null;
    this._selectorResearchImageIdCloseDelegate = null;
    this._selectorResearchImageIdUploaderViewFileChangedDelegate = null;

    this._ResearchImageIdDialog = null;
    this._selectorResearchImageId = null;
    this._ResearchImageIdId = null;


    /* Initialize ResearchLinks fields */
    this._pageSelectorResearchLinks = null;
    this._selectorTagResearchLinks = null;
    this._ResearchLinksDialog = null;
 
    this._showPageSelectorResearchLinksDelegate = null;
    this._pageSelectedResearchLinksDelegate = null;
    
    /* Initialize BreakThroughPageId fields */
    this._pageSelectorBreakThroughPageId = null;
    this._selectorTagBreakThroughPageId = null;
    this._BreakThroughPageIdDialog = null;
 
    this._showPageSelectorBreakThroughPageIdDelegate = null;
    this._pageSelectedBreakThroughPageIdDelegate = null;
    
    /* Initialize ChevronThreeText fields */
    this._chevronThreeText = null;
    
    /* Initialize StatisticImageId fields */
    this._selectButtonStatisticImageId = null;
    this._selectButtonStatisticImageIdClickDelegate = null;
    this._deselectButtonStatisticImageId = null;
    this._deselectButtonStatisticImageIdClickDelegate = null;
    this._selectorStatisticImageIdCloseDelegate = null;
    this._selectorStatisticImageIdUploaderViewFileChangedDelegate = null;
    
    this._StatisticImageIdDialog = null;
    this._selectorStatisticImageId = null;
    this._StatisticImageIdId = null;
    
    /* Initialize WaysToServePageId fields */
    this._pageSelectorWaysToServePageId = null;
    this._selectorTagWaysToServePageId = null;
    this._WaysToServePageIdDialog = null;

    this._showPageSelectorWaysToServePageIdDelegate = null;
    this._pageSelectedWaysToServePageIdDelegate = null;

    /* Initialize StatisticText fields */
    this._statisticText = null;
    
    /* Initialize OuttroText fields */
    this._outtroText = null;
    
    /* Initialize the service url for the image thumbnails */
    this.imageServiceUrl = null;

    /* Calls the base constructor */
    Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner.initializeBase(this, [element]);
}

Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner.callBaseMethod(this, 'initialize');

        /* Initialize MainImageId */
        this._selectButtonMainImageIdClickDelegate = Function.createDelegate(this, this._selectButtonMainImageIdClicked);
        if (this._selectButtonMainImageId) {
            $addHandler(this._selectButtonMainImageId, "click", this._selectButtonMainImageIdClickDelegate);
        }

        this._deselectButtonMainImageIdClickDelegate = Function.createDelegate(this, this._deselectButtonMainImageIdClicked);
        if (this._deselectButtonMainImageId) {
            $addHandler(this._deselectButtonMainImageId, "click", this._deselectButtonMainImageIdClickDelegate);
        }

        if (this._selectorMainImageId) {
            this._MainImageIdDialog = jQuery(this._selectorMainImageId.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorMainImageIdCloseDelegate
            });
        } 

        jQuery("#previewMainImageId").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorMainImageIdInsertDelegate = Function.createDelegate(this, this._selectorMainImageIdInsertHandler);
        this._selectorMainImageId.set_customInsertDelegate(this._selectorMainImageIdInsertDelegate);
        $addHandler(this._selectorMainImageId._cancelLink, "click", this._selectorMainImageIdCloseHandler);
        this._selectorMainImageIdCloseDelegate = Function.createDelegate(this, this._selectorMainImageIdCloseHandler);
        this._selectorMainImageIdUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorMainImageIdUploaderViewFileChangedHandler);


        /* Initialize ResearchImageId */
        this._selectButtonResearchImageIdClickDelegate = Function.createDelegate(this, this._selectButtonResearchImageIdClicked);
        if (this._selectButtonMainImageId) {
            $addHandler(this._selectButtonResearchImageId, "click", this._selectButtonResearchImageIdClickDelegate);
        }

        this._deselectButtonResearchImageIdClickDelegate = Function.createDelegate(this, this._deselectButtonResearchImageIdClicked);
        if (this._deselectButtonResearchImageId) {
            $addHandler(this._deselectButtonResearchImageId, "click", this._deselectButtonResearchImageIdClickDelegate);
        }

        if (this._selectorResearchImageId) {
            this._ResearchImageIdDialog = jQuery(this._selectorResearchImageId.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorResearchImageIdCloseDelegate
            });
        }

        jQuery("#previewResearchImageId").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorResearchImageIdInsertDelegate = Function.createDelegate(this, this._selectorResearchImageIdInsertHandler);
        this._selectorResearchImageId.set_customInsertDelegate(this._selectorResearchImageIdInsertDelegate);
        $addHandler(this._selectorResearchImageId._cancelLink, "click", this._selectorResearchImageIdCloseHandler);
        this._selectorResearchImageIdCloseDelegate = Function.createDelegate(this, this._selectorResearchImageIdCloseHandler);
        this._selectorResearchImageIdUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorResearchImageIdUploaderViewFileChangedHandler);


        /* Initialize ResearchLinks */
        this._showPageSelectorResearchLinksDelegate = Function.createDelegate(this, this._showPageSelectorResearchLinksHandler);
        $addHandler(this.get_pageSelectButtonResearchLinks(), "click", this._showPageSelectorResearchLinksDelegate);

        this._pageSelectedResearchLinksDelegate = Function.createDelegate(this, this._pageSelectedResearchLinksHandler);
        this.get_pageSelectorResearchLinks().add_doneClientSelection(this._pageSelectedResearchLinksDelegate);

        if (this._selectorTagResearchLinks) {
            this._ResearchLinksDialog = jQuery(this._selectorTagResearchLinks).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }

        /* Initialize BreakThroughPageId */
        this._showPageSelectorBreakThroughPageIdDelegate = Function.createDelegate(this, this._showPageSelectorBreakThroughPageIdHandler);
        $addHandler(this.get_pageSelectButtonBreakThroughPageId(), "click", this._showPageSelectorBreakThroughPageIdDelegate);

        this._pageSelectedBreakThroughPageIdDelegate = Function.createDelegate(this, this._pageSelectedBreakThroughPageIdHandler);
        this.get_pageSelectorBreakThroughPageId().add_doneClientSelection(this._pageSelectedBreakThroughPageIdDelegate);

        if (this._selectorTagBreakThroughPageId) {
            this._BreakThroughPageIdDialog = jQuery(this._selectorTagBreakThroughPageId).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }

        /* Initialize WaysToServePageId */
        this._showPageSelectorWaysToServePageIdDelegate = Function.createDelegate(this, this._showPageSelectorWaysToServePageIdHandler);
        $addHandler(this.get_pageSelectButtonWaysToServePageId(), "click", this._showPageSelectorWaysToServePageIdDelegate);

        this._pageSelectedWaysToServePageIdDelegate = Function.createDelegate(this, this._pageSelectedWaysToServePageIdHandler);
        this.get_pageSelectorWaysToServePageId().add_doneClientSelection(this._pageSelectedWaysToServePageIdDelegate);

        if (this._selectorTagWaysToServePageId) {
            this._WaysToServePageIdDialog = jQuery(this._selectorTagWaysToServePageId).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }


        /* Initialize StatisticImageId */
        this._selectButtonStatisticImageIdClickDelegate = Function.createDelegate(this, this._selectButtonStatisticImageIdClicked);
        if (this._selectButtonStatisticImageId) {
            $addHandler(this._selectButtonStatisticImageId, "click", this._selectButtonStatisticImageIdClickDelegate);
        }

        this._deselectButtonStatisticImageIdClickDelegate = Function.createDelegate(this, this._deselectButtonStatisticImageIdClicked);
        if (this._deselectButtonStatisticImageId) {
            $addHandler(this._deselectButtonStatisticImageId, "click", this._deselectButtonStatisticImageIdClickDelegate);
        }

        if (this._selectorStatisticImageId) {
            this._StatisticImageIdDialog = jQuery(this._selectorStatisticImageId.get_element()).dialog({
                autoOpen: false,
                modal: false,
                width: 655,
                height: "auto",
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000,
                close: this._selectorStatisticImageIdCloseDelegate
            });
        } 

        jQuery("#previewStatisticImageId").load(function () {
            dialogBase.resizeToContent();
        });

        this._selectorStatisticImageIdInsertDelegate = Function.createDelegate(this, this._selectorStatisticImageIdInsertHandler);
        this._selectorStatisticImageId.set_customInsertDelegate(this._selectorStatisticImageIdInsertDelegate);
        $addHandler(this._selectorStatisticImageId._cancelLink, "click", this._selectorStatisticImageIdCloseHandler);
        this._selectorStatisticImageIdCloseDelegate = Function.createDelegate(this, this._selectorStatisticImageIdCloseHandler);
        this._selectorStatisticImageIdUploaderViewFileChangedDelegate = Function.createDelegate(this, this._selectorStatisticImageIdUploaderViewFileChangedHandler);
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner.callBaseMethod(this, 'dispose');

        /* Dispose MainImageId */
        if (this._selectButtonMainImageId) {
            $removeHandler(this._selectButtonMainImageId, "click", this._selectButtonMainImageIdClickDelegate);
        }
        if (this._selectButtonMainImageIdClickDelegate) {
            delete this._selectButtonMainImageIdClickDelegate;
        }
        
        if (this._deselectButtonMainImageId) {
            $removeHandler(this._deselectButtonMainImageId, "click", this._deselectButtonMainImageIdClickDelegate);
        }
        if (this._deselectButtonMainImageIdClickDelegate) {
            delete this._deselectButtonMainImageIdClickDelegate;
        }

        $removeHandler(this._selectorMainImageId._cancelLink, "click", this._selectorMainImageIdCloseHandler);

        if (this._selectorMainImageIdCloseDelegate) {
            delete this._selectorMainImageIdCloseDelegate;
        }

        if (this._selectorMainImageIdUploaderViewFileChangedDelegate) {
            this._selectorMainImageId._uploaderView.remove_onFileChanged(this._selectorMainImageIdUploaderViewFileChangedDelegate);
            delete this._selectorMainImageIdUploaderViewFileChangedDelegate;
        }


        /* Dispose ResearchImageId */
        if (this._selectButtonResearchImageId) {
            $removeHandler(this._selectButtonResearchImageId, "click", this._selectButtonResearchImageIdClickDelegate);
        }
        if (this._selectButtonResearchImageIdClickDelegate) {
            delete this._selectButtonResearchImageIdClickDelegate;
        }

        if (this._deselectButtonResearchImageId) {
            $removeHandler(this._deselectButtonResearchImageId, "click", this._deselectButtonResearchImageIdClickDelegate);
        }
        if (this._deselectButtonResearchImageIdClickDelegate) {
            delete this._deselectButtonResearchImageIdClickDelegate;
        }

        $removeHandler(this._selectorResearchImageId._cancelLink, "click", this._selectorResearchImageIdCloseHandler);

        if (this._selectorResearchImageIdCloseDelegate) {
            delete this._selectorResearchImageIdCloseDelegate;
        }

        if (this._selectorResearchImageIdUploaderViewFileChangedDelegate) {
            this._selectorResearchImageId._uploaderView.remove_onFileChanged(this._selectorResearchImageIdUploaderViewFileChangedDelegate);
            delete this._selectorResearchImageIdUploaderViewFileChangedDelegate;
        }


        /* Dispose ResearchLinks */
        if (this._showPageSelectorResearchLinksDelegate) {
            $removeHandler(this.get_pageSelectButtonResearchLinks(), "click", this._showPageSelectorResearchLinksDelegate);
            delete this._showPageSelectorResearchLinksDelegate;
        }

        if (this._pageSelectedResearchLinksDelegate) {
            this.get_pageSelectorResearchLinks().remove_doneClientSelection(this._pageSelectedResearchLinksDelegate);
            delete this._pageSelectedResearchLinksDelegate;
        }

        /* Dispose BreakThroughPageId */
        if (this._showPageSelectorBreakThroughPageIdDelegate) {
            $removeHandler(this.get_pageSelectButtonBreakThroughPageId(), "click", this._showPageSelectorBreakThroughPageIdDelegate);
            delete this._showPageSelectorBreakThroughPageIdDelegate;
        }

        if (this._pageSelectedBreakThroughPageIdDelegate) {
            this.get_pageSelectorBreakThroughPageId().remove_doneClientSelection(this._pageSelectedBreakThroughPageIdDelegate);
            delete this._pageSelectedBreakThroughPageIdDelegate;
        }

        /* Dispose WaysToServePageId */
        if (this._showPageSelectorWaysToServePageIdDelegate) {
            $removeHandler(this.get_pageSelectButtonWaysToServePageId(), "click", this._showPageSelectorWaysToServePageIdDelegate);
            delete this._showPageSelectorBreakThroughPageIdDelegate;
        }

        if (this._pageSelectedWaysToServePageIdDelegate) {
            this.get_pageSelectorWaysToServePageId().remove_doneClientSelection(this._pageSelectedWaysToServePageIdDelegate);
            delete this._pageSelectedWaysToServePageIdDelegate;
        }

        /* Dispose StatisticImageId */
        if (this._selectButtonStatisticImageId) {
            $removeHandler(this._selectButtonStatisticImageId, "click", this._selectButtonStatisticImageIdClickDelegate);
        }
        if (this._selectButtonStatisticImageIdClickDelegate) {
            delete this._selectButtonStatisticImageIdClickDelegate;
        }
        
        if (this._deselectButtonStatisticImageId) {
            $removeHandler(this._deselectButtonStatisticImageId, "click", this._deselectButtonStatisticImageIdClickDelegate);
        }
        if (this._deselectButtonStatisticImageIdClickDelegate) {
            delete this._deselectButtonStatisticImageIdClickDelegate;
        }

        $removeHandler(this._selectorStatisticImageId._cancelLink, "click", this._selectorStatisticImageIdCloseHandler);

        if (this._selectorStatisticImageIdCloseDelegate) {
            delete this._selectorStatisticImageIdCloseDelegate;
        }

        if (this._selectorStatisticImageIdUploaderViewFileChangedDelegate) {
            this._selectorStatisticImageId._uploaderView.remove_onFileChanged(this._selectorStatisticImageIdUploaderViewFileChangedDelegate);
            delete this._selectorStatisticImageIdUploaderViewFileChangedDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control(); /* JavaScript clone of your control - all the control properties will be properties of the controlData too */


        /* RefreshUI MainImageId */
        this.get_selectedMainImageId().innerHTML = controlData.MainImageId;
        if (controlData.MainImageId && controlData.MainImageId != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonMainImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonMainImageId()).show()
            var url = this.imageServiceUrl + controlData.MainImageId + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewMainImageId").show();
                    jQuery("#previewMainImageId").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonMainImageId()).hide()
        }

        /* RefreshUI MainOverlayText */
        jQuery(this.get_mainOverlayText()).val(controlData.MainOverlayText);

        /* RefreshUI TransitionText */
        jQuery(this.get_transitionText()).val(controlData.TransitionText);

        /* RefreshUI ChevronOneText */
        jQuery(this.get_chevronOneText()).val(controlData.ChevronOneText);

        /* RefreshUI TestimonialsId */
        var comboBoxTypeName = this.get_comboTypeName();
        if (controlData.FieldTypeName != null) {
            comboBoxTypeName.set_value(controlData.FieldTypeName);
            comboBoxTypeName.set_text(controlData.FieldTypeName);
        }

        /* RefreshUI ChevronTwoText */
        jQuery(this.get_chevronTwoText()).val(controlData.ChevronTwoText);

        /* RefreshUI ResearchImageId */
        this.get_selectedResearchImageId().innerHTML = controlData.ResearchImageId;
        if (controlData.ResearchImageId && controlData.ResearchImageId != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonResearchImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonResearchImageId()).show()
            var url = this.imageServiceUrl + controlData.ResearchImageId + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewResearchImageId").show();
                    jQuery("#previewResearchImageId").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonResearchImageId()).hide()
        }


        /* RefreshUI ResearchLinks */
        if (controlData.ResearchLinks && controlData.ResearchLinks !== "00000000-0000-0000-0000-000000000000") {
            var wrapper = this.get_pageSelectorResearchLinks().get_pageSelector();
            var selector = this.get_pageSelectorResearchLinks().get_pageSelector();;
            var selectedPageLabelResearchLinks = this.get_selectedResearchLinksLabel();
            var selectedPageButtonResearchLinks = this.get_pageSelectButtonResearchLinks();

            var selecting = false;
            wrapper.add_selectionApplied(function () {
                if (selecting) {
                    return;
                }
                selecting = true;
                var pageIds = controlData.ResearchLinks;
                if (pageIds) {

                    selectedPageButtonResearchLinks.innerHTML = '<span>Change</span>';

                    var pages = pageIds.split(",");
                    var oldMethod = selector._updateTreeSelection;
                    selector._updateTreeSelection = function () {
                        if (this._treeIsBound == false) {
                            this._treeMustBeUpdated = true;
                            return;
                        }
                        this._treeMustBeUpdated = false;
                        var tree = this.get_itemsTree();
                        var binder = tree.getBinder();
                        if (this._selectedItemId) {
                            binder.setSelectedValues([this._selectedItemId], true, true);
                        } else {
                            var selectedItems = tree.get_selectedItems();
                            if (selectedItems && selectedItems.length > 0) {
                                binder.clearSelection();
                                binder.setSelectedItems(selectedItems, true, true);
                            } else {
                                if (this._selectedItemIds) {
                                    binder.setSelectedValues(this._selectedItemIds, true, true);
                                }
                            }
                        }
                        this._raiseSelectionApplied(this, {});
                    };
                    selector.set_selectedItemIds(pages);
                    selector._updateTreeSelection = oldMethod;
                    selectedPageLabelResearchLinks.innerHTML = '';

                    for (var x = 0; x < selector.get_selectedItems().length; x++) {
                        var page = selector.get_selectedItems()[x];
                        if (x != (selector.get_selectedItems().length -1))
                            selectedPageLabelResearchLinks.innerHTML += page.Title.Value + ', ';
                        else
                            selectedPageLabelResearchLinks.innerHTML += page.Title.Value;
                    }
                    selectedPageLabelResearchLinks.innerHTML += "<br/>";
                    selectedPageLabelResearchLinks.innerHTML += "<br/>";
                    jQuery(selectedPageLabelResearchLinks).show();

                }
            });
        }        

        /* RefreshUI BreakThroughPageId */
        if (controlData.BreakThroughPageId && controlData.BreakThroughPageId !== "00000000-0000-0000-0000-000000000000") {
            var pagesSelectorBreakThroughPageId = this.get_pageSelectorBreakThroughPageId().get_pageSelector();
            var selectedPageLabelBreakThroughPageId = this.get_selectedBreakThroughPageIdLabel();
            var selectedPageButtonBreakThroughPageId = this.get_pageSelectButtonBreakThroughPageId();
            pagesSelectorBreakThroughPageId.add_selectionApplied(function (o, args) {
                var selectedPage = pagesSelectorBreakThroughPageId.get_selectedItem();
                if (selectedPage) {
                    selectedPageLabelBreakThroughPageId.innerHTML = selectedPage.Title.Value;
                    jQuery(selectedPageLabelBreakThroughPageId).show();
                    selectedPageButtonBreakThroughPageId.innerHTML = '<span>Change</span>';
                }
            });
            pagesSelectorBreakThroughPageId.set_selectedItems([{ Id: controlData.BreakThroughPageId}]);
        }        

        /* RefreshUI ChevronThreeText */
        jQuery(this.get_chevronThreeText()).val(controlData.ChevronThreeText);


        /* RefreshUI WaysToServePageId */
        if (controlData.WaysToServePageId && controlData.WaysToServePageId !== "00000000-0000-0000-0000-000000000000") {
            var pagesSelectorWaysToServePageId = this.get_pageSelectorWaysToServePageId().get_pageSelector();
            var selectedPageLabelWaysToServePageId = this.get_selectedWaysToServePageIdLabel();
            var selectedPageButtonWaysToServePageId = this.get_pageSelectButtonWaysToServePageId();
            pagesSelectorWaysToServePageId.add_selectionApplied(function (o, args) {
                var selectedPage = pagesSelectorWaysToServePageId.get_selectedItem();
                if (selectedPage) {
                    selectedPageLabelWaysToServePageId.innerHTML = selectedPage.Title.Value;
                    jQuery(selectedPageLabelWaysToServePageId).show();
                    selectedPageButtonWaysToServePageId.innerHTML = '<span>Change</span>';
                }
            });
            pagesSelectorWaysToServePageId.set_selectedItems([{ Id: controlData.WaysToServePageId }]);
        }

        /* RefreshUI StatisticImageId */
        this.get_selectedStatisticImageId().innerHTML = controlData.StatisticImageId;
        if (controlData.StatisticImageId && controlData.StatisticImageId != "00000000-0000-0000-0000-000000000000") {
            this.get_selectButtonStatisticImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery(this.get_deselectButtonStatisticImageId()).show()
            var url = this.imageServiceUrl + controlData.StatisticImageId + "/?published=true";
            jQuery.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    jQuery("#previewStatisticImageId").show();
                    jQuery("#previewStatisticImageId").attr("src", data.Item.ThumbnailUrl);
                    dialogBase.resizeToContent();
                }
            });
        }
        else {
            jQuery(this.get_deselectButtonStatisticImageId()).hide()
        }

        /* RefreshUI StatisticText */
        jQuery(this.get_statisticText()).val(controlData.StatisticText);

        /* RefreshUI OuttroText */
        jQuery(this.get_outtroText()).val(controlData.OuttroText);

    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control();

        /* ApplyChanges MainImageId */
        controlData.MainImageId = this.get_selectedMainImageId().innerHTML;

        /* ApplyChanges MainOverlayText */
        controlData.MainOverlayText = jQuery(this.get_mainOverlayText()).val();

        /* ApplyChanges TransitionText */
        controlData.TransitionText = jQuery(this.get_transitionText()).val();

        /* ApplyChanges ChevronOneText */
        controlData.ChevronOneText = jQuery(this.get_chevronOneText()).val();

        /* ApplyChanges TestimonialsId */
        var comboTypeNameSelected = this.get_comboTypeName().get_selectedItem();
        if (comboTypeNameSelected) controlData.FieldTypeName = comboTypeNameSelected.get_text();

        /* ApplyChanges ChevronTwoText */
        controlData.ChevronTwoText = jQuery(this.get_chevronTwoText()).val();

        /* ApplyChanges ResearchImageId */
        controlData.ResearchImageId = this.get_selectedResearchImageId().innerHTML;

        /* ApplyChanges ResearchLinks */

        /* ApplyChanges BreakThroughPageId */

        /* ApplyChanges ChevronThreeText */
        controlData.ChevronThreeText = jQuery(this.get_chevronThreeText()).val();

        /* ApplyChanges StatisticImageId */
        controlData.StatisticImageId = this.get_selectedStatisticImageId().innerHTML;

        /* ApplyChanges StatisticText */
        controlData.StatisticText = jQuery(this.get_statisticText()).val();

        /* ApplyChanges OuttroText */
        controlData.OuttroText = jQuery(this.get_outtroText()).val();
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* MainImageId event handlers */
    _selectButtonMainImageIdClicked: function (sender, args) {
        this._selectorMainImageId._uploaderView.add_onFileChanged(this._selectorMainImageIdUploaderViewFileChangedDelegate);
        this._MainImageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._MainImageIdDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorMainImageId.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorMainImageId.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonMainImageIdClicked: function (sender, args) {
        jQuery("#previewMainImageId").hide();
                    jQuery("#previewMainImageId").attr("src", "");
        this.get_selectedMainImageId().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonMainImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonMainImageId()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* TestimonialsId event handlers */


    /* MainImageId event handlers */
    _selectButtonResearchImageIdClicked: function (sender, args) {
        this._selectorResearchImageId._uploaderView.add_onFileChanged(this._selectorResearchImageIdUploaderViewFileChangedDelegate);
        this._ResearchImageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._ResearchImageIdDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorResearchImageId.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorResearchImageId.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonResearchImageIdClicked: function (sender, args) {
        jQuery("#previewResearchImageId").hide();
        jQuery("#previewResearchImageId").attr("src", "");
        this.get_selectedResearchImageId().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonResearchImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonResearchImageId()).hide()
        dialogBase.resizeToContent();
        return false;
    },

    /* StatisticImageId event handlers */
    _selectButtonStatisticImageIdClicked: function (sender, args) {
        this._selectorStatisticImageId._uploaderView.add_onFileChanged(this._selectorStatisticImageIdUploaderViewFileChangedDelegate);
        this._StatisticImageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._StatisticImageIdDialog.dialog().parent().css("min-width", "655px");
        dialogBase.resizeToContent();
        try {
            this._selectorStatisticImageId.get_uploaderView().get_altTextField().set_value("");
        }
        catch (ex) { }
        jQuery(this._selectorStatisticImageId.get_uploaderView().get_settingsPanel()).hide();
        return false;
    },

    _deselectButtonStatisticImageIdClicked: function (sender, args) {
        jQuery("#previewStatisticImageId").hide();
                    jQuery("#previewStatisticImageId").attr("src", "");
        this.get_selectedStatisticImageId().innerHTML = "00000000-0000-0000-0000-000000000000";
        this.get_selectButtonStatisticImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Select...</span>";
        jQuery(this.get_deselectButtonStatisticImageId()).hide()
		dialogBase.resizeToContent();
        return false;
    },

    /* --------------------------------- private methods --------------------------------- */

    /* MainImageId private methods */
    _selectorMainImageIdInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._MainImageIdId = selectedItem.Id;
            this.get_selectedMainImageId().innerHTML = this._MainImageIdId;
            jQuery(this.get_deselectButtonMainImageId()).show()
            this.get_selectButtonMainImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewMainImageId").show();
                    jQuery("#previewMainImageId").attr("src", selectedItem.ThumbnailUrl);
        }
        this._MainImageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorMainImageIdCloseHandler: function () {
        if(this._MainImageIdDialog){
            this._MainImageIdDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorMainImageIdUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },


    /* ResearchImageId private methods */
    _selectorResearchImageIdInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._ResearchImageIdId = selectedItem.Id;
            this.get_selectedResearchImageId().innerHTML = this._ResearchImageIdId;
            jQuery(this.get_deselectButtonResearchImageId()).show()
            this.get_selectButtonResearchImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewResearchImageId").show();
            jQuery("#previewResearchImageId").attr("src", selectedItem.ThumbnailUrl);
        }
        this._ResearchImageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorResearchImageIdCloseHandler: function () {
        if (this._ResearchImageIdDialog) {
            this._ResearchImageIdDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorResearchImageIdUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },


    /* ResearchLinks private methods */
    _showPageSelectorResearchLinksHandler: function (selectedItem) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorResearchLinks().get_pageSelector();
        if (controlData.ResearchLinks) {
            pagesSelector.set_selectedItems([{ Id: controlData.ResearchLinks }]);
        }
        this._ResearchLinksDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._ResearchLinksDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedResearchLinksHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorResearchLinks().get_pageSelector();

        if (items == null) {
            this._ResearchLinksDialog.dialog("close");
            jQuery("#designerLayoutRoot").show();
            dialogBase.resizeToContent();
            return;
        }

        if (items.length > 8) {
            alert('You can only have 8 pages selected.')
            return;
        }

        this._ResearchLinksDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
      var selectedPages = pagesSelector.get_selectedItems();
        var pages = [];
        if (selectedPages) {
            this.get_selectedResearchLinksLabel().innerHTML = '';
            for (var i = 0, l = selectedPages.length; i < l; i++) {
                var page = selectedPages[i];
                pages.push(page.Id);
                if (i != (selectedPages.length - 1))
                    this.get_selectedResearchLinksLabel().innerHTML += page.Title.Value + ', ';
                else
                    this.get_selectedResearchLinksLabel().innerHTML += page.Title.Value;

            }
            this.get_selectedResearchLinksLabel().innerHTML += '<br/>';
            this.get_selectedResearchLinksLabel().innerHTML += '<br/>';
            jQuery(this.get_selectedResearchLinksLabel()).show();
            controlData.ResearchLinks = pages.join();
       }
        else {
            jQuery(this.get_selectedResearchLinksLabel()).hide();
            this.get_pageSelectButtonResearchLinks().innerHTML = '<span>Select...</span>';
            controlData.ResearchLinks = "00000000-0000-0000-0000-000000000000";
        }
    },

    /* BreakThroughPageId private methods */
    _showPageSelectorBreakThroughPageIdHandler: function (selectedItem) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorBreakThroughPageId().get_pageSelector();
        if (controlData.BreakThroughPageId) {
            pagesSelector.set_selectedItems([{ Id: controlData.BreakThroughPageId }]);
        }
        this._BreakThroughPageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._BreakThroughPageIdDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedBreakThroughPageIdHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorBreakThroughPageId().get_pageSelector();
        this._BreakThroughPageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
        if (items == null) {
            return;
        }
        var selectedPage = pagesSelector.get_selectedItem();
        if (selectedPage) {
            this.get_selectedBreakThroughPageIdLabel().innerHTML = selectedPage.Title.Value;
            jQuery(this.get_selectedBreakThroughPageIdLabel()).show();
            this.get_pageSelectButtonBreakThroughPageId().innerHTML = '<span>Change</span>';
            controlData.BreakThroughPageId = selectedPage.Id;
        }
        else {
            jQuery(this.get_selectedBreakThroughPageIdLabel()).hide();
            this.get_pageSelectButtonBreakThroughPageId().innerHTML = '<span>Select...</span>';
            controlData.BreakThroughPageId = "00000000-0000-0000-0000-000000000000";
        }
    },


    /* WaysToServePageId private methods */
    _showPageSelectorWaysToServePageIdHandler: function (selectedItem) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorWaysToServePageId().get_pageSelector();
        if (controlData.WaysToServePageId) {
            pagesSelector.set_selectedItems([{ Id: controlData.WaysToServePageId }]);
        }
        this._WaysToServePageIdDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._WaysToServePageIdDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedWaysToServePageIdHandler: function (items) {
        var controlData = this._propertyEditor.get_control();
        var pagesSelector = this.get_pageSelectorWaysToServePageId().get_pageSelector();
        this._WaysToServePageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
        if (items == null) {
            return;
        }
        var selectedPage = pagesSelector.get_selectedItem();
        if (selectedPage) {
            this.get_selectedWaysToServePageIdLabel().innerHTML = selectedPage.Title.Value;
            jQuery(this.get_selectedWaysToServePageIdLabel()).show();
            this.get_pageSelectButtonWaysToServePageId().innerHTML = '<span>Change</span>';
            controlData.WaysToServePageId = selectedPage.Id;
        }
        else {
            jQuery(this.get_selectedWaysToServePageIdLabel()).hide();
            this.get_pageSelectButtonWaysToServePageId().innerHTML = '<span>Select...</span>';
            controlData.WaysToServePageId = "00000000-0000-0000-0000-000000000000";
        }
    },

    /* StatisticImageId private methods */
    _selectorStatisticImageIdInsertHandler: function (selectedItem) {

        if (selectedItem) {
            this._StatisticImageIdId = selectedItem.Id;
            this.get_selectedStatisticImageId().innerHTML = this._StatisticImageIdId;
            jQuery(this.get_deselectButtonStatisticImageId()).show()
            this.get_selectButtonStatisticImageId().innerHTML = "<span class=\"sfLinkBtnIn\">Change</span>";
            jQuery("#previewStatisticImageId").show();
                    jQuery("#previewStatisticImageId").attr("src", selectedItem.ThumbnailUrl);
        }
        this._StatisticImageIdDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorStatisticImageIdCloseHandler: function () {
        if(this._StatisticImageIdDialog){
            this._StatisticImageIdDialog.dialog("close");
        }
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
    },

    _selectorStatisticImageIdUploaderViewFileChangedHandler: function () {
        dialogBase.resizeToContent();
    },

    /* --------------------------------- properties -------------------------------------- */

    /* MainImageId properties */
    get_selectorMainImageId: function () {
        return this._selectorMainImageId;
    },
    set_selectorMainImageId: function (value) {
        this._selectorMainImageId = value;
    },
    get_selectButtonMainImageId: function () {
        return this._selectButtonMainImageId;
    },
    set_selectButtonMainImageId: function (value) {
        this._selectButtonMainImageId = value;
    },
    get_deselectButtonMainImageId: function () {
        return this._deselectButtonMainImageId;
    },
    set_deselectButtonMainImageId: function (value) {
        this._deselectButtonMainImageId = value;
    },
    get_selectedMainImageId: function () {
        if (this._selectedMainImageId == null) {
            this._selectedMainImageId = this.findElement("selectedMainImageId");
        }
        return this._selectedMainImageId;
    },

    /* MainOverlayText properties */
    get_mainOverlayText: function () { return this._mainOverlayText; }, 
    set_mainOverlayText: function (value) { this._mainOverlayText = value; },

    /* TransitionText properties */
    get_transitionText: function () { return this._transitionText; }, 
    set_transitionText: function (value) { this._transitionText = value; },

    /* ChevronOneText properties */
    get_chevronOneText: function () { return this._chevronOneText; }, 
    set_chevronOneText: function (value) { this._chevronOneText = value; },

    /* TestimonialsId properties */
    get_comboTypeName: function () {
        return this._comboTypeName
    },
    set_comboTypeName: function (value) {
        this._comboTypeName = value;
    },

    /* ChevronTwoText properties */
    get_chevronTwoText: function () { return this._chevronTwoText; }, 
    set_chevronTwoText: function (value) { this._chevronTwoText = value; },

    /* ResearchImageId properties */
    get_selectorResearchImageId: function () {
        return this._selectorResearchImageId;
    },
    set_selectorResearchImageId: function (value) {
        this._selectorResearchImageId = value;
    },
    get_selectButtonResearchImageId: function () {
        return this._selectButtonResearchImageId;
    },
    set_selectButtonResearchImageId: function (value) {
        this._selectButtonResearchImageId = value;
    },
    get_deselectButtonResearchImageId: function () {
        return this._deselectButtonResearchImageId;
    },
    set_deselectButtonResearchImageId: function (value) {
        this._deselectButtonResearchImageId = value;
    },
    get_selectedResearchImageId: function () {
        if (this._selectedResearchImageId == null) {
            this._selectedResearchImageId = this.findElement("selectedResearchImageId");
        }
        return this._selectedResearchImageId;
    },

    /* ResearchLinks properties */
    get_pageSelectButtonResearchLinks: function () {
        if (this._pageSelectButtonResearchLinks == null) {
            this._pageSelectButtonResearchLinks = this.findElement("pageSelectButtonResearchLinks");
        }
        return this._pageSelectButtonResearchLinks;
    },
    get_selectedResearchLinksLabel: function () {
        if (this._selectedResearchLinksLabel == null) {
            this._selectedResearchLinksLabel = this.findElement("selectedResearchLinksLabel");
        }
        return this._selectedResearchLinksLabel;
    },
    get_pageSelectorResearchLinks: function () {
        return this._pageSelectorResearchLinks;
    },
    set_pageSelectorResearchLinks: function (val) {
        this._pageSelectorResearchLinks = val;
    },
    get_selectorTagResearchLinks: function () {
        return this._selectorTagResearchLinks;
    },
    set_selectorTagResearchLinks: function (value) {
        this._selectorTagResearchLinks = value;
    },

    /* BreakThroughPageId properties */
    get_pageSelectButtonBreakThroughPageId: function () {
        if (this._pageSelectButtonBreakThroughPageId == null) {
            this._pageSelectButtonBreakThroughPageId = this.findElement("pageSelectButtonBreakThroughPageId");
        }
        return this._pageSelectButtonBreakThroughPageId;
    },
    get_selectedBreakThroughPageIdLabel: function () {
        if (this._selectedBreakThroughPageIdLabel == null) {
            this._selectedBreakThroughPageIdLabel = this.findElement("selectedBreakThroughPageIdLabel");
        }
        return this._selectedBreakThroughPageIdLabel;
    },
    get_pageSelectorBreakThroughPageId: function () {
        return this._pageSelectorBreakThroughPageId;
    },
    set_pageSelectorBreakThroughPageId: function (val) {
        this._pageSelectorBreakThroughPageId = val;
    },
    get_selectorTagBreakThroughPageId: function () {
        return this._selectorTagBreakThroughPageId;
    },
    set_selectorTagBreakThroughPageId: function (value) {
        this._selectorTagBreakThroughPageId = value;
    },

    /* WaysToServePageId properties */
    get_pageSelectButtonWaysToServePageId: function () {
        if (this._pageSelectButtonWaysToServePageId == null) {
            this._pageSelectButtonWaysToServePageId = this.findElement("pageSelectButtonWaysToServePageId");
        }
        return this._pageSelectButtonWaysToServePageId;
    },
    get_selectedWaysToServePageIdLabel: function () {
        if (this._selectedWaysToServePageIdLabel == null) {
            this._selectedWaysToServePageIdLabel = this.findElement("selectedWaysToServePageIdLabel");
        }
        return this._selectedWaysToServePageIdLabel;
    },
    get_pageSelectorWaysToServePageId: function () {
        return this._pageSelectorWaysToServePageId;
    },
    set_pageSelectorWaysToServePageId: function (val) {
        this._pageSelectorWaysToServePageId = val;
    },
    get_selectorTagWaysToServePageId: function () {
        return this._selectorTagWaysToServePageId;
    },
    set_selectorTagWaysToServePageId: function (value) {
        this._selectorTagWaysToServePageId = value;
    },

    /* ChevronThreeText properties */
    get_chevronThreeText: function () { return this._chevronThreeText; }, 
    set_chevronThreeText: function (value) { this._chevronThreeText = value; },

    /* StatisticImageId properties */
    get_selectorStatisticImageId: function () {
        return this._selectorStatisticImageId;
    },
    set_selectorStatisticImageId: function (value) {
        this._selectorStatisticImageId = value;
    },
    get_selectButtonStatisticImageId: function () {
        return this._selectButtonStatisticImageId;
    },
    set_selectButtonStatisticImageId: function (value) {
        this._selectButtonStatisticImageId = value;
    },
    get_deselectButtonStatisticImageId: function () {
        return this._deselectButtonStatisticImageId;
    },
    set_deselectButtonStatisticImageId: function (value) {
        this._deselectButtonStatisticImageId = value;
    },
    get_selectedStatisticImageId: function () {
        if (this._selectedStatisticImageId == null) {
            this._selectedStatisticImageId = this.findElement("selectedStatisticImageId");
        }
        return this._selectedStatisticImageId;
    },

    /* StatisticText properties */
    get_statisticText: function () { return this._statisticText; }, 
    set_statisticText: function (value) { this._statisticText = value; },

    /* OuttroText properties */
    get_outtroText: function () { return this._outtroText; }, 
    set_outtroText: function (value) { this._outtroText = value; },

}

Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner.registerClass('Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
