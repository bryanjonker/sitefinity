<%@ Control %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="sfFields" Namespace="Telerik.Sitefinity.Web.UI.Fields" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
    <%--<sitefinity:ResourceFile Name="Telerik.Sitefinity.Resources.Scripts.Kendo.kendo.all.min.js" />--%>
    <%--<sitefinity:ResourceFile Name="Case.Framework.Sitefinity.Widgets.Chevron.Resources.KendoWidgetStyle.css"
        Static="true" AssemblyInfo="Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidget, Case.Framework.Sitefinity.Widgets" />--%>

</sitefinity:ResourceLinks>

<div id="designerLayoutRoot" class="sfContentViews sfSingleContentView" style="max-height: 400px; overflow: auto; ">
<ol>        
    <li class="sfFormCtrl">
    <asp:Label runat="server" CssClass="sfTxtLbl">Intro Image</asp:Label>
    <img id="previewMainImageId" src="" alt="" style="display:none;" />
    <span style="display: none;" class="sfSelectedItem" id="selectedMainImageId"></span>
    <div>
      <asp:LinkButton ID="selectButtonMainImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
      </asp:LinkButton>
      <asp:LinkButton ID="deselectButtonMainImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
      </asp:LinkButton>
    </div>
    <sf:EditorContentManagerDialog runat="server" ID="selectorMainImageId" DialogMode="Image" HostedInRadWindow="false" BodyCssClass="" />
    <div class="sfExample"></div>
    </li>
    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="MainOverlayText" CssClass="sfTxtLbl">Main Overlay Text</asp:Label>
    <asp:TextBox ID="MainOverlayText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    
  
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="TransitionText" CssClass="sfTxtLbl">Transition Text</asp:Label>
    <asp:TextBox ID="TransitionText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    <li>
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="ChevronOneText" CssClass="sfTxtLbl">Chevron Text</asp:Label>
    <asp:TextBox ID="ChevronOneText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    <li>
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    
    <li class="sfFormCtrl">
          <ul id="panelBar">
                <li>Testimonials Rotator Configuration
                    <br />
                    <div id="allEcommerceOptions">
                        <div id="productFieldEvents">
                            <div class="sfExample">
                                <span style="font-weight: bold">Select the content type to use for testimonial data: </span>
                            </div>
                            <div>
                                <telerik:RadComboBox ID="RadComboBoxTypeName" runat="server" Width="100%" Skin="Metro"
                                    EmptyMessage="Choose a content type..." CssClass="comboWrapper">
                                </telerik:RadComboBox>
                            </div>
                        </div>
          
                    </div>
                </li>
            </ul>
    </li>
    <li class="sfFormCtrl">
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    

    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="ChevronTwoText" CssClass="sfTxtLbl">Chevron Text</asp:Label>
    <asp:TextBox ID="ChevronTwoText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
      <li>
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    
    <li class="sfFormCtrl">
    <asp:Label ID="Label1" runat="server" CssClass="sfTxtLbl">Research & Outreach Image</asp:Label>
    <img id="previewResearchImageId" src="" alt="" style="display:none;" />
    <span style="display: none;" class="sfSelectedItem" id="selectedResearchImageId"></span>
    <div>
      <asp:LinkButton ID="selectButtonResearchImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
      </asp:LinkButton>
      <asp:LinkButton ID="deselectButtonReSearchImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
      </asp:LinkButton>
    </div>
    <sf:EditorContentManagerDialog runat="server" ID="selectorResearchImageId" DialogMode="Image" HostedInRadWindow="false" BodyCssClass="" />
    <div class="sfExample"></div>
    </li>   
    <li class="sfFormCtrl">
    <label class="sfTxtLbl" for="selectedResearchLinksLabel">Research & Outreach Page(s)</label>
    <span style="display: none;" class="sfSelectedItem" id="selectedResearchLinksLabel">
        <asp:Literal runat="server" Text="" />
    </span>
    <span class="sfLinkBtn sfChange">
        <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonResearchLinks">
            <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </a>
    </span>
    <div id="selectorTagResearchLinks" runat="server" style="display: none;">
        <sf:PagesSelector runat="server" ID="pageSelectorResearchLinks" 
            AllowExternalPagesSelection="false" AllowMultipleSelection="true" />
    </div>
    <div class="sfExample"></div>
    </li>

    <li class="sfFormCtrl">
    <label class="sfTxtLbl" for="selectedBreakThroughPageIdLabel">Breakthrough Research Page</label>
    <span style="display: none;" class="sfSelectedItem" id="selectedBreakThroughPageIdLabel">
        <asp:Literal runat="server" Text="" />
    </span>
    <span class="sfLinkBtn sfChange">
        <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonBreakThroughPageId">
            <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </a>
    </span>
    <div id="selectorTagBreakThroughPageId" runat="server" style="display: none;">
        <sf:PagesSelector runat="server" ID="pageSelectorBreakThroughPageId" 
            AllowExternalPagesSelection="false" AllowMultipleSelection="false" />
    </div>
    <div class="sfExample"></div>
    </li>

     <li>
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    

    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="ChevronThreeText" CssClass="sfTxtLbl">Chevron Text</asp:Label>
    <asp:TextBox ID="ChevronThreeText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    <li>
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    

    <li class="sfFormCtrl">
    <asp:Label runat="server" CssClass="sfTxtLbl">Statistic Background Image</asp:Label>
    <img id="previewStatisticImageId" src="" alt="" style="display:none;" />
    <span style="display: none;" class="sfSelectedItem" id="selectedStatisticImageId"></span>
    <div>
      <asp:LinkButton ID="selectButtonStatisticImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </span>
      </asp:LinkButton>
      <asp:LinkButton ID="deselectButtonStatisticImageId" OnClientClick="return false;" runat="server" CssClass="sfLinkBtn sfChange">
        <span class="sfLinkBtnIn">
          <asp:Literal runat="server" Text="<%$Resources:Labels, Remove %>" />
        </span>
      </asp:LinkButton>
    </div>
    <sf:EditorContentManagerDialog runat="server" ID="selectorStatisticImageId" DialogMode="Image" HostedInRadWindow="false" BodyCssClass="" />
    <div class="sfExample"></div>
    </li>
    <li class="sfFormCtrl">
    <label class="sfTxtLbl" for="selectedWaysToServePageIdLabel">Ways to Serve Page</label>
    <span style="display: none;" class="sfSelectedItem" id="selectedWaysToServePageIdLabel">
        <asp:Literal ID="Literal3" runat="server" Text="" />
    </span>
    <span class="sfLinkBtn sfChange">
        <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonWaysToServePageId">
            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:Labels, SelectDotDotDot %>" />
        </a>
    </span>
    <div id="selectorTagWaysToServePageId" runat="server" style="display: none;">
        <sf:PagesSelector runat="server" ID="pageSelectorWaysToServePageId" 
            AllowExternalPagesSelection="false" AllowMultipleSelection="false" />
    </div>
    <div class="sfExample"></div>
    </li>    
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="StatisticText" CssClass="sfTxtLbl">Statistic Text</asp:Label>
    <asp:TextBox ID="StatisticText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
     <li>
        <div class="sfFormSeparator" style="border-bottom:1px dotted #bbb">
            &nbsp;
        </div>
    </li>    
  
    <li class="sfFormCtrl">
    <asp:Label runat="server" AssociatedControlID="OuttroText" CssClass="sfTxtLbl">Outtro Text</asp:Label>
    <asp:TextBox ID="OuttroText" runat="server" CssClass="sfTxt" TextMode="MultiLine" />
    <div class="sfExample"></div>
    </li>
    
    
</ol>
</div>
