using System;
using System.Linq;
using System.Web.UI;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Modules.Pages;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Sitefinity.DynamicModules.Builder;
using Telerik.Sitefinity.DynamicModules.Builder.Model;

[assembly: WebResource(Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidgetDesigner.scriptReference, "application/x-javascript")]
namespace Case.Framework.Sitefinity.Widgets.Chevron
{
    /// <summary>
    /// Represents a designer for the <typeparamref name="Case.Framework.Sitefinity.Widgets.Chevron.ChevronWidget"/> widget
    /// </summary>
    public class ChevronWidgetDesigner : ControlDesignerBase
    {
        #region Properties
        /// <summary>
        /// Obsolete. Use LayoutTemplatePath instead.
        /// </summary>
        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.Chevron.Resources.Views.ChevronWidgetDesigner.ascx";
            }
        }

       
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        #endregion

        #region Control references
        /// <summary>
        /// The LinkButton for selecting MainImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonMainImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonMainImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting MainImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonMainImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonMainImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the MainImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorMainImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorMainImageId", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the MainOverlayText property
        /// </summary>
        protected virtual Control MainOverlayText
        {
            get
            {
                return this.Container.GetControl<Control>("MainOverlayText", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the TransitionText property
        /// </summary>
        protected virtual Control TransitionText
        {
            get
            {
                return this.Container.GetControl<Control>("TransitionText", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the ChevronOneText property
        /// </summary>
        protected virtual Control ChevronOneText
        {
            get
            {
                return this.Container.GetControl<Control>("ChevronOneText", true);
            }
        }

        public RadComboBox comboTypeName
        {
            get { return Container.GetControl<RadComboBox>("RadComboBoxTypeName", true); }
        }

        /// <summary>
        /// Gets the control that is bound to the ChevronTwoText property
        /// </summary>
        protected virtual Control ChevronTwoText
        {
            get
            {
                return this.Container.GetControl<Control>("ChevronTwoText", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting ResearchImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonResearchImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonResearchImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting ResearchImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonResearchImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonResearchImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the ResearchImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorResearchImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorResearchImageId", false);
            }
        }



        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual PagesSelector PageSelectorResearchLinks
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorResearchLinks", true);
            }
        }

        /// <summary>
        /// Gets the selector tag.
        /// </summary>
        /// <value>The selector tag.</value>
        public HtmlGenericControl SelectorTagResearchLinks
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagResearchLinks", true);
            }
        }

        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual PagesSelector PageSelectorBreakThroughPageId
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorBreakThroughPageId", true);
            }
        }

        /// <summary>
        /// Gets the selector tag.
        /// </summary>
        /// <value>The selector tag.</value>
        public HtmlGenericControl SelectorTagBreakThroughPageId
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagBreakThroughPageId", true);
            }
        }


        /// <summary>
        /// Gets the page selector control.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual PagesSelector PageSelectorWaysToServePageId
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorWaysToServePageId", true);
            }
        }

        /// <summary>
        /// Gets the selector tag.
        /// </summary>
        /// <value>The selector tag.</value>
        public HtmlGenericControl SelectorTagWaysToServePageId
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagWaysToServePageId", true);
            }
        }


        /// <summary>
        /// Gets the control that is bound to the ChevronThreeText property
        /// </summary>
        protected virtual Control ChevronThreeText
        {
            get
            {
                return this.Container.GetControl<Control>("ChevronThreeText", true);
            }
        }

        /// <summary>
        /// The LinkButton for selecting StatisticImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton SelectButtonStatisticImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("selectButtonStatisticImageId", false);
            }
        }

        /// <summary>
        /// The LinkButton for deselecting StatisticImageId.
        /// </summary>
        /// <value>The page selector control.</value>
        protected internal virtual LinkButton DeselectButtonStatisticImageId
        {
            get
            {
                return this.Container.GetControl<LinkButton>("deselectButtonStatisticImageId", false);
            }
        }

        /// <summary>
        /// Gets the RadEditor Manager dialog for inserting image, document or video for the StatisticImageId property.
        /// </summary>
        /// <value>The RadEditor Manager dialog for inserting image, document or video.</value>
        protected EditorContentManagerDialog SelectorStatisticImageId
        {
            get
            {
                return this.Container.GetControl<EditorContentManagerDialog>("selectorStatisticImageId", false);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the StatisticText property
        /// </summary>
        protected virtual Control StatisticText
        {
            get
            {
                return this.Container.GetControl<Control>("StatisticText", true);
            }
        }

        /// <summary>
        /// Gets the control that is bound to the OuttroText property
        /// </summary>
        protected virtual Control OuttroText
        {
            get
            {
                return this.Container.GetControl<Control>("OuttroText", true);
            }
        }

        #endregion

        #region Methods
        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            // Place your initialization logic here

            if (this.PropertyEditor != null)
            {
                // Place your initialization logic here
                this.comboTypeName.DataSource = GetModuleTypes();
                this.comboTypeName.DataBind();

                var item = new DataMemberInfo
                {
                    Name = "Title",
                    HeaderText = "Title",
                    ColumnTemplate = "<span>{{Title}}</span>",
                    IsSearchField = true
                };

                var uiCulture = this.PropertyEditor.PropertyValuesCulture;
                this.PageSelectorResearchLinks.UICulture = uiCulture;
                this.PageSelectorBreakThroughPageId.UICulture = uiCulture;
            }
        }
        #endregion

        #region IScriptControl implementation
        /// <summary>
        /// Gets a collection of script descriptors that represent ECMAScript (JavaScript) client components.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var descriptor = (ScriptControlDescriptor)scriptDescriptors.Last();

            descriptor.AddElementProperty("selectButtonMainImageId", this.SelectButtonMainImageId.ClientID);
            descriptor.AddElementProperty("deselectButtonMainImageId", this.DeselectButtonMainImageId.ClientID);
            descriptor.AddComponentProperty("selectorMainImageId", this.SelectorMainImageId.ClientID);
            descriptor.AddElementProperty("selectButtonResearchImageId", this.SelectButtonResearchImageId.ClientID);
            descriptor.AddElementProperty("deselectButtonResearchImageId", this.DeselectButtonResearchImageId.ClientID);
            descriptor.AddComponentProperty("selectorResearchImageId", this.SelectorResearchImageId.ClientID);

            descriptor.AddElementProperty("mainOverlayText", this.MainOverlayText.ClientID);
            descriptor.AddElementProperty("transitionText", this.TransitionText.ClientID);
            descriptor.AddElementProperty("chevronOneText", this.ChevronOneText.ClientID);
            descriptor.AddElementProperty("chevronTwoText", this.ChevronTwoText.ClientID);
            descriptor.AddComponentProperty("pageSelectorResearchLinks", this.PageSelectorResearchLinks.ClientID);
            descriptor.AddElementProperty("selectorTagResearchLinks", this.SelectorTagResearchLinks.ClientID);
            descriptor.AddComponentProperty("pageSelectorBreakThroughPageId", this.PageSelectorBreakThroughPageId.ClientID);
            descriptor.AddElementProperty("selectorTagBreakThroughPageId", this.SelectorTagBreakThroughPageId.ClientID);
            descriptor.AddElementProperty("chevronThreeText", this.ChevronThreeText.ClientID);
            descriptor.AddComponentProperty("pageSelectorWaysToServePageId", this.PageSelectorWaysToServePageId.ClientID);
            descriptor.AddElementProperty("selectorTagWaysToServePageId", this.SelectorTagWaysToServePageId.ClientID);
            descriptor.AddElementProperty("selectButtonStatisticImageId", this.SelectButtonStatisticImageId.ClientID);
            descriptor.AddElementProperty("deselectButtonStatisticImageId", this.DeselectButtonStatisticImageId.ClientID);
            descriptor.AddComponentProperty("selectorStatisticImageId", this.SelectorStatisticImageId.ClientID);
            descriptor.AddElementProperty("statisticText", this.StatisticText.ClientID);
            descriptor.AddElementProperty("outtroText", this.OuttroText.ClientID);
            descriptor.AddProperty("imageServiceUrl", this.imageServiceUrl);
            descriptor.AddComponentProperty("comboTypeName", this.comboTypeName.ClientID);

            return scriptDescriptors;
        }

        /// <summary>
        /// Gets a collection of ScriptReference objects that define script resources that the control requires.
        /// </summary>
        public override System.Collections.Generic.IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference(ChevronWidgetDesigner.scriptReference, typeof(ChevronWidgetDesigner).Assembly.FullName));
            return scripts;
        }

        /// <summary>
        /// Gets the required by the control, core library scripts predefined in the <see cref="ScriptRef"/> enum.
        /// </summary>
        protected override ScriptRef GetRequiredCoreScripts()
        {
            return ScriptRef.JQuery | ScriptRef.JQueryUI;
        }
        #endregion

        public List<string> GetModuleTypes()
        {
            List<string> types = new List<string>();
            ModuleBuilderManager dynMgr = ModuleBuilderManager.GetManager();
            var modules = dynMgr.GetItems(typeof(DynamicModule), "", "", 0, 0).Cast<DynamicModule>();

            foreach (var module in modules)
            {

                types.Add(module.Name);

            }
            return types;

        }
  

        #region Private members & constants
        public const string scriptReference = "Case.Framework.Sitefinity.Widgets.Chevron.Resources.Javascript.ChevronWidgetDesigner.js";
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
        #endregion
    }
}
 
