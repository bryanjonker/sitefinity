﻿<%@ Control Language="C#" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Sitefinity" TagPrefix="designers" Namespace="Telerik.Sitefinity.Web.UI.ControlDesign" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI.Fields" TagPrefix="sitefinity" %>
<%@ Register Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Modules.Libraries.Web.UI.Designers" TagPrefix="sitefinity" %>

<sitefinity:ResourceLinks ID="resourcesLinks" runat="server">
    <sitefinity:ResourceFile Name="Styles/Ajax.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.core.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.dialog.css" />
    <sitefinity:ResourceFile Name="Styles/jQuery/jquery.ui.theme.sitefinity.css" />
</sitefinity:ResourceLinks>

<div id="selectorTag" style="display: none;" class="sfDesignerSelector">
<sitefinity:MediaContentSelectorView
    ID="selectorView"
    runat="server"
     ItemName="Image"
     ItemsName="Images"
     ContentType="Telerik.Sitefinity.Libraries.Model.Image"
     ParentType="Telerik.Sitefinity.Libraries.Model.Album"
     LibraryServiceUrl="~/Sitefinity/Services/Content/AlbumService.svc/folders/"
     MediaContentBinderServiceUrl="~/Sitefinity/Services/Content/ImageService.svc/"
     MediaContentItemsListDescriptionTemplate="Telerik.Sitefinity.Resources.Templates.Designers.Libraries.Images.ImageItemDescriptionTemplate.htm"
     DisplayResizingOptionsControl="false"
     ShowOpenOriginalSizeCheckBox="false"
     CssClass="sfContentViews">
</sitefinity:MediaContentSelectorView>
 
</div>

<div id="selectorTagPageId" runat="server" style="display: none;">
    <sitefinity:PagesSelector runat="server" ID="pageSelectorPageId" 
        AllowExternalPagesSelection="true" AllowMultipleSelection="false" />
</div>

<div class="sfContentViews">
<div class="sfColWrapper sfEqualCols sfModeSelector sfClearfix sfNavDesignerCtrl sfNavDim">
<div id="RotatorDesignChoice" class="sfLeftCol">
    <h2 class="sfStep1">Add Slides to Image Carousel</h2>
    <br />
    <div class="sfModeSwitcher">
		<a id="aAddTab" class="sfLinkBtn"><strong class="sfLinkBtnIn">Add Slide</strong></a>
    </div>
    <br />
   <div class="sfExample">
        <span style="font-weight: bold">Slide: </span>
   </div>  
    <ul id="ulAdded" class="sfRadioList RotatorDesignList"> </ul>
    <br />
</div>
<div class="sfRightCol">
    <h2 class="sfStep2">Select an Image.</h2>
    <div id="divStep2" class="sfStep2Options" <!--style="display: none;"-->>    
        <div id="groupSettingPageSelect">
            <div class="sfExpandableSection">
                <ul class="sfRadioList RotatorDesignList">
                  <li>
                        <label style="color: #333;">Caption</label>
                        <br />
                        <input id="txtTitle" type="text" class="sfTxt" style="width: 600px;" />
                    </li>
                    <li>
                        <label style="color: #333;">Image</label>
                        <br />
                        <input id="txtImageUrl" type="text" class="sfTxt" disabled="disabled" />&nbsp;<a class="sfLinkBtn"><strong class="sfLinkBtnIn" onclick="openSelector('normal');">Select Image...</strong></a>
                    </li>
                     <li>
                        <label style="color: #333;">Slide URL</label>
                        <br />
                        <input id="txtUrl" type="text" class="sfTxt" disabled="disabled" />
                        <span class="sfLinkBtn">
                            <a href="javascript: void(0)" class="sfLinkBtnIn" id="pageSelectButtonPageId">
                                <asp:Literal runat="server" Text="Select Page..." /><br /><br />
                            </a>
                        </span>
                        <input id="txtUrlGuid" type="text" class="sfTxt" style="width: 600px; visibility: hidden;" />
                    </li>
                </ul>
                <br /><br /><br /><br />
            </div>
        </div>    
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    function openSelector(forState) {
        $('#selectorTag').css('display', 'block');
        dialogBase.resizeToContent();

        _forState = forState;
    }
</script>

