﻿<%@ Control Language="C#" Inherits="Case.Framework.Sitefinity.Widgets.Slider.SliderWidget.cs" %>

<div class="slider">
    <asp:Literal runat="server" ID="ltSlider"></asp:Literal>
    <span>
        <a class="slider_l column one-quarter first">&lsaquo;</a>
        <em class="caption_right"></em>
        <em class="text_right">
            <a href="#" id="MoreLink" class="button transparent inline">More&nbsp;&rsaquo;</a>
        </em>
        <a class="slider_r column one-quarter last">&rsaquo;</a>
    </span>
</div>