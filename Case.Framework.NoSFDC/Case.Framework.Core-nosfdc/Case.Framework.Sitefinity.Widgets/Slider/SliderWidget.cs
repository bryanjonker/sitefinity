﻿namespace Case.Framework.Sitefinity.Widgets.Slider
{
    using Case.Framework.Sitefinity.Data;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Telerik.Sitefinity.Modules.Pages.Web.UI;
    using Telerik.Sitefinity.Web.UI;
    using Telerik.Sitefinity.Web.UI.ControlDesign;

    [RequireScriptManager]
    [ControlDesigner(typeof(SliderWidgetDesigner))]
    public class SliderWidget : SimpleView
    {
        public string Data { get; set; } = string.Empty;

        protected virtual Literal Slider => this.Container.GetControl<Literal>("ltSlider", true);

        protected override void InitializeControls(GenericContainer container)
        {
            try
            {
                List<Section> sections = new JavaScriptSerializer().Deserialize<List<Section>>(this.Data);
                var sWriter = new StringWriter();
                var writer = new HtmlTextWriter(sWriter);

                foreach (var section in sections)
                {
                    System.Uri sUrl = new System.Uri(section.Image);

                    if (Convert.ToInt32(section.Index) == 0)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "first_img");
                    }
                    else if (Convert.ToInt32(section.Index) == sections.Count - 1)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "last_img");
                        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display:none;");
                    }
                    if (Convert.ToInt32(section.Index) == 0)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display:none;");
                    }
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, sUrl.AbsolutePath);
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, section.Index);
                    writer.AddAttribute("data-caption", section.Title);
                    writer.AddAttribute("data-link", GetUrl(section));
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag(); // img
                }
                this.Slider.Text = sWriter.ToString();
            }
            catch { }
        }


        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.Slider.Resources.SliderWidget.ascx";
            }
        }

        private static string GetUrl(Section section)
        {
            if (section.URLGuid != Guid.Empty)
            {
                var page = CaseManagers.Pages.GetById(section.URLGuid);
                if (page != null)
                {
                    return page.Url;
                }
            }
            return section.URL;
        }

        private string GetTabName(string tabNumber)
        {
            return "TabNumber" + Regex.Replace(tabNumber, @"\s+", string.Empty);
        }

        public class Section
        {
            public string Title;
            public string Name;
            public string Index;
            public string URL;
            public string Image;
            public Guid URLGuid;
        }
    }
}
