﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Sitefinity.Modules.Libraries.Web.UI.Designers;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ControlDesign;
using Telerik.Sitefinity.Web.UI.Fields;
using Telerik.Sitefinity.Web.UI.Fields.Enums;
using Telerik.Web.UI;

namespace Case.Framework.Sitefinity.Widgets.Slider
{
    class SliderWidgetDesigner : ControlDesignerBase
    {

        protected virtual MediaContentSelectorView SelectorView
        {
            get
            {
                return this.Container.GetControl<MediaContentSelectorView>("selectorView", true);
            }
        }

        protected internal virtual PagesSelector PageSelectorPageId
        {
            get
            {
                return this.Container.GetControl<PagesSelector>("pageSelectorPageId", true);
            }
        }

        public HtmlGenericControl SelectorTagPageId
        {
            get
            {
                return this.Container.GetControl<HtmlGenericControl>("selectorTagPageId", true);
            }
        }

        protected override void InitializeControls(Telerik.Sitefinity.Web.UI.GenericContainer container)
        {
            PropertyEditor.HideAdvancedMode = true;
        }

        protected override string LayoutTemplateName
        {
            get
            {
                return "Case.Framework.Sitefinity.Widgets.Slider.Resources.SliderWidgetDesigner.ascx";
            }
        }

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            var scriptDescriptors = new List<ScriptDescriptor>(base.GetScriptDescriptors());
            var desc = (ScriptControlDescriptor)scriptDescriptors.Last();
            desc.AddComponentProperty("selectorView", SelectorView.ClientID);
            desc.AddComponentProperty("pageSelectorPageId", this.PageSelectorPageId.ClientID);
            desc.AddElementProperty("selectorTagPageId", this.SelectorTagPageId.ClientID);
            return scriptDescriptors.ToArray();
        }
        public override IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences()
        {
            var res = new List<System.Web.UI.ScriptReference>(base.GetScriptReferences());
            var assemblyName = this.GetType().Assembly.GetName().ToString();
            res.Add(new ScriptReference("Case.Framework.Sitefinity.Widgets.Slider.Resources.SliderWidgetDesigner.js", assemblyName));
            return res.ToArray();
        }
        private string imageServiceUrl = VirtualPathUtility.ToAbsolute("~/Sitefinity/Services/Content/ImageService.svc/");
    }
}
