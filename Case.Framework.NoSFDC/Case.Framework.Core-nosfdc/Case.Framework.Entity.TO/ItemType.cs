﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Case.Framework.Entity.TO
{
    public class ItemType
    {
        public int ItemTypeId { get; set; }
        public string Name { get; set; }
        public bool? Default { get; set; }
      
        //<fkEventId>64478</fkEventId>
        //<calendarId>0</calendarId>
        
    }
}
