﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Entity.TO
{
    public class CalendarTO
    {
        public int CalendarId { get; set; }
        public string CalendarName { get; set; }
        public IEnumerable<EventWsTO> Events{ get; set; }
    }
}
