﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Entity.TO
{
    public class EventWsTO
    {
        public int EventId { get; set; }
        public bool? Recurrence { get; set; }
        public int RecurrenceId { get; set; }
        public CalendarTO Calendar { get; set; }
        public bool? Rss { get; set; }
        public bool? PublicEngagement { get; set; }
        public string TitleLong { get; set; }
        public string TitleShort { get; set; }
        public string TitleLink { get; set; }
        public string EventType { get; set; }
        public string Sponsor { get; set; }
        
        public bool? DateDisplay { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TimeType { get; set; }
        public string EndTimeLabel { get; set; }
        
        public string LocationText { get; set; }
        public string LocationHtml { get; set; }
        public string SearchTerms { get; set; }
        public string Description { get; set; }
        
        public string Speaker { get; set; }
        
        public string RegistrationLabel { get; set; }
        public string RegistrationUrl { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string ContactName { get; set; }
        
        public string CostText { get; set; }
        public string CostHtml { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string EditedBy{ get; set; }
        public DateTime EditedDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; } 

    }
}
