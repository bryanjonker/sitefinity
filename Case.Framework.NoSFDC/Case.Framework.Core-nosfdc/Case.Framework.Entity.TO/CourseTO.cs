﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIUC.Custom.Entity.TO.CourseSubsets;

namespace UIUC.Custom.Entity.TO
{
    public class CourseTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public string Subject { get; set; }
        public string Number { get; set; }
        public string Formats { get; set; }
        public string FormatSearch { get; set; }
        public string Subject_name { get; set; }
        public string Terms_offered { get; set; }
        public decimal Credit_hours_min { get; set; }
        public decimal Credit_hours_max { get; set; }
        public string Instructors { get; set; }

        public CourseTO() { }

        public CourseTO(IEnumerable<CourseDatabase> courses, IEnumerable<DescriptionDatabase> descriptions, IEnumerable<InstructorDatabase> instructors)
        {
            if (courses != null)
            {
                var course = courses.FirstOrDefault();
                if (course != null)
                {
                    this.Title = course.Title;
                    this.Department = course.Department;
                    this.Subject = course.Subject;
                    this.Number = course.Number;
                    this.Subject_name = course.Subject_name;
                    this.Credit_hours_max = course.Credit_hours_max;
                    this.Credit_hours_min = course.Credit_hours_min;
                    this.Formats = string.Join(",", courses.Select(c => c.Format).Distinct());
                    this.Terms_offered = string.Join(",", courses.Select(c => c.TermDescription).Distinct());
                }
                if (descriptions != null)
                {
                    var description = descriptions.OrderByDescending(d => d.Terms).FirstOrDefault();
                    if (description != null)
                    {
                        this.Description = description.Description;
                    }
                }
                if (instructors != null && instructors.Any())
                {
                    this.Instructors = string.Join(",", instructors.Select(i => i.Uin).Distinct());
                }
            }
        }
    }
}
