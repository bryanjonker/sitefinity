﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIUC.Custom.Entity.TO.CourseSubsets
{
    public class CourseDatabase : BaseDatabase
    {
        public string Department { get; set; }
        public string Title { get; set; }
        public string Subject_name { get; set; }
        public decimal Credit_hours_min { get; set; }
        public decimal Credit_hours_max { get; set; }
        public string Schedule_type_code { get; set; }
        public string Part_of_term_code { get; set; }
        public string Terms { get; set; }

        public string Format
        {
            get
            {
                return this.Schedule_type_code.Equals("ONL", StringComparison.OrdinalIgnoreCase) ?
                    "Online" :
                    this.Part_of_term_code.Equals("XM", StringComparison.OrdinalIgnoreCase) ?
                    "Off-Campus" : "On Campus (Urbana)";
            }
        }

        public string TermDescription
        {
            get
            {
                return this.Terms.EndsWith("1") ?
                    "Spring" :
                    this.Terms.EndsWith("5") ?
                    "Summer" : "Fall";
            }
        }
    }
}