﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIUC.Custom.Entity.TO.CourseSubsets
{
    public class DescriptionDatabase : BaseDatabase
    {
        public string Terms { get; set; }
        public string Description { get; set; }
    }
}
