﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIUC.Custom.Entity.TO.CourseSubsets
{
    public abstract class BaseDatabase
    {
        public string Subject { get; set; }
        public string Number { get; set; }
    }
}
