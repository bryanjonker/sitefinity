﻿using System;
using System.Runtime.CompilerServices;

namespace Case.Framework.Sitefinity.Mvc.Web.ViewModels
{
    public class PageTitleViewModel
    {
        public string TextTag
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public PageTitleViewModel()
        {
        }
    }
}