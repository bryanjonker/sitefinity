﻿using Case.Framework.Sitefinity.Content.Managers;
using Case.Framework.Sitefinity.Data;
using Case.Framework.Sitefinity.Mvc.Web.Controllers.Abstracts;
using Case.Framework.Sitefinity.Mvc.Web.ViewModels;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace Case.Framework.Sitefinity.Mvc.Web.Controllers
{
    public class PageTitleController : BaseController
    {
        public bool ShowParentTitle
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string TextTag
        {
            get;
            set;
        }

        public PageTitleController()
        {
            this.TextTag = "h1";
        }

        public ActionResult Index()
        {
            PageTitleViewModel pageTitleViewModel = new PageTitleViewModel()
            {
                TextTag = this.TextTag
            };
            if (!string.IsNullOrEmpty(this.Text))
            {
                pageTitleViewModel.Title = this.Text;
            }
            else
            {
                try
                {
                    SiteMapNode currentSiteMapNode = CaseManagers.Pages.GetCurrentSiteMapNode();
                    if (currentSiteMapNode != null)
                    {
                        pageTitleViewModel.Title = (!this.ShowParentTitle || currentSiteMapNode.ParentNode == null ? currentSiteMapNode.Title : currentSiteMapNode.ParentNode.Title);
                    }
                }
                catch (Exception exception)
                {
                    ErrorHelper.LogException(exception);
                }
            }
            return this.EmbeddedView(pageTitleViewModel);
        }
    }
}