﻿using Case.Framework.Core.Extensions;
using System;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace Case.Framework.Sitefinity.Mvc.Web.Controllers.Abstracts
{
    public abstract class BaseController : Controller
    {
        private string _virtualPathRoot = "~/CaseMvcSF";

        public BaseController()
        {
        }

        public BaseController(string virtualPathRoot)
        {
            this._virtualPathRoot = virtualPathRoot;
        }

        public virtual ViewResult EmbeddedView(string viewName, object model, bool autoPrefixNamespace = false)
        {
            string str = this._virtualPathRoot;
            char[] chrArray = new char[] { '~', '/' };
            string str1 = str.TrimStart(chrArray);
            char[] chrArray1 = new char[] { '/' };
            string str2 = string.Concat("../../../", str1.TrimEnd(chrArray1), "/");
            string str3 = (autoPrefixNamespace ? string.Concat(base.GetType().FullName.Replace(string.Concat(".Controllers.", base.GetType().Name), string.Concat(".Views.", base.GetType().Name.TrimEnd("Controller"))), ".") : string.Empty);
            return base.View(string.Concat(str2, str3, viewName), model);
        }

        public virtual ViewResult EmbeddedView(object model)
        {
            string str = base.RouteData.Values["action"].ToString();
            return this.EmbeddedView(str, model, true);
        }
    }
}