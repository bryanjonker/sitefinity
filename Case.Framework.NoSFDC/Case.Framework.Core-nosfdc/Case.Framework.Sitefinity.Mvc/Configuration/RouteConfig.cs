﻿namespace Case.Framework.Sitefinity.Mvc.Configuration
{
    using System.Web.Routing;
    using System.Web.Mvc;

    public class RouteConfig
    {
        public static void RegisterAll(RouteCollection routes)
        {
            RouteTable.Routes.MapRoute("CaseClassicDefault", "mvcroute", new { controller = "Home", action = "Index" });
            RouteTable.Routes.MapRoute("CaseClassicControllerActionId", "mvcroute/{controller}/{action}/{id}", null, new { action = "^[a-zA-Z]+[_ a-zA-Z0-9]*$", id = "^\\d+$" });
            RouteTable.Routes.MapRoute("CaseClassicControllerAction", "mvcroute/{controller}/{action}", null, new { action = "^[a-zA-Z]+[_ a-zA-Z0-9]*$" });
            RouteTable.Routes.MapRoute("CaseClassicControllerId", "mvcroute/{controller}/{id}", new { action = "Detail" }, new { id = "^\\d+$" });
            var variable = new { action = "Index" };
            string[] strArrays = { "GET" };
            RouteTable.Routes.MapRoute("CaseClassicControllerGet", "mvcroute/{controller}", variable, new { controller = "^[a-zA-Z]+[_ a-zA-Z0-9]*$", httpMethod = new HttpMethodConstraint(strArrays) });
        }
    }
}