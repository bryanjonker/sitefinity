﻿using System;

namespace Case.Framework.Sitefinity.Mvc
{
    public static class Constants
    {
        public const string VALUE_CLASSIC_MVC_ROOT_PATH = "mvcroute";

        public const string VALUE_CUSTOM_VIRTUAL_ROOT_PATH = "~/CaseMvcSF";

        public const string KEY_MVC_ENABLE_CLASSIC_ROUTES = "Case.Framework.Sitefinity.Mvc.EnableClassicRoutes";
    }
}