﻿using Case.Framework.Core.Utilities;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Telerik.Sitefinity.Modules.Pages;

namespace Case.Framework.Sitefinity.Mvc.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static void AddCssFile(this HtmlHelper html, string path, string id = null)
        {
            WebHelper.IncludeCss(path, null, id);
        }

        public static void AddCssResource(this HtmlHelper html, string resource)
        {
            html.AddCssFile(Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl(resource, null), resource);
        }

        public static void AddScriptFile(this HtmlHelper html, string path, bool head = false, string id = null)
        {
            WebHelper.IncludeJs(path, head, id);
        }

        public static void AddScriptReference(this HtmlHelper html, ScriptRef reference)
        {
            Page currentHandler = HttpContext.Current.CurrentHandler as Page;
            if (currentHandler != null)
            {
                PageManager.ConfigureScriptManager(currentHandler, reference);
            }
        }

        public static void AddScriptResource(this HtmlHelper html, string resource, bool head = false, string id = null)
        {
            html.AddScriptFile(Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl(resource, null), head, resource);
        }
    }
}