﻿using System;
using System.Web;
using System.Web.Routing;

namespace Case.Framework.Sitefinity.Mvc.Constraints
{
    public class GuidConstraint : IRouteConstraint
    {
        public GuidConstraint()
        {
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            object obj;
            Guid guid;
            if (!values.TryGetValue(parameterName, out obj))
            {
                return false;
            }
            if (obj is Guid)
            {
                return true;
            }
            if (!Guid.TryParse(Convert.ToString(obj), out guid))
            {
                return false;
            }
            return guid != Guid.Empty;
        }
    }
}