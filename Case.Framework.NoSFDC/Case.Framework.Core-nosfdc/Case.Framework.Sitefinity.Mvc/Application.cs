﻿using Case.Framework.Core.Configuration;
using Case.Framework.Sitefinity;
using Case.Framework.Sitefinity.Models.Abstracts;
using Case.Framework.Sitefinity.Mvc.Configuration;
using Case.Framework.Sitefinity.Mvc.Routes;
using Case.Framework.Sitefinity.Mvc.Web.Controllers;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Collections.ObjectModel;
using System.Web.Routing;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Data;

namespace Case.Framework.Sitefinity.Mvc
{
    public class Application : BaseApplication<Application>
    {
        public Application()
        {
        }

        protected static void OnBootstrapperInitialized(object sender, ExecutedEventArgs e)
        {
            if (e.CommandName == "RegisterRoutes")
            {
                string str = PageHelper.GetScriptsPath().TrimStart(new char[] { '/' });
                RouteTable.Routes.Add(new Route(string.Concat(str, "/baseurl"), new UrlModule()));
                RouteTable.Routes.Add(new Route(string.Concat(str, "/basemvcurl"), new MvcUrlModule()));
                RouteTable.Routes.Add(new Route(string.Concat(str, "/basescriptsurl"), new ScriptsUrlModule()));
                RouteTable.Routes.Add(new Route(string.Concat(str, "/main"), new RequireJsConfigModule()));
            }
            if (e.CommandName == "Bootstrapped")
            {
                int? nullable = null;
                ConfigHelper.RegisterToolboxWidget<PageTitleController>("Page Title", "Dynamically displays title of the currently viewed page.", null, null, "", "Extras", nullable, ToolboxType.PageControls);
            }
        }

        protected static void OnBootstrapperInitializing(object sender, ExecutingEventArgs e)
        {
            if (e.CommandName == "RegisterRoutes")
            {
                ConfigHelper.RegisterVirtualPath("~/CaseMvcSF/*", "Case.Framework.Sitefinity.Mvc");
            }
        }

        public static void PreInit()
        {
            BaseApplication<Application>.RegisterStartup();
            RouteConfig.RegisterAll(RouteTable.Routes);
            Bootstrapper.Initializing += new EventHandler<ExecutingEventArgs>(Application.OnBootstrapperInitializing);
            Bootstrapper.Initialized += new EventHandler<ExecutedEventArgs>(Application.OnBootstrapperInitialized);
        }
    }
}