﻿using Case.Framework.Core.Utilities;
using System;
using System.Runtime.CompilerServices;
using System.Web;

namespace Case.Framework.Sitefinity.Mvc.Routes.Abstracts
{
    public class BaseScriptModule : BaseScript
    {
        public virtual bool IncludeQuotes
        {
            get;
            set;
        }

        public virtual string Key
        {
            get;
            set;
        }

        public BaseScriptModule()
        {
        }

        public override string ProcessValue(HttpContext context)
        {
            return WebHelper.ToJsModule(this.Key,
                (this.IncludeQuotes ? string.Concat("'", this.Value, "'") : this.Value));
        }
    }
}