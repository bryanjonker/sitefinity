﻿using System;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Routing;

namespace Case.Framework.Sitefinity.Mvc.Routes.Abstracts
{
    public class BaseScript : IHttpHandler, IRouteHandler
    {
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public virtual string Value
        {
            get;
            set;
        }

        public BaseScript()
        {
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/javascript";
            context.Response.Write(this.ProcessValue(context));
        }

        public virtual string ProcessValue(HttpContext context)
        {
            return this.Value;
        }
    }
}