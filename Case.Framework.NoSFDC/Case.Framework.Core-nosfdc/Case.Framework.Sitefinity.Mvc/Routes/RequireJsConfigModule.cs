﻿using Case.Framework.Core.Extensions;
using Case.Framework.Core.Utilities;
using Case.Framework.Sitefinity;
using Case.Framework.Sitefinity.Mvc.Routes.Abstracts;
using Case.Framework.Sitefinity.Utilities;
using System;
using System.Web;

namespace Case.Framework.Sitefinity.Mvc.Routes
{
    public class RequireJsConfigModule : BaseScript
    {
        public RequireJsConfigModule()
        {
        }

        public override string ProcessValue(HttpContext context)
        {
            string embeddedResource = FileHelper.GetEmbeddedResource("Case.Framework.Sitefinity.Resources.Scripts.main.js", typeof(Case.Framework.Sitefinity.Application));
            string str = embeddedResource.EscapeForFormat();
            object[] scriptsPath = new object[] { Case.Framework.Sitefinity.Utilities.PageHelper.GetScriptsPath(), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Telerik.Sitefinity.Resources.Scripts.jquery.cookie.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Telerik.Sitefinity.Resources.Scripts.RequireJS.text.js", null),
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.require.css.min.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.moment.moment.min.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.moment.moment-timezone-with-data.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.underscore.lodash.underscore.min.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.underscore.underscore.string.min.js", null),
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.glide.jquery.glide.min.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.js-url.url.min.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.lostorage.loStorage.min.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.case.api.js", null), 
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.case.helpers.js", null),
                Case.Framework.Sitefinity.Utilities.PageHelper.GetWebResourceUrl("Case.Framework.Sitefinity.Resources.Scripts.case.alerts.js", null) };
            return string.Format(str, scriptsPath);
           
        }
    }
}