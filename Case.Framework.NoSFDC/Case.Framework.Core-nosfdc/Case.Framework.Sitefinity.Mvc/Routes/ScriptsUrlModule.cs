﻿using Case.Framework.Sitefinity.Utilities;
using System;

namespace Case.Framework.Sitefinity.Mvc.Routes
{
    public class ScriptsUrlModule : UrlModule
    {
        public override string Key
        {
            get
            {
                return "basescriptsurl";
            }
        }

        public override string Value
        {
            get
            {
                return PageHelper.GetScriptsPath();
            }
        }

        public ScriptsUrlModule()
        {
        }
    }
}