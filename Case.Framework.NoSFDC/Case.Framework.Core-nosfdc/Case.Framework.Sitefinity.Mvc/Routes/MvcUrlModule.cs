﻿using System;
using System.Web;

namespace Case.Framework.Sitefinity.Mvc.Routes
{
    public class MvcUrlModule : UrlModule
    {
        public override string Key
        {
            get
            {
                return "basemvcurl";
            }
        }

        public override string Value
        {
            get
            {
                return VirtualPathUtility.ToAbsolute("~/mvcroute");
            }
        }

        public MvcUrlModule()
        {
        }
    }
}