﻿using Case.Framework.Sitefinity.Mvc.Routes.Abstracts;
using System;
using System.Web.Hosting;

namespace Case.Framework.Sitefinity.Mvc.Routes
{
    public class UrlModule : BaseScriptModule
    {
        public override string Key
        {
            get
            {
                return "baseurl";
            }
        }

        public override string Value
        {
            get
            {
                return HostingEnvironment.ApplicationVirtualPath;
            }
        }

        public UrlModule()
        {
            this.IncludeQuotes = true;
        }
    }
}