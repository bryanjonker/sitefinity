﻿namespace UIUC.Custom.Sitefinity.ContentManager
{
    using Case.Framework.Core.Utilities;
    using Case.Framework.Entity.TO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Telerik.Sitefinity;
    using Telerik.Sitefinity.DynamicModules;
    using Telerik.Sitefinity.DynamicModules.Model;
    using Telerik.Sitefinity.Events.Model;
    using Telerik.Sitefinity.GenericContent.Model;
    using Telerik.Sitefinity.Model;
    using Telerik.Sitefinity.Modules.Events;
    using Telerik.Sitefinity.Taxonomies;
    using Telerik.Sitefinity.Taxonomies.Model;
    using Telerik.Sitefinity.Utilities.TypeConverters;

    public class EventContentManager
    {
        public static void CreateEvent(EventWsTO pEvent)
        {
            var eventIdGuid = Guid.NewGuid();

            EventsManager eventsManager = new EventsManager();
            eventsManager.Provider.SuppressSecurityChecks = true;
            Event oEvent;
            bool needCheckin;
            var existingEvent = eventsManager.GetEvents().Where(x => x.GetValue<decimal?>("EventId") == pEvent.EventId && x.Status == ContentLifecycleStatus.Master).ToList().FirstOrDefault(x => !x.IsDeleted);

            if (existingEvent == null)
            {
                oEvent = eventsManager.CreateEvent(eventIdGuid);
                needCheckin = false;
            }
            else if (pEvent.EditedDate.Date > existingEvent.PublicationDate.Date)
            {
                oEvent = eventsManager.Lifecycle.CheckOut(existingEvent) as Event;
                needCheckin = true;
            }
            else
            {
                return;
            }

            if (oEvent == null)
            {
                return;
            }

            oEvent.Title = pEvent.TitleLong.TruncateString(250, SitefinityExtensions.TruncateOptions.FinishWord);
            oEvent.UrlName = Regex.Replace(pEvent.TitleShort.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");
            oEvent.Country = "USA";
            oEvent.EventStart = pEvent.StartDate;
            oEvent.EventEnd = pEvent.StartDate > pEvent.EndDate ? pEvent.StartDate : pEvent.EndDate;
            oEvent.PublicationDate = DateTime.Today;
            oEvent.ExpirationDate = DateTime.Today.AddYears(5);
            oEvent.ContactEmail = pEvent.ContactEmail;
            oEvent.ContactPhone = pEvent.ContactPhone;

            oEvent.Content = pEvent.Description;
            if (pEvent.TimeType == "ALL_DAY")
            {
                oEvent.AllDayEvent = true;
            }

            if (pEvent.Recurrence != null)
            {
                oEvent.IsRecurrent = pEvent.Recurrence.Value;
            }

            //Custom fields
            oEvent.SetValue("TitleLong", pEvent.TitleLong);
            oEvent.SetValue("EventId", pEvent.EventId);
            oEvent.SetValue("TitleLink", pEvent.TitleLink);
            oEvent.SetValue("CostText", pEvent.CostText);
            oEvent.SetValue("TimeType", pEvent.TimeType);
            oEvent.SetValue("StartTime", pEvent.StartTime);
            oEvent.SetValue("EndTime", pEvent.EndTime);
            oEvent.SetValue("SponsorText", pEvent.Sponsor);
            oEvent.SetValue("CreatedDate", pEvent.CreatedDate);
            oEvent.SetValue("EditedDate", pEvent.EditedDate);

            oEvent.SetValue("LocationLong", HtmlRemovalHelper.StripTagsRegexCompiled(pEvent.LocationText));
            oEvent.SetValue("ContactNameLong", pEvent.ContactName);

            using (var taxonomyManager = TaxonomyManager.GetManager())
            {
                taxonomyManager.Provider.SuppressSecurityChecks = true;
                oEvent.Organizer.ClearAll();
                var fixUrlRegex = new Regex(@"[^\w\-\!\$\'\(\)\=\@\d_,]+", RegexOptions.Compiled);
                foreach (var fullUnit in GetUnits(pEvent.EventType))
                {
                    var unitName = fullUnit.GetValue("unit_name").ToString();
                    var taxon = taxonomyManager.GetTaxa<HierarchicalTaxon>().SingleOrDefault(t => t.Title.ToString() == unitName);
                    if (taxon == null)
                    {
                        var unitId = fullUnit.GetValue("unit_id").ToString();
                        taxon = taxonomyManager.GetTaxa<HierarchicalTaxon>().SingleOrDefault(t => t.Title.ToString() == unitId);
                    }
                    if (taxon == null)
                    {
                        taxon = taxonomyManager.CreateTaxon<HierarchicalTaxon>();
                        taxon.Name = unitName;
                        taxon.Title = unitName;
                        taxon.UrlName = fixUrlRegex.Replace(unitName, "-");
                        var facultyTaxonomy = taxonomyManager.GetTaxa<HierarchicalTaxon>().SingleOrDefault(s => s.Name == "Units");
                        taxon.Taxonomy = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>().SingleOrDefault(s => s.Name == "Categories");
                        taxon.Parent = facultyTaxonomy;
                        taxonomyManager.SaveChanges();
                    }
                    oEvent.Organizer.AddTaxa("Category", taxon.Id);
                }
            }

            ValidateUrl(pEvent, eventsManager, oEvent);

            oEvent.SetWorkflowStatus(eventsManager.Provider.ApplicationName, "Published");
            // Save the changes
            eventsManager.SaveChanges();

            if (needCheckin)
            {
                eventsManager.Lifecycle.Publish(eventsManager.Lifecycle.CheckIn(oEvent));
            }
            else
            {
                eventsManager.Lifecycle.Publish(oEvent);
            }
        }

        private static void ValidateUrl(EventWsTO pEvent, EventsManager eventsManager, Event oEvent, int count = 1)
        {
            try
            {
                eventsManager.RecompileAndValidateUrls(oEvent);
            }
            catch (Exception)
            {
                oEvent.UrlName = Regex.Replace(string.Concat(pEvent.TitleLong.ToLower(), count), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");
                ValidateUrl(pEvent, eventsManager, oEvent, count++);
            }
        }

        private static IEnumerable<DynamicContent> GetUnits(string eventType)
        {
            var returnValue = new List<DynamicContent>();

            if (eventType.Equals("College of Education"))
            {
                return returnValue;
            }

            var units = LoadUnits();

            var tempValue = GetBaseUnitFromNameOrTitle(units, eventType);
            if (tempValue != null)
            {
                returnValue.Add(tempValue);
            }

            while (tempValue != null)
            {
                tempValue = GetParentUnit(units, tempValue);
                if (tempValue != null)
                {
                    returnValue.Add(tempValue);
                }
            }
            return returnValue;
        }

        private static IEnumerable<DynamicContent> LoadUnits()
        {
            var unitType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Unit.Unit");
            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
            return dynamicModuleManager.GetDataItems(unitType).ToList().Where(x => (!x.IsDeleted) && x.Status == ContentLifecycleStatus.Master).ToList();
        }

        private static DynamicContent GetBaseUnitFromNameOrTitle(IEnumerable<DynamicContent> units, string eventType)
        {
            var tempValue = units.FirstOrDefault(u => u.GetValue("unit_id").ToString().Equals(eventType, StringComparison.OrdinalIgnoreCase));
            return tempValue ?? units.FirstOrDefault(u => u.GetValue("unit_name").ToString().Equals(eventType, StringComparison.OrdinalIgnoreCase));
        }

        private static DynamicContent GetParentUnit(IEnumerable<DynamicContent> units, DynamicContent content)
        {
            var parentId = content.GetValue("parent_ed_units_id");
            return parentId == null ? null : units.FirstOrDefault(u => u.GetValue("ed_units_id").ToString() == parentId.ToString());
        }
    }
}