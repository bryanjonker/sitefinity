﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch
{
    using Dapper;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class ImportProcess
    {
        public string ImportCourseData()
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UIUC_Proc_TablesConnection"].ConnectionString))
            {
                using (var rowsCourses = conn.QueryMultiple("webpublic.courses", commandTimeout: 0))
                {
                    var finalCourseList = rowsCourses.Read().Select(f => new Course(f)).ToList()
                        .GroupBy(c => new { c.CourseNumber, c.Rubric }).Select(course => new Course(course)).ToList();
                    var descriptions = rowsCourses.Read();
                    foreach (var description in descriptions.Where(d => finalCourseList.Any(f => f.CourseNumber == d.number.ToString() && f.Rubric == d.subject.ToString())).OrderByDescending(d => d.terms.ToString()))
                    {
                        finalCourseList.Single(f => f.CourseNumber == description.number.ToString() && f.Rubric == description.subject.ToString()).AddDescription(description.description.ToString());
                    }
                    var external = new Faculty.ExportProcess();
                    var sections = rowsCourses.Read();
                    var i = 1;
                    foreach (var section in sections.Where(d => finalCourseList.Any(f => f.CourseNumber == d.CRS_NBR.ToString() && f.Rubric == d.CRS_SUBJ_CD.ToString())))
                    {
                        var internalFaculty = section.UIN == null ? null : external.GetFacultySingleByUin(section.UIN.ToString());
                        var username = internalFaculty == null ? string.Empty : internalFaculty.Username;
                        var lastName = internalFaculty == null ? (section.PERS_LNAME == null ? string.Empty : section.PERS_LNAME.ToString()) : internalFaculty.LastName;
                        var firstName = internalFaculty == null ? (section.PERS_FNAME == null ? string.Empty : section.PERS_FNAME.ToString()) : internalFaculty.FirstName;
                        finalCourseList.Single(f => f.CourseNumber == section.CRS_NBR.ToString() && f.Rubric == section.CRS_SUBJ_CD.ToString()).AddSection(new CourseSection(section, i++, username, firstName, lastName));
                    }
                    finalCourseList.ForEach(f => f.FixFormats());
                    using (var context = new FacetedSearchContext())
                    {
                        context.Database.ExecuteSqlCommand("facetedsearch.ResetCourses");
                        foreach (var course in finalCourseList)
                        {
                            context.Database.ExecuteSqlCommand("facetedsearch.DeleteCourses_Single @Rubric, @CourseNumber", new SqlParameter("Rubric", course.Rubric), new SqlParameter("CourseNumber", course.CourseNumber));
                        }
                        context.Courses.AddRange(finalCourseList);
                        context.SaveChanges();
                    }
                }
            }
            return string.Empty;
        }

        public void ImportCredential(string title, CredentialType credentialType, bool isOnline, bool isOnCampus, bool isOffCampus)
        {
            using (var context = new FacetedSearchContext())
            {
                var program = context.Programs.SingleOrDefault(x => x.Name == title);
                if (program != null)
                {
                    var credential = program.Credentials.FirstOrDefault(c => c.CredentialType == credentialType);
                    if (credential == null && (isOnCampus || isOffCampus || isOnline))
                    {
                        program.Credentials.Add(new Credential
                        {
                            CredentialType = credentialType,
                            IsOnline = isOnline,
                            IsOffCampus = isOffCampus,
                            IsOncampus = isOnCampus
                        });
                    }
                    else if (credential != null && (isOnCampus || isOffCampus || isOnline))
                    {
                        credential.IsOnline = isOnline;
                        credential.IsOffCampus = isOffCampus;
                        credential.IsOncampus = isOnCampus;
                    }
                    else if (credential != null)
                    {
                        context.Credentials.Remove(credential);
                    }
                    context.SaveChanges();
                }
            }
        }

        public void ImportSingleProgram(string oldTitle, string name, string description, string onlineUrl, string url, DepartmentType department, bool isActive)
        {
            using (var context = new FacetedSearchContext())
            {
                var program = context.Programs.SingleOrDefault(x => x.Name == name);
                if (program == null)
                {
                    context.Database.ExecuteSqlCommand("UPDATE facetedsearch.credential SET Name = @newName WHERE name = @oldName; UPDATE facetedsearch.program SET Name = @newName WHERE name = @oldName", new SqlParameter("newName", name), new SqlParameter("oldName", oldTitle));
                    program = context.Programs.SingleOrDefault(x => x.Name == name);
                }
                if (program == null)
                {
                    context.Programs.Add(new Program
                    {
                        Name = name,
                        Description = description,
                        Url = url,
                        OnlineUrl = onlineUrl,
                        Department = department,
                        IsActive = isActive
                    });
                }
                else
                {
                    program.Name = name;
                    program.Description = description;
                    program.Url = url;
                    program.OnlineUrl = onlineUrl;
                    program.Department = department;
                    program.IsActive = isActive;
                }
                context.SaveChanges();
            }
        }
    }
}