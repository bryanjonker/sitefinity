﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch
{
    using System.Data.Entity;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class FacetedSearchContext : DbContext
    {
        public FacetedSearchContext()
            : base("SitefinityDirect")
        {
            // Database.SetInitializer<FacetedSearchContext>(null);
        }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Credential> Credentials { get; set; }

        public DbSet<Program> Programs { get; set; }

        public DbSet<CourseSection> Sections { get; set; }
    }
}