﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("facetedsearch.course")]
    public class Course
    {
        public Course() {}

        public Course(dynamic data)
        {
            this.CourseNumber = data.number.ToString();
            DepartmentType tempDepartmentType;
            this.DepartmentType = Enum.TryParse(data.department.ToString(), true, out tempDepartmentType) ? tempDepartmentType : DepartmentType.None;
            this.MinCreditHours = data.credit_hours_min.ToString("0");
            this.MaxCreditHours = data.credit_hours_max == null ? data.credit_hours_min.ToString("0") : data.credit_hours_max.ToString("0");
            this.Formats = ConvertToFormat(data.schedule_type_code.ToString(), data.part_of_term_code.ToString());
            this.Rubric = data.subject.ToString();
            this.TermsOffered = ConvertToTerm(data.terms.ToString());
            this.Title = data.title.ToString();
            this.IsActive = true;
        }

        public Course(IEnumerable<Course> courses)
        {
            this.CourseNumber = courses.First().CourseNumber;
            this.Rubric = courses.First().Rubric;
            this.Title = courses.First().Title;
            this.DepartmentType = courses.First().DepartmentType;
            this.MinCreditHours = courses.First(f => f.MinCreditHours != null).MinCreditHours;
            this.MaxCreditHours = courses.First(f => f.MaxCreditHours != null).MaxCreditHours;
            this.Formats = string.Join(", ", courses.Select(x => x.Formats).Distinct().OrderBy(x => x));
            this.TermsOffered = string.Join(", ", courses.Select(x => x.TermsOffered).Distinct().OrderBy(x => x));
            this.CreditHours = this.MinCreditHours == this.MaxCreditHours ? this.MinCreditHours : $"{this.MinCreditHours}-{this.MaxCreditHours}";
            this.IsActive = true;
        }

        [Key]
        [Column(Order = 1)]
        public string CourseNumber { get; set; }

        public string CreditHours { get; set; }

        public DepartmentType DepartmentType { get; set; }

        public string Description { get; set; }

        public string Formats { get; set; }

        public bool IsActive { get; set; }

        [NotMapped]
        public string MaxCreditHours { get; set; }

        [NotMapped]
        public string MinCreditHours { get; set; }

        [Key]
        [Column(Order = 0)]
        public string Rubric { get; set; }

        public virtual ICollection<CourseSection> Sections { get; set; }

        public string TermsOffered { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public void AddDescription(string s)
        {
            if (string.IsNullOrEmpty(this.Description))
            {
                this.Description = s;
            }
        }

        public void AddSection(CourseSection section)
        {
            if (this.Sections == null)
            {
                this.Sections = new List<CourseSection>();
            }
            section.Course = $"{this.Rubric} {this.CourseNumber}";
            this.Sections.Add(section);
        }

        public void FixFormats()
        {
            this.Formats = string.Join(", ", this.Sections.Select(x => x.Format).Distinct().OrderBy(x => x));
        }

        private static string ConvertToFormat(string format, string partTermCode)
        {
            if (format == "ONL")
            {
                return "Online";
            }
            if (partTermCode == "XM")
            {
                return "Off Campus";
            }
            return "On Campus (Urbana)";
        }

        private static string ConvertToTerm(string s)
        {
            if (s.EndsWith("1"))
            {
                return "Spring";
            }
            if (s.EndsWith("5"))
            {
                return "Summer";
            }
            if (s.EndsWith("8"))
            {
                return "Fall";
            }
            return string.Empty;
        }
    }
}