﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("facetedsearch.credential")]
    public class Credential
    {
        public string Concentration { get; set; }

        public CredentialType CredentialType { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string License { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public bool IsOnline { get; set; }

        public bool IsOncampus { get; set; }

        public bool IsOffCampus { get; set; }

        public string CredentialTypeString
        {
            get
            {
                if (this.CredentialType == CredentialType.Grad_Minor)
                {
                    return "Graduate Minor";
                }
                if (this.CredentialType == CredentialType.Undergrad_Minor)
                {
                    return "Undergraduate Minor";
                }
                if (this.CredentialType == CredentialType.Grad_Concentration)
                {
                    return "Graduate Concentration";
                }
                return this.CredentialType.ToString().Replace("_", " ");
            }
        }
    }
}