﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("facetedsearch.section")]
    public class CourseSection
    {
        public CourseSection()
        {
        }

        public CourseSection(dynamic data, int key, string username, string firstName, string lastName)
        {
            this.BeginDate = (DateTime)data.SECT_START_DT;
            this.EndDate = (DateTime)data.SECT_END_DT;
            this.Building = data.BLDG_DESC.ToString();
            this.Room = data.SECT_ROOM_NBR.ToString();
            this.ScheduleType = data.SCHED_TYPE_DESC.ToString();
            this.SectionNumber = data.SECT_NBR.ToString();
            this.SectionTitle = data.CRS_TITLE.ToString();
            this.SequenceNumber = data.SECT_SEQ_NBR.ToString();
            this.Time = string.IsNullOrWhiteSpace(data.SECT_BGN_TIME.ToString())
                            ? string.Empty
                            : $"{data.SECT_BGN_TIME.ToString()} - {data.SECT_END_TIME.ToString()}";
            this.Days = CreateDayString(
                data.SECT_SUNDAY_MEET_IND.ToString(),
                data.SECT_MONDAY_MEET_IND.ToString(),
                data.SECT_TUESDAY_MEET_IND.ToString(),
                data.SECT_WEDNESDAY_MEET_IND.ToString(),
                data.SECT_THURSDAY_MEET_IND.ToString(),
                data.SECT_FRIDAY_MEET_IND.ToString(),
                data.SECT_SATURDAY_MEET_IND.ToString());
            this.KeyNumber = key;
            this.Crn = data.CRN.ToString();
            this.TermCode = data.TERM_CD.ToString();
            this.Format = ConvertToFormat(data.SCHED_TYPE_CD, data.PART_OF_TERM_CD);
            this.ShowInProfilePage = ConvertToShowInProfilePage(data.SCHED_TYPE_CD);
            this.Uin = data.UIN?.ToString();
            this.Username = username;
            if (string.IsNullOrWhiteSpace(lastName))
            {
                this.FullName = "TBD";
            }
            else if (string.IsNullOrWhiteSpace(firstName))
            {
                this.FullName = lastName;
            }
            else
            {
                this.FullName = $"{lastName}, {firstName.First()}";
            }
        }

        public DateTime BeginDate { get; set; }

        public string Building { get; set; }

        [Key]
        [Column(Order = 0)]
        public string Course { get; set; }

        public string Crn { get; set; }

        public string Days { get; set; }

        public DateTime EndDate { get; set; }

        public string Format { get; set; }

        public string FullName { get; set; }

        [Key]
        [Column(Order = 1)]
        public int KeyNumber { get; set; }

        public string Room { get; set; }

        public string ScheduleType { get; set; }

        public string SectionNumber { get; set; }

        public string SectionTitle { get; set; }

        public string SequenceNumber { get; set; }

        public bool ShowInProfilePage { get; set; }

        [NotMapped]
        public string Term
        {
            get
            {
                if (this.TermCode.EndsWith("1") || this.TermCode.EndsWith("2"))
                {
                    return this.TermCode.Substring(1, 4) + " Spring";
                }
                if (this.TermCode.EndsWith("5"))
                {
                    return this.TermCode.Substring(1, 4) + " Summer";
                }
                if (this.TermCode.EndsWith("8"))
                {
                    return this.TermCode.Substring(1, 4) + " Fall";
                }
                return string.Empty;
            }
        }

        public string TermCode { get; set; }

        public string Time { get; set; }

        public string Uin { get; set; }

        public string Username { get; set; }

        public string GetHtmlName()
        {
            return string.IsNullOrWhiteSpace(this.Username) ? this.FullName : $"<a href=\"/faculty/{this.Username}\">{this.FullName}</a>";
        }

        private static string ConvertToFormat(string format, string partTermCode)
        {
            if (format == "ONL")
            {
                return "Online";
            }
            if (partTermCode == "XM")
            {
                return "Off-Campus";
            }
            return "On-Campus (Urbana)";
        }

        private static bool ConvertToShowInProfilePage(string format)
        {
            return format != "IND"; 
        }

        private static string CreateDayString(
            string s1,
            string s2,
            string s3,
            string s4,
            string s5,
            string s6,
            string s7)
        {
            return string.Join(",", new[] { s1, s2, s3, s4, s5, s6, s7 }.Where(s => !string.IsNullOrWhiteSpace(s)));
        }
    }
}