﻿// ReSharper disable InconsistentNaming
namespace UIUC.Custom.DAL.DataAccess.FacetedSearch.Model
{
    public enum DepartmentType { None, EPOL, CI, EdPsy, SPED, College }

    public enum CredentialType { None, BS, EdM, MA, MS, CAS, EdD, PhD, Grad_Concentration, Undergrad_Minor, Certificate, Endorsement, Grad_Minor}
}
