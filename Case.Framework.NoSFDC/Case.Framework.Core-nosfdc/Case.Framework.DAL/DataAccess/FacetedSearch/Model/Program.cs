﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("facetedsearch.program")]
    public class Program
    {
        public virtual ICollection<Credential> Credentials { get; set; }

        public DepartmentType Department { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        [Key]
        public string Name { get; set; }

        public string OnlineUrl { get; set; }

        public string Url { get; set; }

        [NotMapped]
        public string DepartmentName => TranslationDictionary[this.Department];

        private static readonly Dictionary<DepartmentType, string> TranslationDictionary = new Dictionary<DepartmentType, string>
                                                                                               {
                                                                                                   { DepartmentType.CI, "Curriculum & Instruction" },
                                                                                                   { DepartmentType.EPOL, "Education Policy, Organization and Leadership" },
                                                                                                   { DepartmentType.EdPsy, "Educational Psychology" },
                                                                                                   { DepartmentType.SPED, "Special Education" },
                                                                                                   { DepartmentType.College, "The College of Education" }
                                                                                               };
    }
}