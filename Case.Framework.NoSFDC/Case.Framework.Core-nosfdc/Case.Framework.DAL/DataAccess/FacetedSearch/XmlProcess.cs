﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    public class XmlProcess
    {
        private readonly string googleFeed;

        private readonly string path;

        public XmlProcess(string path, string googleFeed)
        {
            this.path = path;
            this.googleFeed = googleFeed;
        }

        public string GenerateProgramXmlFile()
        {
            var exportFile = new ExportProcess();
            var stringBuilder = new StringBuilder();
            var programList = exportFile.GetAllPrograms();
            foreach (var program in programList)
            {
                if (program != null)
                {
                    var validUrl = !string.IsNullOrWhiteSpace(program.Url) ? program.Url : program.OnlineUrl;
                    if (validUrl.StartsWith("http"))
                    {
                        validUrl = "/" + Guid.NewGuid();
                    }
                    var document = new XElement("record",
                        new XAttribute("url", this.path + validUrl),
                        new XAttribute("displayurl", this.path + validUrl),
                        new XAttribute("action", program.IsActive ? "add" : "delete"),
                        new XAttribute("mimetype", "text/html"),
                        new XAttribute("last-modified", DateTime.UtcNow.ToString("G")),
                        new XAttribute("lock", "true"),
                        new XElement("metadata",
                            new XElement("meta",
                                new XAttribute("name", "title"),
                                new XAttribute("content", program.Name)),
                            new XElement("meta",
                                new XAttribute("name", "onlineUrl"),
                                new XAttribute("content", Fix(program.OnlineUrl))),
                            new XElement("meta",
                                new XAttribute("name", "url"),
                                new XAttribute("content", Fix(program.Url))),
                            new XElement("meta",
                                new XAttribute("name", "objectives"),
                                new XAttribute("content", Fix(string.Join(", ", program.Credentials.Where(o => o.IsOncampus).OrderBy(o => (int)o.CredentialType).Select(x => x.CredentialType.ToString().Replace("_", " ")))))),
                            new XElement("meta",
                                new XAttribute("name", "onlineObjectives"),
                                new XAttribute("content", Fix(string.Join(", ", program.Credentials.Where(o => o.IsOnline).OrderBy(o => (int)o.CredentialType).Select(x => x.CredentialType.ToString().Replace("_", " ")))))),
                            new XElement("meta",
                                new XAttribute("name", "offcampusObjectives"),
                                new XAttribute("content", Fix(string.Join(", ", program.Credentials.Where(o => o.IsOffCampus).OrderBy(o => (int)o.CredentialType).Select(x => x.CredentialType.ToString().Replace("_", " ")))))),
                            new XElement("meta",
                                new XAttribute("name", "departmentName"),
                                new XAttribute("content", program.DepartmentName)),
                            new XElement("meta",
                                new XAttribute("name", "department"),
                                new XAttribute("content", program.Department.ToString())),
                            new XElement("meta",
                                new XAttribute("name", "format"),
                                new XAttribute("content", (program.Credentials.Any(c => c.IsOnline) ? "online," : string.Empty) +(program.Credentials.Any(c => c.IsOncampus) ? "on campus," : string.Empty) +(program.Credentials.Any(c => c.IsOffCampus) ? "off campus," : string.Empty))),
                            new XElement("meta",
                                new XAttribute("name", "description"),
                                new XAttribute("content", program.Description))),
                        new XElement("content", program.Description));
                    stringBuilder.Append(document);
                }
            }
            stringBuilder.Insert(0,
                $"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<!DOCTYPE gsafeed PUBLIC \"-//Google//DTD GSA Feeds//EN\" \"\">\r\n<gsafeed><header><datasource>{this.googleFeed}</datasource><feedtype>incremental</feedtype></header><group>");
            stringBuilder.Append("</group></gsafeed>");
            return stringBuilder.ToString();
        }

        public string GenerateXmlFile()
        {
            var exportFile = new ExportProcess();
            var stringBuilder = new StringBuilder();
            var courseList = exportFile.GetAllCourses();
            foreach (var course in courseList)
            {
                if (course != null)
                {
                    var document = new XElement("record",
                        new XAttribute("url", this.path + "/course/" + course.Rubric + "/" + course.CourseNumber),
                        new XAttribute("displayurl", this.path + "/course/" + course.Rubric + "/" + course.CourseNumber),
                        new XAttribute("action", course.IsActive ? "add" : "delete"),
                        new XAttribute("mimetype", "text/html"),
                        new XAttribute("last-modified", DateTime.UtcNow.ToString("G")),
                        new XAttribute("lock", "true"),
                        new XElement("metadata",
                            new XElement("meta",
                                new XAttribute("name", "rubric"),
                                new XAttribute("content", course.Rubric)),
                            new XElement("meta",
                                new XAttribute("name", "courseNumber"),
                                new XAttribute("content", course.CourseNumber)),
                            new XElement("meta",
                                new XAttribute("name", "title"),
                                new XAttribute("content", course.Title)),
                            new XElement("meta",
                                new XAttribute("name", "description"),
                                new XAttribute("content", course.Description)),
                            new XElement("meta",
                                new XAttribute("name", "creditHours"),
                                new XAttribute("content", course.CreditHours)),
                            new XElement("meta",
                                new XAttribute("name", "faculty"),
                                new XAttribute("content", course.Sections.Any() ? string.Join(", ", course.Sections.OrderBy(p => p.FullName).Select(p => p.GetHtmlName())) : "N/A")),
                            new XElement("meta",
                                new XAttribute("name", "department"),
                                new XAttribute("content", course.DepartmentType.ToString())),
                            new XElement("meta",
                                new XAttribute("name", "formats"),
                                new XAttribute("content", course.Formats)),
                            new XElement("meta",
                                new XAttribute("name", "termsOffered"),
                                new XAttribute("content", course.TermsOffered))),
                        new XElement("content", course.Description));
                    stringBuilder.Append(document);
                }
            }
            stringBuilder.Insert(0,
                $"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<!DOCTYPE gsafeed PUBLIC \"-//Google//DTD GSA Feeds//EN\" \"\">\r\n<gsafeed><header><datasource>{this.googleFeed}</datasource><feedtype>incremental</feedtype></header><group>");
            stringBuilder.Append("</group></gsafeed>");
            return stringBuilder.ToString();
        }

        private static string Fix(string s)
        {
            return !string.IsNullOrWhiteSpace(s) ? s : "none";
        }
    }
}