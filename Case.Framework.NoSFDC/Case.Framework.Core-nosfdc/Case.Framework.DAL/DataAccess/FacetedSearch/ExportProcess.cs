﻿namespace UIUC.Custom.DAL.DataAccess.FacetedSearch
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class ExportProcess
    {
        public IEnumerable<Course> GetAllCourses()
        {
            using (var db = new FacetedSearchContext())
            {
                return db.Courses.Include(x => x.Sections).ToList();
            }
        }

        public IEnumerable<Program> GetAllPrograms()
        {
            using (var db = new FacetedSearchContext())
            {
                return db.Programs.Include(x => x.Credentials).ToList();
            }
        }

        public int GetCourseCount(IEnumerable<string> formatTypes, IEnumerable<string> terms, IEnumerable<string> departments, IEnumerable<string> rubrics)
        {
            using (var db = new FacetedSearchContext())
            {
                var courses = db.Courses.Where(x => x.IsActive);
                if (formatTypes.Any())
                {
                    courses=courses.Where(course => formatTypes.Any(f => course.Formats.Contains(f)));
                }
                if (terms.Any())
                {
                    courses=courses.Where(course => terms.Any(t => course.TermsOffered.Contains(t)));
                }
                if (rubrics.Any())
                {
                    courses = courses.Where(course => rubrics.Any(r => course.Rubric.Contains(r)));
                }
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        courses=courses.Where(course => listEnums.Contains(course.DepartmentType));
                    }
                }
                return courses.Count();
            }
        }

        public IEnumerable<string> GetCourseNames(string search)
        {
            using (var db = new FacetedSearchContext())
            {
                return db.Courses.Where(c => c.IsActive&&c.Title.StartsWith(search)).Select(c => c.Title).OrderBy(s => s).Take(10).ToList();
            }
        }

        public IEnumerable<Course> GetCourses(IEnumerable<string> formatTypes, IEnumerable<string> terms, IEnumerable<string> departments, IEnumerable<string> rubrics, int skip, int take)
        {
            using (var db = new FacetedSearchContext())
            {
                var courses = db.Courses.Include(x => x.Sections).Where(x => x.IsActive);
                if (formatTypes.Any(x => !string.IsNullOrWhiteSpace(x)))
                {
                    courses=courses.Where(course => formatTypes.Any(f => course.Formats.Contains(f)));
                }
                if (rubrics.Any())
                {
                    courses = courses.Where(course => rubrics.Any(r => course.Rubric.Contains(r)));
                }
                if (terms.Any(x => !string.IsNullOrWhiteSpace(x)))
                {
                    courses=courses.Where(course => terms.Any(t => course.TermsOffered.Contains(t)));
                }
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        courses=courses.Where(course => listEnums.Contains(course.DepartmentType));
                    }
                }
                return courses.OrderBy(x => x.Rubric).ThenBy(x => x.CourseNumber).Skip(skip).Take(take).ToList();
            }
        }

        public Dictionary<string, int> GetFacetCountByDepartment(IEnumerable<string> formatTypes, IEnumerable<string> terms, IEnumerable<string> departments, IEnumerable<string> rubrics)
        {
            using (var db = new FacetedSearchContext())
            {
                var courses = db.Courses.Where(x => x.IsActive);
                if (formatTypes.Any())
                {
                    courses=courses.Where(course => formatTypes.Any(f => course.Formats.Contains(f)));
                }
                if (terms.Any())
                {
                    courses=courses.Where(course => terms.Any(t => course.TermsOffered.Contains(t)));
                }
                if (rubrics.Any())
                {
                    courses = courses.Where(course => rubrics.Any(r => course.Rubric.Contains(r)));
                }
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        courses=courses.Where(course => listEnums.Contains(course.DepartmentType));
                    }
                }
                return courses.GroupBy(x => x.DepartmentType).ToDictionary(x => x.Key.ToString().ToLowerInvariant(), y => y.Count());
            }
        }

        public Dictionary<string, int> GetFacetCountByRubric(IEnumerable<string> formatTypes, IEnumerable<string> terms, IEnumerable<string> departments, IEnumerable<string> rubrics)
        {
            using (var db = new FacetedSearchContext())
            {
                var courses = db.Courses.Where(x => x.IsActive);
                if (formatTypes.Any())
                {
                    courses = courses.Where(course => formatTypes.Any(f => course.Formats.Contains(f)));
                }
                if (terms.Any())
                {
                    courses = courses.Where(course => terms.Any(t => course.TermsOffered.Contains(t)));
                }
                if (rubrics.Any())
                {
                    courses = courses.Where(course => rubrics.Any(r => course.Rubric.Contains(r)));
                }
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum) && departmentEnum != DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x != DepartmentType.None))
                    {
                        courses = courses.Where(course => listEnums.Contains(course.DepartmentType));
                    }
                }
                return courses.GroupBy(x => x.Rubric).ToDictionary(x => x.Key.ToString().ToLowerInvariant(), y => y.Count());
            }
        }
        public Dictionary<string, int> GetFacetCountByFormat(IEnumerable<string> formatTypes, IEnumerable<string> terms, IEnumerable<string> departments, IEnumerable<string> rubrics)
        {
            using (var db = new FacetedSearchContext())
            {
                var courses = db.Courses.Where(x => x.IsActive);
                if (formatTypes.Any())
                {
                    courses=courses.Where(course => formatTypes.Any(f => course.Formats.Contains(f)));
                }
                if (terms.Any())
                {
                    courses=courses.Where(course => terms.Any(t => course.TermsOffered.Contains(t)));
                }
                if (rubrics.Any())
                {
                    courses = courses.Where(course => rubrics.Any(r => course.Rubric.Contains(r)));
                }
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        courses=courses.Where(course => listEnums.Contains(course.DepartmentType));
                    }
                }
                return new Dictionary<string, int>
                           {
                               { "online", courses.Where(course => course.Formats.Contains("Online")).Count() },
                               { "oncampus", courses.Where(course => course.Formats.Contains("On Campus")).Count() },
                               { "offcampus", courses.Where(course => course.Formats.Contains("Off Campus")).Count() }
                           };
            }
        }

        public int GetProgramCount(IEnumerable<string> formatTypes, IEnumerable<string> departments, IEnumerable<string> credentials)
        {
            using (var db = new FacetedSearchContext())
            {
                var courses = db.Programs.Where(x => x.IsActive);
                return courses.Count();
            }
        }

        public Dictionary<string, int> GetProgramFacetCountByCredential(IEnumerable<string> formatTypes, IEnumerable<string> departments, IEnumerable<string> credentials)
        {
            using (var db = new FacetedSearchContext())
            {
                var programs = db.Programs.Include(x => x.Credentials).Where(x => x.IsActive);
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        programs=programs.Where(program => listEnums.Contains(program.Department));
                    }
                }
                var returnedPrograms = programs.ToList();
                if (formatTypes.Any())
                {
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => (c.IsOnline||!formatTypes.Contains("online"))
                      &&(c.IsOffCampus||!formatTypes.Contains("off campus"))
                      &&(c.IsOncampus||!formatTypes.Contains("on campus"))).ToList());
                }

                if (credentials.Any())
                {
                    var credentialEnum = CredentialType.None;
                    var listEnums = (from credential in credentials where Enum.TryParse(credential, true, out credentialEnum)&&credentialEnum!=CredentialType.None select credentialEnum).ToList();
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => listEnums.Contains(c.CredentialType)).ToList());
                }
                return Enum.GetValues(typeof(CredentialType)).Cast<CredentialType>().ToDictionary(item => item.ToString(), item => returnedPrograms.Count(p => p.Credentials.Any(c => c.CredentialType==item)));
            }
        }

        public Dictionary<string, int> GetProgramFacetCountByDepartment(IEnumerable<string> formatTypes, IEnumerable<string> departments, IEnumerable<string> credentials)
        {
            using (var db = new FacetedSearchContext())
            {
                var programs = db.Programs.Include(x => x.Credentials).Where(x => x.IsActive);
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        programs=programs.Where(program => listEnums.Contains(program.Department));
                    }
                }
                var returnedPrograms = programs.ToList();
                if (formatTypes.Any())
                {
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => (c.IsOnline||!formatTypes.Contains("online"))
                      &&(c.IsOffCampus||!formatTypes.Contains("off campus"))
                      &&(c.IsOncampus||!formatTypes.Contains("on campus"))).ToList());
                }

                if (credentials.Any())
                {
                    var credentialEnum = CredentialType.None;
                    var listEnums = (from credential in credentials where Enum.TryParse(credential, true, out credentialEnum)&&credentialEnum!=CredentialType.None select credentialEnum).ToList();
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => listEnums.Contains(c.CredentialType)).ToList());
                }
                return returnedPrograms.Where(c => c.Credentials.Any()).GroupBy(x => x.Department).ToDictionary(x => x.Key.ToString().ToLowerInvariant(), y => y.Count());
            }
        }

        public Dictionary<string, int> GetProgramFacetCountByFormat(IEnumerable<string> formatTypes, IEnumerable<string> departments, IEnumerable<string> credentials)
        {
            using (var db = new FacetedSearchContext())
            {
                var programs = db.Programs.Include(x => x.Credentials).Where(x => x.IsActive);
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        programs=programs.Where(program => listEnums.Contains(program.Department));
                    }
                }
                var returnedPrograms = programs.ToList();
                if (formatTypes.Any())
                {
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => (c.IsOnline||!formatTypes.Contains("online"))
                      &&(c.IsOffCampus||!formatTypes.Contains("off campus"))
                      &&(c.IsOncampus||!formatTypes.Contains("on campus"))).ToList());
                }

                if (credentials.Any())
                {
                    var credentialEnum = CredentialType.None;
                    var listEnums = (from credential in credentials where Enum.TryParse(credential, true, out credentialEnum)&&credentialEnum!=CredentialType.None select credentialEnum).ToList();
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => listEnums.Contains(c.CredentialType)).ToList());
                }
                return new Dictionary<string, int>
                           {
                               { "online", returnedPrograms.Count(c => c.Credentials.Any(co => co.IsOnline)) },
                               { "oncampus", returnedPrograms.Count(c => c.Credentials.Any(co => co.IsOncampus)) },
                               { "offcampus", returnedPrograms.Count(c => c.Credentials.Any(co => co.IsOffCampus)) }
                           };
            }
        }

        public IEnumerable<Program> GetPrograms(IEnumerable<string> formatTypes, IEnumerable<string> departments, IEnumerable<string> credentials)
        {
            using (var db = new FacetedSearchContext())
            {
                var programs = db.Programs.Include(x => x.Credentials).Where(x => x.IsActive);
                if (departments.Any())
                {
                    var departmentEnum = DepartmentType.None;
                    var listEnums = (from department in departments where Enum.TryParse(department, true, out departmentEnum)&&departmentEnum!=DepartmentType.None select departmentEnum).ToList();
                    if (listEnums.Any(x => x!=DepartmentType.None))
                    {
                        programs=programs.Where(program => listEnums.Contains(program.Department));
                    }
                }
                var returnedPrograms = programs.ToList();
                if (credentials.Any())
                {
                    var credentialEnum = CredentialType.None;
                    var listEnums = (from credential in credentials where Enum.TryParse(credential, true, out credentialEnum) && credentialEnum!=CredentialType.None select credentialEnum).ToList();
                    returnedPrograms.ForEach(p => p.Credentials=p.Credentials.Where(c => listEnums.Contains(c.CredentialType)).ToList());
                }
                if (formatTypes.Any())
                {
                    if (!formatTypes.Contains("online"))
                    {
                        returnedPrograms.SelectMany(c => c.Credentials).ToList().ForEach(c => c.IsOnline=false);
                    }
                    if (!formatTypes.Contains("off campus"))
                    {
                        returnedPrograms.SelectMany(c => c.Credentials).ToList().ForEach(c => c.IsOffCampus=false);
                    }
                    if (!formatTypes.Contains("on campus"))
                    {
                        returnedPrograms.SelectMany(c => c.Credentials).ToList().ForEach(c => c.IsOncampus=false);
                    }
                }

                return returnedPrograms.Where(rp => rp.Credentials.Any(c => c.IsOffCampus || c.IsOnline || c.IsOncampus)).ToList();
            }
        }

        public Course GetSpecificCourse(string rubric, string courseNumber)
        {
            using (var db = new FacetedSearchContext())
            {
                 return db.Courses.Include(x => x.Sections).FirstOrDefault(x => x.IsActive && x.Rubric == rubric && x.CourseNumber == courseNumber); 
            }
        }

        public Program GetSpecificProgram(string name)
        {
            using (var db = new FacetedSearchContext())
            {
                return db.Programs.Include(x => x.Credentials).Single(x => x.Name==name);
            }
        }
    }
}