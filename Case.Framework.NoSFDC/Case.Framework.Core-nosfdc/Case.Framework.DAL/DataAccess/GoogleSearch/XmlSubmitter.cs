﻿namespace UIUC.Custom.DAL.DataAccess.GoogleSearch
{
    using System;
    using System.Net;
    using System.Text;

    public class XmlSubmitter
    {
        private static readonly Encoding Encoding = Encoding.UTF8;
        private readonly string boundry;
        private readonly string feedName;
        private readonly string url;

        public XmlSubmitter(string url, string feedName)
        {
            this.url = url;
            this.boundry = "------WebKitFormBoundaryPb1goLXxFAdQ01lo";
            this.feedName = feedName;
        }

        public string PostForm(string file, bool isFull)
        {
            var request = WebRequest.Create(this.url) as HttpWebRequest;

            if (request == null)
            {
                throw new NullReferenceException("request is not a http request");
            }

            var formData = this.Information(file, isFull);

            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=" + this.boundry;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.CookieContainer = new CookieContainer();
            request.ContentLength = formData.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(formData, 0, formData.Length);
                requestStream.Close();
            }

            var response = request.GetResponse() as HttpWebResponse;
            return response == null ? "no response" : response.StatusDescription;
        }

        private byte[] Information(string file, bool isFull)
        {
            var sb = new StringBuilder();
            sb.AppendLine("--" + this.boundry);
            sb.AppendLine("Content-Disposition: form-data; name=\"datasource\"");
            sb.AppendLine(string.Empty);
            sb.AppendLine(this.feedName);
            sb.AppendLine("--" + this.boundry);
            sb.AppendLine("Content-Disposition: form-data; name=\"feedtype\"");
            sb.AppendLine(string.Empty);
            sb.AppendLine(isFull ? "full" : "incremental");
            sb.AppendLine("--" + this.boundry);
            sb.AppendLine("Content-Disposition: form-data; name=\"data\"; filename=\"mainfile.xml\"");
            sb.AppendLine("Content-Type: text/xml");
            sb.AppendLine(string.Empty);
            sb.AppendLine(file);
            sb.AppendLine(string.Empty);
            sb.AppendLine("--" + this.boundry + "--");
            return Encoding.GetBytes(sb.ToString());
        }
    }
}