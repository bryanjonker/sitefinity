﻿namespace UIUC.Custom.DAL.DataAccess.Faculty
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;

    using Dapper;

    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    public class ImportProcess
    {
        private readonly string imagePath;

        private readonly string mapPath;

        public ImportProcess(string imagePath, string mapPath)
        {
            this.imagePath = imagePath;
            this.mapPath = mapPath;
        }

        public string ImportData(string username)
        {
            string returnValue;
            var newsItemList = new List<NewsItem>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UIUC_Proc_TablesConnection"].ConnectionString))
            {
                var sqlCommand = string.IsNullOrWhiteSpace(username) ? @"webpublic.directory_profile NULL, 0" : @"webpublic.directory_profile @username, 1";
                using (var rows = conn.QueryMultiple(sqlCommand, new { username }, commandTimeout: 0))
                {
                    using (var context = new FacultyContext())
                    {
                        if (!string.IsNullOrWhiteSpace(username))
                        {
                            var oldProfile = context.Profiles.SingleOrDefault(x => x.Username == username);
                            if (oldProfile != null)
                            {
                                newsItemList = oldProfile.NewsItems.ToList();
                            }
                        }
                        context.Database.ExecuteSqlCommand("faculty.DeleteFaculty @username", new SqlParameter("username", username ?? ""));
                    }
                    using (var context = new FacultyContext())
                    {
                        returnValue = rows.Read().Single().refreshStatus.ToString();
                        var profiles = rows.Read().Select(f => new Profile(f)).ToList();
                        var appointments = rows.Read().Select(f => new Appointment(f)).ToList();
                        var histories = rows.Read().Select(f => new History(f));
                        var backgrounds = rows.Read().Select(f => new Background(f));
                        var activities = rows.Read().Select(f => new Activity(f));
                        var grants = rows.Read().Select(f => new Grant(f));
                        var publications = rows.Read().Select(f => new Publication(f));
                        var links = rows.Read().Select(f => new Link(f));
                        // not doing anything with media yet, so skip this
                        rows.Read();
                        var units = rows.Read().Select(f => new Unit(f));
                        foreach (var appointment in appointments)
                        {
                            var mainUnit = units.SingleOrDefault(x => x.UnitId == appointment.UnitId);
                            appointment.Unit = mainUnit == null ? "College of Education" : mainUnit.Name;
                            var departmentCodes = appointment.Hierarchy.Split('.');
                            var mainDepartment = units.SingleOrDefault(x => x.UnitId == (departmentCodes.Length > 2 ? departmentCodes[2] : departmentCodes.Last()));
                            appointment.Department = mainDepartment == null ? "College of Education" : mainDepartment.Name;
                        }
                        foreach (var profile in profiles)
                        {
                            profile.Image = this.GetPhoto(profile.Username, profile.Photo);
                            profile.Activities = activities.Where(i => i.Username == profile.Username).ToList();
                            profile.Appointments = appointments.Where(i => i.Username == profile.Username).ToList();
                            profile.History = histories.Where(i => i.Username == profile.Username).ToList();
                            profile.Backgrounds = backgrounds.Where(i => i.Username == profile.Username).ToList();
                            profile.Grants = grants.Where(i => i.Username == profile.Username).ToList();
                            profile.Publications = publications.Where(i => i.Username == profile.Username).ToList();
                            profile.Links = links.Where(i => i.Username == profile.Username).ToList();
                            if (string.IsNullOrWhiteSpace(username))
                            {
                                profile.NewsItems = new NewsItemAttachment().GetNewsItems(profile.FullName).ToList();
                            }
                            else
                            {
                                profile.NewsItems = newsItemList;
                            }
                        }
                        context.Profiles.AddRange(profiles);
                        if (string.IsNullOrWhiteSpace(username))
                        {
                            context.Units.AddRange(units);
                            context.NewsItems.AddRange(newsItemList);
                        }
                        context.SaveChanges();
                        context.Database.ExecuteSqlCommand("faculty.AddCoursesToFaculty @username", new SqlParameter("username", username ?? ""));
                    }
                }
            }
            return returnValue;
        }

        private string GetPhoto(string userName, bool usePhoto)
        {
            if (!usePhoto)
            {
                return this.imagePath + "_notdisplayed.jpg";
            }
            var file = new FileInfo(this.mapPath + userName + ".jpg");
            return file.Exists ? this.imagePath + userName + ".jpg" : this.imagePath + "_unknown.jpg";
        }
    }
}