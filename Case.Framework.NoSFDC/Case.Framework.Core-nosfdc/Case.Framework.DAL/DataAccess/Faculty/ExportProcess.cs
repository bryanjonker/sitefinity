﻿namespace UIUC.Custom.DAL.DataAccess.Faculty
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    public class ExportProcess
    {
        public int CountFacultyAppointments(IEnumerable<string> departments, IEnumerable<string> units)
        {
            using (var db = new FacultyContext())
            {
                return this.Limit(db, departments, units).Count();
            }
        }

        public IEnumerable<Profile> GetAllFacultyAppointmentsFacultyProfile()
        {
            using (var db = new FacultyContext())
            {
                var faculty = db.Profiles.Include(x => x.Appointments).Where(x => x.Appointments.Any(y => y.ShowInFacultyProfile));

                return faculty.OrderBy(fac => fac.LastName).ToList();
            }
        }

        public Profile GetFaculty(string username)
        {
            using (var db = new FacultyContext())
            {
                var item = db.Profiles.Where(x => x.Username == username)
                    .Include(x => x.Activities)
                    .Include(x => x.Appointments)
                    .Include(x => x.Backgrounds)
                    .Include(x => x.Grants)
                    .Include(x => x.History)
                    .Include(x => x.Links)
                    .Include(x => x.Courses)
                    .Include(x => x.Publications)
                    .Include(x => x.Media)
                    .Include(x => x.NewsItems).SingleOrDefault();

                return item;
            }
        }

        public Profile GetFacultyAppointmentsOnly(string username)
        {
            using (var db = new FacultyContext())
            {
                var item = db.Profiles.Where(x => x.Username == username)
                    .Include(x => x.Appointments)
                    .SingleOrDefault();

                return item;
            }
        }

        public Profile GetFacultySingle(string username)
        {
            using (var db = new FacultyContext())
            {
                var item = db.Profiles.Where(x => x.Username == username).SingleOrDefault();
                return item;
            }
        }

        public Profile GetFacultySingleByUin(string uin)
        {
            using (var db = new FacultyContext())
            {
                var item = db.Profiles.Where(x => x.Uin == uin).SingleOrDefault();
                return item;
            }
        }

        public IEnumerable<Profile> SearchFacultyAppointments(IEnumerable<string> departments, IEnumerable<string> units, int take, int skip)
        {
            using (var db = new FacultyContext())
            {
                return this.Limit(db, departments, units).OrderBy(fac => fac.LastName).Skip(skip).Take(take).ToList();
            }
        }

        public IEnumerable<Profile> SearchFacultyAppointmentsByName(string name)
        {
            using (var db = new FacultyContext())
            {
                return db.Profiles.Include(x => x.Appointments)
                    .Where(x => x.Appointments.Any(y => y.ShowInFacultyProfile) && (x.FirstName.StartsWith(name) || x.LastName.StartsWith(name)))
                    .OrderBy(z => z.LastName).ToList();
            }
        }

        private IQueryable<Profile> Limit(FacultyContext db, IEnumerable<string> departments, IEnumerable<string> units)
        {
            var faculty = db.Profiles.Include(x => x.Appointments).Where(x => x.Appointments.Any(y => y.ShowInFacultyProfile));
            if (departments.Any())
            {
                // ReSharper disable once ConvertClosureToMethodGroup
                faculty = faculty.Where(f => f.Appointments.Select(a => a.Department).Any(dept => departments.Contains(dept)));
            }
            if (units.Any())
            {
                // ReSharper disable once ConvertClosureToMethodGroup
                faculty = faculty.Where(f => f.Appointments.Select(a => a.Unit).Any(unit => units.Contains(unit)));
            }
            return faculty;
        }
    }
}