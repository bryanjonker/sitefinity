﻿namespace UIUC.Custom.DAL.DataAccess.Faculty
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Telerik.OpenAccess;
    using Telerik.Sitefinity.GenericContent.Model;
    using Telerik.Sitefinity.Model;
    using Telerik.Sitefinity.Modules.News;
    using Telerik.Sitefinity.Taxonomies;
    using Telerik.Sitefinity.Taxonomies.Model;
    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    public class NewsItemAttachment
    {
        private readonly NewsManager newsManager;
        private readonly TaxonomyManager taxonomyMgr;
        
        public NewsItemAttachment()
        {
            this.newsManager = NewsManager.GetManager();
            this.taxonomyMgr = TaxonomyManager.GetManager();
        }

        public IEnumerable<NewsItem> GetNewsItems(string name)
        {
            var taxonId = this.taxonomyMgr.GetTaxa<HierarchicalTaxon>()
                                     .SingleOrDefault(t => t.Title == name);
            if (taxonId != null)
            {
                return this.newsManager.GetNewsItems()
                    .Where(newsItem => newsItem.Status == ContentLifecycleStatus.Live && newsItem.GetValue<TrackedList<Guid>>("Category").Contains(taxonId.Id))
                    .OrderByDescending(x => x.PublicationDate).ToList()
                    .Select(x => new NewsItem { PublicationDate = x.PublicationDate, Title = x.Title.ToString(), Url = "/news" + x.Urls.First().Url });
            }
            return new List<NewsItem>();
        }
    }
}
