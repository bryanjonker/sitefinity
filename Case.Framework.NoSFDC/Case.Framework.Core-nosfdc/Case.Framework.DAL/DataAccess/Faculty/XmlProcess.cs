﻿namespace UIUC.Custom.DAL.DataAccess.Faculty
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    public class XmlProcess
    {
        private readonly string googleFeed;
        private readonly string path;

        public XmlProcess(string path, string googleFeed)
        {
            this.path = path;
            this.googleFeed = googleFeed;
        }

        public string GenerateXmlFile(string username)
        {
            var exportFile = new ExportProcess();
            var stringBuilder = new StringBuilder();
            var facultyList = string.IsNullOrWhiteSpace(username) ?
                exportFile.GetAllFacultyAppointmentsFacultyProfile().ToList() :
                new List<Profile> { exportFile.GetFacultyAppointmentsOnly(username) };
            foreach (var faculty in facultyList)
            {
                if (faculty != null)
                {
                    var document = new XElement("record",
                            new XAttribute("url", this.path + "/faculty/" + faculty.Username),
                            new XAttribute("displayurl", this.path + "/faculty/" + faculty.Username),
                            new XAttribute("action", "add"),
                            new XAttribute("mimetype", "text/html"),
                            new XAttribute("last-modified", DateTime.UtcNow.ToString("G")),
                            new XAttribute("lock", "true"),
                            new XElement("metadata",
                                new XElement("meta",
                                    new XAttribute("name", "department"),
                                    new XAttribute("content", faculty.Appointments.First().Department)),
                                new XElement("meta",
                                    new XAttribute("name", "title"),
                                    new XAttribute("content", faculty.Appointments.First().Title)),
                                new XElement("meta",
                                    new XAttribute("name", "sort"),
                                    new XAttribute("content", faculty.LastName + " " + faculty.FirstName)),
                                new XElement("meta",
                                    new XAttribute("name", "image"),
                                    new XAttribute("content", faculty.Image)),
                                new XElement("meta",
                                    new XAttribute("name", "fullname"),
                                    new XAttribute("content", faculty.FullName))),
                            new XElement("content", faculty.Biography + " " + faculty.Research + " " + faculty.Teaching));
                    stringBuilder.Append(document);
                }
            }
            stringBuilder.Insert(0,
                $"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<!DOCTYPE gsafeed PUBLIC \"-//Google//DTD GSA Feeds//EN\" \"\">\r\n<gsafeed><header><datasource>{this.googleFeed}</datasource><feedtype>{(string.IsNullOrWhiteSpace(username) ? "full" : "incremental")}</feedtype></header><group>");
            stringBuilder.Append("</group></gsafeed>");
            return stringBuilder.ToString();
        }
    }
}
