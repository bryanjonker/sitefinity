﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.background")]
    public class Background : BaseFaculty
    {
        public Background() { }

        public Background(dynamic data)
        {
            this.Username = this.Convert(data.username);
            this.Department = this.Convert(data.department);
            this.Institution = this.Convert(data.institution);
            this.Title = this.Convert(data.position_title);
            this.YearStarted = this.Convert(data.year_start);
            this.YearEnded = this.Convert(data.year_end);
        }

        public string Department { get; set; }

        public string Institution { get; set; }

        public string Title { get; set; }

        public string YearEnded { get; set; }

        public string YearStarted { get; set; }
    }
}