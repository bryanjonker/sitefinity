﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    [Table("faculty.appointment")]
    public class Appointment : BaseFaculty
    {
        public Appointment()
        {
        }

        public Appointment(dynamic data)
        {
            this.Address1 = this.Convert(data.address);
            this.Building = this.Convert(data.building);
            this.City = this.Convert(data.city);
            this.Fax = this.Convert(data.office_fax);
            this.Hierarchy = this.Convert(data.hierarchy);
            this.IsPrimary = this.Convert(data.PRIMARY_JOB).Equals("Yes", StringComparison.OrdinalIgnoreCase);
            this.JobType = this.Convert(data.JOB_TYPE);
            this.Phone = this.Convert(data.public_phone);
            this.Room = this.Convert(data.room);
            this.ShowInFacultyProfile = !this.Convert(data.SHOW_ONLINE).Equals("Staff Directory Only", StringComparison.OrdinalIgnoreCase);
            this.State = this.Convert(data.state);
            this.Title = this.Convert(data.job_title);
            this.UnitId = this.Convert(data.ed_units_id);
            this.Zip = this.Convert(data.zip);
            this.Username = data.username.ToString();
        }

        public string Address1 { get; set; }

        public virtual Profile Profile { get; set; }

        public string Building { get; set; }

        public string City { get; set; }

        public string Department { get; set; }

        public string Fax { get; set; }

        public string Hierarchy { get; set; }

        public string Hours { get; set; }

        public bool IsPrimary { get; set; }

        public string JobType { get; set; }

        public string Phone { get; set; }

        public string Room { get; set; }

        public bool ShowInFacultyProfile { get; set; }

        public string State { get; set; }

        public string Title { get; set; }

        public string Unit { get; set; }

        public string UnitId { get; set; }

        public string Zip { get; set; }
    }
}