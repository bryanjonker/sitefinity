﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.history")]
    public class History : BaseFaculty
    {
        public History() { }

        public History(dynamic data)
        {
            this.Username = data.username.ToString();
            this.Area = this.Convert(data.area);
            this.DegreeYear = this.Convert(data.degree_year);
            this.Degree = this.Convert(data.degree);
            this.Institution = this.Convert(data.institution);
        }

        public string Area { get; set; }

        public string Degree { get; set; }

        public string DegreeYear { get; set; }

        public string Institution { get; set; }
    }
}