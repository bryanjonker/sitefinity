﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("faculty.publication")]
    public class Publication : BaseFaculty
    {
        public Publication()
        {
        }

        public Publication(dynamic data)
        {
            var date = (DateTime?)data.DT_PUB;
            this.Name = GenerateName(
                this.Convert(data.TYPE),
                date,
                this.Convert(data.TITLE),
                this.Convert(data.TITLE_SECONDARY),
                this.Convert(data.authors),
                this.Convert(data.PUBLISHER),
                this.Convert(data.PUBLOCATION),
                this.Convert(data.VOLUME),
                this.Convert(data.ISSUE),
                this.Convert(data.PAGENUM));
            this.Status = this.Convert(data.STATUS);
            this.Url = this.Convert(data.WEB_ADDRESS);
            this.Username = data.username.ToString();
            this.PublishDate = (date.HasValue && date.Value.Year <= DateTime.Now.AddYears(10).Year && date.Value.Year >= DateTime.Now.AddYears(-50).Year) ? date.Value : new DateTime(2001, 1, 1);
            this.SortOrder = (int?)data.WEB_PROFILE_ORDER;
            this.FixUrl();
        }

        public int? SortOrder { get; set; }

        public string Name { get; set; }

        public DateTime PublishDate { get; set; }

        public string Status { get; set; }

        public string Url { get; set; }

        public void FixUrl()
        {
            if (this.Url.Contains("DOI: "))
            {
                this.Url = this.Url.Replace("DOI: ", "http://dx.doi.org/");
            }
            else if (this.Url.Contains("DOI "))
            {
                this.Url = this.Url.Replace("DOI ", "http://dx.doi.org/");
            }
            else if (!this.Url.StartsWith("http"))
            {
                this.Url = "http://" + this.Url;
            }
            if (!this.Url.Contains("."))
            {
                this.Url = string.Empty;
            }
        }

        private static string GenerateName(string type, DateTime? publishedDateTime, string title, string titleSecondary, string author, string publisher, string location, string volume, string issue, string pageNumber)
        {
            char[] endingItems = { '.', ',' };
            var yearPublished = publishedDateTime.HasValue ? publishedDateTime.Value.ToString("yyyy") : string.Empty;
            var returnValue = author;
            if (!string.IsNullOrEmpty(yearPublished))
            {
                returnValue += $" ({yearPublished})";
            }
            if (endingItems.Contains(returnValue.Last()))
            {
                returnValue += ".";
            }
            returnValue += $" {title}";
            if (type.ToLowerInvariant().Contains("journal"))
            {
                var secondaryValue = $"{publisher} {(string.IsNullOrWhiteSpace(volume) ? string.Empty : volume.Trim(' ', ','))}";
                if (!string.IsNullOrWhiteSpace(secondaryValue))
                {
                    returnValue += $" <em>{secondaryValue}</em>";
                }
                if (!string.IsNullOrWhiteSpace(issue))
                {
                    returnValue += $" ({issue})";
                }
                if (!string.IsNullOrWhiteSpace(pageNumber))
                {
                    returnValue += $", {pageNumber}";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(titleSecondary))
                {
                    returnValue = returnValue + ". " + titleSecondary;
                }
                if (endingItems.Contains(returnValue.Last()))
                {
                    returnValue += ".";
                }
                if (!string.IsNullOrWhiteSpace(publisher))
                {
                    returnValue += $" {publisher}";
                }
                if (!string.IsNullOrWhiteSpace(location.Trim(' ', ',')))
                {
                    returnValue += $": {location.Trim(' ', ',')}";
                }
            }
            if (endingItems.Contains(returnValue.Last()))
            {
                returnValue += ".";
            }
            return returnValue;
        }
    }
}