﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.unit")]
    public class Unit
    {
        public Unit()
        {
        }

        public Unit(dynamic data)
        {
            this.Name = this.Convert(data.unit_name);
            this.Type = this.Convert(data.unit_type);
            this.Order = (int?)data.sub_unit_order;
            this.Code = this.Convert(data.unit_id);
            this.ParentUnitId = this.Convert(data.parent_ed_units_id);
            this.UnitId = this.Convert(data.ed_units_id);
            this.Category = this.Convert(data.category);
        }

        public string Category { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public int? Order { get; set; }

        public string ParentUnitId { get; set; }

        public string Type { get; set; }

        [Key]
        public string UnitId { get; set; }

        internal string Convert(object o)
        {
            return o == null ? string.Empty : o.ToString();
        }
    }
}