﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.link")]
    public class Link : BaseFaculty
    {
        public Link()
        {
        }

        public Link(dynamic data)
        {
            this.Username = data.username.ToString();
            this.Name = this.Convert(data.link_name);
            this.Url = this.Convert(data.link_url);
        }

        public string Name { get; set; }

        public string Url { get; set; }
    }
}