﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public abstract class BaseFaculty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Username { get; set; }

        internal string Convert(object o)
        {
            return o == null ? string.Empty : o.ToString().Trim();
        }
    }
}