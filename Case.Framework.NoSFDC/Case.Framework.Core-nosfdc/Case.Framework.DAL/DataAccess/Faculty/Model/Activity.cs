﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.activity")]
    public class Activity : BaseFaculty
    {
        public Activity() { }

        public Activity(dynamic data)
        {
            this.Area = this.Convert(data.area);
            this.Name = this.Convert(data.name);
            this.Organization = this.Convert(data.organization_name);
            this.SortOrder = (int?)data.custom_sort;
            this.YearStarted = this.Convert(data.year_start);
            this.YearEnded = this.Convert(data.year_end);
            this.Username = data.username.ToString();
        }

        public string Area { get; set; }

        public string Name { get; set; }

        public string Organization { get; set; }

        public int? SortOrder { get; set; }

        public string YearEnded { get; set; }

        public string YearStarted { get; set; }
    }
}