﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("faculty.profile")]
    public class Profile
    {
        public Profile()
        {
        }

        public Profile(dynamic data)
        {
            this.Biography = this.Convert(data.bio);
            this.Email = this.Convert(data.email);
            this.FirstName = this.Convert(data.fname);
            this.IsActive = true;
            this.LastName = this.Convert(data.lname);
            this.LastUpdated = (DateTime)data.lastRefreshed;
            this.LinkedIn = this.Convert(data.linkedin);
            this.Photo = !data.photo.ToString().Equals("No", StringComparison.OrdinalIgnoreCase);
            this.Quote = this.Convert(data.bio_quote);
            this.Research = this.Convert(data.bio_research);
            this.Teaching = this.Convert(data.bio_teaching);
            this.Twitter = this.Convert(data.twitter);
            this.Uin = data.uin.ToString();
            this.Username = data.username.ToString();
        }

        public virtual ICollection<Activity> Activities { get; set; }

        public Appointment Appointment
        {
            get
            {
                return this.Appointments.Any() ? this.Appointments.First() : new Appointment();
            }
        }

        public virtual ICollection<Appointment> Appointments { get; set; }

        public virtual ICollection<Background> Backgrounds { get; set; }

        public string Biography { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        public virtual ICollection<Grant> Grants { get; set; }

        public virtual ICollection<History> History { get; set; }

        public string Image { get; set; }

        public bool IsActive { get; set; }

        public string LastName { get; set; }

        public DateTime LastUpdated { get; set; }

        public string LinkedIn { get; set; }

        public virtual ICollection<Link> Links { get; set; }

        public virtual ICollection<Media> Media { get; set; }

        public virtual ICollection<NewsItem> NewsItems { get; set; }

        public bool Photo { get; set; }

        public virtual ICollection<Publication> Publications { get; set; }

        public string Quote { get; set; }

        public string Research { get; set; }

        public string Teaching { get; set; }

        public string Twitter { get; set; }

        public string Uin { get; set; }

        [Key]
        public string Username { get; set; }

        internal string Convert(object o)
        {
            return o == null ? string.Empty : o.ToString();
        }
    }
}