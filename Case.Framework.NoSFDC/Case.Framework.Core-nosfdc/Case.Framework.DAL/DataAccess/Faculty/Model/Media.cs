﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.media")]
    public class Media : BaseFaculty
    {
        public Media() { }

        public Media(dynamic data)
        {
            this.PublicationDate = (DateTime)data.publicationdate;
            this.Title = this.Convert(data.title);
            this.Url = this.Convert(data.url);
            this.Username = data.username.ToString();
        }

        public DateTime PublicationDate { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }
    }
}
