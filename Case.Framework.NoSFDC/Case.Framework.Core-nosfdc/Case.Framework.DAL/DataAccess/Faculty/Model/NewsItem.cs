﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.newsitem")]
    public class NewsItem : BaseFaculty
    {
        public DateTime PublicationDate { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }
    }
}
