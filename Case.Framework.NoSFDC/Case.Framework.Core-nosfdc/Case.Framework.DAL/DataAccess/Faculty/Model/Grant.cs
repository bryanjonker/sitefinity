﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.grant")]
    public class Grant : BaseFaculty
    {
        public Grant()
        {
        }

        public Grant(dynamic data)
        {
            this.Username = data.username.ToString();
            this.Role = this.Convert(data.role);
            this.Title = this.Convert(data.proposal_title);
            this.Organization = this.Convert(data.organization_name);
            this.Status = this.Convert(data.prop_status);
            this.YearStarted = this.Convert(data.awd_beg_date);
            this.YearEnded = this.Convert(data.awd_end_date);
        }

        public string Organization { get; set; }

        public string Role { get; set; }

        public string Status { get; set; }

        public string Title { get; set; }
        public string YearStarted { get; set; }
        public string YearEnded { get; set; }
    }
}