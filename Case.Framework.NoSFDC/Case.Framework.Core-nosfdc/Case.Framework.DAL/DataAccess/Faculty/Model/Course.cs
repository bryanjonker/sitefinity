﻿namespace UIUC.Custom.DAL.DataAccess.Faculty.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("faculty.course")]
    public class Course : BaseFaculty
    {
        public string CourseNumber { get; set; }

        public string Description { get; set; }

        public bool IsLinked { get; set; }

        public string Name { get; set; }

        public string Rubric { get; set; }

        public string TermCode { get; set; }
    }
}