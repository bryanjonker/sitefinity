﻿namespace UIUC.Custom.DAL.DataAccess.Faculty
{
    using System.Data.Entity;

    using UIUC.Custom.DAL.DataAccess.Faculty.Model;

    public class FacultyContext : DbContext
    {
        public FacultyContext()
            : base("SitefinityDirect")
        {
            // Database.SetInitializer<FacultyContext>(null);
        }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<Appointment> Appointments { get; set; }

        public DbSet<Background> Backgrounds { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Grant> Grants { get; set; }

        public DbSet<History> Histories { get; set; }

        public DbSet<Link> Links { get; set; }

        public DbSet<Media> Media { get; set; }

        public DbSet<NewsItem> NewsItems { get; set; }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Publication> Publications { get; set; }

        public DbSet<Unit> Units { get; set; }
    }
}