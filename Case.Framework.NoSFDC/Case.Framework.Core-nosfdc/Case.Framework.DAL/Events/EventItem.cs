﻿namespace UIUC.Custom.DAL.Events
{
    using System;

    public class EventItem
    {
        public string ContactEmail { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string CostText { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool? DateDisplay { get; set; }

        public string Description { get; set; }

        public string EditedBy { get; set; }

        public DateTime EditedDate { get; set; }

        public DateTime EndDate { get; set; }

        public string EndTime { get; set; }

        public string EndTimeLabel { get; set; }

        public int EventId { get; set; }

        public string EventType { get; set; }

        public string FullDescription
        {
            get
            {
                var returnValue = this.Description;
                if (!string.IsNullOrWhiteSpace(this.Speaker))
                {
                    returnValue = $"<table><tr><td><p>Speaker Information:</p></td><td>{this.Speaker}</td></tr></table>{returnValue}";
                }
                if (!string.IsNullOrWhiteSpace(this.CostText) || !string.IsNullOrWhiteSpace(this.ContactName) || !string.IsNullOrWhiteSpace(this.Sponsor))
                {
                    returnValue = $"{returnValue}<table>";
                    if (!string.IsNullOrWhiteSpace(this.CostText))
                    {
                        returnValue = $"{returnValue}<tr><td><p>Cost:</p></td><td>{this.CostText}</td></tr>";
                    }
                    if (!string.IsNullOrWhiteSpace(this.ContactName))
                    {
                        returnValue = string.IsNullOrWhiteSpace(this.ContactEmail) ?
                            $"{returnValue}<tr><td><p>Contact:</p></td><td>{this.ContactName}{this.ContactPhone}</td></tr>" :
                            $"{returnValue}<tr><td><p>Contact:</p></td><td>{this.ContactName}{this.ContactPhone}<p><a href='mailto:{this.ContactEmail}'>{this.ContactEmail}</a></p></td></tr>";
                    }
                    if (!string.IsNullOrWhiteSpace(this.Sponsor))
                    {
                        returnValue = $"{returnValue}<tr><td><p>Sponsor:</p></td><td>{this.Sponsor}</td></tr>";
                    }
                    returnValue = $"{returnValue}</table>";
                }
                if (!string.IsNullOrWhiteSpace(this.RegistrationUrl))
                {
                    returnValue = $"{returnValue}<p><a href='{this.RegistrationUrl}'>{this.RegistrationLabel}</a></p>";
                }
                return returnValue;
            }
        }

        public string LocationHtml { get; set; }

        public string LocationText { get; set; }

        public bool? PublicEngagement { get; set; }

        public bool? Recurrence { get; set; }

        public int RecurrenceId { get; set; }

        public string RegistrationLabel { get; set; }

        public string RegistrationUrl { get; set; }

        public bool? Rss { get; set; }

        public string SearchTerms { get; set; }

        public string Speaker { get; set; }

        public string Sponsor { get; set; }

        public DateTime StartDate { get; set; }

        public string StartTime { get; set; }

        public string TimeType { get; set; }

        public string TitleLink { get; set; }

        public string TitleLong { get; set; }

        public string TitleShort { get; set; }
    }
}