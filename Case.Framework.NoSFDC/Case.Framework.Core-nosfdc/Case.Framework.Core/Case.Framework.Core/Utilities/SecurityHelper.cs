﻿using Case.Framework.Core.Configuration;
using Case.Framework.Core.Security;
using System.Security.Cryptography;
using System.Text;

namespace Case.Framework.Core.Utilities
{
    public static class SecurityHelper
    {
        public static string Decrypt(string value, string encryptionKey = null)
        {
            return (new Encryption(encryptionKey)).Decrypt(value);
        }

        public static string Encrypt(string value, string encryptionKey = null)
        {
            return (new Encryption(encryptionKey)).Encrypt(value);
        }

        public static string GetCodeForString(string id)
        {
            byte[] numArray;
            var str = AppSettings.Get(Constants.KeySecurityHash, "B5BbSFLD9RkpysoeR5E3SJd6G7Uqpowh");
            const int num = 6;
            var chrArray = new[] { '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            using (var hMacsha1 = new HMACSHA1(Encoding.ASCII.GetBytes(str)))
            {
                numArray = hMacsha1.ComputeHash(Encoding.UTF8.GetBytes(id));
            }
            var length = numArray[numArray.Length - 1] % (numArray.Length - num);
            var stringBuilder = new StringBuilder();
            for (var i = length; i < length + num; i++)
            {
                stringBuilder.Append(chrArray[numArray[i] % chrArray.Length]);
            }
            return stringBuilder.ToString();
        }
    }
}