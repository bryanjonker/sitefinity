﻿using System;

namespace Case.Framework.Core.Utilities
{
    public static class TypeHelper
    {
        public static Guid Base64ToGuid(string base64)
        {
            Guid empty;
            base64 = string.Concat(base64.Replace("-", "/").Replace("_", "+"), "==");
            try
            {
                var guid = new Guid(Convert.FromBase64String(base64));
                return guid;
            }
            catch 
            {
                empty = Guid.Empty;
            }
            return empty;
        }

        public static string GetFriendlyDateRange(DateTime startDate, DateTime endDate)
        {
            if (startDate == DateTime.MinValue && endDate == DateTime.MinValue)
            {
                return string.Empty;
            }
            startDate = startDate.ToLocalTime();
            endDate = endDate.ToLocalTime();
            string str = startDate.ToString("MMMM");
            string empty = string.Empty;
            if (startDate.Month != endDate.Month)
            {
                str = string.Concat(str, string.Format(" {0} - {1}", startDate.ToString("dd"), endDate.ToString("MMMM dd")));
            }
            else
            {
                str = (startDate.Day != endDate.Day ? string.Concat(str, string.Format(" {0} - {1}", startDate.ToString("dd"), endDate.ToString("dd"))) : string.Concat(str, " ", startDate.ToString("dd")));
            }
            if (startDate.TimeOfDay != TimeSpan.Zero && endDate.TimeOfDay != TimeSpan.Zero && startDate.TimeOfDay != endDate.TimeOfDay)
            {
                empty = string.Format(" from {0} to {1}", startDate.ToString("hh:mm tt"), endDate.ToString("hh:mm tt"));
            }
            else if (startDate.TimeOfDay != TimeSpan.Zero)
            {
                empty = string.Format(" at {0}", startDate.ToString("hh:mm tt"));
            }
            return string.Concat(str, empty);
        }

        public static string GuidToBase64(Guid guid)
        {
            return Convert.ToBase64String(guid.ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("=", "");
        }
    }
}