﻿namespace Case.Framework.Core.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.Net.Configuration;
    using System.Net.Mail;

    public static class MailHelper
    {
        public static void SendEmail(string fromEmail, string toEmail, string subject, string body)
        {
            var strArrays = new[] { toEmail };
            SendEmail(fromEmail, strArrays, subject, body);
        }

        public static void SendEmail(string fromEmail, string[] toEmail, string subject, string body)
        {
            SendEmail(fromEmail, toEmail, subject, body, null, null);
        }

        public static void SendEmail(string fromEmail, string[] toEmail, string subject, string body,
                                     SmtpNetworkElement config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            SendEmail(fromEmail, toEmail, subject, body, null, config);
        }

        public static void SendEmail(string fromEmail, string[] toEmail, string subject, string body, Dictionary<Stream, string> attachments, SmtpNetworkElement config = null)
        {
            if (config == null)
            {
                var smtpSection = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;
                if (smtpSection != null)
                {
                    config = smtpSection.Network;
                }
            }
            if (config == null)
            {
                return;
            }
            var smtpClient = new SmtpClient(config.Host)
                                 {
                                     EnableSsl = config.EnableSsl,
                                     Port = config.Port
                                 };
            if (!string.IsNullOrWhiteSpace(config.UserName) && !string.IsNullOrWhiteSpace(config.Password))
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(config.UserName, config.Password);
            }
            var mailMessage = new MailMessage
                                  {
                                      From = new MailAddress(fromEmail),
                                      Subject = subject,
                                      Body = body,
                                      IsBodyHtml = true
                                  };
            var mailMessage1 = mailMessage;
            var strArrays = toEmail;
            foreach (var str in strArrays)
            {
                mailMessage1.To.Add(str);
            }
            if (attachments != null && attachments.Count > 0)
            {
                foreach (var attachment in attachments)
                {
                    mailMessage1.Attachments.Add(new Attachment(attachment.Key, attachment.Value));
                }
            }
            smtpClient.Send(mailMessage1);
        }
    }
}