﻿namespace Case.Framework.Core.Utilities
{
    using System;
    using System.Collections;
    using System.Web.UI;

    public static class PageHelper
    {
        public static Control FindControl(this Control root, string id, bool recurse)
        {
            if (!recurse)
            {
                return root.FindControl(id);
            }
            if (root == null)
            {
                return null;
            }
            var control1 = root.FindControl(id);
            if (control1 != null)
            {
                return control1;
            }
            var enumerator = root.Controls.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = (Control)enumerator.Current;
                    control1 = current.FindControl(id, true);
                    if (control1 == null)
                    {
                        continue;
                    }
                    var control = control1;
                    return control;
                }
                return null;
            }
            finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
        }

        public static T FindControl<T>(this Control startingControl, string id)
            where T : Control
        {
            var t = startingControl.FindControl(id) as T ?? startingControl.FindChildControl<T>(id);
            return t;
        }

        public static ArrayList FindControls(this Control parent, Type type)
        {
            var arrayLists = new ArrayList();
            foreach (Control control in parent.Controls)
            {
                if (control.GetType() == type)
                {
                    arrayLists.Add(control);
                }
                if (!control.HasControls())
                {
                    continue;
                }
                arrayLists.AddRange(control.FindControls(type));
            }
            return arrayLists;
        }

        private static T FindChildControl<T>(this Control startingControl, string id)
                                    where T : Control
        {
            var t = default(T);
            var enumerator = startingControl.Controls.GetEnumerator();
            try
            {
                do
                {
                    if (!enumerator.MoveNext())
                    {
                        break;
                    }
                    var current = (Control)enumerator.Current;
                    t = current as T;
                    if (t != null && String.Compare(id, t.ID, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        continue;
                    }
                    t = current.FindChildControl<T>(id);
                }
                while (t == null);
            }
            finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
            return t;
        }
    }
}