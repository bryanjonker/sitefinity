﻿using System;
using System.Data;
using System.Text;
using Case.Framework.Core.Classes;

namespace Case.Framework.Core.Utilities
{
    public static class DataHelper
    {
        public static string GetReadableSize(ulong numberOfBytes)
        {
            double num;
            string suffix;
            if (numberOfBytes >= FileSizeUnit.ExaByte.Size)
            {
                suffix = FileSizeUnit.ExaByte.Suffix;
                num = numberOfBytes >> 50;
            }
            else if (numberOfBytes >= FileSizeUnit.PetaByte.Size)
            {
                suffix = FileSizeUnit.PetaByte.Suffix;
                num = numberOfBytes >> 40;
            }
            else if (numberOfBytes >= FileSizeUnit.TeraByte.Size)
            {
                suffix = FileSizeUnit.TeraByte.Suffix;
                num = numberOfBytes >> 30;
            }
            else if (numberOfBytes >= FileSizeUnit.GigaByte.Size)
            {
                suffix = FileSizeUnit.GigaByte.Suffix;
                num = numberOfBytes >> 20;
            }
            else if (numberOfBytes < FileSizeUnit.MegaByte.Size)
            {
                if (numberOfBytes < FileSizeUnit.KiloByte.Size)
                {
                    return numberOfBytes.ToString("0 B");
                }
                suffix = FileSizeUnit.KiloByte.Suffix;
                num = numberOfBytes;
            }
            else
            {
                suffix = FileSizeUnit.MegaByte.Suffix;
                num = numberOfBytes >> 10;
            }
            num = num/1024;
            return string.Concat(num.ToString("0.### "), suffix);
        }

        public static string ToCsv(DataTable table)
        {
            return ToDelimited(table, ",", "\"");
        }

        public static string ToDelimited(DataTable table, string delimiter, string qualifier)
        {
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < table.Columns.Count; i++)
            {
                string columnName = table.Columns[i].ColumnName;
                if (columnName.Contains(delimiter))
                {
                    if (columnName.Contains(qualifier))
                    {
                        columnName = columnName.Replace(qualifier, string.Concat(qualifier, qualifier));
                    }
                    columnName = string.Format("{0}{1}{0}", qualifier, columnName);
                }
                stringBuilder.Append(columnName);
                stringBuilder.Append(i == table.Columns.Count - 1 ? Environment.NewLine : delimiter);
            }
            foreach (DataRow row in table.Rows)
            {
                for (var j = 0; j < table.Columns.Count; j++)
                {
                    var str = row[j].ToString().Replace(Environment.NewLine, " ");
                    if (str.Contains(delimiter))
                    {
                        if (str.Contains(qualifier))
                        {
                            str = str.Replace(qualifier, string.Concat(qualifier, qualifier));
                        }
                        str = string.Format("{0}{1}{0}", qualifier, str);
                    }
                    stringBuilder.Append(str);
                    stringBuilder.Append(j == table.Columns.Count - 1 ? Environment.NewLine : delimiter);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
