﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Case.Framework.Core.Extensions;

namespace Case.Framework.Core.Utilities
{
    public static class WebHelper
    {
        public const string JsTag =
            "\r\n            <script> \r\n                //<![CDATA[\r\n                {0}        \r\n                //]]>\r\n            </script>";

        public const string JsLinkTemplate = "<script src=\"{0}\"></script>";

        private const string JsModuleTemplate = "define('{0}', [], function () {{ return {1}; }});";

        public const string CssLinkTemplate = "<link rel=\"stylesheet\" href=\"{0}\" />";

        public const string CssTag = "\r\n            <style>\r\n                {0}\r\n            </style>";

        public static bool ContainsHtml(string content)
        {
            return (new Regex("<\\s*([^ >]+)[^>]*>.*?<\\s*/\\s*\\1\\s*>")).IsMatch(content);
        }

        public static string GenerateSafeName(string value, bool titleCase = false)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }
            value = (titleCase ? value.ToTitleCase() : value.ToLower());
            return Regex.Replace(value, "[!\"#$%&'()\\*\\+,\\./:;<=>\\?@\\[\\\\\\]^`{\\|}~ ]", string.Empty);
        }

        public static string GetIpAddress()
        {
            HttpContext current = HttpContext.Current;
            string item = current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(item)) return current.Request.ServerVariables["REMOTE_ADDR"];
            string[] strArrays = item.Split(new[] {','});
            return strArrays.Length != 0 ? strArrays[0] : current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static void IncludeBundle(string bundleName, string bundleContent, string placeholder, string id = null)
        {
            var currentHandler = HttpContext.Current.CurrentHandler as Page;
            if (currentHandler == null || currentHandler.Header == null) return;
            Control header;
            if (!string.IsNullOrWhiteSpace(placeholder))
            {
                header = currentHandler.Header.FindControl(placeholder) ?? currentHandler.Header;
            }
            else
            {
                header = currentHandler.Header;
            }
            var control = header;
            if (string.IsNullOrWhiteSpace(id))
            {
                id = GenerateSafeName(bundleName);
            }
            if (control.FindControl(id) != null)
            {
                return;
            }

            var htmlGenericControl = new LiteralControl
            {
                ID = id,
                Text = bundleContent
            };
            control.Controls.Add(htmlGenericControl);
        }

        public static void IncludeTitle(string title,string placeholder,string id = null)
        {
            var currentHandler = HttpContext.Current.CurrentHandler as Page;
            if (currentHandler == null || currentHandler.Header == null) return;
            Control header;
            if (!string.IsNullOrWhiteSpace(placeholder))
            {
                header = currentHandler.Header.FindControl(placeholder) ?? currentHandler.Header;
            }
            else
            {
                header = currentHandler.Header;
            }
            var control = header;
            
            if (control.FindControl(id) != null)
            {
                ((LiteralControl)control.FindControl(id)).Text = title;
                return;
            }
            var htmlGenericControl = new LiteralControl("title")
            {
                ID = id
            };
            htmlGenericControl.Text = title;
            control.Controls.Add(htmlGenericControl);
        }

        public static void IncludeCss(string path, string placeholder, string id = null)
        {
            IncludeCss(path, null, placeholder, id);
        }

        public static void IncludeCss(string path, Type type = null, string placeholder = null, string id = null)
        {
            var currentHandler = HttpContext.Current.CurrentHandler as Page;
            if (currentHandler == null || currentHandler.Header == null) return;
            Control header;
            if (!string.IsNullOrWhiteSpace(placeholder))
            {
                header = currentHandler.Header.FindControl(placeholder) ?? currentHandler.Header;
            }
            else
            {
                header = currentHandler.Header;
            }
            var control = header;
            if (string.IsNullOrWhiteSpace(id))
            {
                id = GenerateSafeName(path);
            }
            if (control.FindControl(id) != null)
            {
                return;
            }
            path = type == null
                ? WebUtility.HtmlEncode((path.StartsWith("~/") ? currentHandler.ResolveUrl(path) : path))
                : currentHandler.ClientScript.GetWebResourceUrl(type, path);
            var htmlGenericControl = new HtmlGenericControl("link")
            {
                ID = id
            };
            htmlGenericControl.Attributes.Add("href", path);
            htmlGenericControl.Attributes.Add("rel", "stylesheet");
            control.Controls.Add(htmlGenericControl);
        }

        public static void IncludeJs(string path, bool head, string id = null)
        {
            IncludeJs(path, null, head, null, id);
        }

        public static void IncludeJs(string path, string placeholder, string id = null)
        {
            IncludeJs(path, null, false, placeholder, id);
        }

        public static void IncludeJs(string path, Type type = null, bool head = false, string placeholder = null,
            string id = null)
        {
            var currentHandler = HttpContext.Current.CurrentHandler as Page;
            if (currentHandler == null) return;
            if (string.IsNullOrWhiteSpace(id))
            {
                id = GenerateSafeName(path);
            }
            path = type == null
                ? WebUtility.HtmlEncode((path.StartsWith("~/") ? currentHandler.ResolveUrl(path) : path))
                : currentHandler.ClientScript.GetWebResourceUrl(type, path);
            if (head || !string.IsNullOrWhiteSpace(placeholder))
            {
                Control control;
                if (!head)
                {
                    control = currentHandler.FindControl(placeholder);
                }
                else
                {
                    Control header;
                    if (!string.IsNullOrWhiteSpace(placeholder))
                    {
                        header = currentHandler.Header.FindControl(placeholder) ?? currentHandler.Header;
                    }
                    else
                    {
                        header = currentHandler.Header;
                    }
                    control = header;
                }
                if (control == null || control.FindControl(id) != null) return;
                var htmlGenericControl = new HtmlGenericControl("script")
                {
                    ID = id
                };
                htmlGenericControl.Attributes.Add("src", path);
                control.Controls.Add(htmlGenericControl);
            }
            else
            {
                currentHandler.ClientScript.RegisterStartupScript(currentHandler.GetType(), id, ToJsLink(path), false);
            }
        }

        public static void PostToRemote(string url, Dictionary<string, string> inputs)
        {
            if (string.IsNullOrEmpty(url))
            {
                return;
            }
            var current = HttpContext.Current;
            const string str = "remoteform1";
            current.Response.Clear();
            current.Response.Write("<html><head></head>");
            current.Response.Write(string.Format("<body onload=\"document.{0}.submit()\">", str));
            current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", str, "post", url));
            foreach (var input in inputs)
            {
                current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", input.Key,
                    input.Value));
            }
            current.Response.Write("</form>");
            current.Response.Write("</body></html>");
            current.Response.End();
        }

        public static string ResolveServerUrl(string url, bool forceHttps = false)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }
            if (url.IndexOf("://", StringComparison.Ordinal) > -1)
            {
                return !forceHttps ? url : url.Replace("http://", "https://");
            }
            var uri = new Uri(HttpContext.Current.Request.Url, ResolveUrl(url));
            if (!forceHttps)
            {
                return uri.ToString();
            }
            var uriBuilder = new UriBuilder(uri)
            {
                Scheme = Uri.UriSchemeHttps,
                Port = 443
            };
            return uriBuilder.Uri.ToString();
        }

        public static string ResolveUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }
            if (url.IndexOf("://", StringComparison.Ordinal) != -1)
            {
                return url;
            }
            if (!url.StartsWith("~"))
            {
                return url;
            }
            var num = url.IndexOf('?');
            if (num == -1)
            {
                return VirtualPathUtility.ToAbsolute(url);
            }
            string str = url.Substring(num);
            string str1 = url.Substring(0, num);
            return string.Concat(VirtualPathUtility.ToAbsolute(str1), str);
        }

        public static void StreamToBrowser(string fileLocation, string contentType)
        {
            HttpContext current = HttpContext.Current;
            string str = string.Format("attachment; filename={0}", Path.GetFileName(fileLocation));
            current.Response.AddHeader("Pragma", "public");
            current.Response.AddHeader("content-disposition", str);
            current.Response.ContentType = contentType;
            current.Response.WriteFile(current.Server.MapPath(fileLocation));
            current.Response.End();
        }

        public static void StreamToBrowser(string content, string downloadName, string contentType)
        {
            var current = HttpContext.Current;
            var str = string.Format("attachment; filename={0}", downloadName);
            current.Response.AddHeader("Pragma", "public");
            current.Response.AddHeader("content-disposition", str);
            current.Response.ContentType = contentType;
            current.Response.Write(content);
            current.Response.End();
        }

        public static void StreamToBrowser(byte[] data, string downloadName, string contentType)
        {
            var current = HttpContext.Current;
            var str = string.Format("inline; filename={0}", downloadName);
            current.Response.Buffer = true;
            current.Response.AddHeader("Pragma", "public");
            current.Response.AddHeader("content-disposition", str);
            current.Response.AddHeader("Content-Length", data.Length.ToString(CultureInfo.InvariantCulture));
            current.Response.ContentType = contentType;
            current.Response.BinaryWrite(data);
            current.Response.Flush();
            current.Response.End();
        }

        public static string StripHtml(string source)
        {
            var chrArray = new char[source.Length];
            var num = 0;
            var flag = false;
            foreach (var chr in source)
            {
                switch (chr)
                {
                    case '<':
                        flag = true;
                        break;
                    case '>':
                        flag = false;
                        break;
                    default:
                        if (!flag)
                        {
                            chrArray[num] = chr;
                            num++;
                        }
                        break;
                }
            }
            return new string(chrArray, 0, num);
        }

        public static string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
            {
                return relativeUrl;
            }
            if (relativeUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ||
                relativeUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return relativeUrl;
            }
            if (HttpContext.Current == null)
            {
                return relativeUrl;
            }
            if (relativeUrl.StartsWith("/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~");
            }
            if (!relativeUrl.StartsWith("~/"))
            {
                return relativeUrl.Insert(0, "~/");
            }
            var url = HttpContext.Current.Request.Url;
            var str = (url.Port != 80 ? string.Concat(":", url.Port) : string.Empty);
            object[] scheme = {url.Scheme, url.Host, str, VirtualPathUtility.ToAbsolute(relativeUrl)};
            return string.Format("{0}://{1}{2}{3}", scheme);
        }

        public static string ToCssBlock(string value)
        {
            return string.Format(CssTag, value);
        }

        public static string ToCssLink(string url)
        {
            return ToLinkFormat(CssLinkTemplate, url);
        }

        public static string ToJsBlock(string value)
        {
            return string.Format(JsTag, value);
        }

        public static string ToJsLink(string url)
        {
            return ToLinkFormat(JsLinkTemplate, url);
        }

        public static string ToJsModule(string key, string value)
        {
            return string.Format(JsModuleTemplate, key, value);
        }

        public static string ToLinkFormat(string format, string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return string.Empty;
            }
            url = WebUtility.HtmlEncode(url);
            if (url.StartsWith("~/"))
            {
                url = string.Concat(HostingEnvironment.ApplicationVirtualPath, url.Substring(1));
                if (url.StartsWith("//"))
                {
                    url = url.Substring(1);
                }
            }
            return string.Format(format, url);
        }
    }
}