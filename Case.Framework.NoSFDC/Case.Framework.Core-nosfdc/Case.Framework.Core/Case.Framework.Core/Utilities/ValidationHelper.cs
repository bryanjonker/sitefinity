﻿namespace Case.Framework.Core.Utilities
{
    using System.Linq;
    using System.Text.RegularExpressions;

    public static class ValidationHelper
    {
        public static bool IsValidEmail(string value)
        {
            return value != null && (new Regex("^([a-zA-Z0-9_\\-\\.\\+]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")).IsMatch(value);
        }

        public static bool IsValidNumber(int value, bool zeroAllowed, bool negativeAllowed, int? minValue = null, int? maxValue = null)
        {
            if (!zeroAllowed && value == 0)
            {
                return false;
            }
            if (!negativeAllowed && value < 0)
            {
                return false;
            }
            if (minValue.HasValue)
            {
                var num = value;
                var nullable = minValue;
                if (num < nullable.GetValueOrDefault())
                {
                    return false;
                }
            }
            if (!maxValue.HasValue)
            {
                return true;
            }
            var num1 = value;
            var nullable1 = maxValue;
            return num1 <= nullable1.GetValueOrDefault();
        }

        public static bool IsValidString(string value, bool checkForNull, bool checkIfEmpty, bool checkForSpecialChar, int minSize, int maxSize)
        {
            if (value == null)
            {
                return !checkForNull;
            }
            if (!checkForSpecialChar)
            {
                return !StringSizeIsInvalid(value, checkIfEmpty, minSize, maxSize);
            }
            if (value.Any(c => !char.IsLetterOrDigit(c)))
            {
                return false;
            }
            return !StringSizeIsInvalid(value, checkIfEmpty, minSize, maxSize);
        }

        private static bool StringSizeIsInvalid(string value, bool checkIfEmpty, int minSize, int maxSize)
        {
            if (checkIfEmpty && value.Length == 0 || minSize > 0 && value.Length < minSize)
            {
                return true;
            }
            if (maxSize <= 0)
            {
                return false;
            }
            return value.Length > maxSize;
        }
    }
}