﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web;

namespace Case.Framework.Core.Utilities
{
    public static class FileHelper
    {
        public static bool CopyDirectory(string sourcePath, string destPath, bool overwrite)
        {
            sourcePath = GetPhysicalPath(sourcePath);
            destPath = GetPhysicalPath(destPath);
            var directoryInfo = new DirectoryInfo(sourcePath);
            var directoryInfo1 = new DirectoryInfo(destPath);
            try
            {
                if (directoryInfo.Exists)
                {
                    if (!directoryInfo1.Exists)
                    {
                        directoryInfo1.Create();
                    }
                    var files = directoryInfo.GetFiles();
                    foreach (var fileInfo in files)
                    {
                        if (overwrite)
                        {
                            fileInfo.CopyTo(Path.Combine(directoryInfo1.FullName, fileInfo.Name), true);
                        }
                        else if (!File.Exists(Path.Combine(directoryInfo1.FullName, fileInfo.Name)))
                        {
                            fileInfo.CopyTo(Path.Combine(directoryInfo1.FullName, fileInfo.Name), false);
                        }
                    }
                    var directories = directoryInfo.GetDirectories();
                    foreach (var directoryInfo2 in directories)
                    {
                        CopyDirectory(directoryInfo2.FullName,
                            Path.Combine(directoryInfo1.FullName, directoryInfo2.Name), overwrite);
                    }
                    return true;
                }
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {
            }
            return false;
        }

        public static string FindLineContaining(string linePattern, string file)
        {
            string end;
            file = GetPhysicalPath(file);
            using (var streamReader = new StreamReader(file))
            {
                end = streamReader.ReadToEnd();
            }
            var length = 0;
            for (var i = GetNextLine(end, length); i.Length > 0; i = GetNextLine(end, length))
            {
                if (i.Contains(linePattern))
                {
                    return i;
                }
                length = length + i.Length;
            }
            return null;
        }

        public static string GetContent(string file)
        {
            string end;
            file = GetPhysicalPath(file);
            using (var streamReader = new StreamReader(file))
            {
                end = streamReader.ReadToEnd();
            }
            return end;
        }

        public static string GetEmbeddedResource(string resourceName, Type type = null)
        {
            string end;
            using (
                var streamReader =
                    new StreamReader(
// ReSharper disable once AssignNullToNotNullAttribute
                        ((type != null ? type.Assembly : Assembly.GetExecutingAssembly())).GetManifestResourceStream(
                            resourceName)))
            {
                end = streamReader.ReadToEnd();
            }
            return end;
        }

        public static string GetNextAvailableFile(string file)
        {
            file = GetPhysicalPath(file);
            var str = file;
            var num = 2;
            while (File.Exists(str))
            {
                str = string.Concat(file.Substring(0, file.LastIndexOf('.')), "_", num.ToString(CultureInfo.InvariantCulture),
                    Path.GetExtension(file));
                num++;
            }
            return str;
        }

        public static string GetNextLine(string input, int startPos)
        {
            if (startPos >= input.Length)
            {
                return string.Empty;
            }
            var num = input.IndexOf('\n', startPos);
            return num <= -1 ? input.Substring(startPos) : input.Substring(startPos, num - startPos + 1);
        }

        public static string GetPhysicalPath(string file, bool mustExist = false)
        {
            if (string.IsNullOrWhiteSpace(file))
            {
                return string.Empty;
            }
            if (!Path.IsPathRooted(file))
            {
                file = HttpContext.Current.Server.MapPath(file);
            }
            if (mustExist && !File.Exists(file))
            {
                return string.Empty;
            }
            return file;
        }

        public static string[] GetSafeFileExts()
        {
            string[] strArrays =
            {
                ".jpg", ".jpeg", ".gif", ".png", ".bmp", ".tif", ".tiff", ".txt", ".rtf", ".doc",
                ".docx", ".xls", ".xlsx", ".pps", ".pdf", ".mp3", ".wav", ".avi", ".mpg", ".mpeg", ".wmv", ".iso",
                ".flv", ".mov", ".dvr", ".vob", ".zip", ".tar", ".rar", ".psd", ".ai"
            };
            return strArrays;
        }

        public static string[] GetVirtualFiles(string directoryPath, string searchPattern)
        {
            string[] files = null;
            try
            {
                files = Directory.GetFiles(GetPhysicalPath(directoryPath), searchPattern,
                    SearchOption.AllDirectories);
                for (var i = 0; i < files.Length; i++)
                {
                    files[i] = GetVirtualPath(files[i]);
                }
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {
            }
            return files;
        }

        public static string GetVirtualPath(string file)
        {
            if (string.IsNullOrWhiteSpace(file))
            {
                return string.Empty;
            }
            return string.Concat("~/",
// ReSharper disable once AssignNullToNotNullAttribute
                file.Replace(HttpContext.Current.Request.PhysicalApplicationPath, string.Empty).Replace("\\", "/"));
        }

        public static bool IsFileLocked(string file)
        {
            var flag = false;
            file = GetPhysicalPath(file);
            try
            {
                File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None).Close();
            }
            catch
            {
                flag = true;
            }
            return flag;
        }

        public static void SaveStreamToFile(string fileFullPath, Stream stream)
        {
            if (stream.Length == 0)
            {
                return;
            }
            var buffer = new byte[16*1024];

            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                var numArray = ms.ToArray();
                using (var fileStream = File.Create(fileFullPath, (int) stream.Length))
                {
                    stream.Read(numArray, 0, numArray.Length);
                    fileStream.Write(numArray, 0, numArray.Length);
                }
            }
        }

        public static string ScrubDelimitedField(string value, string delimited = "\t", string replace = " ")
        {
            return string.IsNullOrWhiteSpace(value) ? value : value.Replace(Environment.NewLine, " ").Replace("\n", " ").Replace(delimited, replace);
        }

        public static void UpdateContent(string file, string contents)
        {
            file = GetPhysicalPath(file);
            string empty;
            using (var streamReader = new StreamReader(file))
            {
                empty = streamReader.ReadToEnd();
            }
            if (contents == empty)
            {
                (new FileInfo(file)).LastWriteTime = DateTime.Now;
            }
            else
            {
                using (var streamWriter = new StreamWriter(file))
                {
                    streamWriter.Write(contents);
                }
            }
        }
    }
}
