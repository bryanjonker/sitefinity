﻿namespace Case.Framework.Core.Utilities
{
    using System.Text.RegularExpressions;
    using System.Web;

    public static class HtmlRemovalHelper
    {
        private static readonly Regex HtmlRegex = new Regex("<[^>]+>|&nbsp;", RegexOptions.Compiled);

        public static string StripTagsRegex(string source)
        {
            source = HttpUtility.HtmlDecode(source);
            return Regex.Replace(source, "<[^>]+>|&nbsp;", string.Empty);
        }

        public static string StripTagsRegexCompiled(string source)
        {
            return HtmlRegex.Replace(source, string.Empty);
        }

        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}