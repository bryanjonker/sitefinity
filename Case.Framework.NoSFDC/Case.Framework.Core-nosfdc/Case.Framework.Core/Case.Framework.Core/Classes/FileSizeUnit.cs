﻿namespace Case.Framework.Core.Classes
{
    public sealed class FileSizeUnit
    {
        public readonly static FileSizeUnit KiloByte;

        public readonly static FileSizeUnit MegaByte;

        public readonly static FileSizeUnit GigaByte;

        public readonly static FileSizeUnit TeraByte;

        public readonly static FileSizeUnit PetaByte;

        public readonly static FileSizeUnit ExaByte;

        public readonly ulong Size;

        public readonly string Suffix;

        static FileSizeUnit()
        {
            KiloByte = new FileSizeUnit(1024, "KB");
            MegaByte = new FileSizeUnit(1048576, "MB");
            GigaByte = new FileSizeUnit(1073741824, "GB");
            TeraByte = new FileSizeUnit(1099511627776L, "TB");
            PetaByte = new FileSizeUnit(1125899906842624L, "PB");
            ExaByte = new FileSizeUnit(1152921504606846976L, "EB");
        }

        private FileSizeUnit(ulong size, string suffix)
        {
            this.Size = size;
            this.Suffix = suffix;
        }
    }
}
