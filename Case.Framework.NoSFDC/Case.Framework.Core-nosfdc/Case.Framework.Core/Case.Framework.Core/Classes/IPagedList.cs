﻿using System.Collections.Generic;

namespace Case.Framework.Core.Classes
{
    public interface IPagedList<out T> : IPagedList, IEnumerable<T>
    {
        int Count { get; }

        T this[int index] { get; }

        IPagedList GetMetaData();
    }

    public interface IPagedList
    {
        int FirstItemOnPage { get; }

        bool HasNextPage { get; }

        bool HasPreviousPage { get; }

        bool IsFirstPage { get; }

        bool IsLastPage { get; }

        int LastItemOnPage { get; }

        int PageCount { get; }

        int PageNumber { get; }

        int PageSize { get; }

        int TotalItemCount { get; }
    }
}