﻿namespace Case.Framework.Core.Classes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    [Serializable]
    public abstract class BasePagedList<T> : PagedListMetaData, IPagedList<T>
    {
        protected readonly List<T> Subset = new List<T>();

        protected internal BasePagedList()
        {
        }

        protected internal BasePagedList(int pageNumber, int pageSize, int totalItemCount)
        {
            if (pageNumber < 1)
            {
                throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "PageNumber cannot be below 1.");
            }
            if (pageSize < 1)
            {
                throw new ArgumentOutOfRangeException("pageSize", pageSize, "PageSize cannot be less than 1.");
            }

            // set source to blank list if superset is null to prevent exceptions
            this.TotalItemCount = totalItemCount;
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            this.PageCount = this.TotalItemCount > 0
                            ? (int)Math.Ceiling(this.TotalItemCount / (double)this.PageSize)
                            : 0;
            this.HasPreviousPage = this.PageNumber > 1;
            this.HasNextPage = this.PageNumber < this.PageCount;
            this.IsFirstPage = this.PageNumber == 1;
            this.IsLastPage = this.PageNumber >= this.PageCount;
            this.FirstItemOnPage = (this.PageNumber - 1) * this.PageSize + 1;
            var numberOfLastItemOnPage = this.FirstItemOnPage + this.PageSize - 1;
            this.LastItemOnPage = numberOfLastItemOnPage > this.TotalItemCount
                                 ? this.TotalItemCount
                                 : numberOfLastItemOnPage;
        }

        public int Count
        {
            get
            {
                return this.Subset.Count;
            }
        }

        public T this[int index]
        {
            get
            {
                return this.Subset[index];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.Subset.GetEnumerator();
        }

        public IPagedList GetMetaData()
        {
            return new PagedListMetaData(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}