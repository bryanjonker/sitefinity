﻿namespace Case.Framework.Core.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [Serializable]
    public class PagedList<T> : BasePagedList<T>
    {
        public PagedList(IQueryable<T> superset, int pageNumber, int pageSize)
        {
            if (pageNumber < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(pageNumber), pageNumber, "PageNumber cannot be below 1.");
            }
            if (pageSize < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(pageSize), pageSize, "PageSize cannot be less than 1.");
            }

            // set source to blank list if superset is null to prevent exceptions
            this.TotalItemCount = superset?.Count() ?? 0;
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            this.PageCount = this.TotalItemCount > 0
                            ? (int)Math.Ceiling(this.TotalItemCount / (double)this.PageSize)
                            : 0;
            this.HasPreviousPage = this.PageNumber > 1;
            this.HasNextPage = this.PageNumber < this.PageCount;
            this.IsFirstPage = this.PageNumber == 1;
            this.IsLastPage = this.PageNumber >= this.PageCount;
            this.FirstItemOnPage = (this.PageNumber - 1) * this.PageSize + 1;
            var numberOfLastItemOnPage = this.FirstItemOnPage + this.PageSize - 1;
            this.LastItemOnPage = numberOfLastItemOnPage > this.TotalItemCount
                                 ? this.TotalItemCount
                                 : numberOfLastItemOnPage;

            // add items to internal list
            if (superset != null && this.TotalItemCount > 0)
            {
                this.Subset.AddRange(pageNumber == 1
                                    ? superset.Skip(0).Take(pageSize).ToList()
                                    : superset.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList()
                    );
            }
        }

        public PagedList(IEnumerable<T> superset, int pageNumber, int pageSize)
            : this(superset.AsQueryable<T>(), pageNumber, pageSize) {}
    }
}