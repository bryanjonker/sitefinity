﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Case.Framework.Core.DynamicCollection
{
    public static class DynamicExtensions
    {
        /// <summary>
        ///     Converts a dictionary into a dynamic implementation
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dictionary">The dictionary.</param>
        /// <returns>
        ///     A dynamic implementation of the dictionary using <see>
        ///         <cref>DynamicDictionary&amp;lt;TKey, TValue&amp;gt;</cref>
        ///     </see>
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the dictionary is null</exception>
        public static dynamic AsDynamic<TValue>(this IDictionary<string, TValue> dictionary)
        {
            if (dictionary == null)
                throw new ArgumentNullException("dictionary", "Dictionary cannot be null");
            return new DynamicDictionary<TValue>(dictionary);
        }

        /// <summary>
        ///     Converts a KeyValuePair into a dynamic implementation
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static dynamic AsDynamic<TKey, TValue>(this KeyValuePair<TKey, TValue> item)
        {
            return new DynamicKeyValuePair<TKey, TValue>(item);
        }

        /// <summary>
        ///     Converts an XElement to the expando
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static dynamic ToElastic(this XElement e)
        {
            return ElasticFromXElement(e);
        }


        /// <summary>
        ///     Converts an expando to XElement
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static XElement ToXElement(this ElasticObject e)
        {
            return XElementFromElastic(e);
        }

        /// <summary>
        ///     Build an expando from an XElement
        /// </summary>
        /// <param name="el"></param>
        /// <returns></returns>
        public static ElasticObject ElasticFromXElement(XElement el)
        {
            var exp = new ElasticObject();

            if (!string.IsNullOrEmpty(el.Value))
                exp.InternalValue = el.Value;

            exp.InternalName = el.Name.LocalName;

            foreach (var a in el.Attributes())
                exp.CreateOrGetAttribute(a.Name.LocalName, a.Value);


            var textNode = el.Nodes().FirstOrDefault();
            if (textNode is XText)
            {
                exp.InternalContent = textNode.ToString();
            }

            foreach (var child in el.Elements().Select(ElasticFromXElement))
            {
                child.InternalParent = exp;
                exp.AddElement(child);
            }
            return exp;
        }


        /// <summary>
        ///     Returns an XElement from an ElasticObject
        /// </summary>
        /// <param name="elastic"></param>
        /// <returns></returns>
        public static XElement XElementFromElastic(ElasticObject elastic)
        {
            var exp = new XElement(elastic.InternalName);


            foreach (var a in elastic.Attributes.Where(a => a.Value.InternalValue != null))
            {
                exp.Add(new XAttribute(a.Key, a.Value.InternalValue));
            }

            if (null != elastic.InternalContent && elastic.InternalContent is string)
            {
                exp.Add(new XText(elastic.InternalContent as string));
            }

            foreach (var child in elastic.Elements.Select(XElementFromElastic))
            {
                exp.Add(child);
            }
            return exp;
        }
    }
}
