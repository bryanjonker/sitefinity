﻿using System;
using System.Linq;

namespace Case.Framework.Core.Extensions
{
    public static class TypeExtensions
    {
        public static bool IsNullableType(this Type value)
        {
            if (value == null || !value.IsGenericType)
            {
                return false;
            }
            var genericTypeDefinition = value.GetGenericTypeDefinition();
            var type = typeof(Nullable<>);
            if (genericTypeDefinition == null)
            {
                return false;
            }
            return genericTypeDefinition == type;
        }

        public static bool IsSimpleType(this Type value)
        {
            var typeArray = new[] { typeof(string), typeof(decimal), typeof(DateTime), typeof(DateTimeOffset), typeof(TimeSpan), typeof(Guid) };
            var typeArray1 = typeArray;
            if (value.IsValueType || value.IsPrimitive || typeArray1.Contains(value))
            {
                return true;
            }
            return Convert.GetTypeCode(value) != TypeCode.Object;
        }
    }
}
