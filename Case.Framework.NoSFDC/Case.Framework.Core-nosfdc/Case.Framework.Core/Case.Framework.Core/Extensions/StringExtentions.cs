﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Case.Framework.Core.Extensions
{
    public static class StringExtensions
    {
        public static string EscapeForFormat(this string value)
        {
            return Regex.Replace(value.Replace("{", "{{").Replace("}", "}}"), "{{([[0-9]+)}}", "{$1}");
        }

        public static string FirstLetterToUpper(this string value)
        {
            return string.IsNullOrEmpty(value) ? value : string.Concat(char.ToUpper(value[0]), value.Substring(1));
        }

        public static string GetFirstChars(this string value, int count)
        {
            if (string.IsNullOrWhiteSpace(value) || count <= 0)
            {
                return string.Empty;
            }
            return value.Length <= count ? value : value.Substring(0, count);
        }

        public static string MaskLeft(this string value, int showRightCount, char maskCharacter = '*')
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value.Length <= showRightCount ? value : value.Substring(value.Length - showRightCount).PadLeft(value.Length, maskCharacter);
        }

        public static string RemoveUrlQueryAndHash(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }
            if (value.Contains("?"))
            {
                var chrArray = new[] { '?' };
                value = value.Split(chrArray)[0];
            }
            if (!value.Contains("#")) return value;
            var chrArray1 = new[] { '#' };
            value = value.Split(chrArray1)[0];
            return value;
        }

        public static string[] Split(this string value, string separator, StringSplitOptions options)
        {
            return value?.Split(new[] { separator }, options) ?? new string[0];
        }

        public static string ToTitleCase(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? value : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower());
        }

        public static string TrimEnd(this string value, string stringToTrim)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(stringToTrim))
            {
                return value;
            }
            while (value.EndsWith(stringToTrim))
            {
                value = value.Remove(value.LastIndexOf(stringToTrim, StringComparison.Ordinal));
            }
            return value;
        }
    }
}
