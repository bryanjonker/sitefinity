﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Case.Framework.Core.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value)
        {
            if (value == null)
            {
                return true;
            }
            return !value.Any();
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            if (list == null)
            {
                return;
            }
            var random = new Random();
            var count = list.Count;
            while (count > 1)
            {
                count--;
                var item = random.Next(count + 1);
                var t = list[item];
                list[item] = list[count];
                list[count] = t;
            }
        }
    }
}
