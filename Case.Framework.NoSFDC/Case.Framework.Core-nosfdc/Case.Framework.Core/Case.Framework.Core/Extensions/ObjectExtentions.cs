﻿using System;
using System.ComponentModel;

namespace Case.Framework.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static TDestinationType ChangeTypeTo<TDestinationType>(this object value)
        {
            TDestinationType tDestinationType;
            if (value != null && typeof(TDestinationType) == value.GetType())
            {
                return (TDestinationType)value;
            }
            var underlyingType = typeof(TDestinationType);
            if (underlyingType.IsNullableType())
            {
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                {
                    return default(TDestinationType);
                }
                underlyingType = (new NullableConverter(underlyingType)).UnderlyingType;
            }
            try
            {
                tDestinationType = (TDestinationType)Convert.ChangeType(value, underlyingType);
            }
            catch (FormatException formatException1)
            {
                var formatException = formatException1;
                var str = $"Cannot cast element value '{value}' to type '{typeof(TDestinationType).Name}'.";
                throw new InvalidCastException(str, formatException);
            }
            return tDestinationType;
        }
    }
}
