﻿namespace Case.Framework.Core
{
    public static class Constants
    {
        public const string ValueAlphanumericRegex = "^[a-zA-Z]+[_ a-zA-Z0-9]*$";

        public const string ValueNumericRegex = "^\\d+$";

        public const string KeySecurityEncryption = "Case.Framework.Core.Security.EncryptionKey";

        public const string KeySecurityHash = "Case.Framework.Core.Security.Hash";
    }
}
