﻿using Case.Framework.Core.Extensions;
using System.Configuration;

namespace Case.Framework.Core.Configuration
{
    public static class AppSettings
    {
        public static string Get(string key, string defaultValue = null)
        {
            return ConfigurationManager.AppSettings[key] ?? (defaultValue ?? string.Empty);
        }

        public static T Get<T>(string key, T defaultValue)
        {
            string item = ConfigurationManager.AppSettings[key];
            if (item == null)
            {
                return default(T);
            }
            return item.ChangeTypeTo<T>();
        }
    }
}
