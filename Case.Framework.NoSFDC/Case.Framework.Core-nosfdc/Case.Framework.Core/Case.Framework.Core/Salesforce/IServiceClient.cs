using System.Threading.Tasks;

namespace Case.Framework.Sitefinity.Salesforce
{
    public interface IServiceHttpClient
    {

        Task<T> HttpGetAsync<T>(string urlSuffix);
        Task<T> HttpPostAsync<T>(object inputObject, string urlSuffix);
        Task<bool> HttpPatchAsync(object inputObject, string urlSuffix);
        Task<bool> HttpDeleteAsync(string urlSuffix);
        Task<T> HttpGetRestArrayServiceAsync<T>(string urlSuffix);
        Task<T> HttpGetRestServiceAsync<T>(string urlSuffix);
        void Dispose();
    }
}