﻿namespace Case.Framework.Core.Salesforce.Models
{
    public class AuthErrorResponse
    {
        public string error_description;
        public string error;
    }
}