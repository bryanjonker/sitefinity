﻿namespace Case.Framework.Core.Salesforce.Models
{
    public class AuthToken
    {
        public string id;
        public string issued_at;
        public string instance_url;
        public string signature;
        public string access_token;
        public string refresh_token;
    }
}
