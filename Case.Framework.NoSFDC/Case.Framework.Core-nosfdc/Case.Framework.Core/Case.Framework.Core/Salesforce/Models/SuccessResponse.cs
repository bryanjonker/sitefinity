﻿namespace Case.Framework.Core.Salesforce.Models
{
    public class SuccessResponse
    {
        public string id;
        public string success;
        public object errors;
    }
}

