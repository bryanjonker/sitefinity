﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Core.Salesforce.Models
{
    public class DescribeGlobalResult<T>
    {
        public string encoding { get; set; }
        public int maxBatchSize { get; set; }
        public List<T> sobjects { get; set; }
    }
}
