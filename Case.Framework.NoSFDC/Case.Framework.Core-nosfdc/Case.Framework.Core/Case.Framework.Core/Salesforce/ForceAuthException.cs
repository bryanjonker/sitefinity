﻿using Case.Framework.Core.Salesforce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case.Framework.Sitefinity.Salesforce
{
    public class ForceAuthException : ForceException
    {
        public ForceAuthException(string error, string description)
            : base(error, description)
        {
        }

        public ForceAuthException(string error, string description, string[] fields)
            : base(error, description, fields)
        {
        }

        public ForceAuthException(Error error, string description, string[] fields)
            : base(error, description, fields)
        {
        }

        public ForceAuthException(Error error, string description)
            : base(error, description)
        {
        }
    }
}
