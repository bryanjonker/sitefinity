﻿namespace Case.Framework.Core.Security
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;

    using Case.Framework.Core.Configuration;

    public class Encryption
    {
        private const string DefaultEncryptionKey = "%#@case#^!!fsaw!W";

        private readonly string encryptionKey = string.Empty;

        private readonly byte[] iv = { 18, 52, 86, 120, 144, 171, 205, 239 };

        private byte[] encryptionBytes = new byte[0];

        public Encryption()
        {
            this.encryptionKey = AppSettings.Get("Case.Framework.Core.Security.EncryptionKey", DefaultEncryptionKey);
        }

        public Encryption(string encryptionKey)
        {
            this.encryptionKey = !string.IsNullOrWhiteSpace(encryptionKey) ? encryptionKey.PadRight(8, ' ') : AppSettings.Get("Case.Framework.Core.Security.EncryptionKey", "%#@case#^!!fsaw!W");
        }

        public string Decrypt(string stringToDecrypt)
        {
            string str = null;
            stringToDecrypt = HttpUtility.UrlDecode(stringToDecrypt);
            if (stringToDecrypt != null)
            {
                var length = stringToDecrypt.Length % 4;
                if (length > 0)
                {
                    stringToDecrypt = string.Concat(stringToDecrypt, new string('=', 4 - length));
                }
            }
            try
            {
                this.encryptionBytes = Encoding.UTF8.GetBytes(this.encryptionKey.Substring(0, 8));
                var dEsCryptoServiceProvider = new DESCryptoServiceProvider();
                if (stringToDecrypt != null)
                {
                    var numArray = Convert.FromBase64String(stringToDecrypt.Replace(" ", "+"));
                    var memoryStream = new MemoryStream();
                    var cryptoStream = new CryptoStream(memoryStream, dEsCryptoServiceProvider.CreateDecryptor(this.encryptionBytes, this.iv), CryptoStreamMode.Write);
                    cryptoStream.Write(numArray, 0, numArray.Length);
                    cryptoStream.FlushFinalBlock();
                    str = Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
            catch (Exception)
            {
                str = stringToDecrypt;
            }
            return str;
        }

        public string Encrypt(string stringToEncrypt)
        {
            string message;
            try
            {
                this.encryptionBytes = Encoding.UTF8.GetBytes(this.encryptionKey.Substring(0, 8));
                var dEsCryptoServiceProvider = new DESCryptoServiceProvider();
                var bytes = Encoding.UTF8.GetBytes(stringToEncrypt);
                var memoryStream = new MemoryStream();
                var cryptoStream = new CryptoStream(memoryStream, dEsCryptoServiceProvider.CreateEncryptor(this.encryptionBytes, this.iv), CryptoStreamMode.Write);
                cryptoStream.Write(bytes, 0, bytes.Length);
                cryptoStream.FlushFinalBlock();
                message = HttpUtility.UrlEncode(Convert.ToBase64String(memoryStream.ToArray()));
            }
            catch (Exception exception)
            {
                message = exception.Message;
            }
            return message;
        }
    }
}