/// <binding BeforeBuild='default' />
/// <vs BeforeBuild='default' />
var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var cssmin = require('gulp-cssmin');
var sass = require('gulp-sass');

gulp.task('sass-compile', function () {
    gulp.src('theme/css/college-of-illinois.scss')
        .pipe(sass())
        .pipe(gulp.dest('theme/css/'));
});

gulp.task("main-scripts", function () {
    return gulp.src(["theme/js/jquery.simplemodal-1.4.4.js", "theme/js/main.js", "theme/js/accordion.js", "theme/js/lunametrics-youtube-v7.gtm.min.js", "theme/customjs/zopim.js", "theme/js/app.js", "theme/customjs/slider.js"])
      .pipe(concat("all.js"))
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("mainsearch-scripts", function () {
    return gulp.src("theme/customjs/MainSearch.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("events-scripts", function () {
    return gulp.src("theme/customjs/Events.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("facetedsearch-scripts", function () {
    return gulp.src("theme/customjs/FacetedSearch.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("searchwidgetcourses-scripts", function () {
    return gulp.src("theme/customjs/searchwidgetcourses.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("searchwidgetprograms-scripts", function () {
    return gulp.src("theme/customjs/searchwidgetprograms.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("facultyfinder-scripts", function () {
    return gulp.src("theme/customjs/FacultyFinder.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("findanexpert-scripts", function () {
    return gulp.src("theme/customjs/FindAnExpert.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("news-scripts", function () {
    return gulp.src("theme/customjs/News.js")
      .pipe(uglify())
      .pipe(gulp.dest("theme/js"));
});

gulp.task("css", ['sass-compile'], function () {
    return gulp.src(["theme/webfonts/MyFontsWebfontsKit/MyFontsWebfontsKit.css", "theme/ie/*.css", "theme/webfonts/*.css", "theme/css/jquery-ui.min.css", "theme/css/video-js.css", "theme/css/college-of-illinois.css", "theme/css/custom.css"])
      .pipe(concat("main.css"))
      .pipe(cssmin())
      .pipe(gulp.dest("theme/css"));
});

gulp.task("csswebfont", ['css'], function () {
    return gulp.src(["theme/webfonts/MyFontsWebfontsKit/MyFontsWebfontsKit.css", "theme/css/main.css"])
      .pipe(concat("all.css"))
      .pipe(gulp.dest("theme/css/"));
});

gulp.task("occrl-css", ['css'], function () {
    return gulp.src(["theme/ie/*.css", "theme/webfonts/*.css", "theme/css/jquery-ui.min.css", "theme/css/video-js.css", "theme/css/college-of-illinois.css", "theme/css/custom.css", , "theme/css/occrl-custom.css"])
      .pipe(concat("occrl.css"))
      .pipe(cssmin())
      .pipe(gulp.dest("theme/css"));
});

gulp.task("default", ["sass-compile", "events-scripts", "mainsearch-scripts", "facetedsearch-scripts", "searchwidgetcourses-scripts", "searchwidgetprograms-scripts", "facultyfinder-scripts", "findanexpert-scripts", "news-scripts", "main-scripts", "css", "csswebfont", "occrl-css"]);