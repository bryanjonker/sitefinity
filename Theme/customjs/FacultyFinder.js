﻿jQuery(function ($) {

    $(".input_text").autocomplete({
        messages: {
            noResults: "",
            results: function() {}
        },
        source: function (request, response) {
            $.ajax({
                url: "/api/FacultySearch/GetSuggestions",
                data: { searchTerm: request.term },
                type: "GET",
                error: function (xhr) {
                    alert(xhr.status);
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        select: function () { getFacultyData(1, true, true); return true; }
    });

    jQuery.ui.autocomplete.prototype._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth());
    }

    var department = $('#Dep').val();
    if (department != undefined) {
        $("span[aria-label='Expand 1']").trigger( "click" );
    }

    $.expr[":"].containsNoCase = function (el, i, m) {
        var search = m[3];
        if (!search) return false;
        return new RegExp(search, "i").test($(el).text());
    };

    function buildPagination(numItems, perPage) {
        $('.pager').show();
        $(".pager").pagination({
            items: numItems,
            itemsOnPage: perPage,
            prevText: '&lsaquo;',
            nextText: '&rsaquo;',
            displayedPages: 3,
            edges: 1,
            cssStyle: "light-theme",
            onPageClick: function (pageNumber) { getFacultyData(pageNumber, false); }
        });
    }
    
    function getFacultyData(page, rebuildPager, selectedFromAutoComplete) {
        if (selectedFromAutoComplete == null) {
            selectedFromAutoComplete = false;
        }

        var departmentsArray = [];
        var centersArray = [];

        $('#departments input[type=checkbox]:checked').each(function () { departmentsArray.push($(this).val()); });
        $('#Centers input[type=checkbox]:checked').each(function () { centersArray.push($(this).val()); });

        var size = parseInt($("#pagination-bottom").val());
 
        $.ajax({
            url: '/api/FacultySearch/SearchData',
            data: { searchTerm: $('.input_text').val().toLowerCase(), departments: departmentsArray.join("[-]").toLowerCase(), centers: centersArray.join("[-]").toLowerCase(), page: page, size: size },
            type: 'GET',
            //    beforeSend: function () { $(".ajaxoverlay").show() },
            //    complete: function () { $(".ajaxoverlay").hide() },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
            },
            success: function (data) {
                // if selected from autocomplete AND only one result returned, take them to profile
                if (selectedFromAutoComplete && data.Faculty.length == 1) {
                    for (i = 0; i < data.Faculty.length; i++) {
                        window.location.href = data.Faculty[i].Url;
                    }
                } else {
                    $('.results').empty();
                    var html;
                    html = '<li><img alt="${Name}" src="${Image}" />';
                    html += '<div class="faculty-name"><a href="${Url}">${Name}</a></div>';
                    html += ' <div class="faculty-description">${Title}</div>';
                    html += '<p>${Department}</p>';
                    $.tmpl(html, data.Faculty).appendTo(".results");

                    $('.pagination.display').show();
                    var minSize = (page - 1) * size + 1;
                    var maxSize = page * size > data.TotalResults ? data.TotalResults : page * size;
                    if (data.TotalResults === 0 && data.SpellingSuggestion.length > 0) {
                        $('.pager').hide();
                        $('.pagination.display').html('Did you mean <a href="#" class="insertSuggestion">' + data.SpellingSuggestion.substring(0, data.SpellingSuggestion.indexOf("(")) + '</a>?');
                    }
                    else if (data.TotalResults === 0) {
                        $('.pager').hide();
                        $('.pagination.display').html('No results were found with that search criteria');
                    }
                    else if (data.TotalResults < size) {
                        $('.pager').hide();
                        $('.pagination.display').html('Showing <strong>' + data.TotalResults + '</strong> of all <strong>' + data.TotalResults + ' faculty</strong>');
                    } else if ($('.input_text').val() === "" && departmentsArray.length === 0 && centersArray.length === 0) {
                        if (rebuildPager)
                            buildPagination(data.TotalResults, size);
                        $('.pagination.display').html('Showing <strong>' + minSize + '-' + maxSize + '</strong> of all <strong>' + data.TotalResults + ' faculty</strong>');
                    } else {
                        if (rebuildPager)
                            buildPagination(data.TotalResults, size);
                        $('.pagination.display').html('Showing <strong>' + minSize + '-' + maxSize + '</strong> of <strong>' + data.TotalResults + ' faculty</strong>');
                    }
                }
            }
        });
    }

    $("#pagination-top").change(function () {
        $('.pager').pagination('destroy');
        var value = parseInt($(this).val());
        $("#pagination-bottom").val(value);
        getFacultyData(1, true);
    });

    $("#pagination-bottom").change(function () {
        $('.pager').pagination('destroy');
        var value = parseInt($(this).val());
        $("#pagination-top").val(value);
        getFacultyData(1, true);
     });


    $('#find').on('click', function (event) {
        event.preventDefault();
        var selectedAutoCompleteItem = $('.autocomplete-suggestion.selected');

        // check if autocomplete item was selected for search
        if (selectedAutoCompleteItem.length >= 1) {
            getFacultyData(1, true, true);
        } else {
            getFacultyData(1, true, false);
        }
    });

    $('.pagination.display').on('click', '.insertSuggestion', function (event) {
        event.preventDefault();
        $('.input_text').val($('.insertSuggestion').text().trim());
        getFacultyData(1, true, true);
    });

    $("input[type='checkbox']").change(function () {
        getFacultyData(1, true);
    });


    var indexOf = window.location.search.indexOf("q=");
    if (indexOf > 0) {
        $('.input_text').val(decodeURI(window.location.search.substring(indexOf + 2)));
    }
    getFacultyData(1, true, false);
});