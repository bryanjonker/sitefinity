﻿jQuery(function ($) {
    var previousUrl;
    if (document.referrer != '')
        previousUrl = document.referrer;
    else previousUrl = window.location.href;

    function removeURLParameter(url, parameter,page) {
        //prefer to use l.search if you have a location/link object
        var urlparts = url.split('?');
        if (urlparts.length >= 2) {

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + '?' + pars.join('&') + '&page=' + page;
            return url;
        } else {
            return url + '?page=' + page;
        }
    }

         function buildPagination(items, per,currentPage) {
             var numItems = items;
             var perPage = per;
             if (parseInt(numItems) > parseInt(per)) {
                 $(".pageination").show();
                 // now setup pagination
                 $(".pagination").pagination({
                     items: numItems,
                     currentPage: currentPage,
                     prevText: '&lsaquo;',
                     nextText: '&rsaquo;',
                     displayedPages: 3,
                     edges: 1,
                     itemsOnPage: perPage,
                     cssStyle: "light-theme",
                     onPageClick: function (pageNumber, e) { // this is where the magic happens
                         e.preventDefault();
                         window.location.href = removeURLParameter(window.location.href, 'page', pageNumber);
                     }
                 });
             } else {
                 $(".pageination").hide();
             }
         }

         buildPagination($('#itemCount').val(), $('#pagesize').val(),$('#page').val())
     });
