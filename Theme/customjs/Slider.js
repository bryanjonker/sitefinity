﻿var sliders = $(".slider").closest("span");
if (sliders.length > 0) {
    sliders.each(function () {
        var that = this;
        var randomItem = Math.floor((Math.random() * $("#" + that.id + " img").length));
        var defaultImg = $("#" + that.id + " img:eq(" + randomItem + ")");

        defaultImg.addClass("active");
        $("#" + that.id + " img").hide();
        defaultImg.show();

        if (defaultImg.attr("data-link") !== "") {
            $("#" + that.id + " #MoreLink").show();
            $("#" + that.id + " #MoreLink").attr("href", defaultImg.attr("data-link"));
        } else {
            $("#" + that.id + " #MoreLink").hide();
        }

        $("#" + that.id + " .caption_right").html(defaultImg.attr("data-caption"));

        $("#" + this.id + " a.slider_l").click(function () {
            var activeItem = $("#" + that.id + " img.active");
            var prevItem = activeItem.prev();
            if (!(prevItem.is("img") && prevItem.length > 0)) {
                prevItem = $("#" + that.id + " .last_img");
            }
            if (prevItem.is("img") && prevItem.length > 0) {
                activeItem.removeClass("active");
                prevItem.addClass("active");
                activeItem.hide();
                prevItem.fadeIn(1000);
                if (prevItem.attr("data-link") !== "") {
                    $("#" + that.id + " #MoreLink").show();
                    $("#" + that.id + " #MoreLink").attr("href", prevItem.attr("data-link"));
                } else {
                    $("#" + that.id + " #MoreLink").hide();
                }
                $("#" + that.id + " .caption_right").html(prevItem.attr("data-caption"));
            }
        });

        $("#" + this.id + " a.slider_r").click(function () {
            var activeItem = $("#" + that.id + " img.active");
            var nextItem = activeItem.next();
            if (!(nextItem.is("img") && nextItem.length > 0)) {
                nextItem = $("#" + that.id + " .first_img");
            }
            if (nextItem.is("img") && nextItem.length > 0) {
                activeItem.removeClass("active");
                nextItem.addClass("active");
                activeItem.hide();
                nextItem.fadeIn(1000);
                if (nextItem.attr("data-link") !== "") {
                    $("#" + that.id + " #MoreLink").show();
                    $("#" + that.id + " #MoreLink").attr("href", nextItem.attr("data-link"));
                } else {
                    $("#" + that.id + " #MoreLink").hide();
                }
                $("#" + that.id + " .caption_right").html(nextItem.attr("data-caption"));
            }
        });
    });
}