﻿var cx = '006447029465651469952:l5metdnmzla';
var gcse = document.createElement('script');
gcse.type = 'text/javascript';
gcse.async = true;
gcse.src = 'http://www.google.com/cse/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(gcse, s);
getSearch();

$('.gsa-wrapper').on('keyup', 'input', function (e) {
    if (e.keyCode === 13) { getSearch(this.value); }
});

function getSearch(qs) {
    $('.faculty-information').html('');
    if (qs == null) {
        var queryString = location.search.substring(1);
        var key = queryString.split('=');
        if (key.length > 1) {
            qs = decodeURI(key[1]);
        }
    }
    qs = qs.trim();

    if (qs != null && qs !== "") {
        $.ajax({
            url: '/api/FacultySearch/SearchData',
            data: { searchTerm: qs.toLowerCase(), departments: "", centers: "", page: 1, size: 5 },
            type: 'GET',
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
            },
            success: function (data) {
                $('.faculty-information').empty();
                if (data.Faculty.length > 0) {
                    var html = '<div class="picture"><img alt="${Name}" src="${Image}" height="73" width="61"/><div class="text"><a href="${Url}">${Name}</a></div></div>';
                    $(".faculty-information").append('<div class="title">Interested in faculty related to <i>' + qs + '</i>?</div>');
                    $.tmpl(html, data.Faculty.slice(0, 5)).appendTo(".faculty-information");
                    if (data.TotalResults > 5) {
                        $(".faculty-information").append('<div style="clear: both;"><a href="/faculty-finder/?q=' + qs + '">...or see all ' + data.TotalResults + ' results</a></div>');
                    }
                }
            }
        });
        $.ajax({
            url: '/api/facetedsearch/SearchPrograms',
            data: { searchTerm: qs.toLowerCase(), departments: "", formats: "", credentials: "" },
            type: 'GET',
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
            },
            success: function (data) {
                $('.program-information').empty();
                if (data.Programs.length > 0) {
                    var html = '<div class="text"><a href="${Url}">${Title}</a> (${Objectives})</div>';
                    $(".program-information").append('<div class="title">Interested in programs related to <i>' + qs + '</i>?');
                    $.tmpl(html, data.Programs.slice(0, 5)).appendTo(".program-information");
                    if (data.Programs.length > 5) {
                        $(".program-information").append('<div style="clear: both;"><a href="/faceted-search/programs/?q=' + qs + '">...or see all ' + data.Programs.length + ' results</a></div>');
                    }
                }
            }
        });
    }
}