﻿jQuery(function ($) {

    function buildPagination(numItems, perPage) {
        $(".paginationContainer").show();
        $(".paginationContainer").pagination({
            items: numItems,
            itemsOnPage: perPage,
            prevText: '&lsaquo;',
            nextText: '&rsaquo;',
            displayedPages: 3,
            edges: 1,
            cssStyle: "light-theme",
// ReSharper disable once FunctionsUsedBeforeDeclared
            onPageClick: function (pageNumber) { getCourseData(pageNumber, false); }
        });
    }

    function getCourseData(page, rebuildPager) {
        var formatArray = [];
        var departmentsArray = [];
        var subjectArray = [];

        $('.format input[type=checkbox]:checked').each(function () { formatArray.push($(this).val()); });
        $('.department input[type=checkbox]:checked').each(function () { departmentsArray.push($(this).val()); });
        $('.subject input[type=checkbox]:checked').each(function () { subjectArray.push($(this).val()); });

        var size = parseInt($("#pagination-bottom").val());

        $.ajax({
            url: '/api/facetedsearch/SearchCourses',
            data: { searchTerm: $('.input_text').val().toLowerCase(), departments: departmentsArray.join("[-]").toLowerCase(), formats: formatArray.join("[-]").toLowerCase(), rubrics: subjectArray.join("[-]").toLowerCase(), page: page, size: size, getFacet: rebuildPager },
            type: 'GET',
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
            },
            success: function (data) {
                $('.results').empty();
                var html;
                html = "<header class='expand'>";
                html += "<h3><span class='ui-accordion-header-icon ui-icon iconClosed expand-this' role='button'></span><a href='/course/${Rubric}/${CourseNumber}'>${Name}</a></h3>";
                html += "<p class='shortDesc'>{{html ShortDescription}}</p>";
                html += "<p class='hidden'>{{html FullDescription}}<br /><br />";
                html += "<span class='row' style='display: inline;'>";
                html += "<span class='column one-quarter' style='padding: 0px; display: inline;'><strong>Format Available</strong>: </span>";
                html += "<span class='column three-quarter' style='padding: 0px; display: inline;'>${Formats}</span>";
                html += "<span class='column one-quarter' style='padding: 0px; display: inline;'><strong>Terms Offered</strong>: </span>";
                html += "<span class='column three-quarter' style='padding: 0px; display: inline;'>${TermsOffered}</span>";
                html += "<span class='column one-quarter' style='padding: 0px; display: inline;'><strong>Credit Hours</strong>: </span>";
                html += "<span class='column three-quarter' style='padding: 0px; display: inline;'>${CreditHours}</span>";
                html += "<span class='column one-quarter' style='padding: 0px; display: inline;'><strong>Instructors</strong>: </span>";
                html += "<span class='three-quarter' style='padding: 0px; display: inline-block; width: 75%; position: relative;'>{{html Faculty}}</span>";
                html += "</span></p></header>";
                $.tmpl(html, data.Courses).appendTo(".results");

                $(".hidden").hide();
                $(".expand-this").on("click", function () {
                    if ($(this).hasClass("iconClosed")) {
                        $(this).parents(".expand").find(".shortDesc").hide(20);
                        $(this).parents(".expand").find(".hidden").show(20);
                        $(this).removeClass("iconClosed");
                        $(this).addClass("iconOpen");
                    } else {
                        $(this).parents(".expand").find(".shortDesc").show(20);
                        $(this).parents(".expand").find(".hidden").hide(20);
                        $(this).removeClass("iconOpen");
                        $(this).addClass("iconClosed");
                    }
                });

                $('.paginationContainer').show();
                var minSize = (page - 1) * size + 1;
                var maxSize = page * size > data.TotalResults ? data.TotalResults : page * size;
                if (data.TotalResults === 0 && data.SpellingSuggestion.length > 0) {
                    $('.pager').hide();
                    $('.pagination .display').html('Did you mean <a href="#" class="insertSuggestion">' + data.SpellingSuggestion.substring(0, data.SpellingSuggestion.indexOf("(")) + '</a>?');
                }
                else if (data.TotalResults === 0) {
                    $('.pager').hide();
                    $('.pagination .display').html('No results were found with that search criteria');
                }
                else if (data.TotalResults < size) {
                    $('.pager').hide();
                    $('.pagination .display').html('Showing <strong>' + data.TotalResults + '</strong> of all <strong>' + data.TotalResults + ' courses</strong>');
                } else {
                    if (rebuildPager) {
                        buildPagination(data.TotalResults, size);
                    }
                    $('.pagination .display').html('Showing <strong>' + minSize + '-' + maxSize + '</strong> of <strong>' + data.TotalResults + ' courses</strong>');
                }
                if (rebuildPager) {
                    $(".spQtd").hide();
                    for (var i1 = 0; i1 < data.FormatFacet.length; i1++) {
                        var dataArrayFacet = data.FormatFacet[i1].split("-");
                        var label = dataArrayFacet[0].replace(" ", "").toLowerCase();
                        if (dataArrayFacet[0] !== "All") {
                            $(".format-" + label + "-label .spQtd").text("(" + dataArrayFacet[1] + ")");
                            $(".format-" + label + "-label .spQtd").show();
                        }
                    }

                    for (var i2 = 0; i2 < data.DepartmentFacet.length; i2++) {
                        var dataArrayDepartment = data.DepartmentFacet[i2].split("-");
                        if (dataArrayDepartment[0] !== "None") {
                            $(".department-" + dataArrayDepartment[0].toLowerCase() + "-label .spQtd").text("(" + dataArrayDepartment[1] + ")");
                            $(".department-" + dataArrayDepartment[0].toLowerCase() + "-label .spQtd").show();
                        }
                    }
                }
            }
        });
    }

    $("#pagination-top").change(function () {
        $('.paginationContainer').pagination('destroy');
        var value = parseInt($(this).val());
        $("#pagination-bottom").val(value);
        getCourseData(1, true);
    });

    $("#pagination-bottom").change(function () {
        $('.paginationContainer').pagination('destroy');
        var value = parseInt($(this).val());
        $("#pagination-top").val(value);
        getCourseData(1, true);
    });

    $('.input_text').keyup(function (event) {
        if (event.keyCode === 13) {
            getCourseData(1, true);
        }
    });

    $(".format input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".format input[type='checkbox'][name!='format-all']").prop("checked", false);
        } else {
            $("#format-all").prop("checked", false);
        }
        getCourseData(1, true);
    });

    $(".subject input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".subject input[type='checkbox'][name!='subject-all']").prop("checked", false);
        } else {
            $("#subject-all").prop("checked", false);
        }
        getCourseData(1, true);
    });

    $(".department input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".department input[type='checkbox'][name!='department-all']").prop("checked", false);
        } else {
            $("#department-all").prop("checked", false);
        }
        getCourseData(1, true);
    });

    $(".expandall").click(function () {
        $(".expand-this").each(function () {
            if ($(this).hasClass("iconClosed")) {
                $(this).parents(".expand").find(".shortDesc").hide();
                $(this).parents(".expand").find(".hidden").show();
                $(this).removeClass("iconClosed");
                $(this).addClass("iconOpen");
            }
        });
    });

    $(".collapseall").click(function () {
        $(".expand-this").each(function () {
            if ($(this).hasClass("iconOpen")) {
                $(this).parents(".expand").find(".shortDesc").show();
                $(this).parents(".expand").find(".hidden").hide();
                $(this).removeClass("iconOpen");
                $(this).addClass("iconClosed");
            }
        });
    });
    
    $(".ui-accordion-header-clickable").click(function () {
        if ($(this).find(".ui-icon").hasClass("iconClosed")) {
            $(this).parent().find(".pnFacetDetails").slideDown(200);
            $(this).find(".ui-icon").removeClass("iconClosed");
            $(this).find(".ui-icon").addClass("iconOpen");
        } else {
            $(this).parent().find(".pnFacetDetails").slideUp(200);
            $(this).find(".ui-icon").removeClass("iconOpen");
            $(this).find(".ui-icon").addClass("iconClosed");
        }
        return false;
    });

    $(".input_text").autocomplete({
        messages: {
            noResults: "",
            results: function () { }
        },
        source: function (request, response) {
            $.ajax({
                url: "/api/FacetedSearch/GetSuggestionsForCourses",
                data: { searchTerm: request.term },
                type: "GET",
                error: function (xhr) {
                    alert(xhr.status);
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        select: function () { getCourseData(1, true); return true; }
    });

    $(".display").on("click", ".insertSuggestion", function (event) {
        event.preventDefault();
        $(".input_text").val($('.insertSuggestion').text().trim());
        getCourseData(1, true);
    });

    $("#faceted-submit").click(function () {
        getCourseData(1, true);
    });

    function get(n) {
        var half = location.search.split(n + '=')[1];
        return half !== undefined ? decodeURIComponent(half.split('&')[0]) : null;
    }

    $(".pnFacetDetails").hide();
    var department = get('department');
    if (department != null) {
        $("#department-all").prop("checked", false);
        var deptArray = department.split('-');
        for (var i = 0; i < deptArray.length; i++) {
            $("#department-" + deptArray[i]).prop("checked", true);
        }
        $(".department").parent().slideDown(200);
        $(".department").parent().parent().find(".ui-icon").removeClass("iconClosed");
        $(".department").parent().parent().find(".ui-icon").addClass("iconOpen");
    }
    var format = get('format');
    if (format != null) {
        $("#format-all").prop("checked", false);
        var formatArray = format.split('-');
        for (var i = 0; i < formatArray.length; i++) {
            $("#format-" + formatArray[i]).prop("checked", true);
        }
        $(".format").parent().slideDown(200);
        $(".format").parent().parent().find(".ui-icon").removeClass("iconClosed");
        $(".format").parent().parent().find(".ui-icon").addClass("iconOpen");
    }
    var subject = get('subject');
    if (subject != null) {
        $("#subject-all").prop("checked", false);
        var subjectArray = subject.split('-');
        for (var i = 0; i < subjectArray.length; i++) {
            $("#subject-" + subjectArray[i]).prop("checked", true);
        }
        $(".subject").parent().slideDown(200);
        $(".subject").parent().parent().find(".ui-icon").removeClass("iconClosed");
        $(".subject").parent().parent().find(".ui-icon").addClass("iconOpen");
    }

    getCourseData(1, true);
});