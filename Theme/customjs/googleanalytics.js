﻿/**
* Function that tracks a click on an outbound link in Google Analytics.
* This function takes a valid URL string as an argument, and uses that URL string
* as the event label.
* 
* Usage in raw HTML:   <a href="http://www.example.com" onclick="trackOutboundLink('http://www.example.com'); return false;">Check out example.com</a>
* or using jquery:     <a href="http://www.example.com" class="track orange">Check out example.com</a>
* https://support.google.com/analytics/answer/1136920?hl=en
* https://developers.google.com/analytics/devguides/collection/analyticsjs/events
*/
var trackOutboundLink = function(url) {
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
      function () {
          document.location = url;
      }
    });
}

var trackOutboundEvent = function (info) {
    ga('send', 'event', 'button', 'click', info)
}

$('a.track').click(function () { trackOutboundLink(this.href); return false; });
$('.button:contains(Apply)').click(function () { trackOutboundLink(this.href); return false; });
$('.button:contains(Visit)').click(function () { trackOutboundLink(this.href); return false; });