$(window).load(function () {
    var zopimKey = "";

    var trackOutboundEvent = function (info) {
        ga("send", "event", "button", "click", info);
    }

    function gotoChat() {
        if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; }
        if ($.modal == null) {
            alert("The dialog box does not work when in edit mode -- please log out to review full functionality");
        } else {
            trackOutboundEvent("chat from " + window.location.href);
            $.modal("<div class='content modal-dialog-border'><a href='#' class='simplemodal-close modal-dialog-bottom'><img alt-text='close' src='/Theme/img/icons/icon-close-orange.png' /></a><div class='modal-dialog-text'><h2>Talk with us</h2><p>We want to hear from you. What topic did you want to talk about? </p><div class='modal-dialog-buttons'><a class='button orange' href='/chat/undergraduate'>Undergraduate</a><a class='button orange' href='/chat/graduate'>Graduate</a><br/ ><a class='button orange' href='/chat/online'>Online</a><a class='button orange' href='/chat/general'>General</a></div></div>",
                { overlayClose: true, opacity: 80, overlayCss: { backgroundColor: "#000" } });
        }
        return false;
    }

    function gotoApply() {
        if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; }
        if ($.modal == null) {
            alert("The dialog box does not work when in edit mode -- please log out to review full functionality");
        } else {
            trackOutboundEvent("apply from " + window.location.href);
            $.modal("<div class='content modal-dialog-border'><a href='#' class='simplemodal-close modal-dialog-bottom'><img alt-text='close' src='/Theme/img/icons/icon-close-orange.png' /></a><div class='modal-dialog-text'><h2>Apply to the College</h2><p><strong>Congratulations!</strong> You've taken the first step to becoming a student of the College of Education. The application process is handled through the University of Illinois, and is different for undergraduate and graduate studies. </p><div class='modal-dialog-buttons'><a class='button orange' href='http://admissions.illinois.edu/apply'>Undergraduate</a><a class='button orange' href='https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=applicant'>Graduate</a><a class='button orange' href='https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=applicant'>Online</a></div></div>",
                { overlayClose: true, opacity: 80, overlayCss: { backgroundColor: "#000" } });
        }
        return false;
    }

    function gotoRequestInfo() {
        if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; }
        if ($.modal == null) {
            alert("The dialog box does not work when in edit mode -- please log out to review full functionality");
        } else {
            trackOutboundEvent("request information from " + window.location.href);
            $.modal("<div class='content modal-dialog-border'><a href='#' class='simplemodal-close modal-dialog-bottom'><img alt-text='close' src='/Theme/img/icons/icon-close-orange.png' /></a><div class='modal-dialog-text'><h2>Get Information</h2><p>We want to hear from you. Which program did you want information for? </p><div class='modal-dialog-buttons'><a class='button orange' href='mailto:saao@education.illinois.edu'>Undergraduate</a> <a class='button orange' href='https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=coeinterestpage'>Graduate</a><a class='button orange' href='https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=coeinterestpage'>Online</a></div></div>",
                { overlayClose: true, opacity: 80, overlayCss: { backgroundColor: "#000" } });
        }
        return false;
    }

    function gotoRegister() {
        if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; }
        if ($.modal == null) {
            alert("The dialog box does not work when in edit mode -- please log out to review full functionality");
        } else {
            $.modal("<div class='content modal-dialog-border'><a href='#' class='simplemodal-close modal-dialog-bottom'><img alt-text='close' src='/Theme/img/icons/icon-close-orange.png' /></a><div class='modal-dialog-text'><h2>Register for a Class</h2><p>Once you've chosen your courses, register for them online. Review our registration procedures through the <a href='https://registrar.illinois.edu/registration'>Office of the Registrar.</a> </p><div class='modal-dialog-buttons'><a class='button orange' href='https://apps.uillinois.edu/selfservice/'>Undergraduate</a><a class='button orange' href='https://apps.uillinois.edu/selfservice/'>Graduate</a><a class='button orange' href='http://citl.illinois.edu/online-learning/registration/nondegree-students'>Non-Degree Online</a><a class='button orange' href='https://registrar.illinois.edu/concurrent-enrollment'>Concurrent</a></div></div>",
                { overlayClose: true, opacity: 80, overlayCss: { backgroundColor: "#000" } });
        }
        return false;
    }

    function launchChat() {
        trackOutboundEvent("chat from " + window.location.href);
        $zopim.livechat.window.show();
        return false;
    }

    $(".online-program").click(function () { if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "/admissions-academics/online-offcampus"; return false; });

    if (typeof window.pageType == "undefined" || window.pageType == null) {
        zopimKey = "";
    }
    else if (window.pageType === "undergraduate") {
        zopimKey = "2c61qbMWlwPQlxadV86wZPSzkNErMa7A";
        $("#lnk_Apply").click(function () { trackOutboundEvent("undergraduate apply from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/apply"; return false; });
        $(".apply_menu").click(function () { trackOutboundEvent("undergraduate apply menu from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/apply"; return false; });
        $("#lnk_Visit").click(function () { trackOutboundEvent("undergraduate visit from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $(".visit_menu").click(function () { trackOutboundEvent("undergraduate visit menu from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $("#lnk_Request_Information").click(function () { trackOutboundEvent("undergraduate request information from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "mailto:saao@education.illinois.edu"; return false; });
    }
    else if (window.pageType === "graduate") {
        zopimKey = "1z2RzWFu497D56OBVVc7u3oDteIjp3Q4";
        $("#lnk_Apply").click(function () { trackOutboundEvent("graduate apply from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=applicant"; return false; });
        $(".apply_menu").click(function () { trackOutboundEvent("graduate apply menu from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=applicant"; return false; });
        $("#lnk_Visit").click(function () { trackOutboundEvent("graduate visit from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $(".visit_menu").click(function () { trackOutboundEvent("graduate visit menu from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $("#lnk_Request_Information").click(function () { trackOutboundEvent("graduate request information from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=coeinterestpage"; return false; });
    }
    else if (window.pageType === "tech") {
        zopimKey = "29fvDGySmQMA77vN4CkqjNLoreZKYdEK";
        $("#lnk_Apply").click(gotoApply);
        $(".apply_menu").click(gotoApply);
        $("#lnk_Visit").click(function () { trackOutboundEvent("tech visit from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $(".visit_menu").click(function () { trackOutboundEvent("tech visit menu from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $("#lnk_Request_Information").click(function () { trackOutboundEvent("tech request information from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "https://uiucgrad.askadmissions.net/emtinterestpage.aspx?ip=coeinterestpage"; return false; });
    }
    if (zopimKey !== "") {
        window.$zopim || (function (d, s) { var z = $zopim = function (c) { z._.push(c) }, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) { z.set._.push(o) }; z._ = []; z.set._ = []; $.async = !0; $.setAttribute("charset", "utf-8"); $.src = "//v2.zopim.com/?" + zopimKey; z.t = +new Date; $.type = "text/javascript"; e.parentNode.insertBefore($, e) })(document, "script");
        $("#lnk_Chat").click(launchChat);
        $(".chat_menu").click(launchChat);
        $("#lnk_Register").click(gotoRegister);
    }
    else {
        $("#lnk_Chat").click(gotoChat);
        $(".chat_menu").click(gotoChat);
        $("#lnk_Apply").click(gotoApply);
        $(".apply_menu").click(gotoApply);
        $("#lnk_Visit").click(function () { trackOutboundEvent("visit from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $(".visit_menu").click(function () { trackOutboundEvent("visit menu from " + window.location.href); if ($(this).attr("href") !== "#" && $(this).attr("href") !== "") { return true; } window.location.href = "http://admissions.illinois.edu/visit"; return false; });
        $("#lnk_Request_Information").click(gotoRequestInfo);
        $("#lnk_Register").click(gotoRegister);
    }

    $(".blog a").click(function () {
        ga("send", "event", "dean-blog", "click", "from " + window.location.href);
        return true;
    });

    $("#share a").click(function () {
        ga("send", "event", "social-media-connect", "click", "from " + window.location.href + " to " + this.name);
        return true;
    });

    $("#social-media-links a").click(function () {
        ga("send", "event", "social-media-share", "click", "from " + window.location.href + " to " + this.name);
        return true;
    });

    $("a.online-program").click(function () {
        ga("send", "event", "online-link", "click", "from " + window.location.href);
        return true;
    });
});
