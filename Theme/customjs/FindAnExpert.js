﻿
(function ($) {
    $.fn.autoComplete = function (options) {
        var o = $.extend({}, $.fn.autoComplete.defaults, options);

        // public methods
        if (typeof options == 'string') {
            this.each(function () {
                var that = $(this);
                if (options == 'destroy') {
                    $(window).off('resize.autocomplete', that.updateSC);
                    that.off('keydown.autocomplete keyup.autocomplete');
                    if (that.data('autocomplete'))
                        that.attr('autocomplete', that.data('autocomplete'));
                    else
                        that.removeAttr('autocomplete');
                    $(that.data('el')).remove();
                    that.removeData('el').removeData('autocomplete');
                }
            });
            return this;
        }

        return this.each(function () {
            var that = $(this);
            // sc = 'suggestions container'
            that.sc = $('<div class="autocomplete-suggestions"></div>').addClass(o.menuClass);
            that.data('el', that.sc).data('autocomplete', that.attr('autocomplete'));
            that.attr('autocomplete', 'off');
            that.cache = {};
            that.last_val = '';

            that.updateSC = function (resize, next) {
                that.sc.css({
                    top: that.offset().top + that.outerHeight(),
                    left: that.offset().left,
                    width: that.outerWidth()
                });
                if (!resize) {
                    that.sc.show();
                    if (!that.sc.maxHeight) that.sc.maxHeight = parseInt(that.sc.css('max-height'));
                    if (!that.sc.suggestionHeight) that.sc.suggestionHeight = $('.autocomplete-suggestion', that.sc).first().outerHeight();
                    if (that.sc.suggestionHeight)
                        if (!next) that.sc.scrollTop(0);
                        else {
                            var scrTop = that.sc.scrollTop(), selTop = next.offset().top - that.sc.offset().top;
                            if ((selTop + that.sc.suggestionHeight) - that.sc.maxHeight > 0)
                                that.sc.scrollTop((selTop + that.sc.suggestionHeight + scrTop) - that.sc.maxHeight);
                            else if (selTop < 0)
                                that.sc.scrollTop(selTop + scrTop);
                        }
                }
            }
            $(window).on('resize.autocomplete', that.updateSC);

            that.sc.appendTo('body');

            that.sc.on('mouseleave.autocomplete', '.autocomplete-suggestion', function () {
                $('.autocomplete-suggestion.selected').removeClass('selected');
            });

            that.sc.on('mouseenter.autocomplete', '.autocomplete-suggestion', function () {
                $('.autocomplete-suggestion.selected').removeClass('selected');
                $(this).addClass('selected');
            });

            that.sc.on('mousedown.autocomplete', '.autocomplete-suggestion', function () {
                var u = $(this).data('url');
                var v = $(this).data('val');
                that.val(v);
                o.onSelect(v,u);
                setTimeout(function () { that.focus(); }, 10);
                that.sc.hide();
            });

            that.blur(function (e) {
                that.last_val = that.val();
                $('.autocomplete-suggestions').hide();
            });

            function suggest(data) {
                var val = that.val();
                that.cache[val] = data;
                if (data.length && val.length >= o.minChars) {
                    var s = '';
                    for (i = 0; i < data.length; i++) s += o.renderItem(data[i], val);
                    that.sc.html(s);
                    that.updateSC(0);
                }
                else
                    that.sc.hide();
            }

            that.on('keydown.autocomplete', function (e) {
                // down
                if (e.which == 40 && that.sc.html()) {
                    var next, sel = $('.autocomplete-suggestion.selected', that.sc);
                    if (!sel.length) {
                        next = $('.autocomplete-suggestion', that.sc).first();
                        that.val(next.addClass('selected').data('val'));
                    } else {
                        next = sel.next('.autocomplete-suggestion');
                        if (next.length) {
                            sel.removeClass('selected');
                            that.val(next.addClass('selected').data('val'));
                        }
                        else { sel.removeClass('selected'); that.val(that.last_val); next = 0; }
                    }
                    that.updateSC(0, next);
                    return false;
                }
                    // up
                else if (e.which == 38 && that.sc.html()) {
                    var next, sel = $('.autocomplete-suggestion.selected', that.sc);
                    if (!sel.length) {
                        next = $('.autocomplete-suggestion', that.sc).last();
                        that.val(next.addClass('selected').data('val'));
                    } else {
                        var next = sel.prev('.autocomplete-suggestion');
                        if (next.length) {
                            sel.removeClass('selected');
                            that.val(next.addClass('selected').data('val'));
                        }
                        else { sel.removeClass('selected'); that.val(that.last_val); next = 0; }
                    }
                    that.updateSC(0, next);
                    return false;
                }
                    // esc
                else if (e.which == 27) that.val(that.last_val).sc.hide();
            });

            that.on('keyup.autocomplete', function (e) {
                if (!~$.inArray(e.which, [27, 38, 40, 37, 39])) {
                    var val = that.val();
                    if (val.length >= o.minChars) {
                        if (val != that.last_val) {
                            that.last_val = val;
                            clearTimeout(that.timer);
                            if (o.cache) {
                                if (val in that.cache) { suggest(that.cache[val]); return; }
                                // no requests if previous suggestions were empty
                                for (i = 1; i < val.length - o.minChars; i++) {
                                    var part = val.slice(0, val.length - i);
                                    if (part in that.cache && !that.cache[part].length) { suggest([]); return; }
                                }
                            }
                            that.timer = setTimeout(function () { o.source(val, suggest) }, o.delay);
                        }
                    } else {
                        that.last_val = val;
                        that.sc.hide();
                    }
                }
            });
        });
    }

    $.fn.autoComplete.defaults = {
        source: 0,
        minChars: 3,
        delay: 100,
        cache: 1,
        menuClass: '',
        renderItem: function (item, search) {
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            var dep = $('#Dep').val();
            if (dep != undefined) {
                return '<div class="autocomplete-suggestion" data-url="' + item.UnitUrl + '" data-val="' + item.Title + '">' + item.Match.replace(re, "<b>$1</b>") + '</div>';
            } else {
                return '<div class="autocomplete-suggestion" data-url="' + item.Url + '" data-val="' + item.Title + '">' + item.Match.replace(re, "<b>$1</b>") + '</div>';
            }
        },
        onSelect: function (term,value) { }
    };
}(jQuery));


$(function () {
    var start = navigator.userAgent.indexOf("Android ");
    var droidVers = parseFloat(navigator.userAgent.substr(start + 8, 3))


    if (droidVers > 4.1 || navigator.userAgent.indexOf("Android ") === -1) {
        $("#name_kw").autoComplete({
            source: function (term, response) {
                var dep = $('#Dep').val();
                var url = '/api/faculty/search';
                if (dep != undefined)
                    url = '/api/faculty/searchbydepartment';


                $.ajax({
                    url: url,
                    data: { value: term.toLowerCase(), page: 1, size: 500, department: dep },
                    type: 'GET',
                    // beforeSend: function () { $(".ajaxoverlay").show() },
                    // complete: function () { $(".ajaxoverlay").hide() },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        // alert(thrownError);
                    },
                    success: function (data) {
                        var suggestions = [];
                        for (i = 0; i < data.Faculty.length; i++) {
                            suggestions.push(data.Faculty[i]);
                        }
                        //for (i = 0; i < data.Faculty.length; i++) {
                        //    if (data.Faculty[i].Title.indexOf(term) > -1)
                        //        suggestions.push(data.Faculty[i].Title);
                        //    //else if (data.Faculty[i].UnitCategory.indexOf(term) > -1) {
                        //    //    var arr = data.Faculty[i].UnitCategory.split(",");
                        //    //    var item = jQuery.inArray(term, arr);
                        //    //    suggestions.push(values[item]);
                        //    //}
                        //}
                        response(suggestions);
                    }
                });
            },
            onSelect: function (term, value) {
                window.location.href = value;
            }
        });
    }

    $('#find').on('click', function (event) {
        event.preventDefault();
        var dep = $('#Dep').val();
        var url = '/api/faculty/search';
        if (dep != undefined)
            url = '/api/faculty/searchbydepartment';
        var term = $('.input_text').val();

        $.ajax({
            url: url,
            data: { value: term.toLowerCase(), page: 1, size: 500, department: dep },
            type: 'GET',
            // beforeSend: function () { $(".ajaxoverlay").show() },
            // complete: function () { $(".ajaxoverlay").hide() },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                // alert(thrownError);
            },
            success: function (data) {
                var suggestions = [];
                if (data.Faculty.length == 1) {

                    for (i = 0; i < data.Faculty.length; i++) {
                        var dep = $('#Dep').val();
                        if (dep != undefined) {
                            window.location.href =  data.Faculty[i].UnitUrl 
                        } else {
                            window.location.href =  data.Faculty[i].Url
                        }
                    }
                } else {
                    window.location.href = "/faculty-finder?q=" + $('.input_text').val();
                }
          
            }
        });
    });
});