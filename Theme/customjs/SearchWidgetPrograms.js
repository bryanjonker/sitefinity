﻿jQuery(function ($) {

    function getProgramData() {
        var formatArray = [];
        var departmentsArray = [];
        var credentialsArray = [];

        $('.format input[type=checkbox]:checked').each(function () { formatArray.push($(this).val()); });
        $('.department input[type=checkbox]:checked').each(function () { departmentsArray.push($(this).val()); });
        $('.degree input[type=checkbox]:checked').each(function () { credentialsArray.push($(this).val()); });
        $('.credential input[type=checkbox]:checked').each(function () { credentialsArray.push($(this).val()); });

        $.ajax({
            url: '/api/facetedsearch/SearchPrograms',
            data: { searchTerm: $('.input_text').val().toLowerCase(), departments: departmentsArray.join("[-]").toLowerCase(), formats: formatArray.join("[-]").toLowerCase(), credentials: credentialsArray.join("[-]").toLowerCase() },
            type: 'GET',
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
            },
            success: function (data) {
                $('.results').empty();
                var html;
                html = "<h2>${m_Item1}</h2>";
                html += "<p>Offers the following areas of study, degrees &amp; credentials:</p>";
                html += "{{each m_Item2}}";
                html += "<div class='subarea-heading'><div class='subarea-block'>";
                html += "<h3><a href='${Url}'>${Title}</a></h3>";
                html += "<p><span class='degrees'>${Objectives}</span>{{html ShortDescription}}</p></div></div>";
                html += "{{/each}}";

                $.tmpl(html, data.SortedProgramItems).appendTo(".results");

                if (data.TotalResults === 0 && data.SpellingSuggestion.length > 0) {
                    $('.pager').hide();
                    $('.pagination .display').html('Did you mean <a href="#" class="insertSuggestion">' + data.SpellingSuggestion.substring(0, data.SpellingSuggestion.indexOf("(")) + '</a>?');
                }
                else if (data.TotalResults === 0) {
                    $('.pager').hide();
                    $('.pagination .display').html('No results were found with that search criteria');
                }
                $(".spQtd").hide();
                for (var i1 = 0; i1 < data.FormatFacet.length; i1++) {
                    var dataArrayFacet = data.FormatFacet[i1].split("-");
                    var label = dataArrayFacet[0].replace(" ", "").toLowerCase();
                    if (dataArrayFacet[0] !== "All") {
                        $(".format-" + label + "-label .spQtd").text("(" + dataArrayFacet[1] + ")");
                        $(".format-" + label + "-label .spQtd").show();
                    }
                }

                for (var i2 = 0; i2 < data.DepartmentFacet.length; i2++) {
                    var dataArrayDepartment = data.DepartmentFacet[i2].split("-");
                    if (dataArrayDepartment[0] !== "None") {
                        $(".department-" + dataArrayDepartment[0].toLowerCase() + "-label .spQtd").text("(" + dataArrayDepartment[1] + ")");
                        $(".department-" + dataArrayDepartment[0].toLowerCase() + "-label .spQtd").show();
                    }
                }

                for (var i3 = 0; i3 < data.DegreeFacet.length; i3++) {
                    var dataArrayCredential = data.DegreeFacet[i3].split("-");
                    if (dataArrayCredential[0] !== "None") {
                        $(".degree-" + dataArrayCredential[0].toLowerCase() + "-label .spQtd").text("(" + dataArrayCredential[1] + ")");
                        $(".degree-" + dataArrayCredential[0].toLowerCase() + "-label .spQtd").show();
                        $(".credential-" + dataArrayCredential[0].toLowerCase() + "-label .spQtd").text("(" + dataArrayCredential[1] + ")");
                        $(".credential-" + dataArrayCredential[0].toLowerCase() + "-label .spQtd").show();
                    }
                }
            }
        });
    }

    $('.input_text').keyup(function (event) {
        if (event.keyCode === 13) {
            getProgramData();
        }
    });

    $(".format input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".format input[type='checkbox'][name!='format-all']").prop("checked", false);
        } else {
            $("#format-all").prop("checked", false);
        }
        getProgramData();
    });

    $(".department input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".department input[type='checkbox'][name!='department-all']").prop("checked", false);
        } else {
            $("#department-all").prop("checked", false);
        }
        getProgramData();
    });

    $(".degree input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".degree input[type='checkbox'][name!='degree-all']").prop("checked", false);
        } else {
            $("#degree-all").prop("checked", false);
        }
        getProgramData();
    });

    $(".credential input[type='checkbox']").change(function () {
        if ($(this).val() === "") {
            $(".credential input[type='checkbox'][name!='credential-all']").prop("checked", false);
        } else {
            $("#credential-all").prop("checked", false);
        }
        getProgramData();
    });

    $(".ui-accordion-header-clickable").click(function () {
        if ($(this).find(".ui-icon").hasClass("iconClosed")) {
            $(this).parent().find(".pnFacetDetails").slideDown(200);
            $(this).find(".ui-icon").removeClass("iconClosed");
            $(this).find(".ui-icon").addClass("iconOpen");
        } else {
            $(this).parent().find(".pnFacetDetails").slideUp(200);
            $(this).find(".ui-icon").removeClass("iconOpen");
            $(this).find(".ui-icon").addClass("iconClosed");
        }
        return false;
    });

    $(".input_text").autocomplete({
        messages: {
            noResults: "",
            results: function () { }
        },
        source: function (request, response) {
            $.ajax({
                url: "/api/FacetedSearch/GetSuggestionsForPrograms",
                data: { searchTerm: request.term },
                type: "GET",
                error: function (xhr) {
                    alert(xhr.status);
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        select: function () { getProgramData(); return true; }
    });

    $(".display").on("click", ".insertSuggestion", function (event) {
        event.preventDefault();
        $(".input_text").val($('.insertSuggestion').text().trim());
        getProgramData();
    });

    $("#faceted-submit").click(function () {
        getProgramData();
    });

    function get(n) {
        var half = location.search.split(n + '=')[1];
        return half !== undefined ? decodeURIComponent(half.split('&')[0]) : null;
    }

    $(".pnFacetDetails").hide();
    var department = get('department');
    if (department != null) {
        $("#department-all").prop("checked", false);
        var deptArray = department.split('-');
        for (var i = 0; i < deptArray.length; i++) {
            $("#department-" + deptArray[i]).prop("checked", true);
        }
        $(".department").parent().slideDown(200);
        $(".department").parent().parent().find(".ui-icon").removeClass("iconClosed");
        $(".department").parent().parent().find(".ui-icon").addClass("iconOpen");
    }
    var format = get('format');
    if (format != null) {
        $("#format-all").prop("checked", false);
        var formatArray = format.split('-');
        for (var i = 0; i < formatArray.length; i++) {
            $("#format-" + formatArray[i]).prop("checked", true);
        }
        $(".format").parent().show();
        $(".format").parent().find(".ui-icon").removeClass("iconClosed");
        $(".format").parent().find(".ui-icon").addClass("iconOpen");
    }
    var degree = get('degree');
    if (degree != null) {
        $("#degree-all").prop("checked", false);
        var degreeArray = degree.split('-');
        for (var i = 0; i < degreeArray.length; i++) {
            $("#degree-" + degreeArray[i]).prop("checked", true);
        }
        $(".degree").parent().show();
        $(".degree").parent().find(".ui-icon").removeClass("iconClosed");
        $(".degree").parent().find(".ui-icon").addClass("iconOpen");
    }
    var credential = get('credential');
    if (credential != null) {
        $("#credential-all").prop("checked", false);
        var credentialArray = credential.split('-');
        for (var i = 0; i < credentialArray.length; i++) {
            $("#credential-" + credentialArray[i]).prop("checked", true);
        }
        $(".credential").parent().show();
        $(".credential").parent().find(".ui-icon").removeClass("iconClosed");
        $(".credential").parent().find(".ui-icon").addClass("iconOpen");
    }

    var indexOf = window.location.search.indexOf("q=");
    if (indexOf > 0) {
        $('.input_text').val(decodeURI(window.location.search.substring(indexOf + 2)));
    }

    getProgramData();
});