﻿;(function () {
    var paths = {
        'all': 'all'
    };

    define('jquery', [], function () { return window.jQuery; });
    define('jqueryUI', [], function () { return window.jQuery.ui; });

    require.config({
        baseUrl: '/theme/js',
        paths: paths,
        shim: {
            'jqueryUI': {
                exports: "$",
                deps: ['jquery']
            },
            'jquery': {
                exports: '$'
            }
        }
    });
})();