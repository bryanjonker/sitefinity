$(function() {
	$('body').removeClass('no-jquery');

	$('.microsite .search-button').addClass('blue');
	$('.reveal-menu').click(function() {

		var button = $(this);

		var easeOpen = 'easeInQuad';
		var easeClose = 'easeOutCirc';

		var microsite = $('#masthead').hasClass('microsite');


		if ( button.hasClass('hide') ) {
			
			button.removeClass('hide');
			$('#masthead h1.logo').removeClass('logo-white'); // change to the blue logo...
			$('#primary-navigation').animate({ height: [0, easeOpen] }, 500); // hide the menu...
			$('.reveal-menu').animate({ backgroundColor: ['#003e7e', easeOpen] }, 500); // button to blue...
			
			if ( !microsite ) {
				$('#masthead').animate({ backgroundColor: ['#f36b37', easeOpen] }, 500); // masthead to orange...
			} else if ( microsite ) {
				$('#masthead').animate({ backgroundColor: ['#ffffff', easeOpen] }, 500); // masthead to white...
				$('.logo a').animate({ color: ['#003e7e', easeOpen] }, 500); // logo text to blue...
				$('.search-button').addClass('blue'); // search button to blue...
			}

		} else {
			button.addClass('hide');

			// TODO: change body height to window height...
			$('body').height( $(window).height() );

			var navHeight = $(window).height() - $('#primary-masthead').height();
			$('#primary-navigation').animate({ height: [navHeight, easeClose] }, 500); // show the menu...
			$('#masthead').animate({ backgroundColor: ['#3c3c3c', easeClose] }, 500); // masthead to gray...
			$('.reveal-menu').animate({ backgroundColor: ['#f36b37', easeClose] }, 500); // button to orange...
			$('#masthead h1.logo').addClass('logo-white'); // change to the white logo...

			if ( !microsite ) {
				// $('#masthead').animate({ backgroundColor: ['#f36b37', easeOpen] }, 500); // masthead to orange...
			} else if ( microsite ) {
				$('.logo a').animate({ color: ['#ffffff', easeClose] }, 500); // logo text to white...
				$('.search-button').removeClass('blue'); // search button to white...
			}
		}

	});


	/**
	 * Search box visibility
	 */
	$('.search-button').click(function(){
		var button = $(this);
		if ( button.hasClass('hide') ) {
			button.removeClass('hide');
			$('.search-input').animate({ width: [0, 'easeInCirc'] }, 300).removeClass('open');
		} else {
			button.addClass('hide');
			$('.search-input').animate({ width: [300, 'easeInCirc'] }, 300).addClass('open').focus();
		}
	});

	/**
	 * Scroll to top functionality
	 */
	 $.ajax({
	        url: 'includes/top.html',
	        dataType: 'html',
	        success: function(data) {
				$("a[href='#top']").click(function() {
	  				$("html, body").velocity("scroll", { duration: 500, easing: "easeOutExpo" });
	 				return false;
				});
	        }
	 });


	/**
	 * Control sidebar menu visibility
	 */

	// click anywhere else to hide the open menu...
	$(document).click(function () {
		if( $('.drop-down').css('display') ) {
			$('.drop-down').slideUp( 'slow', function(){} );
		}
	});

	 $(".reveal-sidebar").click(function(e) {
	 	$(this).parent().find('.drop-down').slideToggle();
	 	e.stopPropagation();
	 });


	/**
	 * Clear Input Text
	 */
	 $( ".input_text" ).focus(function() {
	 	if ($( ".input_text" ).val() == "Enter") {
	 		$( ".input_text" ).attr("value", "");
	 	}

	 	if ($( ".input_text" ).val() == "Research, Centers, Outreach...") {
	 		$( ".input_text" ).attr("value", "");
	 	}
	 });


});