require(['jquery', 'jqueryUI'],
    function (jQuery) {

        /// RESIZE FUNCTIONS
        // http://stackoverflow.com/questions/5683377/jquery-resize-event-not-firing
        var $window = $(window);
        var width = $window.width();
        var height = $window.height();

        setInterval(function () {
            if ((width != $window.width()) || (height != $window.height())) {
                width = $window.width();
                height = $window.height();
                setup_carousel();

                if ($(window).width() <= 990 && $('#site-search').hasClass('open')) {
                    $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").hide();
                    $("img.coe-mobile").addClass("hide");
                }
                else {
                    $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").show();
                    $("img.coe-mobile").removeClass("hide");
                }

                search_width();
                mobile_menu();
                sidebar_menu();
                faceted_filters();
                reposition();

                $('section.background-1').css({ 'height': '', 'padding-bottom': '' });
                initscroll();
            }
        }, 100);


        // CHECK IF ANDROID
        //console.log(navigator.userAgent);

        if (navigator.userAgent.match(/Android/)) {
            $(".tabs .social-box, .slider, .faculty .tabs .social-box").addClass("android-calc");
        }

        /**
            * Control menu visibility – Put all Menu Functionality in Here
            */

        $('.microsite .search-btn').addClass('blue');
        
        // MENU
        $('.reveal-menu').click(function () {
            fn_menu();
        });

        $("#primary-navigation").css("overflow-y", "hidden");
        $("#microsite-navigation").css("overflow-y", "hidden");

        function fn_menu() {
            var button = $(".reveal-menu");
            var easeOpen = 'easeInQuad';
            var easeClose = 'easeOutCirc';
            var microsite = $('#masthead').hasClass('microsite');
            var transparent = $('#masthead').hasClass('transparent');

            $('.reveal-menu .menu-img').toggleClass('menu-img-close'); // set menu X or hamburger

            if (button.hasClass('hide')) {
                // hiding the menu...
                $("#primary-navigation").css("overflow-y", "hidden");
                $("#microsite-navigation").css("overflow-y", "hidden");

                // removing the body height changes...
                $('body').css({ height: "auto", overflow: "auto", overflowY: "auto" });

                button.removeClass('hide');
                $('#masthead .logo').removeClass('logo-white'); // change to the blue logo...
                $('#primary-navigation').animate({ height: [0, easeOpen] }, 500); // hide the menu...
                $("#microsite-navigation").removeClass("show", 500); // hide the menu...
                $('.reveal-icon').html("&#9776;"); // set menu hamburger

                if (!microsite) {
                    if (!transparent) {
                        $('#masthead').animate({ backgroundColor: ['#f36b37', easeOpen] }, 0); // masthead to orange...
                    }
                    else {
                        if ($(window).width() <= 700) {
                            $('#masthead').animate({ backgroundColor: ['#f36b37', easeOpen] }, 0); // masthead to orange...
                        }
                        else {
                            $('#masthead').animate({ backgroundColor: ['rgba(243,107,55,0.75)', easeOpen] }, 0); // masthead to orange...
                        }
                    }
                    $('.reveal-menu').animate({ backgroundColor: ['#003e7e', easeOpen], borderColor: ['#003e7e', easeOpen] }, 0); // button to blue... // button border to blue...
                } else if (microsite) {
                    $('.reveal-menu').animate({ backgroundColor: ['#404241', easeOpen], borderColor: ['#404241', easeClose] }, 0); // button to gray... // button border to gray...
                    $('#masthead').animate({ backgroundColor: ['#ffffff', easeOpen] }, 0); // masthead to white...
                    $('.logo a').animate({ color: ['#003e7e', easeOpen] }, 0); // logo text to blue...
                    setTimeout(function () { $('.search-btn').addClass('blue'); }, 0); // search button to blue...
                }
                button.focus();
            } else {
                // showing the menu...
                $("#primary-navigation").css("overflow-y", "scroll");
                $("#microsite-navigation").css("overflow-y", "scroll");

                var microsite = $('#masthead').hasClass('microsite');
                button.addClass('hide');

                // change body height to window height...
                if (!microsite) {
                    console.log("not microsite");
                    $('body').css({ height: $(window).height() + 'px', overflow: "hidden", overflowY: "hidden" });
                }

                var navHeight = $(window).height() - $('#masthead').outerHeight();
                $('#primary-navigation').animate({ height: [navHeight, easeClose] }, 500).css({ overflowY: "scroll" }); // show the menu & scrollbar...
                $("#microsite-navigation").addClass("show", 500); // show the menu...

                if (!microsite) {
                    $('#masthead').animate({ backgroundColor: ['#323534', easeClose] }, 0); // masthead to gray...
                    $('.reveal-menu').animate({ backgroundColor: ['#f36b37', easeClose], borderColor: ['#f36b37', easeClose] }, 0); // button to orange... // button border to orange...
                    $('#masthead .logo').addClass('logo-white'); // change to the white logo...
                } else if (microsite) {
                    $('#masthead').animate({ backgroundColor: ['#ffffff', easeClose] }, 0); // masthead to white...
                }
                $('.sitemap a').first().focus();
            }
        }

        // FAE KEYBOARD NAVIGABLE MENU
        var lastFocusedControl = null;
        window.focus();
        $("body").keydown(function (e) {
            var avail;
            // TAB to enter main menu 
            if (e.keyCode === 9 && !e.shiftKey && !$(".reveal-menu").hasClass('hide') && ($(document.activeElement).is(".search-inp") || $(document.activeElement).is(".lastitem"))) {
                fn_menu();
                $('.sitemap a').first().focus();
                e.preventDefault();
                $(".sitemap a :focus").css('background-color: red');
            // Shift-TAB to enter main menu 
            } else if (e.keyCode === 9 && e.shiftKey && !$(".reveal-menu").hasClass('hide') && $(document.activeElement).is($(".breadcrumbs_icon").next().find('a').first())) {
                fn_menu();
                $('.sitemap a').first().focus();
                e.preventDefault();
                $(".sitemap a :focus").css('background-color: red');
            // TAB to exit menu
            } else if (e.keyCode === 9 && $(".reveal-menu").hasClass('hide') && $(document.activeElement).is(".lastitem")) {
                fn_menu();
            // ESC key to exit menu
            } else if (e.keyCode === 27 && $(".reveal-menu").hasClass('hide')) {
                debugger;
                fn_menu();
                if (lastFocusedControl !== null && typeof lastFocusedControl !== "undefined") {
                    lastFocusedControl.focus();
                } else {
                    $(".breadcrumbs_icon").next().find('a').first().focus();
                }
            // Right arrow key
            } else if (e.keyCode === 39 && $(".reveal-menu").hasClass('hide')) {
                avail = $(document.activeElement).closest('.column').next().find('a').first();
                if (typeof avail === "undefined" || "" === avail.text()) {
                    avail = $(document.activeElement).closest('.row').next().find('a').first();
                    if (typeof avail === "undefined" || "" === avail.text() && $(document.activeElement).closest('div').is('.social-media-links')) {
                        fn_menu();
                        avail = $('.ss-icon').first();
                    } else if (typeof avail === "undefined" || "" === avail.text() && $(document.activeElement).is('.apply_menu')) {
                        avail = $('.news-events').first();
                    } else {
                        avail = $('.apply_menu').first();
                    }
                }
                avail.focus();
            // Left arrow key
            } else if (e.keyCode === 37 && $(".reveal-menu").hasClass('hide')) {
                avail = $(document.activeElement).closest('.column').prev().find('a').first();
                if (typeof avail === "undefined" || "" === avail.text()) {
                    avail = $(document.activeElement).closest('.row').prev().find('a').last().closest('.column').prev().next().find('a').first();
                    if (typeof avail === "undefined") {
                        avail = $('.search-inp');
                        fn_menu();
                    }
                }
                avail.focus();
            //Down arrow key
            } else if (e.keyCode === 40 && $(".reveal-menu").hasClass('hide')) {
                avail = $(document.activeElement).next();
                if (typeof avail === "undefined" || avail.length === 0 || avail.hasClass("expand-menu")) {
                    avail = $(document.activeElement).parent().next().find('a').first();
                }
                avail.focus();
            //Up arrow key -- need to jump to the higher menu if you can't find the link. 
            } else if (e.keyCode === 38 && $(".reveal-menu").hasClass('hide')) {
                avail = $(document.activeElement).parent().prev().find('a').first();
                if (typeof avail === "undefined" || avail.length === 0) {
                    avail = $(document.activeElement).parent().parent().prev().find('a').first();
                }
                if (typeof avail === "undefined" || avail.length === 0) {
                    avail = $(document.activeElement).prev();
                }
                avail.focus();
            }
        });

        /**
            * Control sidebar menu visibility
            */

        // click anywhere else to hide the open menu...
        $(document).click(function () {
            if ($('.sidebar-collapse .drop-down').css('display')) {
                $('.sidebar-collapse .drop-down').slideUp('slow', function () { });
                $('.sidebar-collapse .menu-img-close').addClass('menu-img');
                $('.sidebar-collapse .menu-img').removeClass('menu-img-close');
            }
        });

        $(".reveal-sidebar").click(function (e) {
            if ($('.sidebar-collapse .drop-down').css('display') == "none") {
                $('.sidebar-collapse .menu-img').addClass('menu-img-close');
                $('.sidebar-collapse .menu-img-close').removeClass('menu-img');
                $('.sidebar-collapse .drop-down').slideDown();
                e.stopPropagation();
            }
            else {
                $('.sidebar-collapse .menu-img-close').addClass('menu-img');
                $('.sidebar-collapse .menu-img').removeClass('menu-img-close');
                $('.sidebar-collapse .drop-down').slideUp();
                e.stopPropagation();
            }
        });


        $('#site-search').keydown(function (e) {
            if (e.keyCode == 13) {
                search_visibility();
                e.preventDefault();
            }
        });


        $('#magnify').on('click', function (e) {
            if ($('.input_text').val() != '') {
                window.location.href = "/research/search?rq=" + $('.input_text').val();
            }
            e.preventDefault();
        });

        function populate_searchbar() {
            if (window.location.href.indexOf('/search?q=') > 0) {
                $('#site-search').val(decodeURI(window.location.href.substr(window.location.href.indexOf('/search?q=') + 10)));
                search_visibility();
            }
        }

        /**
            * Search box visibility
            */

        $('.search-btn').click(function () {
            if (navigator.userAgent.match(/Android/)) {
                search_visibility_android();
            }
            else {
                search_visibility();
            }
        });

        $('.search-btn').focus(function () {
            if (navigator.userAgent.match(/Android/)) {
                search_visibility_android();
            }
            else {
                search_visibility();
            }
        });

        // functions to run on document load
        search_width();
        mobile_menu();
        sidebar_menu();
        faceted_filters();
        reposition();

        function search_visibility() {
            var button = $('.search-btn');
            if (button.hasClass('hide')) {
                if ($('#site-search').val() != '') {
                    window.location.href = "/search?q=" + $('#site-search').val();
                }
            } else {
                var w = 0;

                if ($(window).width() <= 700) {
                    w = $(window).innerWidth();
                    w -= 105;
                    $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").hide();
                    $("img.coe-mobile").addClass("hide");
                }
                else if ($(window).width() <= 990) {
                    w = $(window).innerWidth();
                    w -= 228;
                    $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").hide();
                    $("img.coe-mobile").addClass("hide");
                }
                else { w = 225; }

                $('.search-inp');
                button.addClass('hide');
                $('.search-btn').addClass('orange');
                $('.search-inp').css("width", 0);

                $('.search-inp').animate({
                    opacity: 1,
                    width: w
                }, 'slow').addClass('open').focus();
            }
        }

        function search_visibility_android() {
            var button = $('.search-btn');
            if (button.hasClass('hide')) {
                if ($('#site-search').val() != '') {
                    window.location.href = "/search?q=" + $('#site-search').val();
                }
            } else {
                var w = 0;

                if ($(window).width() <= 700) {
                    w = $(window).width();
                    w -= 105;
                    $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").hide();
                    $("img.coe-mobile").addClass("hide");
                }
                else if ($(window).width() <= 990) {
                    w = $(window).width();
                    w -= 228;
                    $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").hide();
                    $("img.coe-mobile").addClass("hide");
                }
                else { w = 225; }

                $('.search-inp');
                button.addClass('hide');
                $('.search-btn').addClass('orange');
                $('.search-inp').css("width", 0);
                $('.search-inp').css("opacity", 1);
                $('.search-inp').css("width", w);
                $('.search-inp').addClass("open").focus();

                /*$('.search-inp').animate({
                    opacity: 1,
                    width: w
                },'slow').addClass('open').focus();*/
            }
        }

        // click anywhere to close the menu
        $(document).click(function (e) {
            if ($(e.target).hasClass("search-btn") || $(e.target).hasClass("search-inp")) {
                // clicked search button or input, do nothing
            }
            else {
                if ($('.search-btn').hasClass('hide')) {

                    if (navigator.userAgent.match(/Android/)) {
                        $('.search-inp').css("opacity", 0);
                        $('.search-inp').css("width", 0);
                        $('.search-inp').removeClass('open');
                        $('.search-btn').removeClass('hide');
                        $('.search-btn').removeClass('orange');
                    }
                    else {
                        $('.search-inp').animate({
                            opacity: 0,
                            width: 0
                        }, 'slow', function () {
                            $('.search-inp').removeClass('open');
                            $('.search-btn').removeClass('hide');
                            $('.search-btn').removeClass('orange');
                        });
                    }

                    if ($(window).width() <= 990) {
                        $("#logo-standard img, #logo-microsite img, #masthead.microsite .logo a, img.coe").show();
                        $("img.coe-mobile").removeClass("hide");
                    }
                }
            }
        });

        function search_width() {
            if ($(window).width() <= 700) {
                var w = $(window).width();
                w -= 105;
                $("#site-search").css("width", w, "important");
            }
            else if ($(window).width() <= 990) {
                w = $(window).width();
                w -= 228;
                $("#site-search").css("width", w, "important");
            }
            else {
                $("#site-search").css("width", 225);
            }
        }

        /**
            * Responsive Menu
            * move area of menu: http://stackoverflow.com/questions/7238571/moving-a-span-element-from-one-div-to-another-with-jquery
            */

        function mobile_menu() {
            if ($(window).width() <= 700) {
                $("#primary-navigation .audience-box ul.one-half").addClass("toMove");
                $("#primary-navigation .audience-box ul.one-half").addClass("one-quarter");
                $("#primary-navigation .audience-box ul.one-quarter").removeClass("one-half");
                $infoFor = $("#primary-navigation .audience-box ul.one-quarter").clone();
                $("#primary-navigation .audience-box ul.one-quarter").remove();
                $("#primary-navigation .sitemap").append($infoFor);

                var expand_fallback = '/theme/img/icons/icon-expand.png';
                var expand = '<img src="/theme/img/icons/icon-expand.svg" onerror="this.src=' + expand_fallback + ';" alt="Expand" height="12px" width="12px">';

                var collapse_fallback = '/theme/img/icons/icon-collapse.png';
                var collapse = '<img src="/theme/img/icons/icon-collapse.svg" onerror="this.src=' + collapse_fallback + ';" alt="Collapse" height="12px" width="12px">';

                $("#primary-navigation .sitemap .one-quarter h2, #primary-navigation .menu-section").siblings(".expand").hide();
                $("#primary-navigation .sitemap .one-quarter h2 .expand-menu, #primary-navigation .menu-section .expand-menu").html(expand);
                $('#primary-navigation .sitemap .one-quarter h2 .expand-menu, #primary-navigation .menu-section .expand-menu').unbind('click');
                $("#primary-navigation .sitemap .one-quarter h2 .expand-menu, #primary-navigation .menu-section .expand-menu").bind("click", function () {
                    if ($(this).html() == expand) { $(this).html(collapse); }
                    else { $(this).html(expand); }
                    $(this).parent().siblings(".expand").slideToggle("slow");
                });

                var move = $("#primary-navigation .social-media-links").parent("div.row").children("ul.column.one-quarter");
                $("#primary-navigation .social-media-links").insertBefore(move);

                $("#microsite-navigation .sitemap .one-quarter:not(:first-child) h2, #microsite-navigation .sitemap .one-quarter:not(:first-child) .menu-section").siblings().hide();
                $("#microsite-navigation .sitemap .one-quarter:not(:first-child) h2 .expand-menu, #microsite-navigation .sitemap .one-quarter:not(:first-child) .menu-section .expand-menu").html(expand);
                $('#microsite-navigation .sitemap .one-quarter:not(:first-child) h2 .expand-menu, #microsite-navigation .sitemap .one-quarter:not(:first-child) .menu-section .expand-menu').unbind('click');
                $("#microsite-navigation .sitemap .one-quarter:not(:first-child) h2 .expand-menu, #microsite-navigation .sitemap .one-quarter:not(:first-child) .menu-section .expand-menu").bind("click", function () {
                    if ($(this).html() == expand) { $(this).html(collapse); }
                    else { $(this).html(expand); }
                    $(this).parent().siblings(".expand").slideToggle("slow");
                });

            }
            else {
                if ($("#primary-navigation .sitemap ul.one-quarter:last-child").hasClass("toMove")) {
                    $("#primary-navigation .sitemap ul.one-quarter:last-child").addClass("one-half");
                    $("#primary-navigation .sitemap ul.one-half:last-child").removeClass("one-quarter");
                    $("#primary-navigation .sitemap ul.one-half:last-child").removeClass("toMove");
                    $infoFor = $("#primary-navigation .sitemap ul.one-half:last-child").clone();
                    $("#primary-navigation .sitemap ul.one-half:last-child").remove();
                    $("#primary-navigation .audience-bg").append($infoFor);
                    $("#primary-navigation .audience-bg ul.one-half li").show();
                }

                $('#primary-navigation .sitemap .one-quarter h2 .expand-menu, #primary-navigation .menu-section .expand-menu').unbind('click');
                $("#primary-navigation .sitemap .one-quarter h2, #primary-navigation .menu-section, #primary-navigation .audience-box h2, #primary-navigation .audience-box .menu-section").siblings(".expand").show();

                var move = $("#primary-navigation .social-media-links").parent("div.row").children("ul.column.one-quarter");
                $("#primary-navigation .social-media-links").insertAfter(move);

                $('#microsite-navigation .sitemap .one-quarter:not(:first-child) h2 .expand-menu, #microsite-navigation .sitemap .one-quarter:not(:first-child) .menu-section .expand-menu').unbind('click');
                $("#microsite-navigation .sitemap .one-quarter:not(:first-child) h2, #microsite-navigation .sitemap .one-quarter:not(:first-child) .menu-section").siblings().show();

            }
        }

        function sidebar_menu() {
            if ($(window).width() <= 990) {
                if ($(".sidebar").hasClass("sidebar-change")) {
                    // do nothing
                }
                else {
                    $(".sidebar:not(.sidebar-collapse)").addClass("sidebar-change");
                    $(".sidebar-change ul:not(.inner)").wrap("<section class='drop-down'></section>");
                    $(".sidebar-change ul.inner h1, .sidebar-change ul.inner div.sidebar-section").remove();
                    $(".sidebar-change h1, .sidebar-change div.sidebar-section").insertBefore(".sidebar-change ul:not(.inner)");
                    $(".sidebar-change h2, .sidebar-change div.sidebar-title").wrap("<a class='reveal-sidebar'></a>");
                    $(".sidebar-change h2, .sidebar-change div.sidebar-title").prepend("<span class='menu-img'></span>");
                }
                $("span.archives").hide();
                $('.news_listing .column.one-quarter.left.first h6, .news_listing .column.one-quarter.left.first .archives-title, .microsite_content .column.one-quarter.left.first h6, .microsite_content .column.one-quarter.left.first .archives-title').removeClass('open');
            }
            else {
                $(".sidebar-change h1, .sidebar-change div.sidebar-section").unwrap();
                $(".sidebar-change .menu-img, .sidebar-change .menu-img-close").remove();
                $(".sidebar-change section").removeClass("drop-down");
                $(".sidebar-change section").show();
                $(".sidebar-change").removeClass("sidebar-change");
                $("span.archives").show();
                $('.news_listing .column.one-quarter.left.first h6, .news_listing .column.one-quarter.left.first .archives-title, .microsite_content .column.one-quarter.left.first h6, .microsite_content .column.one-quarter.left.first .archives-title').removeClass('open');
            }
        }

        // http://stackoverflow.com/questions/10920355/attaching-click-event-to-a-jquery-object-not-yet-added-to-the-dom
        $('body').on('click', '.reveal-sidebar', function (e) {
            if ($('.sidebar-change a span').hasClass('menu-img')) {
                $('.sidebar-change .drop-down').stop(true, true).slideDown();
                $('.sidebar-change .menu-img').addClass('menu-img-close');
                $('.sidebar-change .menu-img-close').removeClass('menu-img');
                e.stopPropagation();
                e.preventDefault();
            }
            else {
                $('.sidebar-change .drop-down').stop(true, true).slideUp();
                $('.sidebar-change .menu-img-close').addClass('menu-img');
                $('.sidebar-change .menu-img').removeClass('menu-img-close');
                e.stopPropagation();
                e.preventDefault();
            }
        });

        // news listing archives
        $('body').on('click', '.news_listing .column.one-quarter.left.first h6, .news_listing .column.one-quarter.left.first .archives-title, .microsite_content .column.one-quarter.left.first h6, .microsite_content .column.one-quarter.left.first .archives-title', function (e) {
            if ($(window).width() <= 990) {
                if ($('.news_listing .column.one-quarter.left.first h6, .news_listing .column.one-quarter.left.first .archives-title, .microsite_content .column.one-quarter.left.first h6, .microsite_content .column.one-quarter.left.first .archives-title').hasClass('open')) {
                    $('.news_listing span.archives, .microsite_content span.archives').stop(true, true).slideUp();
                    $('.news_listing .column.one-quarter.left.first h6, .news_listing .column.one-quarter.left.first .archives-title, .microsite_content .column.one-quarter.left.first h6, .microsite_content .column.one-quarter.left.first .archives-title').removeClass('open');
                    e.stopPropagation();
                    e.preventDefault();
                }
                else {
                    $('.news_listing span.archives, .microsite_content span.archives').stop(true, true).slideDown();
                    $('.news_listing .column.one-quarter.left.first h6, .news_listing .column.one-quarter.left.first .archives-title, .microsite_content .column.one-quarter.left.first h6, .microsite_content .column.one-quarter.left.first .archives-title').addClass('open');
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
        });

        function faceted_filters() {
            var check = $(".faceted .inputs").find("input:checkbox:checked").length;
            $(".faceted .faceted_filters span").text("(" + check + ")");
        }

        $("a.faceted_filters").on("click touchstart", function (e) {
            if ($("a.faceted_filters").hasClass("open")) {
                $(".faceted section.content.column.one-quarter.left.first").addClass("open");
                $("a.faceted_filters").removeClass("open");
                $("a.faceted_filters").html("&lsaquo;&nbsp;Close");
                e.stopPropagation();
                e.preventDefault();
            }
            else {
                var check = $(".faceted .inputs").find("input:checkbox:checked").length;
                $(".faceted section.content.column.one-quarter.left.first").removeClass("open");
                $("a.faceted_filters").addClass("open");
                $("a.faceted_filters").html("Filters <span>(" + check + ")</span>&nbsp;&rsaquo;");
                e.stopPropagation();
                e.preventDefault();
            }
        });


        function reposition() {
            if ($(window).width() <= 990) {
                $('.faculty-finder .one-quarter.left').insertBefore('.faculty-finder .three-quarter.right');
                $('.faculty .one-quarter.left').insertBefore('.faculty .three-quarter.right');
            }
            else {
                $('.faculty-finder .three-quarter.right').insertBefore('.faculty-finder .one-quarter.left');
                $('.faculty .three-quarter.right').insertBefore('.faculty .one-quarter.left');
            }

            if ($(window).width() <= 700) {
                //$('#find_expert label[for=name_kw]').parent().insertAfter('#find_expert select');
                if ($('.research .video .one-third.first:first-child #aria_info').length == 0) {
                    $infoFor = $('.research .video .two-third #aria_info').clone();
                    $($infoFor).insertAfter('.research .video .one-third h1.research, .research .video .one-third .video-title');
                }
            }
            else {
                //$('#find_expert label[for=name_kw]').parent().insertAfter('#find_expert .column.first');
                //$('#find_expert .row:last-child label[for=name_kw]').parent().remove();
                $('.research .video .one-third #aria_info').remove();
            }

            if ($(window).width() > 700) {
                $("div.breadcrumbs_open").remove();
            }

            if ($(window).width() <= 700) {
                $('.general .events a.button.orange').insertAfter('.general .events .last');
                $('.general .news a.button.orange').insertAfter('.general .news .one-third:last-child');
                $('.department_content .events a.button.orange').insertAfter('.department_content .events .last');
                $('.department_content .news a.button.orange').insertAfter('.department_content .news .one-third:last-child');

                $('.microsite_content .events a.button.orange').insertAfter('.microsite_content .events .last');
                $('.microsite_content .news a.button.orange').insertAfter('.microsite_content .news .one-third:last-child');

                var expand_fallback = '/theme/img/icons/icon-expand.png';
                var expand = '<img src="/theme/img/icons/icon-expand.svg" onerror="this.src=' + expand_fallback + ';" alt="Expand" height="14px" width="14px">';
                var collapse_fallback = '/theme/img/icons/icon-collapse.png';
                var collapse = '<img src="/theme/img/icons/icon-collapse.svg" onerror="this.src=' + collapse_fallback + ';" alt="Collapse" height="14px" width="14px">';

                if ($('#tab0').length == 0) {
                    $('ul.tab-links').replaceWith('<ul class="tab-links mobile one"><li class="active"><a href="#tab1">Profile</a></li></ul><ul class="tab-links mobile two"><li><a href="#tab2">Research &amp; Service</a></li></ul><ul class="tab-links mobile three"><li><a href="#tab3">Teaching</a></li></ul>');
                    var new_tab = $('.mobile_toMove').html();
                    $('.mobile_toMove').hide();
                    $('<ul class="tab-links mobile zero"><li class="active"><a href="#tab0">Contact</a></li></ul>').insertBefore('ul.tab-links.one');
                    $('<div id="tab0">' + new_tab + '</div>').insertBefore('div.tab-content div#tab1');
                    $('ul.tab-links li a').prepend('<span class="expand-this" role="button">' + expand + '</span>');
                    $('div.tab-content').hide();
                }

                $('.faculty .tab-links li a').click(function () {
                    var tab = $(this).attr('href');
                    tabs(tab);

                    if (tab == "#tab0") { $("div.tab-content").insertAfter("ul.tab-links.zero"); }
                    else if (tab == "#tab1") { $("div.tab-content").insertAfter("ul.tab-links.one"); }
                    else if (tab == "#tab2") { $("div.tab-content").insertAfter("ul.tab-links.two"); }
                    else if (tab == "#tab3") { $("div.tab-content").insertAfter("ul.tab-links.three"); }
                    $('div.tab-content').show();

                    if ($(this).find('span.expand-this').html() == collapse) {
                        $('div.tab-content').hide();
                        $(this).parent().find('span.expand-this').html(expand);
                        return false;
                    }
                    $(this).find('span.expand-this').html(collapse);
                    $(this).closest('ul.tab-links').siblings().find('li a span.expand-this').html(expand);

                    var top = $(".faculty .tab-links li.active").offset().top;
                    $(window).scrollTop(top);
                });

                $('.research div.bar').insertBefore('.research .research_top');

                if ($('.research .resources .expand-this').length == 0) {
                    $infoFor = $('.research .video .one-third.first').clone();
                    $('.research .video').append($infoFor);
                    $('.research .video .one-third.first:first-child a, .research .video .one-third.first:first-child #aria_info').hide();
                    $('.research .video .one-third.first:first-child a, .research .video .one-third.first:first-child p, .research .video .one-third.first:first-child h1, .research .video .one-third.first:first-child .video-title, .research .video .one-third.first:first-child h2, .research .video .one-third.first:first-child h3, .research .video .one-third.first:first-child h4, .research .video .one-third.first:first-child h5, .research .video .one-third.first:first-child h6, .research .video .one-third.first:first-child ol, .research .video .one-third.first:first-child ul').hide();
                    $('.research .video .one-third.first:first-child h1.research, .research .video .one-third.first:first-child .video-title').show();
                    $('.research .video .one-third.first:last-child h1.research, .research .video .one-third.first:last-child .video-title').hide();

                    $('.research .resources .expand').hide();
                    $('.research .resources p').wrapInner("<span class='table-cell'></span>");
                    $('.research .resources p').prepend('<span class="expand-this" role="button">' + expand + '</span>');

                    $('.research .resources p span.expand-this').click(function () {
                        var button_research = $(this);
                        var parent_research = button_research.parent().parent().parent();
                        var display_research = parent_research.find('.expand').css("display");

                        if (display_research == "none") {
                            $(this).html(collapse);
                        }
                        else { $(this).html(expand); }
                        parent_research.find('.expand').slideToggle();
                    });
                }
            }
            else {
                $('.general .events a.button.orange').insertAfter('.general .events h2, .general .events .updates-title');
                $('.general .news a.button.orange').insertAfter('.general .news h2, .general .news .updates-title');

                $('.department_content .events a.button.orange').insertAfter('.department_content .events h2, .department_content .events .updates-title');
                $('.department_content .news a.button.orange').insertAfter('.department_content .news h2, .department_content .news .updates-title');

                $('.microsite_content .events a.button.orange').insertAfter('.microsite_content .events .updates-title');
                $('.microsite_content .news a.button.orange').insertAfter('.microsite_content .news .updates-title');

                $('ul.tab-links.zero').remove();
                $('div#tab0').remove();
                $('.mobile_toMove').show();
                if (window.location.hash == "#tab0") {
                    $("a[href='#tab1']").parent("li").addClass("active");
                    $("div#tab1").show();
                    location.hash = "";
                }

                $("ul.tab-links li a span.expand-this").remove();
                if ($('ul.tab-links').hasClass('mobile')) {
                    $('ul.tab-links li').unwrap().wrapAll('<ul class="tab-links"></ul>');
                }
                $('div.tab-content').insertAfter("ul.tab-links");
                $('div.tab-content').show();
                tabs_desktop();

                $('.research div.bar').insertAfter('.research .research_top');
                $('.research .video .one-third.first:first-child a, .research .video .one-third.first:first-child #aria_info').show();
                $('.research .video .one-third.first:first-child a, .research .video .one-third.first:first-child p, .research .video .one-third.first:first-child h1, .research .video .one-third.first:first-child .video-title, .research .video .one-third.first:first-child h2, .research .video .one-third.first:first-child h3, .research .video .one-third.first:first-child h4, .research .video .one-third.first:first-child h5, .research .video .one-third.first:first-child h6, .research .video .one-third.first:first-child ol, .research .video .one-third.first:first-child ul').show();
                $('.research .video .one-third.first:last-child').remove();
                $('.research .resources .expand').show();
                $('.research .resources p span.expand-this').remove();
                $(".research .resources p").each(function (index) {
                    $(this).html($(this).text());
                });

                // DESKTOP TABLES
                if ($("table").length > 0) {
                    $(".table_mobile_hdr").remove();
                }
            }
        }


        /**
            * Scroll to top functionality
            */
        $("a[href='#top']").click(function () {
            $("html, body").velocity("scroll", { duration: 500, easing: "easeOutExpo" });
            return false;
        });


        /* PARALLAX */
        /*** New scripts JP ***/

        /* Parallax scroll */

        function initscroll() {
            var $sect = $('section.background');
            var $sect1 = $('section.background-1');
            var $sect2 = $('section.background-2');
            var sect1Height = $sect1.outerHeight();
            var sectHeight = $sect2.outerHeight();
            var imgHeight = sectHeight * 1.5;

            /* Set first slide height based on viewport if entire slide is not visible */

            if ($(window).height() <= sect1Height) {
                if ($(window).height() / $(window).width() < 0.5) {
                    $sect1.css({ 'padding-bottom': '50%' });
                } else {
                    $sect1.css({ 'height': ($(window).height()) + 'px', 'padding-bottom': '0' });
                }

            }

            if ($(window).width() > 700 && (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera))) {

                /* Set scroll start and end values for each background type */

                $sect.attr({
                    'data-bottom-top': 'background-position: 0px ' + (($(window).height() - (imgHeight - sectHeight)) * 1.1) + 'px;',
                    'data-top-bottom': 'background-position: 0px ' + -(sectHeight * .85) + 'px;'
                });

                $sect1.attr({
                    'data-top-top': 'background-position: 0px 0px;',
                    'data-top-bottom': 'background-position: 0px ' + -(sect1Height / 2) + 'px;'
                });

                s.refresh(); /* Update Skrollr */

            } else {
                $sect.css({ 'background-attachment': 'scroll' });
            }

        }

        /* Initialize scroll – Skrollr */
        /* I needed to add the .length qualifier to determine if on homepage – otherwise receiving JS errors */
        if ($("section.background-1").length > 0) {
            if ($(window).width() > 700 && (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera))) {
                var s = skrollr.init({
                    smoothScrolling: false,
                    mobileCheck: function () { return false; }
                });
            }
            initscroll();
        }

        /**
            * Text Transition Overlay: http://stackoverflow.com/questions/4326845/how-can-i-determine-the-direction-of-a-jquery-scroll-event
            */

        var scrollDir = 0;

        var initialScroll = $('html').scrollTop() || $('body').scrollTop(),
            windowHeight = $("section.featured.introduction.background-1").outerHeight();
        var initial_opacity1 = ((windowHeight / 2) - initialScroll) / (windowHeight / 2);
        var initial_opacity2 = 1 - (((windowHeight / 2) - initialScroll) / (windowHeight / 2));

        if (initialScroll <= windowHeight) {
            $(".background-1 .default").css({ "opacity": initial_opacity1 });
            $(".overlay").css({ "opacity": initial_opacity2 });
        }

        $(window).scroll(function () {
            var currentDir = $(this).scrollTop(),
                currentScroll = $('html').scrollTop() || $('body').scrollTop(),
                divHeight = $("section.featured.introduction.background-1").outerHeight(),
                speed = 1500;
            var opacity1 = ((divHeight / 2) - currentScroll) / (divHeight / 2);
            var opacity2 = 1 - (((divHeight / 2) - currentScroll) / (divHeight / 2));

            if (currentScroll <= divHeight) {
                $(".background-1 .default").css({ "opacity": opacity1 });
                $(".overlay").css({ "opacity": opacity2 });
            }

            scrollDir = currentDir;
        });

        /**
            * Tabs
            */

        $(".tabs .tab-links a").click(function (e) {
            var currentAttrValue = jQuery(this).attr('href');

            // Show/Hide Tabs with Fade
            jQuery('.tabs ' + currentAttrValue).fadeIn(500).siblings().hide();

            // Change/remove current tab to active
            jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });

        $(".tabs .tab-links.mobile.three a").click(function (e) {
            if ($(".tab-content").css("display") == "none" && $("window").width() <= 700) {
                $(".tab-links.mobile.three li").removeClass("active");
            }

            e.preventDefault();
        });

        function tabs(h) {
            jQuery('.tabs ' + h).fadeIn(500).siblings().hide();
            $('.tab-links li a').parent().removeClass('active');
            $('.tab-links li a[href=' + h + ']').parent().addClass('active');
        }

        function tabs_desktop() {
            $(".tabs .tab-links li").click(function (e) {
                var currentAttrValue = jQuery(this).attr('href');
                jQuery('.tabs ' + currentAttrValue).fadeIn(500).siblings().hide();
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
                e.preventDefault();
            });
        }

        /**
            * ± Expand Faculty
            */
        $("#expand1").click(function () {
            var t1 = $("#expand1").text();
            if (t1 == "+") { $("#expand1").text("-"); }
            else { $("#expand1").text("+"); }
            $("#data1").slideToggle();
        });
        $("#expand2").click(function () {
            var t2 = $("#expand2").text();
            if (t2 == "+") { $("#expand2").text("-"); }
            else { $("#expand2").text("+"); }
            $("#data2").slideToggle();
        });
        $("#expand3").click(function () {
            var t3 = $("#expand3").text();
            if (t3 == "+") { $("#expand3").text("-"); }
            else { $("#expand3").text("+"); }
            $("#data3").slideToggle();
        });

        /**
            * Refresh faculty ajax call
            */
            
        $('.faculty-refresh').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: "/mvcroute/import/faculty?id=" + window.location.href.substring(window.location.href.lastIndexOf("/") + 1),
                type: "GET",
                dataType: "text",
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("There was a problem refreshing the data -- please try back in a few minutes (" + thrownError + ")");
                    window.location.reload(true);
                },
                success: function (msg) {
                    if (msg.indexOf("Success") == -1) {
                        alert("There was a problem refreshing the data -- please try back in a few minutes (" + msg + ")");
                    }
                    window.location.reload(true);
                }
            });
            $(".faculty-refresh-popup").css("opacity", 0.9).fadeIn(300);
            var information = "Refreshing data...";
            $(".faculty-refresh-message").html(information);
            $(".faculty-refresh-message").fadeIn(1000);
            setInterval(function () {
                information = (information !== "Refreshing data...") ? "Refreshing data..." : "Please wait...";
                $(".faculty-refresh-message").html(information);
            }, 3000);
        });

        /**
            * ± Expand Faceted Search
            */

        var exp_fallback = '/theme/img/icons/icon-expand.png';
        var exp = '<img src="/theme/img/icons/icon-expand.svg" onerror="this.src=' + exp_fallback + ';" alt="Expand" height="14px" width="14px">';
        var col_fallback = '/theme/img/icons/icon-collapse.png';
        var col = '<img src="/theme/img/icons/icon-collapse.svg" onerror="this.src=' + col_fallback + ';" alt="Collapse" height="14px" width="14px">';

        $('.expand-this').click(function () {
            if ($("main.research").length == 0) {
                var button = $(this);
                var parent = button.parent();
                var t = button.html();
                var display = parent.find('section').css("display");

                if (display == "none") { button.html(col); }
                else { button.html(exp); }
                parent.find('section').slideToggle();
            }
        });

        /**
            * ± Expand All / Collapse / All
            */

        $('.expand_all').click(function () {
            $('.three-quarter .expand section').slideDown();
            $('.three-quarter .expand-this').html(col);

        });

        $('.collapse_all').click(function () {
            $('.three-quarter .expand section').slideUp();
            $('.three-quarter .expand-this').html(exp);
        });


        /**
            * Open Faculty Finder's Facets on Page Load
            */
        $('.faculty-finder .expand section').css("display", "block");
        $('.faculty-finder .expand .expand-this').html(col);


        /**
            * Breadcrumbs
            */

        // Fix for Android – wanted removed
        /*if (navigator.userAgent.match(/Android/)) {
            $(".breadcrumbs").addClass("breadcrumbs_expand breadcrumbs_expand_height");
            $(".breadcrumbs_icon").hide();
        }*/

        $(".breadcrumbs_icon").click(function () {
            var speed = 400;

            if ($(window).width() <= 700) {
                if ($(".breadcrumbs").hasClass("breadcrumbs_expand")) {
                    /*if (navigator.userAgent.match(/Android/)) { }
                    else {*/ $(".breadcrumbs").removeClass("breadcrumbs_expand_height", speed);
                    $(".breadcrumbs").removeClass("breadcrumbs_expand", 0);
                    /*}*/
                }
                else {
                    /*if (navigator.userAgent.match(/Android/)) { }
                    else {*/
                    $(".breadcrumbs").addClass("breadcrumbs_expand", 0);
                    $(".breadcrumbs").addClass("breadcrumbs_expand_height", speed);
                    /*}*/
                }
            }
            else {
                /*if (navigator.userAgent.match(/Android/)) { }
                else {*/
                if ($(".breadcrumbs").hasClass("breadcrumbs_expand")) {
                    $(".breadcrumbs").removeClass("breadcrumbs_expand");
                    $(".breadcrumbs").removeClass("breadcrumbs_expand_height");
                }
                /*}*/
            }
        });


        /**
            * Research Widget
            */

        $(".research .research_top div.one-quarter p a").hover(
            function () {
                var current = ".research_top svg #circle" + this.id;
                var mcurrent = ".research_top svg #mcircle" + this.id;
                $(current).css("fill", "#B7DFF5");
                $(mcurrent).css("fill", "#B7DFF5");
            }, function () {
                var current = ".research_top svg #circle" + this.id;
                var mcurrent = ".research_top svg #mcircle" + this.id;
                $(current).css("fill", "none");
                $(mcurrent).css("fill", "none");
            });


        /* CAROUSEL */

        // carousel wrapper height
        $(".carousel-wrapper").css("opacity", 0);

        // function to run on document load
        load_fn();

        $.when(load_fn()).done(function () {
            $(".carousel-wrapper").delay(100).animate({ opacity: 1 }, 1000);
        });

        function load_fn() { setup_carousel(); }

        function setup_carousel() {
            $(".carousel-wrapper a.carousel_l").hide();
            var num_cols = 0;
            $(".carousel-wrapper .column").each(function (index) { num_cols++; });

            if ($(window).width() > 990) {
                if (num_cols > 3) { $(".carousel-wrapper a.carousel_r").show(); }
                else { $(".carousel-wrapper a.carousel_r").hide(); }
            }
            else if ($(window).width() <= 990 && $(window).width() > 700) {
                if (num_cols > 2) { $(".carousel-wrapper a.carousel_r").show(); }
                else { $(".carousel-wrapper a.carousel_r").hide(); }
            }
            else {
                if (num_cols > 1) { $(".carousel-wrapper a.carousel_r").show(); }
                else { $(".carousel-wrapper a.carousel_r").hide(); }
            }

            resize_carousel();
            if ($(window).width() > 990) {
                position_columns();
                resize_arrows();
            }
            else if ($(window).width() <= 990 && $(window).width() > 700) {
                t_position_columns();
                resize_arrows();
            }
            else {
                m_position_columns();
                m_resize_arrows();
            }
        }

        function resize_carousel() {
            var carousel_h = 0;
            $(window).load(function () {
                $(".carousel-wrapper .column").each(function (index) {
                    if ($(this).outerHeight() > carousel_h) {
                        carousel_h = $(this).outerHeight();
                    }
                });
                if ($(window).width() > 700) {
                    $(".carousel-wrapper .carousel").height(carousel_h + 16);
                }
                else {
                    $(".carousel-wrapper .carousel").height(carousel_h + 16 + 25);
                }
            });
        }

        function position_columns() {
            var pos_left = 0;
            $(".carousel-wrapper .column").each(function (index) {
                $(this).css("left", pos_left + "%");
                pos_left += (100 / 3);
            });
        }

        function t_position_columns() {
            var pos_left = 0;
            $(".carousel-wrapper .column").each(function (index) {
                $(this).css("left", pos_left + "%");
                pos_left += (100 / 2);
            });
        }

        function m_position_columns() {
            var pos_left = 0;
            $(".carousel-wrapper .column").each(function (index) {
                $(this).css("left", pos_left + "%");
                pos_left += 100;
            });
        }

        function resize_arrows() {
            var img_h = $(".carousel-wrapper .column:first-of-type img").height();
            $(".carousel-wrapper .carousel a.carousel_l, .carousel-wrapper .carousel a.carousel_r").height(img_h);
        }

        function m_resize_arrows() {
            $(".carousel-wrapper .carousel a.carousel_l, .carousel-wrapper .carousel a.carousel_r").height("40px");
        }

        // carousel functionality
        // http://stackoverflow.com/questions/4006822/last-element-in-each-set
        // http://stackoverflow.com/questions/21303986/jquery-allow-click-one-time-only
        setup_carousel_mvt();

        var len = $(".carousel-wrapper .column").length;

        $(".carousel-wrapper a.carousel_l, .carousel-wrapper a.carousel_r").on("click", carousel_mvt);

        function carousel_mvt(e) {
            if ($(".carousel-wrapper .column").is(':animated')) { return false; }

            var dir = $(this).attr("class");
            var increment, parent_width, pos_current = 0;
            if ($(window).width() > 990) { increment = (100 / 3); }
            else if ($(window).width() <= 990 && $(window).width() > 700) { increment = (100 / 2); }
            else { increment = 100; }

            $(".carousel-wrapper .column").each(function (index) {
                parent_width = $(this).parent().width();
                pos_current = parseFloat($(this).css("left"));
                pos_current *= 100;
                pos_current /= parent_width;
                if (dir == "carousel_l") { pos_current += increment; }
                else { pos_current -= increment; }
                $(this).stop().animate({ left: pos_current + "%" }, 500);

                // hide left arrow if at beginning
                if (index == 0) {
                    if (parseInt(pos_current) == 0) {
                        $(".carousel-wrapper a.carousel_l").hide();
                    }
                    else { $(".carousel a.carousel_l").show(); }
                }
                    // hide right arrow if at end
                else if (index == len - 1) {
                    if ($(window).width() > 990) {
                        if (parseInt(pos_current) == 66) {
                            $(".carousel-wrapper a.carousel_r").hide();
                        }
                        else { $(".carousel-wrapper a.carousel_r").show(); }
                    }
                    else if ($(window).width() <= 990 && $(window).width() > 700) {
                        if (parseInt(pos_current) == 50) {
                            $(".carousel-wrapper a.carousel_r").hide();
                        }
                        else { $(".carousel-wrapper a.carousel_r").show(); }
                    }
                    else {
                        if (parseInt(pos_current) == 0) {
                            $(".carousel-wrapper a.carousel_r").hide();
                        }
                        else { $(".carousel-wrapper a.carousel_r").show(); }
                    }
                }
            });
        }

        function setup_carousel_mvt() {
            $(".carousel-wrapper a.carousel_l").hide();
            $(".carousel-wrapper a.carousel_r").show();
        }


        // RESEARCH & OUTREACH JS-VIDEO
        $(".research .vjs-big-play-button, .research .vjs-poster, .research .iframe-container").on("click", function () {
            $(".research .bg-img").css("background-image", "none");
        });


        // RESEARCH & OUTREACH NON-JS VIDEO
        $(".research .video-container iframe, .research .video-container embed, .research .video-container object").hide();

        $(".research .video-container").on("click", function () {
            $(".vjs-big-play-button").hide();
            $(".research .video-container iframe, .research .video-container embed, .research .video-container object").show();
        });


        // RESEARCH & OUTREACH — Turn UL with class Resources List into proper formatting
        $('.resources_by_topic_list').first().closest('div').wrapInner('<div class = "resources row" />');

        $('.resources_by_topic_list').each(function () {
            $(this).children('li:gt(0)').wrapAll('<span class="expand" />')
        });

        $('.resources_by_topic_list').each(function () {
            $(this).children('li:eq(0)').wrapInner('<p />')
        });


        // ANDROID FIX FOR FACULTY FINDER
        $(".faculty-finder .find.search-tools .main-form input").on("click", function () {
            $(this).focus();
        });

        // VIDEO-JS VIDEO
        if ($(".vjs-poster").length != 0) {
            $(".vjs-poster").attr("role", "button");
            $(".vjs-poster").attr("aria-label", "play video poster");
        }

        $('.sfAddEventWrp').parent().wrapAll("<p />");
        $('.sfAddEventWrp > span:not(:last-child) > a:last-child').after(document.createTextNode(","));


        videojs.options.flash.swf = "/Theme/video-js.swf";

});
