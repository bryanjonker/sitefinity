require(['jquery', 'jqueryUI'], 
    $(function() {
        $( ".accordion" ).accordion({
            active: false, collapsible: true, heightStyle: "content", icons: { header: "iconClosed", activeHeader: "iconOpen" }  
        });
    })
);