﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.RelatedData;
using System.Text.RegularExpressions; 

namespace SitefinityWebApp.ProgramImport
{
    public class ProgramImportManager : IDisposable
    {
        internal DynamicModuleManager dynamicModuleManager;
        internal Type areaOfStudyType;
        internal Type credentialType;
        internal Type offeringType;
        internal Type areaOfStudyRelationshipType;

        public ProgramImportManager()
        {
            dynamicModuleManager = DynamicModuleManager.GetManager(string.Empty);
            // dynamicModuleManager.Provider.SuppressSecurityChecks = true;
            areaOfStudyType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Programs.AreaOfStudy");
            credentialType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Programs.Credential");
            offeringType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Programs.Offering");
            areaOfStudyRelationshipType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Programs.AreaOfStudyRelationship");
        }

        public string Import(Program program, bool replace = true)
        {
            try
            {
                program.Url = program.Credentials.First().Url;
                var myAreaOfStudy = dynamicModuleManager.GetDataItems(areaOfStudyType).Where(string.Format("Title = \"{0}\"", program.Title))
                    .ToList().FirstOrDefault(x => !x.IsDeleted && x.Status == ContentLifecycleStatus.Master);
                if (myAreaOfStudy != null)
                {
                    if (replace)
                    {
                        this.Delete(myAreaOfStudy);
                        this.CreateNew(program);
                    }
                    else
                    {
                        this.AddNewItem(program.Credentials, myAreaOfStudy);
                    }
                }
                else
                {
                    this.CreateNew(program);
                }

                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        private void Delete(DynamicContent deletedItem)
        {
            var filteredOfferings = dynamicModuleManager.GetDataItems(offeringType).ToArray()
                .Where(x => x.GetRelatedItems("AreaOfStudy").Any() && x.GetRelatedItems("AreaOfStudy").First().Id == deletedItem.Id).ToList();
            foreach (var offering in filteredOfferings)
            {
                dynamicModuleManager.DeleteDataItem(offering.GetRelatedItems("Credential").OfType<DynamicContent>().FirstOrDefault());
                dynamicModuleManager.DeleteDataItem(offering);
            }

            var filteredAreaRelationship = dynamicModuleManager.GetDataItems(areaOfStudyRelationshipType).ToArray()
                .Where(x => x.GetRelatedItems("AreaOfStudy").Any() && x.GetRelatedItems("AreaOfStudy").First().Id == deletedItem.Id).ToList();
            foreach (var areaRelationship in filteredAreaRelationship)
            {
                dynamicModuleManager.DeleteDataItem(areaRelationship);
            }

            dynamicModuleManager.DeleteDataItem(deletedItem);
            dynamicModuleManager.SaveChanges();
        }

        private void CreateNew(Program program)
        {
            var areaOfStudyItem = dynamicModuleManager.CreateDataItem(areaOfStudyType);

            program.ImportBasicItems(areaOfStudyItem);
            areaOfStudyItem.CreateRelation(GetDepartment(program.Department), "Department");
            this.SetGenericInformation(areaOfStudyItem);
            dynamicModuleManager.SaveChanges();
            dynamicModuleManager.Lifecycle.Publish(areaOfStudyItem);
            dynamicModuleManager.SaveChanges();

            var masterItem = dynamicModuleManager.GetDataItems(areaOfStudyType).Where(string.Format("Title = \"{0}\"", program.Title))
                .ToList().FirstOrDefault(x => !x.IsDeleted && x.Status == ContentLifecycleStatus.Master);

            this.AddParent(program.Department.ToString(), masterItem, program.Title);
            this.AddNewItem(program.Credentials, masterItem);
        }

        private void AddParent(string parentName, DynamicContent item, string title)
        {
            var areaOfStudyRelationshipItem = dynamicModuleManager.CreateDataItem(areaOfStudyRelationshipType);

            areaOfStudyRelationshipItem.SetValue("Title", parentName + "-" + title);
            areaOfStudyRelationshipItem.SetString("UrlName", parentName.ToLower() + "-" + Regex.Replace(title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));
            this.SetGenericInformation(areaOfStudyRelationshipItem);
            var parentItems = dynamicModuleManager.GetDataItems(areaOfStudyType).Where(d => d.Status == ContentLifecycleStatus.Master).ToList();
            var parentItem = parentItems.FirstOrDefault(d => d.GetValue("ShortName").ToString() == parentName);
            if (parentItem != null)
            {
                areaOfStudyRelationshipItem.CreateRelation(parentItem, "Parent");

            }
            areaOfStudyRelationshipItem.CreateRelation(item, "Child");
            dynamicModuleManager.SaveChanges();
            dynamicModuleManager.Lifecycle.Publish(areaOfStudyRelationshipItem);
            dynamicModuleManager.SaveChanges();
        }

        private void AddNewItem(IEnumerable<Credential> credentials, DynamicContent item)
        {
            foreach (var credential in credentials)
            {
                if (!dynamicModuleManager.GetDataItems(credentialType).Where(string.Format("Title = \"{0}\"", credential.Title)).ToList().Any(x => !x.IsDeleted))
                {
                    var credentialItem = dynamicModuleManager.CreateDataItem(credentialType);
                    credential.ImportBasicItems(credentialItem);
                    this.SetGenericInformation(credentialItem);
                    dynamicModuleManager.SaveChanges();
                    dynamicModuleManager.Lifecycle.Publish(credentialItem);
                    dynamicModuleManager.SaveChanges();

                    var offeringItem = dynamicModuleManager.CreateDataItem(offeringType);
                    credential.Offering.ImportBasicItems(offeringItem);

                    offeringItem.CreateRelation(item, "AreaOfStudy");
                    offeringItem.CreateRelation(credentialItem, "Credential");

                    this.SetGenericInformation(offeringItem);
                    dynamicModuleManager.SaveChanges();
                    dynamicModuleManager.Lifecycle.Publish(offeringItem);
                    dynamicModuleManager.SaveChanges();
                }
            }
        }

        private void SetGenericInformation(DynamicContent content)
        {
            content.SetValue("Owner", SecurityManager.GetCurrentUserId());
            content.SetValue("PublicationDate", DateTime.Now);
            content.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, "Published");
        }

        private DynamicContent GetDepartment(DepartmentType departmentName)
        {
            DynamicModuleManager departmentManager = DynamicModuleManager.GetManager();
            var departmentType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Unit.Unit");
            return departmentManager.GetDataItems(departmentType).First(d => d.Status == ContentLifecycleStatus.Live && d.GetValue<string>("unit_id") == departmentName.ToString());
        }

        public void Dispose()
        {
            dynamicModuleManager.Provider.SuppressSecurityChecks = false;
            if (this.dynamicModuleManager != null)
            {
                this.dynamicModuleManager.Dispose();
            }
        }
    }
}