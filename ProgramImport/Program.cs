﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Model;

namespace SitefinityWebApp.ProgramImport
{
    public class Program
    {
        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Description { get; set; }

        public DepartmentType Department { get; set; }

        public string Url { get; set; }

        public IEnumerable<Credential> Credentials { get; set; }

        public Program()
        {
            this.Credentials = new List<Credential>();
        }

        public string Title
        {
            get
            {
                return string.Format("{0} - {1}", this.ShortName, this.Name);
            }
        }

        public void ImportBasicItems(IDynamicFieldsContainer container)
        {
            container.SetValue("Title", this.Title);
            container.SetValue("Description", this.Description);
            container.SetValue("Type", "Department");
            container.SetValue("UrlProgram", this.Url);
            container.SetValue("ShortName", this.ShortName);
            container.SetValue("Name", this.Name);
            container.SetString("UrlName", Regex.Replace(this.Title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));
        }
    }
}