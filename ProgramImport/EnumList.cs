﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.ProgramImport
{
    public enum DepartmentType { EPOL, CI, EdPsy, SPED }

    public enum DegreeType { None, BS, MS, EdM, CAS, PhD, EdD, MA }

    public enum CredentialType { None, Grad_Minor, Undergrad_Minor, Certificate, Endorsement }

}