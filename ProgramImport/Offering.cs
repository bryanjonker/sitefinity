﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Telerik.Sitefinity.Model;

namespace SitefinityWebApp.ProgramImport
{
    public class Offering
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public bool IsOnline { get; set; }
        public bool IsOnCampus { get; set; }
        public bool IsOffCampus { get; set; }

        public Credential Credential { get; set; }
        public Program Program { get; set; }

        public void ImportBasicItems(IDynamicFieldsContainer container)
        {
            container.SetValue("Title", this.Title);
            container.SetValue("UrlOffering", this.Url);
            container.SetString("UrlName", Regex.Replace(this.Title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));

            var list = new List<string>();
            if (this.IsOffCampus)
            {
                list.Add("Off_Campus");
            }
            if (this.IsOnCampus)
            {
                list.Add("On_Campus");
            }
            if (this.IsOnline)
            {
                list.Add("Online");
            }
            container.SetValue("Format", list.ToArray());
        }
    }
}