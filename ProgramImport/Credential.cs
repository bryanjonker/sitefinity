﻿using System.Text.RegularExpressions;
using Telerik.Sitefinity.Model;
namespace SitefinityWebApp.ProgramImport
{
    public class Credential
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public string License { get; set; }

        public string Concentration { get; set; }

        public string Url { get; set; }

        public DegreeType Degree { get; set; }

        public CredentialType NonDegree { get; set; }

        public Offering Offering { get; set; }

        public Credential()
        {
            this.Offering = new Offering();
        }

        public string Title
        {
            get
            {
                return string.Format("{0} - {1}", this.Id, this.Name);
            }
        }

        public void ImportBasicItems(IDynamicFieldsContainer container)
        {
            container.SetValue("Title", this.Title);
            container.SetValue("CredentialId", this.Id);
            container.SetValue("Name", this.Name);
            container.SetValue("Concentration", this.Concentration);
            container.SetValue("License", this.License);
            container.SetValue("UrlCredential", this.Url);
            container.SetValue("CredentialType", this.Degree.ToString());
            container.SetValue("CredentialTypeOthers", this.NonDegree.ToString());
            container.SetString("UrlName", Regex.Replace(this.Title.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-"));
        }
    }
}