﻿ <%@ Control Language="C#" %>
  
 <div runat="server" class="sf_cols occrl-featured-item-wrapper occrl-featured-item-wrapper-5cols">
     <div runat="server" class="sf_colsOut sf_5cols_1_20 occrl-1of-5">
         <div runat="server" class="sf_colsIn sf_5col_1in_20 occrl-5col">
         </div>
     </div>
    
     <div runat="server" class="sf_colsOut sf_5cols_2_20 occrl-2of-5">
         <div runat="server" class="sf_colsIn sf_5col_2in_20 occrl-5col">
         </div>
     </div>
  
     <div runat="server" class="sf_colsOut sf_5cols_3_20 occrl-3of-5">
         <div runat="server" class="sf_colsIn sf_5col_3in_20 occrl-5col">
         </div>
     </div>
  
     <div runat="server" class="sf_colsOut sf_5cols_4_20 occrl-4of-5">
         <div runat="server" class="sf_colsIn sf_5col_4in_20 occrl-5col">
         </div>
     </div>
  
     <div runat="server" class="sf_colsOut sf_5cols_5_20 occrl-5of-5">
         <div runat="server" class="sf_colsIn sf_5col_5in_20 occrl-5col">
         </div>
     </div>
 </div>
