﻿ <%@ Control Language="C#" %>
  
 <div runat="server" class="sf_cols occrl-featured-item-wrapper occrl-featured-item-3cols">
     <div runat="server" class="sf_colsOut sf_3cols_1_33 occrl-1of-3">
         <div runat="server" class="sf_colsIn sf_3col_1in_33 occrl-3col">
         </div>
     </div>
    
     <div runat="server" class="sf_colsOut sf_3cols_2_34 occrl-2of-3">
         <div runat="server" class="sf_colsIn sf_3col_2in_34 occrl-3col">
         </div>
     </div>
  
     <div runat="server" class="sf_colsOut sf_3cols_3_33 occrl-3of-3">
         <div runat="server" class="sf_colsIn sf_3col_3in_33 occrl-3col">
         </div>
     </div>
 </div>
