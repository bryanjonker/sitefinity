﻿ <%@ Control Language="C#" %>
  
 <div runat="server" class="sf_cols occrl-featured-item-wrapper occrl-featured-item-4cols">
     <div runat="server" class="sf_colsOut sf_4cols_1_25 occrl-1of-4">
         <div runat="server" class="sf_colsIn sf_4col_1in_25 occrl-4col">
         </div>
     </div>
    
     <div runat="server" class="sf_colsOut sf_4cols_2_25 occrl-2of-4">
         <div runat="server" class="sf_colsIn sf_4col_2in_25 occrl-4col">
         </div>
     </div>
  
     <div runat="server" class="sf_colsOut sf_4cols_3_25 occrl-3of-4">
         <div runat="server" class="sf_colsIn sf_4col_3in_25 occrl-4col">
         </div>
     </div>
  
     <div runat="server" class="sf_colsOut sf_4cols_4_25 occrl-4of-4">
         <div runat="server" class="sf_colsIn sf_4col_4in_25 occrl-4col">
         </div>
     </div>
 </div>
