﻿(function () {
    "use strict";

    var designerModule = angular.module('designer');
    designerModule.requires.push('expander', 'sfSelectors', 'sfFields');
})();