﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using SitefinityWebApp.Mvc.Models.RssFeedWidget;
    using System;
    using System.ComponentModel;
    using System.Web.Mvc;
    using UIUC.Custom.XmlService;

    public class RssFeedWidgetController : Controller
    {
        [Category("Content")]
        [Description("How many articles to display")]
        [DisplayName("ArticleNumber")]
        public int ArticleNumber { get; set; }

        [Category("Content")]
        [Description("Url of feed")]
        [DisplayName("FeedUrl")]
        public string FeedUrl { get; set; }

        [Category("Content")]
        [Description("Footer")]
        [DisplayName("Footer")]
        public string Footer { get; set; }

        [Category("Content")]
        [Description("Header")]
        [DisplayName("Header")]
        public string Header { get; set; }

        [Category("Content")]
        [Description("Node name (default to 'item')")]
        [DisplayName("Node")]
        public string Node { get; set; }

        [Category("Content")]
        [Description("Template (use [] to add variables")]
        [DisplayName("Template")]
        public string Template { get; set; }

        public ActionResult Index()
        {
            if (this.ArticleNumber == 0)
            {
                this.ArticleNumber = 3;
            }
            if (string.IsNullOrWhiteSpace(this.Node))
            {
                this.Node = "item";
            }
            if (string.IsNullOrWhiteSpace(this.Header))
            {
                this.Header = "<ul>";
            }
            if (string.IsNullOrWhiteSpace(this.Footer))
            {
                this.Footer = "</ul>";
            }
            if (string.IsNullOrWhiteSpace(this.Template))
            {
                this.Template = "<li><a href='[link]'>[title]</a> ([date])</li>";
            }
            return this.View(new IndexModel(this.Template, FeedUrlHelper.GetArticles(this.FeedUrl, this.Node, this.ArticleNumber))
            {
                Footer = this.Footer,
                Header = this.Header
            });
        }
    }
}