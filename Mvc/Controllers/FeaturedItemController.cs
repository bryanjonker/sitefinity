namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Web.Mvc;

    using SitefinityWebApp.Mvc.Models.FeaturedItem;

    public class FeaturedItemController : Controller
    {
		
        [DisplayName("Title")]
        public string Title { get; set; }
                
        [DisplayName("Description")]
        public string Description { get; set; }

        
        [DisplayName("Link")]
        public string Link { get; set; }

        [DisplayName("Color")]
        public string Color { get; set; }

     
        // GET: FeaturedItem
        public ActionResult Index()
        {
            return this.View("Index", new IndexModel { Title = this.Title, Description = this.Description, Link = this.Link, Color = this.Color });
        }

        public ActionResult Submit()
        {
            // this.Request.Form["information"]
            return this.Content("Thank you for entering this information");
        }
    }
}