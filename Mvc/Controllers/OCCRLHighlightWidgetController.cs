﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using SitefinityWebApp.Mvc.Models.OCCRLHighlightWidget;
    using System.ComponentModel;
    using System.Web.Mvc;
    using UIUC.Custom.XmlService;

    public class OCCRLHighlightWidgetController : Controller
    {
        [DisplayName("Project")]
        public string Project { get; set; }

        [DisplayName("Layout")]
        public string Layout { get; set; }

        [DisplayName("Image")]
        public string Image { get; set; }

        [DisplayName("Txt")]
        public string Txt { get; set; }

        [DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Link")]
        public string Link { get; set; }

        [DisplayName("ButtonText")]
        public string ButtonText { get; set; }


        // GET: FeaturedItem
        public ActionResult Index()
        {
            return this.View("Index", new IndexModel { Project = this.Project, Layout = this.Layout, Image = this.Image, Txt = this.Txt, Title = this.Title, Description = this.Description, Link = this.Link, ButtonText = this.ButtonText });
        }
    }
}