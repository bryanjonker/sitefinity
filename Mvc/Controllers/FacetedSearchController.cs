﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.Http;

    using SitefinityWebApp.GoogleSearchAppliance.FacetedSearch;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch;

    public class FacetedSearchController : ApiController
    {
        [HttpGet]
        public CourseSearchResult SearchCourses(int page, int size, bool getFacet = true, string searchTerm = "", string departments = "", string terms = "", string formats = "", string rubrics = "")
        {
            var skip = size*(page-1);

            return string.IsNullOrWhiteSpace(searchTerm) ?
                       this.SearchCoursesInternal(skip, size, getFacet, departments, terms, formats, rubrics) :
                       new CourseGsaAccess(ConfigurationManager.AppSettings["googleSearchUrl"]).SearchData(skip, size, searchTerm, formats, departments, rubrics);
        }

        [HttpGet]
        public ProgramSearchResult SearchPrograms(string searchTerm = "", string departments = "", string formats = "", string credentials = "")
        {
            var formatArray = JsSplit(formats);
            var credentialArray = JsSplit(credentials);

            var returnValue = string.IsNullOrWhiteSpace(searchTerm) ?
                       this.SearchProgramsInternal(departments, formatArray, credentialArray) :
                        new ProgramGsaAccess(ConfigurationManager.AppSettings["googleSearchUrl"]).SearchData(searchTerm, formatArray, departments, credentialArray);

            return returnValue;
        }

        [HttpGet]
        public IEnumerable<string> GetSuggestionsForCourses(string searchTerm)
        {
            return new CourseGsaAccess(ConfigurationManager.AppSettings["googleSearchUrl"]).GetSuggestions(searchTerm).Take(10);
        }

        public IEnumerable<string> GetSuggestionsForPrograms(string searchTerm)
        {
            return new ProgramGsaAccess(ConfigurationManager.AppSettings["googleSearchUrl"]).GetSuggestions(searchTerm).Take(10);
        }

        private static string[] JsSplit(string s)
        {
            return string.IsNullOrWhiteSpace(s) ? new string[0] : s.Split(new[] { "[-]" }, StringSplitOptions.RemoveEmptyEntries);
        }

        private CourseSearchResult SearchCoursesInternal(int skip, int size, bool getFacet, string departments, string terms, string formats, string rubrics)
        {
            var export = new ExportProcess();
            var formatArray = JsSplit(formats);
            var termArray = JsSplit(terms);
            var rubricArray = JsSplit(rubrics);
            var departmentArray = JsSplit(departments);
            var courses = export.GetCourses(formatArray, termArray, departmentArray, rubricArray, skip, size);
            var count = export.GetCourseCount(formatArray, termArray, departmentArray, rubricArray);
            var formatCount = getFacet ? export.GetFacetCountByFormat(formatArray, termArray, departmentArray, rubricArray) : new Dictionary<string, int>();
            var departmentCount = getFacet ? export.GetFacetCountByDepartment(formatArray, termArray, departmentArray, rubricArray) : new Dictionary<string, int>();
            var rubricCount = getFacet ? export.GetFacetCountByRubric(formatArray, termArray, departmentArray, rubricArray) : new Dictionary<string, int>();
            return new CourseSearchResult
            {
                Courses=courses.Select(c => new CourseItem(c)),
                TotalResults=count,
                SpellingSuggestion=string.Empty,
                FormatFacet=formatCount.Select(f => f.Key+"-"+f.Value).ToArray(),
                DepartmentFacet=departmentCount.Select(f => f.Key+"-"+f.Value).ToArray(),
                SubjectFacet = rubricCount.Select(f => f.Key + "-" + f.Value).ToArray()
            };
        }

        private ProgramSearchResult SearchProgramsInternal(string departments, string[] formatArray, string[] credentialArray)
        {
            var export = new ExportProcess();
            var departmentArray = JsSplit(departments);
            var programs = export.GetPrograms(formatArray, departmentArray, credentialArray);
            var count = export.GetProgramCount(formatArray, departmentArray, credentialArray);
            var formatCount = export.GetProgramFacetCountByFormat(formatArray, departmentArray, credentialArray);
            var departmentCount = export.GetProgramFacetCountByDepartment(formatArray, departmentArray, credentialArray);
            var degreeCount = export.GetProgramFacetCountByCredential(formatArray, departmentArray, credentialArray);
            return new ProgramSearchResult
            {
                Programs=programs.Select(p => new ProgramItem(p)),
                DegreeFacet=degreeCount.Select(f => f.Key+"-"+f.Value).ToArray(),
                DepartmentFacet=departmentCount.Select(f => f.Key+"-"+f.Value).ToArray(),
                FormatFacet=formatCount.Select(f => f.Key+"-"+f.Value).ToArray(),
                TotalResults=count
            };
        }
    }
}