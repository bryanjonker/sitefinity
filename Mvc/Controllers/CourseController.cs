﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using SitefinityWebApp.Mvc.Models.Course;
    using SitefinityWebApp.SitefinityPlugin;
    using System.Linq;
    using System.Web.Mvc;
    using Telerik.Sitefinity.Web;
    using UIUC.Custom.DAL.DataAccess.FacetedSearch;

    public class CourseController : Controller
    {
        private string rubric;

        public ActionResult Index()
        {
            RouteHelper.SetUrlParametersResolved();
            if (string.IsNullOrEmpty(this.rubric))
            {
                return this.HttpNotFound();
            }
            var array = this.Request.Path.Split('/').Reverse();
            var courseNumber = array.First();
            var sectionNumber = string.Empty;
            if (!courseNumber.All(char.IsNumber) || courseNumber.Length != 3)
            {
                sectionNumber = courseNumber;
                courseNumber = array.Skip(1).First();
            }
            var export = new ExportProcess();
            var course = export.GetSpecificCourse(this.rubric, courseNumber);
            if (course == null)
            {
                return this.HttpNotFound();
            }
            if (!string.IsNullOrEmpty(sectionNumber))
            {
                course.Sections = course.Sections.Where(s => s.SectionNumber == sectionNumber).ToList();
            }
            var model = new IndexModel(course);
            TitleManager.ChangeTitle(model.Name);
            return this.View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            // HandleUnknownAction is used to set up the widget to handle a default path for the page -- we store the path information and feed it to the action.
            this.rubric = actionName;
            this.ActionInvoker.InvokeAction(this.ControllerContext, "Index");
        }
    }
}