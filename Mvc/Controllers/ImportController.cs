﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;

    using Telerik.Sitefinity.Taxonomies;
    using Telerik.Sitefinity.Taxonomies.Model;

    using UIUC.Custom.DAL.DataAccess.Faculty;
    using UIUC.Custom.DAL.DataAccess.GoogleSearch;
    using UIUC.Custom.DAL.Events;
    using UIUC.Custom.XmlService;

    public class ImportController : Controller
    {
        private readonly string imagePath = "/Theme/images/portraits/";

        public ActionResult Courses()
        {
            var import = new UIUC.Custom.DAL.DataAccess.FacetedSearch.ImportProcess();
            var result = import.ImportCourseData();
            var xmlProcess = new UIUC.Custom.DAL.DataAccess.FacetedSearch.XmlProcess(ConfigurationManager.AppSettings["mainSite"], ConfigurationManager.AppSettings["googleSearchFeedNameCourses"]);
            var xmlSubmitter = new XmlSubmitter(ConfigurationManager.AppSettings["googleSearchUrl"] + ":19900/xmlfeed", ConfigurationManager.AppSettings["googleSearchFeedNameCourses"]);
            return this.Content(result + "/" + xmlSubmitter.PostForm(xmlProcess.GenerateXmlFile(), false));
        }

        public ActionResult Events()
        {
            try
            {
                foreach (var item in new EventXmlService().ReadEventXml())
                {
                    EventContentManager.CreateEvent(item);
                }
                return this.Content("Successful");
            }
            catch (Exception e)
            {
                return this.Content(e.ToString());
            }
        }

        public ActionResult OccrlEvents()
        {
            try
            {
                foreach (var item in new EventXmlService().ReadEventXml(null, false, "http://illinois.edu/calendar/eventXML/5334.xml"))
                {
                    item.EventType = "OCCRL";
                    EventContentManager.CreateEvent(item);
                }
                return this.Content("Successful");
            }
            catch (Exception e)
            {
                return this.Content(e.ToString());
            }
        }

        public ActionResult Faculty(string id)
        {
            var mapPath = this.ControllerContext.HttpContext.Server.MapPath(this.imagePath);
            var import = new ImportProcess(this.imagePath, mapPath);
            var result = import.ImportData(id);
            var xmlProcess = new XmlProcess(ConfigurationManager.AppSettings["mainSite"], ConfigurationManager.AppSettings["googleSearchFeedName"]);
            var xmlSubmitter = new XmlSubmitter(ConfigurationManager.AppSettings["googleSearchUrl"] + ":19900/xmlfeed", ConfigurationManager.AppSettings["googleSearchFeedName"]);
            return this.Content(result + "/" + xmlSubmitter.PostForm(xmlProcess.GenerateXmlFile(id), string.IsNullOrWhiteSpace(id)));
        }

        public ActionResult FacultyCategories()
        {
            var fixUrlRegex = new Regex(@"[^\w\-\!\$\'\(\)\=\@\d_,]+", RegexOptions.Compiled);
            var items = new List<string>();
            using (var taxonomyManager = TaxonomyManager.GetManager())
            {
                var facultyTaxonomy = taxonomyManager.GetTaxa<HierarchicalTaxon>().SingleOrDefault(s => s.Name == "faculty-category");
                foreach (var result in new ExportProcess().GetAllFacultyAppointmentsFacultyProfile())
                {
                    var taxon = taxonomyManager.GetTaxa<HierarchicalTaxon>().SingleOrDefault(t => t.Title == result.FullName);
                    if (taxon == null)
                    {
                        var changedName = fixUrlRegex.Replace(result.FullName, "-");
                        items.Add(result.FullName);
                        taxon = taxonomyManager.CreateTaxon<HierarchicalTaxon>();
                        taxon.Title = result.FullName;
                        taxon.Name = changedName;
                        taxon.UrlName = changedName;
                        taxon.Taxonomy = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>().SingleOrDefault(s => s.Name == "Categories");
                        taxon.Parent = facultyTaxonomy;
                        taxonomyManager.SaveChanges();
                    }
                }
            }
            return this.Content($"Names added: <br />{ string.Join("<br />", items)}");
        }

        public ActionResult Program()
        {
            var xmlProcess = new UIUC.Custom.DAL.DataAccess.FacetedSearch.XmlProcess(ConfigurationManager.AppSettings["mainSite"], ConfigurationManager.AppSettings["googleSearchFeedNamePrograms"]);
            var xmlSubmitter = new XmlSubmitter(ConfigurationManager.AppSettings["googleSearchUrl"] + ":19900/xmlfeed", ConfigurationManager.AppSettings["googleSearchFeedNamePrograms"]);
            return this.Content(xmlSubmitter.PostForm(xmlProcess.GenerateProgramXmlFile(), false));
        }
    }
}