﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using SitefinityWebApp.Mvc.Models.Person;
    using System.Web.Mvc;
    using Telerik.Sitefinity.Web;

    public class IllinoisDirectoryController : Controller
    {
        private string netid;

        public ActionResult Index()
        {
            RouteHelper.SetUrlParametersResolved();
            var personManager = new PersonManager();
            var profile = personManager.GetFullProfile(this.netid);
            if (string.IsNullOrWhiteSpace(profile.Username))
            {
                return this.HttpNotFound();
            }
            return this.View("Index", profile);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            // HandleUnknownAction is used to set up the widget to handle a default path for the page -- we store the path information and feed it to the action.
            this.netid = actionName;
            this.ActionInvoker.InvokeAction(this.ControllerContext, "Index");
        }
    }
}