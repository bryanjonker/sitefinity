﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.Http;

    using SitefinityWebApp.GoogleSearchAppliance;

    using UIUC.Custom.DAL.DataAccess.Faculty;

    public class FacultySearchController : ApiController
    {
        [HttpGet]
        public SearchResults SearchData(int page, int size, string searchTerm = "", string departments = "", string centers = "")
        {
            var skip = size * (page - 1);
            return string.IsNullOrWhiteSpace(searchTerm) ?
                this.SearchDataInternal(skip, size, departments, centers) :
                new GsaAccess(ConfigurationManager.AppSettings["googleSearchUrl"]).SearchData(skip, size, searchTerm, departments, centers);
        }

        [HttpGet]
        public IEnumerable<string> GetSuggestions(string searchTerm)
        {
            return new GsaAccess(ConfigurationManager.AppSettings["googleSearchUrl"]).GetSuggestions(searchTerm).Take(10);
        }

        private SearchResults SearchDataInternal(int skip, int size, string departments, string centers)
        {
            var departmentList = string.IsNullOrWhiteSpace(departments) ? new string[0] : departments.Split(new[] { "[-]" }, StringSplitOptions.RemoveEmptyEntries);
            var unitList = string.IsNullOrWhiteSpace(centers) ? new string[0] : centers.Split(new[] { "[-]" }, StringSplitOptions.RemoveEmptyEntries);
            var exportProcess = new ExportProcess();
            var count = exportProcess.CountFacultyAppointments(departmentList, unitList);
            var jobs = exportProcess.SearchFacultyAppointments(departmentList, unitList, size, skip);
            return new SearchResults(jobs, count);
        }
    }
}