﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Web.Mvc;

    using SitefinityWebApp.Designers.Directory;
    using SitefinityWebApp.Mvc.Models.Person;

    using Telerik.Sitefinity.Web;
    using Telerik.Sitefinity.Web.UI.ControlDesign;

    [ControlDesigner(typeof(DirectoryDesigner))]
    public class PersonController : Controller
    {
        [Category("Content")]
        [Description("Insert area information here")]
        [DisplayName("Content")]
        public string Content { get; set; }

        [Category("Content")]
        [Description("Insert the department code here")]
        [DisplayName("DepartmentCode")]
        public string DepartmentCode { get; set; }

        [Category("Content")]
        [Description("Insert the path for the profile")]
        [DisplayName("ProfilePath")]
        public string ProfilePath { get; set; }

        private string Netid { get; set; }
        private string Title { get; set; }

        private IEnumerable<PersonViewModel> TempPersonViewModels { get; set; }

        public ActionResult Index()
        {
            if (this.Request.Url == null)
            {
                return null;
            }
            var personManager = new PersonManager();
            string title;
            return this.View(string.IsNullOrWhiteSpace(this.DepartmentCode) ? "Index" : "IndexSingle", new IndexAlphabeticalModel
            {
                People = personManager.GetPeople(this.DepartmentCode, out title),
                UsedLetters = personManager.GetFirstLetters(),
                CurrentUrl = this.Request.Url.AbsolutePath,
                HtmlContent = this.Content,
                ShowSearchItems = true,
                Title = title
            });
        }

        public new ActionResult Profile()
        {
            RouteHelper.SetUrlParametersResolved();
            if (this.Request.Url == null)
            {
                return null;
            }
            var personManager = new PersonManager();
            var profile = personManager.GetSinglePerson(this.Netid);
            if (profile.IsFaculty)
            {
                this.Response.RedirectPermanent("~/faculty/" + this.Netid);
            }
            else if (!string.IsNullOrWhiteSpace(this.ProfilePath) && !this.Request.Url.AbsolutePath.Contains(this.ProfilePath))
            {
                this.Response.RedirectPermanent(this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.Replace("/" + this.Netid, string.Empty).LastIndexOf(@"/", StringComparison.OrdinalIgnoreCase)) + "/" + this.ProfilePath + "/" + profile.Username);
            }
            return this.View(string.IsNullOrWhiteSpace(profile.Username) ? "ProfileNotFound" : "Profile", profile);
        }

        public ActionResult Department()
        {
            RouteHelper.SetUrlParametersResolved();
            if (this.Request.Url == null)
            {
                return null;
            }
            var personManager = new PersonManager();
            return this.View("IndexAlphabetical", new IndexAlphabeticalModel
            {
                People = this.TempPersonViewModels,
                CurrentLetter = ' ',
                SearchValue = string.Empty,
                CurrentUrl = this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.LastIndexOf(@"/", StringComparison.OrdinalIgnoreCase)),
                UsedLetters = personManager.GetFirstLetters(),
                ShowSearchItems = false,
                Title = this.Title
            });
        }

        public ActionResult Search(string q)
        {
            RouteHelper.SetUrlParametersResolved();
            if (this.Request.Url == null)
            {
                return null;
            }
            var personManager = new PersonManager();
            return this.View("IndexAlphabetical", new IndexAlphabeticalModel
            {
                People = personManager.Search(q),
                CurrentLetter = ' ',
                SearchValue = q,
                CurrentUrl = this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.IndexOf("/search", StringComparison.OrdinalIgnoreCase)),
                UsedLetters = personManager.GetFirstLetters(),
                ShowSearchItems = true
            });
        }

        public ActionResult SingleLetter()
        {
            RouteHelper.SetUrlParametersResolved();
            if (this.Request.Url == null)
            {
                return null;
            }
            var personManager = new PersonManager();
            return this.View("IndexAlphabetical", new IndexAlphabeticalModel
            {
                People = personManager.GetPeopleBySingleLetter(this.Netid.First()),
                CurrentLetter = this.Netid.First(),
                SearchValue = string.Empty,
                CurrentUrl = this.Request.Url.AbsolutePath.Left(this.Request.Url.AbsolutePath.LastIndexOf(@"/", StringComparison.OrdinalIgnoreCase)),
                UsedLetters = personManager.GetFirstLetters(),
                ShowSearchItems = true
            });
        }

        protected override void HandleUnknownAction(string actionName)
        {
            // HandleUnknownAction is used to set up the widget to handle a default path for the page -- we store the path information and feed it to the action.
            this.Netid = actionName;
            if (actionName.Length == 1)
            {
                this.ActionInvoker.InvokeAction(this.ControllerContext, "SingleLetter");
            }
            else
            {
                string title;
                this.TempPersonViewModels = new PersonManager().GetPeopleByDepartment(this.Netid, out title);
                this.Title = title;
                this.ActionInvoker.InvokeAction(
                    this.ControllerContext,
                    this.TempPersonViewModels.Any() ? "Department" : "Profile");
            }
        }
    }
}