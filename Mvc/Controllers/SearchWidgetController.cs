﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using SitefinityWebApp.Mvc.Models.SearchWidget;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch;
    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class SearchWidgetController : Controller
    {
        public ActionResult Courses()
        {
            return this.View("Index", new IndexModel { SearchType = "Courses" });
        }

        [Authorize]
        public ActionResult Edit(EditModel model)
        {
            var export = new ExportProcess();
            var items = export.GetAllPrograms().ToList();
            if (model == null)
            {
                model = new EditModel();
            }
            else
            {
                var selectedItem = !string.IsNullOrWhiteSpace(model?.Title) ? items.Single(i => i.Name == model.Title.Trim()) : new Program();
                model.Title = selectedItem.Name;
                model.Description = selectedItem.Description;
                model.OnlineUrl = selectedItem.OnlineUrl;
                model.Url = selectedItem.Url;
                model.Department = selectedItem.Department;
                model.IsActive = selectedItem.IsActive;
                model.Credentials = selectedItem.Credentials?.ToList() ?? new List<Credential>();
            }
            model.Programs = items.Select(i => new SelectListItem { Text = i.Name, Value = i.Name }).ToList();
            model.Programs.Insert(0, new SelectListItem { Text = " -- Select One or Add New -- ", Value = "" });
            model.Fix();
            return this.View("Edit", model);
        }

        public ActionResult Index()
        {
            return this.View("Index", new IndexModel { SearchType = "Programs" });
        }

        public ActionResult Programs()
        {
            return this.View("Index", new IndexModel { SearchType = "Programs" });
        }

        [Authorize]
        public ActionResult Save(string title, string newTitle, string description, string onlineUrl, string url, string department, bool isActive)
        {
            DepartmentType departmentEnum;
            Enum.TryParse(department, true, out departmentEnum);
            var import = new ImportProcess();
            import.ImportSingleProgram(title, newTitle.Trim(), HttpUtility.HtmlDecode(description), onlineUrl.Trim(), url.Trim(), departmentEnum, isActive);
            return this.Content("Successful");
        }

        [Authorize]
        public ActionResult SaveCredential(string title, string credentialName, bool isOnline, bool isOnCampus, bool isOffCampus)
        {
            CredentialType credentialEnum;
            Enum.TryParse(credentialName, true, out credentialEnum);
            var import = new ImportProcess();
            import.ImportCredential(title, credentialEnum, isOnline, isOnCampus, isOffCampus);

            return this.Content("Successful");
        }
    }
}