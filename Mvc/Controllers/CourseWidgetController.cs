﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System.Web.Mvc;

    using Case.Framework.Sitefinity.Widgets.CourseWidget;

    using SitefinityWebApp.Mvc.Models.CourseWidget;

    using Telerik.Sitefinity.Web.UI.ControlDesign;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch;

    [ControlDesigner(typeof(CourseWidgetDesigner))]
    public class CourseWidgetController : Controller
    {
        public string Courses { get; set; }

        public string Title { get; set; }

        public CourseWidgetController()
        {
            this.Title = "Current & Upcoming Courses";
        }

        public ActionResult Index()
        {
            var model = new IndexModel { Title = this.Title };
            var export = new ExportProcess();
            var courses = this.Courses.Split(';');
            foreach (var courseInfo in courses)
            {
                var courseSection = courseInfo.Split('.');
                var courseArray = courseSection[0].Trim().Split(' ');
                if (courseArray.Length > 1)
                {
                    var course = export.GetSpecificCourse(courseArray[0].Trim(), courseArray[1].Trim());
                    model.AddLines(course, courseArray.Length > 2 ? courseArray[2] : string.Empty, courseSection.Length > 1 ? courseSection[1].Trim() : string.Empty);
                }
            }
            return this.View("Index", model);
        }
    }
}
