﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using SitefinityWebApp.Mvc.Models.OCCRLFeaturedItemWidget;
    using System.ComponentModel;
    using System.Web.Mvc;
    using UIUC.Custom.XmlService;

    public class OCCRLFeaturedItemWidgetController : Controller
    {
        [DisplayName("Project")]
        public string Project { get; set; }

        [DisplayName("Items")]
        public string Items { get; set; }

        [DisplayName("Position")]
        public string Position { get; set; }
        
        [DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Link")]
        public string Link { get; set; }

        [DisplayName("Color")]
        public string Color { get; set; }


        // GET: FeaturedItem
        public ActionResult Index()
        {
            return this.View("Index", new IndexModel { Project = this.Project, Items=this.Items, Position=this.Position, Title = this.Title, Description = this.Description, Link = this.Link, Color = this.Color });
        }
    }
}