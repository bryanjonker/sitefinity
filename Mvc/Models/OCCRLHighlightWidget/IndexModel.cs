﻿namespace SitefinityWebApp.Mvc.Models.OCCRLHighlightWidget
{
    public class IndexModel
    {
        public string Project { get; set; }

        public string Layout { get; set; }

        public string Image { get; set; }

        public string Txt { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public string ButtonText { get; set; }

    }
}