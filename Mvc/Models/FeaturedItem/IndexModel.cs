namespace SitefinityWebApp.Mvc.Models.FeaturedItem
{
    public class IndexModel
    {
        public string Title { get; set; }

        public string Description { get; set; }
		
		public string Link { get; set; }

        public string Color { get; set; }
    }
}