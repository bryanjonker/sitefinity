﻿namespace SitefinityWebApp.Mvc.Models.CourseWidget
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class IndexModel
    {
        private static readonly Dictionary<string, string> TermDictionary = new Dictionary<string, string> { { "Fall", "0003" }, { "Spring", "0006" }, { "Summer", "0008" } };

        public List<Tuple<string, string>> TableLines { get; set; }

        public string Title { get; set; }

        public IndexModel()
        {
            this.TableLines = new List<Tuple<string, string>>();
        }

        public void AddLines(Course course, string sectionCode, string term)
        {
            if (string.IsNullOrWhiteSpace(sectionCode) && string.IsNullOrWhiteSpace(term))
            {
                this.TableLines.Add(new Tuple<string, string>(ConvertTerm(term, course.Rubric, course.CourseNumber, "000", "1"), $"<tr><td colspan='6'><a href='/course/{course.Rubric}/{course.CourseNumber}'>{course.Rubric} {course.CourseNumber} - {course.Title}</a></td></tr>"));
            }
            else if (string.IsNullOrWhiteSpace(sectionCode))
            {
                this.TableLines.Add(new Tuple<string, string>(ConvertTerm(term, course.Rubric, course.CourseNumber, "000", "1"), $"<tr><td colspan='6'>{term}: <a href='/course/{course.Rubric}/{course.CourseNumber}'>{course.Rubric} {course.CourseNumber} - {course.Title}</a></td></tr>"));
            }
            else 
            {
                var section = course?.Sections.OrderByDescending(c => c.BeginDate).FirstOrDefault(c => c.SectionNumber == sectionCode.Trim());
                if (section != null)
                {
                    if (string.IsNullOrWhiteSpace(term))
                    {
                        term = section.Term;
                    }
                    this.TableLines.Add(new Tuple<string, string>(ConvertTerm(term, course.Rubric, course.CourseNumber, section.SectionNumber, "1"), $"<tr style='margin-bottom: 0px; padding: 0px; border-bottom: 0px;'><td colspan='6' style='padding-bottom: 0px;'>{term}: <a href='/course/{course.Rubric}/{course.CourseNumber}/{section.SectionNumber}'>{course.Rubric} {course.CourseNumber} - {course.Title}</a></td></tr>"));
                    this.TableLines.Add(new Tuple<string, string>(ConvertTerm(term, course.Rubric, course.CourseNumber, section.SectionNumber, "2"), $"<tr style='padding-top: 0px; border-top: 0px;'><td>{GetOnlineUrl(section.ScheduleType, section.TermCode, section.Crn, section.SectionNumber)}</td><td>{section.Crn}</td><td>{section.BeginDate.ToShortDateString()}-{section.EndDate.ToShortDateString()}</td><td>{section.Time}</td><td>{section.Days}</td><td>{GetHtmlUrl(section.Username, section.FullName)}</td></tr>"));
                }
            }
        }

        public IEnumerable<string> Lines
        {
            get
            {
                return this.TableLines.OrderBy(s => s.Item1).Select(s => s.Item2);
            }
        }

        private static string GetOnlineUrl(string scheduleType, string termCode, string crn, string sectionNumber)
        {
            return scheduleType.Equals("Online", StringComparison.OrdinalIgnoreCase) ? $"<a href='http://citl.illinois.edu/courses/section/{termCode}/{crn}'>{sectionNumber}</a>" : sectionNumber;
        }

        private static string GetHtmlUrl(string username, string fullName)
        {
            return string.IsNullOrWhiteSpace(username) ? fullName : $"<a href=\"/faculty/{username}\">{fullName}</a>";
        }

        private static string ConvertTerm(string term, string rubric, string course, string section, string additional)
        {
            var termArray = term.Split(' ');
            return termArray[0] + (TermDictionary.ContainsKey(termArray[1]) ? TermDictionary[termArray[1]] : "9999") + (termArray.Length > 2 ? termArray[2] : "0") + rubric + course + section + additional;
        }
    }
}