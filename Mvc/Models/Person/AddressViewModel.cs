﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Mvc.Models.Person
{
    public class AddressViewModel : PersonViewModel
    {
        public string Hours { get; set; }
        public string PrintableHours
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Hours) ? string.Empty: "hours: " + this.Hours;
            }
        }

        public string Room { get; set; }
        public string Building { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string JobType { get; set; }

        public string CityStateZip
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Zip))
                {
                    return string.Empty;
                }
                return string.Format("{0} {1}, {2}", this.City, this.State, this.Zip);
            }
        }
    }
}