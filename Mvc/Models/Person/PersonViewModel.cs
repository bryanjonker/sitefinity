﻿namespace SitefinityWebApp.Mvc.Models.Person
{
    using System.Collections.Generic;

    public class PersonViewModel : IEqualityComparer<PersonViewModel>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public string Email { get; set; }

        public string Hierarchy { get; set; }

        public string Phone { get; set; }

        public string Unit { get; set; }

        public string UnitId { get; set; }

        public string MainUnit { get; set; }

        public string Username { get; set; }

        public string MainUnitId
        {
            get
            {
                return this.Hierarchy.Split('.')[2];
            }
        }

        public string FullName
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.FirstName) ? this.LastName : string.Format("{0}, {1}", this.LastName, this.FirstName);
            }
        }

        public string FormattedPhone
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Phone))
                {
                    return string.Empty;
                }
                return this.Phone.Trim().Length > 9 ? this.Phone : string.Format("217-{0}", this.Phone.Trim());
            }
        }

        public string FormattedPhoneLink
        {
            get
            {
                return this.FormattedPhone.Replace("-", "");
            }
        }

        public string FullTitle
        {
            get
            {
                return this.Unit == this.MainUnit ? this.Title : string.Format("{0}, {1}", this.Title, this.Unit);
            }
        }

        public string UnitOrder
        {
            get
            {
                return this.Unit == this.MainUnit ? string.Empty : this.Unit;
            }
        }

        public bool Equals(PersonViewModel x, PersonViewModel y)
        {
            return x.FirstName == y.FirstName &&
                x.LastName == y.LastName &&
                x.Title == y.Title &&
                x.Unit == y.Unit &&
                x.Phone == y.Phone &&
                x.Email == y.Email;
        }

        public int GetHashCode(PersonViewModel obj)
        {
            int hash = 23;
            hash = hash * 31 + obj.FirstName.GetHashCode();
            hash = hash * 31 + obj.LastName.GetHashCode();
            hash = hash * 31 + obj.Title.GetHashCode();
            hash = hash * 31 + obj.Unit.GetHashCode();
            hash = hash * 31 + obj.Phone.GetHashCode();
            hash = hash * 31 + obj.Email.GetHashCode();
            return hash;
        }
    }
}