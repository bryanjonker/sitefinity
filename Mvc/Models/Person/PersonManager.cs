﻿namespace SitefinityWebApp.Mvc.Models.Person
{
    using System.Collections.Generic;
    using System.Linq;

    using UIUC.Custom.DAL.DataAccess.Faculty;

    public class PersonManager 
    {
        public IEnumerable<PersonViewModel> GetPeopleByDepartment(string department, out string name)
        {
            using (var context = new FacultyContext())
            {
                var chosenUnit = context.Units.FirstOrDefault(x => x.Code == department);
                if (chosenUnit == null)
                {
                        name = string.Empty;
                        return new List<PersonViewModel>();
                }
                name = chosenUnit.Name;
                var hierarchy = "." + chosenUnit.UnitId + ".";
                return context.Profiles.Where(l => l.Appointments.Any(a => a.Hierarchy.Contains(hierarchy))).Select(l => new PersonViewModel
                    {
                        Email = l.Email,
                        FirstName = l.FirstName,
                        LastName = l.LastName,
                        Username = l.Username,
                        
                        Title = l.Appointments.Where(x => x.Hierarchy.Contains(hierarchy)).FirstOrDefault().Title,
                        Hierarchy = l.Appointments.Where(x => x.Hierarchy.Contains(hierarchy)).FirstOrDefault().Hierarchy,
                        Unit = l.Appointments.Where(x => x.Hierarchy.Contains(hierarchy)).FirstOrDefault().Unit,
                        UnitId = l.Appointments.Where(x => x.Hierarchy.Contains(hierarchy)).FirstOrDefault().UnitId,
                        MainUnit = l.Appointments.Where(x => x.Hierarchy.Contains(hierarchy)).FirstOrDefault().Department,
                        Phone = l.Appointments.Where(x => x.Hierarchy.Contains(hierarchy)).FirstOrDefault().Phone
                    }).ToList();
            }
        }

        public IEnumerable<char> GetFirstLetters()
        {
            using (var context = new FacultyContext())
            {
                return context.Profiles.Where(l => l.Appointments.Any()).Select(x => x.LastName).OrderBy(x => x).ToList().Select(s => char.ToUpperInvariant(s.First()));
            }
        }

        public IEnumerable<PersonViewModel> GetPeople(string department, out string name)
        {
            if (!string.IsNullOrWhiteSpace(department))
            {
                return this.GetPeopleByDepartment(department, out name);
            }
            name = string.Empty;
            using (var context = new FacultyContext())
            {
                return context.Profiles.Where(l => l.Appointments.Any()).SelectMany(l => l.Appointments).Select(l => new PersonViewModel
                {
                    Email = l.Profile.Email,
                    FirstName = l.Profile.FirstName,
                    LastName = l.Profile.LastName,
                    Username = l.Profile.Username,
                    Title = l.Title,
                    Hierarchy = l.Hierarchy,
                    Unit = l.Unit,
                    UnitId = l.UnitId,
                    MainUnit = l.Department,
                    Phone = l.Phone
                }).ToList();
            }
        }

        public IEnumerable<PersonViewModel> GetPeopleBySingleLetter(char startingLetter)
        {
            var letter = startingLetter.ToString();
            using (var context = new FacultyContext())
            {
                return context.Profiles.Where(l => l.Appointments.Any() && l.LastName.StartsWith(letter)).Select(l => new PersonViewModel
                {
                    Email = l.Email,
                    FirstName = l.FirstName,
                    LastName = l.LastName,
                    Username = l.Username,
                    Title = l.Appointments.FirstOrDefault(x => x.IsPrimary).Title,
                    Hierarchy = l.Appointments.FirstOrDefault(x => x.IsPrimary).Hierarchy,
                    Unit = l.Appointments.FirstOrDefault(x => x.IsPrimary).Unit,
                    UnitId = l.Appointments.FirstOrDefault(x => x.IsPrimary).UnitId,
                    MainUnit = l.Appointments.FirstOrDefault(x => x.IsPrimary).Department,
                    Phone = l.Appointments.FirstOrDefault(x => x.IsPrimary).Phone
                }).ToList();
            }
        }


        public IEnumerable<PersonViewModel> Search(string searchCriteria)
        {
            using (var context = new FacultyContext())
            {
                return context.Profiles.Where(l => l.Appointments.Any() && (l.LastName.Contains(searchCriteria) || l.FirstName.Contains(searchCriteria) || l.Username.Contains(searchCriteria))).Select(l => new PersonViewModel
                {
                    Email = l.Email,
                    FirstName = l.FirstName,
                    LastName = l.LastName,
                    Username = l.Username,
                    Title = l.Appointments.FirstOrDefault(x => x.IsPrimary).Title,
                    Hierarchy = l.Appointments.FirstOrDefault(x => x.IsPrimary).Hierarchy,
                    Unit = l.Appointments.FirstOrDefault(x => x.IsPrimary).Unit,
                    UnitId = l.Appointments.FirstOrDefault(x => x.IsPrimary).UnitId,
                    MainUnit = l.Appointments.FirstOrDefault(x => x.IsPrimary).Department,
                    Phone = l.Appointments.FirstOrDefault(x => x.IsPrimary).Phone
                }).ToList();
            }
        }

        public ProfileViewModel GetSinglePerson(string netid)
        {
            var exportProcess = new ExportProcess();
            var profile = exportProcess.GetFacultyAppointmentsOnly(netid);
            if (profile == null)
            {
                return new ProfileViewModel();
            }
            return new ProfileViewModel
            {
                Email = profile.Email,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                Username = netid,
                ImageUrl = profile.Image,
                JobInfo = profile.Appointments.Select(j => new AddressViewModel
                {
                    Title = j.Title,
                    Hierarchy = j.Hierarchy,
                    Unit = j.Unit,
                    Phone = j.Phone,
                    Address = j.Address1,
                    City = j.City,
                    State = j.State,
                    Zip = j.Zip,
                    Room = j.Room,
                    Building = j.Building,
                    Hours = j.Hours,
                    JobType = j.JobType
                })
            };
        }

        public FullProfileViewModel GetFullProfile(string netid)
        {
            var exportProcess = new ExportProcess();
            var profile = exportProcess.GetFaculty(netid);

            if (profile == null || string.IsNullOrWhiteSpace(profile.Username))
            {
                return new FullProfileViewModel();
            }
            return new FullProfileViewModel
            {
                Email = profile.Email,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                Username = netid,
                ImageUrl = profile.Image,
                JobInfo = profile.Appointments.Select(j => new AddressViewModel
                {
                    Title = j.Title,
                    Hierarchy = j.Hierarchy,
                    Unit = j.Unit,
                    Phone = j.Phone,
                    Address = j.Address1,
                    City = j.City,
                    State = j.State,
                    Zip = j.Zip,
                    Room = j.Room,
                    Building = j.Building,
                    Hours = j.Hours,
                    JobType = j.JobType
                }),
                Honors = profile.Activities.Select(f => FullProfileViewModel.FormatHonor(f.YearStarted, f.YearEnded, f.Name, f.Organization)),
                EducationalBackground = profile.History.Select(f => FullProfileViewModel.FormatBackground(f.DegreeYear, f.Degree, f.Area)),
                Links = profile.Links.Select(f => FullProfileViewModel.FormatLink(f.Url, f.Name)),
                Publications = profile.Publications.Select(f => FullProfileViewModel.FormatPublication(f.Url, f.Name)),
                Appointments = profile.Backgrounds.Select(f => FullProfileViewModel.FormatAppointment(f.YearStarted, f.YearEnded, f.Title, f.Institution)),
                ProfessionalSummary = profile.Biography,
                ResearchStatement = profile.Research
            };
        }
    }
}