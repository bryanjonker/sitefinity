﻿namespace SitefinityWebApp.Mvc.Models.Person
{
    using System.Collections.Generic;

    public class IndexAlphabeticalModel
    {
        public IndexAlphabeticalModel()
        {
            this.ShowSearchItems = true;
            this.Letters = new[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
        }

        public char CurrentLetter { get; set; }

        public string CurrentUrl { get; set; }

        public string HtmlContent { get; set; }

        public char[] Letters { get; set; }

        public IEnumerable<PersonViewModel> People { get; set; }

        public string SearchValue { get; set; }

        public bool ShowSearchItems { get; set; }

        public string Title { get; set; }

        public IEnumerable<char> UsedLetters { get; set; }
    }
}