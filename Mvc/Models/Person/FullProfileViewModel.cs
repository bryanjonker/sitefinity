﻿namespace SitefinityWebApp.Mvc.Models.Person
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class FullProfileViewModel : ProfileViewModel
    {
        public string ProfessionalSummary { get; set; }
        public IEnumerable<string> EducationalBackground { get; set; }
        public IEnumerable<string> Appointments { get; set; }
        public IEnumerable<string> Honors { get; set; }
        public string ResearchStatement { get; set; }
        public IEnumerable<Tuple<string, string>> Publications { get; set; }
        public IEnumerable<Tuple<string, string>> Links { get; set; }

        public bool ShowProfessionalSummary
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.ProfessionalSummary);
            }
        }

        public bool ShowResearchStatement
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.ResearchStatement);
            }
        }

        public bool ShowEducationalBackground
        {
            get
            {
                return this.EducationalBackground != null && this.EducationalBackground.Any();
            }
        }

        public bool ShowAppointments
        {
            get
            {
                return this.Appointments != null && this.Appointments.Any();
            }
        }

        public bool ShowHonors
        {
            get
            {
                return this.Honors != null && this.Honors.Any();
            }
        }

        public bool ShowPublications
        {
            get
            {
                return this.Publications != null && this.Publications.Any();
            }
        }

        public bool ShowLinks
        {
            get
            {
                return this.Links != null && this.Links.Any();
            }
        }

        public static string FormatHonor(string yearFrom, string yearTo, string name, string organization)
        {
            var returnValue = name.Trim();
            if (!string.IsNullOrWhiteSpace(returnValue))
            {
                returnValue = returnValue + ", ";
            }
            returnValue = returnValue + organization.Trim(new[] { ',', ' ' });
            if (string.IsNullOrWhiteSpace(returnValue))
            {
                return string.Empty;
            }

            if (!string.IsNullOrWhiteSpace(yearFrom) && !string.IsNullOrWhiteSpace(yearTo))
            {
                returnValue = string.Format("{0}, {1}-{2}", returnValue, yearFrom, (yearTo.Equals("9999", StringComparison.OrdinalIgnoreCase) ? "present" : yearTo));
            }
            else if (!string.IsNullOrWhiteSpace(yearTo))
            {
                returnValue = string.Format("{0}, {1}", returnValue, (yearTo.Equals("9999", StringComparison.OrdinalIgnoreCase) ? "present" : yearTo));
            }
            return returnValue;
        }

        public static string FormatBackground(string yearFrom, string yearTo, string area)
        {
            var returnValue = area.Trim();
            if (string.IsNullOrWhiteSpace(returnValue))
            {
                return string.Empty;
            }

            if (!string.IsNullOrWhiteSpace(yearFrom) && !string.IsNullOrWhiteSpace(yearTo))
            {
                returnValue = string.Format("{0}, {1}-{2}", returnValue, yearFrom, (yearTo.Equals("9999", StringComparison.OrdinalIgnoreCase) ? "present" : yearTo));
            }
            else if (!string.IsNullOrWhiteSpace(yearTo))
            {
                returnValue = string.Format("{0}, {1}", returnValue, (yearTo.Equals("9999", StringComparison.OrdinalIgnoreCase) ? "present" : yearTo));
            }
            return returnValue;
        }

        public static Tuple<string, string> FormatLink(string url, string name)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                url = name;
            }
            return new Tuple<string, string>(url, name);
        }

        public static Tuple<string, string> FormatPublication(string url, string name)
        {
            return new Tuple<string, string>(url, name);
        }

        public static string FormatAppointment(string yearFrom, string yearTo, string name, string organization)
        {
            var returnValue = name.Trim();
            if (!string.IsNullOrWhiteSpace(returnValue))
            {
                returnValue = returnValue + ", ";
            }
            returnValue = returnValue + organization.Trim(new[] { ',', ' ' });
            if (string.IsNullOrWhiteSpace(returnValue))
            {
                return string.Empty;
            }

            if (!string.IsNullOrWhiteSpace(yearFrom) && !string.IsNullOrWhiteSpace(yearTo))
            {
                returnValue = string.Format("{0}, {1}-{2}", returnValue, yearFrom, (yearTo.Equals("9999", StringComparison.OrdinalIgnoreCase) ? "present" : yearTo));
            }
            else if (!string.IsNullOrWhiteSpace(yearTo))
            {
                returnValue = string.Format("{0}, {1}", returnValue, (yearTo.Equals("9999", StringComparison.OrdinalIgnoreCase) ? "present" : yearTo));
            }
            return returnValue;
        }
    }
}