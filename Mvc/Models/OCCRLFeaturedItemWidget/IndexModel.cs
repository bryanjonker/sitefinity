﻿namespace SitefinityWebApp.Mvc.Models.OCCRLFeaturedItemWidget
{
    public class IndexModel
    {
        public string Project { get; set; }

        public string Items { get; set; }

        public string Position { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public string Color { get; set; }

    }
}