﻿namespace SitefinityWebApp.Mvc.Models.RssFeedWidget
{
    using System.Collections.Generic;
    using System.Linq;

    using UIUC.Custom.XmlService;

    public class IndexModel
    {
        public string Header { get; set; }

        public string Footer { get; set; }

        public string Data { get; set; }

        public IndexModel(string template, IEnumerable<Article> articles)
        {
            foreach (var article in articles)
            {
                this.Data = this.Data + article.Translations.Aggregate(template, (current, translation) => current.Replace(translation.Key, translation.Value));
            }
        }
    }
}