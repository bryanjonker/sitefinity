﻿namespace SitefinityWebApp.Mvc.Models.Import
{
    using System;
    using System.Collections.Generic;

    using SitefinityWebApp.ProgramImport;

    public class ProgramViewModel
    {
        public bool Replace { get; set; }

        public string Results { get; set; }

        public string Description { get; set; }

        public string Department { get; set; }

        public Dictionary<string, string> Departments { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string CredentialConcentration { get; set; }

        public string CredentialDegree { get; set; }

        public Dictionary<string, string> CredentialDegrees { get; set; }

        public string CredentialNonDegree { get; set; }

        public Dictionary<string, string> CredentialNonDegrees { get; set; }

        public string CredentialLicense { get; set; }

        public string CredentialUrl { get; set; }

        public bool IsOnCampus { get; set; }

        public bool IsOffCampus { get; set; }

        public bool IsOnline { get; set; }

        public ProgramViewModel()
        {
            this.CredentialNonDegrees = new Dictionary<string, string>()
            {
                { "None", "None" },
                { "Certificate", "Certificate" },
                { "Endorsement", "Endorsement" },
                { "Grad_Minor", "Grad_Minor" },
                { "Undergrad_Minor", "Undergrad_Minor" }
            };

            this.CredentialDegrees = new Dictionary<string, string>()
            {
                { "None", "None" },
                { "BS", "BS" },
                { "EdM", "EdM" },
                { "MA", "MA" },
                { "MS", "MS" },
                { "CAS", "CAS" },
                { "EdD", "EdD" },
                { "PhD", "PhD" }
            };

            this.Departments = new Dictionary<string, string>()
            {
                { "CI", "CI" },
                { "EdPsy", "EdPsy" },
                { "EPOL", "EPOL" },
                { "SPED", "SPED" }
            };
        }

        public Program GetBaseProgram()
        {
            return new Program
            {
                Department = (DepartmentType)Enum.Parse(typeof(DepartmentType), this.Department),
                Description = this.Description,
                Name = this.Name,
                ShortName = this.ShortName,
                Credentials = new List<Credential>()
                {
                    new Credential
                    {
                        Degree = (DegreeType)Enum.Parse(typeof(DegreeType), this.CredentialDegree),
                        NonDegree = (CredentialType)Enum.Parse(typeof(CredentialType), this.CredentialNonDegree),
                        Concentration = this.CredentialConcentration,
                        License = this.CredentialLicense,
                        Id = this.CredentialId(),
                        Name = this.Name,
                        Url = this.CredentialUrl,
                        Offering = new Offering
                        {
                            IsOffCampus = this.IsOffCampus,
                            IsOnCampus = this.IsOnCampus,
                            IsOnline = this.IsOnline,
                            Title = this.ShortName + "-" + this.CredentialId(),
                            Url = this.CredentialUrl
                        }
                    }
                }
            };
        }

        private string CredentialId()
        {
            return !string.IsNullOrEmpty(this.CredentialDegree) && !this.CredentialDegree.Equals("none", StringComparison.OrdinalIgnoreCase) ? this.CredentialDegree : this.CredentialNonDegree;
        }
    }
}