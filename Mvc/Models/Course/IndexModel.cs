﻿namespace SitefinityWebApp.Mvc.Models.Course
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class IndexModel
    {
        public IndexModel() { }

        public string Id { get; set; }

        public bool IsCurrent { get; set; }

        public bool IsPast { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string CreditHours { get; set; }

        public string Formats { get; set; }

        public string TermsOffered { get; set; }

        public string Faculty { get; set; }

        public string PageTitle { get; set; }

        public string Url { get; set; }

        public IEnumerable<string> SectionInformation { get; set; }

        public IEnumerable<string> PastSectionInformation { get; set; }

        public IndexModel(Course course)
        {
            this.Id = course.Rubric + course.CourseNumber;
            this.Name = $"{course.Rubric} {course.CourseNumber}: {course.Title}";
            this.Description = course.Description.TrimEnd().EndsWith(".") ? course.Description.TrimEnd() : course.Description.TrimEnd() + ".";
            this.CreditHours = course.CreditHours;
            this.Formats = course.Formats;
            this.TermsOffered = course.TermsOffered;
            this.Faculty = string.Join(", ", course.Sections.OrderBy(p => p.FullName).Select(p => p.GetHtmlName()).Distinct());
            this.Url = $"https://courses.illinois.edu/schedule/DEFAULT/DEFAULT/{course.Rubric}/{course.CourseNumber}";
            this.SectionInformation = course.Sections.Where(x => x.EndDate >= DateTime.Today)
                .Select(s => $"<tr><td>{GetOnlineUrl(s.ScheduleType, s.TermCode, s.Crn, s.SectionNumber)}</td><td>{s.ScheduleType}</td><td>{s.Crn}</td><td>{s.BeginDate.ToShortDateString()}-{s.EndDate.ToShortDateString()}</td><td>{GetHtmlUrl(s.Username, s.FullName)}</td><td>{s.Days}</td><td>{s.Time}</td><td>{s.Room} {s.Building}</td></tr>");
            this.PastSectionInformation = course.Sections.Where(x => x.EndDate < DateTime.Today)
                .Select(s => $"<tr><td>{s.SectionNumber}</td><td>{s.ScheduleType}</td><td>{s.BeginDate.ToShortDateString()}-{s.EndDate.ToShortDateString()}</td><td>{GetHtmlUrl(s.Username, s.FullName)}</td><td>{s.Days}</td><td>{s.Time}</td><td>{s.Room} {s.Building}</td></tr>");
            if (string.IsNullOrWhiteSpace(this.Faculty))
            {
                this.Faculty = "N/A";
            }
            this.IsCurrent = this.SectionInformation.Any();
            this.IsPast = this.PastSectionInformation.Any();
        }


        private static string GetOnlineUrl(string scheduleType, string termCode, string crn, string sectionNumber)
        {
            return scheduleType.Equals("Online", StringComparison.OrdinalIgnoreCase) ? $"<a href='http://citl.illinois.edu/courses/section/{termCode}/{crn}'>{sectionNumber}</a>" : sectionNumber;
        }

        private static string GetHtmlUrl(string username, string fullName)
        {
            return string.IsNullOrWhiteSpace(username) ? fullName : $"<a href=\"/faculty/{username}\">{fullName}</a>";
        }
    }
}