﻿namespace SitefinityWebApp.Mvc.Models.SearchWidget
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    using UIUC.Custom.DAL.DataAccess.FacetedSearch.Model;

    public class EditModel
    {
        public List<Credential> Credentials { get; set; }

        public List<CredentialType> CredentialTypes { get; set; }

        public DepartmentType Department { get; set; }

        public List<SelectListItem> DepartmentTypes { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string OnlineUrl { get; set; }

        public List<SelectListItem> Programs { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public void Fix()
        {
            this.DepartmentTypes=new List<SelectListItem>
            {
                new SelectListItem { Text = "College of Education", Value = DepartmentType.College.ToString(), Selected = DepartmentType.College == this.Department },
                new SelectListItem { Text = "Education Policy, Organization and Leadership", Value = DepartmentType.EPOL.ToString(), Selected = DepartmentType.EPOL == this.Department },
                new SelectListItem { Text = "Educational Psychology", Value = DepartmentType.EdPsy.ToString(), Selected = DepartmentType.EdPsy == this.Department },
                new SelectListItem { Text = "Curriculum & Instruction" , Value = DepartmentType.CI.ToString(), Selected = DepartmentType.CI == this.Department },
                new SelectListItem { Text = "Special Education", Value = DepartmentType.SPED.ToString(), Selected = DepartmentType.SPED == this.Department }
            };

            this.Title=this.Title?.Trim();
            this.Url=string.IsNullOrWhiteSpace(this.Url)||this.Url=="NULL" ? string.Empty : this.Url;
            this.OnlineUrl=string.IsNullOrWhiteSpace(this.OnlineUrl)||this.OnlineUrl=="NULL" ? string.Empty : this.OnlineUrl;
            this.CredentialTypes=new List<CredentialType>
            {
                CredentialType.BS,
                CredentialType.EdM,
                CredentialType.MA,
                CredentialType.MS,
                CredentialType.CAS,
                CredentialType.EdD,
                CredentialType.PhD,
                CredentialType.Certificate,
                CredentialType.Endorsement,
                CredentialType.Undergrad_Minor,
                CredentialType.Grad_Concentration,
                CredentialType.Grad_Minor
            };
        }
    }
}