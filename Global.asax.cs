﻿namespace SitefinityWebApp
{
    using SitefinityWebApp.SitefinityPlugin;
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Telerik.Sitefinity.Abstractions;
    using Telerik.Sitefinity.Data;
    using Telerik.Sitefinity.Services;
    using Telerik.Sitefinity.SitemapGenerator.Abstractions.Events;

    public class Global : HttpApplication
    {
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Initialized -= this.BootstrapperInitialized;
            Bootstrapper.Initialized += this.BootstrapperInitialized;
        }

        protected void BeforeSitemapFileIsWritten(ISitemapGeneratorBeforeWriting @event)
        {
            @event.Entries = SitemapProcessor.AddItemsToSitemapEntries(@event.Entries);
        }

        protected void BootstrapperInitialized(object sender, ExecutedEventArgs args)
        {
            if (args.CommandName == "Bootstrapped")
            {
                RouteTable.Routes.MapRoute("ClassicDefault", "mvcroute", new { controller = "Home", action = "Index" });
                RouteTable.Routes.MapRoute("ClassicControllerActionId", "mvcroute/{controller}/{action}/{id}");
                RouteTable.Routes.MapRoute("ClassicControllerAction", "mvcroute/{controller}/{action}");
                EventHub.Subscribe<ISitemapGeneratorBeforeWriting>(this.BeforeSitemapFileIsWritten);
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }
    }
}